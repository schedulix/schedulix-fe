var  hooks = {
    //
    // Example Hook for Cancel Check
    // using a http request 
    // for o2 we have replace the command with a select canCancel(...) from dual
    // instead of checking CANCEL_CHECK for FAIL
    //

    // cancel:
    //     function (dialogData, tabInfo) {

    //         function namePathQuote(string) {
    //             let splittedStr = string.split(".");
    //             if (splittedStr[0] == "") {
    //                 splittedStr.shift();
    //             }
    //             if (splittedStr.length <= 0) {
    //                 return string;
    //             }
    //             for (let i = 1; i < splittedStr.length; i++) {
    //                 splittedStr[i] = "'" + splittedStr[i] + "'";
    //             }
    //             let res = splittedStr.join('.');
    //             return res;
    //         }

    //         function synchronousRequest(url) {
    //             const xhr = new XMLHttpRequest();
    //             xhr.open('POST', url, false);
    //             xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    //             xhr.send(null);
    //             if (xhr.status === 200) {
    //                 return xhr.responseText;
    //             } else {
    //                 throw new Error('Request failed: ' + xhr.statusText);
    //             }
    //         }

    //         if (dialogData.IVC_CANCEL_CHECK == "NO_CANCEL_CHECK") {
    //             return {};
    //         }
    //         let url = "http://192.168.0.77:8511/BICsuite/SDMS/webservice?" + 
    //             "SERVER=" + encodeURIComponent("DEFAULT") + "&" +
    //             "USER=" + encodeURIComponent("SYSTEM") + "&" +
    //             "PASSWORD=" + encodeURIComponent("G0H0ME") + "&" +
    //             "COMMAND=" + encodeURIComponent("SHOW JOB DEFINITION " + namePathQuote(dialogData.bicsuiteObject.SE_NAME));
    //         let result = JSON.parse(synchronousRequest(url));
    //         if (result.ERROR) {
    //             return result;
    //         }
    //         for (let parameter of result.DATA.RECORD.PARAMETER.TABLE) {
    //             if (parameter.NAME == 'CANCEL_CHECK' && parameter.DEFAULT_VALUE == 'FAIL') {
    //                 result = {
    //                     "ERROR": {
    //                         "ERRORCODE": "n/a",
    //                         "ERRORMESSAGE": dialogData.bicsuiteObject.SE_NAME + "[" + dialogData.bicsuiteObject.ID + "] does not allow cancel"
    //                     }
    //                 };
    //                 return (result);
    //             }
    //         }
    //         return {};
    //     }
}
