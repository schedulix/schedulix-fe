import { Pipe, PipeTransform } from '@angular/core';
import { TranslateService } from '../services/translate.service';

@Pipe({
  name: 'translate',

  //pure to auto check if values have changed
  pure: false
})
export class TranslatePipe implements PipeTransform {

  constructor(private translateService: TranslateService) {
  }

  transform(value: string, ...args: string[]): string {
    // pipe returns the return value of the translate service
    return this.translateService.translate(value)
  }

}
