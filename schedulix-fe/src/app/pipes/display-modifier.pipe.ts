import { Pipe, PipeTransform } from '@angular/core';
import { ToolboxService } from '../services/toolbox.service';

@Pipe({
  name: 'displayModifier'
})

export class DisplayModifierPipe implements PipeTransform {
  transform(value: string, args: { displayMode: string }): string {
    if (!value) return '';
    let result = "";
    if (args !== undefined && args.displayMode != undefined) {
      if (args.displayMode === "ESM") {
        result = this.esm_display(value);
      }
      else if (args.displayMode == "REMOVE_PATH") {
        result = this.remove_path(value);
      }
      else if (args.displayMode.startsWith("TRUNCATE_")) {
        result = this.truncate_string(value, parseInt(args.displayMode.substring("TRUNCATE_".length)));
      }
      else if (args.displayMode.startsWith("FIXED_")) {
        result = parseFloat(value).toFixed(parseInt(args.displayMode.substring("FIXED_".length)));
      }
      else if(args.displayMode === "DISPLAY_REFERENCE"){
        result = value.replace(/([^ ]*) *\(([^)]*)\)/, '$2 @ $1');
      }
      else if(args.displayMode === "REMOVE_WEBCONFIG"){
        // return directly because comments can be empty after replace
        return value.replace(/<web_config>.*<\/web_config>/si, "");
      }
      else {
        console.warn("DISPLAY MODIFIER '" + args.displayMode + "' does not exist.");
      }
      
    }

    return result === "" ? value : result;
  }

  private remove_path(value: string): string {
    if(!value) {
      // console.warn("cannot modify path on empty string")
      return '';
    }
    let rl : string[] = []
    let pArr:string[] = value.split(':');
    for (let p of pArr) {
      let strArr = p.split('.');
      rl.push(strArr[strArr.length - 1]);
    }
    return rl.join(' : ');
    // let strArr:string[] = value.split(".");
    // return strArr[strArr.length - 1];
  }

  private truncate_string(value: string, len: number): string {
    if (value == null || value == undefined) {
      return "";
    }
    if (value.length > len) {
      value = value.slice(0, len - 4) + " ...";
    }
    return value;
  }

  private esm_display(value: string): string {
    if (value == "2147483647")
      return "MAX";
    else if (value == "-2147483648")
      return "MIN";

    return value;
  }

}
