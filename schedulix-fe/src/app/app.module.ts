import { APP_INITIALIZER, ErrorHandler, NgModule, NgZone } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient, HttpClientModule, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GlobalSharedModule } from './modules/global-shared/global-shared.module';
import { CustomPropertiesService } from './services/custom-properties.service';
import { Injectable } from '@angular/core';
import { BicsuiteHttpException, BicsuiteJavascriptException, ErrorHandlerService, sdmsException } from './services/error-handler.service';
import { Observable } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs/internal/observable/throwError';
import { ErrorDialogComponent } from './modules/main/components/dialog-components/error-dialog/error-dialog.component';

@Injectable()
class BicsuiteErrorHandler implements ErrorHandler {

  constructor(private errorHandlerservice: ErrorHandlerService, private ngZone: NgZone) {
  }

  // if error in errorhandler itself, the errorhandler for javascript errors is broken to avoid endless error loop
  jsbroken: boolean = false

  handleError(error: Error | sdmsException | HttpErrorResponse | any) {
    if (!error) {
      return;
    }

    if(error.rejection) {
      console.warn('rejection error')
    }
    if (error instanceof sdmsException) {
      // set context so that the popup window is closeable
      this.ngZone.run(() => {
        if (error) {
          try {
            this.errorHandlerservice.createError(error)
          } catch (e) {
            console.error(e)
          }
        }
      });
    } else if (error instanceof HttpErrorResponse) {
      // http errors are handled at the http interceptor below
      console.error(error)
    } else {
      if (this.jsbroken) {
        // fall back to normal error handling
        console.error(error)
      } else {
        // console.error(error)
        // console.error(error)
        // check if error is type of string
        if (error.message && error.message.startsWith('Error retrieving icon')) {
          console.error('Error retrieving icon')
          return
        }
        try {
          this.ngZone.run(() => {
            this.errorHandlerservice.createError(new BicsuiteJavascriptException(error.stack))
          });
        } catch (e: any) {
          // if error occurs here set to broken state
          this.jsbroken = true
          console.error('error inside the error handler, error do not pop up anymore!')
          console.error(e)
        }
      }
    }
  }
}

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

  constructor(private errorHandlerservice: ErrorHandlerService, private ngZone: NgZone) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(retry(2), catchError((error: HttpErrorResponse) => {
      console.error(error)
      this.ngZone.run(() => {
        this.errorHandlerservice.createError(new BicsuiteHttpException(error.error, error.message, error.statusText, error.url + ''))
      });
      return throwError(error);
    })); // Retry failed request up to 2 times.
  }
}

@NgModule({
  declarations: [
    //Global Components
    AppComponent,
    ErrorDialogComponent
    // HighlightPipe - Pipes must be added in global-shared module!!!,
  ],
  imports: [
    BrowserModule,

    //inport global components like translate
    GlobalSharedModule,

    //Global Routing Module
    AppRoutingModule,

    //Browser Animations Package
    BrowserAnimationsModule,

    HttpClientModule,

    FormsModule,

    ReactiveFormsModule,

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
    { provide: ErrorHandler, useClass: BicsuiteErrorHandler },
    {
      provide: APP_INITIALIZER,
      useFactory: (customPropertiesService: CustomPropertiesService) => function () { return customPropertiesService.loadCustomJsonObjects() },
      deps: [CustomPropertiesService],
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
