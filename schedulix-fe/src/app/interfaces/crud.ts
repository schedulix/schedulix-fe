import { Injectable } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Router } from '@angular/router';
import { resolve } from 'dns';
import { throwError } from 'rxjs';
import { BookmarkingCrud } from '../classes/bookmarking-crud';
import { CommentCrud } from '../classes/comment-crud';
import { ExitStateDefinitionCrud } from '../classes/exit-state-definition-crud';
import { ExitStateMappingCrud } from '../classes/exit-state-mapping-crud';
import { ExitStateProfileCrud } from '../classes/exit-state-profile-crud';
import { ExitStateTranslationCrud } from '../classes/exit-state-translation-crud';
import { GroupCrud } from '../classes/group-crud';
import { JobCrud } from '../classes/job-crud';
import { ResourceStateDefinitionCrud } from '../classes/resource-state-definition-crud';
import { ResourceStateMappingCrud } from '../classes/resource-state-mapping-crud';
import { ResourceStateProfileCrud } from '../classes/resource-state-profile';
import { SysinfoCrud } from '../classes/sysinfo-crud';
import { ApprovalCrud } from '../classes/approval-crud';
import { CriticalPathCrud } from '../classes/critical-path-crud';
import { TriggerCrud } from '../classes/trigger-crud';
import { UserCrud } from '../classes/user-crud';
import { EditorformOptions, EditorformRender } from '../data-structures/editorform-options';
import { Tab, TabMode } from '../data-structures/tab';
import { OutputType, SnackbarType } from '../modules/global-shared/components/snackbar/snackbar-data';
import { EditorformDialogComponent, EditorformDialogData } from '../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component';
import exportDialogForm from '../json/editorforms/exportDialog.json';
import { BookmarkService } from '../modules/main/services/bookmark.service';
import { PrivilegesService } from '../modules/main/services/privileges.service';
import { SubmitEntityService } from '../modules/main/services/submit-entity.service';
import { TreeFunctionsService } from '../modules/main/services/tree-functions.service';
import { CommandApiService } from '../services/command-api.service';
import { CustomPropertiesService } from '../services/custom-properties.service';
import { EventEmitterService } from '../services/event-emitter.service';
import { ToolboxService } from '../services/toolbox.service';
import { TranslateService } from '../services/translate.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';

import { sharedCrud } from '../interfaces/shared-crud'
import { MainInformationService } from '../modules/main/services/main-information.service';
import { ClipboardService, SdmsClipboardMode, SdmsClipboardType } from '../modules/main/services/clipboard.service';
import { Bookmark } from '../data-structures/bookmark';
import { frontendExceptionTypes, sdmsException } from '../services/error-handler.service';

// Interface for creating new crud object.
// Workaround for the abstract class enforcement.
export abstract class Crud {
    constructor(protected commandApi: CommandApiService,
        protected bookmarkService: BookmarkService,
        protected router: Router,
        protected toolbox: ToolboxService,
        protected eventEmitterService: EventEmitterService,
        protected privilegesService: PrivilegesService,
        protected dialog: MatDialog,
        protected treeFunctionService: TreeFunctionsService,
        protected customPropertiesService: CustomPropertiesService,
        protected submitEntityService: SubmitEntityService,
        protected mainInformationService: MainInformationService,
        protected clipboardService: ClipboardService) { }

    abstract create(obj: any, tabInfo: Tab): Promise<Object>;
    abstract read(obj: any, tabInfo: Tab): Promise<Object>;
    abstract update(obj: any, tabInfo: Tab): Promise<Object>;
    abstract drop(obj: any, tabInfo: Tab): Promise<Object>;

    getMethod(name: string): any {
        interface IIndexable {
            [key: string]: any;
        };
        let method = (this as IIndexable)[name];
        if (!method) {
            return undefined;
        }
        else {
            return method;
        }
    }

    abstract editorform(): any;

    sharedCrud = new sharedCrud(this.commandApi,
        this.bookmarkService,
        this.router,
        this.toolbox,
        this.eventEmitterService,
        this.privilegesService,
        this.dialog,
        this.treeFunctionService,
        this.customPropertiesService,
        this.submitEntityService,
        this.mainInformationService)

    custom(params: any, bicsuiteObject: any, tabInfo?: Tab) {
        /* Functionality is added by classes that inherit from Crud */
    }

    getConfig(key: string, defaultValue: any): any {
        let o: any = this.customPropertiesService.getCustomObjectProperty(key);
        if (o) {
            return o;
        }
        else {
            return defaultValue;
        }
    }

    checkFieldCondition(conditionMethodName: string, editorformField: any, bicsuiteObject: any, tabInfo: Tab, table?: any) {
        let check: boolean = true;
        if (conditionMethodName[0] == "!") {
            conditionMethodName = conditionMethodName.substring(1);
            check = false;
        }
        let method = this.getMethod(conditionMethodName);
        if (!method) {
            console.warn("bad " + conditionMethodName + " for " + editorformField.NAME);
            return false;
        }
        method = method.bind(this);
        let result: boolean = method(editorformField, bicsuiteObject, tabInfo, table);
        if (typeof result != "boolean") {
            console.warn("field condition " + conditionMethodName + " of field " + editorformField.NAME + " return value of type " + typeof result + " instead of boolean!");
        }
        return (result == check);
    }

    checkCSSCondition(conditionMethodName: string, editorformField: any, bicsuiteObject: any, tabInfo: Tab, table?: any) {

        let method = this.getMethod(conditionMethodName).bind(this);
        let ret = method(editorformField, bicsuiteObject, tabInfo, table);
        // console.log(ret);
        return ret;
    }

    filterMethod(conditionMethodName: string, editorformField: any, bicsuiteObject: any, tabInfo: Tab, arg?: any) {

        let method = this.getMethod(conditionMethodName).bind(this);
        let result = method(editorformField, bicsuiteObject, tabInfo, arg);
        // console.log(ret);
        return result;
    }

    manipulateEditorformOptions(editorformOptions: EditorformOptions, editorformField: any, bicsuiteObject: any, tabInfo: Tab, table?: any) {
        if (editorformField.TYPE == "COMMENT" || ["CREATOR", "CREATE_TIME","CHANGER", "CHANGE_TIME"].includes(editorformField.NAME)) {
            editorformOptions.render.hidden = true;
            if (!editorformField.VISIBLE_CONDITION) {
                editorformField.VISIBLE_CONDITION = "!conditionIsNew";
            }
        }
        if (editorformField.hasOwnProperty("VISIBLE_CONDITION")) {
            editorformOptions.render.hidden = !this.checkFieldCondition(editorformField["VISIBLE_CONDITION"], editorformField, bicsuiteObject, tabInfo, table);
        }
        if (editorformField.hasOwnProperty("ENABLE_CONDITION")) {
            // only check if the rendered option is not already disabled due to PRIVS or HIDDEN
            if (editorformOptions.render.disabled != true || editorformField.FORCE_ENABLE_CONDITION) {
                editorformOptions.render.disabled = !this.checkFieldCondition(editorformField["ENABLE_CONDITION"], editorformField, bicsuiteObject, tabInfo, table);
            }

        }
        if (editorformField.hasOwnProperty("CSS_CONDITION")) {
            editorformOptions.flags.CSS_CLASS += this.checkCSSCondition(editorformField["CSS_CONDITION"], editorformField, bicsuiteObject, tabInfo, table);
        }
    }

    callMethod(name: string, bicsuiteObject: any, tabInfo: Tab, arg?: any, arg1?: any) {
        bicsuiteObject.F_REFRESH_NAVIGATOR = false;
        let method;
        try {
            method = this.getMethod(name).bind(this);
        }
        catch {
            console.warn("METHOD " + name + " DOES NOT EXIST")
        }
        let promise: Promise<any>;
        try {
            promise = method(bicsuiteObject, tabInfo, arg, arg1);
        }
        catch (e: any) {
          // console.log(e)
            if (e.ERROR == undefined) {
                throw (e);
                // throw new sdmsException(frontendExceptionTypes.sdmsException, e.ERROR,)
            }
            promise = Promise.resolve(e);
        }
        promise.then((result) => {
            this.afterCallMethod(name, bicsuiteObject, tabInfo, result);
        },this.passThroughRejection);
        return promise;
    }

    afterCallMethod(method: string, bicsuiteObject: any, tabInfo: Tab, result: any) {

        if (method == "read" && result.hasOwnProperty('reponse')) {
            // crud read has slightly different response structure, mapped to normal crud methods
            result = result.response;
        }

        // Output messages for user
        // console.log(result  );
        if (result && result.hasOwnProperty("FEEDBACK")) {
            // this.eventEmitterService.pushSnackBarMessage([result.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);
            // })).catch((err: any) => {
            //     this.eventEmitter.pushSnackBarMessage(['connection_problem'], SnackbarType.ERROR, OutputType.NONE_SEPARATED);
            // });
            // console.log(method)
            // console.log(tabInfo)
            switch (method) {
                case "drop":

                    this.eventEmitterService.pushSnackBarMessage([result.FEEDBACK], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                    this.eventEmitterService.removeTabEmitter.next(tabInfo);
                    this.eventEmitterService.refreshNavigationEmittor.next();
                    break;
                case "save":

                    if (bicsuiteObject.F_REFRESH_NAVIGATOR == true || this.conditionIsNew(null, bicsuiteObject, tabInfo)) {
                        this.eventEmitterService.refreshNavigationEmittor.next();
                    }
                    tabInfo.ID = bicsuiteObject.ID;
                    if (!bicsuiteObject.NAME) {
                        console.warn("bicsuiteObject does not contain Property NAME");
                    }
                    tabInfo.NAME = bicsuiteObject.NAME;
                    tabInfo.PATH = bicsuiteObject.PATH;
                    tabInfo.MODE = TabMode.EDIT;
                    tabInfo.ID = bicsuiteObject.ID;
                    this.eventEmitterService.pushSnackBarMessage([result.FEEDBACK], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                    this.eventEmitterService.updateTabEmitter.next(tabInfo);
                    break;
                case "clone":
                    this.eventEmitterService.pushSnackBarMessage(['cloned object ' + bicsuiteObject.NAME], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                    // this.eventEmitterService.removeTabEmitter.next(tabInfo);

                    tabInfo.NAME = bicsuiteObject.NAME;
                    tabInfo.PATH = bicsuiteObject.PATH;
                    // need new id of created object
                    // id is not returned when creating an id
                    // rest ID to -1 to tell the read commands to use Path + Name instead of ID
                    tabInfo.ID = "-1";
                    // console.log(bicsuiteObject)

                    this.eventEmitterService.updateTabEmitter.next(tabInfo);
                    this.eventEmitterService.refreshNavigationEmittor.next();

                    // this.eventEmitterService.updateTabEmitter.next(tabInfo);

                    // this.eventEmitterService.createTab(tabInfo, true);
                    // this.eventEmitterService.refreshNavigationEmittor.next();
                    break;
                case "read":
                    // trigger render after read
                    this.eventEmitterService.selectedTabEmitter.next(result.DATA.RECORD.ID);
                    break;
            }
        }
    }

    onTabLoad(bicsuiteObject: any, tab: Tab, form: any, getBicsuiteData?: Function): Promise<any> {
        // do nothing, to be overwritten by the sub-cruds
        return Promise.resolve({});
    }

  // default rejection
  passThroughRejection(rejection:any){
    // console.log("no?")
    // simple pass through because we maybe want to handle the error aka rejections unique
    return rejection;
  }
  emptyRejection(rejection:any) {
    // empty object so that .hasOwnProperty does not fail
    return {}
  }



    // --------------------------------- shared crud-methods -------------------------


    default(tabInfo: Tab): any {
        // every object has PRIVS E when created ? TODO REWORK THIS
        // + this PRIVS are translated to the owned privileges. Not the required. REWORK!!!
        return { DATA: { RECORD: { 'PRIVS': "E" } } };
    }

    clone(bicsuiteObject: any, tabInfo: Tab) {
        return this.create(bicsuiteObject, tabInfo);
    }

    save(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        if (tabInfo.MODE == TabMode.NEW) {
            return this.create(bicsuiteObject, tabInfo);
        }
        else {
            return this.update(bicsuiteObject, tabInfo);
        }
    }

    revert(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "revert_confirm", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                this.eventEmitterService.updateTabEmitter.next(tabInfo);
            }
            return Promise.resolve({});
        });
    }

    reload(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        this.eventEmitterService.updateTabEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    refresh(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.reload(bicsuiteObject, tabInfo)
    }


    lockUnlockTabMethod(promiseFromMethod: Promise<any>, tabInfo: Tab): Promise<any> {
      // console.log(parameters);
      this.eventEmitterService.setTabLockEmitter.next({Tab: tabInfo, lock: true});
      return promiseFromMethod.then((result)=>{
        this.eventEmitterService.setTabLockEmitter.next({Tab: tabInfo, lock: false});
        return result;
      }, (rejection)=>{
        this.eventEmitterService.setTabLockEmitter.next({Tab: tabInfo, lock: false});
        return {};
      })
    }

    createConfirmDialog(additionalFields: any[], confirmText: string, tabInfo: Tab, method: string, bicsuiteObject: any, initDataObj?: any) {
        let translatorService = new TranslateService(new LocalStorageHelper(this.eventEmitterService));
        confirmText = translatorService.translate(confirmText);
        let submitBeforeClose = 'CLOSE_WITH_DATA';
        let submitMethod = undefined;
        if (method != '') {
            submitBeforeClose = "EXECUTE_AND_CLOSE";
            submitMethod = method;
        }
        let dialogForm = {
            "FIELDS": additionalFields,
            "DIALOG_BUTTONS": [
                {
                    "MODE": "CLOSE",
                    "TRANSLATE": "close",
                    "SYS_OBJ_EDITABLE": true,
                    "STROKED": true,
                },
                {
                    "MODE": submitBeforeClose,
                    "METHOD": submitMethod,
                    "SYS_OBJ_EDITABLE": true,
                    "TRANSLATE": "confirm"
                }
            ]
        };
        let dataObj: any = {};
        if (initDataObj) {
            dataObj = initDataObj;
        }
        if (!dataObj.PRIVS) {
            dataObj.PRIVS = 'E';
        }
        dataObj.bicsuiteObject = bicsuiteObject;
        if (additionalFields == null || additionalFields == undefined) {
            additionalFields = [];
        }
        // add confirm text
    additionalFields.unshift({
            "NAME": "confirmText",
            "USE_LABEL": false,
            "TYPE": "TEXT",
            "DEFAULT": confirmText,
            "CSS_CLASS": "confirm-text"
        })

        additionalFields.forEach(field => {
            let value: any = null;
            if (field["DEFAULT"] != undefined && dataObj[field["NAME"]] == undefined) {
                dataObj[field["NAME"]] = field["DEFAULT"];
            }
        });

        return this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(dialogForm, dataObj, tabInfo, translatorService.translate("confirm"), this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "800px"
        });
    }

    export(obj: any, tabInfo: Tab): Promise<Object> {

        // console.log(tabInfo.TYPE)
        let type = tabInfo.TYPE;
        obj.TYPE == 'JOB' || obj.TYPE == 'BATCH' || obj.TYPE == 'MILESTONE' ? type = 'JOB DEFINITION' : '';
        obj.TYPE == 'CATEGORY' || obj.TYPE == 'STATIC' || obj.TYPE == 'SYSTEM' || obj.TYPE == 'SYNCHRONIZING' ? type = 'NAMED RESOURCE' : '';
        obj.TYPE == 'SERVER' || obj.TYPE == 'SCOPE' ? type = 'SCOPE' : '';
        let name = ''
        tabInfo.PATH ? name = tabInfo.PATH + '.' + tabInfo.NAME : name = tabInfo.NAME;
        let modes = this.sharedCrud.getExportModes(type);
        // console.log(modes)
        // console.log(modes.length == 0)
        if (!tabInfo.NAME) {
            type = '';
        }
        let dataObj: any = {
            NAME: name,
            TYPE: type,
            EXPORT_TYPE: 'ALL',
            MODES: modes,
            MODE: modes[0].NAME,
            GRANT: 'false',
            INCLUDE_SCHEDULES: 'false',
            LANGLEVEL: this.mainInformationService.getSystem().DumpLangLevel,
            FILE: ''
        };

        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(exportDialogForm, dataObj, tabInfo, "(Import)Export", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "700px"
        });

        return dialogRef.afterClosed().toPromise().then((result: any) => {
            //console.log(result);
            if (result != undefined || result != null && result) {

                if (result.hasOwnProperty('ERROR')) {
                    return 1;
                } else {
                    // console.log(result)
                    return result;
                }
                // });
            } else {
                return 1;
            }

        });
    }

    checkFile(obj: any, tabInfo: Tab): Promise<Object> {
        obj.CHECK_ONLY = "true";
        return this.importFile(obj, tabInfo);
    }


    importFile(obj: any, tabInfo: Tab): Promise<Object> {
        // console.log('importFile');
        let promise = new Promise<string>((resolve, reject) => {
            if (!obj.FILE) {
                // console.log("no file!")
                resolve('');
            }
            else {
                var fileReader = new FileReader();
                fileReader.onload = () => {
                    if (fileReader.result) {
                        let cmd : string = fileReader.result.toString();
                        cmd = cmd.replace('\r\n','\n');
                        let type = obj.TYPE;
                        let typeTag = 'EXPORT_TYPE =';
                        let nameTag = 'EXPORT_NAME =';
                        let scopeTag = 'EXPORT_SCOPE =';
                        let exportType = '';
                        let exportName = '';
                        let exportScope = '';
                        let lines = cmd.split('\n');
                        for (let line of lines ) {
                            let trimmedLine = line.trim();
                            if (trimmedLine.startsWith(typeTag)) {
                                exportType = trimmedLine.substring(typeTag.length).trim();
                            }
                            if (trimmedLine.startsWith(nameTag)) {
                                exportName = trimmedLine.substring(nameTag.length).trim();
                            }
                            if (trimmedLine.startsWith(scopeTag)) {
                                exportScope = trimmedLine.substring(scopeTag.length).trim();
                            }
                        }
                        let acceptAnyScript = false;
                        if (obj.NAME == '' && obj.ACCEPT_ANY_SCRIPT == "true") {
                            acceptAnyScript = true
                        }
                        if ((exportType == '' || exportName == '') && !acceptAnyScript) {
                            this.commandApi.throwSdmsException('ZSI-02708301318', 'EXPORT_TYPE and/or EXPORT_NAME not found in script', 'Import File');
                            return reject({});
                        }

                        if (acceptAnyScript) {
                            let bmc = /begin[ \t\n]+multicommand/i;
                            let emc = /end[ \t\n]+multicommand[ \t\n]+[^;]*;/i;
                            cmd = cmd.replace(bmc, '');
                            cmd = cmd.replace(emc, '');
                            cmd = "begin multicommand\n" + cmd + ";\nend multicommand /* rollback */";
                        }

                        if (['FOLDER','NAMED RESOURCE CATEGORY','SCOPE','SERVER'].includes(type)) {
                            let validTypes: any[] = [];
                            if (type == 'SCOPE') {
                                validTypes = ['SCOPE','SERVER','RESOURCE','POOL'];
                            }
                            if (type == 'SERVER') {
                                validTypes = ['RESOURCE','POOL'];
                            }
                            if (type == 'FOLDER') {
                                validTypes = ['FOLDER','BATCH','JOB DEFINITION','MILESTONE','JOB'];
                            }
                            if (type == 'NAMED RESOURCE CATEGORY') {
                                validTypes = ['NAMED RESOURCE CATEGORY','NAMED RESOURCE'];
                            }
                            if (!validTypes.includes(exportType)) {
                                this.commandApi.throwSdmsException('ZSI-02708301319', 'Invalid EXPORT_TYPE ' + exportType + ' not in '
                                    + validTypes.toString(), 'Import File');
                                return reject({});
                            }
                            let expName = this.toolbox.namePathQuote(exportName);
                            let curName = this.toolbox.namePathQuote(obj.NAME);
                            let toReplace = expName;
                            let replacement = curName;
                            let pathList = expName.split('.');
                            let baseName = pathList.pop();

                            if (['FOLDER','NAMED RESOURCE CATEGORY','SCOPE','SERVER'].includes(exportType)) {
                                replacement = replacement + '.' + baseName;
                            }

                            if (['BATCH','JOB','MILESTONE','NAMED RESOURCE','JOB DEFINITION'].includes(exportType)) {
                                toReplace = pathList.join('.'); // already popped above
                                // console.log(toReplace);
                            }

                            if (['RESOURCE','POOL'].includes(exportType)) {
                                if (!exportScope) {
                                    this.commandApi.throwSdmsException('ZSI-02708301320', 'EXPORT_SCOPE not found in script', 'Import File');
                                    return reject({});
                                }
                                toReplace = this.toolbox.namePathQuote(exportScope);
                            }
                            // console.log(toReplace);
                            // console.log(replacement);
                            const searchRegExp = new RegExp(toReplace, 'g');
                            cmd = cmd.replace (searchRegExp, replacement);
                        }
                        let checkOnly = (obj.CHECK_ONLY == "true");
                        if (checkOnly) {
                            let newCmd = cmd.replace("end multicommand /* rollback */", "end multicommand rollback");
                            // if (newCmd == cmd) {
                            //     this.commandApi.throwSdmsException('ZSI-02708301321', 'Check Only option only available with export files generated from the GUI', 'Import File');
                            //     return reject({});
                            // }
                            cmd = newCmd;
                        }

                        // TODO: folder import hook
                        // if hasattr(context.Custom,'FolderImportHook'):
                        //     script = context.Custom.FolderImportHook(REQUEST['NAME'], script)

                        // console.log(cmd);
                        let message = "File successfully imported."
                        return this.commandApi.execute(cmd, false).then((result: any) => {
                            if (result.hasOwnProperty('ERROR')) {
                                if (result.ERROR.ERRORCODE != "03704251206") {
                                    this.commandApi.throwSdmsException(result.ERROR.ERRORCODE, result.ERROR.ERRORMESSAGE, cmd);
                                    return reject({});
                                }
                            }
                            if (checkOnly) {
                                message = "File successfully checked to be importable.";
                                result.FEEDBACK = message;
                            }
                            else {
                                if (obj.TYPE == "FOLDER") {
                                    // send a fake create message to make the folder refresh its contents after import
                                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "FOLDER", OP : "CREATE", NAME : obj.NAME + ".DUMMY"});
                                }
                            }
                            this.eventEmitterService.pushSnackBarMessage([message], SnackbarType.INFO);
                            resolve(result);
                        });
                    }
                    else {
                        resolve ('');
                        return ''; // why is this neccessary ?
                    }
                };
                fileReader.onerror = reject;
                fileReader.readAsText(obj.FILE);
            }
        });
        // console.log(promise);
        return promise;
    }

    filterGroupChooserUserInGroupOrCurrent(editorformField: any, bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): any {
        let userGroups : string[] = this.privilegesService.getGroups();
        let current: string = parentBicsuiteObject.ORIGINAL_OWNER;
        if (current && !userGroups.includes(current) || !this.privilegesService.isAdmin()) {
            userGroups.push(current);
        }
        let result = [];
        for (let row of bicsuiteObject) {
            if (userGroups.includes(row.NAME) || this.privilegesService.isAdmin()) {
                result.push(row);
            }
        }
        return result;
    }

    exportOpenScriptInShell(obj: any, tabInfo: Tab): Promise<Object> {
        let type = obj.TYPE;
        if (obj.NAME == '') {
            type = obj.EXPORT_TYPE;
        }
        obj.TYPE == 'JOB' || obj.TYPE == 'BATCH' || obj.TYPE == 'MILESTONE' ? type = 'JOB DEFINITION' : '';
        obj.TYPE == 'CATEGORY' || obj.TYPE == 'STATIC' || obj.TYPE == 'SYSTEM' || obj.TYPE == 'SYNCHRONIZING' ? type = 'NAMED RESOURCE' : '';
        obj.TYPE == 'SERVER' || obj.TYPE == 'SCOPE' ? type = 'SCOPE' : '';
        let cmt = '';

        cmt = this.sharedCrud.createDump(type, obj.NAME, obj.LANGLEVEL, obj.MODE, obj.GRANT == "true", obj.INCLUDE_SCHEDULES == 'true');
        return this.commandApi.execute(cmt).then((result: any) => {
            if (result.hasOwnProperty('ERROR')) {
                return result;
            } else {
                // console.log(result.DATA.RECORD.TEXT)
                let ShellTab = new Tab('Shell', new Date().getTime().toString(), 'Shell'.toUpperCase(), 'shell', TabMode.SHELL);
                ShellTab.DATA = result.DATA.RECORD.TEXT;
                this.eventEmitterService.createTab(ShellTab, true);
                return result;
            }
        });
    }

    exportDownloadScript(obj: any, tabInfo: Tab): Promise<Object> {
        let type = obj.TYPE;
        if (obj.NAME == '') {
            type = obj.EXPORT_TYPE;
        }
        obj.TYPE == 'JOB' || obj.TYPE == 'BATCH' || obj.TYPE == 'MILESTONE' ? type = 'JOB DEFINITION' : '';
        obj.TYPE == 'CATEGORY' || obj.TYPE == 'STATIC' || obj.TYPE == 'SYSTEM' || obj.TYPE == 'SYNCHRONIZING' ? type = 'NAMED RESOURCE' : '';
        obj.TYPE == 'SERVER' || obj.TYPE == 'SCOPE' ? type = 'SCOPE' : '';
        let cmt = '';

        cmt = this.sharedCrud.createDump(type, obj.NAME, obj.LANGLEVEL, obj.MODE, obj.GRANT == "true", obj.INCLUDE_SCHEDULES == 'true');
        return this.commandApi.execute(cmt).then((result: any) => {
            if (result.hasOwnProperty('ERROR')) {
                return result;
            } else {
                // console.log(result)
                download(obj.NAME + '_smds.txt', result.DATA.RECORD.TEXT)
                return result;
            }
        });

        function download(filename: string, text: string) {
            var element = document.createElement('a');
            element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
            element.setAttribute('download', filename);

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
        }
    }

    exportViewScript(obj: any, tabInfo: Tab): Promise<Object> {
        let type = obj.TYPE;
        if (obj.NAME == '') {
            type = obj.EXPORT_TYPE;
        }
        obj.TYPE == 'JOB' || obj.TYPE == 'BATCH' || obj.TYPE == 'MILESTONE' ? type = 'JOB DEFINITION' : '';
        obj.TYPE == 'CATEGORY' || obj.TYPE == 'STATIC' || obj.TYPE == 'SYSTEM' || obj.TYPE == 'SYNCHRONIZING' ? type = 'NAMED RESOURCE' : '';
        obj.TYPE == 'SERVER' || obj.TYPE == 'SCOPE' ? type = 'SCOPE' : '';
        let cmt = '';
        cmt = this.sharedCrud.createDump(type, obj.NAME, obj.LANGLEVEL, obj.MODE, obj.GRANT == "true", obj.INCLUDE_SCHEDULES == 'true');
        return this.commandApi.execute(cmt).then((result: any) => {

            if (result.hasOwnProperty('ERROR')) {
                return result;
            } else {
                // console.log(result)
                // let url =
                let w = window.open('about:blank', '_blank', "scrollbars=yes, resizable=yes, width=800, height=600");
                // w?.document.write(result.DATA.RECORD.TEXT);
                w?.document.write('<div style="white-space: pre;">' + result.DATA.RECORD.TEXT + '</div>');
                try {
                    w ? w.focus() : '';
                } catch (e) {
                    alert("Failed to open popup. Check your browser settings to allow javascript to open popup windows");
                }
                return result;
            }
        });
    }

    convertIdentifier(identifier: string, oldIdentifier: string) {
        if (this.mainInformationService.getPrivateProperty("NAME_UPPERCASING", "nameUppercasing") == "false") {
            return identifier;
        }
        identifier = identifier.trim();
        if (identifier.startsWith('\'')) {
            identifier = identifier.substring(1);
        }
        else {
            if (oldIdentifier == null ||
                oldIdentifier == undefined ||
                oldIdentifier == '' ||
                oldIdentifier.toUpperCase() == oldIdentifier) {
                identifier = identifier.toUpperCase();
            }
        }
        if (identifier.endsWith('\'')) {
            identifier = identifier.slice(0, -1);
        }
        return identifier;
    }

    getQuotedFullName(tabInfo: Tab) {
        return (tabInfo.PATH && tabInfo.PATH != '') ? this.toolbox.namePathQuote(tabInfo.PATH + "." + tabInfo.NAME) : tabInfo.NAME
    }


    openExitStateDefinition(name : string, focus : boolean = true): Promise<any> {
        let tab = new Tab(name,"", "EXIT STATE DEFINITION", "esd", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, focus);
        return Promise.resolve({});
    }

    openResourceStateDefinition(name : string, focus : boolean = true): Promise<any> {
        let tab = new Tab(name,"", "RESOURCE STATE DEFINITION", "rsd", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, focus);
        return Promise.resolve({});
    }

    openResourceStateMapping(name : string): Promise<any> {
        let tab = new Tab(name,"", "RESOURCE STATE MAPPING", "rsm", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openCreator(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any) : Promise<any> {
        return this.openUser(bicsuiteObject.CREATOR);
    }

    openChanger(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any) : Promise<any> {
        return this.openUser(bicsuiteObject.CHANGER);
    }

    openNamedResource(name : string, type: string): Promise<any> {
        let p = name.split('.');
        let n = p.pop();
        if (n) {
            let tab = new Tab(n,"", "NAMED RESOURCE", type, TabMode.EDIT);
            tab.PATH = p.join('.');
            this.eventEmitterService.createTab(tab, true);
        }
        return Promise.resolve({});
    }

    openResourceName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openNamedResource(bicsuiteObject.RESOURCE_NAME, bicsuiteObject.RESOURCE_USAGE.toLowerCase());
    }

    openCategory(name : string): Promise<any> {
        let p = name.split('.');
        let n = p.pop();
        if (n) {
            let tab = new Tab(n,"", "CATEGORY", "category", TabMode.EDIT);
            tab.PATH = p.join('.');
            this.eventEmitterService.createTab(tab, true);
        }
        return Promise.resolve({});
    }

    openExitStateMapping(name : string): Promise<any> {
        let tab = new Tab(name,"", "EXIT STATE MAPPING", "esm", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openExitStateProfile(name : string): Promise<any> {
        let tab = new Tab(name,"", "EXIT STATE PROFILE", "esp", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openExitStateTranslation(name : string): Promise<any> {
        let tab = new Tab(name,"", "EXIT STATE TRANSLATION", "est", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openEnv(name : string): Promise<any> {
        let tab = new Tab(name,"", "ENVIRONMENT", "env", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openFootprint(name : string): Promise<any> {
        let tab = new Tab(name,"", "FOOTPRINT", "footprint", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openInterval(name : string): Promise<any> {
        let tab = new Tab(name,"", "INTERVAL", "interval", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openScope(name : string, type: string): Promise<any> {
        let p = name.split('.');
        let n = p.pop();
        if (n) {
            let tab = new Tab(n, '', "SCOPE", type, TabMode.EDIT);
            tab.PATH = p.join('.');
            this.eventEmitterService.createTab(tab, true);
        }
        return Promise.resolve({});
    }

    openCalendar(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openInterval(bicsuiteObject.CALENDAR);
    }

    openScopePath(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openScope(bicsuiteObject.PATH, bicsuiteObject.TYPE.toLowerCase());
    }

    openFolder(name : string): Promise<any> {
        let p = name.split('.');
        let n = p.pop();
        if (n) {
            let tab = new Tab(n, '', "FOLDER", "folder", TabMode.EDIT);
            tab.PATH = p.join('.');
            this.eventEmitterService.createTab(tab, true);
        }
        return Promise.resolve({});
    }

    openTabForPath(name : string, type : string): Promise<any> {
        if (name.endsWith(']')) {

            let n;
            n = name.split('[')[0];
            let path = '';
            let p = n.split('.');
            if (p.length > 1) {
                n = p.pop();
                path = p.join('.');
            }
            let id = name.split('[')[1].split(']')[0]
            if (n) {
                let tab = new Tab(n, id, 'SUBMITTED ENTITY', 'submitted_entity', TabMode.EDIT);
                tab.PATH = path;
                this.eventEmitterService.createTab(tab, true);
            }
        }
        else {
            let p = name.split('.');
            let n = p.pop();
            let tab = new Tab(n ? n : '', '' , type.toUpperCase(), type, TabMode.EDIT);
            tab.PATH = p.join('.');
            this.eventEmitterService.createTab(tab, true);
        }
        return Promise.resolve({});
    }

    openResourceById(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let type = tabInfo.TYPE.toLowerCase();
        if (type == 'named resource') {
            type = tabInfo.shortType;
        }
        let tab = new Tab(bicsuiteObject.NAME, bicsuiteObject.ID, 'RESOURCE', type, TabMode.EDIT);
        tab.DATA = {};
        tab.DATA.RESOURCE_ID = bicsuiteObject.ID;
        tab.DATA.USAGE = tabInfo.TYPE;
        tab.DATA.f_IsInstance = false;
        if (bicsuiteObject.TYPE == 'JOB') {
            tab.DATA.f_isScopeOrFolderInstance = false;
        }
        else {
            tab.DATA.f_isScopeOrFolderInstance = true;
        }
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openUser(name : string): Promise<any> {
        let tab = new Tab(name,"", "USER", "serveruser", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openWatchType(name : string): Promise<any> {
        let tab = new Tab(name,"", "WATCH TYPE", "watchtype", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openGroup(name : string): Promise<any> {
        let tab = new Tab(name,"", "GROUP", "group", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openGroupOwner(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openGroup(bicsuiteObject.OWNER);
    }

    openSmeInHierarchy(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        return this.openSmeInHierarchyById(bicsuiteObject.ID);
    }

    openSmeInHierarchyById(smeId: string): Promise<Object> {
        let monitorJobTab = new Tab('Monitor Jobs', 'Hierarchy_' + smeId, 'MONITOR JOBS', 'monitorjobs', TabMode.MONITOR_JOBS)
        monitorJobTab.BOOKMARK = new Bookmark(smeId,'SEARCH');
        monitorJobTab.BOOKMARK.scope = "USER";
        monitorJobTab.BOOKMARK.value = {
            filterObject : {
                JOB_ID : smeId,
                NAME_PATTERN : { TABLE : [] },
                FILTER_ESD : { TABLE : [] },
                FILTER_JOB_STATES : { TABLE : [] },
                MASTER_ID : "",
                HISTORY_TO : "",
                HISTORY_FROM : "",
                HISTORY_FROM_UNIT : "DAYS",
                HISTORY_FUTURE : "",
                CONDITION: ""
            },
            settingsObject : {
                ENABLE_BULK : 'true',
                AUTO_REFRESH: '0'
            }
        };
        // console.log(monitorJobTab.BOOKMARK);
        this.eventEmitterService.createTab(monitorJobTab, true);
        return Promise.resolve({});
    }

    getChangeConfig() : any {
        return {};
    }

    updateObjectRecord (message: any, tabInfo: Tab, field: any, object: any) : boolean {
        let didUpdate : boolean = false;
        if (message.OP == "RENAME") {
            let fieldName = field.NAME;
            if (!object.hasOwnProperty(fieldName)) {
                return false;
            }
            let oldName: string = message.OLD_NAME;
            let newName: string = message.NEW_NAME;
            if (object[fieldName] == oldName) {
                object[fieldName] = newName;
                didUpdate = true;
            }
            else {
                if (object[fieldName] && object[fieldName].startsWith(oldName + '.')) {
                    object[fieldName] = object[fieldName].replace(oldName + '.', newName + '.');
                    didUpdate = true;
                }
            }
        }
        if (message.OP == "TYPECHANGE") {
            let name = message.NAME;
            let type = message.NEWTYPE;
            let nameFieldName = field.NAME;
            let typeFieldName = field.TYPENAME;
            if (!object.hasOwnProperty(nameFieldName) || !object.hasOwnProperty(typeFieldName))  {
                return false;
            }
            if (object[nameFieldName] == name) {
                object[typeFieldName] = type;
                didUpdate = true;
            }
        }
        return didUpdate;
    }

    updateObjectTable (message: any, tabInfo: Tab, field: any, object: any, tableName: string) : boolean  {
        let didUpdate : boolean = false;
        if (!object[tableName]) {
            return false;
        }
        if (object[tableName].TABLE) {
            if (message.OP == "DROP") {
                let newTable : any[] = [];
                for (let o of object[tableName].TABLE) {
                    if (o[field.NAME] == message.NAME || o[field.NAME].startsWith(message.NAME + '.')) {
                        didUpdate = true;
                        continue;
                    }
                    newTable.push(o);
                }
                if (didUpdate) {
                    object[tableName].TABLE = newTable;
                }
            }
            else {
                for (let o of object[tableName].TABLE) {
                    if (o[field.NAME].trim().endsWith(']')) {
                        // do not update job names for defined resource instances
                        continue;
                    }
                    if (this.updateObjectRecord (message, tabInfo, field, o)) {
                        didUpdate = true;
                    }
                }
            }
        }
        else {
            // console.log("updateObjectTable: no object[" + tableName + "].TABLE")
            // console.log(tabInfo);
            // console.log(object);
            // console.log(message);
        }
        return didUpdate;
    }

    processDataChange(message: any, tabInfo: Tab, field: any, bicsuiteObject: any, initialBicsuiteObject: any) {
        let tableName = field.TABLE
        let fieldName = field.NAME
        let didUpdate: boolean = false;
        if (tableName) {
            if (this.updateObjectTable (message, tabInfo, field, bicsuiteObject.DATA.RECORD, tableName)) {
                didUpdate = true;
            }
            if (initialBicsuiteObject && initialBicsuiteObject.DATA && initialBicsuiteObject.DATA.RECORD) {
                this.updateObjectTable (message, tabInfo, field, initialBicsuiteObject.DATA.RECORD, tableName);
            }
        }
        else {
            if (this.updateObjectRecord (message, tabInfo, field, bicsuiteObject.DATA.RECORD)) {
                didUpdate = true;
            }
            if (initialBicsuiteObject && initialBicsuiteObject.DATA && initialBicsuiteObject.DATA.RECORD) {
                this.updateObjectRecord (message, tabInfo, field, initialBicsuiteObject.DATA.RECORD);
            }
        }
        return didUpdate;
    }

    private processFields(fields: any, message: any, tabInfo: Tab, bicsuiteObject: any, initialBicsuiteObject: any, didUpdate: boolean ) {
        if (fields && bicsuiteObject?.DATA?.RECORD) {
            for (let field of fields) {
                if (this.processDataChange(message, tabInfo, field, bicsuiteObject, initialBicsuiteObject)) {
                    didUpdate = true;
                }
            }
            if (didUpdate) {
                this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
            }
        }
    }

    processNotifyDataChange(message: any, tabInfo: Tab, bicsuiteObject: any, initialBicsuiteObject: any) {
        if (message.OP == "DROP") {
            if (tabInfo.PATH) {
                if ((tabInfo.PATH + '.' + tabInfo.NAME).startsWith(message.NAME + '.') ||
                    (tabInfo.PATH + '.' + tabInfo.NAME == message.NAME && message.FROM_CONTENT)) {
                    this.eventEmitterService.removeTabEmitter.next(tabInfo);
                    return;
                }
            }
            if (tabInfo.PATH + '.' + tabInfo.NAME == message.NAME) {
                // we handle DROP in the tab causing the DROP
                return;
            }
        }
        let config = this.getChangeConfig();
        let opConfig = config[message.TYPE]?.[message.OP];
        let didUpdate: boolean = false;
        if (opConfig) {
            if (opConfig.METHOD) {
                // dataChange Metgods have to return a boolean
                this.callMethod(opConfig.METHOD, bicsuiteObject, tabInfo, initialBicsuiteObject, message).then((result) => {
                    this.processFields(opConfig.FIELDS, message, tabInfo, bicsuiteObject, initialBicsuiteObject, result);
                }, this.emptyRejection);
            }
            else {
                this.processFields(opConfig.FIELDS, message, tabInfo, bicsuiteObject, initialBicsuiteObject, didUpdate);
            }
        }
    }

    setInheritGrants(record: any) {
      if (this.privilegesService.isEdition('BASIC')) {
            let inherit_grants = record.INHERIT_PRIVS;
            if (inherit_grants == '') {
                record.INHERIT_GRANTS = 'NONE'
            } else if (inherit_grants == 'V') {
                record.INHERIT_GRANTS = 'VIEW'
            } else {
                record.INHERIT_GRANTS = 'ALL'
            }
        }
    }

    getConfirmInfoFields(operation: string, bicsuiteObject: any, initDataObj: any): any[] {
        let fields :any[] = [];
        let confirmInfoFields = this.customPropertiesService.getCustomObjectProperty ("confirmInfoFields");
        if (confirmInfoFields != undefined && confirmInfoFields != null) {
            for (let field of confirmInfoFields) {
                if (field["MODES"] != undefined && field["MODES"][operation] == 1) {
                    for (let parameter of bicsuiteObject.PARAMETER.TABLE) {
                        if (parameter.NAME == field.PARAMETER && parameter.VALUE) {
                            fields.push({
                                "NAME": "CONFIRM_INFO_FIELD_" + field.PARAMETER,
                                "CUSTOM_LABEL": field["LABEL"],
                                "TYPE": "TEXT"
                            });
                            initDataObj[ "CONFIRM_INFO_FIELD_" + field.PARAMETER] = parameter.VALUE;
                        }
                    }
                }
            }
        }
        return fields;
    }
    
    getConfirmReasonFields(operation: string): any[] {
        let fields = [];
        let confirmReasonFields = this.customPropertiesService.getCustomObjectProperty ("confirmReasonFields");
        if (confirmReasonFields != undefined && confirmReasonFields != null) {
            for (let field of confirmReasonFields) {
                if (field["MODES"] != undefined && field["MODES"][operation] == 1) {
                    let values = [];
                    let labels = [];
                    for (let value of field["VALUES"]) {
                        values.push(value["VALUE"]);
                        if (value["LABEL"] != undefined) {
                            labels.push(value["LABEL"]);
                        }
                        else {
                            labels.push(value["VALUE"]);
                        }
                    }
                    let defaultValue = values[0];
                    if (field["DEFAULT"] != undefined) {
                        defaultValue = field["DEFAULT"];
                    }
                    fields.push({
                        "NAME": field["NAME"],
                        "CUSTOM_LABEL": field["LABEL"],
                        "TYPE": "SELECT",
                        "LABELS": labels,
                        "VALUES": values,
                        "DEFAULT": defaultValue
                    });
                }
            }
        }
        return fields;
    }

    fieldsToComment(result: any, fields: any[]): string {
        let comment = result.COMMENT;
        if (comment == undefined) {
            comment = '';
        }
        for (let field of fields) {
            if (comment != '') {
                comment += ',\n';
            }
            comment += field["NAME"] + " : " + result[field["NAME"]];
        }
        return comment;
    }
    // --------------------------------- shared crud-conditions -------------------------

    updateFullname(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        
        bicsuiteObject.FULLNAME = this.toolbox.namePathQuote(bicsuiteObject.PATH + '.' + 
            this.convertIdentifier(bicsuiteObject.NAME, bicsuiteObject.ORIGINAL_NAME));
        return false;
    }

    conditionTableHasMoreThenOneRows(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        if(table) {
            if(table.length > 1) {
                return true
            } else {
                return false
            }
        }
        return true;
    }
    conditionAuditEnabled(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE']);
    }

    conditionInUserGroups(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let group = bicsuiteObject[editorformField['NAME']];
        if (this.privilegesService.getGroups().includes(group)) {
            return true;
        }
        if (this.privilegesService.isAdmin()) {
            return true;
        }
        return false;
    }

    conditionIsBasic(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.privilegesService.isEdition('BASIC');
    }

    conditionHasSchedules(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        // console.log(tabInfo.TYPE)
        // WIP DIETER EXPORT INCLUDE SCHEDULES
        if (['JOB DEFINITION', 'FOLDER', 'BATCH', 'JOB'].includes(tabInfo.TYPE)) {
            return true;
        }
        return false;
    }

    conditionHaveName(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (tabInfo.NAME) {
            return true;
        }
        return false;
    }

    conditionTypeIsHierarchicalOrNameEmpty(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (['SCOPE', 'FOLDER', 'CATEGORY'].includes(tabInfo.TYPE) || tabInfo.NAME == '') {
            return true;
        }
        return false;
    }

    conditionFileSelected(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.FILE) {
            return true;
        }
        return false;
    }

    conditionIsOwnerOrAdmin(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (this.privilegesService.getGroups().includes(bicsuiteObject.OWNER) || this.privilegesService.isAdmin()) {
            return true;
        }
        return false;
    }
    
    conditionIsAdmin(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return this.privilegesService.isAdmin();
    }

    conditionIsNew(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        if (tabInfo.MODE == TabMode.NEW) {
            return true;
        } else {
            return false;
        }
    }

    conditionNever(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return false
    }

    conditionNameChanged(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        if (bicsuiteObject.NAME != bicsuiteObject.ORIGINAL_NAME) {
            return true;
        }
        else {
            return false;
        }
    }

}
