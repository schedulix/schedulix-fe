import { MatLegacyDialog as MatDialog } from "@angular/material/legacy-dialog"
import { Router } from "@angular/router"
import { BookmarkService } from "../modules/main/services/bookmark.service"
import { MainInformationService } from "../modules/main/services/main-information.service"
import { PrivilegesService } from "../modules/main/services/privileges.service"
import { SubmitEntityService } from "../modules/main/services/submit-entity.service"
import { TreeFunctionsService } from "../modules/main/services/tree-functions.service"
import { CommandApiService } from "../services/command-api.service"
import { CustomPropertiesService } from "../services/custom-properties.service"
import { EventEmitterService } from "../services/event-emitter.service"
import { ToolboxService } from "../services/toolbox.service"


export class sharedCrud {
    constructor(protected commandApi: CommandApiService,
        protected bookmarkService: BookmarkService,
        protected router: Router,
        protected toolbox: ToolboxService,
        protected eventEmitterService: EventEmitterService,
        protected privilegesService: PrivilegesService,
        protected dialog: MatDialog,
        protected treeFunctionService: TreeFunctionsService,
        protected customPropertiesService: CustomPropertiesService,
        protected submitEntityService: SubmitEntityService,
        protected mainInformationService: MainInformationService) { }

    // REQUEST = context.REQUEST
    exportModes = [
        {
            TYPE: 'ENVIRONMENT',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'EXIT STATE DEFINITION',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'EXIT STATE MAPPING',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'EXIT STATE PROFILE',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'EXIT STATE TRANSLATION',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'FOLDER',
            NAME: 'DEFAULT',
            EXPAND: 'FOLDER = (CONTENT), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'FOOTPRINT',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'GROUP',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'INTERVAL',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'SERVER',
            NAME: 'DEFAULT',
            EXPAND: 'SCOPE = (RESOURCE, POOL), POOL = (DISTRIBUTION), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'NAMED RESOURCE',
            NAME: 'DEFAULT',
            EXPAND: 'NAMED_RESOURCE = (TRIGGER), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'NAMED RESOURCE CATEGORY',
            NAME: 'DEFAULT',
            EXPAND: 'NAMED RESOURCE = (CHILDREN), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'OBJECT MONITOR',
            NAME: 'DEFAULT',
            EXPAND: 'OBJECT_MONITOR = (TRIGGER,WATCH_TYPE), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'POOL',
            NAME: 'DEFAULT',
            EXPAND: 'POOL = (DISTRIBUTION), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'RESOURCE',
            NAME: 'DEFAULT',
            EXPAND: 'RESOURCE = (TRIGGER), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'RESOURCE STATE DEFINITION',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'RESOURCE STATE MAPPING',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'RESOURCE STATE PROFILE',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'SCHEDULING ENTITY',
            NAME: 'DEFAULT',
            EXPAND: 'JOB_DEFINITION = (TRIGGER), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'SCOPE SYSTEM',
            NAME: 'DEFAULT',
            EXPAND: 'SCOPE = (CHILDREN, RESOURCE, POOL), POOL = (DISTRIBUTION), ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'USER',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'WATCH TYPE',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        },
        {
            TYPE: 'none',
            NAME: 'DEFAULT',
            EXPAND: 'ALL = (COMMENT)',
            CLEANUP: 'false',
            MODE: 'BACKUP',
            MAP: 'none',
            HEADER: 'none',
            IGNORE_READ_ERROR: 'false'
        }
    ];

    getExportModes(type: string) {
        let modes: any[] = []
        let custom = Object.assign(this.exportModes);
        let defaults = undefined;
        try {
            defaults = Object.assign(this.customPropertiesService.getCustomObjectProperty ('export_modes'));
        }
        catch (e) {
            console.warn("Exception reaing config.export_modes:");
            console.warn(e);
        }
        modes = modes.concat(defaults);
        modes = modes.concat(custom);
        // console.log(modes)
        // console.log(type)
        let exportModes: any[] = [];
        for (let m of modes) {
            if (m.TYPE.includes(type)) {
                exportModes.push(m);
            }
        }
        if (exportModes.length == 0) {
            for (let m of modes) {
                if (m.TYPE == 'none') {
                    exportModes.push(m);
                }
            }
        }
        return exportModes;
    }

    createDump(type: string, name: string, langLevel: string, mode: string, grant: boolean, include_schedules: boolean) {
        let dump_type = type
        let export_type = dump_type
        let mode_type = type
        let conf = undefined;
        if (mode_type == 'SERVER') dump_type = 'SCOPE';
        if (mode_type == 'NAMED RESOURCE' || mode_type == 'CATEGORY') dump_type = 'NAMED RESOURCE';
        if (['BATCH', 'JOB', 'MILESTONE'].includes(mode_type)) {
            dump_type = 'JOB DEFINITION'
            mode_type = 'SCHEDULING ENTITY'
        }
        let modes = this.getExportModes(type);
        for (let m of modes) {
            if (m.NAME == mode && (m.TYPE.includes(type) || m.TYPE == 'none')) {
                conf = m;
            }
        }
        if (!conf) {
            return "";
        }
        let stmt = '';
        let dump_name = '';
        if (name != '') {
            if (['SCOPE', 'NAMED RESOURCE', 'JOB DEFINITION', 'FOLDER', 'RESOURCE'].includes(dump_type)) {
                dump_name = name
            } else {
                dump_name = '\'' + name + '\''
            }
            stmt = 'dump ' + dump_type + ' ' + this.toolbox.namePathQuote(dump_name)
        }
        else {
            if (export_type != 'ALL') {
                export_type = export_type + ' ALL';
            }
            //         export_type = export_type + ' ALL'
            stmt = 'DUMP ' + export_type
        }
        if (['RESOURCE', 'POOL'].includes(type)) {
            stmt = stmt + ' IN ' + name;
        }
        stmt = stmt + "\nWITH\nMULTICOMMAND,\nLANGUAGE LEVEL = '" + langLevel + "'";

        let all_expand = '';
        let sep = '';
        if (grant) {
            all_expand = 'ALL(*) = (GRANT)'
            sep = ',\n'
        }
        if (include_schedules) {
            //     #
            //     # using the compound expand TIME_SCHEDULES will also dump embedded intervals
            //     # ie. calendars which is typically not what we want because if we dump a schedule of a job
            //     # and deploy it to this or another system later we might overwrite calendards !!!
            //     #
            //     #    all_expand = all_expand + sep + 'JOB_DEFINITION(*) = (TIME_SCHEDULES)'
            //     #
            //     # So we explicitly define the expansion here to not cointain the embedded intervals
            //     #
            all_expand = all_expand + sep + 'JOB_DEFINITION(*) = (EVENT), EVENT(*) = (SCHEDULED_EVENT), SCHEDULED_EVENT(*) = (SCHEDULE), SCHEDULE(*) = (INTERVAL,PARENTS), INTERVAL(*) = (CHILDREN)'
        }

        if (conf.EXPAND && ![undefined, ''].includes(conf.EXPAND)) {
            if (all_expand != '') {
                stmt = stmt + ',\nEXPAND = (\n' + conf['EXPAND'] + ',\n' + all_expand + '\n)'
            } else {
                stmt = stmt + ',\nEXPAND = (\n' + conf['EXPAND'] + '\n)'
            }
        }
        if (type == 'FOLDER' && conf.CLEANUP == 'true') {
            stmt = stmt + ',\nCLEANUP'
        }

        if (conf.MODE != 'none' && conf.MODE) {
            stmt = stmt + ',\nMODE = ' + conf['MODE']
        }
        if (conf.MAP != 'none' && conf.MAP) {
            // console.log(conf.MAP)
            stmt = stmt + ',\nMAP = (\n' + conf['MAP'] + '\n)'
        }

        if (conf.IGNORE_READ_ERROR == 'true') {
            stmt = stmt + ',\nIGNORE READ ERROR'
        }
        let header = ''
        if (conf.HEADER != 'none' && conf.HEADER) {
            header = conf['HEADER'] + '\n'
        }

        // check if name has to be mapped
        // TODO WIP DIETER REWORK
        console.warn("TODO DIETER MAPPING REVIEW")
        let mapname = name
        if (type == 'FOLDER' && (conf.MAP != 'none' && conf.MAP)) {
            let map = conf['MAP']
            // map = map.replace(',', ' ')
            // # get folder mappings
            let fmaps = []
            let ml: any[] = map.split(',')
            let i = 0
            while (i < ml.length) {
                if (ml[i].toUpperCase() == 'FOLDER') {
                    let frm = ml[i + 1]
                    let pl1: any[] = frm.split('.')
                    let pl2: any[] = []
                    for (let n of pl1) {
                        if (n[0] != '\'') {
                            pl2.push(n.toUpperCase())
                        } else {
                            pl2.push(n)
                        }
                    }
                    frm = pl2.join('.')
                    frm = frm.replace('\'', '')
                    let to = ml[i + 3]
                    pl1 = to.split('.')
                    pl2 = []
                    for (let n of pl1) {
                        if (n[0] != '\'') {
                            pl2.push(n.toUpperCase())
                        } else {
                            pl2.push(n)
                        }
                    }
                    to = pl2.join('.')
                    to = to.replace('\'', '')
                    fmaps.push([frm.length, frm, to])
                    i = i + 4
                } else {
                    i = i + 1
                }
            }

            fmaps.sort()
            fmaps.reverse()
            // now check if my folder had to be mapped
            for (let m of fmaps) {
                mapname = name.replace(m[1], m[2])
                if (mapname != name) {
                    break
                }
            }
        }
        let expType = type;
        let expName = mapname;
        if (name == '') {
            expType = export_type;
            expName = "";
        }
        header = header + "/* Do not edit the content of this Comment !!! \n file may not be able to be imported using Web-interface otherwise\n" +
            " EXPORT_TYPE = " + expType + "\n EXPORT_NAME = " + expName;

        if (['RESOURCE', 'POOL'].includes(type)) {
            header = header + '\nEXPORT_SCOPE=' + name;
        }
        //     
        header = header + '\n*/'
        header = header.replace('\\', '\\\\')
        header = header.replace('\'', '\\\'')
        stmt = stmt + ',\n HEADER = \'' + header + '\''

        // console.log(stmt)
        return stmt
    }
}
