export interface treeNode {
    id: number;
    name: string;
    namePath: string;
    hasChildren: boolean;
    isPinned: boolean;
    isPinnedParent: boolean
    isInvisible: boolean;
    isClickable: boolean;

    // is dialogproperty
    isProperty: boolean;
    isPropertyParent: boolean;


    iconType: string;
    type: string;

    children?: treeNode[];
    parentNode?: treeNode;
    isNotSubmitable?: boolean;
    hasMasterSubmittableChildren?: boolean;
    bicsuiteObject: any;
    textColor: string;
    isDragTarget?: boolean;
  }


  export interface masterTreeNode {
    id: number;
    name: string;
    namePath: string;
    hasChildren: boolean;
    isPinned: boolean;
    isPinnedParent: boolean
    isInvisible: boolean;

    index: number;


    iconType: string;
    type: string;

    children?: masterTreeNode[];
    parentNode?: masterTreeNode;
    level?: number[];
    childrenStates?: string[];

    // state logic

    // batches haben nur batch apspects, jobs ohne jinder nur den job aspect, jobs mit kindern habern beide
    parentIconColor: string;  // icon des batch aspects
    jobIconColor: string;     // icon des job aspects
    textColor: string;
    displayState: string;   // angezeigter technischer state


    final_ts: string;
    submit_ts: string;
    isSuspended: string;
    parentSuspended: string;

    state: string;
    jobState: string;
    end: string;
    start: string;
    runtime: number;
    exitstate: string;
    hit: string;
    parity: string;

    // button properties to check
    is_restartable: string;
    is_disabled: string;
    // is_replaced: string;
    is_suspended: string;
    is_cancelled: string;
    cnt_restartable: string;

    workdir: string,
    logUrl: string;
    normalizedPrivs: string;
  }


  export interface column {
    parameter_name: string;
    color: string;
    label: string;
    align: string;
    translate: boolean;

  }
