import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import int_editorform from '../json/editorforms/int.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";

export abstract class TsCommonCrud extends Crud {

    DOWselections = ['1', '2', '3', '4', '5', '6', '7'];
    DOMselections = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
        '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
        '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31',
        '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1'
    ];
    WOMselections = ['1', '2', '3', '4', '5', '-1', '-2', '-3', '-4', '-5'];
    WOMmap = {
        '1': '1,2,3,4,5,6,7',
        '2': '8,9,10,11,12,13,14',
        '3': '15,16,17,18,19,20,21',
        '4': '22,23,24,25,26,27,28',
        '5': '29,30,31',
        '-1': '-1,-2,-3,-4,-5,-6,-7',
        '-2': '-8,-9,-10,-11,-12,-13,-14',
        '-3': '-15,-16,-17,-18,-19,-20,-21',
        '-4': '-22,-23,-24,-25,-26,-27,-28',
        '-5': '-29,-30,-31'
    };
    IWYselections = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
        '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
        '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
        '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
        '41', '42', '43', '44', '45', '46', '47', '48', '49', '50',
        '51', '52', '53',
        '-6', '-5', '-4', '-3', '-2', '-1'
    ];
    MOYselections = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'];
    CLXselections = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
        '-1', '-2', '-3', '-4', '-5', '-6', '-7', '-8', '-9', '-10'
    ];

    allPossibleSelections = [
        '1', '2', '3', '4', '5', '6', '7', '8', '9', '10',
        '11', '12', '13', '14', '15', '16', '17', '18', '19', '20',
        '21', '22', '23', '24', '25', '26', '27', '28', '29', '30',
        '31', '32', '33', '34', '35', '36', '37', '38', '39', '40',
        '41', '42', '43', '44', '45', '46', '47', '48', '49', '50',
        '51', '52', '53',
        '-10', '-9', '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1'
    ]

    setSelectionsFromCheckboxes(interval: any) {
        let selections: any[] = [];
        for (let s of this.allPossibleSelections) {
            if (interval[("CHECKBOX" + s).replace("-", "_")] == "true") {
                if (!selections.includes(s)) {
                    selections.push(s);
                }
            }
        }
        interval.SELECTION = selections.join(',');
    }

    mergeSelections(interval: any, selections: string[]) {
        let selection = "";
        switch (interval.TYPE) {
            case "TOD":
                selection = "'T" + interval.HOUR + ":" + interval.MINUTE + "'";
                if (!selections.includes(selection)) {
                    selections.push(selection);
                }
                break;
            case "ROD":
                selection = "'T" + interval.HOUR_FROM + ":" + interval.MINUTE_FROM + "'-" +
                    "'T" + interval.HOUR_TO + ":" + interval.MINUTE_TO + "'";
                if (!selections.includes(selection)) {
                    selections.push(selection);
                }
                break;
        }
    }

    mergeAndSortIntervals(subSchedule: any, requireDriver: boolean) {
        let typeOrder = ["TOD", "REP", "ROD", "DOW", "DOM", "WOM", "IWM", "IWY", "MOY", "LST", "CLD", "CLF"];
        let mergedIntervals: any[] = [];
        let drivers = 0;
        for (let type of typeOrder) {
            let selections: string[] = [];
            let found = false;
            let merged = false;
            let interval: any;
            let lastInterval: any = null;
            for (interval of subSchedule.INTERVALS.TABLE) {
                if (interval.TYPE == type) {
                    found = true;
                    if (['TOD', 'ROD'].includes(type)) {
                        merged = true;
                        this.mergeSelections(interval, selections);
                        lastInterval = interval;
                    }
                    else {
                        if (type != 'REP') {
                            this.setSelectionsFromCheckboxes(interval);
                        }
                        mergedIntervals.push(interval);
                    }
                }
            }
            if (merged) {
                interval = lastInterval;
                interval.SELECTION = selections.join(",");
                mergedIntervals.push(interval);
            }
            if (found && ['TOD', 'REP', 'CLD'].includes(type)) {
                drivers++;
                if (drivers > 1) {
                    this.commandApi.throwSdmsException('ZSI-02311071230', 'Time Of Day, Repeat or Calendar(Driver) must not be mixed with other Drivers (Time Of Day, Repeat or Calendar(Driver))', 'mergeAndSortIntervals', true);
                }
            }
        }
        if (mergedIntervals.length != 0 && requireDriver && drivers == 0) {
            this.commandApi.throwSdmsException('ZSI-02207040630', 'A Driver (Time Of Day, Repeat or Calendar (Driver)) is required!', 'mergeAndSortIntervals', true);
        }
        if (mergedIntervals.length == 0) {
            mergedIntervals.push({ "TYPE": "FIX" });
        }
        return mergedIntervals;
    }

    genDispatcherSelect(subSchedule: any) {
        let stmt = "            ( 'SELECT' with\n";
        let starttime = "none";
        if (subSchedule.START.trim() != "") {
            starttime = "'" + subSchedule.START.trim() + "'";
            starttime = starttime.replace(" ", "T");
        }
        stmt += "                starttime = " + starttime + ",\n";
        let endtime = "none"
        if (subSchedule.END.trim() != "") {
            endtime = "'" + subSchedule.END.trim() + "'";
            endtime = endtime.replace(" ", "T");
        }
        stmt += "                endtime = " + endtime + ",\n";
        let filter = "none"
        if (subSchedule.INTERVAL_NAME && subSchedule.INTERVAL_NAME != "") {
            filter = "( '" + subSchedule.INTERVAL_NAME + "' )";
        }
        stmt += "                filter = " + filter + "\n";
        stmt += "            )\n";
        return stmt;
    }

    genDispatcherInterval(subSchedule: any, interval: any, indent: string) {
        let stmt = "";
        switch (interval.TYPE) {
            case "FIX":
                let starttime = "none";
                if (subSchedule.START.trim() != "") {
                    starttime = "'" + subSchedule.START.trim() + "'";
                }
                stmt += indent + "starttime = " + starttime + ",\n";
                let endtime = "none";
                if (subSchedule.END.trim() != "") {
                    endtime = "'" + subSchedule.END.trim() + "'";
                }
                stmt += indent + "endtime = " + endtime;
                break;
            case "TOD":
                stmt += indent + "selection = (" + interval.SELECTION + "),\n";
                stmt += indent + "base = NONE,\n";
                stmt += indent + "duration = NONE";
                break;
            case "ROD":
                stmt += indent + "selection = (" + interval.SELECTION + ")";
                break;
            case "REP":
                let badRepeat = false;
                try {
                    let repeatMinutes = parseInt(interval.REPEAT_MINUTES);
                    if (repeatMinutes != parseFloat(interval.REPEAT_MINUTES)) {
                        badRepeat = true;
                    }
                } catch (e) {
                    badRepeat = true;
                }
                if (badRepeat) {
                    this.commandApi.throwSdmsException('ZSI-02227040630', 'Repeat Minutes has to be an integer > 0!', 'genDispatcherInterval', true);
                }
                stmt += indent + "base = " + interval.REPEAT_MINUTES + " minutes,\n";
                stmt += indent + "duration = " + interval.REPEAT_MINUTES + " minutes,\n";
                // TODO: synctime checking and default today 00:00
                if (subSchedule.START.trim() != "") {
                    stmt += indent + "synctime = '" + subSchedule.START.trim() + "'";
                }
                else {
                    stmt += indent + "synctime = '1970-01-01T00:00'";
                }
                break;
            case "DOW":
                stmt += indent + "base = 1 week,\n";
                stmt += indent + "duration = 1 day,\n";
                stmt += indent + "selection = (" + interval.SELECTION + ")";
                break;
            case "DOM":
                stmt += indent + "base = 1 month,\n";
                stmt += indent + "duration = 1 day,\n";
                stmt += indent + "selection = (" + interval.SELECTION + ")";
                break;
            case "WOM":
                stmt += indent + "base = 1 month,\n";
                stmt += indent + "duration = 1 day,\n";
                let selectedList: string[] = interval.SELECTION.split(',');
                let selectionList: string[] = [];
                let key: string;
                interface IIndexable {
                    [key: string]: any;
                };
                for (key of selectedList) {
                    selectionList.push((this.WOMmap as IIndexable)[key]);
                }
                stmt += indent + "selection = (" + selectionList.join(',') + ")";
                break;
            case "IWM":
                stmt += indent + "base = 1 month,\n";
                stmt += indent + "duration = 1 week,\n";
                stmt += indent + "selection = (" + interval.SELECTION + ")";
                break;
            case "IWY":
                stmt += indent + "base = 1 year,\n";
                stmt += indent + "duration = 1 week,\n";
                stmt += indent + "selection = (" + interval.SELECTION + ")";
                break;
            case "MOY":
                stmt += indent + "base = 1 year,\n";
                stmt += indent + "duration = 1 month,\n";
                stmt += indent + "selection = (" + interval.SELECTION + ")";
                break;
            case "LST":
                stmt += indent + "base = none,\n";
                stmt += indent + "duration = none,\n";
                let selections = interval.LIST.split(',');
                let list = [];
                for (let selection of selections) {
                    let l = selection.split('to');
                    if (l.length < 2) {
                        list.push("'" + l[0].trim().replace(" ", "T") + "'");
                    }
                    else {
                        list.push("'" + l[0].trim().replace(" ", "T") + "' - '" + l[1].trim().replace(" ", "T") + "'");
                    }
                }
                stmt += indent + "selection = (" + list.join(",") + ")";
                break;
            case "CLD":
            case "CLF":
                stmt += indent + "embedded = '" + interval.CALENDAR + "',\n";
                stmt += indent + "base = 1 " + interval.SELECT_ON + ",\n";
                interval.SELECTION ? stmt += indent + "selection = (" + interval.SELECTION + ")" : stmt += indent + "selection = NONE";
                break;
        }
        if (!["TOD", "ROD", "REP"].includes(interval.TYPE)) {
            if (interval.SELECTION_MODE == "INVERSE") {
                stmt += ",\n" + indent + "inverse";
            }
        }
        return stmt;
    }

    genDispatcherIntervalChain(subSchedule: any, mergedIntervals: any[], index: number, indent: string) {
        let interval = mergedIntervals[index];
        let stmt = "";
        stmt += indent + "( '" + interval.TYPE + "' with\n";
        stmt += this.genDispatcherInterval(subSchedule, interval, indent + "    ");
        index++;
        if (index < mergedIntervals.length) {
            stmt += ",\n" + indent + "    filter = (\n";
            stmt += this.genDispatcherIntervalChain(subSchedule, mergedIntervals, index, indent + "        ");
            stmt += "\n" + indent + "    )";
        }
        stmt += "\n" + indent + ")"
        return stmt;
    }

    genDispatcher(subSchedule: any, requireDriver: boolean) {
        let stmt = "";
        stmt += "        '" + subSchedule.NAME + "' ";
        if (subSchedule.ACTIVE == "true") {
            stmt += "active\n";
        }
        else {
            stmt += "inactive\n";
        }
        let indent = "            ";
        stmt += this.genDispatcherSelect(subSchedule);
        stmt += this.genDispatcherIntervalChain(subSchedule, this.mergeAndSortIntervals(subSchedule, requireDriver), 0, indent);
        stmt += "\n";

        // dispatchers created from time schedules should be always enabled !!!
        // when saving (converting) an version 1.0 schedule as version 2.9 schedule we do not habe ENABLED set and would create it as disabled
        // which is wrong !!!
        // This why we have to check for !subSchedule.ENABLED to enable
        // We cannot just alwys set to enable because genDispatcher is also called for intervals where we have to take ENABLED into respect !!!
        if (!subSchedule.ENABLED || subSchedule.ENABLED == "true") {
            stmt += indent + "enable";
        }
        else {
            stmt += indent + "disable";
        }

        return stmt;
    }

    setupCheckboxValues(interval: any, list: string[]) {
        let setup = interval.SETUP;
        let l = setup.split(',');
        for (let e of list) {
            let et = e.trim();
            let set = false;
            for (let v of l) {
                if (v.trim() == et) {
                    set = true;
                }
            }
            let fieldName = 'CHECKBOX' + e.replace('-', '_');
            if (set) {
                interval[fieldName] = 'true';
            }
            else {
                interval[fieldName] = 'false';
            }
        }
    }

    setupFieldValues(interval: any) {
        interval.HOUR = "00";
        interval.MINUTE = "00";
        interval.HOUR_FROM = "00";
        interval.MINUTE_FROM = "00";
        interval.HOUR_TO = "00";
        interval.MINUTE_TO = "00";
        interval.REPEAT_MINUTES = "60";
        interval.SELECT_ON = "MONTH";
        let l, l1, l2;
        switch (interval.TYPE) {
            case 'TOD':
                l = interval.SETUP.trim().split(':');
                interval.HOUR = l[0].substring(1);
                interval.MINUTE = l[1];
                break;
            case 'ROD':
                l = interval.SETUP.trim().split('-');
                l1 = l[0].trim().split(':');
                if (l.length == 2) {
                    l2 = l[1].trim().split(':');
                }
                else {
                    l2 = l1;
                }
                interval.HOUR_FROM = l1[0].substring(1);
                interval.MINUTE_FROM = l1[1];
                interval.HOUR_TO = l2[0].substring(1);
                interval.MINUTE_TO = l2[1];
                break;
            case 'DOW':
                this.setupCheckboxValues(interval, this.DOWselections);
                break;
            case 'DOM':
                this.setupCheckboxValues(interval, this.DOMselections);
                break;
            case 'WOM':
            case 'IWM':
                this.setupCheckboxValues(interval, this.WOMselections);
                break;
            case 'IWY':
                this.setupCheckboxValues(interval, this.IWYselections);
                break;
            case 'MOY':
                this.setupCheckboxValues(interval, this.MOYselections);
                break;
            case 'LST':
                let dl = interval.SETUP.split(',');
                let tmpl: string[] = [];
                for (let dle of dl) {
                    tmpl.push(dle.trim());
                }
                dl = tmpl;
                let ndl: string[] = [];
                for (let line of dl) {
                    let ll = line.split(" - ")
                    if (ll.length == 1) {
                        ndl.push(ll[0]);
                    }
                    else {
                        if (ll[0].substring(ll[0].length - 3) == ':00' && ll[1].substring(ll[1].length - 3) == ':59') {
                            ll[0] = ll[0].substring(0, ll[0].length - 3);
                            ll[1] = ll[1].substring(0, ll[1].length - 3);
                            if (ll[0].substring(ll[0].length - 3) == 'T00' && ll[1].substring(ll[1].length - 3) == 'T23') {
                                ll[0] = ll[0].substring(0, ll[0].length - 3);
                                ll[1] = ll[1].substring(0, ll[1].length - 3);
                                if (ll[0] == ll[1]) {
                                    ndl.push(ll[0]);
                                    continue
                                }
                            }
                        }
                        ndl.push(ll[0] + ' to ' + ll[1]);
                    }
                }
                interval.LIST = ndl.join(",\n");
                break;
            case 'CLD':
            case 'CLF':
                this.setupCheckboxValues(interval, this.CLXselections);
                break;
            case 'REP':
                interval.REPEAT_MINUTES = interval.SETUP;
                break;
        }
    }

    filterSelectableIntervals(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        let result = [];
        for (let row of bicsuiteObject) {
            if (row.SE_ID && parseInt(row.SE_ID) > 0 || row.NAME == tabInfo.NAME) {
                continue;
            }
            result.push(row);
        }
        return result;
    }

    conditionInvertable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !["TOD", "REP", "ROD"].includes(bicsuiteObject.TYPE);
    }

    conditionCanDropDispatcher(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        return table.length > 1;
    }
}
