import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import esd_editorform from '../json/editorforms/esd.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";


export class MonitorJobsCrud extends Crud {
    create(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    read(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    update(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    drop(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    editorform() {
        return esd_editorform;
    }

    ConditionBookmarkIsEditable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        // console.log(tabInfo.BOOKMARK);
        if(tabInfo.BOOKMARK == undefined) return false;
        return  this.bookmarkService.isSelectedBookmarkEditable(tabInfo.BOOKMARK);
    }
}
