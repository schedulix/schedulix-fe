import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import esp_editorform from '../json/editorforms/esp.json';
import { Tab } from "../data-structures/tab";
import { Crud } from "../interfaces/crud";
import { PromiseType } from "protractor/built/plugins";


export class ExitStateProfileCrud extends Crud {
    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create exit state profile '" + obj.NAME + "'";

        if (obj.DEFAULT_ESM_NAME !== '' && obj.DEFAULT_ESM_NAME !== undefined) {
            stmt += " with default mapping='" + obj.DEFAULT_ESM_NAME + "',";
        }
        else {
            stmt += "with";
        }

        let statesLength = obj.STATES.TABLE.length;
        for (let i = 0; i < statesLength; i++) {
            let item = obj.STATES.TABLE[i];

            if (i === 0) {
                stmt += " states=(";
            }
            stmt += "'" + item.ESD_NAME + "' "
            stmt += item.TYPE.toLowerCase() + " "

            if (item.IS_BROKEN === "true") {
                stmt += "broken ";
            }
            if (item.TYPE === "FINAL") {
                if (item.IS_UNREACHABLE === "true") {
                    stmt += "unreachable ";
                }
                if (item.IS_DISABLED === "true") {
                    stmt += "disable ";
                }
                if (item.IS_BATCH_DEFAULT === "true") {
                    stmt += "batch default ";
                }
                if (item.IS_DEPENDENCY_DEFAULT === "true") {
                    stmt += "dependency default"
                }
            }

            if (i === statesLength - 1) {
                stmt += ");";
            }
            else {
                stmt += ","
            }
        }

        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESP", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show exit state profile '" + tabInfo.NAME + "'";

        return this.commandApi.execute(stmt).then((response: any) => {

            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;

            if (promise.response.DATA.RECORD.DEFAULT_ESM_NAME === "<null>") {
                promise.response.DATA.RECORD.DEFAULT_ESM_NAME = "";
            }

            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("exit state profile");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("exit state profile");
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename " + "exit state profile" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';";
        }
        stmt += "alter exit state profile '" + obj.NAME + "'";
        if (obj.DEFAULT_ESM_NAME !== '' && obj.DEFAULT_ESM_NAME !== undefined) {
            stmt += " with default mapping='" + obj.DEFAULT_ESM_NAME + "',";
        }
        else {
            stmt += "with";
        }
        let statesLength = obj.STATES.TABLE.length;
        for (let i = 0; i < statesLength; i++) {
            let item = obj.STATES.TABLE[i];
            if (i === 0) {
                stmt += " states=(";
            }
            stmt += "'" + item.ESD_NAME + "' "
            stmt += item.TYPE.toLowerCase() + " "

            if (item.IS_BROKEN === "true") {
                stmt += "broken ";
            }
            if (item.TYPE === "FINAL") {
                if (item.IS_UNREACHABLE === "true") {
                    stmt += "unreachable ";
                }
                if (item.IS_DISABLED === "true") {
                    stmt += "disable ";
                }
                if (item.IS_BATCH_DEFAULT === "true") {
                    stmt += "batch default ";
                }
                if (item.IS_DEPENDENCY_DEFAULT === "true") {
                    stmt += "dependency default "
                }
            }

            if (i === statesLength - 1) {
                stmt += ");";
            }
            else {
                stmt += ","
            }
        }
        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESP", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Exit State Profile}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop exit state profile '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESP", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    openDefaultEsmName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateMapping(bicsuiteObject.DEFAULT_ESM_NAME);
    }

    openEsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.ESD_NAME);
    }

    editorform() {
        return esp_editorform;
    }

    default(tabInfo: Tab) {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["STATES"] = {
            TABLE: [{
                ESD_NAME: "",
                IS_DEPENDENCY_DEFAULT: "false",
                PREFERENCE: "false",
                IS_BROKEN: "false",
                IS_DISABLED: "false",
                IS_BATCH_DEFAULT: "false",
                TYPE: "FINAL",
                IS_UNREACHABLE: "false",
                NOFORM_DISABLE_AUTOCHOOSE: "true"
            }],
            DESC: [
                "ESD_NAME",
                "IS_DEPENDENCY_DEFAULT",
                "PREFERENCE",
                "IS_BROKEN",
                "IS_DISABLED",
                "IS_BATCH_DEFAULT",
                "TYPE",
                "IS_UNREACHABLE",
                "NOFORM_DISABLE_AUTOCHOOSE"]
        };
        return bicsuiteObj;
    }

    conditionTypeIsFinal(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        return bicsuiteObject.TYPE == 'FINAL';
    }

    conditionTypeIsRestartable(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        return bicsuiteObject.TYPE == 'RESTARTABLE';
    }

    getChangeConfig() {
        return {
            ESD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "STATES",
                            NAME: "ESD_NAME"
                        }
                    ]
                }
            },
            ESM: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "DEFAULT_ESM_NAME"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}
