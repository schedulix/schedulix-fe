import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import cat_editorform from '../json/editorforms/cat.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { type } from "os";
import { EditorformOptions } from "../data-structures/editorform-options";
import { ToolboxService } from "../services/toolbox.service";
import { NewItemDialogComponent } from "../modules/main/components/dialog-components/new-item-dialog/new-item-dialog.component";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";
import { SdmsClipboardMode, SdmsClipboardType } from "../modules/main/services/clipboard.service";
import { SnackbarType } from "../modules/global-shared/components/snackbar/snackbar-data";


export class CategoryCrud extends Crud {

    new(obj: any): Promise<Object> {
        let itemTypes = ['CATEGORY', 'STATIC', 'SYSTEM', 'SYNCHRONIZING'];
        if (this.privilegesService.validateEdition(['ENTERPRISE'])) {
            itemTypes.push('POOL');
        }
        const dialogRef = this.dialog.open(NewItemDialogComponent, {
            data: {
                title: "new_item",
                translate: "create_new_item_chooser",
                itemTypes: itemTypes,
                path: obj.PATH ? obj.PATH + '.' + obj.NAME : obj.NAME
            },
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
        });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result !== undefined && result) {
                let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
                tab.PATH = result.path;
                // console.log(result)
                this.eventEmitterService.createTab(tab, true);
            }
        });
        return Promise.resolve({});
    }

    createWith(mode: string, obj: any, tabInfo?: Tab) {
        let isAdmin: boolean = this.privilegesService.isAdmin();
        let withClause: string = "";
        let sep = "    ";
        if (mode != "UPDATE") {
            withClause += sep + "usage = CATEGORY";
            sep = ",\n    ";
        }
        if (obj.OWNER != obj.ORIGINAL_OWNER) {
            withClause += sep + "group = '" + obj.OWNER + "'";
            if (obj.CHOWN_CASCADED == "true") {
                withClause += " cascade";
            }
        }
        if (withClause != "") {
            withClause = " with\n" + withClause;
        }
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME =  this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create named resource " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + obj.NAME) + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "CATEGORY", OP: "CREATE", NAME: tabInfo.PATH + "." + obj.NAME });
            obj.F_REFRESH_NAVIGATOR = true;
            return response;
        });
    }

    clone(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "copy named resource "
            + obj.QUOTED_FULLNAME
            + " to "
            + "'" + obj.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            obj.QUOTED_FULLNAME = tabInfo.PATH ? this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME) : obj.NAME;
            obj.ORIGINAL_NAME = obj.NAME;
            let withClause = this.createWith("UPDATE", obj, tabInfo);
            if (withClause != "") {
                let r = this.update(obj, tabInfo);
                obj.ORIGINAL_OWNER = obj.OWNER;
                return r;
            }
            this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "CATEGORY", OP: "CREATE", NAME: tabInfo.PATH + "." + obj.NAME });
            return response;
        });
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)
        // console.log(bicsuiteObject)

        let grantObject = {
            TYPE: bicsuiteObject.USAGE,
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME,
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS: "",
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;
                if (grantObject.GRANTS.TABLE[0]) {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    // grantObject.GRANTS.TABLE[0].OWNER = 'Inherit grant form parent';

                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        // console.log(i)
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            // console.log("merge inheritance pirvs to the root object")
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }

                // console.log(grantObject);

            }

            let editorForm = crud.editorformByType(grantObject.TYPE, true, true)
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }

            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for " + bicsuiteObject.TYPE + " " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                width: "1000px",
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog']
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }

    showContent(categoryName: string): Promise<Object> {
        let stmt: string = "list named resource " + categoryName;
        return this.commandApi.execute(stmt).then((response: any) => {
            response.DATA.TABLE.shift() // remove first item (itself)
            response.DATA.TABLE = response.DATA.TABLE.sort(function (a: any, b: any) {
                if (a.USAGE == "CATEGORY" && b.USAGE != "CATEGORY") {
                    return -1;
                }
                if (a.USAGE != "CATEGORY" && b.USAGE == "CATEGORY") {
                    return 1;
                }
                if (a.NAME.toUpperCase() < b.NAME.toUpperCase()) {
                    return -1;
                }
                if (a.NAME.toUpperCase() > b.NAME.toUpperCase()) {
                    return 1;
                }
                return 0;
            })
            let cutIds = this.clipboardService.getCutIds();
            for (let row of response.DATA.TABLE) {
                if (cutIds.includes(row.ID)) {
                    row.fe_flagged_to_cut = true;
                }
            }
            return response.DATA;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show named resource " + this.toolbox.namePathQuote(tabInfo.PATH + "." + tabInfo.NAME);
        return this.commandApi.execute(stmt).then((response: any) => {
            let record = response.DATA.RECORD;
            tabInfo.ID = record.ID;
            let promise = new BicsuiteResponse();
            record.QUOTED_FULLNAME = this.getQuotedFullName(tabInfo);
            record.FULLNAME = this.toolbox.namePathQuote(record.NAME);
            let pathlist = record.NAME.split(".");
            record.NAME = pathlist.pop();
            record.PATH = pathlist.join(".");
            record.TYPE = "NAMED RESOURCE";
            record.ORIGINAL_OWNER = record.OWNER;

            promise.response = response;
            let stmt_content: string = "list named resource " + record.QUOTED_FULLNAME;
            return this.commandApi.execute(stmt_content).then((response_content: any) => {
                return this.showContent(record.QUOTED_FULLNAME).then(content => {
                    record.NOFORM_CONTENT = content; // from showContent

                    // map inherit grants for basic
                    this.setInheritGrants(record);
                    // if (this.privilegesService.isEdition('BASIC')) {
                    //     let inherit_grants = record.INHERIT_PRIVS;
                    //     if (inherit_grants == '') {
                    //         record.INHERIT_GRANTS = 'NONE'
                    //     } else if (inherit_grants == 'V') {
                    //         record.INHERIT_GRANTS = 'VIEW'
                    //     } else {
                    //         record.INHERIT_GRANTS = 'ALL'
                    //     }
                    // }
                    return promise;
                });
            });
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts: string[] = [];

        if (obj.NAME != obj.ORIGINAL_NAME) {
            stmts.push("rename named resource " + obj.QUOTED_FULLNAME + " to '" + obj.NAME + "'");
        }
        let withClause = this.createWith("UPDATE", obj, tabInfo);
        if (withClause != "") {
            stmts.push("alter named Resource " + obj.QUOTED_FULLNAME + " " + this.createWith("UPDATE", obj, tabInfo));
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            if (obj.ORIGINAL_NAME != obj.NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "CATEGORY", OP: "RENAME", OLD_NAME: tabInfo.PATH + "." + tabInfo.NAME, NEW_NAME: tabInfo.PATH + "." + obj.NAME });
                tabInfo.NAME = obj.NAME;
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {

        const dialog = this.createConfirmDialog([
            {
                "NAME": "DROP_CASCADED",
                "TRANSLATE": "cascade",
                "USE_LABEL": true,
                "TYPE": "CHECKBOX",
                "TICKVALUE": "true",
                "DEFAULT": "false"
            }
        ], "Drop Category " + tabInfo.NAME, tabInfo, "", undefined);

        // * Note, dialogs unsubsrcibe autoamtically!
        return dialog.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop named resource " + obj.QUOTED_FULLNAME;
                if (result.DROP_CASCADED == "true") {
                    stmt += " cascade"
                }
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "CATEGORY", OP: "DROP", NAME: tabInfo.PATH + "." + tabInfo.NAME });
                    return response;
                });
            }
            return {};
        });
    }

    openContentListItem(bicsuiteObject: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.NAME, bicsuiteObject.USAGE.toLowerCase());
    };

    editorform() {
        return cat_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.FULLNAME = this.toolbox.namePathQuote(tabInfo.PATH + '.');
        record.NAME = '';
        record.ORIGINAL_NAME = '';
        record.PATH = tabInfo.PATH;
        record.OWNER = this.privilegesService.getDefaultGroup();
        return bicsuiteObject;
    }

    conditionOwnerChanged(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !this.conditionIsNew(editorformField, bicsuiteObject, tabInfo) && bicsuiteObject.OWNER != bicsuiteObject.ORIGINAL_OWNER;
    }

    copyContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.FROM_ITEM_USAGE);
        for(let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = false;
        }
       return Promise.resolve({});
    }

    cutContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.FROM_ITEM_USAGE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = true;
        }
        return Promise.resolve({});
    }


    categoryPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.SYNCHRONIZING, SdmsClipboardType.SYSTEM, SdmsClipboardType.STATIC, SdmsClipboardType.CATEGORY, SdmsClipboardType.POOL];

    pasteContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        let itemsToBePasted: any[] = this.clipboardService.getItems(this.categoryPasteTypes);
        let sourceObject: any = {};
        let sourceTabInfo: Tab;
        let stmts: string[] = [];
        let sourcePathes : any[] = [];

        for (let item of itemsToBePasted) {
            sourceObject = item.sourceObject;
            sourceTabInfo = item.sourceTabInfo;
            if (item.mode == SdmsClipboardMode.COPY) {
                stmts.push("copy named resource " + this.toolbox.namePathQuote(item.data.NAME) + " to " + this.toolbox.namePathQuote(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) + ';');
            }
            else {
                let p = item.data.NAME.split('.');
                p.pop();
                let sourcePath = p.join('.');
                if (sourcePath != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) {
                    stmts.push("move named resource " + this.toolbox.namePathQuote(item.data.NAME) + " to " + this.toolbox.namePathQuote(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) + ';');
                    if (!sourcePathes.includes(sourcePath)) {
                        sourcePathes.push(sourcePath);
                    }
                }
                else {
                    if (item.mode == SdmsClipboardMode.CUT) {
                        if (!sourcePathes.includes(sourcePath)) {
                            sourcePathes.push(sourcePath);
                        }
                        item.mode = SdmsClipboardMode.COPY;
                        item.data.fe_flagged_to_cut = false;
                    }
                    // No deep copy necessary here because we re read content later anyway, so no cross editing can occure
                }
                if (sourcePath != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME || tabInfo.FROM_CONTEXT_MENU) {
                    // put target path in sourcepathes to get target refreshed if paste is done from tree context menu
                    if (!sourcePathes.includes(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME)) {
                        sourcePathes.push(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME);
                    }
                }
            }
        }
        if (stmts.length > 0) {
            return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                for (let item of itemsToBePasted) {
                    item.mode = SdmsClipboardMode.COPY;
                }
                for (let sourcePath of sourcePathes) {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "CATEGORY", OP : "CREATE", NAME : sourcePath + ".DUMMY"});
                }
                return this.showContent(bicsuiteObject.QUOTED_FULLNAME).then(content => {
                    bicsuiteObject.NOFORM_CONTENT = content;
                    if (itemsToBePasted.length > 1) {
                        this.eventEmitterService.pushSnackBarMessage([itemsToBePasted.length + ' items have been inserted'], SnackbarType.INFO);
                    } else {
                        this.eventEmitterService.pushSnackBarMessage(['Item has been inserted'], SnackbarType.INFO);
                    }
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    this.eventEmitterService.refreshNavigationEmittor.next();
                    return {};

                });
            });
        }
        return Promise.resolve({});
    }

    dropContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let fields: any[] = [];
        for (let selected of checklistSelection._selected) {
            if (selected.USAGE == 'CATEGORY') {
                fields.push({
                    'NAME': 'CASCADE',
                    'TRANSLATE': 'cascade',
                    'TYPE': 'CHECKBOX',
                    "SYS_OBJ_EDITABLE": true,
                    'USE_LABEL': true
                });
                break;
            }
        }

        return this.createConfirmDialog(fields, "drop_confirm_selected", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmts: string[] = [];
                for (let selected of checklistSelection._selected) {
                    let stmt = "drop named resource " + this.toolbox.namePathQuote(selected.NAME);
                    if (selected.USAGE == 'CATEGORY' && result.CASCADE && result.CASCADE == "true") {
                        stmt += ' cascade';
                    }
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                        return this.showContent(bicsuiteObject.QUOTED_FULLNAME).then(content => {
                        bicsuiteObject.NOFORM_CONTENT = content;
                        for (let selected of checklistSelection._selected) {
                            if (selected.TYPE == 'CATEGORY') {
                                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "CATEGORY", OP: "DROP", NAME: selected.NAME, FROM_CONTENT: true });
                            }
                            else {
                                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "NAMED RESOURE", OP: "DROP", NAME: selected.NAME, FROM_CONTENT: true });
                            }
                        }
                        this.eventEmitterService.refreshNavigationEmittor.next();
                        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    });
                });
            }
            return {};
        });
    }

    conditionClipBoardOfTypeNotEmpty(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // check if there is item to be pasted and if the source is not the same
        return this.clipboardService.checkItemTypesPresent(this.categoryPasteTypes, bicsuiteObject.ID);
    }

    processContentOrPathChange(bicsuiteObject: any, tabInfo: Tab, initialBicsuiteObject: any, message: any): Promise<boolean> {
        let record = bicsuiteObject?.DATA?.RECORD;
        let affected: boolean = false;

        if (!record.hasOwnProperty("PATH")) {
            return Promise.resolve(false);
        }
        if (message.OP == "RENAME") {
            let oldName = message.OLD_NAME;
            let newName = message.NEW_NAME;
            if (tabInfo.PATH == oldName) {
                tabInfo.PATH = newName;
                tabInfo.updateTooltip();
            }
            else {
                if (tabInfo.PATH.startsWith(oldName + '.')) {
                    tabInfo.PATH = tabInfo.PATH.replace(oldName + '.', newName + '.');
                    tabInfo.updateTooltip();
                }
            }
            // Handle Path Change
            if (oldName && newName) {
                if (record.PATH == oldName) {
                    record.PATH = newName;
                }
                else {
                    if (record.PATH.startsWith(oldName + '.')) {
                        record.PATH = record.PATH.replace(oldName + '.', newName + '.');
                    }
                }
            }
        }
        if (message.OP == "DROP" || message.OP == "CREATE") {
            if (!message.FROM_CONTENT) {
                let path = message.NAME.split(".");
                path.pop()
                if ((tabInfo.PATH + '.' + tabInfo.NAME) == path.join('.')) {
                    affected = true;
                }
            }
        }
        // reload content if content changed
        if (affected && record.QUOTED_FULLNAME) {
            this.showContent(record.QUOTED_FULLNAME).then(content => {
                record.NOFORM_CONTENT = content;
            });
        }
        return Promise.resolve(false);
    }

    getChangeConfig() {
        return {
            CATEGORY: {
                RENAME: {
                    METHOD: "processContentOrPathChange",
                    FIELDS: [
                    ]
                },
                CREATE: {
                    METHOD: "processContentOrPathChange"
                },
                DROP: {
                    METHOD: "processContentOrPathChange"
                }
            },
            GROUP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "OWNER"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
