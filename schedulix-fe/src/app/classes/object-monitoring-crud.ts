import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import objectMonitoring_editorform from '../json/editorforms/objectMonitoring.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import { EditorformOptions } from "../data-structures/editorform-options";
import { setMaxListeners } from "process";

export class ObjectMonitoringCrud extends Crud {

    createWith(mode: string, obj: any, tabInfo: Tab) {
        let withClause: string = " with ";
        let sep: string = "";

        if (obj.PARAMETERS.TABLE.length > 0) {
            withClause += " parameters = ";
            withClause += "(";
            for (let parameter of obj.PARAMETERS.TABLE) {
                withClause += sep + " '" + parameter.NAME + "' = ";
                if (parameter.IS_DEFAULT == "true") {
                    withClause += "DEFAULT";
                }
                else {
                    withClause += "'" + parameter.VALUE + "'";
                }
                sep = ", ";
            }
            withClause += ")";
        }
        let delete_clause = "";
        if (isNaN(parseInt(obj.DELETE_AMOUNT))) {
            delete_clause = "delete NONE";
        }
        else {
            delete_clause = "delete after " + obj.DELETE_AMOUNT + " " + obj.DELETE_BASE;
        }
        withClause += sep + delete_clause;
        let event_delete_clause = "";
        if (isNaN(parseInt(obj.EVENT_DELETE_AMOUNT))) {
            event_delete_clause = "event delete NONE";
        }
        else {
            event_delete_clause = "event delete after " + obj.EVENT_DELETE_AMOUNT + " " + obj.EVENT_DELETE_BASE;
        }
        withClause += ", " + event_delete_clause;
        withClause += ", recreate = " + (obj.RECREATE == "DO NOTHING" ? "NONE" : obj.RECREATE);
        withClause += ", group = '" + obj.OWNER + "'";
        if (obj.WATCHER) {
            withClause += ", watcher = " + this.toolbox.namePathQuote(obj.WATCHER);
        }
        else {
            withClause += ", watcher = NONE";
        }
        return withClause;
    }

    createTriggerWith(trigger: any) : string {
        let triggerWith = " with ";
        let submitName = this.toolbox.namePathQuote(trigger.SUBMIT_NAME);
        triggerWith += "submit " + submitName;
        if (trigger.IS_GROUP == 'true') {

            if (trigger.MAIN_NAME) {
                let mainName = this.toolbox.namePathQuote(trigger.MAIN_NAME);
                triggerWith += ", main " + mainName;
            }
            else {
                triggerWith += ", main none"
            }
            if (trigger.PARENT_NAME) {
                let parentName = this.toolbox.namePathQuote(trigger.PARENT_NAME);
                triggerWith += ", parent " + parentName;
            }
            else {
                triggerWith += ", parent none"
            }
        }
        triggerWith += ", " + (trigger.ACTIVE == "true" ? "active" : "inactive");
        if (trigger.MASTER == "true") {
            triggerWith += ", master, group = '" + trigger.SUBMIT_OWNER + "'";
        }
        else {
            triggerWith += ", nomaster";
        }
        triggerWith += ", " + (trigger.SUSPEND == "true" ? "suspend" : "nosuspend");
        let events: string[] = [];
        if (trigger.IS_CREATE == "true") {
            events.push("create");
        }
        if (trigger.IS_CHANGE == "true") {
            events.push("change");
        }
        if (trigger.IS_DELETE == "true") {
            events.push("delete");
        }
        triggerWith += ", events = (" + events.join(", ") + ")";
        triggerWith += ", " + (trigger.IS_GROUP == "true" ? "group event" : "single events");

        return triggerWith;
    }

    generateTriggerStatements(bicsuiteObject: any) : string [] {
        let stmts : string [] = [];
        for (let trigger of bicsuiteObject.TRIGGERS.TABLE) {
            if (bicsuiteObject.TRIGGERS_READ[trigger.ID]) {
                // prexisting trigger
                if (bicsuiteObject.TRIGGERS_READ[trigger.ID] != trigger.NAME) {
                    trigger.NAME = this.convertIdentifier(trigger.NAME, bicsuiteObject.TRIGGERS_READ[trigger.ID]);
                    stmts.push("rename trigger '" + bicsuiteObject.TRIGGERS_READ[trigger.ID] + "' on object monitor '" + bicsuiteObject.NAME + "' to '" + trigger.NAME + "'");
                }
                stmts.push("alter trigger '" + trigger.NAME + "' on object monitor '" + bicsuiteObject.NAME + "'" + this.createTriggerWith(trigger));
            }
            else {
                // new trigger
                trigger.NAME = this.convertIdentifier(trigger.NAME, "");
                stmts.push("create trigger '" + trigger.NAME + "' on object monitor '" + bicsuiteObject.NAME + "'" + this.createTriggerWith(trigger));
            }
            delete bicsuiteObject.TRIGGERS_READ[trigger.ID];
        }
        // left over entries in TRIGGERS_READ are to drop
        let k: keyof typeof bicsuiteObject.TRIGGERS_READ;
        for (k in bicsuiteObject.TRIGGERS_READ) {
            let triggerName =  bicsuiteObject.TRIGGERS_READ[k];
            stmts.push("drop trigger '" + triggerName + "' for object monitor '" + bicsuiteObject.NAME + "'");
        }
        return stmts;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        let stmts : string[] = [];
        stmts.push("create object monitor " + "'" + obj.NAME + "' watch type '" +  obj.WATCH_TYPE + "' " + this.createWith("CREATE", obj, tabInfo));
        stmts = stmts.concat(this.generateTriggerStatements(obj));
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "OM", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }


    addObjectInstanceColumn (table: any[], name: string) {
        for (let f of table) {
            if (f.NAME == name) {
                return;
            }
        }
        table.push({
                "NAME": name,
                "TYPE": "TEXT",
                "USE_LABEL": false,
                "LABEL": name,
                "CSS_CLASS": "editorform_nowrap"
        });
    }

    resolveState(prefix: string, object: any): string {
        if (!object.SME_ID) {
            return ''
        }
        let state = object[prefix + 'JOBSTATE'];
        if (['FINAL', 'CANCELLED'].includes(state)) {
            return state;
        }
        let fail_states = ['UNREACHABLE', 'RESTARTABLE', 'ERROR', 'BROKEN_ACTIVE', 'BROKEN_FINISHED'];
        let run_states = ['SUBMITTED', 'RESOURCE_WAIT', 'RUNNABLE', 'STARTING', 'STARTED', 'RUNNING', 'TO_KILL', 'KILLED'];
        if (object[prefix + 'JOB_IS_RESTARTABLE'] == 'true') {
            return 'BROKEN'
        }
        for (let s of fail_states) {
            if (Number(object[prefix + 'CNT_' + s]) > 0) {
                return 'BROKEN';
            }
        }
        if (run_states.includes(object[prefix + 'JOBSTATE'])) {
            return 'ACTIVE'
        }
        for (let s of run_states) {
            if (Number(object[prefix + 'CNT_' + s]) > 0) {
                return 'ACTIVE';
            }
        }
        return 'IDLE'
    }

    readWatchTypeParameters (record : any) : Promise<Object> {
        return this.commandApi.execute("show watch type '" + record.WATCH_TYPE + "'").then((response: any) => {
            record.WATCH_TYPE_PARAMETERS = { TABLE: response.DATA.RECORD.PARAMETERS.TABLE };
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    createConfigForWatchType (bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        return this.readWatchTypeParameters(bicsuiteObject).then((response: any) => {
            let configTable: any[] = [];
            for (let parameter of bicsuiteObject.WATCH_TYPE_PARAMETERS.TABLE) {
                if (parameter.TYPE == 'CONFIG') {
                    parameter.VALUE = parameter.DEFAULT_VALUE;
                    if (parameter.DEFAULT_VALUE) {
                        parameter.IS_DEFAULT = "true";
                    }
                    else {
                        parameter.IS_DEFAULT = "false";
                    }
                    configTable.push(parameter);
                }
            }
            bicsuiteObject.PARAMETERS.TABLE = configTable;
            return Promise.resolve({});
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show object monitor '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let record = response.DATA.RECORD;
            tabInfo.ID = record.ID;
            if (record.RECREATE == "NONE") {
                record.RECREATE = "DO NOTHING";
            }
            if (!record.DELETE_AMOUNT) {
                record.DELETE_AMOUNT = "FOREVER";
            }
            if (!record.DELETE_BASE) {
                record.DELETE_BASE = "DAY";
            }
            if (!record.EVENT_DELETE_AMOUNT) {
                record.EVENT_DELETE_AMOUNT = "FOREVER";
            }
            if (!record.EVENT_DELETE_BASE) {
                record.EVENT_DELETE_BASE = "DAY";
            }
            return this.readWatchTypeParameters(record).then((response_wt: any) => {
                for (let field of objectMonitoring_editorform.FIELDS) {
                    if (field.VALUE == "OBJECT_INSTANCES") {
                        for (let tField of field.FIELDS) {
                            let f: any = tField;
                            if (f.NAME == "OBJECT_INSTANCES") {
                                let tableFields = f.TABLE.FIELDS;
                                // display VALUES first
                                for (let p of record.WATCH_TYPE_PARAMETERS.TABLE) {
                                    if (p.TYPE == "VALUE") {
                                        this.addObjectInstanceColumn(tableFields, p.NAME);
                                    }
                                }
                                // display infos after values
                                for (let p of record.WATCH_TYPE_PARAMETERS.TABLE) {
                                    if (p.TYPE == "INFO") {
                                        this.addObjectInstanceColumn(tableFields, p.NAME);
                                    }
                                }
                            }
                        }
                    }
                }

                return this.commandApi.execute("list trigger for object monitor '" + record.NAME + "'").then((response_tr: any) => {
                    record.TRIGGERS = { TABLE: response_tr.DATA.TABLE };
                    interface IDictionary {
                        [index: string]: string;
                    }
                    let triggersRead = {} as IDictionary;
                    for (let trigger of record.TRIGGERS.TABLE) {
                        triggersRead[trigger.ID] = trigger.NAME;
                        if (!trigger.MAIN_NAME || trigger.MAIN_NAME == 'NONE') {
                            trigger.MAIN_NAME = "";
                        }
                    }
                    record.TRIGGERS_READ = triggersRead;
                    let defaultTimeZone = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
                    let defaultTimeFormat = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat");
                    let object_instances: any[] = [];
                    let parentId = '';
                    for (let instance of record.INSTANCES.TABLE) {
                        if (instance.CREATE_TS) {
                            instance.ICON = 'object_created';
                            instance.CREATE_TS = this.toolbox.convertTimeStamp(new Date(instance.CREATE_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                            if (instance.MODIFY_TS) {
                                instance.ICON = 'object_changed';
                                instance.MODIFY_TS = this.toolbox.convertTimeStamp(new Date(instance.MODIFY_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                            };
                            if (instance.REMOVE_TS) {
                                instance.ICON = 'object_removed';
                                instance.REMOVE_TS = this.toolbox.convertTimeStamp(new Date(instance.REMOVE_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                            };
                            instance.PATH = instance.ID;
                            parentId = instance.ID;
                        }
                        else {
                            instance.PATH = parentId + '.' + instance.ID;
                            instance.ICON = 'event_' + instance.EVENTTYPE.toLowerCase();
                            instance.DISPLAY_MAIN_JOBNAME = instance.MAIN_JOBNAME.split('.').pop();
                            instance.IN_MEM = true;
                            if (instance.SME_ID.startsWith('[')) {
                                instance.DISPLAY_MAIN_JOBNAME = '[' + instance.DISPLAY_MAIN_JOBNAME + ']';
                                instance.IN_MEM = false;
                            }
                            instance.DISPLAY_UNIQUE_NAME = this.toolbox.convertTimeStamp(new Date(instance.SUBMIT_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                            instance.CREATE_TS = instance.MAIN_JOBSTATE;
                            instance.MODIFY_TS = instance.MAIN_FINAL_EXIT_STATE;
                            instance.REMOVE_TS = this.toolbox.convertTimeStamp(new Date(instance.FINAL_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                        }
                        // instance.NAME = instance.UNIQUE_NAME;
                        instance.RESOLVED_STATE = this.resolveState('', instance);
                        instance.MAIN_RESOLVED_STATE = this.resolveState('MAIN_', instance);
                        object_instances.push(instance);
                    }

                    record.OBJECT_INSTANCES = { TABLE: object_instances };

                    let object_events: any[] = [];
                    let main_events: any = {};
                    parentId = '';
                    for (let instance of record.INSTANCES.TABLE) {
                        if (!instance.SME_ID) {
                            continue;
                        }
                        if (instance.SME_ID.startsWith('[')) {
                            instance.SME_ID = instance.SME_ID.substring(1, instance.SME_ID.length - 1);
                        }
                        instance.IN_MEM = true;
                        if (instance.MAIN_SME_ID) {
                            if (instance.MAIN_SME_ID.startsWith('[')) {
                                instance.IN_MEM = false;
                                instance.MAIN_SME_ID = instance.MAIN_SME_ID.substring(1, instance.MAIN_SME_ID.length - 1);
                            }
                        }
                        if (!instance.MAIN_SME_ID) {
                            instance.MAIN_SME_ID = instance.SME_ID;
                        }
                        if (instance.FINAL_TS) {
                            instance.FINAL_TS = this.toolbox.convertTimeStamp(new Date(instance.FINAL_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                        }
                        if (instance.MAIN_FINAL_TS) {
                            instance.MAIN_FINAL_TS = this.toolbox.convertTimeStamp(new Date(instance.FINAL_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                        }
                        instance.EVENT_JOBTYPE = instance.JOBTYPE;
                        instance.EVENT_FINAL_TS = instance.FINAL_TS;
                        instance.EVENT_FINAL_EXIT_STATE = instance.FINAL_EXIT_STATE;
                        instance.EVENT_JOBSTATE = instance.RESOLVED_STATE;
                        instance.EVENT_DISPLAY_JOBNAME = instance.JOBNAME.split('.').pop();
                        instance.EVENT_DISPLAY_FULL_JOBNAME = instance.JOBNAME;
                        instance.EVENT_DISPLAY_SME_ID = instance.SME_ID;
                        instance.EVENT_UNIQUE_NAME = instance.UNIQUE_NAME;
                        if (!instance.IN_MEM) {
                            instance.EVENT_DISPLAY_JOBNAME = '[' + instance.EVENT_DISPLAY_JOBNAME + ']';
                        }
                        instance.EVENT = this.toolbox.convertTimeStamp(new Date(instance.SUBMIT_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                        if (!main_events[instance.MAIN_SME_ID] && instance.MAIN_SME_ID != instance.SME_ID) {
                            let main_instance = this.toolbox.deepCopy(instance);
                            main_instance.EVENT_ICON = 'group_event';
                            main_instance.EVENT_DISPLAY_JOBNAME = instance.MAIN_JOBNAME.split('.').pop();
                            main_instance.EVENT_DISPLAY_FULL_JOBNAME = instance.MAIN_JOBNAME;
                            main_instance.EVENT_DISPLAY_SME_ID = instance.MAIN_SME_ID;
                            if (!main_instance.IN_MEM) {
                                main_instance.EVENT_DISPLAY_JOBNAME = '[' + main_instance.EVENT_DISPLAY_JOBNAME + ']';
                            }
                            main_instance.EVENT_JOBTYPE = instance.MAIN_JOBTYPE;
                            main_instance.EVENT = main_instance.EVENT;
                            main_instance.EVENT_PATH = main_instance.MAIN_SME_ID;
                            main_instance.EVENT_FINAL_TS = main_instance.MAIN_FINAL_TS;
                            main_instance.EVENT_FINAL_EXIT_STATE = instance.MAIN_FINAL_EXIT_STATE;
                            main_instance.EVENT_JOBSTATE = instance.MAIN_RESOLVED_STATE;
                            main_instance.EVENT_UNIQUE_NAME = '';
                            main_instance.EVENT_TRIGGERNAME = instance.TRIGGERNAME;
                            object_events.push(main_instance);
                            main_events[instance.MAIN_SME_ID] = true;
                        }
                        if (main_events[instance.MAIN_SME_ID]) {
                            instance.EVENT_TRIGGERNAME = "";
                        }
                        instance.EVENT_ICON = 'event_' + instance.EVENTTYPE.toLowerCase();
                        instance.EVENT_PATH = instance.MAIN_SME_ID + '.' + instance.SME_ID;
                        object_events.push(instance);
                    }
                    record.OBJECT_EVENTS = { TABLE: object_events.sort(
                        (obj1, obj2) => {
                            let p1 = obj1.EVENT_PATH.split('.');
                            let p2 = obj2.EVENT_PATH.split('.');
                            if (Number(p1[0]) < Number(p2[0])) {
                                return 1;
                            }
                            if (Number(p1[0]) > Number(p2[0])) {
                                return -1;
                            }
                            if (p1.length > p2.length) {
                                return 1;
                            }
                            if (p1.length < p2.length) {
                                return -1;
                            }
                            if (p1.length == 1) {
                                return 0;
                            }
                            if (Number(p1[1]) > Number(p2[1])) {
                                return 1;
                            }
                            if (Number(p1[1]) < Number(p2[1])) {
                                return -1;
                            }
                            return 0;
                        })
                    };
                    let promise = new BicsuiteResponse();
                    promise.response = response;
                    return promise;
                });
            });
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmts : string[] = [];
        // rename
        if (obj.NAME != obj.ORIGINAL_NAME) {
            obj.NAME = this.convertIdentifier( obj.NAME, obj.ORIGINAL_NAME);
            // call rename
            stmts.push("rename " + "object monitor" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "'");
        }
        stmts.push("alter object monitor " + "'" + obj.NAME + "' " + this.createWith("UPDATE", obj, tabInfo));
        stmts = stmts.concat(this.generateTriggerStatements(obj));
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "OM", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Object Monitor}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop object monitor '" + obj.ORIGINAL_NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "OM", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    manipulateEditorformOptions(editorformOptions: EditorformOptions, editorformField: any, bicsuiteObject: any, tabInfo: any): void {
        super.manipulateEditorformOptions(editorformOptions, editorformField, bicsuiteObject, tabInfo);
        // IDs < 1000 should not be editable (TODO ask for clarification)
        if (editorformField.hasOwnProperty('NAME')) {
            // inside table: bicsuiteobject == row
            if (editorformField.NAME == "DISPLAY_MAIN_JOBNAME") {
                if (bicsuiteObject.MAIN_RESOLVED_STATE == 'FINAL') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-green';
                }
                else if (bicsuiteObject.MAIN_RESOLVED_STATE == 'CANCELLED') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-brown';
                }
                else if (bicsuiteObject.MAIN_RESOLVED_STATE == 'BROKEN') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-red';
                }
                else if (bicsuiteObject.MAIN_RESOLVED_STATE == 'ACTIVE') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-blue';
                }
                else
                editorformOptions.flags.CSS_CLASS += ' crud-color-purple';
            }
            if (editorformField.NAME == "EVENT_DISPLAY_JOBNAME") {
                if (bicsuiteObject.EVENT_JOBSTATE == 'FINAL') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-green';
                }
                else if (bicsuiteObject.EVENT_JOBSTATE == 'CANCELLED') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-brown';
                }
                else if (bicsuiteObject.EVENT_JOBSTATE == 'BROKEN') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-red';
                }
                else if (bicsuiteObject.EVENT_JOBSTATE == 'ACTIVE') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-blue';
                }
                else
                editorformOptions.flags.CSS_CLASS += ' crud-color-purple';
            }
        }
    }

    openMonitorJobsForInstances(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.openSmeInHierarchyById(bicsuiteObject.MAIN_SME_ID);
    }

    openMonitorJobsForEvents(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.openSmeInHierarchyById(bicsuiteObject.EVENT_DISPLAY_SME_ID);
    }

    openObjectMonitorWatchType(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        return this.openWatchType(bicsuiteObject.WATCH_TYPE);
    }

    openWatcher(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.WATCHER, "job");
    }

    openSubmitName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.SUBMIT_NAME, bicsuiteObject.SUBMIT_TYPE.toLowerCase());
    }

    openMainName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.MAIN_NAME, bicsuiteObject.MAIN_TYPE.toLowerCase());
    }

    openParentName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.PARENT_NAME, bicsuiteObject.PARENT_TYPE.toLowerCase());
    }

    conditionInMem(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.IN_MEM == true) {
            return true;
        }
        else {
            return false;
        }
    }

    conditionHasDefault(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.IS_DEFAULT == "true") {
            bicsuiteObject.VALUE = bicsuiteObject.DEFAULT_VALUE;
        }
        return (bicsuiteObject.DEFAULT_VALUE ? true :  false);
    }

    conditionIsMaster(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.MASTER == "true") {
            return true;
        }
        return false;
    }

    conditionIsGrouped(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.IS_GROUP == "true" && bicsuiteObject.MASTER == "true") {
            return true;
        }
        return false;
    }

    conditionHasMain(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.IS_GROUP == "true" && bicsuiteObject.MAIN_NAME) {
            return true;
        }
        return false;
    }

    conditionHasConfigParameters(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.PARAMETERS.TABLE && bicsuiteObject.PARAMETERS.TABLE.length > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    default() {
        return {
            DATA: {
                RECORD: {
                    OWNER: this.privilegesService.getDefaultGroup(),
                    DELETE_BASE: "DAY",
                    EVENT_DELETE_BASE: "DAY",
                    RECREATE: "CREATE",
                    TRIGGERS_READ: {}
                }
            }
        };
    }

    editorform() {
        return objectMonitoring_editorform;
    }
}
