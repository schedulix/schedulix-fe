import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import est_editorform from '../json/editorforms/est.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { PromiseType } from "protractor/built/plugins";
import { ExitStateProfileCrud } from "./exit-state-profile-crud";

export class ExitStateTranslationCrud extends Crud {

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create exit state translation '" + obj.NAME + "'";
        let translation = obj.TRANSLATION;
        // console.log(obj.TRANSLATION)
        if (translation != undefined && translation != null) {
            stmt += " with translation = (";
            for (let i = 0; i < translation.TABLE.length; i++) {
                stmt += "'" + translation.TABLE[i].FROM_ESD_NAME + "' to '" + translation.TABLE[i].TO_ESD_NAME + "'";
                stmt += i == translation.TABLE.length - 1 ? "" : ",";
            }
            stmt += ")";
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "EST", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }


    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show exit state translation '" + tabInfo.NAME + "'";

        return this.commandApi.execute(stmt).then((response: any) => {

            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;
            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("exit state translation");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("exit state translation");
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        // rename
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename " + "exit state translation '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';";
        }
        let translation = obj.TRANSLATION;
        // let rangeLength = obj.MAPPINGS.TABLE.length;
        stmt += " alter exit state translation '" + obj.NAME + "'";
        if (translation != undefined && translation != null) {
            stmt += " with translation = (";
            for (let i = 0; i < translation.TABLE.length; i++) {
                stmt += "'" + translation.TABLE[i].FROM_ESD_NAME + "' to '" + translation.TABLE[i].TO_ESD_NAME + "'";
                stmt += i == translation.TABLE.length - 1 ? "" : ",";
            }
        }
        stmt += "); end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "EST", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Exit State Translation}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop exit state translation '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "EST", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return est_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["TRANSLATION"] = {
            TABLE: [{
                FROM_ESD: "",
                TO_ESD: ""
            }],
            DESC: [
                "FROM_ESD",
                "TO_ESD"
            ]
        };
        return bicsuiteObj;
    }

    estChooserAny(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        bicsuiteObject.unshift({
            ID: "-1",
            NAME: "ANY",
            PRIVS: "KDEVG",
            CSS_CLASS: "crud-list-any",
            ICON_DISABLED: true
        });

        return bicsuiteObject;
    }

    openFromEsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.FROM_ESD_NAME);
    }

    openToEsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.TO_ESD_NAME);
    }

    getChangeConfig() {
        return {
            ESD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "TRANSLATION",
                            NAME: "FROM_ESD_NAME"
                        },
                        {
                            TABLE: "TRANSLATION",
                            NAME: "TO_ESD_NAME"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
