import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import bookmarking_editorform from '../json/editorforms/bookmarking.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";

export class BookmarkingCrud extends Crud {

    // constructor(protected commandApi: CommandApiService, protected bookmarkService: BookmarkService) {
    //     super();
    // }

    create(obj: any): Promise<Object> {

        let data = this.bookmarkService.getAllBookmarks();
        return new Promise<Object>((resolve, reject) => {
            let promise = new BicsuiteResponse();
            promise.response = {
            }
            promise.editorform = bookmarking_editorform;
            // resolve()
            resolve(promise);
            return promise;
            // resolve(promise);
        });
    }

    read(obj: any): Promise<Object> {
        let data = this.bookmarkService.getAllBookmarks();
        // this.bookmarkService.
        // console.warn(data)

        return new Promise<Object>((resolve, reject) => {
            let promise = new BicsuiteResponse();
            data.sort(function (a: any, b: any) {
                if (a.type < b.type) {
                    return -1;
                }
                if (a.type > b.type) {
                    return 1;
                }
                if (a.bookmark.toUpperCase() < b.bookmark.toUpperCase()) {
                    return -1;
                }
                return 1
            });
            promise.response = {
                DATA: {
                    RECORD: {
                        PRIVS: 'E',
                        BOOKMARKS: {
                            TABLE: data,
                            DESC: [
                                "bookmark",
                                "scope",
                                "type",
                                "connection",
                                "autostart"
                            ]
                        },
                        NAME: 'Bookmarking',
                        ID: 'bookmarking'
                    }
                }
            }
            resolve(promise);
            // console.log(promise)
            return promise;
        });
    }

    update(obj: any): Promise<Object> {
        // console.log(obj.BOOKMARKS.TABLE)
        this.bookmarkService.setAllBookmarks(obj.BOOKMARKS.TABLE);
        this.bookmarkService.createDefaults();
        return new Promise<Object>((resolve, reject) => {
            let promise = {
                FEEDBACK: 'Bookmarks has been updated'
            };
            resolve(promise);
            return promise;
        });
    }

    drop(obj: any): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    // Soll eine neue Resource erstellen
    // Bekommt ein bicsuiteObject als parameter


    editorform() {
        return bookmarking_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        // on bookmarking default is E
        // bicsuiteObj.DATA.RECORD.PRIVS = 'E';
        bicsuiteObj.DATA.RECORD["PARAMETERS"] = {
            TABLE: [],
            DESC: [
                "bookmark",
                "scope",
                "type",
                "autostart"
            ]
        };
        return bicsuiteObj;
    }

    custom(params: any, bicsuiteObject: any) {

        // this.router.navigate();
        // ROUTE as it was written in the editorform
        if (params.hasOwnProperty('ROUTE')) {

        }
        // console.log(params);
        // console.log(bicsuiteObject);
    }

    conditionBookmarkIsMasterSearch(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        // return bicsuiteObject.TYPE == 'FINAL';
        return (bicsuiteObject.type == 'MASTER') || (bicsuiteObject.type == 'SEARCH')
    }

    conditionIsBookmarkisSystemAndAdmin(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        // return bicsuiteObject.TYPE == 'FINAL';
        // console.log(bicsuiteObject.scope)
        return ((bicsuiteObject.scope == 'SYSTEM') && this.conditionIsAdmin(editorformField, bicsuiteObject, tabInfo)) || bicsuiteObject.scope != 'SYSTEM';
    }

    conitionIsInputOwnOrAdminOrconditionIsText(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        // return bicsuiteObject.TYPE == 'FINAL';
        // console.log(bicsuiteObject.scope)
        return (((bicsuiteObject.scope == 'SYSTEM') && this.conditionIsAdmin(editorformField, bicsuiteObject, tabInfo)) || bicsuiteObject.scope != 'SYSTEM') || (editorformField.TYPE == 'TEXT');
    }

    conditionAdminOrUserBookmark(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        // return bicsuiteObject.TYPE == 'FINAL';
        // console.log(bicsuiteObject.scope)
        return ((bicsuiteObject.scope == 'USER') || this.conditionIsAdmin(editorformField, bicsuiteObject, tabInfo));
    }

    manipulateBookmarkFieldType(bicsuiteObject: any, tabInfo: Tab, editorForm: any, parentBicsuiteObject: any): Promise<Object> {
        // console.log(parentBicsuiteObject.NOFORM_TEXT)
        parentBicsuiteObject.NOFORM_TEXT == 'TEXT' ? editorForm.TYPE = 'INPUT' : editorForm.TYPE = 'TEXT';
        return Promise.resolve({});
    }

    toggleRename(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        bicsuiteObject.NOFORM_TEXT == 'TEXT' ? bicsuiteObject.NOFORM_TEXT = 'INPUT' : bicsuiteObject.NOFORM_TEXT = 'TEXT';
        parentBicsuiteObject.NOFORM_TEXT;
        return Promise.resolve(1);
    }


    openOrRouteToBookmark(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        if (bicsuiteObject.hasOwnProperty('type')) {
            switch (bicsuiteObject.type) {
                case "DETAIL":
                    console.warn("click handler for detail not implemented yet")
                    // console.log(bicsuiteObject)

                    // let detailTab: Tab = new Tab();
                    // this.eventEmitterService.createTab(detailTab)
                    break;
                case "FOLDER":
                    console.log(bicsuiteObject)
                    this.router.navigate(['/home/BatchesAndJobs'], { queryParams: { selectedBookmark: bicsuiteObject.bookmark, scope: bicsuiteObject.scope} })

                    break;
                case "MASTER":
                    // console.log(bicsuiteObject)
                    // console.warn("click handler for master not implemented yet")
                    let monMasterTab = new Tab('Monitor Master ' + bicsuiteObject.bookmark, 'Monitor Master ' + bicsuiteObject.bookmark, 'Monitor Master'.toLocaleUpperCase(), 'runmajobs', TabMode.MONITOR_MASTER)
                    monMasterTab.BOOKMARK = bicsuiteObject;
                    this.eventEmitterService.createTab(monMasterTab, true);
                    break;
                case "SUBMIT":
                    this.router.navigate(['/home/SubmitBatchesAndJobs'], { queryParams: { selectedBookmark: bicsuiteObject.bookmark, scope: bicsuiteObject.scope } })

                    break;
                case "SEARCH":
                    let monSearchTab = new Tab('Monitor Jobs ' + bicsuiteObject.bookmark, 'Monitor Jobs ' + bicsuiteObject.bookmark, 'Monitor Jobs'.toLocaleUpperCase(), 'monitorjobs', TabMode.MONITOR_JOBS)
                    monSearchTab.BOOKMARK = bicsuiteObject;
                    this.eventEmitterService.createTab(monSearchTab, true);
                    // console.warn("click handler for search not implemented yet")
                    break;
                case "CALENDAR":
                    // let calendarTab = new Tab('Monitor Jobs ' + bicsuiteObject.bookmark, 'Monitor Jobs ' + bicsuiteObject.bookmark, 'Monitor Jobs'.toLocaleUpperCase(), 'monitorjobs', TabMode.MONITOR_JOBS)
                    let calendarTab = new Tab('Calendar ' + bicsuiteObject.bookmark, 'Calendar '+ bicsuiteObject.bookmark, 'CALENDAR', 'calendar', TabMode.CALENDAR);
                    calendarTab.BOOKMARK = bicsuiteObject;

                    // this.eventEmitterService.createTab(CalendarTab, true);
                    this.eventEmitterService.createTab(calendarTab, true);
                    // console.warn("click handler for search not implemented yet")
                    break;
            }
        } else {
            return Promise.resolve(1)
        }
        return Promise.resolve(0)
    }
}