import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import job_editorform from '../json/editorforms/batchesandjobs/job.json';

import submitDialog_editorform from '../json/editorforms/submitDialog.json'
import { TriggerCrud } from "./trigger-crud";
import { Tab, TabMode } from "../data-structures/tab";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { OutputType, SnackbarType } from "../modules/global-shared/components/snackbar/snackbar-data";
import editJobTriggerStates_editorform from '../json/editorforms/editJobTriggerStates_editorForm.json';
import editJobDependencyStates_editorform from '../json/editorforms/editJobDependencyStates_editorForm.json';
import editJobParameterComment_editorform from '../json/editorforms/editJobParameterComment_editorForm.json';
import editJobTriggerCondition_editorform from '../json/editorforms/editJobTriggerCondition_editorForm.json';
import editJobTriggerParameter_editorform from '../json/editorforms/editJobTriggerParameters_editorForm.json';
import editJobRequiredResourceState_editorform from '../json/editorforms/editJobRequiredResourceState.json';
import { ResourceCrud } from "./resource-crud";
import { GrantCrud } from "./grant-crud";
import { UntypedFormGroup } from "@angular/forms";
import { getValidatorsByEdtitorform } from "./formgenerator/getValidatorsByEditorform";
import { BFCommonCrud } from "./bf-common-crud";
import { trigger } from "@angular/animations";
import { SdmsClipboardMode, SdmsClipboardType } from "../modules/main/services/clipboard.service";
import { TranslateService } from "../services/translate.service";
import { LocalStorageHelper } from "../services/local-storage-helper.service";
import { Bookmark } from "../data-structures/bookmark";

export class JobCrud extends BFCommonCrud {

    private triggerCrud: TriggerCrud = new TriggerCrud(this.commandApi, this.bookmarkService, this.router, this.toolbox, this.eventEmitterService, this.privilegesService, this.dialog, this.treeFunctionService, this.customPropertiesService, this.submitEntityService, this.mainInformationService, this.clipboardService);

    parseForWebConfig(parameter: any) {
        if (parameter.COMMENT != undefined &&
            parameter.COMMENT != null) {
            let match = parameter.COMMENT.match(/<web_config>(.*)<\/web_config>/si);
            if (match != null) {
                let jsonStr: string = "{" + match[1] + "}";
                jsonStr = jsonStr.replace(/'/g, '"');
                try {
                    parameter["WEBCONFIG"] = JSON.parse(jsonStr);
                } catch {
                    console.warn('Error parsing parameter comments <web_config> tag for parameter ' + parameter.NAME);
                }
            }
        }
    }

    checkParentIsConventionFolderId(obj: any): Promise<Object> {
        let parentPath = obj.PATH.split('.');
        parentPath.pop();
        if (parentPath.length == 0) {
            return Promise.resolve({});
        }
        parentPath = parentPath.join('.');
        let stmt = "show folder " + this.toolbox.namePathQuote(parentPath);
        return this.commandApi.execute(stmt).then((response: any) => {
            let id = response.DATA.RECORD.ID;
            return this.doCheckConventionFolder(obj, parentPath, id, "PARENT_IS_CONVENTION_FOLDER");
        });
    }

    reloadDefinedResources(bicsuiteObject: any, tabInfo: Tab) {
        let stmt = "show job definition " + tabInfo.ID;
        return this.commandApi.execute(stmt).then((response: any) => {
            let record = response.DATA.RECORD;
            bicsuiteObject.NOFORM_DEFINED_RESOURCES.TABLE = record["DEFINED_RESOURCES"].TABLE.filter(function (value: any, index: any, arr: any) {
                return value.USAGE != 'CATEGORY';
            });
            this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        // console.log(tabInfo)
        let stmt = '';

        // check if versioned readonly job
        if (tabInfo.VERSIONED) {
            stmt += 'version ' + tabInfo.DATA.VERSION + ' show job definition ' + this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME)  + "; ";
            // console.log(tabInfo.TYPE)
        } else {
            // if object was cloned there is no ID available/ set to -1 , take path + name instead:
            if (tabInfo.ID == '-1' || !tabInfo.ID) {

                stmt += "show job definition " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + "; ";
            } else {
                stmt += "show job definition " + tabInfo.ID + "; ";
            }
        }


        // stmt += "show job definition " + tabInfo.ID + "; ";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            // Edit response because job definition does not match editorform completely
            if (response.hasOwnProperty("FEEDBACK")) {
                let record = response.DATA.RECORD;
                // check version first before overwriting settings
                // let isVersion: boolean = this.isVersioned(tabInfo);

                tabInfo.VERSIONED ? tabInfo.ISREADONLY = true: tabInfo.ISREADONLY = false;
                // set tabinfo ID
                tabInfo.VERSIONED ? '' : tabInfo.ID = record.ID;

                // if type has changed before reload batch to job for example
                // in versiond that type cannot change
                tabInfo.VERSIONED ? '' : tabInfo.TYPE = record.TYPE;

                // set icon only in case of not versioned
                tabInfo.VERSIONED ? '' : tabInfo.shortType = record.TYPE?.toLowerCase();

                tabInfo.VERSIONED ? record.VERSION = tabInfo.DATA.VERSION : "" ;
        
                record.FULLNAME = this.toolbox.namePathQuote(record.NAME);
                let pathArray = record.NAME.split('.')
                record.NAME = pathArray.pop();
                record.PATH = pathArray.join('.');
                record.READ_MASTER_SUBMITABLE = record.MASTER_SUBMITTABLE;
                record.FP_NAME = record.FP_NAME == '<null>' ? '' : record.FP_NAME;
                record.KILL_PROGRAM = (record.KILL_PROGRAM == null ? '' : record.KILL_PROGRAM)
                record.RERUN_PROGRAM = (record.RERUN_PROGRAM == null ? '' : record.RERUN_PROGRAM)
                record.RESUME_TYPE = 'NONE';
                record.RESUME_IN_UNIT = record.RESUME_BASE;
                if (record.RESUME_IN) {
                    record.RESUME_TYPE = 'IN';
                }
                if (record.RESUME_AT) {
                    record.RESUME_TYPE = 'AT'
                }
                if (record.SUBMIT_SUSPENDED != "true") {
                    record.RESUME_TYPE = "NONE";
                }
                if (record.RESUME_TYPE != "AT") {
                    record.RESUME_TIME = "";
                }
                if (record.RESUME_TYPE != "IN") {
                    record.RESUME_IN = "1";
                    record.RESUME_IN_UNIT = "MINUTE";
                }
                // reset env name so that the required tag works
                record.ENV_NAME = ((record.ENV_NAME == "<null>" || !record.ENV_NAME) ? null : record.ENV_NAME)
                // fix parameters
                let rowIndex = 0;
                for (let parameter of record["PARAMETER"].TABLE) {
                    parameter.PARENTNAME = record.PATH + '.' + record.NAME;
                    if (parameter.COMMENT) {
                        parameter.COMMENT = parameter.COMMENT.trim();
                    }
                    parameter.NOFORM_COMMENT = parameter.COMMENT;
                    parameter.ORIGINAL_NAME = parameter.NAME;
                    if (parameter.TYPE == "EXPRESSION") {
                        parameter.EXPRESSION_TYPE = parameter.EXPRESSION.split("(")[0];
                        parameter.EXPRESSION_VALUE = parameter.EXPRESSION.split("(")[1].split(")")[0];
                        parameter.DEFAULT = "false";
                    }
                    else {
                        if (parameter.TYPE == 'PARAMETER') {
                            this.parseForWebConfig(parameter);
                            if (parameter.hasOwnProperty("WEBCONFIG") && parameter.WEBCONFIG.hasOwnProperty("DEFAULT")) {
                                parameter.DEFAULT = "true";
                                parameter.DEFAULT_VALUE = this.getCustomProperty(parameter.WEBCONFIG.DEFAULT);
                            }
                            parameter.SUBMIT_VALUE = parameter.DEFAULT_VALUE;
                        }
                    }
                    if (parameter.TYPE == "REFERENCE" || parameter.TYPE == "CHILDREFERENCE" || parameter.TYPE == "RESOURCEREFERENCE") {
                        parameter.REFERENCE = parameter.REFERENCE_PATH + " (" + parameter.REFERENCE_PARAMETER + ")";
                    }
                    // DEFAULT_VALUE is not set to '' if it is null in deepNulltoEmptyString !!!
                    if (parameter.DEFAULT_VALUE == null || parameter.DEFAULT_VALUE == undefined) {
                        parameter.DEFAULT = "false";
                        parameter.DEFAULT_VALUE = '';
                    }
                    else {
                        parameter.DEFAULT = "true";
                    }
                    parameter.rowIndex = rowIndex;
                    rowIndex++;
                }
                this.sortParameters(record);
                // for (let parameterReference of record["REFERENCES"].TABLE) {
                //     parameterReference.REFERENCE_DESCRIPTION = parameterReference.REFERENCE_PARAMETER + ' -> ' + parameterReference.REFERENCE_PATH.split('.').pop();
                // }
                for (let child of record["CHILDREN"].TABLE) {
                    if (child.RESUME_IN) {
                        child.RESUME_TYPE = 'IN';
                    } else if (child.RESUME_AT) {
                        child.RESUME_TYPE = 'AT';
                    } else {
                        child.RESUME_TYPE = 'NONE';
                    }
                    if (!child.INT_NAME) {
                        child.INT_NAME = '';
                    }
                    if (!child.EST_NAME) {
                        child.EST_NAME = '';
                    }
                }
                this.sortChildren(record);
                for (let requiredResource of record["REQUIRED_RESOURCES"].TABLE) {
                    requiredResource.PARENTNAME = record.PATH + '.' + record.NAME;
                    let map: any = {
                        'N': "NOLOCK",
                        'S': "SHARED",
                        'X': "EXCLUSIVE",
                        'SX': "SHARED EXCLUSIVE",
                        'SC': "SHARED COMPATIBLE"
                    }
                    if (requiredResource.ORIGIN) {
                        requiredResource.NOFORM_BULK_DISABLED = true;
                    }
                    requiredResource.LOCKMODE = map[requiredResource.LOCKMODE];
                    if (requiredResource.STICKY_NAME == null) {
                        requiredResource.STICKY_NAME = "";
                    }
                    if (requiredResource.STICKY_PARENT == null) {
                        requiredResource.STICKY_PARENT = "";
                    }
                    if (!requiredResource.RESOURCE_STATE_MAPPING) {
                        requiredResource.RESOURCE_STATE_MAPPING = '';
                    }
                }
                this.sortRequiredResources(record);
                record["DEFINED_RESOURCES"].TABLE = record["DEFINED_RESOURCES"].TABLE.filter(function (value: any, index: any, arr: any) {
                    value.fe_flagged_to_cut = false;
                    return value.USAGE != 'CATEGORY';
                });
                record["NOFORM_DEFINED_RESOURCES"] = {}
                record["NOFORM_DEFINED_RESOURCES"].TABLE = record["DEFINED_RESOURCES"].TABLE.filter(function (value: any, index: any, arr: any) {
                    return value.USAGE != 'CATEGORY';
                });
                // map inherit grants for basic
                this.sortRequiredJobs(record);
                this.setInheritGrants(record);

                record.AGING_BASE == null ? record.AGING_BASE = 'MINUTE' : '';
                record.OLD_TYPE = record.TYPE;

                let approvalOperations = this.getApprovalOperations();
                let approvals : any[] = [];
                let translatorService = new TranslateService (new LocalStorageHelper(this.eventEmitterService));
                for (let approvalOperation of approvalOperations) {
                    approvals.push({
                        "OPERATOR_ACTION" : translatorService.translate(approvalOperation.LABEL),
                        "APPROVAL" : record[approvalOperation.APPROVAL + "_APPROVAL"],
                        "LEADING" : record[approvalOperation.APPROVAL + "_LEAD_FLAG"]
                    });
                }
                record.APPROVALS = { "TABLE" : approvals };
            }
            promise.response = response;
            return promise;
        }).then((promise: any) => {
            // init restart trigger before loading triggers
            let record = promise.response.DATA.RECORD;
            this.initRestartTrigger(record)
            let promises: Promise<any>[] = [
                this.getTriggers(record),
                this.getTriggersby(record)
            ];
            if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
                // console.log(tabInfo)
                // console.log(record)
                promises.push(this.checkConventionFolder(record));
                if (record.IN_CONVENTION_FOLDER = "true") {
                    // for clone we need to know whether the paranet folder of our dolder is a convention folder
                    promises.push(this.checkParentIsConventionFolderId(record));
                }
            }
            return Promise.all(promises).then(() => {
                if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
                    return this.checkConventionFolder(record).then(content => {
                        // console.log("IN_CONVENTION_FOLDER = " + record.IN_CONVENTION_FOLDER)
                        // console.log("IS_CONVENTION_FOLDER = " + record.IS_CONVENTION_FOLDER)
                        if (record.PATH.split('.').pop() == record.NAME) {
                            record.RENAME_BATCH_FOLDER = "true";
                        }
                        if (record.IN_CONVENTION_FOLDER == "true") {
                            if ((record.IN_CONVENTION_FOLDER == "true" && record.PATH.split('.').pop() != record.NAME) ||
                                (record.PARENT_IS_CONVENTION_FOLDER == "true" && record.PATH.split('.').pop() == record.NAME)
                            ) {
                                record.ADD_CHILD = "true";
                            }
                            else {
                                record.ADD_CHILD = "false";
                            }
                            if (record.PARENT_IS_CONVENTION_FOLDER == "true") {
                                if (record.PARENT_IS_CONVENTION_FOLDER_TYPE == "JOB") {
                                    record.STATIC = "false";
                                }
                                else {
                                    record.STATIC = "true";
                                }
                            }
                            else {
                                if (record.IN_CONVENTION_FOLDER_TYPE == "JOB") {
                                    record.STATIC = "false";
                                }
                                else {
                                    record.STATIC = "true";
                                }
                            }
                        }
                        return promise;
                    });
                }
                return promise;
            });
        });
    }

    getApprovalOperations() : any[] {
        return [
            { "APPROVAL" : "CANCEL"         , "LABEL" : "cancel"           , "SYNTAX" : "CANCEL" },
            { "APPROVAL" : "RERUN"          , "LABEL" : "rerun"            , "SYNTAX" : "RERUN" },
            { "APPROVAL" : "ENABLE"         , "LABEL" : "enable_disable"   , "SYNTAX" : "ENABLE" },
            { "APPROVAL" : "SET_JOB_STATE"  , "LABEL" : "set_job_state"    , "SYNTAX" : "SET JOB STATE" },
            { "APPROVAL" : "SET_STATE"      , "LABEL" : "set_state"        , "SYNTAX" : "SET STATE" },
            { "APPROVAL" : "IGN_DEPENDENCY" , "LABEL" : "ignore_dependency", "SYNTAX" : "IGNORE DEPENDENCY" },
            { "APPROVAL" : "IGN_RESOURCE"   , "LABEL" : "ignore_resource"  , "SYNTAX" : "IGNORE RESOURCE" },
            { "APPROVAL" : "CLONE"          , "LABEL" : "clone"            , "SYNTAX" : "CLONE" },
            { "APPROVAL" : "EDIT_PARAMETER" , "LABEL" : "edit_parameter"   , "SYNTAX" : "EDIT PARAMETER" },
            { "APPROVAL" : "KILL"           , "LABEL" : "kill",              "SYNTAX" : "KILL" }
        ];
    }

    sortParameters(bicsuiteObject : any) {
        let parameterSequenceStr = "";
        for (let parameter of bicsuiteObject.PARAMETER.TABLE) {
            if (parameter.TYPE == "CONSTANT") {
                if (parameter.NAME == "PARAMETER_SEQUENCE") {
                    parameterSequenceStr = parameter.DEFAULT_VALUE;
                    break;
                }
            }
        }
        if (parameterSequenceStr != "") {
            let defaultIndex = 10000;
            let parameterSequence = parameterSequenceStr.split(",");
            let ps = [];
            for (let p of parameterSequence) {
                ps.push(p.trim());
            }
            parameterSequence = ps;
            for (let parameter of bicsuiteObject.PARAMETER.TABLE) {
                let index = parameterSequence.indexOf(parameter.NAME);
                if (index == -1) {
                    parameter.SEQ = defaultIndex;
                }
                else {
                    parameter.SEQ = index;
                }
            }
            bicsuiteObject.PARAMETER.TABLE = bicsuiteObject.PARAMETER.TABLE.sort(
                function (a: any, b: any) {
                    if (a.SEQ == b.SEQ) {
                        return a.NAME < b.NAME ? -1 : 1;
                    }
                    else {
                        return a.SEQ - b.SEQ;
                    }
                }
            );
        }
        else {
            bicsuiteObject.PARAMETER.TABLE.sort(function(a : any, b : any) {
                return a.NAME < b.NAME ? -1 : 1;
            });
        }
    }

    getTriggers(jobData: any) {
        return this.triggerCrud.list(jobData.PATH + "." + jobData.NAME).then((triggerData: any) => {
            jobData.TRIGGERS = triggerData.DATA;
            jobData.ORIGINAL_TRIGGERS = JSON.parse(JSON.stringify(triggerData.DATA));
            let triggers = [];
            let defaultGroup = this.privilegesService.getDefaultGroup();
            let haveRestartTrigger: boolean = false;
            for (let trigger of jobData.TRIGGERS.TABLE) {
                if (trigger.ACTION == "RERUN") {
                    this.setRestartTrigger(jobData, trigger);
                    haveRestartTrigger = true;
                    continue;
                }
                if (trigger.IS_INVERSE == "false") {
                    trigger.NOFORM_BULK_DISABLED = false;
                }
                else {
                    trigger.NOFORM_BULK_DISABLED = true;
                }
                if (trigger.MASTER != 'true') {
                    trigger.SUBMIT_OWNER = defaultGroup;
                }
                trigger.ORIGINAL_NAME = trigger.NAME;
                trigger.TRIGGER_TYPE = trigger.TRIGGER_TYPE.replace('_', ' ')
                trigger.TRIGGER_PARAMETERS = trigger.PARAMETERS;
                trigger.RESUME_TYPE = 'NONE';
                if (trigger.RESUME_AT) {
                    trigger.RESUME_TYPE = 'AT'
                }
                if (trigger.RESUME_IN) {
                    trigger.RESUME_TYPE = 'IN'
                }
                if (!trigger.RESUME_BASE) {
                    trigger.RESUME_BASE = 'MINUTE';
                }
                triggers.push(trigger);
            }
            if (!haveRestartTrigger) {
                jobData.RESTART_MODE = "NO";
            }
            jobData.ORIGINAL_RESTART_MODE = jobData.RESTART_MODE;
            jobData.TRIGGERS.TABLE = triggers;
            this.sortTriggers(jobData);
            jobData.ORIGINAL_TRIGGERS = JSON.parse(JSON.stringify(jobData.TRIGGERS));
            return jobData;
        })
    }

    getTriggersby(jobData: any) {
        return this.triggerCrud.list(jobData.PATH + "." + jobData.NAME, null, true).then((triggerData: any) => {
            jobData.TRIGGERED_BY = triggerData.DATA;
            let triggers = [];
            let defaultGroup = this.privilegesService.getDefaultGroup();
            for (let trigger_by of jobData.TRIGGERED_BY.TABLE) {
                if (trigger_by.ACTION == "RERUN") {
                    continue;
                }
                if (trigger_by.IS_INVERSE == "false") {
                    trigger_by.NOFORM_BULK_DISABLED = true;
                }
                else {
                    trigger_by.NOFORM_BULK_DISABLED = false;
                }
                if (trigger_by.MASTER != 'true') {
                    trigger_by.SUBMIT_OWNER = defaultGroup;
                }
                trigger_by.ORIGINAL_NAME = trigger_by.NAME;
                trigger_by.TRIGGER_TYPE = trigger_by.TRIGGER_TYPE.replace('_', ' ')
                trigger_by.TRIGGER_PARAMETERS = trigger_by.PARAMETERS;
                trigger_by.RESUME_TYPE = 'NONE';
                if (trigger_by.RESUME_AT) {
                    trigger_by.RESUME_TYPE = 'AT'
                }
                if (trigger_by.RESUME_IN) {
                    trigger_by.RESUME_TYPE = 'IN'
                }
                if (!trigger_by.RESUME_BASE) {
                    trigger_by.RESUME_BASE = 'MINUTE';
                }
                triggers.push(trigger_by);
            }
            jobData.TRIGGERED_BY.TABLE = triggers;
            this.sortTriggersBy(jobData);
            jobData.ORIGINAL_TRIGGERED_BY = JSON.parse(JSON.stringify(jobData.TRIGGERED_BY));
            return jobData;
        });
    }

    initRestartTrigger(jobData: any) {
        jobData.RESTART_MODE = 'NO';
        jobData.RESTART_AT = '';
        jobData.RESTART_IN = '';
        jobData.RESTART_BASE = 'MINUTE';
        jobData.RESTART_STATES = { TABLE: [] };
        jobData.RESTART_CONDITION = '';
        jobData.RESTART_MAX_RETRY = 0;
        jobData.RESTART_WARN = false;
        jobData.RESTART_LIMIT_STATE = '';
    }

    setRestartTrigger(jobData: any, trigger: any) {
        // console.log(trigger)

        jobData.RESTART_MODE = 'IMMEDIATE';

        if (trigger.RESUME_AT) {
            jobData.RESTART_MODE = 'AT';
            jobData.RESTART_AT = trigger.RESUME_AT;
        }

        if (trigger.RESUME_IN) {
            jobData.RESTART_MODE = 'IN';
            jobData.RESTART_IN = trigger.RESUME_IN;
            jobData.RESTART_BASE = trigger.RESUME_BASE;
        }
        let states = trigger.STATES.split(',');
        let table = [];
        for (let state of states) {
            table.push({
                'EXIT_STATE': state
            })
        }
        jobData.RESTART_STATES = { TABLE: table };

        // trigger.STATES;
        jobData.RESTART_CONDITION = trigger.CONDITION ? trigger.CONDITION : '';
        jobData.RESTART_MAX_RETRY = trigger.MAX_RETRY;
        jobData.RESTART_WARN = trigger.WARN;
        jobData.RESTART_LIMIT_STATE = trigger.LIMIT_STATE ? trigger.LIMIT_STATE : '';

    }

    async create(obj: any, tabInfo: Tab): Promise<Object> {

        obj.PATH = tabInfo.PATH;
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts: string[] = [];

        let parentPath = obj.PATH + "." + obj.PATH.split('.').pop();

        let createdFolder: string;
        let path: string = obj.PATH;
        if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true" &&
           (obj.CREATE_BATCH_FOLDER == "true")) {
            stmts.push("create folder " + this.toolbox.namePathQuote(obj.PATH + "." + obj.NAME) + " with group= '" + obj.OWNER + "'");
            createdFolder = obj.PATH + "." + obj.NAME;
            path = obj.PATH + "." + obj.NAME;
        }

        stmts.push("create job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + this.createWith(obj, tabInfo, obj.PATH, "CREATE"));

        obj.ORIGINAL_TRIGGERS = {}
        obj.ORIGINAL_TRIGGERS.TABLE = [];
        obj.ORIGINAL_TRIGGERED_BY = {}
        obj.ORIGINAL_TRIGGERED_BY.TABLE = [];

        stmts = stmts.concat(this.buildTriggers(obj, path));

        if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true" &&
            obj.ADD_CHILD == "true" && obj.IN_CONVENTION_FOLDER == "true") {
            stmts.push(this.createAddChildCommand(parentPath, path + "." + obj.NAME, obj.STATIC == "true"));
        }

        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "JOB_DEFINITION", OP: "CREATE", NAME: path + "." + obj.NAME });
            if (createdFolder) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "CREATE", NAME: createdFolder });
            }
            obj.PATH = path;
            tabInfo.PATH = obj.PATH;
            return response;
        }, (rejection: any)=>{
        });
    }

    fixupPath(name: string, oldPath: string, newPath: string): string {
        // console.log("fixupPath(" + name + ", " + oldPath + ", " + newPath + ")")
        if (oldPath != newPath && (name + '.').startsWith(oldPath + '.')) {
            name = name.replace(oldPath, newPath);
        }
        // console.log("   -> " + name)
        return name;
    }

    fixup(obj: any, type: string, fullNameOld: string, fullNameNew: string) {
        if (type == "FOLDER") {
            for (let o of obj.CHILDREN.TABLE) {
                o.CHILDNAME = this.fixupPath(o.CHILDNAME, fullNameOld, fullNameNew);
                o.PARENTNAME = this.fixupPath(o.PARENTNAME, fullNameOld, fullNameNew);
            }
            for (let o of obj.PARENTS.TABLE) {
                o.CHILDNAME = this.fixupPath(o.CHILDNAME, fullNameOld, fullNameNew);
                o.PARENTNAME = this.fixupPath(o.PARENTNAME, fullNameOld, fullNameNew);
            }
            for (let o of obj.REQUIRED_JOBS.TABLE) {
                o.DEPENDENTNAME = this.fixupPath(o.DEPENDENTNAME, fullNameOld, fullNameNew);
                o.REQUIREDNAME = this.fixupPath(o.REQUIREDNAME, fullNameOld, fullNameNew);
            }
            for (let o of obj.ORIGINAL_TRIGGERED_BY.TABLE) {
                o.OBJECT_NAME = this.fixupPath(o.OBJECT_NAME, fullNameOld, fullNameNew);
                o.SUBMIT_NAME = this.fixupPath(o.SUBMIT_NAME, fullNameOld, fullNameNew);
            }
            for (let o of obj.TRIGGERED_BY.TABLE) {
                o.OBJECT_NAME = this.fixupPath(o.OBJECT_NAME, fullNameOld, fullNameNew);
                o.SUBMIT_NAME = this.fixupPath(o.SUBMIT_NAME, fullNameOld, fullNameNew);
            }
            for (let o of obj.ORIGINAL_TRIGGERS.TABLE) {
                o.OBJECT_NAME = this.fixupPath(o.OBJECT_NAME, fullNameOld, fullNameNew);
                o.SUBMIT_NAME = this.fixupPath(o.SUBMIT_NAME, fullNameOld, fullNameNew);
            }
            for (let o of obj.TRIGGERS.TABLE) {
                o.OBJECT_NAME = this.fixupPath(o.OBJECT_NAME, fullNameOld, fullNameNew);
                o.SUBMIT_NAME = this.fixupPath(o.SUBMIT_NAME, fullNameOld, fullNameNew);
            }
        }
    }

    clone(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let oldPath = obj.PATH;
        let newPath = obj.PATH;
        let stmts: string[] = [];
        let path = obj.PATH;
        let isBifc: boolean = this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true";
        let cloningBifcFolderBatch = false;
        if (isBifc && obj.ORIGINAL_NAME == obj.PATH.split('.').pop()) {
            // cloning a convention folder main batch/job
            cloningBifcFolderBatch = true;
            path = obj.PATH.split('.');
            path.pop();
            path.push(obj.NAME);
            path = path.join('.');
            stmts.push("copy folder "
                + this.toolbox.namePathQuote(oldPath)
                + " to "
                + this.toolbox.namePathQuote(path));
            stmts.push("rename job definition  " + this.toolbox.namePathQuote(path + '.' + obj.ORIGINAL_NAME) + " to '" + this.toolbox.namePathQuote(obj.NAME) + "'");
            obj.ORIGINAL_NAME = obj.NAME;
        }
        else {
            stmts.push("copy job definition "
                + this.toolbox.namePathQuote(path + "." + obj.ORIGINAL_NAME)
                + " to "
                + this.toolbox.namePathQuote(path + "." + obj.NAME))
        }
        if (isBifc && obj.ADD_CHILD == "true") {
            if (cloningBifcFolderBatch) {
                let parentPath = obj.PATH.split('.');
                parentPath.pop();
                parentPath = parentPath.join('.') + "." + parentPath.pop();
                stmts.push(this.createAddChildCommand(parentPath, path + "." + obj.NAME, obj.STATIC == "true"));
            }
            else {
                let parentPath = obj.PATH + "." + obj.PATH.split('.').pop();
                stmts.push(this.createAddChildCommand(parentPath, path + "." + obj.NAME, obj.STATIC == "true"));
            }
        }
        if (newPath != oldPath) {
            this.fixup(obj, "FOLDER", oldPath, newPath);
        }
        stmts.push("alter job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + this.createWith(obj, tabInfo, path, "CLONE"));
        stmts = stmts.concat(this.buildTriggers(obj, path));
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            tabInfo.NAME = obj.NAME;
            obj.PATH = path;
            this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "JOB_DEFINITION", OP: "CREATE", NAME: tabInfo.PATH + "." + obj.NAME });
            return response;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        // name casing
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let path = obj.PATH;
        let stmts: string[] = [];
        let folderOldName = "";
        let folderNewName = "";

        if (obj.ORIGINAL_NAME != obj.NAME) {
            if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true" &&
                obj.RENAME_BATCH_FOLDER == "true") {
                folderOldName = path;
                stmts.push("rename folder " + this.toolbox.namePathQuote(path) + " to '" + this.toolbox.namePathQuote(obj.NAME) + "'");
                let pathArray: string[] = path.split('.');
                pathArray.pop();
                pathArray.push(obj.NAME);
                path = pathArray.join('.');
                folderNewName = path;
            }
            stmts.push("rename job definition " + this.toolbox.namePathQuote(path + "." + obj.ORIGINAL_NAME) + " to '" + this.toolbox.namePathQuote(obj.NAME) + "'");
            obj.F_REFRESH_NAVIGATOR = true;
        }
        if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true" &&
            obj.CREATE_BATCH_FOLDER == "true" && obj.OLD_TYPE == "JOB" && obj.TYPE == "BATCH"
        ) {
            let tmpPath = this.toolbox.namePathQuote(path + '.' + '__TMP__');
            stmts.push("create folder " + tmpPath + " with group = '" + obj.OWNER + "'");
            stmts.push("move job definition " + this.toolbox.namePathQuote(path + '.' + obj.NAME) + " to " + tmpPath);
            stmts.push("rename folder " + tmpPath + " to '" + obj.NAME + "'");
            path = path + '.' + obj.NAME;
            obj.F_REFRESH_NAVIGATOR = true;
        }

        if (obj.DECOMPOSITE == "true") {
            // create job including definied resources
            obj.NOFORM_JOB_NAME = this.convertIdentifier(obj.NOFORM_JOB_NAME, obj.ORIGINAL_NAME + '_');
            stmts.push("copy job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) +
                " to " + this.toolbox.namePathQuote(path + "." + obj.NOFORM_JOB_NAME));
            // remove defined resources from batch
            for (let resource of obj.DEFINED_RESOURCES.TABLE) {
                stmts.push("drop resource " + this.toolbox.namePathQuote(resource.NAME) + " in " + this.toolbox.namePathQuote(
                    this.fixupPath(resource.SCOPE, obj.PATH, path)));
                obj.DEFINED_RESOURCES.TABLE = [];
            }
            // update job
            let jobObj = this.toolbox.deepCopy(obj);
            jobObj.TYPE = "JOB";
            jobObj.NAME = obj.NOFORM_JOB_NAME;
            jobObj.PATH = path;
            let jobTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
            jobTabInfo.TYPE = "JOB";
            jobTabInfo.NAME = obj.NOFORM_JOB_NAME;
            // remove dependencies from job because those are kept with the batch
            jobObj.REQUIRED_JOBS.TABLE = [];
            stmts.push("alter job definition " + this.toolbox.namePathQuote(path + "." + obj.NOFORM_JOB_NAME) + this.createWith(jobObj, jobTabInfo, path, "CREATE"));
            // remove triggered_by because the batch will be triggered by
            jobObj.TRIGGERED_BY.TABLE = [];
            // prepare job triggers
            let jobTriggers: any[] = [];
            for (let trigger of jobObj.TRIGGERS.TABLE) {
                if (['AFTER_FINAL', 'WARNING', 'UNTIL_FINAL'].includes(trigger.TRIGGER_TYPE)) {
                    // AFTER_FINAL, WARNING and UNTIL_FINAL triggers will be triggerd on batch
                    continue;
                }
                jobTriggers.push(trigger);
            }
            jobObj.TRIGGERS.TABLE = jobTriggers;
            stmts = stmts.concat(this.buildTriggers(jobObj, path));
            // prepare batch triggers
            let batchTriggers: any[] = [];
            for (let trigger of obj.TRIGGERS.TABLE) {
                if (['BEFORE_FINAL', 'UNTIL_FINISHED'].includes(trigger.TRIGGER_TYPE)) {
                    // BEFORE_FINAL and UNTL_FINISHED triggers will be triggerd on job
                    continue;
                }
                if (trigger.TRIGGER_TYPE == "WARNING") {
                    trigger.TRIGGER_TYPE = "CHILD WARNING"
                }
                if (trigger.TRIGGER_TYPE == "IMMEDIATE LOCAL") {
                    trigger.TRIGGER_TYPE = "IMMEDIATE MERGE"
                }
                batchTriggers.push(trigger);
            }
            obj.TRIGGERS.TABLE = batchTriggers;
            obj.F_REFRESH_NAVIGATOR = true;
        }
        if (obj.PATH != path) {
            if (obj.DECOMPOSITE != "true") {
                this.fixup(obj, "FOLDER", obj.PATH, path);
            }
            obj.PATH = path;
            tabInfo.PATH = path;
        }
        stmts.push("alter job definition " + this.toolbox.namePathQuote(obj.PATH + "." + obj.NAME) + this.createWith(obj, tabInfo, path, "UPDATE"));
        stmts = stmts.concat(this.buildTriggers(obj, path));

        if (obj.DECOMPOSITE == "true") {
            // add job to children of batch
            stmts.push(this.createAddChildCommand(obj.PATH + "." + obj.NAME, obj.PATH + "." + obj.NOFORM_JOB_NAME, true));
        }

        for (let parameter of obj.PARAMETER.TABLE) {
            if (parameter.NOFORM_comments) {
                let stmt = "create comment on parameter '" + parameter.NAME + "' of job definition " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
                let withStmt = []
                for (let c of parameter.NOFORM_comments) {
                    c.TAG = c.TAG ? c.TAG : '';
                    let desc = c.COMMENT ? this.toolbox.textQuote(c.COMMENT) : '';
                    withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'");
                }
                stmt += withStmt.join(',');
                stmts.push(stmt);
            }
        }

        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            tabInfo.NAME = obj.NAME;
            tabInfo.TYPE = obj.TYPE;
            tabInfo.PATH = obj.PATH;    // pay change due to process decomposition
            if (obj.ORIGINAL_NAME != obj.NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "JOB_DEFINITION", OP: "RENAME", OLD_NAME: obj.PATH + "." + obj.ORIGINAL_NAME, NEW_NAME: obj.PATH + "." + obj.NAME });
            }
            if (obj.OLD_TYPE != obj.TYPE) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "JOB_DEFINITION", OP: "TYPECHANGE", NAME: obj.PATH + "." + obj.NAME, NEWTYPE: obj.TYPE });
            }
            if (folderNewName != folderOldName) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "RENAME", OLD_NAME: folderOldName, NEW_NAME: folderNewName });
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([
            {
                'NAME': 'FORCE',
                'TRANSLATE': 'force',
                'TYPE': 'CHECKBOX',
                'USE_LABEL': true
            }
        ], "drop_confirm{" + obj.TYPE + "}{" + obj.ORIGINAL_NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop job definition " + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.ORIGINAL_NAME);
                if (result.FORCE == "true") {
                    stmt += ' force';
                }
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "JOB_DEFINITION", OP: "DROP", NAME: tabInfo.PATH + "." + obj.ORIGINAL_NAME });
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return job_editorform;
    }


    private createWith(obj: any, tabInfo: Tab, path: string, mode: string): string {
        let stmt: string = " with\n";

        /** ---------------------------------------------- Properties Tab ---------------------------------------------- **/
        stmt += "type = " + obj.TYPE + ",\n";
        if (this.privilegesService.getGroups().includes(obj.OWNER) || this.privilegesService.isAdmin()) {
            stmt += "group= '" + obj.OWNER + "',\n";
        }
        if (this.privilegesService.isEdition('BASIC')) {
            if (obj.INHERIT_GRANTS == 'ALL') {
                stmt += 'inherit grant = ' + '(DROP,EDIT,MONITOR,OPERATE,SUBMIT,RESOURCE,VIEW),\n';
            } else if (obj.INHERIT_GRANTS == 'VIEW') {
                stmt += 'inherit grant = ' + '(VIEW),\n';
            } else {
                stmt += 'inherit grant = ' + 'NONE,\n';
            }
        }
        stmt += "profile = " + (obj.ESP_NAME ? "'" + obj.ESP_NAME + "'" : 'NONE') + ",\n";
        if (obj.SUBMIT_SUSPENDED == 'true') {
            stmt += 'suspend,\n';
            if (obj.RESUME_TYPE == 'AT') {
                stmt += 'resume at \'' + obj.RESUME_AT + '\',\n';
            }
            else if (obj.RESUME_TYPE == 'IN') {
                stmt += 'resume in ' + obj.RESUME_IN + ' ' + obj.RESUME_IN_UNIT + ',\n';
            }
            else {
                stmt += 'noresume,\n';
            }
        } else {
            stmt += 'nosuspend,\n'
        }

        if (obj.TYPE != 'MILESTONE') {
            stmt += (obj.MASTER_SUBMITTABLE == "false" || !obj.MASTER_SUBMITTABLE ? "nomaster" : "master") + ",\n";
        }
        if (obj.TYPE != 'MILESTONE') {
            stmt += 'runtime final = ' + (obj.EXPECTED_FINALTIME ? obj.EXPECTED_FINALTIME : '0') + ',\n'

        }

        if (obj.TYPE == 'BATCH') {
            // only at batches "nice value",  else at run tab: priority = ...
            stmt += 'nicevalue = ' + (obj.PRIORITY ? obj.PRIORITY : 'NONE') + ',\n'
        }

        /** ---------------------------------------------- Run Tab ----------------------------------------------**/

        // aging and priority
        if (obj.TYPE == 'JOB') {
            stmt += "priority = " + (obj.PRIORITY ? obj.PRIORITY : "none") + ",\n";
            stmt += "aging = " + (obj.AGING_AMOUNT ? obj.AGING_AMOUNT + ' ' + obj.AGING_BASE : 'none') + ",\n";
            stmt += "min priority = " + (obj.MIN_PRIORITY ? obj.MIN_PRIORITY : 'none') + ",\n";
            obj.EXPECTED_RUNTIME ? stmt += "runtime = " + obj.EXPECTED_RUNTIME + ",\n" : '';
            stmt += "run program = " + (obj.RUN_PROGRAM ? "'" + this.toolbox.textQuote(obj.RUN_PROGRAM) + "'" : 'NONE') + "\n,";
            stmt += "rerun program = " + (obj.RERUN_PROGRAM ? "'" + this.toolbox.textQuote(obj.RERUN_PROGRAM) + "'" : 'NONE') + "\n,";
            stmt += "kill program = " + (obj.KILL_PROGRAM ? "'" + this.toolbox.textQuote(obj.KILL_PROGRAM) + "'" : 'NONE') + "\n,";
            stmt += "environment = '" + obj.ENV_NAME + "'\n,";
            if (obj.ESM_NAME != '<default>' && obj.ESM_NAME) {this.openEspName
                stmt += "mapping = '" + obj.ESM_NAME + "'\n,";
            } else {
                stmt += "mapping = NONE" + "\n,";
            }
            stmt += "footprint = " + (obj.FP_NAME && obj.FP_NAME != "" ? "'" + obj.FP_NAME + "'" : "NONE") + ",\n";
            stmt += "workdir = " + (obj.WORKDIR ? "'" + obj.WORKDIR + "'" : "NONE") + "\n,";
            stmt += "logfile = " + (obj.LOGFILE ? "'" + obj.LOGFILE + "' " + (obj.TRUNC_LOG == "true" ? "trunc" : "notrunc") : "NONE") + ",\n";
            stmt += "errlog  = " + (obj.ERRLOGFILE ? "'" + obj.ERRLOGFILE + "' " + (obj.TRUNC_ERRLOG == "true" ? "trunc" : "notrunc") : "NONE") + ",\n";
        }

        // ----------------------------------------------- PARAMETER ------------------------------------------------
        if (obj.PARAMETER.TABLE.length > 0) {
            stmt += "parameters = (\n"

            let substmts = [];
            for (let parameter of obj.PARAMETER.TABLE) {
                let name = this.convertIdentifier(parameter.NAME, parameter.ORIGINAL_NAME);
                let substmt = '';
                let parameterId = "0";
                if (!parameter.DEFAULT_VALUE) {
                    parameter.DEFAULT_VALUE = '';
                }
                if (parameter.ID && mode == "UPDATE") {
                    parameterId = parameter.ID;
                }
                substmt += "'" + name + "' (" + parameterId + ")";
                if (parameter.TYPE == "EXPRESSION") {
                    substmt += " " + parameter.EXPRESSION_TYPE + "(" + parameter.EXPRESSION_VALUE + ")";
                }
                else if (parameter.TYPE == "CONSTANT") {
                    substmt += " " + parameter.TYPE;
                    substmt += " = '" + this.toolbox.textQuote(parameter.DEFAULT_VALUE) + "'";
                }
                else if (parameter.TYPE == "REFERENCE") {
                    substmt += "reference " + parameter.REFERENCE;
                }
                else if (parameter.TYPE == "CHILDREFERENCE") {
                    substmt += "reference child " + parameter.REFERENCE;
                }
                else if (parameter.TYPE == "RESOURCEREFERENCE") {
                    substmt += "reference resource " + parameter.REFERENCE;
                }
                else {
                    let parameterType = parameter.TYPE;
                    if (parameterType == 'import_unresolved') {
                        parameterType = 'import_unresolved';
                    }
                    substmt += " " + parameterType;
                }
                if (parameter.TYPE != "EXPRESSION" && parameter.TYPE != "CONSTANT") {
                    if (parameter.DEFAULT == "true") {
                        substmt += " default = '" + this.toolbox.textQuote(parameter.DEFAULT_VALUE) + "'";
                    }
                }

                if (parameter.IS_LOCAL == true || parameter.IS_LOCAL == "true")
                    substmt += " local";

                substmt += " export = " + (parameter.EXPORT_NAME ? "'" + parameter.EXPORT_NAME + "'" : "NONE");
                substmts.push(substmt);
            }
            stmt += substmts.join(',\n')
            stmt += '\n),'
        } else {
            stmt += "parameters = NONE,\n";
        }

        // ------------------------------------------------------PARENT CHILD -----------------------------------------

        let childrenStmtList = [];
        for (let child of obj.CHILDREN.TABLE) {
            let childstmt = [];
            childstmt.push(this.toolbox.namePathQuote(this.fixupPath(child.CHILDNAME, obj.PATH, path)));
            childstmt.push(((child.IS_STATIC == 'true') || (child.IS_STATIC == true)) ? 'static' : 'dynamic');
            childstmt.push(((child.IS_DISABLED == 'true') || (child.IS_DISABLED == true)) ? 'disable' : 'enable');
            childstmt.push(child.ENABLE_CONDITION ? "condition = '" + this.toolbox.textQuote(child.ENABLE_CONDITION) + "'" : '');
            childstmt.push("mode = " + child.ENABLE_MODE);
            childstmt.push("interval = " + (child.INT_NAME ? child.INT_NAME : "NONE"));
            childstmt.push("alias = " + (child.ALIAS_NAME ? "'" + child.ALIAS_NAME + "'" : "NONE"));
            childstmt.push("priority = " + (child.PRIORITY ? child.PRIORITY : "0"));
            childstmt.push(child.SUSPEND)
            if (child.RESUME_TYPE == 'AT') {
                childstmt.push('resume at ' + "'" + child.RESUME_AT + "'");
            } else if (child.RESUME_TYPE == 'IN') {
                //YYYY-MM-DDTHH:MI:SS
                childstmt.push('resume in ' + child.RESUME_IN + ' ' + child.RESUME_BASE);
            } else {
                childstmt.push('noresume');
            }
            childstmt.push("merge_mode = " + child.MERGE_MODE);
            childstmt.push("translation = " + ((child.EST_NAME && (child.EST_NAME != 'NONE')) ? "'" + child.EST_NAME + "'" : 'NONE'));

            // console.log(child);

            let ignoredDependcyClause: string = "ignore_dependency = ";
            let ignoredDependencies: string[] = [];

            if (child.IGNORED_DEPENDENCIES) {
                if (child.IGNORED_DEPENDENCIES.trim() != "") {
                    ignoredDependencies = child.IGNORED_DEPENDENCIES.split(',');
                }
            }
            if (ignoredDependencies.length > 0) {
                ignoredDependcyClause += '(';
                let quotedIgnoredDependencies: string[] = [];
                for (let name of ignoredDependencies) {
                    quotedIgnoredDependencies.push("'" + name.trim() + "'");
                }
                ignoredDependcyClause += quotedIgnoredDependencies.join(", ");
                ignoredDependcyClause += ')';
            }
            else {
                ignoredDependcyClause += " NONE"
            }
            childstmt.push(ignoredDependcyClause);
            childrenStmtList.push(childstmt.join(' '));
        }
        stmt += "children = "
        stmt += childrenStmtList.length > 0 ? "(" + childrenStmtList + "),\n" : "NONE,\n";

        // ------------------------------------------------------DEPENDEND/ DEPENDENCIES------------------------------

        let depModeMap: any = {
            'AND': 'all',
            'OR': 'any'
        }
        stmt += 'dependency_mode = ' + (depModeMap[obj.DEPENDENCY_MODE] ? depModeMap[obj.DEPENDENCY_MODE] : 'ALL'); + ",\n"

        let dependencyStmtList = [];
        for (let dependency of obj.REQUIRED_JOBS.TABLE) {
            let requiredName = this.fixupPath(dependency.REQUIREDNAME, obj.PATH, path);
            // self dependencies are allowed because of external dependencies may use this!
            if (requiredName == obj.PATH + '.' + obj.ORIGINAL_NAME) {
                requiredName = obj.PATH + '.' + obj.NAME;
            }
            let dependencyStmt = [];
            if (dependency.NAME && dependency.NAME != 'NONE') {
                dependencyStmt.push("dependency ");
                dependencyStmt.push("'" + dependency.NAME + "'");
            }
            dependencyStmt.push(this.toolbox.namePathQuote(requiredName));
            dependencyStmt.push("unresolved = " + (dependency.UNRESOLVED_HANDLING ? dependency.UNRESOLVED_HANDLING : "ERROR"));

            let conditionClause: string = "condition = ";
            conditionClause += (dependency.CONDITION ? "'" + this.toolbox.textQuote(dependency.CONDITION) + "'" : "NONE");
            dependencyStmt.push(conditionClause);

            if (dependency.STATE_SELECTION == "FINAL") {
                if (dependency.STATES) {
                    let stateList = dependency.STATES.split(",");
                    let dependencyStateStmt = [];
                    for (let state of stateList) {
                        dependencyStateStmt.push("'" + state.split(":")[0].trim() + "'" + (state.split(':')[1] ? " condition = '" + this.toolbox.textQuote(state.split(':')[1]).trim() + "'" : ""));
                    }
                    dependencyStmt.push("states = (" + dependencyStateStmt.join(",") + ")");
                }
                else {
                    dependencyStmt.push("states = NONE");
                }
            }
            else {
                dependencyStmt.push("states = " + dependency.STATE_SELECTION.replace('_', ' '));
            }
            dependencyStmt.push("mode = " + dependency.MODE);
            dependencyStmt.push("resolve = " + dependency.RESOLVE_MODE);
            if (dependency.RESOLVE_MODE == 'internal') {
                dependencyStmt.push("expired = NONE");
            } else {
                dependencyStmt.push("expired = " +
                    (dependency.EXPIRED_AMOUNT ?
                        dependency.EXPIRED_AMOUNT + " " + (dependency.EXPIRED_BASE ? dependency.EXPIRED_BASE : "minute") :
                        "NONE"
                    )
                );
            }
            dependencyStmt.push("select condition = " + (dependency.SELECT_CONDITION ? "'" + this.toolbox.textQuote(dependency.SELECT_CONDITION) + "'" : "NONE"));

            dependencyStmtList.push(dependencyStmt.join(" "));
        }
        stmt += dependencyStmtList.length > 0 ? ", required = (\n" + dependencyStmtList.join(",\n") + "\n),\n" : ", REQUIRED = NONE,\n";

        // DEPENDENDS READ ONLY

        // ------------------------------------------------------REQUIRED RESOURCES ----------------------------------

        let requiredResourceStmtList: any[] = [];

        // TIMEOUT
        let timeoutstmt = ''
        if (obj.TIMEOUT_AMOUNT && obj.TIMEOUT_AMOUNT != "NONE" && (obj.TIMEOUT_STATE != "NONE" && obj.TIMEOUT_STATE)) {
            timeoutstmt = 'timeout = ' + obj.TIMEOUT_AMOUNT + ' ' + (obj.TIMEOUT_BASE ? obj.TIMEOUT_BASE : 'minute') + " status '" + obj.TIMEOUT_STATE + "',\n";
        } else {
            timeoutstmt = 'timeout = NONE,\n'
        }
        stmt += timeoutstmt;

        for (let resource of obj.REQUIRED_RESOURCES.TABLE) {
            // when resource is no environmental -> update non environmental
            if (resource.DEFINITION != "ENVIRONMENT") {
                let resourceStmt = [];
                resourceStmt.push(this.toolbox.namePathQuote(resource.RESOURCE_NAME));

                // amount not allowed for static resources
                if (resource.RESOURCE_USAGE != 'STATIC') {
                    resourceStmt.push("amount = " + (resource.AMOUNT ? resource.AMOUNT : '0'));
                }
                // condition only allowed when static resource
                if (resource.RESOURCE_USAGE == 'STATIC') {
                    resourceStmt.push("condition = " + (resource.CONDITION ? "'" + this.toolbox.textQuote(resource.CONDITION) + "'" : "NONE"))
                }
                resourceStmt.push(resource.KEEP_MODE);

                // only allowed when choosen resource has a rsp
                if (resource.RESOURCE_STATE_PROFILE_NAME) {
                    resourceStmt.push("status mapping = " + (resource.RESOURCE_STATE_MAPPING ? "'" + resource.RESOURCE_STATE_MAPPING + "'" : "NONE"));
                }
                // console.log(resource.LOCKMODE)
                let map: any = {
                    'NOLOCK': "N",
                    'SHARED': "S",
                    'EXCLUSIVE': "X",
                    'SHARED EXCLUSIVE': "SX",
                    'SHARED COMPATIBLE': "SC"
                }
                let lockmode = map[resource.LOCKMODE];
                resourceStmt.push("lockmode = " + (lockmode ? lockmode : "N"));

                if (resource.STATES) {
                    resourceStmt.push("status = (" + resource.STATES + ")");
                } else {
                    resourceStmt.push("status = NONE");
                }

                if (resource.IS_STICKY == "true" || resource.IS_STICKY == true) {
                    // TODO Test
                    let stickyClause = "sticky"
                    if (resource.STICKY_NAME || resource.STICKY_PARENT) {
                        stickyClause += " (";
                        if (resource.STICKY_NAME) {
                            stickyClause += "'" + resource.STICKY_NAME + "'";
                            if (resource.STICKY_PARENT) {
                                stickyClause += ", ";
                            }
                        }
                        if (resource.STICKY_PARENT) {
                            stickyClause += (this.toolbox.namePathQuote(this.fixupPath(resource.STICKY_PARENT, obj.PATH, path)));
                        }
                        stickyClause += ")";
                    }
                    resourceStmt.push(stickyClause);
                } else {
                    resourceStmt.push("nosticky");
                }

                resourceStmt.push("expired = " +
                    (resource.EXPIRED_AMOUNT ?
                        resource.EXPIRED_AMOUNT + " " + (resource.EXPIRED_BASE ? resource.EXPIRED_BASE :
                            "minute") : "NONE"
                    )
                );
                resource.EXPIRED_AMOUNT ? resourceStmt.push(resource.IGNORE_ON_RERUN == 'true' || resource.IGNORE_ON_RERUN == true ? 'ignore on rerun' : '') : '';

                requiredResourceStmtList.push(resourceStmt.join(" "));
            }
        }

        // console.log("resource = (\n" + requiredResourceStmtList.join(",\n") + "\n)")
        stmt += requiredResourceStmtList.length > 0 ? "resource = (\n" + requiredResourceStmtList.join(",\n") + "\n)" : "resource = NONE";

        // -------------------------------------------------------------------------------------------------------------

        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            stmt += ',\napproval = (\n'
            let approvals : any[] = [];
            let i = 0;
            for (let approvalOperation of this.getApprovalOperations()) {
                approvals.push(approvalOperation.SYNTAX + " " +
                (obj.APPROVALS.TABLE[i].APPROVAL == "DEFAULT" ? "MASTER" : obj.APPROVALS.TABLE[i].APPROVAL) +
                (obj.APPROVALS.TABLE[i].LEADING == "true" ? " leading" : ""));
                i ++;
            }
            stmt += approvals.join(", ");
            stmt += ')'
        }
        return stmt;
    }

    private buildTriggers(obj: any, path: string): string[] {
        let stmts: string[] = [];
        /** Restart Tab - Trigger */
        // console.log(obj)
        if (obj.TYPE == "JOB") {
            if (obj.RESTART_MODE == "NO" && obj.ORIGINAL_RESTART_MODE != "NO") {
                stmts.push("drop existing trigger 'SYS_RESTART' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME));
            }
            if (obj.RESTART_MODE != "NO") {
                let stmt = "create or alter trigger 'SYS_RESTART' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " with ";
                if (obj.RESTART_MODE == "IN") {
                    stmt += "resume in " + obj.RESTART_IN + " " + obj.RESTART_BASE + ",";
                }
                else if (obj.RESTART_MODE == "AT") {
                    stmt += "resume at '" + obj.RESTART_AT + "',";
                }
                stmt += (obj.RESTART_MODE == "IMMEDIATE" ? "nosuspend" : "suspend") + ",";
                stmt += "active,"
                stmt += "condition = " + (obj.RESTART_CONDITION ? "'" + this.toolbox.textQuote(obj.RESTART_CONDITION) + "'" : "NONE") + ","
                stmt += (obj.RESTART_WARN == "true" ? "warn" : "nowarn") + ",";
                stmt += "limit state = " + (obj.RESTART_LIMIT_STATE ? "'" + obj.RESTART_LIMIT_STATE + "'" : "NONE") + ",";


                let states = this.generateTriggerStatesString(obj.RESTART_STATES.TABLE);
                stmt += "state = " + (states != "NONE" ? ("(" + states + ")") : states) + ",";
                stmt += "submitcount = " + (obj.RESTART_MAX_RETRY ? "" + obj.RESTART_MAX_RETRY : "0") + ",";
                stmt += "rerun,";
                stmt += "type = IMMEDIATE_LOCAL,";
                stmt += "parameters = NONE";
                stmts.push(stmt);
            }
        }
        // Drop Triggers that were deleted from the table.
        let foundTrigger: boolean = false;
        for (let o_trigger of obj.ORIGINAL_TRIGGERS.TABLE) {
            if (o_trigger.IS_INVERSE == "true" || o_trigger.IS_INVERSE == true) {
                continue;
            }
            foundTrigger = false;
            for (let trigger of obj.TRIGGERS.TABLE) {
                if (trigger.ID == undefined || trigger.ID == "" || trigger.ID == null || o_trigger.ID == trigger.ID) {
                    foundTrigger = true;
                    break;
                }
            }
            if (!foundTrigger) {
                stmts.push("drop trigger " + o_trigger.ID);
            }
        }
        // alter or create trigger
        let tmpRenames : string[] = [];
        for (let trigger of obj.TRIGGERS.TABLE) {
            if (trigger.IS_INVERSE == "true" || trigger.IS_INVERSE == true) {
                continue;
            }
            trigger.NAME = this.convertIdentifier(trigger.NAME, trigger.ORIGINAL_NAME);
            let tmpName = trigger.NAME;
            if (trigger.ORIGINAL_NAME && trigger.NAME != trigger.ORIGINAL_NAME) {
                tmpName = trigger.NAME + "_tmpRename";
                tmpRenames.push(trigger.NAME)
                stmts.push("rename trigger '" + trigger.ORIGINAL_NAME + "' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " to '" + tmpName + "'");
            }
            let stmt : string = "create or alter trigger '" + tmpName + "' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " with ";
            stmt += (trigger.ACTIVE == 'true' ? "active" : "inactive") + ",";
            stmt += "condition = " + (!trigger.CONDITION ? "none" : "'" + this.toolbox.textQuote(trigger.CONDITION) + "'") + ",";
            stmt += (this.isTrue(trigger.MASTER) ? "master" : "nomaster") + ",";
            if (this.isTrue(trigger.MASTER)) {
                stmt += "group = '" + trigger.SUBMIT_OWNER + "',"
            }
            stmt += (trigger.WARN == "true" ? "warn" : "nowarn") + ",";
            stmt += "limit state = " + (trigger.LIMIT_STATE != "" && trigger.LIMIT_STATE != null ? trigger.LIMIT_STATE : "none") + ",";
            stmt += (this.isTrue(trigger.SUSPEND) ? "suspend" : "nosuspend") + ",";
            if (this.isTrue(trigger.SUSPEND)) {
                if (trigger.RESUME_TYPE == "AT") {
                    stmt += "resume at '" + trigger.RESUME_AT + "',";
                } else if (trigger.RESUME_TYPE == "IN") {
                    //YYYY-MM-DDTHH:MI:SS
                    stmt += "resume in " + trigger.RESUME_IN + " " + trigger.RESUME_BASE + ",";
                } else {
                    stmt += "noresume,";
                }
            }
            if (trigger.TRIGGER_TYPE != 'UNTIL FINISHED') {
                stmt += "state = " + (!trigger.STATES ? 'none' : "(" + this.toolbox.quoteSeperatedString(trigger.STATES, ',') + ")") + ",";
            }
            stmt += "parameters = " + (!trigger.TRIGGER_PARAMETERS ? 'none' : "(" + trigger.TRIGGER_PARAMETERS + ")") + ","
            stmt += "submitcount = " + (trigger.MAX_RETRY ? trigger.MAX_RETRY : "0") + ","
            stmt += "submit " + this.toolbox.namePathQuote(this.fixupPath(trigger.SUBMIT_NAME, obj.PATH, path)) + ","
            if (['UNTIL FINAL', 'UNTIL FINISHED'].includes(trigger.TRIGGER_TYPE)) {
                stmt += 'check = ' + trigger.CHECK_AMOUNT + ' ' + trigger.CHECK_BASE + ',';
            }
            stmt += "type = " + trigger.TRIGGER_TYPE
            stmts.push(stmt);
        }
        for (let triggerName of tmpRenames) {
            stmts.push("rename trigger '" + triggerName + "_tmpRename' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " to '" + triggerName + "'");
        }
        // Drop Triggers_by that were deleted from the table.
        foundTrigger = false;
        for (let o_trigger of obj.ORIGINAL_TRIGGERED_BY.TABLE) {
            if (o_trigger.IS_INVERSE == "false" || o_trigger.IS_INVERSE == false) {
                continue;
            }
            foundTrigger = false;
            for (let trigger of obj.TRIGGERED_BY.TABLE) {
                if (trigger.ID == undefined || trigger.ID == "" || trigger.ID == null || o_trigger.ID == trigger.ID) {
                    foundTrigger = true;
                    break;
                }
            }
            if (!foundTrigger) {
                stmts.push(" drop trigger " + o_trigger.ID);
            }
        }

        // alter or create triggered_by
        tmpRenames = [];
        for (let trigger of obj.TRIGGERED_BY.TABLE) {
            // console.log(trigger)
            if (trigger.IS_INVERSE == "false" || trigger.IS_INVERSE == false) {
                continue;
            }
            trigger.NAME = this.convertIdentifier(trigger.NAME, trigger.ORIGINAL_NAME);
            let tmpName = trigger.NAME;
            if (trigger.ORIGINAL_NAME && trigger.NAME != trigger.ORIGINAL_NAME) {
                tmpName = trigger.NAME + "_tmpRename";
                tmpRenames.push(trigger.NAME);
                stmts.push("rename trigger '" + trigger.ORIGINAL_NAME + "' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " to '" + tmpName + "'");
            }
            let stmt : string ="create or alter trigger '" + tmpName + "' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " inverse with ";
            stmt += (trigger.ACTIVE == 'true' ? "active" : "inactive") + ",";
            stmt += "condition = " + (!trigger.CONDITION ? "none" : "'" + trigger.CONDITION + "'") + ",";
            stmt += (this.isTrue(trigger.MASTER) ? "master" : "nomaster") + ",";
            if (this.isTrue(trigger.MASTER)) {
                stmt += "group = '" + trigger.SUBMIT_OWNER + "',"
            }
            stmt += (trigger.WARN == "true" ? "warn" : "nowarn") + ",";
            stmt += "limit state = " + (trigger.LIMIT_STATE != "" && trigger.LIMIT_STATE != null ? trigger.LIMIT_STATE : "none") + ",";
            stmt += (this.isTrue(trigger.SUSPEND) ? "suspend" : "nosuspend") + ",";
            if (this.isTrue(trigger.SUSPEND)) {
                if (trigger.RESUME_TYPE == "AT") {
                    stmt += "resume at '" + trigger.RESUME_AT + "',";
                } else if (trigger.RESUME_TYPE == "IN") {
                    //YYYY-MM-DDTHH:MI:SS
                    stmt += "resume in " + trigger.RESUME_IN + " " + trigger.RESUME_BASE + ",";
                } else {
                    stmt += "noresume,";
                }
            }
            if (trigger.TRIGGER_TYPE != 'UNTIL FINISHED') {
                stmt += "state = " + (!trigger.STATES ? 'none' : "(" + this.toolbox.quoteSeperatedString(trigger.STATES, ',') + ")") + ",";
            }
            stmt += "parameters = " + (!trigger.TRIGGER_PARAMETERS ? 'none' : "(" + trigger.TRIGGER_PARAMETERS + ")") + ","
            stmt += "submitcount = " + (trigger.MAX_RETRY ? trigger.MAX_RETRY : "0") + ","
            stmt += "submit " + this.toolbox.namePathQuote(this.fixupPath(trigger.OBJECT_NAME, obj.PATH, path)) + ","
            if (['UNTIL FINAL', 'UNTIL FINISHED'].includes(trigger.TRIGGER_TYPE)) {
                stmt += 'check = ' + trigger.CHECK_AMOUNT + ' ' + trigger.CHECK_BASE + ',';
            }
            stmt += "type = " + trigger.TRIGGER_TYPE;
            stmts.push(stmt);
        }
        for (let triggerName of tmpRenames) {
            stmts.push("rename trigger '" + triggerName + "_tmpRename' on job definition " + this.toolbox.namePathQuote(path + "." + obj.NAME) + " to '" + triggerName + "'");
        }
        return stmts;
    }

    private isTrue(stmt: any): boolean {
        return stmt != undefined && stmt != null && (stmt == "true" || stmt == true);
    }

    private generateTriggerStatesString(arr: any): string {
        let resString: string;
        if (arr != undefined && arr != null && arr.length > 0) {
            let resString = "";
            for (let i = 0; i < arr.length; i++) {
                resString += "'" + arr[i]['EXIT_STATE'] + "'";
                resString += i == arr.length - 1 ? "" : ",";
            }

            return resString != "''" ? resString : 'NONE';
        } else { return 'NONE'; }
    }

    private parameterJoin(parameters: any): string {
        // console.log(parameters)
        let resString: string;
        if (parameters != undefined && parameters != null && parameters.length > 0) {
            let resString = "";
            for (let i = 0; i < parameters.length; i++) {
                resString += "'" + parameters[i]["NAME"] + "' = '" + parameters[i]["EXPRESSION"] + "'";
                resString += i == parameters.length - 1 ? "" : ",";
            }

            return resString != "''" ? resString : "none";
        } else { return "none"; }
    }

    custom(params: any, bicsuiteObject: any, tabInfo?: Tab) {
        // console.log(bicsuiteObject);

        if (tabInfo != undefined) {

            if (params.CLICK_METHOD == "OPEN_PARENT") {
                let tab = new Tab(bicsuiteObject.PARENTNAME.split(".").pop(), bicsuiteObject.PARENT_ID, tabInfo.TYPE, tabInfo.TYPE.toLowerCase(), TabMode.EDIT);
                this.eventEmitterService.createTab(tab, true);
            }
            else if (params.CLICK_METHOD == "OPEN_CHILD") {
                let tab = new Tab(bicsuiteObject.CHILDNAME.split(".").pop(), bicsuiteObject.CHILD_ID, tabInfo.TYPE, tabInfo.TYPE.toLowerCase(), TabMode.EDIT);
                this.eventEmitterService.createTab(tab, true);
            }
        }
    }

    default(tabInfo: Tab) {
        let bicsuiteObj = super.default(tabInfo);
        let record = bicsuiteObj.DATA.RECORD;
        // let
        // console.log(bicsuiteObj)
        let isStatic: string = tabInfo.DATA.IN_CONVENTION_FOLDER_TYPE != "JOB" ? "true" : "false";

        record.RESTART_MODE = "NO";
        record.TYPE = tabInfo.TYPE;
        record.IN_CONVENTION_FOLDER = tabInfo.DATA.IN_CONVENTION_FOLDER;
        record.IN_CONVENTION_FOLDER_TYPE = tabInfo.DATA.IN_CONVENTION_FOLDER_TYPE;
        record.CREATE_BATCH_FOLDER = tabInfo.TYPE == "BATCH" ? "true" : "false";
        record.RENAME_BATCH_FOLDER = "true";
        record.ADD_CHILD = "true";
        record.STATIC = isStatic;
        record.INHERIT_GRANTS = "ALL";
        record.AGING_BASE = "MINUTE";
        record.OWNER = this.mainInformationService.getUser().defaultGroup;
        record.ENV_NAME = this.mainInformationService.getPrivateProperty("DEFAULT_ENVIRONMENT", "");
        record.ESP_NAME = this.mainInformationService.getPrivateProperty("DEFAULT_EXIT_STATE_PROFILE", "");
        record.PATH = tabInfo.PATH;
        record.FULLNAME = this.toolbox.namePathQuote(tabInfo.PATH + '.');
        record.NAME = '';
        record.ORIGINAL_NAME = '';
        record.LOGFILE = this.mainInformationService.getPrivateProperty("DEFAULT_LOGFILE", "") ? this.mainInformationService.getPrivateProperty("DEFAULT_LOGFILE", "") : '${JOBID}.log';
        record.ERRLOGFILE = this.mainInformationService.getPrivateProperty("DEFAULT_ERROR_LOGFILE", "") ? this.mainInformationService.getPrivateProperty("DEFAULT_ERROR_LOGFILE", "") : '${JOBID}.log';
        record.DEFINED_RESOURCES = {
            TABLE: []
        };
        record.TRIGGER_SYS_RESTART_STATES = {
            TABLE: [
                {
                    "FROM_STATE": null,
                    "ID": "",
                    "TO_STATE": ""
                }
            ],
            DESC: [
                "ID",
                "FROM_STATE",
                "TO_STATE"
            ]
        };

        let approvalOperations = this.getApprovalOperations();
        let approvals : any[] = [];
        let translatorService = new TranslateService (new LocalStorageHelper(this.eventEmitterService));
        for (let approvalOperation of approvalOperations) {
            approvals.push({
                "OPERATOR_ACTION" : translatorService.translate(approvalOperation.LABEL),
                "APPROVAL" : "DEFAULT",
                "LEADING" : "false"
            });
        }
        record.APPROVALS = { "TABLE" : approvals };

        return bicsuiteObj;
    }

    submitjob(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NOFORM_ALL_PARAMETERS = this.toolbox.deepCopy(obj.PARAMETER);
        let bicsuiteObject = this.toolbox.deepCopy(obj);
        if (bicsuiteObject.hasOwnProperty('PARAMETER')) {
            if (bicsuiteObject.PARAMETER.TABLE.length > 0) {
                for (var i = 0; i < bicsuiteObject.PARAMETER.TABLE.length; i++) {
                    if (this.conditionParameterIsNotVisibleInSubmit(obj, bicsuiteObject.PARAMETER.TABLE[i], tabInfo)) {
                        bicsuiteObject.PARAMETER.TABLE.splice(i, 1);
                        i--;
                    }
                }
                bicsuiteObject.PARAMETER.TABLE.map((obj: any) => {
                    obj.EXPRESSION == 'NONE' ? obj.EXPRESSION = obj.DEFAULT_VALUE : '';
                    return obj;
                });
            }
        }
        let comments: string = '';
        if (bicsuiteObject.hasOwnProperty('COMMENT')) {
            for (let commentRow of (bicsuiteObject.COMMENT.TABLE)) {
                comments += commentRow.DESCRIPTION + '\n';
            }
        }
        // let submitSuspended: string = 'NO';
        // bicsuiteObject.SUBMIT_SUSPENDED == 'false' ? submitSuspended = 'NO' : '';
        // bicsuiteObject.SUBMIT_SUSPENDED == 'true' ? submitSuspended = 'YES' : '';
        // // bicsuiteObject.SUBMIT_SUSPENDED == 'ADMINSUSPEND' ? bicsuiteObject.SUBMIT_SUSPENDED = 'NO' : '';

        //YYYY-MM-DDTHH:MI:SS
        let resumeTime = new Date().toISOString().slice(0, 19);

        // YYYY-MM-DDTHH:MI:SS
        let dataObj: any = {
            PRIVS: 'E',
            ID: bicsuiteObject.ID,
            TYPE: tabInfo.TYPE,
            NAME: bicsuiteObject.ORIGINAL_NAME,
            PATH: bicsuiteObject.PATH + '.' + bicsuiteObject.ORIGINAL_NAME,
            GROUP: bicsuiteObject.OWNER,
            SUBMIT_GROUP: this.privilegesService.getDefaultGroup(),
            TIME_ZONE: this.customPropertiesService.getCustomObjectProperty('defaultTimeZone'),
            RESUME_TYPE: 'NONE',
            RESUME_TIME: resumeTime,
            RESUME_IN: '1',
            RESUME_IN_UNIT: 'MONTH',
            SUBMIT_SUSPENDED: "JOBSUSPEND",
            CHECK_ONLY: 'false',
            ON_UNRESOLVE_ERROR: "ERROR",
            JOB_DESCRIPTION: comments,
            PARAMETER: bicsuiteObject.PARAMETER,
            NOFORM_ALL_PARAMETERS: bicsuiteObject.NOFORM_ALL_PARAMETERS
        };

        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(submitDialog_editorform, dataObj, tabInfo, "Submit " + tabInfo.TYPE + " " + (tabInfo != undefined ? tabInfo.NAME : ""), this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "800px"
        });
        // * Note, dialogs unsubsrcibe autoamtically!
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result) {
                if (result.hasOwnProperty('ERROR')) {
                    return 1;
                } else {
                    if (obj.TYPE == 'BATCH') {
                        let monDetailTab = new Tab(obj.NAME, result.DATA.RECORD.ID.toString(), 'MONITOR DETAIL', 'detail', TabMode.MONITOR_DETAIL)
                        let bm : any = this.bookmarkService.getBookmarkOfNameAndType('DEFAULT', 'DETAIL');
                        let parameter : any;
                        for (parameter of obj.PARAMETER.TABLE) {
                                if (parameter.NAME == 'DETAIL_BOOKMARK') {
                                    bm = this.bookmarkService.getBookmarkOfNameAndType(parameter.DEFAULT_VALUE, 'DETAIL');
                                    break;
                                }
                        }
                        monDetailTab.BOOKMARK = bm;
                        this.eventEmitterService.createTab(monDetailTab, true);                
                    }
                    else {
                        let tab = new Tab(bicsuiteObject.ORIGINAL_NAME, result.DATA.RECORD.ID.toString() /* node.id.toString() */, 'SUBMITTED ENTITY', 'SUBMITTED_ENTITY'.toLowerCase(), TabMode.EDIT);
                        tab.PATH = result.PATH;
                        this.eventEmitterService.createTab(tab, true);
                    }
                    return 0;
                }
                // });
            }
            return 1;
        });
    }

    submitSuspendedStatement(bicsuiteObject: any): string {
        let stmt = '';
        if (bicsuiteObject.SUBMIT_SUSPENDED == 'YES') {
            stmt += ' SUSPEND'
            if (bicsuiteObject.RESUME_TYPE == 'NONE') {
                stmt += ',';
            } else if (bicsuiteObject.RESUME_TYPE == 'AT') {
                stmt += ', RESUME AT ' + "'" + bicsuiteObject.RESUME_TIME + "',";
            } else if (bicsuiteObject.RESUME_TYPE == 'IN') {
                //YYYY-MM-DDTHH:MI:SS
                stmt += ', RESUME IN ' + bicsuiteObject.RESUME_IN + ' ' + bicsuiteObject.RESUME_IN_UNIT + ',';
            }
        }
        if (bicsuiteObject.SUBMIT_SUSPENDED == 'NO') {
            stmt += ' NOSUSPEND,'
        }
        return stmt;
    }

    submitParameterStatement(bicsuiteObject: any): string {
        // console.log(bicsuiteObject)
        let stmt = '';
        if (bicsuiteObject.hasOwnProperty('PARAMETER')) {

            if (bicsuiteObject.PARAMETER.TABLE != undefined || bicsuiteObject.PARAMETER.TABLE.length != 0) {
                stmt += ', parameters = ('
                let count = 0;
                for (let o of bicsuiteObject.PARAMETER.TABLE) {
                    count++;
                    stmt += "'" + o.NAME + "' = '" + this.toolbox.textQuote(o.SUBMIT_VALUE) + "',"
                }
                stmt = stmt.slice(0, -1);
                stmt += ')'
                count == 0 ? stmt = '' : '';
            }
        }
        return stmt;
    }

    openHierarchy(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {

        let hierarchyTab = new Tab('Hierarchy ' + bicsuiteObject.ORIGINAL_NAME, 'Hierarchy ' + bicsuiteObject.ORIGINAL_NAME, 'HIERARCHY'.toUpperCase(), 'hierarchy', TabMode.HIERARCHY);
        hierarchyTab.DATA = {}
        hierarchyTab.DATA.PATH = bicsuiteObject.PATH;
        hierarchyTab.DATA.ORIGINAL_NAME = bicsuiteObject.ORIGINAL_NAME;
        // this.data.PATH + '.' + this.data.ORIGINAL_NAME
        this.eventEmitterService.createTab(hierarchyTab, true);
        return Promise.resolve({});

    }

    openSchedules(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let schedulesTab = new Tab(bicsuiteObject.ORIGINAL_NAME, '', 'SCHEDULES', 'interval', TabMode.EDIT);
        schedulesTab.NUMERIC_ID = bicsuiteObject.ID;
        schedulesTab.SE_OWNER = bicsuiteObject.OWNER;
        schedulesTab.SE_TYPE = bicsuiteObject.TYPE;
        schedulesTab.OWNER = bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME;
        schedulesTab.PRIVS = bicsuiteObject.PRIVS;
        this.eventEmitterService.createTab(schedulesTab, true);
        return Promise.resolve({});
    }

    submitJobDefinition(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        // console.log(bicsuiteObject)
        // if check only is true the error is displayed in the dialog
        // TODOS ENNO check if all errors from the submit should be displayed there
        // console.log(bicsuiteObject)
        let doNotThrow = (bicsuiteObject.CHECK_ONLY == 'true');
        let stmt: string = 'submit ' + this.toolbox.namePathQuote(bicsuiteObject.PATH)
            + ' with'
            + this.submitSuspendedStatement(bicsuiteObject)
            + ' unresolved = ' + bicsuiteObject.ON_UNRESOLVE_ERROR
            + ", group = '" + bicsuiteObject.SUBMIT_GROUP + "'"
            + ", time zone = '" + bicsuiteObject.TIME_ZONE + "'"
            + this.submitParameterStatement(bicsuiteObject)
            + ((bicsuiteObject.CHECK_ONLY == 'true') ? ', check only ' : '');

        // console.log(doNotThrow)

        return this.commandApi.execute(stmt).then((result1: any) => {
            if (result1.ERROR == undefined && bicsuiteObject.CHECK_ONLY != 'true') {
                result1.FEEDBACK = 'Job ' + result1.DATA.RECORD.ID + ' submitted';
                bicsuiteObject.SUBMITTED_ID = result1.DATA.RECORD.ID;
            }
            return result1;
        });
    }

    testSubmitJobDefinition(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        bicsuiteObject.CHECK_ONLY = 'true';
        return this.submitJobDefinition(bicsuiteObject, tabInfo, editorForm)
    }

    openLastAndClose(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        if (bicsuiteObject.TYPE == 'JOB') {
            let tab = new Tab(bicsuiteObject.NAME, bicsuiteObject.SUBMITTED_ID, 'SUBMITTED ENTITY', 'SUBMITTED_ENTITY'.toLowerCase(), TabMode.EDIT);
            tab.PATH = bicsuiteObject.PATH;
            this.eventEmitterService.createTab(tab, true);
        }
        else {
            let monDetailTab = new Tab(bicsuiteObject.NAME, bicsuiteObject.SUBMITTED_ID, 'MONITOR DETAIL', 'detail', TabMode.MONITOR_DETAIL)
            let bm : any = this.bookmarkService.getBookmarkOfNameAndType('DEFAULT', 'DETAIL');
            let parameter : any;
            for (parameter of bicsuiteObject.NOFORM_ALL_PARAMETERS.TABLE) {
                    if (parameter.NAME == 'DETAIL_BOOKMARK') {
                        bm = this.bookmarkService.getBookmarkOfNameAndType(parameter.DEFAULT_VALUE, 'DETAIL');
                        break;
                    }
            }
            monDetailTab.BOOKMARK = bm;
            this.eventEmitterService.createTab(monDetailTab, true);    
        }
        return Promise.resolve(0);
    }

    getCustomProperty(obj: any): string {
        if (typeof obj === "object") {
            if (obj.hasOwnProperty("KEY")) {
                let paramDict = this.customPropertiesService.getCustomObjectProperty("paramDict");
                if (paramDict) {
                    if (paramDict.hasOwnProperty(obj["KEY"])) {
                        return paramDict[obj["KEY"]];
                    }
                    else {
                        console.warn("config.json property 'paramDict' does not contain key " + obj["KEY"]);
                    }
                } else {
                    console.warn("config.json has no property 'paramDict'");
                }
            }
        }
        return obj;
    }

    saveParameterComments(bicsuiteObject: any, tabInfo: Tab, deepCopyBicsuiteObject: any): Promise<Object> {
        let stmt;
        if (deepCopyBicsuiteObject.COMMENT.TABLE.length > 0) {
            stmt = "create or alter comment on parameter '" + bicsuiteObject.NAME + "' of job definition " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
            let withStmt = []
            for (let c of deepCopyBicsuiteObject.COMMENT.TABLE) {
                c.TAG = c.TAG ? c.TAG : '';
                let desc = c.DESCRIPTION ? this.toolbox.textQuote(c.DESCRIPTION) : '';
                withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'")
            }
            stmt += withStmt.join(',')
        }
        else {
            stmt = "drop comment on parameter '" + bicsuiteObject.NAME + "' of job definition " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        }
        // console.log(stmt)
        return this.commandApi.execute(stmt).then((response: any) => {
            if (response.hasOwnProperty("FEEDBACK")) {
                this.eventEmitterService.pushSnackBarMessage([response.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);
            }
            return response;
        });
    }

    openParameterReferencePath(bicsuiteObject: any, tabInfo: Tab, deepCopyBicsuiteObject: any): Promise<Object> {
        let rp = bicsuiteObject.REFERENCE_PATH.split(".");
        let tab = new Tab(rp.pop(), bicsuiteObject.REFERENCE_ID, bicsuiteObject.REFERENCE_TYPE, bicsuiteObject.REFERENCE_TYPE.toLowerCase(), TabMode.EDIT);
        tab.PATH = rp.join(".");
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    filterStatesBasedOnTriggerType(editorformField: any, bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): any {
        if (['UNTIL FINAL', 'AFTER FINAL', 'BEFORE FINAL'].includes(parentBicsuiteObject.TRIGGER_TYPE)) {
            if (Array.isArray(bicsuiteObject)) {
                bicsuiteObject = bicsuiteObject.filter(state => state.TYPE == 'FINAL');
                // console.log(bicsuiteObject);
                return bicsuiteObject;
            }
        }
        return bicsuiteObject;
    }

    filterStatesBasedOnResourceStateProfileID(editorformField: any, bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): any {
        if (Array.isArray(bicsuiteObject)) {
            bicsuiteObject = bicsuiteObject.filter(resourceState => parentBicsuiteObject.ALLOWED_RESOURCE_STATES.includes(resourceState.NAME));
        }
        return bicsuiteObject;
    }

    filterStatesBasedOnDependencyStateSelection(editorformField: any, bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): any {
        if (['FINAL'].includes(parentBicsuiteObject.STATE)) {
            if (Array.isArray(bicsuiteObject)) {
                bicsuiteObject = bicsuiteObject.filter(state => state.TYPE == 'FINAL');
                return bicsuiteObject;
            }
        }
        return bicsuiteObject;
    }

    editJobTriggerParams(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let parameters: any[] = []
        if (bicsuiteObject.TRIGGER_PARAMETERS) {
            for (let parameter of bicsuiteObject.TRIGGER_PARAMETERS.split(',')) {

                let parameterArray = parameter.split('=')
                let name = this.toolbox.deQuote(parameterArray[0].trim());
                let expression = this.toolbox.deQuote(parameterArray[1].trim());

                parameters.push({
                    "NAME": name ? name : '',
                    "EXPRESSION": expression ? expression : ''
                })
            }
        }
        let dataObj: any = {
            "PARAMETERS": { "TABLE": parameters }
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(
                editJobTriggerParameter_editorform, dataObj, tabInfo, "Edit Trigger Parameters", this),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            let parameters: any[] = []
            if (result != undefined || result != null && result) {
                for (let row of dataObj.PARAMETERS.TABLE) {
                    parameters.push("'" + row.NAME.trim() + "'='" + this.toolbox.textQuote(row.EXPRESSION).trim() + "'");
                }
                let triggerParameters = parameters.join(', ');
                if (bicsuiteObject.TRIGGER_PARAMETERS != triggerParameters) {
                    bicsuiteObject.TRIGGER_PARAMETERS = triggerParameters;
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                }
            }
            return 1;
        });

    };

    openConditionDialog(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let dataObj: any = {
            CONDITION: bicsuiteObject.CONDITION
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editJobTriggerCondition_editorform, dataObj, tabInfo, "Edit Dependency Condition", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null) {
                if (bicsuiteObject.CONDITION != dataObj.CONDITION) {
                    bicsuiteObject.CONDITION = dataObj.CONDITION;
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                }
            }
            return 1;
        });
    };
   
    openSelectConditionDialog(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let dataObj: any = {
            CONDITION: bicsuiteObject.SELECT_CONDITION
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editJobTriggerCondition_editorform, dataObj, tabInfo, "Edit Select Condition", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null) {
                if (bicsuiteObject.SELECT_CONDITION != dataObj.CONDITION) {
                    bicsuiteObject.SELECT_CONDITION = dataObj.CONDITION;
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                }
            }
            return 1;
        });
    };

    openChildConditionDialog(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        // console.log(parentBicsuiteObject)
        // console.log(bicsuiteObject)


        let dataObj: any = {
            CONDITION: bicsuiteObject.ENABLE_CONDITION
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editJobTriggerCondition_editorform, dataObj, tabInfo, "Edit Child Condition", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                if (result != undefined || result != null) {
                    if (bicsuiteObject.ENABLE_CONDITION != dataObj.CONDITION) {
                        bicsuiteObject.ENABLE_CONDITION = dataObj.CONDITION;
                        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    }
                }
                }
            return 1;
        });
    };

    loadResourceStateProfile(name: string) {
        console.warn("NO RESOURCE STATE PROFILE AVAILABLE; TODO") // should not happen
        if (!name) {
            let result = {
                DATA: {
                    RECORD: {
                        STATES: {
                            TABLE: []
                        }
                    }
                }
            }
            return Promise.resolve(result);
        }
        return this.commandApi.execute("SHOW RESOURCE STATE PROFILE " + name).then(result => {
            return result;
        });
    }

    openJobRequiredResourceStates(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let states: any[] = [];
        if (bicsuiteObject.STATES) {
            for (let state of bicsuiteObject.STATES.split(',')) {
                states.push({
                    "RESOURCE_STATE": state
                })
            }
        }

        let allowedResourceStates: string[] = [];

        return this.loadResourceStateProfile(bicsuiteObject.RESOURCE_STATE_PROFILE_NAME).then((result: any) => {
            // console.log(result.DATA.RECORD.STATES.TABLE);
            for (let o of result.DATA.RECORD.STATES.TABLE) {
                allowedResourceStates.push(o.RSD_NAME);
            }
            let dataObj: any = {
                "ALLOWED_RESOURCE_STATES": allowedResourceStates,
                "RESOURCE_STATES": { "TABLE": states },
            };
            if (bicsuiteObject.PRIVS) {
                dataObj.PRIVS = bicsuiteObject.PRIVS;
            }
    
            let data: EditorformDialogData = new EditorformDialogData(editJobRequiredResourceState_editorform, dataObj, tabInfo, "Edit Resource States", this);
            data.parentBicsuiteObject = parentBicsuiteObject;
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: data,
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });

            return dialogRef.afterClosed().toPromise().then((result: any) => {
                let states = [];
                if (result != undefined || result != null && result) {
                    for (let row of dataObj.RESOURCE_STATES.TABLE) {
                        if (row.RESOURCE_STATE) {
                            states.push(row.RESOURCE_STATE)
                        }
                    }
                    let resourceStates = states.join(', ');
                    if (bicsuiteObject.STATES != resourceStates) {
                        bicsuiteObject.STATES = resourceStates;
                        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    }
                }
                return 1;
            });
        });
    }

    openJobDependencyStateSelection(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let states: any[] = [];
        if (bicsuiteObject.STATES) {
            for (let state of bicsuiteObject.STATES.split(',')) {
                let exitState = state.split(':')[0] ? state.split(':')[0].replace(' ', '') : undefined
                let condition = state.split(':')[1]
                if (condition) {
                    condition = condition.trim()
                }
                states.push({
                    "EXIT_STATE": exitState,
                    "CONDITION": condition ? condition : ''
                })
            }
        }

        // SHOW job bicsuitobject.REQUIREDNAME to get ESP from selected Job
        let showJobStmt = 'SHOW JOB DEFINITION ' + this.toolbox.namePathQuote(bicsuiteObject.REQUIREDNAME);
        return this.commandApi.execute(showJobStmt).then((response: any) => {
            if (response.FEEDBACK) {
                let record = response.DATA.RECORD;
                let dataObj: any = {
                    "ESP_NAME": record.ESP_NAME,
                    "STATE": bicsuiteObject.STATE_SELECTION,
                    "EXIT_STATES": { "TABLE": states },
                };
                if (bicsuiteObject.PRIVS) {
                    dataObj.PRIVS = bicsuiteObject.PRIVS;
                }
                        let data: EditorformDialogData = new EditorformDialogData(editJobDependencyStates_editorform, dataObj, tabInfo, "Edit Job Dependency States", this);
                data.parentBicsuiteObject = parentBicsuiteObject;
                const dialogRef = this.dialog.open(EditorformDialogComponent, {
                    data: data,
                    panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                    width: "1000px"
                });
                return dialogRef.afterClosed().toPromise().then((result: any) => {
                    // console.log(result)
                    let states = [];
                    if (result != undefined || result != null && result) {
                        for (let row of dataObj.EXIT_STATES.TABLE) {
                            states.push(row.EXIT_STATE + (row.CONDITION ? ':' + row.CONDITION : ''))
                        }
                        let dependencyStates = states.join(', ');
                        if (bicsuiteObject.STATES != dependencyStates) {
                            bicsuiteObject.STATES = dependencyStates;
                            this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                        }
                    }
                    return 1;
                });
            } else {
                return 1;
            }
        });
    };

    editJobTriggerStates(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let states = [];
        if (bicsuiteObject.STATES) {
            for (let state of bicsuiteObject.STATES.split(',')) {
                state = state.replace(' ', '')
                states.push({ "EXIT_STATE": state })
            }
        }
        let dataObj: any = {
            "TRIGGER_TYPE": bicsuiteObject.TRIGGER_TYPE,
            "ESP_NAME": parentBicsuiteObject.ESP_NAME,
            "EXIT_STATES": { "TABLE": states }
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        let data: EditorformDialogData = new EditorformDialogData(editJobTriggerStates_editorform, dataObj, tabInfo, "Edit Trigger Exitstates", this);
        data.parentBicsuiteObject = parentBicsuiteObject;
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: data,
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            let states = [];
            if (result != undefined || result != null && result) {
                for (let row of dataObj.EXIT_STATES.TABLE) {
                    states.push(row.EXIT_STATE)
                }
                let triggerStates = states.join(', ');
                if (bicsuiteObject.STATES != triggerStates) {
                    bicsuiteObject.STATES = triggerStates;
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                }
            }
            return 1;
        });
    };

    editJobTriggerCondition(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let dataObj: any = {
            CONDITION: bicsuiteObject.CONDITION
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editJobTriggerCondition_editorform, dataObj, tabInfo, "Edit Trigger Condition", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                if (bicsuiteObject.CONDITION != dataObj.CONDITION) {
                    bicsuiteObject.CONDITION = dataObj.CONDITION;
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                }
            }
            return 1;
        });
    };

    editParameterComment(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show comment on parameter '" + bicsuiteObject.NAME + "' of job definition " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        return this.commandApi.execute(stmt).then((response1: any) => {
            // console.log(response1)
            for (let o of response1.DATA.TABLE) {
                o.DESCRIPTION = o.COMMENT
            }
            let dataObj: any = {
                COMMENT: {
                    TABLE: response1.DATA.TABLE
                },
                NAME: bicsuiteObject.NAME
            };
            if (bicsuiteObject.PRIVS) {
                dataObj.PRIVS = bicsuiteObject.PRIVS;
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editJobParameterComment_editorform, dataObj, tabInfo, "Edit Parameter Comments", this),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                // Update Original Object
                let visibleComment = []
                for (let c of result.COMMENT.TABLE) {
                    if (c.TAG) {
                        visibleComment.push(c.TAG + "\n\n" + c.DESCRIPTION)
                    }
                    else {
                        visibleComment.push(c.DESCRIPTION)
                    }
                }
                bicsuiteObject.NOFORM_COMMENT = visibleComment.join('\n\n').trim();
                return result;
            });
        });
    }

    onChangeParameterDefault(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.DEFAULT != 'true' && bicsuiteObject.DEFAULT_VALUE) {
            bicsuiteObject.DEFAULT_VALUE = '';
            rootForm.get('PARAMETER').get('DEFAULT_VALUE').get(''+ bicsuiteObject.rowIndex).setValue(bicsuiteObject.DEFAULT_VALUE); 
            this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        }
        return Promise.resolve({});
    }

    onChangeParameterDefaultValue(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.DEFAULT_VALUE && bicsuiteObject.DEFAULT != 'true') {
            bicsuiteObject.DEFAULT = 'true';
            let p = rootForm.get('PARAMETER');
            rootForm.get('PARAMETER').get('DEFAULT').get('' +bicsuiteObject.rowIndex).setValue(bicsuiteObject.DEFAULT); 
            this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        }
        return Promise.resolve({});
    }

    manipulateFormField(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        // console.log(bicsuiteObject);
        if (bicsuiteObject.hasOwnProperty("WEBCONFIG")) {
            if (bicsuiteObject.WEBCONFIG.hasOwnProperty("TYPE")) {
                editorForm.TYPE = bicsuiteObject.WEBCONFIG.TYPE;
                switch (editorForm.TYPE) {
                    case "CHECKBOX":
                        if (bicsuiteObject.WEBCONFIG.hasOwnProperty("TICKVALUE")) {
                            editorForm.ONVALUE = this.getCustomProperty(bicsuiteObject.WEBCONFIG.TICKVALUE);
                        }
                        if (bicsuiteObject.WEBCONFIG.hasOwnProperty("ONVALUE")) {
                            editorForm.ONVALUE = this.getCustomProperty(bicsuiteObject.WEBCONFIG.ONVALUE);
                        }
                        if (bicsuiteObject.WEBCONFIG.hasOwnProperty("OFFVALUE")) {
                            editorForm.OFFVALUE = this.getCustomProperty(bicsuiteObject.WEBCONFIG.OFFVALUE);
                        }
                        break;
                    case "SELECT":
                        if (bicsuiteObject.WEBCONFIG.hasOwnProperty("VALUES")) {
                            editorForm.VALUES = this.getCustomProperty(bicsuiteObject.WEBCONFIG.VALUES);
                        }
                        if (bicsuiteObject.WEBCONFIG.hasOwnProperty("LABELS")) {
                            editorForm.LABELS = this.getCustomProperty(bicsuiteObject.WEBCONFIG.LABELS);
                        }
                        break;
                }
                if (bicsuiteObject.DEFAULT == "false") {
                    // editorForm.MANDATORY = true;
                    if (bicsuiteObject.WEBCONFIG.hasOwnProperty("DEFAULT")) {
                        editorForm.DEFAULT = this.getCustomProperty(bicsuiteObject.WEBCONFIG.DEFAULT);
                        if (bicsuiteObject.DEFAULT == "false") {
                            bicsuiteObject[editorForm.NAME] = this.getCustomProperty(bicsuiteObject.WEBCONFIG.DEFAULT);
                        }
                    }
                }
            }
        }
        // else {
        //     if (bicsuiteObject.DEFAULT == "false") {
        //         editorForm.MANDATORY = true;
        //     }
        // }
        // console.log(editorForm);
        return Promise.resolve({});
    }

    changetoedit(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let newTab : Tab = tabInfo.getCopy();
        newTab.VERSIONED = false;
        newTab.ID = bicsuiteObject.ID;
        newTab.TYPE = bicsuiteObject.TYPE;
        this.eventEmitterService.createTab(newTab, true);
        // if already exit in tabs no refresh but move to the tab
        return Promise.resolve({});
    };

    manipulateParameterResourceChooserFormField(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        // console.log(bicsuiteObject)
        // TODO RESET RESOURCE FIELD WHEN CHANGED ?

        switch (bicsuiteObject.TYPE) {
            case 'CHILDREFERENCE': {
                // console.log('CHILDREFERENCE');
                editorForm.CHOOSER = "ParameterTreeChooser";
                break;
            }
            case 'REFERENCE': {
                // console.log('REFERENCE');
                editorForm.CHOOSER = "ParameterTreeChooser";
                break;
            }
            case 'RESOURCEREFERENCE': {
                // console.log('RESOURCEREFERENCE');
                editorForm.CHOOSER = "ResourceParameterTreeChooser";
                break;
            }
            default: {
                //statements;
                break;
            }
        }

        return Promise.resolve({});
    }

    appendTrigger(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        row.SUBMIT_OWNER = this.privilegesService.getDefaultGroup();
        return Promise.resolve({});
    }

    appendChild(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        if (bicsuiteObject.TYPE == "JOB") {
            row.IS_STATIC = "false";
        }
        else {
            row.IS_STATIC = "true"
        }
        return Promise.resolve({});
    }

    createDefinedResource(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let definedResourceTab = new Tab('*untitled', new Date().getTime().toString(), 'RESOURCE'.toUpperCase(), 'resource', TabMode.NEW);
        definedResourceTab.DATA = {};
        definedResourceTab.DATA.ID = bicsuiteObject.ID;
        definedResourceTab.DATA.ID = bicsuiteObject.ID;
        definedResourceTab.DATA.TYPE = bicsuiteObject.TYPE;
        definedResourceTab.DATA.f_IsInstance = false;
        definedResourceTab.DATA.f_IsInstance = false;
        definedResourceTab.DATA.f_isScopeOrFolderInstance = false;
        definedResourceTab.OWNER = bicsuiteObject.PATH + '.' + bicsuiteObject.ORIGINAL_NAME;
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    }

    // no longer used
    // done by bulk drop now, see dropDefinedResources
    // dropDefinedResource(parentBicsuiteObject: any, tabInfo: Tab, bicsuiteObject: any): Promise<Object> {
    //     return this.createConfirmDialog([], "Do your really want do drop Resource " + bicsuiteObject.ORIGINAL_NAME + " ?", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
    //         if (result != undefined || result != null && result) {
    //             let stmt = "drop resource " + this.toolbox.namePathQuote(bicsuiteObject.NAME) + " in " + this.toolbox.namePathQuote(parentBicsuiteObject.PATH + "." + parentBicsuiteObject.ORIGINAL_NAME) + ";";
    //             // console.log(stmt);
    //             return this.commandApi.execute(stmt).then((response: any) => {
    //                 if (response.FEEDBACK) {
    //                     this.eventEmitterService.pushSnackBarMessage([response.FEEDBACK], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
    //                 }
    //                 return response;
    //             });
    //         } else {
    //             return Promise.resolve('NO_DROP');
    //         }
    //     });
    // }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)

        let grantObject = {
            TYPE: bicsuiteObject.TYPE,
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME,
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS:"",
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;
                if (grantObject.GRANTS.TABLE[0]) {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            let editorForm = crud.editorformByType(grantObject.TYPE, false, true)
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for " + bicsuiteObject.TYPE + " " + (bicsuiteObject != undefined ? bicsuiteObject.ORIGINAL_NAME : ""), crud),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "100%"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }

    getSelectedParameterComments(bicsuiteObject: any, tabInfo: Tab, selectedParameter : any) : Promise<Object> {
        let stmt : string = "show comment on parameter '" + selectedParameter.ORIGINAL_NAME + "' of job definition " +
            this.toolbox.namePathQuote(bicsuiteObject.PATH + '.' + bicsuiteObject.NAME);
        return this.commandApi.execute(stmt).then((response: any) => {
            selectedParameter.NOFORM_comments = response.DATA.TABLE;
            return response;
        });
    }

    getSelectedParametersComments(bicsuiteObject: any, tabInfo: Tab, selectedParameters : any[]) : Promise<Object> {
        let promises: Promise<any>[] = [];
        for (let selectedParameter of selectedParameters) {
            if (selectedParameter.COMMENT) {
                promises.push(this.getSelectedParameterComments(bicsuiteObject, tabInfo, selectedParameter));
            }
        }
        return Promise.all(promises).then((res) => {
            return {};
        });
    }

    copyParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        checklistSelection.selected[0].NOFORM_PARENTTYPE = bicsuiteObject.TYPE;
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.JOB_PARAMETER);
            return {};
        });
    }

    cutParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.JOB_PARAMETER);
            return this.dropParameters(bicsuiteObject, tabInfo, form, checklistSelection);
        });
    }

    dropParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newParameters = [];
        for (let parameter of bicsuiteObject.PARAMETER.TABLE) {
            if (dropRowIndexes.includes(parameter.rowIndex)) {
                continue;
            }
            newParameters.push(parameter);
        }
        bicsuiteObject.PARAMETER.TABLE = newParameters;
        this.sortParameters(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    parameterPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.JOB_PARAMETER];

    conditionPasteParameters(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.parameterPasteTypes, tabInfo.ID);
    }

    pasteParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.parameterPasteTypes)) {
            // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
            let p = JSON.parse(JSON.stringify(item.data));
            if (p.PARENTNAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                p.PARENTNAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                p.ID = undefined; // this is  a new parameter!!!
            }
            item.mode = SdmsClipboardMode.COPY;
            bicsuiteObject.PARAMETER.TABLE.push(p);
        }
        this.sortParameters(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    copyChildren(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.JOB_CHILD);
        return Promise.resolve({});
    }

    cutChildren(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.JOB_CHILD);
        return this.dropChildren(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropChildren(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newChildren = [];
        for (let child of bicsuiteObject.CHILDREN.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newChildren.push(child);
        }
        bicsuiteObject.CHILDREN.TABLE = newChildren;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    childrenPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.JOB_CHILD, SdmsClipboardType.BATCH, SdmsClipboardType.JOB, SdmsClipboardType.MILESTONE];

    conditionPasteChildren(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.childrenPasteTypes, tabInfo.ID);
    }

    pasteChildren(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.childrenPasteTypes)) {
            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.JOB_CHILD:
                    // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
                    c = JSON.parse(JSON.stringify(item.data));
                    // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
                    if (c.PARENTNAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                        c.PARENTNAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                        c.ID = undefined; // this is a new child!!!
                    }
                    break;
                case SdmsClipboardType.JOB:
                case SdmsClipboardType.BATCH:
                case SdmsClipboardType.MILESTONE:
                    c = {
                        "PRIORITY": "0",
                        "EXPORT_NAME": "",
                        "IS_STATIC": "true",
                        "IS_DISABLED": "false"
                    };
                    c.CHILDNAME = item.data.NAME;
                    c.CHILDTYPE = item.data.TYPE;
                    if (bicsuiteObject.TYPE == "JOB" && item.itemType != SdmsClipboardType.MILESTONE) {
                        c.IS_STATIC = "false";
                    }
                    else {
                        c.IS_STATIC = "true";
                    }
                    break;
            }
            item.mode = SdmsClipboardMode.COPY;
            bicsuiteObject.CHILDREN.TABLE.push(c);
        }
        this.sortChildren(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortChildren(bicsuiteObject: any) {
        bicsuiteObject.CHILDREN.TABLE.sort(function (a: any, b: any) {
            return a.CHILDNAME < b.CHILDNAME ? -1 : 1;
        });
    }

    copyDependencies(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.JOB_DEPENDENCY);
        return Promise.resolve({});
    }

    cutDependencies(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.JOB_DEPENDENCY);
        return this.dropDependencies(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropDependencies(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newRequiredJobs = [];
        for (let child of bicsuiteObject.REQUIRED_JOBS.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newRequiredJobs.push(child);
        }
        bicsuiteObject.REQUIRED_JOBS.TABLE = newRequiredJobs;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    dependenciesPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.JOB_DEPENDENCY, SdmsClipboardType.BATCH, SdmsClipboardType.JOB, SdmsClipboardType.MILESTONE];

    conditionPasteDependencies(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.dependenciesPasteTypes, tabInfo.ID);
    }

    pasteDependencies(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.dependenciesPasteTypes)) {

            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.JOB_DEPENDENCY:
                    // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
                    c = JSON.parse(JSON.stringify(item.data));
                    // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
                    if (c.DEPENDENTNAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                        c.DEPENDENTNAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME
                        c.ID = undefined; // this is a new child!!!
                    }
                    break;
                case SdmsClipboardType.JOB:
                case SdmsClipboardType.BATCH:
                case SdmsClipboardType.MILESTONE:
                    c = {
                        "NAME": "",
                        "MODE": "ALL_FINAL",
                        "UNRESOLVED_HANDLING": "ERROR",
                        "STATE_SELECTION": "ALL_REACHABLE",
                        "RESOLVE_MODE": "INTERNAL",
                        "REQUIREDTYPE": "JOB",
                        "EXPIRED_AMOUNT": "1",
                        "EXPIRED_BASE": "MINUTE"
                     };
                    c.REQUIREDNAME = item.data.NAME;
                    c.REQUIREDTYPE = item.data.TYPE;
                    break;
            }
            item.mode = SdmsClipboardMode.COPY;
            bicsuiteObject.REQUIRED_JOBS.TABLE.push(c);
        }
        this.sortRequiredJobs(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortRequiredJobs(bicsuiteObject: any) {
        bicsuiteObject.REQUIRED_JOBS.TABLE.sort(function (a: any, b: any) {
            return a.REQUIREDNAME < b.REQUIREDNAME ? -1 : 1;
        });
    }

    copyRequiredResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.REQUIRED_RESOURCE);
        return Promise.resolve({});
    }

    cutRequiredResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.REQUIRED_RESOURCE);
        return this.dropRequiredResources(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropRequiredResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newRequiredResources = [];
        for (let child of bicsuiteObject.REQUIRED_RESOURCES.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newRequiredResources.push(child);
        }
        bicsuiteObject.REQUIRED_RESOURCES.TABLE = newRequiredResources;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    requiredResourcesPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.REQUIRED_RESOURCE, SdmsClipboardType.STATIC, SdmsClipboardType.SYSTEM, SdmsClipboardType.SYNCHRONIZING];

    conditionPasteRequiredResources(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.requiredResourcesPasteTypes, tabInfo.ID);
    }

    pasteRequiredResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.requiredResourcesPasteTypes)) {
            console.log(item)
            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.REQUIRED_RESOURCE:
                    // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
                    c = JSON.parse(JSON.stringify(item.data));
                    // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
                    if (c.PARENTNAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                        c.PARENTNAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                        c.ID = undefined; // this is  a new child!!!
                    }
                    break;
                case SdmsClipboardType.STATIC:
                case SdmsClipboardType.SYSTEM:
                case SdmsClipboardType.SYNCHRONIZING:
                    c = {
                        "PARENTNAME" : bicsuiteObject.PATH + '.' + bicsuiteObject.NAME,
                        "RESOURCE_NAME": item.data.NAME,
                        "CONDITION": "",
                        "AMOUNT": "1",
                        "KEEP_MODE": "NOKEEP",
                        "IS_STICKY": "false",
                        "STICKY_NAME": "",
                        "STICKY_PARENT": "",
                        "RESOURCE_STATE_MAPPING": "",
                        "EXPIRED_AMOUNT": "",
                        "EXPIRED_BASE": "MINUTE",
                        "IGNORE_ON_RERUN": "false",
                        "LOCKMODE": "NOLOCK",
                        "STATES": "",
                        "RESOURCE_USAGE": item.data.USAGE
                     };
                    break;
            }
            item.mode = SdmsClipboardMode.COPY;
            console.log(c)
            bicsuiteObject.REQUIRED_RESOURCES.TABLE.push(c);
        }
        this.sortRequiredResources(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortRequiredResources(bicsuiteObject: any) {
        bicsuiteObject.REQUIRED_RESOURCES.TABLE.sort(function (a: any, b: any) {
            if (a.ORIGIN && !b.ORIGIN) {
                return 1;
            }
            if (!a.ORIGIN && b.ORIGIN) {
                return -1;
            }
            return a.RESOURCE_NAME < b.RESOURCE_NAME ? -1 : 1;
        });
    }

    copyTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.JOB_TRIGGER);
        return Promise.resolve({});
    }

    cutTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.JOB_TRIGGER);
        return this.dropTriggers(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newTriggers = [];
        for (let child of bicsuiteObject.TRIGGERS.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newTriggers.push(child);
        }
        bicsuiteObject.TRIGGERS.TABLE = newTriggers;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    triggersPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.JOB_TRIGGER, SdmsClipboardType.BATCH, SdmsClipboardType.JOB];

    conditionPasteTriggers(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.triggersPasteTypes, tabInfo.ID);
    }

    pasteTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.triggersPasteTypes)) {

            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.JOB_TRIGGER:
                    c = JSON.parse(JSON.stringify(item.data));
                    if (c.IS_INVERSE == "true") {
                        c.SUBMIT_NAME = c.OBJECT_NAME;
                        c.SUBMIT_TYPE = c.OBJECT_SUBTYPETYPE;
                    }
                    // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
                    if (c.OBJECT_NAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                        c.OBJECT_NAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                        c.OBJECT_SUBTYPE = bicsuiteObject.TYPE;
                        c.ID = undefined; // this is  a new child!!!
                    }
                    break;
                case SdmsClipboardType.JOB:
                case SdmsClipboardType.BATCH:
                    c = {
                        "NAME": "NEW_TRIGGER",
                        "EXIT_STATES": "",
                        "ACTIVE": "true",
                        "MAX_RETRY": "1",
                        "CHECK_AMOUNT": "1",
                        "CHECK_BASE": "MINUTE"
                    };
                    c.OBJECT_NAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                    c.OBJECT_SUBTYPE = bicsuiteObject.TYPE;
                    c.SUBMIT_NAME = item.data.NAME;
                    c.SUBMIT_TYPE = item.data.TYPE;
                    break;
            }
            item.mode = SdmsClipboardMode.COPY;
            c.IS_INVERSE = "false";
            bicsuiteObject.TRIGGERS.TABLE.push(c);
        }
        this.sortTriggers(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortTriggers(bicsuiteObject: any) {
        bicsuiteObject.TRIGGERS.TABLE.sort(function (a: any, b: any) {
            if (a.IS_INVERSE == "false" && b.IS_INVERSE == "true") {
                return -1;
            }
            if (a.IS_INVERSE == "true" && b.IS_INVERSE == "false") {
                return 1;
            }
            if (a.NAME < b.NAME) {
                return -1;
            }
            if (a.NAME > b.NAME) {
                return 1;
            }
            return a.SUBMIT_NAME < b.SUBMIT_NAME ? -1 : 1;
        });
    }

    copyTriggersBy(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.JOB_TRIGGER);
        return Promise.resolve({});
    }

    cutTriggersBy(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.JOB_TRIGGER);
        return this.dropTriggersBy(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropTriggersBy(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newTriggers = [];
        for (let child of bicsuiteObject.TRIGGERED_BY.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newTriggers.push(child);
        }
        bicsuiteObject.TRIGGERED_BY.TABLE = newTriggers;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    triggersPasteTypesBy: SdmsClipboardType[] = [SdmsClipboardType.JOB_TRIGGER, SdmsClipboardType.BATCH, SdmsClipboardType.JOB, SdmsClipboardType.MILESTONE];

    conditionPasteTriggersBy(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.triggersPasteTypes, tabInfo.ID);
    }

    pasteTriggersBy(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {

        for (let item of this.clipboardService.getItems(this.triggersPasteTypesBy)) {

            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.JOB_TRIGGER:
                    c = JSON.parse(JSON.stringify(item.data));
                    // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
                    if (c.IS_INVERSE == "false") {
                        c.OBJECT_NAME = c.SUBMIT_NAME;
                        c.OBJECT_SUBTYPE = c.SUBMIT_TYPE;
                    }
                    if (c.SUBMIT_NAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                        c.SUBMIT_NAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                        c.SUBMIT_TYPE = bicsuiteObject.TYPE;
                        c.ID = undefined; // this is  a new child!!!
                    }
                    break;
                case SdmsClipboardType.JOB:
                case SdmsClipboardType.BATCH:
                case SdmsClipboardType.MILESTONE:
                    c = {
                        "NAME": "NEW_TRIGGER",
                        "EXIT_STATES": "",
                        "ACTIVE": "true",
                        "MAX_RETRY": "1",
                        "CHECK_AMOUNT": "1",
                        "CHECK_BASE": "MINUTE"
                    };
                    c.SUBMIT_OWNER = this.privilegesService.getDefaultGroup();
                    c.SUBMIT_NAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                    c.SUBMIT_TYPE = bicsuiteObject.TYPE;
                    c.OBJECT_NAME = item.data.NAME;
                    c.OBJECT_SUBTYPE = item.data.TYPE;
                    break;
            }
            item.mode = SdmsClipboardMode.COPY;
            c.IS_INVERSE = "true";
            c.MASTER = "true";
            bicsuiteObject.TRIGGERED_BY.TABLE.push(c);
        }
        this.sortTriggersBy(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortTriggersBy(bicsuiteObject: any) {
        bicsuiteObject.TRIGGERED_BY.TABLE.sort(function (a: any, b: any) {
            if (a.IS_INVERSE == "false" && b.IS_INVERSE == "true") {
                return 1;
            }
            if (a.IS_INVERSE == "true" && b.IS_INVERSE == "false") {
                return -1;
            }
            if (a.NAME < b.NAME) {
                return -1;
            }
            if (a.NAME > b.NAME) {
                return 1;
            }
            return a.OBJECT_NAME < b.OBJECT_NAME ? -1 : 1;
        });
    }

    copyDefinedResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.RESOURCE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = false;
        }
        return Promise.resolve({});
    }

    cutDefinedResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.RESOURCE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = true;
        }
        return Promise.resolve({});
    }

    definedResourcesPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.RESOURCE, SdmsClipboardType.SYSTEM, SdmsClipboardType.SYNCHRONIZING]

    conditionClipBoardOfTypeDefinedResources(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // check if there is item to be pasted and if the source is not the same
        return this.clipboardService.checkItemTypesPresent(this.definedResourcesPasteTypes, bicsuiteObject.ID);
    }

    dropDefinedResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {

        return this.createConfirmDialog([], "drop_confirm_selected", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmts: string[] = [];
                for (let selected of checklistSelection._selected) {
                    let stmt = "drop resource " + this.toolbox.namePathQuote(selected.NAME) + " in " + this.toolbox.namePathQuote(selected.SCOPE);
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {

                    let dropRowIndexes = [];
                    for (let selected of checklistSelection._selected) {
                        this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "DROP", NAME: selected.NAME, OWNER: selected.SCOPE })
                        dropRowIndexes.push(selected.rowIndex);
                    }
                    let newDefinedResources = [];
                    for (let definedResource of bicsuiteObject.NOFORM_DEFINED_RESOURCES.TABLE) {
                        if (dropRowIndexes.includes(definedResource.rowIndex)) {
                            continue;
                        }
                        newDefinedResources.push(definedResource);
                    }
                    bicsuiteObject.NOFORM_DEFINED_RESOURCES.TABLE = newDefinedResources;

                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    this.eventEmitterService.refreshNavigationEmittor.next();
                });
            }
            return {};
        });
    }

    pasteDefinedResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        let stmts: string[] = [];
        let droppedResources : any[] =[];
        for (let item of this.clipboardService.getItems(this.definedResourcesPasteTypes)) {
            let stmt = "";

            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.RESOURCE:
                    if (item.data.USAGE == 'STATIC') {
                        continue; // no static defined resources !!!
                    }
                    if (item.data.SCOPE == tabInfo.PATH + '.' + tabInfo.NAME && item.mode == SdmsClipboardMode.CUT) {
                            item.mode = SdmsClipboardMode.COPY;
                            item.data.fe_flagged_to_cut = false;
                    }
                    else {
                        if (item.mode == SdmsClipboardMode.CUT) {
                            stmt = "drop resource " + this.toolbox.namePathQuote(item.data.NAME) + " in " + this.toolbox.namePathQuote(item.data.SCOPE);
                            stmts.push(stmt);
                            droppedResources.push(item);
                        }
                        stmts.push(this.genCreateDefinedResource(item, tabInfo));
                    }
                    break;
                case SdmsClipboardType.SYSTEM:
                case SdmsClipboardType.SYNCHRONIZING:
                    let defaultAmount = "1";
                    if (item.itemType ==  SdmsClipboardType.SYNCHRONIZING) {
                        defaultAmount = "INFINITE";
                    }
                    c = {
                        data : {
                            NAME : item.data.NAME,
                            AMOUNT : defaultAmount,
                            REQUESTABLE_AMOUNT : defaultAmount,
                            IS_ONLINE : "true"
                        }
                    }
                    stmts.push(this.genCreateDefinedResource(c, tabInfo));
                    break;
            }
        }
        if (stmts.length > 0) {
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                for (let dropped of droppedResources) {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "DROP", NAME: dropped.data.NAME, OWNER: dropped.data.SCOPE })
                    dropped.mode = SdmsClipboardMode.COPY;
                    dropped.sourceId = "";
                }
                this.reloadDefinedResources(bicsuiteObject, tabInfo).then (() => {
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    return Promise.resolve(false);
                });
            });
        }
        else {
            return Promise.resolve(false);
        }
    }

    private genCreateDefinedResource(item: any, tabInfo: Tab) {
        let stmt = "create resource " +
            this.toolbox.namePathQuote(item.data.NAME) + " in " +
            this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) +
            " with " +
            " " + (item.data.IS_ONLINE == "true" ? "online" : "offline");
        if (item.data.STATE) {
            stmt += ", status = '" + item.data.STATE + "'";
        }
        stmt += ", amount = " + item.data.AMOUNT + ", requestable_amount = " + item.data.REQUESTABLE_AMOUNT;
        return stmt;
    }

    openPath(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openFolder(bicsuiteObject.PATH);
    }

    openDefinedResource(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let definedResourceTab = new Tab(bicsuiteObject.NAME.split('.').pop(), bicsuiteObject.ID, 'RESOURCE'.toUpperCase(), bicsuiteObject.USAGE.toLowerCase(), TabMode.EDIT);
        definedResourceTab.DATA = {};
        definedResourceTab.DATA.RESOURCE_NAME = bicsuiteObject.NAME;
        definedResourceTab.DATA.RESOURCE_OWNER_TYPE = tabInfo.TYPE;
        definedResourceTab.DATA.f_IsInstance = false;
        definedResourceTab.DATA.f_isScopeOrFolderInstance = false;
        definedResourceTab.OWNER = parentBicsuiteObject.PATH + '.' + parentBicsuiteObject.ORIGINAL_NAME;
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    }

    openReference(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        let ref = bicsuiteObject.REFERENCE.split(' ')[0];
        return this.openTabForPath(ref, bicsuiteObject.REFERENCE_TYPE.toLowerCase());
    }

    openIntName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openInterval(bicsuiteObject.INT_NAME);
    }

    openChildName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.CHILDNAME, bicsuiteObject.CHILDTYPE.toLowerCase());
    }

    openStickyParent(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.STICKY_PARENT, "batch");
    }

    openRequiredName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.REQUIREDNAME, bicsuiteObject.REQUIREDTYPE.toLowerCase());
    }

    openRequiredResourceStateMapping(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateMapping(bicsuiteObject.RESOURCE_STATE_MAPPING);
    }

    openResourceState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition(bicsuiteObject.RESOURCE_STATE, false);
    }

    openDependentName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.DEPENDENTNAME, bicsuiteObject.DEPENDENTTYPE.toLowerCase());
    }

    openSubmitName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.SUBMIT_NAME, bicsuiteObject.SUBMIT_TYPE.toLowerCase());
    }

    openObjectName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.OBJECT_NAME, bicsuiteObject.OBJECT_SUBTYPE.toLowerCase());
    }

    openParentName(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        return this.openTabForPath(bicsuiteObject.PARENTNAME, bicsuiteObject.PARENTTYPE.toLowerCase());
    }

    openRestartExitState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.EXIT_STATE);
    }

    openDependencyExitState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.EXIT_STATE, false);
    }

    openTriggerExitState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.EXIT_STATE, false);
    }

    openSubmitOwner(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openGroup(bicsuiteObject.SUBMIT_OWNER);
    }

    openRestartLimitState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.RESTART_LIMIT_STATE);
    }

    openEspName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateProfile(bicsuiteObject.ESP_NAME);
    }

    openEstName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateTranslation(bicsuiteObject.EST_NAME);
    }

    openEsmName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateMapping(bicsuiteObject.ESM_NAME);
    }

    openEnvName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openEnv(bicsuiteObject.ENV_NAME);
    }

    openFpName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openFootprint(bicsuiteObject.FP_NAME);
    }

    checkForDecomposition(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.OLD_TYPE == 'JOB' && bicsuiteObject.TYPE == 'BATCH') {
            bicsuiteObject.CREATE_BATCH_FOLDER = "true";
            bicsuiteObject.DECOMPOSITE = "true";
        }
        return Promise.resolve({});
    }

    chooseParameterReference(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        if (chooseResult.parentNode) {
            bicsuiteObject.REFERENCE_TYPE = chooseResult.parentNode.type.toUpperCase();
        }
        else {
            bicsuiteObject.REFERENCE = chooseResult.bicsuiteObject.PARENTNAME + " (" + chooseResult.bicsuiteObject.ORIGINAL_NAME + ")";
            bicsuiteObject.REFERENCE_TYPE = chooseResult.bicsuiteObject.NOFORM_PARENTTYPE;
        }
        return Promise.resolve({});
    }

    chooseResource(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.RESOURCE_STATE_PROFILE_NAME = chooseResult.bicsuiteObject.RESOURCE_STATE_PROFILE;
        bicsuiteObject.RESOURCE_USAGE = chooseResult.bicsuiteObject.USAGE;
        return Promise.resolve({});
    }

    chooseChildren(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.CHILDTYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }

    chooseObjectName(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.OBJECT_SUBTYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }

    chooseSubmitName(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.SUBMIT_TYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }

    chooseRequired(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.REQUIREDTYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }
    
    conditionIsVersioned(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.VERSION ? true : false );
    }

    conditionRequiredIsJob(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.REQUIREDTYPE == 'JOB');
    }

    conditionDependencyResolveModeIsExternal(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.RESOLVE_MODE == 'EXTERNAL' || bicsuiteObject.RESOLVE_MODE == 'BOTH');
    }

    conditionDependencyStateSelectionIsFinal(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.STATE_SELECTION == 'FINAL';
    }

    conditionIsMasterSubmitable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.READ_MASTER_SUBMITABLE == 'true';
    }

    conditionDidSubmit(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SUBMITTED_ID != undefined;
    }

    conditionSubmitSuspendedIsYes(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SUBMIT_SUSPENDED == 'YES' || bicsuiteObject.SUBMIT_SUSPENDED == 'JOBSUSPEND';
    }

    conditionSubmitResumeInAndSubmitSuspendedIsYes(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
      if(this.conditionSubmitSuspendedIsYes(editorformField, bicsuiteObject, tabInfo)) {
        return bicsuiteObject.RESUME_TYPE == 'IN';
      } else {
        return false;
      }
    }

    conditionSubmitResumeAtAndSubmitSuspendedIsYes(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
      if(this.conditionSubmitSuspendedIsYes(editorformField, bicsuiteObject, tabInfo)) {
        return bicsuiteObject.RESUME_TYPE == 'AT';
      } else {
        return false;
      }
    }

    conditonIsNotVersioned(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !tabInfo.VERSIONED;
    }


    conditionIsNewParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.ID == undefined;
    }

    conditionTriggerIsMaster(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.MASTER == true || bicsuiteObject.MASTER == 'true';
    }
    conditionTriggerIsAsync(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return ['UNTIL FINAL', 'UNTIL FINISHED'].includes(bicsuiteObject.TRIGGER_TYPE);
    }

    conditionIsInverse(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.IS_INVERSE == 'true' || bicsuiteObject.IS_INVERSE == true;
    }

    conditionParameterIsNotVisibleInSubmit(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE != "PARAMETER";
    }

    conditionParameterTypeIsExpression(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE == 'EXPRESSION';
    }

    conditionParameterTypeIsExpressionOrConstant(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE == 'EXPRESSION' || bicsuiteObject.TYPE == 'CONSTANT';
    }

    conditionParameterHasValue(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.TYPE != 'EXPRESSION');
    }

    conditionParameterHasWebConfig(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.WEBCONFIG)
            return true;
        else
            return false;
    }

    conditionParameterValueIsReference(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if ((["REFERENCE", "CHILDREFERENCE"].includes(bicsuiteObject.TYPE) && !["BATCH","JOB"].includes(bicsuiteObject.REFERENCE_TYPE)) ||
            (bicsuiteObject.TYPE == "RESOURCEREFERENCE" && !["STATIC", "SYSTEM", "SYNCHRONIZING"].includes(bicsuiteObject.REFERENCE_TYPE))) {
                bicsuiteObject.REFERENCE = "";
                bicsuiteObject.REFERENCE_TYPE = "";
        }
        return ["REFERENCE", "CHILDREFERENCE", "RESOURCEREFERENCE"].includes(bicsuiteObject.TYPE)
    }

    conditionDependencyRequiredNameIsSet(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.REQUIREDNAME != '' && bicsuiteObject.REQUIREDNAME != null && bicsuiteObject.REQUIREDNAME != undefined;
    }

    conditionIsSuspended(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SUBMIT_SUSPENDED == 'true';
    }

    conditionChildIsSuspended(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SUSPEND == 'SUSPEND';
    }

    conditionTriggerIsSuspended(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SUSPEND == 'true';
    }

    conditionTriggerIsNotTypeOfUntilFinished(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TRIGGER_TYPE != 'UNTIL FINISHED';
    }

    conditionIsResumeIn(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.SUBMIT_SUSPENDED == 'true') {
            return bicsuiteObject.RESUME_TYPE == 'IN'
        } else {
            return false;
        };
    }

    conditionIsResumeAt(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.SUBMIT_SUSPENDED == 'true') {
            return bicsuiteObject.RESUME_TYPE == 'AT'
        } else {
            return false;
        };
    }

    conditionChildIsResumeIn(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.SUSPEND == 'SUSPEND') {
            return bicsuiteObject.RESUME_TYPE == 'IN'
        } else {
            return false;
        };
    }

    conditionChildIsResumeAt(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.SUSPEND == 'SUSPEND') {
            return bicsuiteObject.RESUME_TYPE == 'AT'
        } else {
            return false;
        };
    }

    conditionTriggerIsResumeIn(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.SUSPEND == 'true') {
            return bicsuiteObject.RESUME_TYPE == 'IN'
        } else {
            return false;
        };
    }

    conditionTriggerIsResumeAt(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.SUSPEND == 'true') {
            return bicsuiteObject.RESUME_TYPE == 'AT'
        } else {
            return false;
        };
    }

    conditionIsBatch(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE == 'BATCH';
    }

    conditionIsJob(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE == 'JOB';
    }

    conditionIsJobOrBatch(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.TYPE == 'JOB' || bicsuiteObject.TYPE == 'BATCH');
    }

    conditionIsMilestone(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE == 'MILESTONE';
    }

    conditionResourceIsStatic(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESOURCE_USAGE == 'STATIC';
    }

    conditionRequiredResourceisNew(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !bicsuiteObject.ID;
    }

    conditionRequiredResourceHasNoOrigin(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.IS_STICKY != "true") {
            if (bicsuiteObject.STICKY_NAME) bicsuiteObject.STICKY_NAME = "";
            if (bicsuiteObject.STICKY_PARENT) bicsuiteObject.STICKY_PARENT = "";
        }
        return !bicsuiteObject.ORIGIN;
    }

    conditionJobHasTimeoutAmount(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.TIMEOUT_AMOUNT && bicsuiteObject.TIMEOUT_AMOUNT != 'NONE') {
            return true;
        }
        else {
            return false;
        }
    }

    conditionRequiredResourceIsStickyAndSynchronized(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (this.conditionRequiredResourceIsSynchronizing(editorformField, bicsuiteObject, tabInfo)) {
            return bicsuiteObject.IS_STICKY == "true";
        } else {
            return false;
        }
    }

    conditionRequiredResourceHasExpiredAmount(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.EXPIRED_AMOUNT > 0) {
            return true;
        }
        else {
            return false;
        }
    }

    conditionRequiredResourceIsSynchronizing(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESOURCE_USAGE == 'SYNCHRONIZING';
    }

    conditionHasStateProfile(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.RESOURCE_STATE_PROFILE_NAME) {
            return true;
        }
        return false;
    }

    conditionRequiredResourceHasResourceStateProfile(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.RESOURCE_USAGE != "SYNCHRONIZING") {
            return false;
        } else {

            // TODO CHECK RESOURCE IF RESOURCE STATE PROFILE IS SET
            return true;
        }
        // muss syncronicing resource sein
        // resource state profile haben
        return bicsuiteObject.IS_STICKY == "true";
    }

    conditionRequiredResourceIsSynchronizingAndLockModeExclusiveAndResourceStateProfile(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.RESOURCE_USAGE == 'SYNCHRONIZING' && bicsuiteObject.LOCKMODE == 'EXCLUSIVE' && bicsuiteObject.RESOURCE_STATE_PROFILE_NAME) {
            return true;
        }
        return false;
    }

    conditionRequiredResourceIsSynchronizingOrSystem(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.RESOURCE_USAGE == 'SYNCHRONIZING') || (bicsuiteObject.RESOURCE_USAGE == 'SYSTEM');
    }

    conditionDefinedResourceisNew(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !bicsuiteObject.ID;
    }

    conditionCreateBatchFolderVisible(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let result: boolean = this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true" &&
            ((tabInfo.MODE == TabMode.NEW && (bicsuiteObject.TYPE == "BATCH" || bicsuiteObject.TYPE == "JOB")) ||
                this.conditionDecomposite(editorformField, bicsuiteObject, tabInfo));
        return result;
    }

    conditionRenameBatchConventionFolder(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") &&
            bicsuiteObject.PATH.split('.').pop() == bicsuiteObject.ORIGINAL_NAME &&
            this.conditionNameChanged(editorformField, bicsuiteObject, tabInfo);
    }

    conditionAddChild(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return tabInfo.MODE == TabMode.NEW && bicsuiteObject.IN_CONVENTION_FOLDER == "true";
    }

    conditionDecomposite(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.OLD_TYPE == "JOB" && bicsuiteObject.TYPE == "BATCH";
    }

    conditionIsDecomposite(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (!bicsuiteObject.NOFORM_JOB_NAME) {
            bicsuiteObject.NOFORM_JOB_NAME = bicsuiteObject.NAME + "_";
        }
        return this.conditionDecomposite(editorformField, bicsuiteObject, tabInfo) && bicsuiteObject.DECOMPOSITE == "true";
    }

    conditionAddChildOnClone(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return tabInfo.MODE == TabMode.EDIT &&
            bicsuiteObject.NAME != bicsuiteObject.ORIGINAL_NAME &&
            (
                (bicsuiteObject.IN_CONVENTION_FOLDER == "true" && bicsuiteObject.PATH.split('.').pop() != bicsuiteObject.ORIGINAL_NAME) ||
                (bicsuiteObject.PARENT_IS_CONVENTION_FOLDER == "true" && bicsuiteObject.PATH.split('.').pop() == bicsuiteObject.ORIGINAL_NAME)
            );
    }

    conditionIsAddChildJob(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (this.conditionAddChild(editorformField, bicsuiteObject, tabInfo) || this.conditionAddChildOnClone(editorformField, bicsuiteObject, tabInfo)) &&
            bicsuiteObject.ADD_CHILD == "true";
    }

    processPathChange(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        if (tabInfo.PATH == oldName) {
            tabInfo.PATH = newName;
            tabInfo.updateTooltip();
        }
        else {
            if (tabInfo.PATH.startsWith(oldName + '.')) {
                tabInfo.PATH = tabInfo.PATH.replace(oldName + '.', newName + '.');
                tabInfo.updateTooltip();
            }
        }
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.PATH) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Path Change
        if (oldName && newName) {
            if (record.PATH == oldName) {
                record.PATH = newName;
                if (initialRecord) {
                    initialRecord.PATH = record.PATH;
                }
                return this.processPathChangeForParameterReferences(bicsuiteObject, tabInfo, inttialBicsuiteObject, message);
            }
            else {
                if (record.PATH.startsWith(oldName + '.')) {
                    record.PATH = record.PATH.replace(oldName + '.', newName + '.');
                    record.FILENAME = (tabInfo.PATH && tabInfo.PATH != '') ? this.toolbox.namePathQuote(tabInfo.PATH + "." + tabInfo.NAME) : tabInfo.NAME;
                    if (initialRecord) {
                        initialRecord.PATH = record.PATH;
                        initialRecord.FILENAME = record.FILENAME;
                    }
                    return this.processPathChangeForParameterReferences(bicsuiteObject, tabInfo, inttialBicsuiteObject, message);
                }
            }
        }
        else {
            this.processPathChangeForParameterReferences(bicsuiteObject, tabInfo, inttialBicsuiteObject, message);
        }
        return Promise.resolve(false);
    }

    processParameterReferences(record: any, newName: string, oldName: string, message: any) {
        let parameters = record.PARAMETER.TABLE;
        let newParameters : any[] = [];
        // console.log("oldname = " + oldName)
        // console.log("newname = " + newName)
        for (let parameter of parameters) {
            if (["REFERENCE", "CHILDREFERENCE", "RESOURCEREFERENCE"].includes(parameter.TYPE)) {
                if ((parameter.REFERENCE + '.').startsWith(oldName + '.')) {
                    if (message.OP == "RENAME") {
                        parameter.REFERENCE = parameter.REFERENCE.replace(oldName, newName);
                        newParameters.push(parameter);
                    }
                    if (message.OP == "DROP") {
                        // we just do not push to new parameter because this was a forced drop
                        // thus parameter is removed
                    }
                }
                else {
                    newParameters.push(parameter);
                }
            }
        }
        record.PARAMETER.TABLE = newParameters;
        return false;
    }

    processDependencyStates(dependencies: any[], newName: string, oldName: string) {
        for (let dependency of dependencies) {
            if (dependency.STATES) {
                let states = dependency.STATES.split(',');
                let newStates = [];
                for (let state of states) {
                    let l = state.split(':');
                    let s = l[0].trim();
                    if (s == oldName) {
                        s = newName;
                    }
                    if (l.length > 1) {
                        s += ':' + l[1]
                    }
                    newStates.push(s);
                }
                dependency.STATES = newStates.join(',');
            }
        }
        return false;
    }

    processTriggerStates(triggers: any[], newName: string, oldName: string) {
        for (let trigger of triggers) {
            if (trigger.STATES) {
                let states = trigger.STATES.split(',');
                let newStates = [];
                for (let state of states) {
                    let s = state.trim();
                    if (s == oldName) {
                        s = newName;
                    }
                    newStates.push(s);
                }
                trigger.STATES = newStates.join(',');
            }
        }
        return false;
    }

    processEsdRename(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.REQUIRED_JOBS) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Esd Name Change
        if (oldName && newName) {
            this.processDependencyStates(record.REQUIRED_JOBS.TABLE, newName, oldName);
            if (initialRecord?.REQUIRED_JOBS) {
                this.processDependencyStates(initialRecord.REQUIRED_JOBS.TABLE, newName, oldName);
            }
            this.processTriggerStates(record.TRIGGERS.TABLE, newName, oldName);
            if (initialRecord?.TRIGGERS) {
                this.processTriggerStates(initialRecord.TRIGGERS.TABLE, newName, oldName);
            }
            this.processTriggerStates(record.TRIGGERED_BY.TABLE, newName, oldName);
            if (initialRecord?.TRIGGERED_BY) {
                this.processTriggerStates(initialRecord.TRIGGERED_BY.TABLE, newName, oldName);
            }
        }
        return Promise.resolve(false);
    }

    processResourceStates(resourceRequirements: any[], newName: string, oldName: string) {
        for (let resourceRequirement of resourceRequirements) {
            if (resourceRequirement.STATES) {
                let states = resourceRequirement.STATES.split(',');
                let newStates = [];
                for (let state of states) {
                    let s = state.trim();
                    if (s == oldName) {
                        s = newName;
                    }
                    newStates.push(s);
                }
                resourceRequirement.STATES = newStates.join(',');
            }
        }
        return false;
    }

    processRsdRenameForResourceStates(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.REQUIRED_RESOURCES) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Rsd Name Change
        if (oldName && newName) {
            this.processResourceStates(record.REQUIRED_RESOURCES.TABLE, newName, oldName);
            if (initialRecord?.REQUIRED_JOBS) {
                this.processResourceStates(initialRecord.REQUIRED_RESOURCES.TABLE, newName, oldName);
            }
        }
        return Promise.resolve(false);
    }

    processPathChangeForParameterReferences(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.PARAMETER) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Path Change or Drop
        if (message.OP == "RENAME") {
            this.processParameterReferences(record, newName, oldName, message);
            if (initialRecord?.PARAMETER) {
                this.processParameterReferences(initialRecord, newName, oldName, message);
            }
        }
        return Promise.resolve(false);
    }

    processDefiniedResourceChange (bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.REQUIRED_RESOURCES) {
            return Promise.resolve(false);
        }
        let owner = tabInfo.PATH + '.' + tabInfo.NAME;
        if (message.OWNER == owner) {
            this.reloadDefinedResources(record, tabInfo).then (() => {
                return Promise.resolve(false);
            });
        }
        return Promise.resolve(false);
    }

    getChangeConfig() {
        return {
            FOLDER: {
                RENAME: {
                    METHOD: "processPathChange",
                    FIELDS: [
                        {
                            TABLE: "REFERENCES",
                            NAME: "REFERENCE_PATH"
                        },
                        {
                            TABLE: "CHILDREN",
                            NAME: "CHILDNAME"
                        },
                        {
                            TABLE: "PARENTS",
                            NAME: "PARENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_JOBS",
                            NAME: "REQUIREDNAME"
                        },
                        {
                            TABLE: "DEPENDENT_JOBS",
                            NAME: "DEPENDENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        },
                        {
                            TABLE: "TRIGGERED_BY",
                            NAME: "OBJECT_NAME"
                        }
                    ]
                },
                DROP: {
                    METHOD: "processPathChangeForParameterReferences",
                    FIELDS: [
                        {
                            TABLE: "REFERENCES",
                            NAME: "REFERENCE_PATH"
                        },
                        {
                            TABLE: "CHILDREN",
                            NAME: "CHILDNAME"
                        },
                        {
                            TABLE: "PARENTS",
                            NAME: "PARENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_JOBS",
                            NAME: "REQUIREDNAME"
                        },
                        {
                            TABLE: "DEPENDENT_JOBS",
                            NAME: "DEPENDENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        },
                        {
                            TABLE: "TRIGGERED_BY",
                            NAME: "OBJECT_NAME"
                        }
                    ]
                },
            },
            JOB_DEFINITION: {
                RENAME: {
                    METHOD: "processPathChangeForParameterReferences",
                    FIELDS: [
                        {
                            TABLE: "REFERENCES",
                            NAME: "REFERENCE_PATH"
                        },
                        {
                            TABLE: "CHILDREN",
                            NAME: "CHILDNAME"
                        },
                        {
                            TABLE: "PARENTS",
                            NAME: "PARENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_JOBS",
                            NAME: "REQUIREDNAME"
                        },
                        {
                            TABLE: "DEPENDENT_JOBS",
                            NAME: "DEPENDENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        },
                        {
                            TABLE: "TRIGGERED_BY",
                            NAME: "OBJECT_NAME"
                        }
                    ]
                },
                DROP: {
                    METHOD: "processPathChangeForParameterReferences",
                    FIELDS: [
                        {
                            TABLE: "REFERENCES",
                            NAME: "REFERENCE_PATH"
                        },
                        {
                            TABLE: "CHILDREN",
                            NAME: "CHILDNAME"
                        },
                        {
                            TABLE: "PARENTS",
                            NAME: "PARENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_JOBS",
                            NAME: "REQUIREDNAME"
                        },
                        {
                            TABLE: "DEPENDENT_JOBS",
                            NAME: "DEPENDENTNAME"
                        },
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        },
                        {
                            TABLE: "TRIGGERED_BY",
                            NAME: "OBJECT_NAME"
                        }
                    ]
                },
                TYPECHANGE: {
                    FIELDS: [
                        {
                            TABLE: "REFERENCES",
                            NAME: "REFERENCE_PATH",
                            TYPENAME: "REFERENCE_TYPE"
                        },
                        {
                            TABLE: "CHILDREN",
                            NAME: "CHILDNAME",
                            TYPENAME: "CHILDTYPE"
                        },
                        {
                            TABLE: "PARENTS",
                            NAME: "PARENTNAME",
                            TYPENAME: "PARENTTYPE"
                        },
                        {
                            TABLE: "REQUIRED_JOBS",
                            NAME: "REQUIREDNAME",
                            TYPENAME: "REQUIREDTYPE"
                        },
                        {
                            TABLE: "DEPENDENT_JOBS",
                            NAME: "DEPENDENTNAME",
                            TYPENAME: "DEPENDENTTYPE"
                        }
                    ]
                }
            },
            GROUP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "OWNER"
                        }
                    ]
                }
            },
            ESP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "ESP_NAME"
                        }
                    ]
                }
            },
            ESM: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "ESM_NAME"
                        }
                    ]
                }
            },
            EST: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "CHILDREN",
                            NAME: "EST_NAME"
                        }
                    ]
                }
            },
            ENV: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "ENV_NAME"
                        }
                    ]
                }
            },
            FP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "FP_NAME"
                        }
                    ]
                }
            },
            ESD: {
                RENAME: {
                    METHOD: "processEsdRename",
                    FIELDS: [
                        {
                            TABLE: "RESTART_STATES",
                            NAME: "EXIT_STATE"
                        },
                        {
                            NAME: "RESTART_LIMIT_STATE"
                        }
                    ]
                }
            },
            RSD: {
                RENAME: {
                    METHOD: "processRsdRenameForResourceStates",
                    FIELDS: [
                        {
                            TABLE: "NOFORM_DEFINED_RESOURCES",
                            NAME: "STATE"
                        }
                    ]
                }
            },
            RSM: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "RESOURCE_STATE_MAPPING"
                        }
                    ]
                }
            },
            CATEGORY: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "RESOURCE_NAME"
                        },
                        {
                            TABLE: "NOFORM_DEFINED_RESOURCES",
                            NAME: "NAME"
                        }
                    ]
                }
            },
            NR: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "REQUIRED_RESOURCES",
                            NAME: "RESOURCE_NAME"
                        },
                        {
                            TABLE: "NOFORM_DEFINED_RESOURCES",
                            NAME: "NAME"
                        }
                    ]
                }
            },
            INTERVAL: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "CHILDREN",
                            NAME: "INT_NAME"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            },
            RESOURCE: {
                CREATE: {
                    METHOD: "processDefiniedResourceChange"
                },
                UPDATE: {
                    METHOD: "processDefiniedResourceChange"
                },
                DROP: {
                    METHOD: "processDefiniedResourceChange"
                }
            }
        }
    }
}
