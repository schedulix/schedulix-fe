import { ThisReceiver } from "@angular/compiler";
import { Tab } from "../data-structures/tab";
import { Crud } from "../interfaces/crud";

export class TriggerCrud extends Crud {

    private triggers: any[] = [];
    private triggeredBys: any[] = [];

    create(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    // TODO, if needed add "SHOW TRIGGER"
    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "";
        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    list(full_path: string, filter: any = null, triggered_by: boolean = false): Promise<Object> {
        let stmt: string = "";
        if (triggered_by)
            stmt = "list trigger of " + this.toolbox.namePathQuote(full_path) + ";" // Triggered By List
        else
            stmt = "list trigger for " + this.toolbox.namePathQuote(full_path) + ";" // Trigger List

        return this.commandApi.execute(stmt).then((response: any) => {
            let filtered: any[] = [];
            for (let trigg of response.DATA.TABLE) {
                if (filter == undefined || filter == null) {
                    // this.transformTrigger(trigg);
                    filtered.push(trigg);
                }
                else {
                    for (let key in filter) {
                        if (trigg[key] != undefined && trigg[key] == filter[key]) {
                            // this.transformTrigger(trigg);
                            filtered.push(trigg);
                        }
                    }
                }
            }

            response.DATA.TABLE = filtered;

            if (triggered_by)
                this.triggeredBys = response.DATA.TABLE;
            else
                this.triggers = response.DATA.TABLE;
            return response;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    drop(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    hasTrigger(triggerName: string, triggered_by: boolean = false): boolean {
        let triggerlist = triggered_by ? this.triggeredBys : this.triggers;

        if (triggerlist.length > 0) {
            for (let trigger of triggerlist) {
                if (trigger.NAME == triggerName)
                    return true;
            }
        }
        return false;

    }


    editorform() {
        throw new Error("Method not implemented.");
    }

    // private isSpecialTrigger(trigg: any) : boolean
    // {
    //     // = SYS_RESTART
    //     if (trigg.ACTION == "RERUN")
    //         return true;

    //     return false;
    // }

    // private transformTrigger(trigg: any) {
    //     // let entries = trigg.STATES.split(",");
    //     // trigg.STATES = {};
    //     // trigg.STATES.DESC = ["EXIT_STATE"],
    //     //     trigg.STATES.TABLE = []

    //     // for (let i = 0; i < entries.length; i++) {
    //     //     trigg.STATES.TABLE[i] = { "EXIT_STATE": entries[i] };
    //     // }

    //     // entries = trigg.PARAMETERS.split(",");
    //     // trigg.PARAMETERS = {};
    // trigg.PARAMETERS.DESC = ["NAME", "EXPRESSION"];
    // trigg.PARAMETERS.TABLE = [];

    // for (let i = 0; i < entries.length; i++) {

    //     let quoteParts: string[] | null = this.toolbox.quoteMatch(entries[i]);
    //     if (quoteParts != null) {
    //         let name: string = quoteParts[0];
    //         let expression: string = quoteParts.length > 1 ? quoteParts[1] : "";
    //         trigg.PARAMETERS.TABLE[i] = { NAME: name, EXPRESSION: expression };
    //     }
    // }

    // }
}
