import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import niceProfile_editorform from '../json/editorforms/niceProfile.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import { group } from "@angular/animations";

export class NiceProfileCrud extends Crud {
    createWith(mode: string, obj: any, tabInfo: Tab) {
        let withClause: string = "";
        withClause = " with\n";
        withClause += (obj.IS_ACTIVE == "true" ? "active" : "inactive");
        let entries : string[] = [];
        withClause += ",\nprofile = ";
        for (let entry of obj.ENTRIES.TABLE) {
            if (!entry.FOLDER_NAME) {
                continue;
            }
            if (!entry.RENICE) {
                entry.RENICE = "0";
            }
            entries.push(
                "folder " + this.toolbox.namePathQuote(entry.FOLDER_NAME) +
                (entry.IS_SUSPENDED == "NOSUSPEND" ? " nosuspend" : (entry.IS_SUSPENDED == "SUSPEND" ? " suspend" : " suspend restrict")) +
                " renice = " + entry.RENICE +
                (entry.ACTIVE == "true" ? " active" : " inactive")
            );
        }
        if (entries.length > 0) {
            withClause += "(\n    " + entries.join(",\n    ") + "\n)";
        }
        else {
            withClause += "NONE";
        }
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "create nice profile " + "'" + obj.NAME + "' " + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "NP", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    clone(bicsuiteObject: any, tabInfo: Tab) {
        bicsuiteObject.IS_ACTIVE = "false";
        return this.create(bicsuiteObject, tabInfo);
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        tabInfo.NAME = tabInfo.NAME.replace(/ *\[.*/, '');
        let stmt: string = "show nice profile '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let record = response.DATA.RECORD;
            let defaultTimeZone = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
            let defaultTimeFormat = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat");
            record.ACTIVE_TS = this.toolbox.convertTimeStamp(new Date(record.ACTIVE_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        // rename
        if (obj.NAME != obj.ORIGINAL_NAME && obj.NAME != 'SYSTEM') {
            // call rename
            stmt += "rename " + "nice profile" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
        }
        stmt += "alter nice profile " + "'" + obj.NAME + "' " + this.createWith("UPDATE", obj, tabInfo) + ";\n";

        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "NP", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
            }
            obj.F_REFRESH_NAVIGATOR = true;
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{nice Profile}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop nice profile " + "'" + obj.ORIGINAL_NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "NP", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    chooseFolderName(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.FOLDER_TYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }

    openFolderName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        // console.log(bicsuiteObject)
        if (bicsuiteObject.FOLDER_TYPE == "FOLDER") {
            return this.openFolder(bicsuiteObject.FOLDER_NAME);
        }
        else {
            return this.openTabForPath(bicsuiteObject.FOLDER_NAME, bicsuiteObject.FOLDER_TYPE.toLowerCase());
        }
    }


    editorform() {
        return niceProfile_editorform;
    }

    getChangeConfig() {
        return {
            FOLDER: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "ENTRIES",
                            NAME: "FOLDER_NAME"
                        }
                    ]
                }
            },
            JOB_DEFINITION: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "ENTRIES",
                            NAME: "FOLDER_NAME"
                        }
                    ]
                }
            },
           USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}
