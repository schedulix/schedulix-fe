import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import watchType_editorform from '../json/editorforms/watchType.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import { group } from "@angular/animations";

export class WatchTypeCrud extends Crud {
    createWith(mode: string, obj: any, tabInfo: Tab) {
        let withClause: string = "";
        withClause = " with\n";
        let sep: string = "";

        withClause += " parameters = (";
        for (let i = 0; i < obj.PARAMETERS.TABLE.length; i++) {
            let parameter = obj.PARAMETERS.TABLE[i];
            withClause += sep + parameter.TYPE + " '" + parameter.NAME + "'";
            if (parameter.DEFAULT_VALUE != "") {
                withClause += " ='" + this.toolbox.textQuote(parameter.DEFAULT_VALUE) + "'";
            }
            if (parameter.IS_SUBMIT_PAR == "true") {
                withClause += " submit";
            }
            sep = ", ";
        }
        withClause += ")";
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "create watch type " + "'" + obj.NAME + "' " + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "WT", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show watch type '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        // rename
        if (obj.NAME != obj.ORIGINAL_NAME && obj.NAME != 'SYSTEM') {
            // call rename
            stmt += "rename " + "watch type" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
        }
        stmt += "alter watch type " + "'" + obj.NAME + "' " + this.createWith("UPDATE", obj, tabInfo) + ";\n";

        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "WT", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Watch Type}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop watch type " + "'" + obj.ORIGINAL_NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "WT", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return watchType_editorform;
    }
}
