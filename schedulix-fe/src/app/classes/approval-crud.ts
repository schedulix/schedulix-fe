import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import approval_editorform from '../json/editorforms/approval.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { group } from "@angular/animations";
import { UntypedFormGroup } from "@angular/forms";
import { promise } from "protractor";
import { TabComponent } from "../modules/main/components/tab/tab.component";

export class ApprovalCrud extends Crud {

    commentField = {
        "NAME": "COMMENT",
        "VISIBLE_CONDITION": "conditionAuditEnabled",
        "TRANSLATE": "comment",
        "TYPE": "TEXTAREA",
        "COLUMNS": 40,
        "ROWS": 4
    };

    create(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    listApproval(): Promise<Object> {
        let stmt: string = "list approval";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        return Promise.all([
            this.listApproval()
        ]).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = {
                "DATA": {
                    "RECORD": {
                        "APPROVALS": {
                            "TABLE": response[0].response.DATA.TABLE
                        },
                        "NOFORM_AUTOREFRESH": tabInfo.AUTORELOAD,
                        "NOFORM_AUTO_REFRESH_VALUE": this.getTime(tabInfo) / 60000
                    }
                }
            };
            tabInfo.COLOR = this.setIconColor(promise.response.DATA.RECORD.APPROVALS.TABLE);
            return promise;

        });
    }

    rejectSelected(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let fields: any[] = [];
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_REJECT");
            if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                fields = fields.concat(confirmReasonFields);
                fields.push(this.commentField);
            }
        }
        return this.createConfirmDialog(fields, "reject_confirm_batch", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {

                let stmts: string[] = [];
                let commentClause = "";
                if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                    let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_REJECT");
                    let comment = this.fieldsToComment(result, confirmReasonFields);
                    if (comment != '') {
                        commentClause += " with comment = '" + this.toolbox.textQuote(comment) + "'";
                    }
                }
                for (let selected of checklistSelection._selected) {
                    let stmt = "reject " + selected.ID;
                    stmt += commentClause;
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                    this.eventEmitterService.updateTabEmitter.next(tabInfo);
                    return {};
                });
            }
            return {};
        });
    }

    approveSelected(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let fields: any[] = [];
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_APPROVE");
            if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                fields = fields.concat(confirmReasonFields);
                fields.push(this.commentField);
            }
        }
        return this.createConfirmDialog(fields, "approve_confirm_batch", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {

                let stmts: string[] = [];
                let commentClause = "";
                if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                    let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_APPROVE");
                    let comment = this.fieldsToComment(result, confirmReasonFields);
                    if (comment != '') {
                        commentClause += " with comment = '" + this.toolbox.textQuote(comment) + "'";
                    }
                }
                for (let selected of checklistSelection._selected) {
                    let stmt = "approve " + selected.ID;
                    stmt += commentClause;
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                    this.eventEmitterService.updateTabEmitter.next(tabInfo);
                    return {};
                });
            }
            return {};
        });
    }
    openApprovalSme(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openSme (bicsuiteObject, tabInfo, 'NAME'       , 'SME_ID',    arg, arg1);
    }

    openMasterSme(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openSme (bicsuiteObject, tabInfo, 'MASTER_NAME', 'MASTER_ID', arg, arg1);
    }

    openSme(bicsuiteObject: any, tabInfo: any, seNameField: string, smeIdField: string, arg?: any, arg1?: any): Promise<any> {
        let name = bicsuiteObject[seNameField];
        let path = '';
        let p = name.split('.');
        if (p.length > 1) {
            name = p.pop();
            path = p.join('.');
        }
        let tab = new Tab(name, bicsuiteObject[smeIdField], 'SUBMITTED ENTITY', 'submitted_entity', TabMode.EDIT);
        tab.PATH = path;
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    setIconColor(approvalList: any[]): string {
        let color = 'GREY';
        for (let approval of approvalList) {
            if (color == 'GREY') {
                color = 'BLUE';
            }
            if (approval.MODE == 'APPROVAL') {
                color = 'RED';  // not indicates an error but needs attention of the approver
            }
        }
        // console.log("color = " + color)
        return color;
    }

    getTime(tabInfo: Tab): number {
        let time = 60000;
        if (tabInfo.DATA) {
            if (this.checkTimeValue(tabInfo.DATA.autoreloadValue)) {
                time = tabInfo.DATA?.autoreloadValue;
            }
        }
        return time;
    }

    checkTimeValue(value: any): boolean {
        return (value && typeof value == 'number' && value > 0)
    }

    setApprovalTime(tabInfo: any, bicsuiteObject: any, form: any, getBicsuiteData?: Function): Promise<any> {
        if (getBicsuiteData) {
            bicsuiteObject = getBicsuiteData('data').response.DATA.RECORD;
            form = getBicsuiteData('form');
        }
        let autoRefreshValue = parseInt(bicsuiteObject.NOFORM_AUTO_REFRESH_VALUE);
        if (this.checkTimeValue(autoRefreshValue)) {
            if (!tabInfo.DATA) {
                tabInfo.DATA = {};
            }

            tabInfo.DATA.autoreloadValue = autoRefreshValue * 60000;
            tabInfo.DATA.autoreloadIdentifier = this.toolbox.uuidv4();
        } else {
            if (!tabInfo.DATA) {
                tabInfo.DATA = {};
            }
            tabInfo.DATA.autoreloadIdentifier = this.toolbox.uuidv4();
        }

        this.eventEmitterService.saveTabEmitter.next();
        return Promise.resolve({});
    }

    onTabLoad(data: any, tab: Tab, form: any, getBicsuiteData?: Function): Promise<any> {
        this.setApprovalTime(tab, {}, form, getBicsuiteData)
        this.autoRefreshFunctionality(tab, {}, form, getBicsuiteData);
        return Promise.resolve({})
    }

    autoRefreshFunctionality(tabInfo: Tab, bicsuiteObject: any, form?: UntypedFormGroup, getBicsuiteData?: Function) {
        // is called when tab loads and when autorefresh state was changed

        // if autoRefreshFunctionality was called from onTabLoad then the bicsuite object is not yet loaded and must be loaded from the parent component
        if (getBicsuiteData) {
            bicsuiteObject = getBicsuiteData('data').response.DATA.RECORD;
            form = getBicsuiteData('form');
        }

        let timeWhenStarted = this.getTime(tabInfo);
        let timeWhenStartedhash = tabInfo.DATA?.autoreloadIdentifier;

        setTimeout(() => {
            // evaluates if the enxt reload is possible;
            let pass = true;
            // check if a newer time exists
            if ((timeWhenStartedhash != tabInfo.DATA?.autoreloadIdentifier) || (timeWhenStarted != this.getTime(tabInfo))) {
                pass = false;
            }

            // check if tab exists
            if (tabInfo.DATA.autoreloadIdentifier == undefined) {
                pass = false;
            }

            // // check if tab is dirty
            // if (form?.dirty == true) {
            //     pass = false;
            // }

            // console.log(tabInfo.DATA.autoreloadIdentifier, timeWhenStartedhash, form?.dirty)
            // time changes abort refresh functionality
            if (pass && tabInfo.AUTORELOAD) {
                // console.log('start reading:', timeWhenStarted);
                this.read({}, tabInfo).then((result: any) => {
                    if (result?.response?.DATA?.RECORD?.APPROVALS) {
                        // replace approvals
                        bicsuiteObject.APPROVALS = result.response.DATA.RECORD.APPROVALS;
                        
                        // tell table to update
                        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    }
                });

                this.autoRefreshFunctionality(tabInfo, bicsuiteObject, form, getBicsuiteData);
            }
        }, this.getTime(tabInfo));
    }

    triggerAutoRefreshFunctionality(bicsuiteObject: any, tabInfo: any, arg: any, form: any): Promise<any> {
        // save tabs in tabinfo so that autorefresh is persistent
        // console.log(bicsuiteObject, tabInfo, form)
        if (bicsuiteObject.NOFORM_AUTOREFRESH == "true") {
            tabInfo.AUTORELOAD = true;
            this.setApprovalTime(tabInfo, bicsuiteObject, form);
            this.autoRefreshFunctionality(tabInfo, bicsuiteObject, form);
        } else {
            tabInfo.AUTORELOAD = false;
        }
        this.eventEmitterService.saveTabEmitter.next();
        return Promise.resolve({});
    }

    triggerAutoRefreshValueChange(bicsuiteObject: any, tabInfo: any, form?: any, arg1?: any): Promise<any> {
        // console.log(bicsuiteObject);
        // console.log(bicsuiteObject, tabInfo, form)
        this.setApprovalTime(tabInfo, bicsuiteObject, form);
        this.autoRefreshFunctionality(tabInfo, bicsuiteObject, form);
        return Promise.resolve({});
    }

    conditionAutoRefreshIsEnabled(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.NOFORM_AUTOREFRESH == 'true' || bicsuiteObject.NOFORM_AUTOREFRESH);
    }

    editorform() {
        return approval_editorform;
    }
}
