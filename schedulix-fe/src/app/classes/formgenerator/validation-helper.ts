import { AbstractControl, FormArray, FormGroup, ValidationErrors, ValidatorFn } from "@angular/forms";
import { ToolboxService } from "src/app/services/toolbox.service";

export class ValidationHelper {
    private initialValueHolder: any = "";
    private bicsuiteReference: any;


    constructor(
        private toolboxService: ToolboxService
    ) {

    }

    resetInitialValues(records: any) {
        this.bicsuiteReference = records;
        this.initialValueHolder = this.toolboxService.deepCopy(records);
    }

    // Custom Validator for the whole form group.
    // Is invalid when all values of the form are initial.
    initialValueValidator(): ValidatorFn {
        return (control: AbstractControl): { [key: string]: any } | null => {
            let changeDetected = false;
            if (this.initialValueHolder != null) {
                // console.log(this.initialValueHolder)
                // console.log(this.bicsuiteReference)
                // console.log(this.toolboxService.deepEquals(this.initialValueHolder, this.bicsuiteReference))
                changeDetected = !(this.toolboxService.deepEquals(this.initialValueHolder, this.bicsuiteReference));
            }

            // console.log(changeDetected ? null : { initialValue: { value: control.value } })
            return changeDetected ? null : { initialValue: { value: control.value } };
        };
    }
    checkEqualValidator(fieldName: string): ValidatorFn {
        return (control: AbstractControl): ValidationErrors | null => {
            let control2 = control.parent?.get(fieldName);
            let isSame: boolean = control?.value === control2?.value;

            if (isSame && control2?.invalid) {
                // Updates Validity in the next validation cycle. That's, why this workaround works!
                control2?.updateValueAndValidity({ onlySelf: true, emitEvent: false });
            }

            return isSame ? null : { notSame: true }
        }
    }

    getInitialValueHolder() {
        return this.initialValueHolder;
    }
}
