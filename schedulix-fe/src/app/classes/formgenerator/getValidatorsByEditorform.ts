import { ValidatorFn, Validators } from "@angular/forms";
import { ToolboxService } from "src/app/services/toolbox.service";
import { ValidationHelper } from "./validation-helper";

export class getValidatorsByEdtitorform {
    
    constructor(private toolboxService: ToolboxService) {
        
    }

    public getValidators(editorform: any): ValidatorFn[] {

        let validators: ValidatorFn[] = [];
        // Set validators, TODO refactor validators to function
        
        if (editorform.MANDATORY != undefined && (editorform.MANDATORY == true)) {
          validators.push(Validators.required);
        }
        if (editorform.MAX_SIZE != undefined && editorform.MAX_SIZE > 0) {
          validators.push(Validators.maxLength(editorform.MAX_SIZE));
        }
        if (editorform.MIN_SIZE != undefined && editorform.MIN_SIZE > 0) {
          validators.push(Validators.minLength(editorform.MIN_SIZE));
        }
        if (editorform.MIN_NUM !== undefined) {
          validators.push(Validators.min(parseInt(editorform.MIN_NUM)));
        }
        if (editorform.MAX_NUM !== undefined) {
          validators.push(Validators.max(parseInt(editorform.MAX_NUM)));
        }
        if (editorform.IDENTIFIER !== undefined && editorform.IDENTIFIER == "true") {
          validators.push(Validators.pattern("^[a-zA-Z'][\\w\\s@\-]*'?$"));
        }
        if (editorform.REGEX_PATTERN !== undefined) {
          // console.log(editorform.REGEX_PATTERN)
          validators.push(Validators.pattern(editorform.REGEX_PATTERN));
        }
        if (editorform.EQUAL_TO !== undefined) {
          validators.push(new ValidationHelper(this.toolboxService).checkEqualValidator(editorform.EQUAL_TO));
        }
        return validators;
      }
}
