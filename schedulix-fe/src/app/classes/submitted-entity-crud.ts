import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import submitted_entity_editorform from '../json/editorforms/submittedEntity.json';
import { Crud } from "../interfaces/crud";
import { trigger } from "@angular/animations";
import { stat } from "fs";
import { TriggerCrud } from "./trigger-crud";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { Tab, TabMode } from "../data-structures/tab";
import { EditorformOptions } from "../data-structures/editorform-options";
import { OutputType, SnackbarType } from "../modules/global-shared/components/snackbar/snackbar-data";

import ignore_resource_editorform from '../json/editorforms/ignoreResourceDialog.json';
import ignore_dependencies_editorform from '../json/editorforms/ignoreDependencyDialog.json';
import { JobCrud } from "./job-crud";
import { ErrorHandlerService, sdmsException } from '../services/error-handler.service';

declare var hooks: any;

export class SubmittedEntityCrud extends Crud {

    private jobCrud: JobCrud = new JobCrud(
        this.commandApi,
        this.bookmarkService,
        this.router,
        this.toolbox,
        this.eventEmitterService,
        this.privilegesService,
        this.dialog,
        this.treeFunctionService,
        this.customPropertiesService,
        this.submitEntityService,
        this.mainInformationService,
        this.clipboardService);

    create(obj: any, tabInfo?: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmts: string[] = [];
        if (obj.NOFORM_NORMALIZED_PRIVS.includes("m")) {
            var stmt : string = '';
            var sep  : string = ' set parameter on ' + obj.ID + '\n';
            for (let p of obj.PARAMETER.TABLE) {
                if (['PARAMETER', 'RESULT'].includes(p.TYPE) && p.VALUE != p.OLD_VALUE) {
                    stmt += sep + "  '" + p.NAME + "' = '" + this.toolbox.textQuote(p.VALUE) + "'";
                    sep = ',\n';
                }
            }
            if (stmt != '' && obj.NOFORM_SET_PARAMETER_COMMENT && this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                stmt += "\n  with comment = '" + this.toolbox.textQuote(obj.NOFORM_SET_PARAMETER_COMMENT) + "'";
            }
            if (stmt != '') {
                stmts.push(stmt);
            }
        }
        if (obj.NOFORM_NORMALIZED_PRIVS.includes("p")) {
            stmts.push('alter job ' + obj.ID + " with nicevalue = " + obj.NICEVALUE + ", priority = " + obj.PRIORITY );
        }

        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            obj.ID   = tabInfo.ID;
            obj.NAME = tabInfo.NAME
            obj.PATH = tabInfo.PATH;
            return response;
        });
    }

    drop(obj: any, tabInfo?: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    //   private triggerCrud: TriggerCrud = new TriggerCrud(this.commandApi, this.bookmarkService, this.router, this.toolbox, this.eventEmitterService, this.privilegesService, this.dialog);

    readExitStateProfile(job: any): Promise<Object> {
        if (job.SE_TYPE == "JOB") {
            let stmt: string = "version " + job.SE_VERSION + " show exit state profile '" + job.SE_ESP_NAME + "'";
            return this.commandApi.execute(stmt).then((response: any) => {
                let promise = new BicsuiteResponse();
                promise.response = response;
                return promise;
            });
        }
        else {
            return Promise.resolve({});
        }
    }

    readExitStateMapping(job: any): Promise<Object> {
        if (job.SE_TYPE == "JOB") {
            let stmt: string = "version " + job.SE_VERSION + " show exit state mapping '" + job.EFFECTIVE_ESM_NAME + "'";
            return this.commandApi.execute(stmt).then((response: any) => {
                let promise = new BicsuiteResponse();
                promise.response = response;
                return promise;
            });
        }
        else {
            return Promise.resolve({});
        }
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        // let stmt = "show job " + tabInfo.ID;
        // initialize audit filter options
        if (!tabInfo.DATA) {
            tabInfo.DATA = {
                NOFORM_OPERATOR_ACTION: 'true',
                NOFORM_RUNTIME_EXCEPTION: 'true',
                NOFORM_RUNTIME_INFO: 'false',
                NOFORM_RECURSIVE: 'false',
                ID: tabInfo.ID
            }
        }

        let stmt: string = this.getAuditFilterCommand(tabInfo.DATA);

        return this.commandApi.execute(stmt).then((response: any) => {

            // console.log(response)
            let childrenStates = this.submitEntityService.getChildrenStates(response.DATA.RECORD);
            // console.log(childrenStates)
            let displayState = this.submitEntityService.getDisplayState(response.DATA.RECORD, childrenStates);  // angezeigter technischer state
            // treeNode.childrenStates ? this.treeFunctionsService.evaluateState(treeNode) : treeNode.state;
            let textColor = this.submitEntityService.getTextColor(response.DATA.RECORD, response.DATA.RECORD.STATE, response.DATA.RECORD.SE_TYPE, response.DATA.RECORD.JOB_IS_FINAL, response.DATA.RECORD.JOB_IS_RESTARTABLE, response.DATA.RECORD.IS_DISABLED, childrenStates);
            let promise = new BicsuiteResponse();

            let defaultTimeZone = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
            let defaultTimeFormat = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat");
    
            let scopeResourcePathes : string[] = [];
            for (let resource of response.DATA.RECORD["REQUIRED_RESOURCES"].TABLE) {
                // console.log(resource)
                if (resource["ONLINE"] == 'false') resource["ONLINE"] = '';
                if (resource["FREE_AMOUNT"] == 'INFINITE') resource["FREE_AMOUNT"] = '';
                if (resource["TOTAL_AMOUNT"] == 'INFINITE') resource["TOTAL_AMOUNT"] = '';

                if (!resource["RESOURCE_ID"]) {
                    resource["BULK_INVISIBLE"] = true
                }
                else {
                    if (resource.RESOURCE_USAGE == "STATIC" || resource.ALLOCATE_STATE == "IGNORE") {
                        resource["BULK_DISABLED"] = true;
                    }
                }
                if (resource.RESOURCE_NAME && ['SCOPE','SERVER'].includes(resource.SCOPE_TYPE)) {
                    if (!scopeResourcePathes.includes(resource.SCOPE_NAME)) {
                        scopeResourcePathes.push(resource.SCOPE_NAME)
                    }
                }
                if (['FOLDER','JOB'].includes(resource.SCOPE_TYPE)) {
                    resource.SCOPE_NAME = resource.DEFINITION;
                }
                if (resource.EXPIRE) {
                    let eTs = new Date(resource.EXPIRE).toISOString();
                    resource.EXPIRE_UNIX = this.toolbox.mktime(eTs);
                    resource.EXPIRE = this.toolbox.convertTimeStamp(eTs, defaultTimeZone, defaultTimeFormat);
                }
                if (resource.RESOURCE_TIMESTAMP) {
                    let rTs = new Date(resource.RESOURCE_TIMESTAMP).toISOString();
                    resource.RESOURCE_TIMESTAMP_UNIX = this.toolbox.mktime(rTs);
                    resource.RESOURCE_TIMESTAMP = this.toolbox.convertTimeStamp(rTs, defaultTimeZone, defaultTimeFormat);    
                }
            }

            // remove servers and scopes which do not have required resources defined in
            if(response.DATA.RECORD["REQUIRED_RESOURCES"]) {
                let filteredRequiredResources : any[] = [];
                for (let resource of response.DATA.RECORD["REQUIRED_RESOURCES"].TABLE) {
                    if (!resource.RESOURCE_NAME && ['SCOPE','SERVER'].includes(resource.SCOPE_TYPE)) {
                        let found = false;
                        for (let path of scopeResourcePathes) {
                            if (path.startsWith(resource.SCOPE_NAME)) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            continue;
                        }
                    }
                    filteredRequiredResources.push(resource);
                }
                response.DATA.RECORD["REQUIRED_RESOURCES"].TABLE = filteredRequiredResources;
            }

            for (let resource of response.DATA.RECORD["DEFINED_RESOURCES"].TABLE) {
                // console.log(parameter["ONLINE"])
                if (resource.FREE_AMOUNT >= 0 && resource.FREE_AMOUNT != null) {
                    let percentage = ((parseInt(resource.TOTAL_AMOUNT) - parseInt(resource.FREE_AMOUNT)) / (parseInt(resource.TOTAL_AMOUNT) / 100));
                    resource.LOAD = percentage ? percentage.toString() : '0';
                }
            }

            response.DATA.RECORD.NOFORM_NORMALIZED_PRIVS =  this.privilegesService.normalize(response.DATA.RECORD.PRIVS);
            for (let parameter of response.DATA.RECORD["PARAMETER"].TABLE) {
                parameter.NOFORM_NORMALIZED_PRIVS = response.DATA.RECORD.NOFORM_NORMALIZED_PRIVS;
                parameter.OLD_VALUE = parameter.VALUE;
            }

            for (let dependency of response.DATA.RECORD["REQUIRED_JOBS"].TABLE) {
                // console.log(parameter["ONLINE"])
                dependency.SME_ID = tabInfo.ID;
                dependency.REQUIREDNAME = dependency.DD_REQUIREDNAME.split('.').pop();
                if ((dependency["IGNORE"] == 'RECURSIVE' || dependency["IGNORE"] == 'JOB_ONLY' || dependency["IGNORE"] == 'YES')) dependency["BULK_DISABLED"] = true;
            }

            if (response.DATA.RECORD['RUNS']) {
                for (let run of response.DATA.RECORD['RUNS'].TABLE) {
                    if (run.SYNC_TS) run.SYNC_TS = this.toolbox.convertTimeStamp(new Date(run.SYNC_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                    if (run.RESOURCE_TS) run.RESOURCE_TS = this.toolbox.convertTimeStamp(new Date(run.RESOURCE_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                    if (run.RUNNABLE_TS) run.RUNNABLE_TS = this.toolbox.convertTimeStamp(new Date(run.RUNNABLE_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                    if (run.START_TS) run.START_TS = this.toolbox.convertTimeStamp(new Date(run.START_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                    if (run.FINISH_TS) run.FINISH_TS = this.toolbox.convertTimeStamp(new Date(run.FINISH_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                    if (run.START_TS && run.FINISH_TS) {
                        run.RUNTIME = this.treeFunctionService.msToTime(Date.parse(run.FINISH_TS) - Date.parse(run.START_TS));
                    }
                    if (run.EXT_PID) {
                        let i = run.EXT_PID.indexOf("@");
                        if (i != -1) run.EXT_PID = run.EXT_PID.substring(0, i);
                    }
                    // else {
                    //     run.EXT_PID = "";
                    // }
                }
            }

            promise.response = response;
            let record = promise.response.DATA.RECORD;
            record.DISPLAY_STATE = displayState;
            record.DISPLAY_STATE_COLOR = textColor;
            record.NOFORM_RECURSIVE = tabInfo.DATA.NOFORM_RECURSIVE;
            record.NOFORM_OPERATOR_ACTION = tabInfo.DATA.NOFORM_OPERATOR_ACTION;
            record.NOFORM_RUNTIME_EXCEPTION = tabInfo.DATA.NOFORM_RUNTIME_EXCEPTION;
            record.NOFORM_RUNTIME_INFO = tabInfo.DATA.NOFORM_RUNTIME_INFO;
            record.COMMENT = { "TABLE": [] }
            record.NOFORM_AUDIT_TRAIL = record.AUDIT_TRAIL;
            record.AUDIT_TRAIL = undefined;
            record.OPEN_ID = record.ID;
            if (record.PROCESS_TIME) {
                record.PROCESS_TIME = this.treeFunctionService.msToTime(record.PROCESS_TIME * 1000)
            }
            if (record.ACTIVE_TIME) {
                record.ACTIVE_TIME = this.treeFunctionService.msToTime(record.ACTIVE_TIME * 1000)
            }
            if (record.IDLE_TIME) {
                record.IDLE_TIME = this.treeFunctionService.msToTime(record.IDLE_TIME * 1000)
            }
            if (record.DEPENDENCY_WAIT_TIME) {
                record.DEPENDENCY_WAIT_TIME = this.treeFunctionService.msToTime(record.DEPENDENCY_WAIT_TIME * 1000)
            }
            if (record.SUSPEND_TIME) {
                record.SUSPEND_TIME = this.treeFunctionService.msToTime(record.SUSPEND_TIME * 1000)
            }
            if (record.SYNC_TIME) {
                record.SYNC_TIME = this.treeFunctionService.msToTime(record.SYNC_TIME * 1000)
            }
            if (record.RESOURCE_TIME) {
                record.RESOURCE_TIME = this.treeFunctionService.msToTime(record.RESOURCE_TIME * 1000)
            }
            if (record.JOBSERVER_TIME) {
                record.JOBSERVER_TIME = this.treeFunctionService.msToTime(record.JOBSERVER_TIME * 1000)
            }
            if (record.RESTARTABLE_TIME) {
                record.RESTARTABLE_TIME = this.treeFunctionService.msToTime(record.RESTARTABLE_TIME * 1000)
            }
            if (record.CHILD_WAIT_TIME) {
                record.CHILD_WAIT_TIME = this.treeFunctionService.msToTime(record.CHILD_WAIT_TIME * 1000)
            }

            return promise;
        });
    }


    editorform() {
        return submitted_entity_editorform;
    }

    custom(params: any, bicsuiteObject: any, tabInfo?: Tab) {

        // console.log(bicsuiteObject)
        if (params.hasOwnProperty('OPEN_DETAIL')) {
            if (params.OPEN_DETAIL == 'OPEN_DETAIL_FROM_CRUD') {
                let monMasterTab = new Tab(bicsuiteObject.SE_NAME.split('.').pop(), bicsuiteObject.ID, 'MONITOR DETAIL', 'detail', TabMode.MONITOR_DETAIL)
                this.eventEmitterService.createTab(monMasterTab, true);
            }
        }

    }

    createOrOpenDetailTree(bicsuiteObject: any, tabInfo?: Tab): Promise<Object> {
        let monMasterTab = new Tab(bicsuiteObject.SE_NAME.split('.').pop(), bicsuiteObject.ID, 'MONITOR DETAIL', 'detail', TabMode.MONITOR_DETAIL)
        if (bicsuiteObject.ID != bicsuiteObject.MASTER_ID) {
            monMasterTab.DATA = { "ID" : bicsuiteObject.ID, "MASTER_ID" : bicsuiteObject.MASTER_ID }
        }
        this.eventEmitterService.createTab(monMasterTab, true);
        return Promise.resolve({})
    }

    openDefinition(bicsuiteObject: any, tabInfo?: Tab): Promise<Object> {
        let tab : any;
        let path = bicsuiteObject.DEFINITION.split('.');
        let name = path.pop();
        path = path.join('.')
        tab = new Tab(name, '', bicsuiteObject.SCOPE_TYPE, bicsuiteObject.SCOPE_TYPE.toLowerCase(), TabMode.EDIT);
        tab.PATH = path;
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({})
    }

    openDependency(bicsuiteObject: any, tabInfo?: Tab): Promise<Object> {
        let dependencyJobTab = new Tab(bicsuiteObject.REQUIREDNAME, bicsuiteObject.REQUIRED_ID, 'SUBMITTED ENTITY'.toLocaleUpperCase(), 'submitted_entity', TabMode.EDIT)
        dependencyJobTab.PATH = bicsuiteObject.REQUIRED_PATH
        this.eventEmitterService.createTab(dependencyJobTab, true);
        return Promise.resolve({})
    }

    openDependent(bicsuiteObject: any, tabInfo?: Tab): Promise<Object> {
        let dependencyJobTab = new Tab(bicsuiteObject.DD_DEPENDENTNAME.split('.').pop(), bicsuiteObject.DEPENDENT_ID, 'SUBMITTED ENTITY'.toLocaleUpperCase(), 'submitted_entity', TabMode.EDIT)
        dependencyJobTab.PATH = bicsuiteObject.DEPENDENT_PATH
        this.eventEmitterService.createTab(dependencyJobTab, true);
        return Promise.resolve({})
    }

    // loadAuditFilter(bicsuiteObject: any, tabInfo?: Tab): Promise<Object> {
    //     // console.log(bicsuiteObject.RECURSIVE)
    //     // console.log(bicsuiteObject.OPERATOR_ACTION)
    //     // console.log(bicsuiteObject.RUNTIME_EXCEPTION)
    //     // console.log(bicsuiteObject.RUNTIME_INFO)
    //     // console.log(bicsuiteObject)


    // }



    getAuditFilterCommand(bicsuiteObject: any): string {

        let runtime_exception_filter = ['TRIGGER FAILURE', 'RESTARTABLE', 'JOB IN ERROR', 'SET WARNING', 'UNREACHABLE'];
        let operator_actions_filter = ['RERUN RECURSIVE', 'CANCEL', 'RERUN', 'SUSPEND', 'RESUME', 'SET STATUS', 'SET EXIT STATUS', 'IGNORE DEPENDENCY',
            'IGNORE DEPENDENCY RECURSIVE', 'IGNORE RESOURCE', 'KILL', 'COMMENT', 'CHANGE PRIORITY', 'RENICE', 'IGNORE NAMED RESOURCE', 'CLEAR WARNING',
            'SET PARAMETERS', 'DISABLE', 'ENABLE', 'CLONE', 'APPROVAL REQUEST', 'REVIEW REQUEST', 'APPROVE', 'REJECT'];
        let runtime_info_filter = ['SUBMIT', 'TRIGGER SUBMIT', 'SUBMIT SUSPEND', 'TIMEOUT', 'SET RESOURCE STATUS'];

        let filter: string[] = [];

        let cmt = 'show job ' + bicsuiteObject.ID;
        // reload audit table with new filters
        if (bicsuiteObject.NOFORM_RECURSIVE == 'true' || bicsuiteObject.NOFORM_OPERATOR_ACTION == 'true' || bicsuiteObject.NOFORM_RUNTIME_EXCEPTION == 'true' || bicsuiteObject.NOFORM_RUNTIME_INFO == 'true') {
            cmt += ' with '
            if (bicsuiteObject.NOFORM_RECURSIVE == 'true') {
                cmt += ' recursive audit,'
            }
            if (bicsuiteObject.NOFORM_OPERATOR_ACTION == 'true' || bicsuiteObject.NOFORM_RUNTIME_EXCEPTION == 'true' || bicsuiteObject.NOFORM_RUNTIME_INFO == 'true') {
                cmt += ' filter = ('

                bicsuiteObject.NOFORM_OPERATOR_ACTION == 'true' ? filter = filter.concat(operator_actions_filter) : '';
                bicsuiteObject.NOFORM_RUNTIME_EXCEPTION == 'true' ? filter = filter.concat(runtime_exception_filter) : '';
                bicsuiteObject.NOFORM_RUNTIME_INFO == 'true' ? filter = filter.concat(runtime_info_filter) : '';

                cmt += this.toolbox.arrayRemoveDublicates(filter).join(',')
                cmt += '),'
            }
            // removes last ","
            cmt = cmt.slice(0, -1);
        }
        return cmt;
    }

    applyAuditCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        return !(bicsuiteObject.NOFORM_RECURSIVE == 'false' && bicsuiteObject.NOFORM_OPERATOR_ACTION == 'false' && bicsuiteObject.NOFORM_RUNTIME_EXCEPTION == 'false' && bicsuiteObject.NOFORM_RUNTIME_INFO == 'false')
    }

    applyAuditFilter(bicsuiteObject: any, tabInfo?: Tab): Promise<Object> {

        if (tabInfo) {
            tabInfo.DATA.NOFORM_OPERATOR_ACTION = bicsuiteObject.NOFORM_OPERATOR_ACTION;
            tabInfo.DATA.NOFORM_RUNTIME_EXCEPTION = bicsuiteObject.NOFORM_RUNTIME_EXCEPTION;
            tabInfo.DATA.NOFORM_RUNTIME_INFO = bicsuiteObject.NOFORM_RUNTIME_INFO;
            tabInfo.DATA.NOFORM_RECURSIVE = bicsuiteObject.NOFORM_RECURSIVE;
        }
    
        let cmt: string = this.getAuditFilterCommand(bicsuiteObject);

        return this.commandApi.execute(cmt).then((result: any) => {
            // console.log(result.DATA.RECORD.AUDIT_TRAIL)
            if (result.hasOwnProperty("ERROR")) {
                return result;
            } else if (result.hasOwnProperty("DATA")) {
                // console.log(result)
                bicsuiteObject.NOFORM_AUDIT_TRAIL = result.DATA.RECORD.AUDIT_TRAIL

                // 1 for changed rows -> update table
                return 1
            }
        })
    }

    isBlocked(bicsuiteObject :any) : boolean {
        if (bicsuiteObject.ALLOCATE_STATE == 'BLOCKED') {
            return true;
        }
        else {
            return false;
        }
    }

    allocateStateColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        return (this.isBlocked(bicsuiteObject) ? ' crud-color-red' : ' crud-color-green');
    }

    isOffline(bicsuiteObject :any) : boolean {
        if (bicsuiteObject.ONLINE != 'true') {
            return true;
        }
        else {
            return false;
        }
    }

    onlineColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        return (this.isOffline(bicsuiteObject) ? ' crud-color-red' : ' crud-color-green');
    }

    isLowAmount(bicsuiteObject :any) : boolean {
        if (bicsuiteObject.FREE_AMOUNT == 'INFINITE' || bicsuiteObject.ALLOCATE_STATE != 'BLOCKED' || bicsuiteObject.REQUESTED_AMOUNT == bicsuiteObject.RESERVED_AMOUNT || 
            parseInt(bicsuiteObject.FREE_AMOUNT) >= (parseInt(bicsuiteObject.REQUESTED_AMOUNT) - parseInt(bicsuiteObject.RESERVED_AMOUNT) + parseInt(bicsuiteObject.ALLOCATED_AMOUNT))
        ) {
            return false;
        }
        else {
            return true;
        }
    }

    freeAmountColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        return (this.isLowAmount(bicsuiteObject) ? ' crud-color-red' : ' crud-color-green');
    }

    isBadState(bicsuiteObject :any) : boolean {
        if (bicsuiteObject.REQUESTED_STATES && !bicsuiteObject.REQUESTED_STATES.split(',').includes(bicsuiteObject.RESOURCE_STATE)) {
            return true;
        }
        else {
            return false;
        }
    }

    requestedStateColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        return (this.isBadState(bicsuiteObject) ? ' crud-color-red' : ' crud-color-green');
    }

    isExpired(bicsuiteObject :any) : boolean {
        if (bicsuiteObject.RESOURCE_TIMESTAMP && bicsuiteObject.EXPIRE && 
            ((bicsuiteObject.EXPIRE_SIGN == '+' && bicsuiteObject.RESOURCE_TIMESTAMP_UNIX < bicsuiteObject.EXPIRE_UNIX) || 
             (bicsuiteObject.EXPIRE_SIGN == '-' && bicsuiteObject.RESOURCE_TIMESTAMP_UNIX > bicsuiteObject.EXPIRE_UNIX))
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    expiredColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        return (this.isExpired(bicsuiteObject) ? ' crud-color-red' : ' crud-color-green');
    }

    lockModeColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        if (this.isOffline(bicsuiteObject) ||
            this.isLowAmount(bicsuiteObject) ||
            this.isBadState(bicsuiteObject) ||
            this.isExpired(bicsuiteObject)
            ) {
            return '';
        }
        if (bicsuiteObject.LOCKMODE == 'N' || bicsuiteObject.LOCKMODE == bicsuiteObject.ALLOCATED_LOCKMODE) {
            return ' crud-color-green';
        }
        else {
            return ' crud-color-red';
        }
    }

    displayStateColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        let cssFlag = ''
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_PURPLE') {
            cssFlag += ' crud-color-purple'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_BLUE') {
            cssFlag += ' crud-color-blue'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_GREEN') {
            cssFlag += ' crud-color-green'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_RED') {
            cssFlag += ' crud-color-red'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_YELLOW') {
            cssFlag += ' crud-color-yellow'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_ORANGE') {
            cssFlag += ' crud-color-orange'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_BROWN') {
            cssFlag += ' crud-color-brown'
        }
        if (bicsuiteObject.DISPLAY_STATE_COLOR == 'TEXT_GREY') {
            cssFlag += ' crud-color-grey'
        }
        return cssFlag
    }

    openIDColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        // openID is always blue
        let cssFlag = ''
        cssFlag += ' crud-color-blue';
        return cssFlag
    }

    openErrorLogfile(bicsuiteObj: any, tabInfo: any, form: any, selectionModel: any): Promise<any> {
      bicsuiteObj.logUrl = this.toolbox.makeLogPath(bicsuiteObj.HTTPHOST, bicsuiteObj.HTTPPORT)
      this.toolbox.OpenTextWindow(bicsuiteObj.ERRLOGFILE, bicsuiteObj)
      return Promise.resolve({})
    }

    openLogfile(bicsuiteObj: any, tabInfo: any, form: any, selectionModel: any): Promise<any> {
      bicsuiteObj.logUrl = this.toolbox.makeLogPath(bicsuiteObj.HTTPHOST, bicsuiteObj.HTTPPORT)
      this.toolbox.OpenTextWindow(bicsuiteObj.LOGFILE, bicsuiteObj)
      return Promise.resolve({})
    }

    dependencyColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        // dependencys have no IS_DISABLED or IS_RESTARTABLE attribute
        let cssFlag = ''
        cssFlag += ' ' + this.submitEntityService.getTextColor(bicsuiteObject, bicsuiteObject.JOB_STATE, bicsuiteObject.DD_REQUIREDTYPE, bicsuiteObject.JOB_IS_FINAL, '', '', this.submitEntityService.getChildrenStates(bicsuiteObject));
        return cssFlag
    }


    default() {
        return {
            DATA: {
                RECORD: {
                    TRIGGER_SYS_RESTART_STATES: {
                        TABLE: [
                            {
                                "FROM_STATE": null,
                                "ID": "",
                                "TO_STATE": ""
                            }
                        ],
                        DESC: [
                            "ID",
                            "FROM_STATE",
                            "TO_STATE"
                        ]
                    }

                }
            }
        };
    }

    isMasterIdCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.MASTER_ID == bicsuiteObject.ID;
    }

    
    conditionIsSuspendable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.IS_SUSPENDED == 'NOSUSPEND' ||
            this.privilegesService.isAdmin()
        ) &&
            bicsuiteObject.STATE != 'FINAL' &&
            bicsuiteObject.STATE != 'CANCELLED' &&
            bicsuiteObject.IS_CANCELLED != 'true';
    }

    conditionIsResumeable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.IS_SUSPENDED != 'NOSUSPEND' &&
            (bicsuiteObject.IS_SUSPENDED != 'ADMINSUSPEND' || this.privilegesService.isAdmin()) &&
            bicsuiteObject.STATE != 'FINAL' && bicsuiteObject.STATE != 'CANCELLED');
    }

    conditionIsResumeableDialog(editorformField: any, dialogData: any, tabInfo: Tab): boolean {
        return this.conditionIsResumeable(editorformField, dialogData.bicsuiteObject, tabInfo);
    }

    conditionIsCancelable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (!['CANCELLED', 'FINAL'].includes(bicsuiteObject.STATE) && bicsuiteObject.IS_CANCELLED != 'true');
    }

    conditionIsScopeOrServer(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (['SERVER', 'SCOPE'].includes(bicsuiteObject.SCOPE_TYPE));
    }

    conditionIsStatechangeable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.JOB_IS_RESTARTABLE == 'true' ||
            (bicsuiteObject.SE_TYPE == 'JOB' && bicsuiteObject.STATE == 'FINISHED' && bicsuiteObject.JOB_IS_FINAL == 'false') ||
            (bicsuiteObject.SE_TYPE == 'JOB' && ['SUSPEND', 'ADMINSUSPEND'].includes(bicsuiteObject.IS_SUSPENDED) && !['STARTING', 'STARTED', 'RUNNING', 'TO_KILL', 'KILLED', 'BROKEN_ACTIVE'].includes(bicsuiteObject.STATE)));
    }

    conditionIsKillable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let result : boolean = ((bicsuiteObject.STATE == 'RUNNING' || 
                 (['FINISHED','KILLED'].includes(bicsuiteObject.STATE) && bicsuiteObject.JOB_IS_RESTARTABLE == "false")) &&
                 bicsuiteObject.SE_KILL_PROGRAM != 'NONE' && bicsuiteObject.SE_KILL_PROGRAM != '' && bicsuiteObject.SE_KILL_PROGRAM != null);
        return (result);
    }

    conditionIsRerunable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.STATE != 'SCHEDULED' &&
            (bicsuiteObject.JOB_IS_RESTARTABLE == 'true' || bicsuiteObject.CNT_RESTARTABLE != '0')
        ) {
            return true;
        }
        else {
            return false;
        }
    }

    conditionIsRerunableRecursive(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        // console.log(bicsuiteObject)
        if (bicsuiteObject.ID) {
            // bicsuitOBject is a show job record
            if (bicsuiteObject.STATE != 'SCHEDULED' && bicsuiteObject.CNT_RESTARTABLE != '0') {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            // bicsuite is a bulk operation list from list job compressed
            // console.log(bicsuiteObject)
            let result: boolean = false
            for (let node of bicsuiteObject) {
                if (bicsuiteObject.STATE == 'SCHEDULED' || bicsuiteObject.CNT_RESTARTABLE == '0') {
                    result = false;
                    break;
                }
                else {
                    result = true;
                }
            }
            return result;
        }
    }

    conditionCanEnable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !(bicsuiteObject.STATE != 'DEPENDENCY_WAIT' || bicsuiteObject.IS_PARENT_DISABLED == 'true' || 
            bicsuiteObject.IS_DISABLED == 'false' ||
            bicsuiteObject.RERUN_SEQ != '0' ||
            bicsuiteObject.CNT_RUNNABLE != '0' ||
            bicsuiteObject.CNT_STARTING != '0' ||
            bicsuiteObject.CNT_STARTED != '0' ||
            bicsuiteObject.CNT_RUNNING != '0' ||
            bicsuiteObject.CNT_TO_KILL != '0' ||
            bicsuiteObject.CNT_KILLED != '0' ||
            bicsuiteObject.CNT_FINISHED != '0' ||
            bicsuiteObject.CNT_FINAL != '0' ||
            bicsuiteObject.CNT_BROKEN_ACTIVE != '0' ||
            bicsuiteObject.CNT_BROKEN_FINISHED != '0' ||
            bicsuiteObject.CNT_ERROR != '0' ||
            bicsuiteObject.CNT_UNREACHABLE != '0' ||
            bicsuiteObject.CNT_RESTARTABLE != '0' ||
            bicsuiteObject.CNT_PENDING != '0');
    }

    conditionCanDisable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !(bicsuiteObject.STATE != 'DEPENDENCY_WAIT' ||
            bicsuiteObject.IS_DISABLED == 'true' ||
            bicsuiteObject.IS_PARENT_DISABLED == 'true' ||
            bicsuiteObject.RERUN_SEQ != '0' ||
            bicsuiteObject.CNT_RUNNABLE != '0' ||
            bicsuiteObject.CNT_STARTING != '0' ||
            bicsuiteObject.CNT_STARTED != '0' ||
            bicsuiteObject.CNT_RUNNING != '0' ||
            bicsuiteObject.CNT_TO_KILL != '0' ||
            bicsuiteObject.CNT_KILLED != '0' ||
            bicsuiteObject.CNT_FINISHED != '0' ||
            bicsuiteObject.CNT_FINAL != '0' ||
            bicsuiteObject.CNT_BROKEN_ACTIVE != '0' ||
            bicsuiteObject.CNT_BROKEN_FINISHED != '0' ||
            bicsuiteObject.CNT_ERROR != '0' ||
            bicsuiteObject.CNT_UNREACHABLE != '0' ||
            bicsuiteObject.CNT_RESTARTABLE != '0' ||
            bicsuiteObject.CNT_PENDING != '0');
    }

    conditionIsCloneable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.READ_MASTER_SUBMITABLE == 'true';
    }

    conditionIsIgnoredDependency(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject["IGNORE"] == 'RECURSIVE' || bicsuiteObject["IGNORE"] == 'JOB_ONLY' || bicsuiteObject["IGNORE"] == 'YES');
    }

    conditionResumeAt(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESUME_TYPE == "AT" && (!bicsuiteObject.SUSPEND || bicsuiteObject.SUSPEND == "true");
    }

    conditionResumeIn(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESUME_TYPE == "IN" && (!bicsuiteObject.SUSPEND || bicsuiteObject.SUSPEND == "true");
    }

    conditionRerunSuspend(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SUSPEND == "true";
    }

    conditionDialogDisplayRerunRecursiveCheckbox(editorformField: any, dialogData: any, tabInfo: Tab): boolean {
        return dialogData.bicsuiteObject.SE_TYPE == 'JOB' && dialogData.bicsuiteObject.CHILDREN.TABLE.length > 0;
    }

    conditionDialogDisplayRerunSuspend(editorformField: any, dialogData: any, tabInfo: Tab): boolean {
        return dialogData.RECURSIVE != "true" && dialogData.bicsuiteObject.JOB_IS_RESTARTABLE == 'true';
    }

    conditionDialogIsSuspended(editorformField: any, dialogData: any, tabInfo: Tab): boolean {
        return dialogData.SUSPEND == "true" && dialogData.RECURSIVE != "true";
    }

    conditionStateError(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.STATE == "ERROR";
    }

    conditionApprovalPending(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.APPROVAL_PENDING == "true";
    }

    conditionHasWarning(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.WARN_COUNT != "0" && bicsuiteObject.STATE != 'CANCELLED';
    }

    conditionIsJob(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SE_TYPE == 'JOB';
    }

    conditionRequiredIsSme(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.STATE == 'DEFERRED') {
            return false;
        }
        return true;
    }

    conditionDependentIsSelf(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.DEPENDENT_ID == bicsuiteObject.SME_ID) {
            return false;
        }
        return true;
    }

    conditionParameterEdited(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        for (let parameter of bicsuiteObject.PARAMETER.TABLE) {
            if (parameter.OLD_VALUE != parameter.VALUE) {
                 return true;
            }
        }
        return false;
    }

    conditionDependencyIsOk(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.STATE == "FULFILLED") {
            return true;
        }
        return false;
    }

    conditionCanEditParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.NOFORM_NORMALIZED_PRIVS.includes("m") && ["RESULT", "PARAMETER"].includes(bicsuiteObject.TYPE);
    }

    conditionCanEditPriority(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        console.log(bicsuiteObject.NOFORM_NORMALIZED_PRIVS)
        return bicsuiteObject.NOFORM_NORMALIZED_PRIVS.includes("p");
    }

    isDependencyOKClass(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        let cssFlag = ''
        // if FULFILLED then green else red
        // WIP TODO DIETER wann OK Nicht OK ??
        if (bicsuiteObject.STATE == 'FULFILLED') {
            cssFlag += ' crud-color-green'
        } else {
            // cssFlag += ' crud-color-red'; // does not work !!!
            cssFlag += ' RED'; // workaround
        }
        return cssFlag
    }

    // isDependencyOKColor(editorformField: any, bicsuiteObjectP: any, tabInfo: Tab): any {
    //     let css = ''
    //     css += 'fill: blue !importand';
    //     return css
    // }

    openVersion(bicsuiteObj: any, tabInfo: any, parentBicsuiteObject: any): Promise<any> {
        // console.log(bicsuiteObj);
        // console.log(tabInfo)
        let namestr= bicsuiteObj.SE_NAME.split('.');
        let name = namestr.pop();
        let path = namestr.join('.');
        let seTab = new Tab(name, bicsuiteObj.SE_VERSION + '&' + bicsuiteObj.SE_NAME, ('VERSIONED ' + bicsuiteObj.SE_TYPE).toUpperCase(), ('version_' + bicsuiteObj.SE_TYPE).toLowerCase(), TabMode.EDIT);
        seTab.PATH = path;
        seTab.DATA = {
            VERSION: bicsuiteObj.SE_VERSION
        };
        seTab.VERSIONED = true;
        this.eventEmitterService.createTab(seTab, true);
        return Promise.resolve({})
    }

    openDefinedResourceItemFromTree(bicsuiteObj: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        // console.log(node);
        let definedResourceTab = new Tab(node.data.RESOURCE_NAME.split('.').pop(),node.data.ID, 'RESOURCE'.toUpperCase(), node.data.RESOURCE_USAGE.toLowerCase(), TabMode.EDIT);
        definedResourceTab.DATA = {};
        definedResourceTab.DATA.TYPE =  node.data.TYPE;
        definedResourceTab.DATA.ID =  node.data.ID;
        definedResourceTab.DATA.RESOURCE_NAME =  node.data.NAME;
        // mandatory for resource instance
        definedResourceTab.DATA.RESOURCE_ID = node.data.ID;
        definedResourceTab.DATA.f_IsInstance = true;
        definedResourceTab.DATA.f_isScopeOrFolderInstance = false;
        definedResourceTab.DATA.RESOURCE_OWNER_TYPE = 'submitted_entity'; // tabInfo.TYPE;

        // definedResourceTab.OWNER = bicsuiteObject.PATH + '.' + node.data.NAME;
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    };

    openIgnoreDependencies(bicsuiteObj: any, tabInfo: any, form: any, selectionModel: any): Promise<any> {
        // console.log(selectionModel.selected)

        let bicsuiteObject = this.toolbox.deepCopy(bicsuiteObj);

        let dataObj: any = {
            SELECTED_ROWS: selectionModel.selected,
            ID: bicsuiteObject.ID,
            MODE: 'RECURSIVE',
            AUDIT_COMMENT: ''
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(ignore_dependencies_editorform, dataObj, tabInfo, "Ignore Dependencies", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        // * Note, dialogs unsubsrcibe autoamtically!
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            // console.log(result);
            if (result != undefined || result != null && result) {
                // console.log(result)
                if (result.hasOwnProperty('ERROR')) {
                    return 1;
                } else {
                    this.eventEmitterService.pushSnackBarMessage([result.FEEDBACK], SnackbarType.SUCCESS);
                    this.reload(bicsuiteObject, tabInfo)
                    return 0;
                }
                // });
            }
            return 1;
        });
    }

    ignoreDependency(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        // alter job 1806098 WITH IGNORE DEPENDENCY = (1806102 RECURSIVE), comment = 'ggd'
        // console.log(bicsuiteObject)
        let selectedIds = [];
        if (bicsuiteObject.SELECTED_ROWS.length > 0) {
            let cmt: string = 'alter job ' + bicsuiteObject.ID
                + ' with ignore dependency ';


            cmt += '= (';
            for (let o of bicsuiteObject.SELECTED_ROWS) {
                // console.log(o)
                selectedIds.push(o.ID);
            }
            cmt += selectedIds.join(',');
            cmt += ' ' + bicsuiteObject.MODE == 'RECURSIVE' ? 'recursive' : '';
            cmt += ')';
            if (bicsuiteObject.AUDIT_COMMENT != '') {
                cmt += ",  comment = '" + this.toolbox.textQuote(bicsuiteObject.AUDIT_COMMENT) + "'";
            }

            return this.commandApi.execute(cmt).then((result1: any) => {
                return result1;
            });
        }

        // nothing selected
        return Promise.resolve(1);

    }
    openIgnoreResources(bicsuiteObj: any, tabInfo: any, form: any, selectionModel: any): Promise<any> {

        console.warn("WIP REWORK DIETER")
        // console.log(selectionModel.selected)

        let bicsuiteObject = this.toolbox.deepCopy(bicsuiteObj);

        let dataObj: any = {
            SELECTED_ROWS: selectionModel.selected,
            ID: bicsuiteObject.ID,
            AUDIT_COMMENT: ''
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(ignore_resource_editorform, dataObj, tabInfo, "Ignore Resources", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        // * Note, dialogs unsubsrcibe autoamtically!
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            // console.log(result);
            if (result != undefined || result != null && result) {
                // console.log(result)
                if (result.hasOwnProperty('ERROR')) {
                    return 1;
                } else {
                    this.eventEmitterService.pushSnackBarMessage([result.FEEDBACK], SnackbarType.SUCCESS);
                    this.reload(bicsuiteObject, tabInfo)
                    return 0;
                }
                // });
            }
            return 0;
        });
    }
    ignoreResource(bicsuiteObject: any, tabInfo: any): Promise<any> {
        console.warn("WIP REWORK DIETER")
        // alter job 1806098 WITH IGNORE DEPENDENCY = (1806102 RECURSIVE), comment = 'ggd'
        // console.log(bicsuiteObject)
        let selectedIds = [];
        if (bicsuiteObject.SELECTED_ROWS.length > 0) {
            let cmt: string = 'alter job ' + bicsuiteObject.ID
                + ' with ignore resource ';


            cmt += '= (';
            for (let o of bicsuiteObject.SELECTED_ROWS) {
                // console.log(o)
                selectedIds.push(o.RESOURCE_ID);
            }
            cmt += selectedIds.join(',');
            cmt += ')';
            if (bicsuiteObject.AUDIT_COMMENT != '') {
                cmt += ",  comment = '" + this.toolbox.textQuote(bicsuiteObject.AUDIT_COMMENT) + "'";
            }

            return this.commandApi.execute(cmt).then((result1: any) => {
                return result1;
            });
        }

        // nothing selected
        return Promise.resolve(1);
    }

    // ---------------------- BUTTON METHODS ----------------------------

    commentField = {
        "NAME": "COMMENT",
        "VISIBLE_CONDITION": "conditionAuditEnabled",
        "TRANSLATE": "comment",
        "TYPE": "TEXTAREA",
        "COLUMNS": 40,
        "ROWS": 4
    };

    manipulateFormField(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        return this.jobCrud.manipulateFormField(bicsuiteObject, tabInfo, editorForm);
    }

    resumeFields = [
        {
            "NAME": "RESUME_AT",
            "REGEX_PATTERN": "\\d{4}-[01]\\d-[0-3]\\dT[0-2]\\d:[0-5]\\d:[0-5]\\d",
            "HIDDEN": true,
            "TRANSLATE": "resume_time",
            "DEFAULT": "",
            "TYPE": "INPUT",
            "SIZE": 16,
            "VISIBLE_CONDITION": "conditionResumeAt"
        },
        {
            "NAME": "RESUME_IN",
            "REGEX_PATTERN": "^[0-9]*$",
            "HIDDEN": true,
            "TRANSLATE": "resume_in",
            "TYPE": "INPUT",
            "DEFAULT": "5",
            "SIZE": 16,
            "VISIBLE_CONDITION": "conditionResumeIn"
        },
        {
            "NAME": "RESUME_IN_UNIT",
            "TYPE": "SELECT",
            "TRANSLATE": "resume_in_unit",
            "ON_CHANGE": "REFRESH_CONDITIONS",
            "DEFAULT": "MINUTE",
            "VALUES": [
                "MINUTE",
                "HOUR",
                "DAY",
                "WEEK",
                "MONTH",
                "YEAR"
            ],
            "USE_LABEL": true,
            "HIDDEN": false,
            "VISIBLE_CONDITION": "conditionResumeIn"
        }
    ];

    // --- SUSPEND --------------------------------------------------------------------------------------------------------------

    createSuspendConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let initDataObj: any = {};
        let fields: any[] = [
            {
                "NAME": "SUSPEND_LOCAL",
                "TYPE": "CHECKBOX",
                "TRANSLATE": "suspend_local",
                "USE_LABEL": true
            }
        ];
        if (this.privilegesService.isAdmin()) {
            fields.push(
                {
                    "NAME": "SUSPEND_RESTRICTED",
                    "TYPE": "CHECKBOX",
                    "TRANSLATE": "suspend_restricted",
                    "USE_LABEL": true
                }
            );
        }
        fields.push(
            {
                "NAME": "RESUME_TYPE",
                "TYPE": "SELECT",
                "TRANSLATE": "resume",
                "ON_CHANGE": "REFRESH_CONDITIONS",
                "DEFAULT": "NONE",
                "VALUES": [
                    "NONE",
                    "AT",
                    "IN"
                ],
                "USE_LABEL": true
            }
        );
        fields = fields.concat(this.resumeFields);
        if (!batch) {
            let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_SUSPEND", bicsuiteObject, initDataObj);
            fields = fields.concat(confirmInfoFields);
        }
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_SUSPEND");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        let message = "";
        if (batch) {
            message = "suspend_confirm_batch";
        }
        else {
            message = "suspend_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject, initDataObj);
    }

    createSuspendCommand(dialogData: any, tabInfo: any): string {
        let stmt = '';
        stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with suspend";
        // console.log(dialogData.bicsuiteObject);
        if (dialogData.SUSPEND_LOCAL == "true") {
            stmt += " local";
        }
        if (dialogData.SUSPEND_RESTRICTED == "true") {
            stmt += " restrict";
        }
        if (dialogData.RESUME_TYPE == 'AT') {
            stmt += ", resume at '" + dialogData.RESUME_AT + "'";
        }
        else if (dialogData.RESUME_TYPE == 'IN') {
            stmt += ", resume in " + dialogData.RESUME_IN + " " + dialogData.RESUME_IN_UNIT;
        }
        else {
            stmt += ", noresume";
        }
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_SUSPEND");
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
            }
        }
        return stmt;
    }

    doSuspendEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.suspend != undefined) {
            hookResult = hooks.suspend(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Suspend Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createSuspendCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    async doSuspendEntities(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let dialogDataEntity = this.toolbox.deepCopy(dialogData);
        let entities = dialogData.bicsuiteObject;   // contains list of objets to suspend
        for (let entity of entities) {
            dialogDataEntity.bicsuiteObject = entity;
            // to pass await a rejection has to be defined
            await this.doSuspendEntity(dialogDataEntity, tabInfo, editorForm).then((result: any) => {}, this.emptyRejection);
        }
        this.eventEmitterService.pushSnackBarMessage([entities.length + " Submitted Enitities have been suspended."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
        return Promise.resolve({});
    }

    suspendEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createSuspendConfirmDialog(bicsuiteObject, tabInfo, false, "doSuspendEntity");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo);
                this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been suspended."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
            }
            return 1;
        });
    }

    // --- KILL --------------------------------------------------------------------------------------------------------------

    killEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let fields: any[] = [];
        let confirmReasonFields : any[] = [];
        let initDataObj : any = {};
        let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_KILL", bicsuiteObject, initDataObj);
        fields = fields.concat(confirmInfoFields);
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            confirmReasonFields = this.getConfirmReasonFields("CONFIRM_KILL");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        return this.createConfirmDialog(fields,
            "kill_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}{" + bicsuiteObject.ID + "}", tabInfo, "", undefined, initDataObj).afterClosed().toPromise().then((result: any) => {
                if (result != undefined || result != null && result) {
                    let stmt = '';
                    stmt = 'alter job ' + bicsuiteObject.ID + " with kill";
                    if (result.RECURSIVE == "true") {
                        stmt += " recursive";
                    }
                    if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                        let comment = this.fieldsToComment(result, confirmReasonFields);
                        if (comment != '') {
                            stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
                        }
                    }
                    return this.commandApi.execute(stmt).then((result) => {
                        this.reload(bicsuiteObject, tabInfo)
                        this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been killed."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                        return result
                    })
                } else {
                    return Promise.resolve({});
                }
            }
            );
    }

    // --- RESUME --------------------------------------------------------------------------------------------------------------

    createResumeConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let fields: any[] = [];
        let initDataObj : any = {};
        if (!batch) {
            let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_RESUME", bicsuiteObject, initDataObj);
            fields = fields.concat(confirmInfoFields);
        }
        fields = fields.concat([
            {
                "NAME": "RESUME_TYPE",
                "TYPE": "SELECT",
                "TRANSLATE": "resume",
                "ON_CHANGE": "REFRESH_CONDITIONS",
                "DEFAULT": "IMMEDIATE",
                "VALUES": [
                    "IMMEDIATE",
                    "AT",
                    "IN"
                ],
                "USE_LABEL": true
            }
        ]);
        fields = fields.concat(this.resumeFields);
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_RESUME");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        let message = "";
        if (batch) {
            message = "resume_confirm_batch";
        }
        else {
            message = "resume_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject, initDataObj);
    }

    createResumeCommand(dialogData: any, tabInfo: any): string {
        let stmt = '';
        stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with resume";
        if (dialogData.RESUME_TYPE == 'AT') {
            if (dialogData.RESUME_AT == undefined) {
                dialogData.RESUME_AT == '';
            }
            stmt += " at '" + dialogData.RESUME_AT + "'";
        }
        if (dialogData.RESUME_TYPE == 'IN') {
            stmt += " in " + dialogData.RESUME_IN + " " + dialogData.RESUME_IN_UNIT;
        }
        let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_RESUME");
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
            }
        }
        return stmt;
    }

    doResumeEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.resume != undefined) {
            hookResult = hooks.resume(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Resume Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createResumeCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    async doResumeEntities(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let dialogDataEntity = this.toolbox.deepCopy(dialogData);
        let entities = dialogData.bicsuiteObject;   // contains list of objets to resume
        for (let entity of entities) {
            let tabInfo: Tab = new Tab(entity.name, entity.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
            await this.read(null, tabInfo).then(async (result: any) => {
                dialogDataEntity.bicsuiteObject = result.response.DATA.RECORD;
                await this.doResumeEntity(dialogDataEntity, tabInfo, editorForm).then((result: any) => {
                }, this.emptyRejection
                );
            });
        }
        this.eventEmitterService.pushSnackBarMessage([entities.length + " Submitted Enitities have been resumed."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
        return Promise.resolve({});
    }

    resumeEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createResumeConfirmDialog(bicsuiteObject, tabInfo, false, "doResumeEntity");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
                this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been resumed."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
            }
            return 1;
        });
    }

    // --- CANCEL --------------------------------------------------------------------------------------------------------------

    createCancelConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let fields: any[] = [];
        let initDataObj : any = {};
        if (!batch) {
            let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_CANCEL", bicsuiteObject, initDataObj);
            fields = fields.concat(confirmInfoFields);
        }
        fields.push(
            {
                "NAME": "KILL",
                "TRANSLATE": "kill",
                "TYPE": "CHECKBOX",
                "USE_LABEL": true
            }
        );
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_CANCEL");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        let message = "";
        if (batch) {
            message = "cancel_confirm_batch";
        }
        else {
            message = "cancel_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject, initDataObj);
    }

    createCancelCommand(dialogData: any, tabInfo: any): string {
        let stmt = '';
        stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with cancel";
        if (dialogData.KILL == "true") {
            stmt += ", kill";
        }
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_CANCEL");
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
            }
        }
        return stmt;
    }

    doCancelEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.cancel != undefined) {
            hookResult = hooks.cancel(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Cancel Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createCancelCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    async doCancelEntities(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let dialogDataEntity = this.toolbox.deepCopy(dialogData);
        let entities = dialogData.bicsuiteObject;   // contains list of objets to cancel
        for (let entity of entities) {
            let tabInfo: Tab = new Tab(entity.name, entity.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
            await this.read(null, tabInfo).then(async (result: any) => {
                dialogDataEntity.bicsuiteObject = result.response.DATA.RECORD;
                await this.doCancelEntity(dialogDataEntity, tabInfo, editorForm);
            });
        }
        this.eventEmitterService.pushSnackBarMessage([entities.length + " Submitted Enitities have been cancelled."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
        return Promise.resolve({});
    }

    cancelEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createCancelConfirmDialog(bicsuiteObject, tabInfo, false, "doCancelEntity");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
                this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been cancelled."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
            }
            return 1;
        });
    }

    // --- SET STATE --------------------------------------------------------------------------------------------------------------
    // TODO

    async createSetStateConfirmDialog(bicsuiteObject: any, tabInfo: Tab, method: string) {
        await this.readExitStateProfile(bicsuiteObject).then((response: any) => {
            let profileStates: any[] = [];
            for (let state of response.response.DATA.RECORD.STATES.TABLE) {
                profileStates.push({ "STATE": state.ESD_NAME, "MAPPED": false });
            }
            bicsuiteObject.ESP_STATES = profileStates;
            bicsuiteObject.DEFAULT_ESM_NAME = response.response.DATA.RECORD.DEFAULT_ESM_NAME;
        });
        bicsuiteObject.EFFECTIVE_ESM_NAME = bicsuiteObject.DEFAULT_ESM_NAME;
        if (bicsuiteObject.SE_ESM_NAME != "<default>") {
            bicsuiteObject.EFFECTIVE_ESM_NAME = bicsuiteObject.SE_ESM_NAME;
        }
        await this.readExitStateMapping(bicsuiteObject).then((response: any) => {
            let profileStates: any[] = [];
            for (let range of response.response.DATA.RECORD.RANGES.TABLE) {
                for (let state of bicsuiteObject.ESP_STATES) {
                    if (state.STATE == range.ESD_NAME) {
                        state.MAPPED = true;
                    }
                }
            }
        });
        let values = [];
        let labels = [];
        for (let state of bicsuiteObject.ESP_STATES) {
            if (state.MAPPED) {
                labels.push(state.STATE);
            }
            else {
                labels.push("[" + state.STATE + "]");
            }
            values.push(state.STATE);
        }
        let parameterSequenceStr: string = "";
        let fields: any[] = [];
        let initDataObj : any = {};
        let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_SET_STATE", bicsuiteObject, initDataObj);
        fields = fields.concat(confirmInfoFields);
        fields = fields.concat([
            {
                "NAME": "STATE",
                "TYPE": "SELECT",
                "TRANSLATE": "state",
                "VALUES": values,
                "LABELS": labels,
                "DEFAULT": bicsuiteObject.JOB_ESD_ID,
                "USE_LABEL": true
            },
            {
                "NAME": "FORCE",
                "TYPE": "CHECKBOX",
                "TRANSLATE": "force",
                "USE_LABEL": true
            },
            {
                "NAME": "RESUME",
                "TYPE": "CHECKBOX",
                "TRANSLATE": "resume",
                "VISIBLE_CONDITION" : "conditionIsResumeableDialog",
                "USE_LABEL": true
            }
        ]);
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {

            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_SET_STATE");
            if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                fields = fields.concat(confirmReasonFields);
                fields.push(this.commentField);
            }
        }
        return this.createConfirmDialog(fields, "setState_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}", tabInfo, method, bicsuiteObject, initDataObj);
    }

    createSetStateCommand(dialogData: any, tabInfo: any, recursive: boolean): string {
        let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_SET_STATE");
        let stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with exit state = '" + dialogData.STATE + "'";
        if (dialogData.FORCE == "true") {
            stmt += " force";
        }
        // console.log(dialogData.bicsuiteObject);
        if (dialogData.bicsuiteObject.RERUN_SEQ) {
            stmt += ", run = " + dialogData.bicsuiteObject.RERUN_SEQ;
        }
        let commentClause = "";
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                commentClause = ", comment = '" + this.toolbox.textQuote(comment) + "'";
                stmt += commentClause;
            }
        }
        if (dialogData.RESUME == "true") {
            let multiCommand  = "begin multicommand\n"
            multiCommand += stmt + ";\n";
            multiCommand += "alter job " + dialogData.bicsuiteObject.ID + " with resume" + commentClause + ";\n"
            multiCommand += "end multicommand"
            stmt = multiCommand;
        }
        return stmt;
    }

    doSetStateEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.setState != undefined) {
            hookResult = hooks.setState(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Set State Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createSetStateCommand(dialogData, tabInfo, false);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    setStateEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.createSetStateConfirmDialog(bicsuiteObject, tabInfo, "doSetStateEntity").then((result: any) => {
            return result.afterClosed().toPromise().then((result: any) => {
                this.reload(bicsuiteObject, tabInfo)
                this.eventEmitterService.pushSnackBarMessage(["State of Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been set."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                return 1;
            });
        });
    }

    // --- RERUN --------------------------------------------------------------------------------------------------------------

    async readJobDefinition(bicsuiteObject: any, batch: boolean) {
        if (!batch) {
            let stmt: string = "show job definition " + this.toolbox.namePathQuote(bicsuiteObject.SE_NAME);
            await this.commandApi.execute(stmt).then((response: any) => {
                bicsuiteObject.SE_PARAMETER = response.DATA.RECORD.PARAMETER;
                return response;
            });
        }
        return null;
    }

    getParamValue(bicsuiteObject: any, name: string) {
        for (let param of bicsuiteObject.PARAMETER.TABLE) {
            if (param.NAME == name) {
                return param.VALUE;
            }
        }
        return "";
    }

    async createRerunConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let initDataObj : any = {};
        // TODO: das kann etwas dauern also eine sanduhr oder sowas anzeigen
        let rerunParameters: any[] = [];
        if (!batch) {
            await this.readJobDefinition(bicsuiteObject, batch);
            let parameterSequenceStr: string = "";
            for (let parameter of bicsuiteObject.SE_PARAMETER.TABLE) {
                this.jobCrud.parseForWebConfig(parameter);
                if (parameter.WEBCONFIG != undefined && parameter.WEBCONFIG.EDIT_ON_RERUN == "true") {
                    let value = this.getParamValue(bicsuiteObject, parameter.NAME);
                    let rerunParameter = {
                        "NAME": parameter.NAME,
                        "EXPRESSION": value,
                        "COMMENT": parameter.COMMENT,
                        "SEQ": -1,
                        "WEBCONFIG": parameter.WEBCONFIG
                    };
                    rerunParameters.push(rerunParameter);
                }
                if (parameter.NAME == "PARAMETER_SEQUENCE") {
                    parameterSequenceStr = parameter.DEFAULT_VALUE;
                }
            }
            if (rerunParameters.length > 0 && parameterSequenceStr != "") {
                let defaultIndex = 10000;
                let parameterSequence = parameterSequenceStr.split(",");
                for (let parameter of rerunParameters) {
                    let index = parameterSequence.indexOf(parameter.NAME);
                    if (index == -1) {
                        parameter.SEQ = defaultIndex;
                    }
                    else {
                        parameter.SEQ = index;
                    }
                }
                rerunParameters = rerunParameters.sort(
                    function (a: any, b: any) {
                        if (a.SEQ == b.SEQ) {
                            return a.NAME < b.NAME ? -1 : 1;
                        }
                        else {
                            return a.SEQ - b.SEQ;
                        }
                    }
                );
            }
        }
        bicsuiteObject.RERUN_PARAMETER = {
            "DESC": ["NAME", "EXPRESSION", "COMMENT"],
            "TABLE": rerunParameters
        };
        let fields: any[] = [];
        if (!batch) {
            let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_RERUN", bicsuiteObject, initDataObj);
            fields = fields.concat(confirmInfoFields);
        }
        fields.push ({
            "NAME": "RECURSIVE",
            "TYPE": "CHECKBOX",
            "TRANSLATE": "recursive",
            "USE_LABEL": true,
            "VISIBLE_CONDITION" : "conditionDialogDisplayRerunRecursiveCheckbox",
            "ON_CHANGE": "REFRESH_CONDITIONS"
        });
        fields.push ({
            "NAME": "SUSPEND",
            "TYPE": "CHECKBOX",
            "TRANSLATE": "suspend",
            "USE_LABEL": true,
            "VISIBLE_CONDITION" : "conditionDialogDisplayRerunSuspend",
            "ON_CHANGE": "REFRESH_CONDITIONS"
        });
        fields.push ({
            "NAME": "SUSPEND_LOCAL",
            "TYPE": "CHECKBOX",
            "TRANSLATE": "suspend_local",
            "USE_LABEL": true,
            "VISIBLE_CONDITION" : "conditionDialogIsSuspended"
        });
        if (this.privilegesService.isAdmin()) {
            fields.push({
                "NAME": "SUSPEND_RESTRICTED",
                "TYPE": "CHECKBOX",
                "TRANSLATE": "suspend_restricted",
                "USE_LABEL": true,
                "VISIBLE_CONDITION" : "conditionDialogIsSuspended"
            });
        }
        fields.push({
            "NAME": "RESUME_TYPE",
            "TYPE": "SELECT",
            "TRANSLATE": "resume",
            "ON_CHANGE": "REFRESH_CONDITIONS",
            "DEFAULT": "NONE",
            "VALUES": [
                "NONE",
                "AT",
                "IN"
            ],
            "USE_LABEL": true,
            "VISIBLE_CONDITION" : "conditionDialogIsSuspended"
        })
        fields = fields.concat(this.resumeFields);
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_RERUN");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        // console.log(bicsuiteObject)
        // console.log(fields)
        if (rerunParameters.length > 0) {
            fields.push(
                {
                    "NAME": "RERUN_PARAMETER",
                    "TRANSLATE": "parameters",
                    "TYPE": "TABLE",
                    "TABLE": {
                        "FIELDS": [
                            {
                                "NAME": "NAME",
                                "TYPE": "TEXT",
                                "TRANSLATE": "name",
                                "USE_LABEL": false
                            },
                            {
                                "NAME": "EXPRESSION",
                                "TYPE": "TEXTAREA",
                                "COLUMNS": 40,
                                "TRANSLATE": "value",
                                "USE_LABEL": false,
                                "DISPLAY_METHOD": "manipulateFormField"
                            },
                            {
                                "NAME": "COMMENT",
                                "TYPE": "TEXT",
                                "TRANSLATE": "comment",
                                "DISPLAY_MODIFIER": "REMOVE_WEBCONFIG",
                                "USE_LABEL": false
                            }
                        ]
                    },
                    "USE_LABEL": false
                }
            );
        }
        let message = "";
        if (batch) {
            message = "rerun_confirm_batch";
        }
        else {
            message = "rerun_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}";
        }
        initDataObj.RERUN_PARAMETER = bicsuiteObject["RERUN_PARAMETER"];
        if (batch || bicsuiteObject.CHILDREN.TABLE.length > 0 || bicsuiteObject.SE_TYPE != 'JOB') {
            initDataObj.RECURSIVE = "true";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject, initDataObj);
    }

    createRerunCommand(dialogData: any, tabInfo: any): string {
        // console.log(dialogData)
        let stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with rerun";
        if (dialogData.RECURSIVE == "true") {
            stmt += " recursive";
        }
        if (dialogData.SUSPEND == "true") {
            stmt += ", suspend";
        }
        if (dialogData.SUSPEND_LOCAL == "true") {
            stmt += " local";
        }
        if (dialogData.SUSPEND_RESTRICTED == "true") {
            stmt += " restrict";
        }
        if (dialogData.RESUME_TYPE == 'AT') {
            stmt += ", resume at '" + dialogData.RESUME_AT + "'";
        }
        if (dialogData.RESUME_TYPE == 'IN') {
            stmt += ", resume in " + dialogData.RESUME_IN + " " + dialogData.RESUME_IN_UNIT;
        }
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_RERUN");
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
            }
        }
        // console.log(dialogData);
        if (dialogData.RERUN_PARAMETER.TABLE.length > 0) {
            let multiCommand = 'begin multicommand\n';
            let sep = 'set PARAMETER ON ' + dialogData.bicsuiteObject.ID + '\n';
            for (let o of dialogData.RERUN_PARAMETER.TABLE) {
                multiCommand += sep + "'" + o.NAME + "' = '" + this.toolbox.textQuote(o.EXPRESSION) + "'";
                sep = ",\n";
            }
            multiCommand += ";\n" + stmt;
            multiCommand += ";\nend multicommand";
            stmt = multiCommand;
        }
        return stmt;
    }

    async doRerunEntitiesRecursive(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let dialogDataEntity = this.toolbox.deepCopy(dialogData);
        let entities = dialogData.bicsuiteObject;   // contains list of objects to cancel
        for (let entity of entities) {
            let tabInfo: Tab = new Tab(entity.name, entity.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
            await this.read(null, tabInfo).then(async (result: any) => {
                dialogDataEntity.bicsuiteObject = result.response.DATA.RECORD;
                await this.doRerunEntity(dialogDataEntity, tabInfo, editorForm);
            });
        }
        this.eventEmitterService.pushSnackBarMessage([entities.length + " Submitted Enitities have been restarted."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
        return Promise.resolve({});
    }

    doRerunEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.rerun != undefined) {
            hookResult = hooks.rerun(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Rerun Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createRerunCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    rerunEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.createRerunConfirmDialog(bicsuiteObject, tabInfo, false, "doRerunEntity").then((result: any) => {
            return result.afterClosed().toPromise().then((result: any) => {

                // reload only if sth was returned (not closed or ESC)
                if (result) {
                    this.reload(bicsuiteObject, tabInfo)
                    this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been restarted."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                }
                return 1;
            });
        });
    }

    // --- ENABLE --------------------------------------------------------------------------------------------------------------

    createEnableConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let fields: any[] = [];
        let initDataObj : any = {};
        if (!batch) {
            let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_ENABLE", bicsuiteObject, initDataObj);
            fields = fields.concat(confirmInfoFields);
        }
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_ENABLE");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        let message = "";
        if (batch) {
            message = "enable_confirm_batch";
        }
        else {
            message = "enable_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject, initDataObj);
    }

    createEnableCommand(dialogData: any, tabInfo: any): string {
        let stmt = '';
        stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with enable";
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_CANCEL");
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
            }
        }
        return stmt;
    }

    doEnableEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.enable != undefined) {
            hookResult = hooks.enable(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Enable Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createEnableCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    async doEnableEntities(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let dialogDataEntity = this.toolbox.deepCopy(dialogData);
        let entities = dialogData.bicsuiteObject;   // contains list of objets to enable
        for (let entity of entities) {
            let tabInfo: Tab = new Tab(entity.name, entity.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
            await this.read(null, tabInfo).then(async (result: any) => {
                dialogDataEntity.bicsuiteObject = result.response.DATA.RECORD;
                await this.doEnableEntity(dialogDataEntity, tabInfo, editorForm);
            });
        }
        this.eventEmitterService.pushSnackBarMessage([entities.length + " Submitted Enitities have been enabled."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
        return Promise.resolve({});
    }

    enableEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createEnableConfirmDialog(bicsuiteObject, tabInfo, false, "doEnableEntity");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
                this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been enabled."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
            }
            return 1;
        });
    }
    
    // --- DISABLE --------------------------------------------------------------------------------------------------------------

    createDisableConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let fields: any[] = [];
        let initDataObj : any = {};
        if (!batch) {
            let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_DISABLE", bicsuiteObject, initDataObj);
            fields = fields.concat(confirmInfoFields);
        }
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_DISABLE");
            fields = fields.concat(confirmReasonFields);
            fields.push(this.commentField);
        }
        let message = "";
        if (batch) {
            message = "disable_confirm_batch";
        }
        else {
            message = "disable_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject, initDataObj);
    }

    createDisableCommand(dialogData: any, tabInfo: any): string {
        let stmt = '';
        stmt = 'alter job ' + dialogData.bicsuiteObject.ID + " with disable";
        if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_ENABLE");
            let comment = this.fieldsToComment(dialogData, confirmReasonFields);
            if (comment != '') {
                stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
            }
        }
        return stmt;
    }

    doDisableEntity(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.disable != undefined) {
            hookResult = hooks.disable(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Disable Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createDisableCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    async doDisableEntities(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let dialogDataEntity = this.toolbox.deepCopy(dialogData);
        let entities = dialogData.bicsuiteObject;   // contains list of objets to disable
        for (let entity of entities) {
            let tabInfo: Tab = new Tab(entity.name, entity.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
            await this.read(null, tabInfo).then(async (result: any) => {
                dialogDataEntity.bicsuiteObject = result.response.DATA.RECORD;
                await this.doDisableEntity(dialogDataEntity, tabInfo, editorForm);
            });
        }
        this.eventEmitterService.pushSnackBarMessage([entities.length + " Submitted Enitities have been disabled."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
        return Promise.resolve({});
    }

    disableEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createEnableConfirmDialog(bicsuiteObject, tabInfo, false, "doDisableEntity");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
                this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been disabled."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
            }
            return 1;
        });
    }

    // --- CLONE --------------------------------------------------------------------------------------------------------------

    cloneEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let initDataObj : any = {};
        let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_CLONE", bicsuiteObject, initDataObj);
        let fields = confirmInfoFields;
        fields.push(this.commentField);
        return this.createConfirmDialog(fields, "clone_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}", tabInfo, "", undefined, initDataObj).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = 'alter job ' + bicsuiteObject.ID + " with clone";
                if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                    let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_CLONE");
                    let comment = this.fieldsToComment(result, confirmReasonFields);
                    if (comment != '') {
                        stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
                    }
                }
                return this.commandApi.execute(stmt).then((result) => {
                    this.reload(bicsuiteObject, tabInfo)
                    this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been clones."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                    return result
                })
            }
            return Promise.resolve({});
        });
    }

    // --- CLEAR WARNING --------------------------------------------------------------------------------------------------------------

    doClearWarning(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let hookResult : any = {};
        if (hooks.clearWarning != undefined) {
            hookResult = hooks.clearWarning(dialogData, tabInfo);
            if (hookResult.hasOwnProperty("ERROR")) {
                let exception: sdmsException = new sdmsException(hookResult.ERROR.ERRORMESSAGE, 'Clear Warning Hook', hookResult.ERROR.ERRORCODE);
                let errorHandlerService = new ErrorHandlerService(this.dialog,this.toolbox);
                errorHandlerService.createError(exception);
                return Promise.reject(hookResult);
            }
        }
        let stmt = this.createCancelCommand(dialogData, tabInfo);
        return this.commandApi.execute(stmt).then((result) => {
            return result;
        });
    }

    clearWarningEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let initDataObj : any = {};
        let confirmInfoFields = this.getConfirmInfoFields("CONFIRM_CLEAR_WARNING", bicsuiteObject, initDataObj);
        let fields = confirmInfoFields;
        fields.push(this.commentField);
        return this.createConfirmDialog(fields, "clear_warning_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}", tabInfo, "", undefined, initDataObj).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = 'alter job ' + bicsuiteObject.ID + " with clear warning";
                if (this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
                    let confirmReasonFields = this.getConfirmReasonFields("CONFIRM_CLONE");
                    let comment = this.fieldsToComment(result, confirmReasonFields);
                    if (comment != '') {
                        stmt += ", comment = '" + this.toolbox.textQuote(comment) + "'";
                    }
                }
                return this.commandApi.execute(stmt).then((result) => {
                    this.reload(bicsuiteObject, tabInfo)
                    this.eventEmitterService.pushSnackBarMessage(["Warnings on Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " have been cleared."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                    return result
                })
            }
            return Promise.resolve({});
        });
    }

    // --- AUDIT COMMENT --------------------------------------------------------------------------------------------------------------

    auditCommentEntity(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([this.commentField], "auditComment_confirm{" + bicsuiteObject.SE_NAME.split('.').pop() + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = '';
                stmt = 'alter job ' + bicsuiteObject.ID + "with comment = '" + this.toolbox.textQuote(result.COMMENT) + "'";
                return this.commandApi.execute(stmt).then((result) => {
                    this.reload(bicsuiteObject, tabInfo)
                    this.eventEmitterService.pushSnackBarMessage(["Submitted Enitity ", bicsuiteObject.SE_NAME.split('.').pop(), " has been commented."], SnackbarType.INFO, OutputType.SPACE_SEPARATED);
                    return result
                })
            }
            return Promise.resolve({});
        });
    }
}
