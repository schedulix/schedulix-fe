import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import rsd_editorform from '../json/editorforms/rsd.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";

export class ResourceStateDefinitionCrud extends Crud {

    // Soll eine neue Resource erstellen
    // Bekommt ein bicsuiteObject als parameter
    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create resource state definition '" + obj.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSD", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    // Soll eine neue Resource lesen
    // Bekommt ein  als parameter Tabobjekt
    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show resource state definition '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("resource state definition");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("resource state definition");
            }
            promise.response = response;
            return promise;
        });
    }
    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt: string = "rename resource state definition '" + obj.ORIGINAL_NAME + "' to '" + obj.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSD", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
            obj.F_REFRESH_NAVIGATOR = true;
            return response;
        });
    }
    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Resource State Definition}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop resource state definition '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSD", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return rsd_editorform;
    }

    getChangeConfig() {
        return {
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
