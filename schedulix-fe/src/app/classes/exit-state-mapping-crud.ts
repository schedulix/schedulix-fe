import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import esm_editorform from '../json/editorforms/esm.json';
import { Tab, TabMode } from "../data-structures/tab";
import { Crud } from "../interfaces/crud";
import { ConditionalExpr } from "@angular/compiler";
import { ExitStateDefinitionCrud } from "./exit-state-definition-crud";

export class ExitStateMappingCrud extends Crud {

    private sort(obj: any) {
        let tableRef = obj.RANGES.TABLE;
        if (tableRef.length <= 0)
            return;

        // remove empties
        for (let i = 0; i < tableRef.length; i++) {
            if (tableRef[i].ECR_START === "") {
                tableRef.splice(i, 1);
                i--;
            }
        }

        // sort valid values
        tableRef.sort(function (a: any, b: any) {
            return a.ECR_START - b.ECR_START;
        });

        for (let i = 0; i < tableRef.length - 1; i++) {
            let curr = tableRef[i];
            let next = tableRef[i + 1];

            curr.ECR_END = next.ECR_START - 1;
        }

        tableRef[0].ECR_START = -2147483648; // = -2^31. <==> Min int for 32 BIT
        tableRef[tableRef.length - 1].ECR_END = 2147483647 // = 2^31 - 1 <==> Max int for 32 BIT;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        this.sort(obj);
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create exit state mapping '" + obj.NAME + "'";
        let ranges = obj.RANGES;

        if (ranges != undefined && ranges != null) {
            stmt += " with map = (";
            for (let i = 0; i < ranges.TABLE.length; i++) {
                if (i == 0) {
                    stmt += "'" + ranges.TABLE[i].ESD_NAME + "'";
                }
                else {
                    stmt += ranges.TABLE[i].ECR_START + ", '" + ranges.TABLE[i].ESD_NAME + "'";
                }
                stmt += i == ranges.TABLE.length - 1 ? "" : ",";
            }
            stmt += ")";
        }

        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESM", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "SHOW EXIT STATE MAPPING '" + tabInfo.NAME + "'";
        // execute returns a promise which can be used to fetch the response.
        return this.commandApi.execute(stmt).then((response: any) => {

            let promise = new BicsuiteResponse();
            promise.response = response;
            tabInfo.ID = response.DATA.RECORD.ID;
            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("exit state mapping");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("exit state mapping");
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        this.sort(obj);
        let stmt = "begin multicommand\n";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename " + "exit state mapping" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';";
            obj.F_REFRESH_NAVIGATOR = true;
        }
        let rangeLength = obj.RANGES.TABLE.length;
        stmt += "alter exit state mapping " + "'" + obj.NAME + "'";
        stmt += " with map = (";
        for (let i = 0; i < rangeLength; i++) {
            let item = obj.RANGES.TABLE[i];
            if (i == 0)
                stmt += " '" + item.ESD_NAME + "'";
            else
                stmt += " " + item.ECR_START + ", '" + item.ESD_NAME + "'";
            stmt += i < rangeLength - 1 ? "," : "";
        }
        stmt += "); end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESM", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Exit State Mapping}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop exit state mapping '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESM", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    openEsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.ESD_NAME);
    }

    conditionIsFirstMapping (editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.rowIndex == 0;
    }

    editorform() {
        return esm_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["RANGES"] = {
            TABLE: [{
                ESD_NAME: "",
                ECR_START: -2147483648,
                ECR_END: 2147483647,
                NOFORM_DISABLE_AUTOCHOOSE: "true"
            }],
            DESC: [
                "ESD_NAME",
                "ECR_START",
                "ECR_END",
                "NOFORM_DISABLE_AUTOCHOOSE"]
        };
        return bicsuiteObj;
    }

    getChangeConfig() {
        return {
            ESD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RANGES",
                            NAME: "ESD_NAME"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}
