import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import group_editorform from '../json/editorforms/group.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import { type } from "os";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";

export class GroupCrud extends Crud {

    mprivs: string[] = [
        "environment",
        "exit state definition",
        "exit state mapping",
        "exit state profile",
        "exit state translation",
        "footprint",
        "group",
        "nice profile",
        "resource state definition",
        "resource state mapping",
        "resource state profile",
        "select",
        "system",
        "user"
    ];

    genUpdateManagePrivs(mode: string, obj: any, tabInfo: Tab) {
        if (this.privilegesService.isEdition('BASIC') || !this.conditionIsAdmin({},{},tabInfo)) {
            // no Manage privs in BASIC
            return '';
        }
        let stmts: string = '';
        for (let priv of obj.MANAGE_PRIVS.TABLE) {
            let grant: boolean = priv.CHECKED == "true";
            if (!grant) {
                if (mode != "CREATE") {
                    stmts += "revoke manage " + priv.PRIVS + " from '" + obj.NAME + "';\n";
                }
            }
            else {
                stmts += "grant manage " + priv.PRIVS + " to '" + obj.NAME + "';\n";
            }
        }
        return stmts;
    }

    createWith(mode: string, obj: any, tabInfo: Tab) {
        let isAdmin: boolean = this.privilegesService.isAdmin();
        let withClause: string = " with\n";
        let sep: string = "(";

        withClause += " users = ";
        for (let i = 0; i < obj.USERS.TABLE.length; i++) {
            let g = obj.USERS.TABLE[i];
            withClause += sep + "'" + g.NAME + "'";
            sep = ", ";
        }
        if (sep != "(")
            withClause += ")";
        else
            withClause += "NONE";

        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        stmt += "create group " + "'" + obj.NAME + "' " + this.createWith("CREATE", obj, tabInfo) + ";";
        stmt += this.genUpdateManagePrivs("CREATE", obj, tabInfo);
        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "GROUP", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    readGroup(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show group '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {

            let promise = new BicsuiteResponse();
            let privs = [];
            for (let priv of this.mprivs) {
                let grant: any = response.DATA.RECORD.MANAGE_PRIVS.TABLE.find((line: any) => line.PRIVS == priv);
                privs.push({ 'PRIVS': priv, 'CHECKED': grant ? "true" : "false" })
            }
            response.DATA.RECORD.MANAGE_PRIVS.TABLE = privs;
            promise.response = response;
            return promise;
        });
    }

    readGrants(obj: any, tabInfo: Tab): Promise<Object> {
        let iconMap = {
            ENVIRONMENT: 'env',
            FOLDER: 'folder',
            GROUP: 'group',
            JOB_DEFINITION_BATCH: 'batch',
            JOB_DEFINITION_JOB: 'job',
            JOB_DEFINITION_MILESTONE: 'milestone',
            NAMED_RESOURCE_CATEGORY: 'category',
            NAMED_RESOURCE_STATIC: 'static',
            NAMED_RESOURCE_SYSTEM: 'system',
            NAMED_RESOURCE_SYNCHRONIZING: 'synchronizing',
            NAMED_RESOURCE_POOL: 'pool',
            RESOURCE_STATIC: 'static',
            RESOURCE_SYSTEM: 'system',
            RESOURCE_SYNCHRONIZING: 'synchronizing',
            RESOURCE_POOL: 'pool',
            SCOPE_SERVER: 'server',
            SCOPE_SCOPE: 'scope',
            INTERVAL: 'interval'
        };
        let stmt: string = "list grant for '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            let grants: any[] = [];
            for (let grant of response.DATA.TABLE) {
                if (grant.NAME == '')
                    continue;
                let iconlookup: string = grant.TYPE;
                if (grant.SUBTYPE != '')
                    iconlookup += '_' + grant.SUBTYPE;
                let lookup = iconlookup as keyof typeof iconMap;
                // console.log(lookup)
                // console.log(iconMap[lookup])
                grants.push({ 'ICONLOOKUP': iconMap[lookup], 'NAME': grant.NAME, 'PRIVS': grant.PRIVS });
            }
            response.DATA.TABLE = grants;
            promise.response = response;
            return promise;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        return Promise.all([
            // console.log('alter systemparameters and userParameters resolved'),
            this.readGroup(obj, tabInfo),
            this.readGrants(obj, tabInfo)
        ]).then((response: any) => {
            let promise = new BicsuiteResponse();
            tabInfo.ID = response[0].response.DATA.RECORD.ID;
            promise.response = response[0].response;
            // console.log(promise.response);
            promise.response.DATA.DESC.push('GRANTS');
            promise.response.DATA.RECORD.GRANTS = {
                'DESC': ['TYPE', 'SUBTYPE', 'NAME', 'PRIVS'], 'TABLE': response[1].response.DATA.TABLE
            };
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename " + "group" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
            obj.F_REFRESH_NAVIGATOR = true;
        }
        if (obj.NAME != 'PUBLIC') {
            stmt += "alter group " + "'" + obj.NAME + "' " + this.createWith("UPDATE", obj, tabInfo) + ";\n";
        }
        stmt += this.genUpdateManagePrivs("UPDATE", obj, tabInfo);
        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "GROUP", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Group}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop group " + "'" + obj.NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "GROUP", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    openUserName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openUser(bicsuiteObject.NAME);
    }

    openDefaultGroup(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openGroup(bicsuiteObject.DEFAULT_GROUP);
    }

    conditionIsPublicOrNew(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.NAME == "PUBLIC" || this.conditionIsNew(null, bicsuiteObject, tabInfo);
    }

    conditionCanAddMember(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (!this.privilegesService.hasManagePriv("GROUP")) {
            return false;
        }
        if (tabInfo.NAME == "PUBLIC") {
            return false;
        }
        return true;
    }

    conditionCanDropMember(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (!this.privilegesService.hasManagePriv("GROUP")) {
            return false;
        }
        if (tabInfo.NAME == "PUBLIC") {
            return false;
        }
        if (["SYSTEM","INTERNAL"].includes(bicsuiteObject.NAME) && tabInfo.NAME == "ADMIN") {
            return false;
        }
        if (bicsuiteObject.DEFAULT_GROUP == tabInfo.NAME) {
            return false;
        }
        return true;
    }

    conditionIsNewMember(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.IS_NEW ? bicsuiteObject.IS_NEW : false;
    }

    editorform() {
        return group_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        let privs = [];
        for (let priv of this.mprivs) {
            privs.push({ 'PRIVS': priv, 'CHECKED': "false" })
        }
        bicsuiteObj.DATA.RECORD["MANAGE_PRIVS"] = { "TABLE" : privs };
        return bicsuiteObj;
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)
        // console.log(bicsuiteObject)

        let grantObject = {
            TYPE: 'GROUP',
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.ORIGINAL_NAME,
            // grants with hierarchy logic gets 'hierarchy' otherwise 'flat' for no parent structured objects
            GRANTTYPE: 'flat',
            PRIVS:"",
            INHERIT_EFFECTIVE_PRIVS: '',
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;

                // read inherit parent grant
                if (grantObject.GRANTS.TABLE[0] && grantObject.GRANTTYPE != 'flat') {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    // grantObject.GRANTS.TABLE[0].OWNER = 'Inherit grant form parent';

                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        // console.log(i)
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            // console.log("merge inheritance pirvs to the root object")
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }

                // console.log(grantObject);

            }
            let editorForm = crud.editorformByType(grantObject.TYPE, false, false)
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for " + bicsuiteObject.TYPE + " " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }

    getChangeConfig() {
        return {
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                        {
                            TABLE: "USERS",
                            NAME: "NAME"
                        },
                    ]
                }
            }
        }
    }

}
