import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import esd_editorform from '../json/editorforms/esd.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";


export class ExitStateDefinitionCrud extends Crud {

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create exit state definition '" + obj.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESD", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show exit state definition '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {



            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;
            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("exit state definition");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("exit state definition");
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt: string = "rename exit state definition '" + obj.ORIGINAL_NAME + "' to '" + obj.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESD", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
            obj.F_REFRESH_NAVIGATOR = true;
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Exit State Definition}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop exit state definition '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ESD", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return esd_editorform;
    }

    getChangeConfig() {
        return {
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
