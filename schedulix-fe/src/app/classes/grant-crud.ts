import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import { Tab } from "../data-structures/tab";
import { Crud } from "../interfaces/crud";



import grant_editorform from '../json/editorforms/editGrants.json';
import { hierarchyTypedObjects, objectTypeMap } from "../maps/ObjectTypemap";
import { privilegeMap, privilegeMapByObjectType } from "../maps/privilegeMap";
import { SnackbarType } from "../modules/global-shared/components/snackbar/snackbar-data";

export class GrantCrud extends Crud {

    create(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    read(obj: any, tabInfo: Tab): Promise<Object> {
        // console.log(obj)
        // console.log(tabInfo)

        //map types...
        //add correct type in map.ts to use grant crud
        let type = objectTypeMap.get(obj.TYPE)

        if (!type) {
            console.warn("create type in ObjectTypeMap.ts")

        }
        // deafutl assumption is no hierarchy object name strings such as groups instead of SYSTEM.FODLDER.BATCH
        let isHieararchyObject = type ? hierarchyTypedObjects.get(type): false;
        // if (['BATCH', 'MILESTONE', 'JOB'].includes(obj.TYPE)) {
        //     type = 'JOB DEFINITION'
        // }

        let stmt: string = "list grant on " + type + " " + (isHieararchyObject ? this.toolbox.namePathQuote(obj.NAME) : "'" + obj.NAME + "'") + "";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (response.DATA) {
                let filteredGrants : any[] = []
                for (let grant of response.DATA.TABLE) {
                    if (grant.PRIVS == '' 
                     && (grant.ORIGIN == null || grant.ORIGIN == 'NONE')
                     && !(grant.GROUP == null || grant.GROUP == '' || grant.GROUP == 'NONE')
                     && grant.GROUP == obj.OWNER
                    ) {
                        continue;
                    }
                    // this.parsePrivs(grant.PRIVS)
                    // grantPrivs will override existing properties, with the same name,
                    // from the decomposition of grant
                    this.parsePrivs(grant.PRIVS, grant, obj)
                    // this.parsePrivs(grant.EFFECTIVE_PRIVS, grant, obj)
                    let effectivPrivsObject = {}
                    this.parsePrivs(grant.INHERITED_PRIVS, effectivPrivsObject, obj)
                    grant.EFFECTIVE_PRIVS_OBJECT = effectivPrivsObject;
                    // console.log(grant)
                    filteredGrants.push(grant)
                }
                response.DATA.TABLE = filteredGrants;
            }
            // console.log(response)
            return response;
        });
    }

    parsePrivs(privs: string, grantsObject: any, obj: any): any {
        privs = this.privilegesService.normalize(privs)
        // let checkBoxMap: any =  {}




        let typeSpecificMap = privilegeMapByObjectType.get(objectTypeMap.get(obj.TYPE)!)

        // console.log(typeSpecificMap)
        // set all PRIVS with value = {}
        if (typeSpecificMap) {
            for (let v of Array.from(typeSpecificMap.values())) {
                if (grantsObject[v]) console.warn("Object property " + v + "already exist")
                grantsObject[v] = "false";
                grantsObject['ORIGINAL_' + v] = "false";
            }

            for (const c of privs) {
                // console.log()
                // console.log(c)
                let privilege: string | undefined = typeSpecificMap.get(c);
                if (privilege) {
                    // console.log(privilege)
                    grantsObject[privilege] = "true";
                    grantsObject['ORIGINAL_' + privilege] = "true";
                } else {
                    if (obj.TYPE != "INTERVAL" || c != "V") {
                        console.warn("Privilege shortcut " + c + " is not in privilegeMap")
                    }
                }
            }
        } else {
            throw new Error("typeSpecificMap does not exist")
        }

        // console.log(grantsObject)
        return grantsObject
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {

        let stmt = 'BEGIN MULTICOMMAND \n ';


        let typeSpecificMap = privilegeMapByObjectType.get(objectTypeMap.get(obj.TYPE)!)
        // console.log(typeSpecificMap)
        // console.log(obj.TYPE)
        let isHieararchyObject = obj.TYPE ? hierarchyTypedObjects.get(objectTypeMap.get(obj.TYPE)!): false;
        // console.log(isHieararchyObject)
        if (typeSpecificMap) {
            for (let grantColumn of obj.GRANTS.TABLE) {

                // if(grantColumn.ORIGIN) {

                // }
                // set inherit grants
                if (grantColumn.GROUP == 'Inherit grant form parent') {
                    let grantList: string[] = [];
                    for (let entry of Object.entries(grantColumn)) {
                        if (Array.from(typeSpecificMap.values()).includes(entry[0]) && (entry[1] == true || entry[1] == 'true')) {
                            // console.log(entry[0] + " " +entry[1])
                            grantList.push(entry[0].replace(/_/g, ' '))
                        }
                    }
                    let inheritGrants = "NONE";
                    if (grantList.length > 0) {
                        inheritGrants = "( " + grantList.join(', ') +  " )";
                    }
                    stmt += 'ALTER ' + objectTypeMap.get(obj.TYPE)! + ' ' + (isHieararchyObject ? this.toolbox.namePathQuote(obj.NAME) : "'" + obj.NAME + "'") + ' WITH INHERIT GRANT = ' +  inheritGrants + "; \n";
                } else {

                    // set specific group grants

                    // compare original privs with altered Privs to not overload server
                    for (let entry of Object.entries(grantColumn)) {
                        if (entry[0].startsWith('ORIGINAL_')) {
                            // console.log('continue ' + entry[0])
                            continue;
                        } else if (Array.from(typeSpecificMap.values()).includes(entry[0]) && (entry[1] == true || entry[1] == 'true') && (entry[1] != grantColumn['ORIGINAL_' + entry[0]])) {
                            // console.log(entry[0] + " " +entry[1])
                            // console.log(grantColumn)
                            // grantList.push(entry[0])
                            stmt += 'GRANT ' + entry[0].replace('_', ' ') + ' ON ' + objectTypeMap.get(obj.TYPE) + ' ' + (isHieararchyObject ? this.toolbox.namePathQuote(obj.NAME) : "'" + obj.NAME + "'") + ' TO ' + grantColumn.GROUP + ';\n';
                        } else if (Array.from(typeSpecificMap.values()).includes(entry[0]) && (entry[1] == false || entry[1] == 'false') && (entry[1] != grantColumn['ORIGINAL_' + entry[0]])) {
                            // console.log(grantColumn)
                            // grantList.push(entry[0])
                            stmt += 'REVOKE ' + entry[0].replace('_', ' ') + ' ON ' + objectTypeMap.get(obj.TYPE) + ' ' + (isHieararchyObject ? this.toolbox.namePathQuote(obj.NAME) : "'" + obj.NAME + "'") + ' FROM ' + grantColumn.GROUP + ';\n';
                        }
                    }

                }
            }

            // WIP WHEN GRANT zeile dropped revoke all grants ENNO TODO

            stmt += 'END MULTICOMMAND';
            // console.log(stmt);
        } else {
            // console.warn("typeSpecificMap does not exist")
            throw new Error("typeSpecificMap does not exist")
        }

        return this.commandApi.execute(stmt).then((res) => {
            this.eventEmitterService.pushSnackBarMessage(['Grants updated'], SnackbarType.INFO)
            return res
        })
        // throw new Error("Method not implemented.");
    }
    drop(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    editorform() {
        return grant_editorform;
    }

    editorformByType(type: string, withCreate: boolean, isHieararchyObject: boolean) {
        let oType = objectTypeMap.get(type)!
        let typeSpecificMap = privilegeMapByObjectType.get(oType)
        let editorForm = this.toolbox.deepCopy(grant_editorform);
        let deleteFields: any[] = [];
        if (typeSpecificMap) {
            for (let field of editorForm.FIELDS[1].TABLE!.FIELDS) {
                // console.log(field.NAME)
                if(withCreate && field.NAME == 'CREATE_CONTENT' ) {
                    // console.log("CREATE FIELD")
                } else if(!withCreate && field.NAME == 'CREATE_CONTENT' ) {
                    deleteFields.push(field)
                }
                else if (Array.from(typeSpecificMap.values()).includes(field.NAME!) || field.TYPE != 'CHECKBOX' ) {
                    // console.log(field.TYPE)

                }else {
                    deleteFields.push(field)
                }
                if (!isHieararchyObject && ['ORIGIN','OWNER'].includes(field.NAME)) {
                    deleteFields.push(field)
                }
            }
            for (let field of deleteFields) {
                const index = editorForm.FIELDS[1].TABLE!.FIELDS.indexOf(field, 0);
                if (index > -1) {
                    editorForm.FIELDS[1].TABLE!.FIELDS.splice(index, 1);
                }
            }
        }
        return editorForm;
    }

    modifyBackgroundBasedOnInheritGrants(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {

        if (bicsuiteObject.CRUD_IS_NEW == true) return Promise.resolve({});

        // console.log(bicsuiteObject.EFFECTIVE_PRIVS_OBJECT[editorForm.NAME] == true || bicsuiteObject.EFFECTIVE_PRIVS_OBJECT[editorForm.NAME] == "true")
        if((bicsuiteObject.EFFECTIVE_PRIVS_OBJECT[editorForm.NAME] == true) || (bicsuiteObject.EFFECTIVE_PRIVS_OBJECT[editorForm.NAME] == "true") && !bicsuiteObject.ORIGIN) {
            editorForm.CSS_CLASS = 'crud-checkbox-color-gey'
        }
        // check if own field is on effective privs
        return Promise.resolve({});
    }
    conditionHasOwner(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        if (bicsuiteObject.OWNER) return true; else return false;
    }
    conditionIsNotExternalGrant(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // console.log()
        return (!bicsuiteObject.ORIGIN);
    }
    conditionIsNotExternalGrantOrTicked(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // console.log()
        return (!bicsuiteObject.ORIGIN || (bicsuiteObject[editorformField.NAME] == 'true' || bicsuiteObject[editorformField.NAME] == true));
    }

    conditionIsInheritGrant(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.GROUP == 'Inherit grant form parent';
    }

    conditionIsNew(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.CRUD_IS_NEW == true;
    }

}
