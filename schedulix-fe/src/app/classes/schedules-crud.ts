import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import schedules_editorform from '../json/editorforms/schedules.json';
import { Tab } from "../data-structures/tab";
import { TsCommonCrud } from "./ts-common-crud";

// Change timestamp from London To Berlin Time:
// moment.tz(someTimeStamp, 'HHmmss','Europe/London').tz('Europe/Berlin').format('HH:mm:ss')

// Change timestamp from UTC to browser's local time:
// moment.tz(someTimeStamp, 'HHmmss','UTC').local().format('HH:mm:ss')

export class SchedulesCrud extends TsCommonCrud {
    defaultTimeZone: string = "";
    defaultTimeFormat: string = "";
    selectedRows: string[] = [];

    // dummy to please abstract method existance, no create for schedules as whole ?
    create(obj: any, tabInfo: Tab): Promise<Object> {
        return Promise.resolve({});
    }

    async showInterval(name: string) {
        let stmt: string = "show interval '" + name + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    async showScheduledEvent(name: string) {
        let stmt: string = "show scheduled event " + name;
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    async readSubSchedule10(bicsuiteObject: any, masterSchedule: any, subSchedule: any) {
        let filter: string = subSchedule.INTERVAL;
        let intervals: any[] = [];
        let haveDriver = false;
        let fidx = 1;
        while (filter != "") {
            await this.showInterval(filter).then((response: any) => {
                let intervalObject = response.response.DATA.RECORD;
                let intervalType = intervalObject.NAME.split("_").splice(-1)[0];
                let syncTime = null;
                if (fidx == 1) {
                    subSchedule.START = intervalObject.STARTTIME;
                    subSchedule.END = intervalObject.ENDTIME;
                }
                let selections = "";
                if (!(["FIX", "REP"].includes(intervalType))) {
                    let selLst: any[] = [];
                    for (let row of intervalObject.SELECTION.TABLE) {
                        if (row.VALUE) {
                            selLst.push(row.VALUE);
                        }
                        else {
                            if (row.PERIOD_TO) {
                                selLst.push(row.PERIOD_FROM + "-" + row.PERIOD_TO);
                            }
                            else {
                                selLst.push(row.PERIOD_FROM);
                            }
                        }
                    }
                    selections = selLst.join(",");
                }

                switch (intervalType) {
                    case 'REP':
                        selections = intervalObject.BASE.split(' ')[0];
                        syncTime = intervalObject.SYNCTIME;
                        haveDriver = true;
                        break;
                    case 'WOM':
                        selections = selections.replace(new RegExp(" ", 'g'), "");
                        let dl = selections.split(',');
                        let dl1 = []
                        for (let dle of [1, 2, 3, 4, 5]) {
                            if (dl.includes((((dle - 1) * 7) + 1).toString())) {
                                dl1.push(dle.toString());
                            }
                        }
                        for (let dle of [-5, -4, -3, -2, -1]) {
                            if (dl.includes((((dle + 1) * 7) - 1).toString())) {
                                dl1.push(dle.toString());
                            }
                        }
                        selections = dl1.join(',');
                        break;
                    case 'ROD':
                        selections = selections.replace(new RegExp(" ", 'g'), "");
                        break;
                }

                if (['null', null, 'NONE'].includes(selections)) {
                    selections = "";
                }

                if (intervalType != 'WOM') {
                    let tmpSel = selections.split(",");
                    let newSel = []
                    for (let tmpS of tmpSel) {
                        newSel.push(tmpS.trim());
                    }
                    if (!['CAL', 'CLF', 'CLD'].includes(intervalType)) {
                        newSel.sort();
                    }
                    selections = newSel.join(",");
                }

                let selectionMode = 'NORMAL';
                if (intervalObject.INVERSE == 'true') {
                    selectionMode = 'INVERSE'
                }

                let interval: any;
                if (intervalType == 'TOD') {
                    haveDriver = true;
                }
                if (intervalType != 'FIX') {
                    if (['TOD', 'ROD'].includes(intervalType)) {
                        let selectionList = selections.split(',');
                        selectionList.sort();
                        for (let selection of selectionList) {
                            interval = {
                                'TYPE': intervalType,
                                'SETUP': selection,
                                'SELECTION_MODE': selectionMode
                            };
                            this.setupFieldValues(interval);
                            intervals.push(interval);
                        }
                    }
                    else {
                        interval = {
                            'TYPE': intervalType,
                            'SETUP': selections,
                            'SELECTION_MODE': selectionMode
                        };
                        this.setupFieldValues(interval);
                        intervals.push(interval);
                    }
                    if (['CAL', 'CLD', 'CLF'].includes(intervalType)) {
                        interval.CALENDAR = intervalObject.EMBEDDED;
                        interval.SELECT_ON = intervalObject.BASE.substring(2).trim();
                    }
                }
                filter = "";
                if (intervalObject.FILTER.TABLE.length > 0) {
                    for (let row of intervalObject.FILTER.TABLE) {
                        if (row.CHILD.endsWith("MASK")) {
                            continue;
                        }
                        else {
                            filter = row.CHILD;
                        }
                    }
                }
                else {
                    filter = "";
                }
            });
            subSchedule.INTERVALS = { TABLE: intervals };

            // read the scheduled event for this subschedule now
            let scheduledEventName = "ROOT.'S" + bicsuiteObject.SE_ID + "'.'" +
                masterSchedule.NAME + "'.'" +
                subSchedule.FULLNAME + "'.'S" + bicsuiteObject.SE_ID + "_" + masterSchedule.NAME + "'";
            await this.showScheduledEvent(scheduledEventName).then(async (response: any) => {
                let promise = new BicsuiteResponse();
                promise.response = response;
                let record = response.response.DATA.RECORD;
                subSchedule.ACTIVE = record.ACTIVE;
                masterSchedule.BACKLOG_HANDLING = record.BACKLOG_HANDLING;
                masterSchedule.EFFECTIVE_SUSPEND_TIMEOUT = record.EFFECTIVE_SUSPEND_LIMIT;
                if (record.SUSPEND_LIMIT) {
                    masterSchedule.SUSPEND_TIMEOUT = record.SUSPEND_LIMIT.split(" ")[0];
                    masterSchedule.SUSPEND_UNIT = record.SUSPEND_LIMIT.split(" ")[1];
                }
                else {
                    masterSchedule.SUSPEND_TIMEOUT = "";
                    masterSchedule.SUSPEND_UNIT = "HOUR";
                }
                masterSchedule.BROKEN = record.BROKEN;
                if (!(masterSchedule.NEXT) || record.NEXT_START < masterSchedule.NEXT) {
                    masterSchedule.NEXT = record.NEXT_START;
                }
                if (record.LAST_START) {
                    masterSchedule.LAST = this.toolbox.convertTimeStamp(record.LAST_START, this.defaultTimeZone, this.defaultTimeFormat);
                    if (masterSchedule.LAST != this.toolbox.convertTimeStamp(record.LAST_START, this.defaultTimeZone, this.defaultTimeFormat)) {
                        masterSchedule.LAST = masterSchedule.LAST + " [" + record.LAST_START + "]";
                    }
                    if (bicsuiteObject.LAST < masterSchedule.LAST) {
                        bicsuiteObject.LAST = masterSchedule.LAST;
                    }
                }
                masterSchedule.CALENDAR = record.CALENDAR;
                masterSchedule.CALENDAR_HORIZON = record.CALENDAR_HORIZON;
                masterSchedule.EFFECTIVE_CALENDAR_HORIZON = record.EFFECTIVE_CALENDAR_HORIZON;
                for (let row of record.CALENDAR_TABLE.TABLE) {
                    let found: Boolean = false;
                    if (!bicsuiteObject.NOFORM_CALENDAR_IDS.includes(row.ID)) {
                        bicsuiteObject.NOFORM_CALENDAR_IDS.push(row.ID);
                        let startTime = this.formatStartTime(row.STARTTIME);
                        bicsuiteObject.NOFORM_CALENDAR_TABLE.TABLE.push({
                            'STARTTIME': row.STARTTIME,
                            'STARTTIME_DISPLAY': startTime,
                            'MASTER_SCHEDULE': masterSchedule.NAME,
                            'ACTIVE': masterSchedule.ACTIVE
                        });
                    }
                }
                return promise;
            });
        }
        for (let interval of intervals) {
            if (interval.TYPE == 'CAL') {
                if (haveDriver) {
                    interval.TYPE = 'CLF';
                }
                else {
                    interval.TYPE = 'CLD';
                    haveDriver = true;
                }
            }
        }
        return Promise.resolve({});
    }

    async readSubSchedules10(bicsuiteObject: any, masterSchedule: any) {
        for (let subSchedule of masterSchedule.SUBSCHEDULES.TABLE) {
            await this.readSubSchedule10(bicsuiteObject, masterSchedule, subSchedule);
        }
        return Promise.resolve({});
    }

    async readMasterSchedule10(bicsuiteObject: any, masterSchedule: any) {
        await this.readSubSchedules10(bicsuiteObject, masterSchedule);
        return Promise.resolve({});
    }

    formatStartTime(startTime: string): string {
        let timezone = startTime.split(" ")[1]
        let localTime = this.toolbox.convertTimeStamp(startTime, this.defaultTimeZone, this.defaultTimeFormat);
        startTime = this.toolbox.convertTimeStamp(startTime, timezone, this.defaultTimeFormat);
        if (startTime != localTime) {
            localTime = localTime + " [" + startTime + " " + timezone + "]"
        }
        return localTime;
    }

    async readMasterSchedule29(bicsuiteObject: any, masterSchedule: any) {
        let subSchedules: any[] = [];
        await this.showInterval(masterSchedule.INTERVAL).then((response: any) => {
            let intervalObject = response.response.DATA.RECORD;
            let MAXLEVEL = 1000000
            let stopLevel = MAXLEVEL
            let subSchedule: any = {};
            let haveDriver = false;
            let selections = "";
            let syncTime = "";
            let intervals = [];
            for (let row of intervalObject.HIERARCHY.TABLE) {
                let level = parseInt(row.LEVEL);
                if (level >= stopLevel) {
                    continue;
                }
                if (row.ROLE == "EMBEDDED") {
                    stopLevel = level + 1;
                    continue;
                }
                if (row.ROLE == "HEAD") {
                    continue;
                }
                // let subScheduleIdx = 0;

                if (row.ROLE == "DISPATCH") {
                    // subScheduleIdx = subScheduleIdx + 1;
                    subSchedule = {
                        "NAME": row.NAME,
                        "OLD_NAME": row.NAME,
                        "ID": row.ID,
                        "ACTIVE": row.IS_ACTIVE,
                        "ENABLED": "true"
                    }
                    subSchedules.push(subSchedule);
                    intervals = [];
                }
                if (row.ROLE == "DISPATCH_SELECT") {
                    subSchedule.START = row.STARTTIME;
                    subSchedule.END = row.ENDTIME;
                }
                if (["DISPATCH_FILTER", "FILTER"].includes(row.ROLE)) {
                    let intervalType = row.NAME.split('_')[0];
                    let selectionMode = "NORMAL";
                    if (row.INVERSE == "true") {
                        selectionMode = "INVERSE";
                    }

                    if (["REP", "TOD", "CLD"].includes(intervalType)) {
                        haveDriver = true;
                    }
                    if (intervalType == "REP") {
                        selections = row.BASE.split(' ')[0];
                        syncTime = row.SYNCTIME;
                    }
                    if (!["FIX", "REP"].includes(intervalType)) {
                        selections = row.SELECTION;
                    }
                    if (intervalType == "WOM") {
                        selections = selections.replace(new RegExp(" ", 'g'), "");
                        let dl = selections.split(',');
                        let dl1 = []
                        for (let dle of [1, 2, 3, 4, 5]) {
                            if (dl.includes((((dle - 1) * 7) + 1).toString())) {
                                dl1.push(dle.toString());
                            }
                        }
                        for (let dle of [-5, -4, -3, -2, -1]) {
                            if (dl.includes((((dle + 1) * 7) - 1).toString())) {
                                dl1.push(dle.toString());
                            }
                        }
                        selections = dl1.join(',');
                    }
                    else {
                        if (intervalType == "ROD") {
                            selections = selections.replace(new RegExp(" ", 'g'), "");
                        }
                        if (['null', null, 'NONE'].includes(selections)) {
                            selections = "";
                        }

                        if (intervalType != 'WOM') {
                            let tmpSel = selections.split(",");
                            let newSel = []
                            for (let tmpS of tmpSel) {
                                newSel.push(tmpS.trim());
                            }
                            if (!['CAL', 'CLF', 'CLD'].includes(intervalType)) {
                                newSel.sort();
                            }
                            selections = newSel.join(",");
                        }
                    }
                    let interval: any;
                    if (intervalType != 'FIX') {
                        if (['TOD', 'ROD'].includes(intervalType)) {
                            let selectionList = selections.split(',');
                            selectionList.sort();
                            for (let selection of selectionList) {
                                interval = {
                                    'TYPE': intervalType,
                                    'SETUP': selection,
                                    'SELECTION_MODE': selectionMode
                                };
                                this.setupFieldValues(interval);
                                intervals.push(interval);
                            }
                        }
                        else {
                            interval = {
                                'TYPE': intervalType,
                                'SETUP': selections,
                                'SELECTION_MODE': selectionMode
                            };
                            this.setupFieldValues(interval);
                            intervals.push(interval);
                        }
                        if (['CAL', 'CLD', 'CLF'].includes(intervalType)) {
                            interval.CALENDAR = row.EMBEDDED;
                            interval.SELECT_ON = row.BASE.substring(2).trim();
                        }
                    }
                }
                subSchedule.INTERVALS = { TABLE: intervals };
            }
            masterSchedule.SUBSCHEDULES = { TABLE: subSchedules };
        });
        // read the scheduled event for this masterSchedule now
        let scheduledEventName = "ROOT.'S" + bicsuiteObject.SE_ID + "'.'" +
            masterSchedule.NAME + "'.'S" + bicsuiteObject.SE_ID + "_" + masterSchedule.NAME + "'";
        await this.showScheduledEvent(scheduledEventName).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            let record = response.response.DATA.RECORD;
            masterSchedule.BACKLOG_HANDLING = record.BACKLOG_HANDLING;
            masterSchedule.EFFECTIVE_SUSPEND_TIMEOUT = record.EFFECTIVE_SUSPEND_LIMIT;
            if (record.SUSPEND_LIMIT) {
                masterSchedule.SUSPEND_TIMEOUT = record.SUSPEND_LIMIT.split(" ")[0];
                masterSchedule.SUSPEND_UNIT = record.SUSPEND_LIMIT.split(" ")[1];
            }
            else {
                masterSchedule.SUSPEND_TIMEOUT = "";
                masterSchedule.SUSPEND_UNIT = "HOUR";
            }
            masterSchedule.BROKEN = record.BROKEN;
            masterSchedule.NEXT = record.NEXT_START;
            if (record.LAST_START) {
                masterSchedule.LAST = this.toolbox.convertTimeStamp(record.LAST_START, this.defaultTimeZone, this.defaultTimeFormat);
                if (masterSchedule.LAST != this.toolbox.convertTimeStamp(record.LAST_START, this.defaultTimeZone, this.defaultTimeFormat)) {
                    masterSchedule.LAST = masterSchedule.LAST + " [" + record.LAST_START + "]";
                }
                if (bicsuiteObject.LAST < masterSchedule.LAST) {
                    bicsuiteObject.LAST = masterSchedule.LAST;
                }
            }
            masterSchedule.CALENDAR = record.CALENDAR;
            masterSchedule.CALENDAR_HORIZON = record.CALENDAR_HORIZON;
            masterSchedule.EFFECTIVE_CALENDAR_HORIZON = record.EFFECTIVE_CALENDAR_HORIZON;
            for (let row of record.CALENDAR_TABLE.TABLE) {
                let found: Boolean = false;
                if (!bicsuiteObject.NOFORM_CALENDAR_IDS.includes(row.ID)) {
                    bicsuiteObject.NOFORM_CALENDAR_IDS.push(row.ID);
                    let startTime = this.formatStartTime(row.STARTTIME);
                    bicsuiteObject.NOFORM_CALENDAR_TABLE.TABLE.push({
                        'STARTTIME': row.STARTTIME,
                        'STARTTIME_DISPLAY': startTime,
                        'MASTER_SCHEDULE': masterSchedule.NAME,
                        'ACTIVE': masterSchedule.ACTIVE
                    });
                }
            }
            return promise;
        });
        return Promise.resolve({});
    }

    async readMasterSchedule(bicsuiteObject: any, masterSchedule: any) {
        masterSchedule.NEXT = null; // initialize to high year for finding next across subschedules for V1.0 schedules
        if (masterSchedule.VERSION == "1.0") {
            await this.readMasterSchedule10(bicsuiteObject, masterSchedule);
        }
        else {
            await this.readMasterSchedule29(bicsuiteObject, masterSchedule);
        }

        if (masterSchedule.CALENDAR == "INACTIVE" && masterSchedule.NEXT) {
            let startTime = this.formatStartTime(masterSchedule.NEXT);
            bicsuiteObject.NOFORM_CALENDAR_TABLE.TABLE.push({
                'STARTTIME': masterSchedule.NEXT,
                'STARTTIME_DISPLAY': startTime,
                'MASTER_SCHEDULE': masterSchedule.NAME,
                'ACTIVE': masterSchedule.ACTIVE
            });
        }

        return Promise.resolve({});
    }

    async listSchedules(name: string, ignoreNotFound: boolean) {
        let stmt: string = "list schedules " + this.toolbox.namePathQuote(name) + " with expand = all";
        return this.commandApi.execute(stmt, !ignoreNotFound).then((response: any) => {
            if (response.hasOwnProperty("ERROR")) {
                if (response.ERROR.ERRORCODE != "03201292040") {
                    this.commandApi.throwSdmsException(response.ERROR.ERRORCODE, response.ERROR.ERRORMESSAGE, stmt);
                    return Promise.reject(response);
                }
            }
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    async read(obj: any, tabInfo: Tab): Promise<Object> {
        this.defaultTimeZone = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
        this.defaultTimeFormat = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat");
        let bicsuiteObject: any = { DATA: { RECORD: {} } };
        let record = bicsuiteObject.DATA.RECORD;
        record.PRIVS = tabInfo.PRIVS;
        record.SE_NAME = tabInfo.OWNER;
        record.SE_TYPE = tabInfo.SE_TYPE;
        record.SE_OWNER = tabInfo.SE_OWNER;
        record.SE_ID = tabInfo.NUMERIC_ID;
        record.LAST = "";
        record.NOFORM_MASTERCHEDULE_READ = [];
        record.MASTERSCHEDULES = { TABLE: [] };
        record.NAME = tabInfo.NAME;
        record.PATH = tabInfo.PATH;

        let command = 
        await this.listSchedules("ROOT.S" + tabInfo.NUMERIC_ID, true).then((response: any) => {
            if (!response.response.hasOwnProperty("ERROR")) {
                let masterSchedules: any = [];
                let masterSchedule: any = {};
                let masterScheduleRead: any = {};
                for (let row of response.response.DATA.TABLE) {
                    let path = row.NAME.split('.');
                    let dotCount = path.length - 1;
                    if (dotCount == 1) {
                        // this is the head row we just ignore
                        continue;
                    }
                    if (dotCount == 2) {
                        masterSchedule = {};
                        masterSchedule.VERSION = "2.9"; // will be overwritten by next row processed in this loop if this is a V1.0 schedule
                        masterSchedule.NAME = path.slice(-1)[0];
                        masterSchedule.OLD_NAME = masterSchedule.NAME;
                        masterSchedule.SUBSCHEDULES = { "TABLE": [] };
                        masterSchedule.INTERVAL = row.INTERVAL;
                        masterSchedule.PRIVS = row.PRIVS;
                        masterSchedule.ACTIVE = row.ACTIVE;
                        masterSchedules.push(masterSchedule);
                        masterScheduleRead = { "NAME" : masterSchedule.NAME, "VERSION": masterSchedule.VERSION };
                        record.NOFORM_MASTERCHEDULE_READ.push( masterScheduleRead );
                    }
                    if (dotCount == 3) {
                        let fullSubscheduleName = path.slice(-1)[0];
                        masterSchedule.VERSION = "1.0";
                        masterScheduleRead.VERSION = "1.0";
                        let subscheduleName = fullSubscheduleName.replace(/[^_]*_/, "");
                        masterSchedule.SUBSCHEDULES.TABLE.push({
                            "NAME": subscheduleName,
                            "FULLNAME": fullSubscheduleName,
                            "INTERVAL": row.INTERVAL
                        }
                        );
                    }
                    masterSchedule.OLD_VERSION = masterSchedule.VERSION;
                    masterSchedule.TIME_ZONE = row.TIME_ZONE;
                    masterSchedule.SUBMIT_OWNER = row.OWNER;
                }
                record.MASTERSCHEDULES = { TABLE: masterSchedules };
            }
        });
        record.NOFORM_CALENDAR_TABLE = { 'TABLE': [] };
        record.NOFORM_CALENDAR_IDS = [];
        for (let masterSchedule of record.MASTERSCHEDULES.TABLE) {
            await this.readMasterSchedule(record, masterSchedule);
            masterSchedule.NOFORM_VALUES_READ = this.toolbox.deepCopy(masterSchedule);
        }

        record.NOFORM_MASTERSCHEDULE = "";
        record.NOFORM_SUBSCHEDULE = "";
        record.NOFORM_SUBSCHEDULES = {
            "TABLE": [],
        };
        record.NOFORM_INTERVALS = {
            "TABLE": [],
        };

        // Select first active master schedule by default
        let rowIndex = 0;
        let found = false;
        for (let master of record.MASTERSCHEDULES.TABLE) {
            if ((this.selectedRows.length == 0 && master.ACTIVE == "true") ||
                (this.selectedRows.length > 0 && this.selectedRows.includes(master.NAME))) {
                    found = true;
                    break;
            }
            rowIndex++;
        }
        if (!found) {
            rowIndex = 0;
        }
        if (record.MASTERSCHEDULES.TABLE.length > 0) {
            this.selectMasterschedule(record, tabInfo, rowIndex);
        }
        record.NOFORM_CALENDAR_TABLE.TABLE.sort(function (a: any, b: any) {
            if (a.STARTTIME < b.STARTTIME) {
                return -1;
            }
            else {
                if (a.STARTTIME == b.STARTTIME) {
                    if (a.MASTER_SCHEDULE < b.MASTER_SCHEDULE) {
                        return -1;
                    }
                }
                return 1;
            }
        });
        let promise = new BicsuiteResponse();
        promise.response = bicsuiteObject;
        return Promise.resolve(promise);
    }


    changedMasterSchedule(masterSchedule: any): boolean {
        this.toolbox.deepEquals(masterSchedule, masterSchedule.NOFORM_VALUES_READ);
        masterSchedule.NOFORM_CHANGED = !(this.toolbox.deepEquals(masterSchedule, masterSchedule.NOFORM_VALUES_READ));
        return masterSchedule.NOFORM_CHANGED;
    }

    async genDropMasterSchedule(bicsuiteObject: any, name: string, version: string): Promise<string[]> {
        let cmdlst: string[] = [];
        await this.listSchedules("ROOT.S" + bicsuiteObject.SE_ID + "." + name, false).then(async (response: any) => {
            let event = "S" + bicsuiteObject.SE_ID + '_' + name;
            let intervals: any[] = [];
            let table = response.response.DATA.TABLE;
            for (let row of table) {
                // only subschedules have intervals for V 1.0 schedules
                // with V 2.9 schedules there is only a row for the master schedule with an interval using dispatchers to implement subschedules
                if (row.INTERVAL != "") {
                    cmdlst.push("drop scheduled event " + this.toolbox.namePathQuote(row.NAME + '.' + event));
                    // V 2.9 schedulues do not have schedules events per subschedule because subschedules are implemented using dispatchers
                    if (version == '1.0') {
                        cmdlst.push('drop schedule ' + this.toolbox.namePathQuote(row.NAME));
                    }
                    intervals.push(row.INTERVAL);
                }
            }
            cmdlst.push('drop schedule ' + this.toolbox.namePathQuote("ROOT.S" + bicsuiteObject.SE_ID + "." + name));
            // now we drop the intervals
            while (intervals.length > 0) {
                let filters: any[] = [];
                for (let interval of intervals) {
                    // get filters
                    let intervalTable: any[] = [];
                    await this.showInterval(interval).then((response: any) => {
                        intervalTable = response.response.DATA.RECORD.FILTER.TABLE;
                    });
                    for (let row of intervalTable) {
                        filters.push(row.CHILD);
                    }
                    // add command to drop interval
                    cmdlst.push("drop interval '" + interval + "'");
                }
                intervals = filters;
            }
            // drop the event
            cmdlst.push("drop event '" + event + "'");
        });
        return Promise.resolve(cmdlst);
    }

    normalizeName(name: string, se_id: string): string {
        return name.replace(RegExp('S' + se_id), 'S0');
    }

    async genCreateMasterSchedule(bicsuiteObject: any, masterSchedule: any): Promise<string[]> {
        let cmdlst: string[] = [];
        let name = masterSchedule.NAME;
        let se_id = masterSchedule.SE_ID;

        // TODO: make sure no duplicate master Schedule name in form!
        // #
        // # Check whether this name already exists
        // #
        // if name != REQUEST['OLD_NAME']:
        //     output = context.NavigatorQueryMethod()
        //     if 'ERROR' in output:
        //         return output
        //     tbl = output['DATA']['TABLE']
        //     for r in tbl:
        //         rname = r['NAME']
        //         if rname == name:
        //             return {
        //                 'ERROR' : {
        //                     'ERRORCODE'    : 'ZSI-02303241450',
        //                     'ERRORMESSAGE' : 'Master schedule with this name already exists'
        //                 }
        //             }

        // TODO: grpClause must not be empty if we drop and recreate
        // we have to not drop but maybe rename objectd to alter them without grpClause to make this work

        // userData = context.Common.Util.GetUserData()
        // groups = userData['GROUPS']
        // group = REQUEST['SUBMIT_OWNER']
        // #we must always render group clause
        // #we are not allowed to edit a foreign schedule
        // #if group in groups:
        // grpclause = 'group = \"" + group + '\""
        // #else:
        // #    grpclause = ""

        let grpClause = "group = '" + masterSchedule.SUBMIT_OWNER + "'";
        let quoteName = this.toolbox.namePathQuote(bicsuiteObject.SE_NAME);
        cmdlst.push("create or alter schedule ROOT.'S0' (" + quoteName + ") with group = PUBLIC");
        masterSchedule.NAME = this.convertIdentifier(masterSchedule.NAME, masterSchedule.OLD_NAME);
        let eventName = "S0_" + masterSchedule.NAME;
        cmdlst.push("create or alter event '" + eventName + "' (" + quoteName + ") with action = submit " + quoteName + ", " + grpClause);

        let intervalClause = "";
        if (masterSchedule.VERSION == '2.9') {
            // since we only need one interval per subschedule, we can use the same name for the interval as we use for the event
            let intervalName = eventName;

            let cmd = "create or alter interval '" + intervalName + "' (" + quoteName + ") with\n";
            cmd += '    ' + grpClause + ',\n';
            cmd += '    ' + "dispatch = (\n";

            let dispatchers: string[] = [];
            for (let subSchedule of masterSchedule.SUBSCHEDULES.TABLE) {
                dispatchers.push(this.genDispatcher(subSchedule, true));
            }
            cmd += dispatchers.join(",\n");
            cmd += "\n)";
            cmdlst.push(cmd);
            intervalClause = "interval = '" + intervalName + "', ";
        }
        let baseScheduleName = "ROOT.'S0'.'" + masterSchedule.NAME + "'";
        let nl = baseScheduleName.replace(new RegExp("'", "g"), "").split('.')
        cmdlst.push("create or alter schedule " + baseScheduleName + " (" + quoteName + ") with " +
            intervalClause + grpClause + ", " + (masterSchedule.ACTIVE == "true" ? "active" : "inactive") + ", time zone = '" + masterSchedule.TIME_ZONE + "'");

        // add command to create scheduled event
        if (masterSchedule.VERSION == '2.9') {
            let cmd = "create or alter scheduled event " + baseScheduleName + ".'" + eventName + "' (" + quoteName + ")";
            // scheduled event always activewith v 2.9 schedules
            cmd += " with active";
            cmd += ", " + grpClause + ", backlog handling = " + masterSchedule.BACKLOG_HANDLING;
            cmd += ", suspend limit = " + (masterSchedule.SUSPEND_TIMEOUT != "" ? masterSchedule.SUSPEND_TIMEOUT + " " + masterSchedule.SUSPEND_UNIT : "default");
            cmd += ", calendar = " + masterSchedule.CALENDAR;
            cmd += ", horizon = " + (masterSchedule.CALENDAR_HORIZON != "" ? masterSchedule.CALENDAR_HORIZON : "NONE");
            cmdlst.push(cmd);
        }
        if (masterSchedule.VERSION == '1.0') {
            // now he have to process the subschedules
            //initialize mask_list
            let masklst: any[] = [];
            let subidx = 1;
            for (let subSchedule of masterSchedule.SUBSCHEDULES.TABLE) {
                let subscheduleName = subSchedule.NAME;
                let ssfullname = baseScheduleName + ".'" + "S" + ("00" + subidx.toString()).slice(-2) + "_" + subscheduleName + "'";
                let sd = subSchedule.START.trim();
                let ed = subSchedule.END.trim();
                //create mask intervals
                for (let mask of masklst) {
                    let maskName = mask[0];
                    let cmd = "create or alter interval '" + this.normalizeName(maskName, se_id) + "' (" + quoteName + ") with " + grpClause;
                    if (mask[1] == null && mask[2] == null) {
                        // this is an idefinite mask interval
                        cmd += ", starttime = '1971-01-01T00:00'";
                        cmd += ", endtime = '1971-01-01T00:01'";
                    }
                    else {
                        if (mask[1] != null) {
                            let masktss = this.toolbox.unixTimetoISO(mask[1]).split("+")[0];
                            cmd += ", starttime = '" + masktss + "'";
                        }
                        else {
                            cmd += ", starttime = NONE";
                        }
                        if (mask[2] != null) {
                            let masktse = this.toolbox.unixTimetoISO(mask[2]).split("+")[0];
                            cmd += ", endtime = '" + masktse + "'";
                        }
                        else {
                            cmd += ", endtime = NONE";
                        }
                    }
                    cmdlst.push(cmd);
                }
                let subidxstr = ("00" + subidx.toString()).slice(-2);
                let intprefix = nl[1] + '_' + nl[2] + '_' + subidxstr;
                let cmd = "";
                let mergedIntervals = this.mergeAndSortIntervals(subSchedule, true);
                let intervalIndex = mergedIntervals.length;
                let lastIntervalName = "";
                while (intervalIndex > 0) {
                    let interval = mergedIntervals[intervalIndex - 1];
                    let intervalName = "";
                    if (interval.TYPE != "FIX") {
                        intervalName = this.normalizeName(intprefix + "_" + ("00" + intervalIndex.toString()).slice(-2) + "_" + interval.TYPE, se_id);
                    }
                    else {
                        intervalName = this.normalizeName(intprefix + "_" + interval.TYPE, se_id);
                    }
                    cmd = "create or alter interval '" + intervalName + "' (" + quoteName + ") with " + grpClause + ", ";
                    cmd += this.genDispatcherInterval(subSchedule, interval, "").replace(new RegExp("\n", "g"), " ");
                    if (lastIntervalName != "" || masklst.length > 0) {
                        cmd += ", filter = (";
                        if (lastIntervalName == "") {
                            // add filter for mask intervals
                            let maskSep = "";
                            for (let mask of masklst) {
                                cmd += maskSep + this.normalizeName(mask[0], se_id);
                                maskSep = ", ";
                            }
                        }
                        else {
                            cmd += "'" + lastIntervalName + "'";
                        }
                        cmd += ")";
                    }
                    cmdlst.push(cmd);
                    lastIntervalName = intervalName;
                    intervalIndex--;
                }
                // add command to create subschedule
                cmd = "create or alter schedule " + this.normalizeName(ssfullname, se_id) + " (" + quoteName + ") with interval = '" +
                    this.normalizeName(lastIntervalName, se_id) + "'";
                cmd = cmd + ", " + grpClause + ", time zone = '" + masterSchedule.TIME_ZONE + "'";
                cmdlst.push(cmd);
                // add command to create scheduled event
                cmd = "create or alter scheduled event " + this.normalizeName(ssfullname + ".'" + eventName + "'", se_id) + " (" + quoteName + ")";
                if (subSchedule.ACTIVE != "true") {
                    cmd = cmd + " with inactive";
                }
                else {
                    cmd = cmd + " with active";
                }
                cmd = cmd + ", " + grpClause + ", backlog handling = " + masterSchedule.BACKLOG_HANDLING;
                cmd += ", suspend limit = " + (masterSchedule.SUSPEND_TIMEOUT != "" ? masterSchedule.SUSPEND_TIMEOUT + " " + masterSchedule.SUSPEND_UNIT : "default");
                cmd += ", calendar = " + masterSchedule.CALENDAR;
                cmd += ", horizon = " + (masterSchedule.CALENDAR_HORIZON != "" ? masterSchedule.CALENDAR_HORIZON : "NONE");
                cmdlst.push(cmd);
                // now build mask intervals and add them to the masklst
                // if there is already an indefinite mask we dont have to do anything
                if (masklst.length != 1 || masklst[0][1] || masklst[0][2]) {
                    let maskidx = 1;
                    let maskName = intprefix + "_" + maskidx.toString() + "_MASK";
                    if (!sd && !ed) {
                        // we dont need other masks any more if we hav an indefinite mask
                        masklst = [[maskName, null, null]]
                    }
                    else {
                        let tmpMasklst = [];
                        let sdsec = null;
                        if (sd) {
                            sdsec = this.toolbox.mktime(sd);
                            tmpMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', null, sdsec])
                            maskidx = 2
                        }
                        let sesec = null;
                        if (ed) {
                            sesec = this.toolbox.mktime(ed);
                            tmpMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', sesec, null]);
                        }
                        // now build new masklst
                        if (masklst.length == 0) {
                            // masklst is empty, so new masklist is our tmp_masklst
                            masklst = tmpMasklst;
                        }
                        else {
                            let newMasklst: any[] = [];
                            maskidx = 1;
                            let midx = 0;
                            while (midx < masklst.length) {
                                let mask = masklst[midx];
                                let op = "";
                                if (op == "" &&
                                    ((sdsec == null && mask[2] != null && sesec != null && mask[2] <= sesec) ||
                                        (sesec == null && mask[1] != null && sdsec != null && mask[1] >= sdsec) ||
                                        (sdsec != null && sesec != null && mask[1] != null && mask[2] != null && mask[1] >= sdsec && mask[2] <= sesec))) {
                                    op = 'DELETE';
                                }
                                if (op == "" &&
                                    ((sdsec != null && mask[2] != null && mask[2] < sdsec) ||
                                        (sesec != null && mask[1] != null && mask[1] > sesec))) {
                                    op = 'KEEP';
                                }
                                if (op == "" &&
                                    sdsec != null &&
                                    sesec != null &&
                                    (mask[1] == null || mask[1] < sdsec) &&
                                    (mask[2] == null || mask[2] > sesec)) {
                                    op = 'SPLIT';
                                }
                                if (op == "" &&
                                    sdsec != null &&
                                    (mask[1] == null || mask[1] <= sdsec) &&
                                    (mask[2] == null || mask[2] >= sdsec) &&
                                    (sesec == null || (mask[2] != null && mask[2] <= sesec))) {
                                    op = 'CLIPEND';
                                }
                                if (op == "" &&
                                    sesec != null &&
                                    (mask[1] == null || mask[1] <= sesec) &&
                                    (mask[2] == null || mask[2] >= sesec) &&
                                    (sdsec == null || (mask[1] != null && mask[1] >= sdsec))) {
                                    op = 'CLIPSTART';
                                }
                                if (op == 'KEEP') {
                                    newMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', mask[1], mask[2]]);
                                    maskidx++;
                                }
                                if (op == 'CLIPEND') {
                                    newMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', mask[1], sdsec]);
                                    maskidx++;
                                }
                                if (op == 'CLIPSTART') {
                                    newMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', sesec, mask[2]]);
                                    maskidx++;
                                }
                                if (op == 'SPLIT') {
                                    newMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', mask[1], sdsec]);
                                    maskidx++;
                                    newMasklst.push([intprefix + '_' + maskidx.toString() + '_MASK', sesec, mask[2]]);
                                    maskidx++;
                                }
                                midx++;
                            }
                            masklst = newMasklst;
                        }
                    }
                }
                subidx++;
            }
        }
        return Promise.resolve(cmdlst);
    }

    getMasterScheduleByOldName(bicsuiteObject: any, name: string) {
        for (let masterSchedule of bicsuiteObject.MASTERSCHEDULES.TABLE) {
            if (masterSchedule.OLD_NAME == name) {
                return masterSchedule;
            }
        }
        return null;
    }

    async update(obj: any, tabInfo: Tab): Promise<Object> {
        // save selected rows to keep selection over update
        this.selectedRows = [];
        for (let masterschedule of obj.MASTERSCHEDULES.TABLE) {
            if (masterschedule.NOFORM_SELECTED) {
                this.selectedRows.push(masterschedule.NAME);
            }
            for (let subschedule of masterschedule.SUBSCHEDULES.TABLE) {
                if (subschedule.NOFORM_SELECTED) {
                    this.selectedRows.push(masterschedule.NAME + ":" + subschedule.NAME);
                }
            }
        }
        let statements: string[] = ["begin multicommand"];
        let leftOversToDrop: any[] = this.toolbox.deepCopy(obj.NOFORM_MASTERCHEDULE_READ);
        // generate statements to drop dropped and changed masterschedules and mark changed masterSchedules
        for (let masterSchedule of obj.MASTERSCHEDULES.TABLE) {
            // remove from table of read masterschedule names if not changed
            if (!this.changedMasterSchedule(masterSchedule)) {
                    leftOversToDrop = leftOversToDrop.filter((obj: any) => obj.NAME !== masterSchedule.OLD_NAME);
            }
        }
        // generate statements to drop leftover master schedules (the changed and dropped ones)
        for (let row of leftOversToDrop) {
            await this.genDropMasterSchedule(obj, row.NAME, row.VERSION).then((response: any) => {
                statements = statements.concat(response);
            });
        }

        // generate statements to (re)create changes or new masterSchedules
        for (let masterSchedule of obj.MASTERSCHEDULES.TABLE) {
            masterSchedule.NAME = this.convertIdentifier(masterSchedule.NAME, masterSchedule.OLD_NAME);
            if (masterSchedule.NOFORM_CHANGED == undefined || /* new one not read */ masterSchedule.NOFORM_CHANGED) {
                await this.genCreateMasterSchedule(obj, masterSchedule).then((response: any) => {
                    statements = statements.concat(response);
                });
            }
        }
        statements.push("end multicommand");
        let stmt = statements.join(";\n");

        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    // dummy to please abstract method existance, no drop of all schedules at once or should we ?
    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return Promise.resolve({});
    }

    editorform() {
        let editorForm = schedules_editorform;
        let fields : any = [];
        for (let field of editorForm.FIELDS) {
            let f : any = field;
            if (f.NAME == 'MASTER_SCHEDULES') {
                fields = field['FIELDS'];
            }
        }
        for (let field of fields) {
            if (field.hasOwnProperty('NAME') && field.NAME == 'MASTERSCHEDULES') {
                let masterFields: any = [];
                if (field.hasOwnProperty('TABLE') && field.TABLE?.hasOwnProperty('FIELDS')) {
                    masterFields = field.TABLE.FIELDS;
                }
                for (let masterField of masterFields) {
                    if (masterField.hasOwnProperty('NAME')) {
                        if (masterField.NAME == 'TIME_ZONE') {
                            masterField.DEFAULT = this.customPropertiesService.getCustomObjectProperty('defaultTimeZone');
                            masterField.VALUES = this.customPropertiesService.getCustomObjectProperty('timeZones');
                        }
                        if (masterField.NAME == 'SUBMIT_OWNER') {
                            masterField.DEFAULT = this.privilegesService.getDefaultGroup();
                        }
                    }
                }
            }
        }
        return editorForm;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.MASTERSCHEDULES = {
            "TABLE": [],
        };
        record.NOFORM_MASTERSCHEDULE = "";
        record.NOFORM_SUBSCHEDULE = "";
        record.NOFORM_SUBSCHEDULES = {
            "TABLE": []
        };
        record.NOFORM_INTERVALS = {
            "TABLE": []
        };
        record.NOFORM_CALENDAR_TABLE = {
            "TABLE": []
        };
        return bicsuiteObject;
    }

    selectMasterschedule(bicsuiteObject: any, tabInfo: Tab, rowIndex: number): Promise<number> {
        for (let row of bicsuiteObject.MASTERSCHEDULES.TABLE) {
            row.NOFORM_SELECTED = false;
        }
        bicsuiteObject.MASTERSCHEDULES.TABLE[rowIndex].NOFORM_SELECTED = true;
        bicsuiteObject.NOFORM_MASTERSCHEDULE = bicsuiteObject.MASTERSCHEDULES.TABLE[rowIndex].NAME;
        bicsuiteObject.NOFORM_SUBSCHEDULES = bicsuiteObject.MASTERSCHEDULES.TABLE[rowIndex].SUBSCHEDULES;

        let index = -1;
        let subscheduleRowIndex = 0;
        for (let subschedule of bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE) {
            if (subschedule.NOFORM_SELECTED) {
                index = subscheduleRowIndex;
            }
            subscheduleRowIndex++;
        }
        if (index == -1) {
            index = 0;
            subscheduleRowIndex = 0;
            for (let subschedule of bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE) {
                if ((this.selectedRows.length == 0 && subschedule.ACTIVE == "true") ||
                    (this.selectedRows.length != 0 && this.selectedRows.includes(bicsuiteObject.NOFORM_MASTERSCHEDULE + ":" + subschedule.NAME))) {
                    index = subscheduleRowIndex;
                    break;
                }
                subscheduleRowIndex++;
            }
        }
        return this.selectSubschedule(bicsuiteObject, tabInfo, index);
    }

    selectSubschedule(bicsuiteObject: any, tabInfo: Tab, rowIndex: number): Promise<number> {
        for (let row of bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE) {
            row.NOFORM_SELECTED = false;
        }
        bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE[rowIndex].NOFORM_SELECTED = true;
        bicsuiteObject.NOFORM_SUBSCHEDULE = bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE[rowIndex].NAME;
        bicsuiteObject.NOFORM_INTERVALS.TABLE = bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE[rowIndex].INTERVALS.TABLE;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve(0);
    }

    syncSubScheduleName(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        bicsuiteObject.NOFORM_SUBSCHEDULE = "";
        for (let subschedule of bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE) {
            if (subschedule.NOFORM_SELECTED) {
                bicsuiteObject.NOFORM_SUBSCHEDULE = subschedule.NAME;
                break;
            }
        }
        return this.checkMasterscheduleSelected(bicsuiteObject);
    }

    syncMasterScheduleName(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        bicsuiteObject.NOFORM_MASTERSCHEDULE = "";
        for (let masterSchedule of bicsuiteObject.MASTERSCHEDULES.TABLE) {
            if (masterSchedule.NOFORM_SELECTED) {
                bicsuiteObject.NOFORM_MASTERSCHEDULE = masterSchedule.NAME;
                break;
            }
        }
        return this.checkMasterscheduleSelected(bicsuiteObject);
    }

    AppendMasterschedule(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        return this.selectMasterschedule(bicsuiteObject, tabInfo, row.rowIndex);
    }

    AppendSubschedule(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        return this.selectSubschedule(bicsuiteObject, tabInfo, row.rowIndex);
    }

    DropMasterschedule(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        for (let masterschedule of bicsuiteObject.MASTERSCHEDULES.TABLE) {
            if (masterschedule.NOFORM_SELECTED) {
                // The dropped row was not the selected row
                return Promise.resolve(0);
            }
        }
        // the selected row was deleted, we have to select another one
        let selected = -1;
        if (row.rowIndex > 0) {
            // drop of non firt row, select prev row
            selected = row.rowIndex - 1;
        }
        else {
            if (bicsuiteObject.MASTERSCHEDULES.TABLE.length > 0) {
                // selected the following row now having the same rowIndex as our deleted row
                selected = row.rowIndex;
            }
        }
        if (selected >= 0) {
            return this.selectMasterschedule(bicsuiteObject, tabInfo, selected);
        }
        else {
            bicsuiteObject.NOFORM_MASTERSCHEDULE = "";
            bicsuiteObject.NOFORM_SUBSCHEDULE = "";
            bicsuiteObject.NOFORM_SUBSCHEDULES = {
                "TABLE": [],
            };
            bicsuiteObject.NOFORM_INTERVALS = {
                "TABLE": [],
            };
        }
        return Promise.resolve(0);
    }


    DropSubschedule(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        for (let subschedule of bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE) {
            if (subschedule.NOFORM_SELECTED) {
                // The dropped row was not the selected row
                return Promise.resolve(0);
            }
        }
        // the selected row was deleted, we have to select another one
        let selected = -1;
        if (row.rowIndex > 0) {
            // drop of non firt row, select prev row
            selected = row.rowIndex - 1;
        }
        else {
            if (bicsuiteObject.NOFORM_SUBSCHEDULES.TABLE.length > 0) {
                // selected the following row now having the same rowIndex as our deleted row
                selected = row.rowIndex;
            }
        }
        if (selected >= 0) {
            return this.selectSubschedule(bicsuiteObject, tabInfo, selected);
        }
        else {
            bicsuiteObject.NOFORM_SUBSCHEDULE = "";
            bicsuiteObject.NOFORM_INTERVALS = {
                "TABLE": [],
            };
        }
        return Promise.resolve(0);
    }

    checkMasterscheduleSelected(bicsuiteObject: any): boolean {
        for (let masterSchedule of bicsuiteObject.MASTERSCHEDULES.TABLE) {
            if (masterSchedule.NOFORM_SELECTED) {
                return true;
            }
        }
        return false;
    }
    conditionMasterscheduleSelected(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.checkMasterscheduleSelected(bicsuiteObject);
    }

    processIntervalTable(subSchedule: any, oldName: string, newName: string) : boolean {
        let affected: boolean = false;
        for (let interval of subSchedule.NOFORM_INTERVALS.TABLE) {
            if (interval.TYPE == "CLF" || interval.TYPE == "CLD") {
                if (interval.CALENDAR == oldName) {
                    interval.CALENDAR = newName;
                }
            }
        }
        return affected;
    }

    subScheduleIsActive(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): any {
        let cssFlag = '';
        if(bicsuiteObject.NOFORM_SELECTED == true){
            cssFlag = 'crud-border-color-grey'
        }
        return cssFlag
    }

    masterScheduleIsActive(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): any {
        let cssFlag = '';
        if(bicsuiteObject.NOFORM_SELECTED == true){
            cssFlag = 'crud-border-color-grey'
        }
        return cssFlag
    }

    openSeName(bicsuiteObject: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.SE_NAME, bicsuiteObject.SE_TYPE.toLowerCase());
    };

    openSeOwner(bicsuiteObject: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        return this.openGroup(bicsuiteObject.SE_OWNER);
    };

    processIntervalRename(bicsuiteObject: any, tabInfo: Tab, initialBicsuiteObject: any, message: any): Promise<boolean> {
        let affected : boolean = false;
        if (bicsuiteObject?.DATA?.RECORD?.NOFORM_SUBSCHEDULES) {
            for (let subSchedule of bicsuiteObject.DATA.RECORD.NOFORM_SUBSCHEDULES.TABLE) {
                if (this.processIntervalTable(subSchedule, message.OLD_NAME, message.NEW_NAME)) {
                    affected = true;
                }
            }
            if (initialBicsuiteObject && initialBicsuiteObject.DATA && initialBicsuiteObject.DATA.RECORD) {
                for (let subSchedule of initialBicsuiteObject.DATA.RECORD.NOFORM_SUBSCHEDULES.TABLE) {
                    if (this.processIntervalTable(subSchedule, message.OLD_NAME, message.NEW_NAME)) {
                        affected = true;
                    }
                }
            }
        }
        return Promise.resolve(affected);
    }

    getChangeConfig() {
        return {
            INTERVAL: {
                RENAME: {
                    METHOD: "processIntervalRename",
                    FIELDS: [
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
