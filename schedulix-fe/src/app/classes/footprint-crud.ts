import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import environment_editorform from '../json/editorforms/fp.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { group } from "@angular/animations";

export class FootprintCrud extends Crud {
    createWith(mode: string, obj: any, tabInfo: Tab) {
        let withClause: string = "";
        withClause = " with\n";
        let sep: string = "";

        withClause += " resource = (";
        for (let i = 0; i < obj.RESOURCES.TABLE.length; i++) {
            let resource = obj.RESOURCES.TABLE[i];
            if (resource.CONDITION == null || resource.CONDITION == undefined) {
                resource.CONDITION = ''
            }
            withClause += sep + this.toolbox.namePathQuote(resource.RESOURCE_NAME) + " amount = " + resource.AMOUNT + " " + resource.KEEP_MODE;
            sep = ", ";
        }
        withClause += ")";
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create footprint '" + obj.NAME + "' " + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "FP", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show footprint '" + tabInfo.NAME + "' with expand = all";
        return this.commandApi.execute(stmt).then((response: any) => {
            tabInfo.ID = response.DATA.RECORD.ID;
            let job_definitions = [];
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "begin multicommand\n";
        // rename
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename footprint '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
            obj.F_REFRESH_NAVIGATOR = true;
        }
        stmt += "alter footprint '" + obj.NAME + "' " + this.createWith("UPDATE", obj, tabInfo) + ";\n";

        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "FP", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Footprint}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop footprint '" + obj.ORIGINAL_NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "FP", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    custom(params: any, bicsuiteObject: any, tabInfo?: Tab): void {
        if (params.hasOwnProperty('CLICK_METHOD')) {
            if (params.CLICK_METHOD == 'OPEN_LISTITEM') {
                if (bicsuiteObject.hasOwnProperty('SE_PATH')) {
                    let pathlist = bicsuiteObject.SE_PATH.split('.');
                    let name = pathlist.pop();
                    let path = pathlist.join('.');

                    let folderTab = new Tab(name, bicsuiteObject.ID, bicsuiteObject.TYPE.toUpperCase(), bicsuiteObject.TYPE.toLowerCase(), TabMode.EDIT);
                    folderTab.PATH = path;

                    this.eventEmitterService.createTab(folderTab, true);
                }
            }
        }
    }

    openResourceName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        // console.log(bicsuiteObject)
        return this.openNamedResource(bicsuiteObject.RESOURCE_NAME, "system");
    }

    editorform() {
        return environment_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["RESOURCES"] = {
            TABLE: [{
                RESOURCE_NAME: "",
                AMOUNT: "1",
                KEEP_MODE: "NOKEEP",
                NOFORM_DISABLE_AUTOCHOOSE: "true"
            }],
            DESC: [
                "RESOURCE_NAME",
                "AMOUNT",
                "KEEP",
                "NOFORM_DISABLE_AUTOCHOOSE"]
        };
        return bicsuiteObj;
    }

    getChangeConfig() {
        return {
            NR: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCE_NAME"
                        }
                    ]
                }
            },
            CATEGORY: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCE_NAME"
                        }
                    ]
                }
            },
            FOLDER: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "JOB_DEFINITIONS",
                            NAME: "SE_PATH"
                        }
                    ]
                }
            },
            JOB_DEFINITION: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "JOB_DEFINITIONS",
                            NAME: "SE_PATH"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
