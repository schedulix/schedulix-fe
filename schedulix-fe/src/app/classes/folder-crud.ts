import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import { EditorformOptions } from "../data-structures/editorform-options";
import { Tab, TabMode } from "../data-structures/tab";
import { Crud } from "../interfaces/crud";
import folder_editorform from '../json/editorforms/batchesandjobs/folder.json';
import { NewItemDialogComponent } from "../modules/main/components/dialog-components/new-item-dialog/new-item-dialog.component";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";
import editFolderParameterComment_editorform from '../json/editorforms/editFolderParameterComment_editorForm.json';
import { OutputType, SnackbarType } from "../modules/global-shared/components/snackbar/snackbar-data";
import { BFCommonCrud } from "./bf-common-crud";
import { SdmsClipboardMode, SdmsClipboardType } from "../modules/main/services/clipboard.service";
export class FolderCrud extends BFCommonCrud {

    new(obj: any): Promise<Object> {
        const dialogRef = this.dialog.open(NewItemDialogComponent, {
            data: {
                title: "new_item",
                translate: "create_new_item_chooser",
                itemTypes: ['FOLDER', 'BATCH', 'JOB', 'MILESTONE'],
                path: obj.PATH ? obj.PATH + '.' + obj.NAME : obj.NAME
            },
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
        });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result !== undefined && result) {
                let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
                tab.PATH = result.path;
                if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
                    if (!tab.DATA) {
                        tab.DATA = {}
                    }
                    tab.DATA.IN_CONVENTION_FOLDER = obj.IS_CONVENTION_FOLDER;
                    tab.DATA.IN_CONVENTION_FOLDER_TYPE = obj.IS_CONVENTION_FOLDER_TYPE;
                }
                this.eventEmitterService.createTab(tab, true);
            }
        });
        return Promise.resolve({});
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts: string[] = [];
        stmts.push("create folder " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + obj.NAME) + this.createWith(obj, tabInfo));
        if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
            if (obj.CREATE_BATCH == "true") {
                stmts.push(this.createAddFolderBatchCommand(obj));
                if (obj.ADD_CHILD == "true" && obj.IN_CONVENTION_FOLDER == "true") {
                    stmts.push(this.createAddChildCommand(tabInfo.PATH + "." + tabInfo.PATH.split('.').pop(),
                        tabInfo.PATH + '.' + obj.NAME + '.' + obj.NAME,
                        obj.STATIC == "true"));
                }
            }
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "CREATE", NAME: tabInfo.PATH + "." + obj.NAME });
            return response;
        });
    }

    createWith(obj: any, tabInfo: Tab) {
        let withClause = " with";
        if (obj.ENVIRONMENT && obj.ENVIRONMENT != "NONE") {
            withClause += " environment = '" + obj.ENVIRONMENT + "'"
        }
        else {
            withClause += " environment = none"
        }

        if (this.privilegesService.isEdition('BASIC')) {
            if (obj.INHERIT_GRANTS == 'ALL') {
                withClause += ', inherit grant = ' + '(DROP,EDIT,MONITOR,OPERATE,SUBMIT,RESOURCE,VIEW)';
            } else if (obj.INHERIT_GRANTS == 'VIEW') {
                withClause += ', inherit grant = ' + '(VIEW)';
            } else {
                withClause += ', inherit grant = ' + 'NONE';
            }
        }

        if (obj.OWNER != obj.ORIGINAL_OWNER || tabInfo.MODE == TabMode.NEW) {
            withClause += ", group = '" + obj.OWNER + "'";
            if (obj.CASCADE_SET_GROUP == "true") {
                withClause += " " + "cascade";
            }
        }

        let haveParameters: boolean = false;
        let parameters = [];
        if (obj.PARAMETERS.TABLE.length > 0) {
            for (let parameter of obj.PARAMETERS.TABLE) {
                if (parameter.DEFINITION != (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME)) {
                    continue;
                }
                let name = this.convertIdentifier(parameter.NAME, parameter.ORIGINAL_NAME);
                parameters.push("'" + name + "' = '" + this.toolbox.textQuote(parameter.DEFAULT_VALUE) + "'");
                haveParameters = true;
            }
        }
        if (haveParameters) {
            withClause += ", parameters = (\n" + parameters.join(', ') + ")";
        } else {
            withClause += ", parameters = none";
        }
        return withClause;
    }

    clone(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts: string[] = [];
        stmts.push("copy folder "
            + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.ORIGINAL_NAME)
            + " to "
            + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME));
        if (obj.RENAME_BATCH == "true") {
            stmts.push(this.createRenameBatchInFolderCommand(obj));
        }
        if (obj.ADD_CHILD == "true") {
            let childName = obj.NAME;
            if (obj.RENAME_BATCH == "false") {
                childName = obj.ORIGINAL_NAME;
            }
            stmts.push(this.createAddChildCommand(tabInfo.PATH + "." + tabInfo.PATH.split('.').pop(),
                tabInfo.PATH + '.' + obj.NAME + '.' + childName,
                obj.STATIC == "true"));
        }
        stmts.push("alter folder " + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME) + this.createWith(obj, tabInfo));
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            tabInfo.NAME = obj.NAME;
            this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "CREATE", NAME: tabInfo.PATH + "." + obj.NAME });
            return response;
        });
    }

    showContent(folderName: string): Promise<Object> {
        let stmt: string = "list folder " + folderName;
        return this.commandApi.execute(stmt).then((response: any) => {
            response.DATA.TABLE.shift() // remove first item (itself)
            response.DATA.TABLE = response.DATA.TABLE.sort(function (a: any, b: any) {
                if (a.TYPE == "FOLDER" && b.TYPE != "FOLDER") {
                    return -1;
                }
                if (a.TYPE != "FOLDER" && b.TYPE == "FOLDER") {
                    return 1;
                }
                if (a.NAME.toUpperCase() < b.NAME.toUpperCase()) {
                    return -1;
                }
                if (a.NAME.toUpperCase() > b.NAME.toUpperCase()) {
                    return 1;
                }
                return 0;
            })
            let cutIds = this.clipboardService.getCutIds();
            for (let row of response.DATA.TABLE) {
                if (cutIds.includes(row.ID)) {
                    row.fe_flagged_to_cut = true;
                }
            }
            return response.DATA;
        });
    }

    filterResources(resources : any[], path : string) : any[] {

        // process resource table after load
        for (let resource of resources) {
            if (resource.FREE_AMOUNT >= 0 && resource.FREE_AMOUNT != null) {
                let percentage = ((parseInt(resource.AMOUNT) - parseInt(resource.FREE_AMOUNT)) / (parseInt(resource.AMOUNT) / 100));
                resource.LOAD = percentage ? percentage.toString() : '0';
            }
            if (resource.USAGE == 'CATEGORY') {
                resource.NOFORM_BULK_DISABLED = true;
            }
            resource.fe_flagged_to_cut = false;
            resource.DISPLAY_SCOPE = resource.SCOPE;
            if (path == resource.SCOPE) {
                resource.DISPLAY_SCOPE = '';
            }
        }
        return resources;
    }

    reloadResources(bicsuiteObject: any, tabInfo: Tab) {
        let stmt = "show folder " + this.getQuotedFullName(tabInfo);
        return this.commandApi.execute(stmt).then((response: any) => {
            let record = response.DATA.RECORD;
            bicsuiteObject.NOFORM_RESOURCES.TABLE =
                this.filterResources(record.DEFINED_RESOURCES.TABLE, (record.PATH ? record.PATH + '.' + record.NAME : record.NAME));
            this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show folder " + this.getQuotedFullName(tabInfo);
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            let record = response.DATA.RECORD;
            tabInfo.ID = record.ID;

            record.FULLNAME = this.toolbox.namePathQuote(record.NAME);
            let pathlist = record.NAME.split(".");
            record.NAME = pathlist.pop();
            record.PATH = pathlist.join(".");
            record.QUOTED_FULLNAME = this.getQuotedFullName(tabInfo);
            record.ORIGINAL_OWNER = record.OWNER;
            if (!record.ENVIRONMENT) {
                record.ENVIRONMENT = '';
            }
            record.NOFORM_RESOURCES = {}
            record.NOFORM_RESOURCES.TABLE =
                this.filterResources(record.DEFINED_RESOURCES.TABLE, (record.PATH ? record.PATH + '.' + record.NAME : record.NAME));
            return this.showContent(record.QUOTED_FULLNAME).then(content => {
                record.NOFORM_CONTENT = content; // from showContent
                // map inherit grants for basic
                this.setInheritGrants(record);
                for (let parameter of record.PARAMETERS.TABLE) {
                    parameter.ORIGINAL_NAME = parameter.NAME;
                    parameter.PARENTNAME = record.PATH + '.' + record.NAME;
                    if (parameter.COMMENT) {
                        parameter.COMMENT = parameter.COMMENT.trim();
                    }
                    parameter.NOFORM_COMMENT = parameter.COMMENT;
                    if (!this.conditionParameterDefinedHere({}, parameter, tabInfo)) {
                        parameter.NOFORM_BULK_DISABLED = true;
                    }
                }
                this.sortParameters(record);
                promise.response = response; // from showFolder
                if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
                    return this.checkConventionFolder(record).then(content => {
                        if (record.IS_CONVENTION_FOLDER == "true") {
                            record.RENAME_BATCH = "true";
                        }
                        if (record.IN_CONVENTION_FOLDER == "true") {
                            record.ADD_CHILD = "true";
                            if (record.IN_CONVENTION_FOLDER_TYPE == "JOB") {
                                record.STATIC = "false";
                            }
                            else {
                                record.STATIC = "true";
                            }
                        }
                        return promise;
                    });
                }
                return promise;
            })
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        // name casing
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);

        let stmts: string[] = [];

        if (obj.ORIGINAL_NAME != obj.NAME) {
            stmts.push("rename folder " + obj.QUOTED_FULLNAME + " to '" + this.toolbox.namePathQuote(obj.NAME) + "'");
            if (obj.RENAME_BATCH == "true") {
                stmts.push(this.createRenameBatchInFolderCommand(obj));
            }
        }
        stmts.push("alter folder " + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME) + this.createWith(obj, tabInfo));
        for (let parameter of obj.PARAMETERS.TABLE) {
            if (parameter.NOFORM_comments) {
                let stmt = "create comment on parameter '" + parameter.NAME + "' of folder " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
                let withStmt = []
                for (let c of parameter.NOFORM_comments) {
                    c.TAG = c.TAG ? c.TAG : '';
                    let desc = c.COMMENT ? this.toolbox.textQuote(c.COMMENT) : '';
                    withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'");
                }
                stmt += withStmt.join(',');
                stmts.push(stmt);
            }
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            if (obj.ORIGINAL_NAME != obj.NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "RENAME", OLD_NAME: tabInfo.PATH + "." + tabInfo.NAME, NEW_NAME: tabInfo.PATH + "." + obj.NAME });
                tabInfo.NAME = obj.NAME;
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        let fields = [
            {
                'NAME': 'CASCADE',
                'TRANSLATE': 'cascade',
                'TYPE': 'CHECKBOX',
                'USE_LABEL': true,
                "ON_CHANGE": "REFRESH_CONDITIONS"
            },
            {
                'NAME': 'FORCE',
                'TRANSLATE': 'force',
                'TYPE': 'CHECKBOX',
                'USE_LABEL': true,
                "VISIBLE_CONDITION": "conditionDropCascade"
            }
        ]
        return this.createConfirmDialog(fields, "drop_confirm{" + obj.TYPE + "}{" + tabInfo.PATH + "." + obj.ORIGINAL_NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop folder " + obj.QUOTED_FULLNAME;
                if (result.CASCADE == "true") {
                    stmt += ' cascade';
                    if (result.FORCE == "true") {
                        stmt += ' force';
                    }
                }
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "DROP", NAME: tabInfo.PATH + "." + obj.ORIGINAL_NAME });
                    return response;
                });
            }
            return {};
        });
    }

    openParameterFolder(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openFolder(bicsuiteObject.DEFINITION);
    }

    openPath(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openFolder(bicsuiteObject.PATH);
    }

    editorform() {
        return folder_editorform;
    }

    custom(params: any, bicsuiteObject: any, tabInfo?: Tab): void {
        if (params.hasOwnProperty('CLICK_METHOD')) {
            if (params.CLICK_METHOD == 'OPEN_LISTITEM') {
                if (bicsuiteObject.hasOwnProperty('TYPE')) {
                    let pathlist = bicsuiteObject.NAME.split('.');
                    let name = pathlist.pop();
                    let path = pathlist.join('.');

                    let folderTab = new Tab(name, bicsuiteObject.ID, bicsuiteObject.TYPE.toUpperCase(), bicsuiteObject.TYPE.toLowerCase(), TabMode.EDIT);
                    folderTab.PATH = path;

                    this.eventEmitterService.createTab(folderTab, true);
                }
            }
        }
    }

    manipulateEditorformOptions(editorformOptions: EditorformOptions, editorformField: any, bicsuiteObject: any, tabInfo: any): void {
        super.manipulateEditorformOptions(editorformOptions, editorformField, bicsuiteObject, tabInfo);
        if (editorformField.hasOwnProperty('NAME')) {
            // inside table: bicsuiteobject == row

            if (editorformField.NAME == "IS_ONLINE") {

                if (bicsuiteObject.IS_ONLINE == 'true') {
                    editorformOptions.flags.CSS_CLASS += ' crud-color-green';
                }
            }
        }
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)

        let grantObject = {
            TYPE: bicsuiteObject.TYPE,
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME,
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS: '',
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;
                if (grantObject.GRANTS.TABLE[0]) {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }
            }
            let editorForm = crud.editorformByType(grantObject.TYPE, true, true);
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for " + bicsuiteObject.TYPE + " " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', "fe-no-border-dialog"],
                width: "100%"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }

    createResource(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let definedResourceTab = new Tab('*untitled', new Date().getTime().toString(), 'RESOURCE'.toUpperCase(), 'resource', TabMode.NEW);
        // console.log(bicsuiteObject);
        definedResourceTab.DATA = {};
        definedResourceTab.DATA.TYPE = bicsuiteObject.TYPE;
        definedResourceTab.DATA.RESOURCE_NAME = bicsuiteObject.RESOURCE_NAME;
        definedResourceTab.DATA.USAGE = bicsuiteObject.USAGE;
        definedResourceTab.DATA.RESOURCE_ID = bicsuiteObject.RESOURCE_ID;
        // definedResourceTab.DATA.f_IsInstance
        // definedResourceTab.DATA.NOFORM_TIME_ZONE

        definedResourceTab.DATA.f_isScopeOrFolderInstance = true;
        definedResourceTab.OWNER = (bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.ORIGINAL_NAME : bicsuiteObject.ORIGINAL_NAME);
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    }

    openResourceItemFromTree(bicsuiteObj: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        let usage = node.data.USAGE;
        let type = 'RESOURCE';
        if (usage == "CATEGORY") {
            type = "NAMED RESOURCE";
        }
        let pathList = node.data.NAME.split('.');
        let name = pathList.pop();
        let path = pathList.join(".")
        let definedResourceTab = new Tab(name, node.data.ID, type, node.data.USAGE.toLowerCase(), TabMode.EDIT);
        // console.log(definedResourceTab)

        definedResourceTab.DATA = {};
        definedResourceTab.DATA.TYPE = node.data.TYPE;
        definedResourceTab.DATA.RESOURCE_NAME = node.data.RESOURCE_NAME;
        definedResourceTab.DATA.USAGE = node.data.USAGE;
        definedResourceTab.DATA.RESOURCE_ID = node.data.RESOURCE_ID;
        // mandatory for resource instance
        if (usage != "CATEGORY") {
            definedResourceTab.DATA.RESOURCE_OWNER_TYPE = tabInfo.TYPE;
            definedResourceTab.DATA.RESOURCE_ID = node.data.ID;
            definedResourceTab.DATA.RESOURCE_NAME = node.data.RESOURCE_NAME;
            definedResourceTab.DATA.f_isScopeOrFolderInstance = true;
        }
        else {
            definedResourceTab.PATH = path;
        }

        // definedResourceTab.OWNER = bicsuiteObject.PATH + '.' + node.data.NAME;
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    };

    openContentListItem(bicsuiteObject: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.NAME, bicsuiteObject.TYPE.toLowerCase());
    };

    editParameterComment(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show comment on parameter '" + bicsuiteObject.NAME + "' of folder " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        return this.commandApi.execute(stmt).then((response1: any) => {
            for (let o of response1.DATA.TABLE) {
                o.DESCRIPTION = o.COMMENT
            }
            let dataObj: any = {
                COMMENT: {
                    TABLE: response1.DATA.TABLE
                },
                NAME: bicsuiteObject.NAME
            };
            if (bicsuiteObject.PRIVS) {
                dataObj.PRIVS = bicsuiteObject.PRIVS;
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editFolderParameterComment_editorform, dataObj, tabInfo, "Edit Parameter Comments", this),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary',"fe-no-border-dialog"],
                width: "1000px"
            });
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                // Update Original Object
                let visibleComment = []
                for (let c of result.COMMENT.TABLE) {
                    visibleComment.push(c.TAG + "\n\n" + c.DESCRIPTION)
                }
                bicsuiteObject.NOFORM_COMMENT = visibleComment.join('\n\n\n')
                return result;
            });
        });
    }

    saveParameterComments(bicsuiteObject: any, tabInfo: Tab, deepCopyBicsuiteObject: any): Promise<Object> {
        let stmt;
        if (deepCopyBicsuiteObject.COMMENT.TABLE.length > 0) {
            stmt = "create or alter comment on parameter '" + bicsuiteObject.NAME + "' of folder " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
            let withStmt = []
            for (let c of deepCopyBicsuiteObject.COMMENT.TABLE) {
                c.TAG = c.TAG ? c.TAG : '';
                let desc = c.DESCRIPTION ? this.toolbox.textQuote(c.DESCRIPTION) : '';
                withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'")
            }
            stmt += withStmt.join(',')
        }
        else {
            stmt = "drop comment on parameter '" + bicsuiteObject.NAME + "' of folder " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            if (response.hasOwnProperty("FEEDBACK")) {
                this.eventEmitterService.pushSnackBarMessage([response.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);
            }
            return response;
        });
    }

    appendParameter(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        row.DEFINITION = (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME);
        return Promise.resolve({});
    }

    copyContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.FROM_ITEM_TYPE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = false;
        }
        return Promise.resolve({});
    }

    cutContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.FROM_ITEM_TYPE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = true;
        }
        return Promise.resolve({});
    }

    doPaste(bicsuiteObject: any, tabInfo: Tab, itemsToBePasted: any[]): Promise<Object> {
        let stmts: string[] = [];
        let copyItems: any[] = [];
        let sourceObject: any = {};
        let sourceTabInfo: Tab;
        let sourcePathes: any[] = [];

        for (let item of itemsToBePasted) {
            sourceObject = item.sourceObject;
            sourceTabInfo = item.sourceTabInfo;
            if (item.mode == SdmsClipboardMode.COPY) {
                copyItems.push(item);
            }
            else {
                let p = item.data.NAME.split('.');
                p.pop();
                let sourcePath = p.join('.');
                if (sourcePath != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) {
                    let stmt = "move " +
                        (item.data.TYPE == "FOLDER" ? "folder" : "job definition") + " " +
                        this.toolbox.namePathQuote(item.data.NAME) + " to " +
                        this.toolbox.namePathQuote(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME);
                    stmts.push(stmt);
                    if (!sourcePathes.includes(sourcePath)) {
                        sourcePathes.push(sourcePath);
                    }
                }
                else {
                    if (item.mode == SdmsClipboardMode.CUT) {
                        if (!sourcePathes.includes(sourcePath)) {
                            sourcePathes.push(sourcePath);
                        }
                        item.mode = SdmsClipboardMode.COPY;
                        item.data.fe_flagged_to_cut = false;
                    }
                    // No deep copy necessary here because we re read content later anyway, so no cross editing can occure
                }
                if (sourcePath != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME || tabInfo.FROM_CONTEXT_MENU) {
                    // put target path in sourcepathes to get target refreshed if paste is done from tree context menu
                    if (!sourcePathes.includes(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME)) {
                        sourcePathes.push(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME);
                    }
                }
            }
        }
        if (copyItems.length > 0) {
            let stmt = "copy\n";
            let copyList: string[] = [];
            for (let item of copyItems) {
                copyList.push((item.data.TYPE == "FOLDER" ? "folder" : "job definition") + " " + this.toolbox.namePathQuote(item.data.NAME));
            }
            stmt += "    " + copyList.join(",\n    ");
            stmt += "\nto " + this.toolbox.namePathQuote(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME);
            stmts.push(stmt);
        }

        let dropChildren: any[] = [];
        let addChildren: any[] = [];
        let dataObj: any = {};
        dataObj.SOURCE_PARENT = sourceObject.PATH + "." + sourceObject.NAME + "." + sourceObject.NAME;
        dataObj.SOURCE_PARENT_TYPE = sourceObject.IS_CONVENTION_FOLDER_TYPE;
        dataObj.TARGET_PARENT = bicsuiteObject.PATH + "." + bicsuiteObject.NAME + "." + bicsuiteObject.NAME;
        dataObj.TARGET_PARENT_TYPE = bicsuiteObject.IS_CONVENTION_FOLDER_TYPE;
        let fields: any[] = [];
        if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
            for (let item of itemsToBePasted) {
                if (item.sourceObject.PATH + "." + item.sourceObject.NAME != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) {
                    let childName: string;
                    let childType: string;
                    if (item.data.TYPE == 'FOLDER') {
                        if (item.data.IS_CONVENTION_FOLDER == 'true') {
                            childName = item.data.NAME + '.' + item.data.NAME.split('.').pop();
                            childType = item.data.IS_CONVENTION_FOLDER_TYPE;
                        }
                        else {
                            // no drop/add child on copy/move non convention folders
                            continue;
                        }
                    }
                    else {
                        childName = item.data.NAME;
                        childType = item.data.TYPE;
                        if (childName == dataObj.SOURCE_PARENT) {
                            // we are pasting a convention folder main batch/job
                            // no drop children have to be generated
                            dropChildren = [];
                            // only the pasted convention folder can be added as child
                            if (bicsuiteObject.IS_CONVENTION_FOLDER == "true") {
                                addChildren = [{
                                    'ADD': 'true',
                                    'TYPE': childType,
                                    'NAME': childName.replace(sourceObject.PATH + "." + sourceObject.NAME, bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME)
                                }]
                            }
                            break;
                        }
                    }
                    if (item.mode == SdmsClipboardMode.CUT) {
                        // console.log("sourceObject.IS_CONVENTION_FOLDER = " + sourceObject.IS_CONVENTION_FOLDER)
                        if (sourceObject.IS_CONVENTION_FOLDER == "true") {
                            dropChildren.push({ 'DROP': 'true', 'TYPE': childType, 'NAME': childName });
                        }
                    }
                    // console.log("bicsuiteObject.IS_CONVENTION_FOLDER = " + bicsuiteObject.IS_CONVENTION_FOLDER)
                    if (bicsuiteObject.IS_CONVENTION_FOLDER == "true") {
                        addChildren.push({
                            'ADD': 'true',
                            'TYPE': childType,
                            'NAME': childName.replace(sourceObject.PATH + "." + sourceObject.NAME, bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME)
                        });
                    }
                }
            }
            dataObj.DROP_CHILDREN = { 'TABLE': dropChildren };
            dataObj.ADD_CHILDREN = { 'TABLE': addChildren };
            if (dropChildren.length > 0) {
                fields.push(
                    {
                        "NAME": "SOURCE_PARENT",
                        "TYPE": "TEXT",
                        "TRANSLATE": "drop_children_from",
                        "ICON_LOOKUP": {
                            "PREFIX": "",
                            "RECORD_PROPERTY": "SOURCE_PARENT_TYPE"
                        },
                        "USE_LABEL": true
                    },
                    {
                        "NAME": "DROP_CHILDREN",
                        "TYPE": "TABLE",
                        "SYS_OBJ_EDITABLE": true,
                        "TABLE": {
                            "FIELDS": [
                                {
                                    "NAME": "DROP",
                                    "SYS_OBJ_EDITABLE": true,
                                    "TYPE": "CHECKBOX"
                                },
                                {
                                    "NAME": "NAME",
                                    "TYPE": "TEXT",
                                    "ICON_LOOKUP": {
                                        "PREFIX": "",
                                        "RECORD_PROPERTY": "TYPE"
                                    },
                                    "USE_LABEL": false
                                }
                            ]
                        }
                    }
                );
            }
            if (addChildren.length > 0) {
                fields.push(
                    {
                        "NAME": "TARGET_PARENT",
                        "TYPE": "TEXT",
                        "TRANSLATE": "add_children_to",
                        "ICON_LOOKUP": {
                            "PREFIX": "",
                            "RECORD_PROPERTY": "TARGET_PARENT_TYPE"
                        },
                        "USE_LABEL": true
                    },
                    {
                        "NAME": "ADD_CHILDREN",
                        "TYPE": "TABLE",
                        "SYS_OBJ_EDITABLE": true,
                        "TABLE": {
                            "FIELDS": [
                                {
                                    "NAME": "ADD",
                                    "SYS_OBJ_EDITABLE": true,
                                    "TYPE": "CHECKBOX"
                                },
                                {
                                    "NAME": "NAME",
                                    "TYPE": "TEXT",
                                    "ICON_LOOKUP": {
                                        "PREFIX": "",
                                        "RECORD_PROPERTY": "TYPE"
                                    },
                                    "USE_LABEL": false
                                }
                            ]
                        }
                    }
                );
            }
        }
        if (fields.length > 0) {
            return this.createConfirmDialog(fields, "", tabInfo, "", bicsuiteObject, dataObj).afterClosed().toPromise().then((result: any) => {
                if (result == undefined || result == null || !result) {
                    return Promise.resolve({});
                }
                for (let dropChild of dataObj.DROP_CHILDREN.TABLE) {
                    if (dropChild.DROP == "true") {
                        let newName = dropChild.NAME.replace(
                            sourceObject.PATH + "." + sourceObject.NAME,
                            bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME);
                        stmts.push(
                            "alter job definition " + this.toolbox.namePathQuote(dataObj.SOURCE_PARENT) +
                            " delete existing children = (" + this.toolbox.namePathQuote(newName) + ")"
                        );
                    }
                }
                for (let addChild of dataObj.ADD_CHILDREN.TABLE) {
                    if (addChild.ADD == "true") {
                        stmts.push(
                            "alter job definition " + this.toolbox.namePathQuote(dataObj.TARGET_PARENT) +
                            " add children = (" + this.toolbox.namePathQuote(addChild.NAME) + ")"
                        );
                    }
                }
                /// sperren
                // this.eventEmitterService.setTabLockEmitter.next({Tab: tabInfo, lock: true});
                return this.doPasteExecuteStatement(stmts, itemsToBePasted, sourcePathes, bicsuiteObject, tabInfo)
            });
        }
        else {
          return this.doPasteExecuteStatement(stmts, itemsToBePasted, sourcePathes, bicsuiteObject, tabInfo);
        }
    }

    doPasteExecuteStatement(stmts: string[], itemsToBePasted: any[], sourcePathes: any[], bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            for (let item of itemsToBePasted) {
                item.mode = SdmsClipboardMode.COPY;
            }
            for (let sourcePath of sourcePathes) {
                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "CREATE", NAME: sourcePath + ".DUMMY" });
            }
            return this.showContent(bicsuiteObject.QUOTED_FULLNAME).then(content => {
                bicsuiteObject.NOFORM_CONTENT = content;
                if (itemsToBePasted.length > 1) {
                    this.eventEmitterService.pushSnackBarMessage([itemsToBePasted.length + ' items have been inserted'], SnackbarType.INFO);
                } else {
                    this.eventEmitterService.pushSnackBarMessage(['Item has been inserted'], SnackbarType.INFO);
                }
                this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                this.eventEmitterService.refreshNavigationEmittor.next();
                return this.checkConventionFolder(bicsuiteObject);
            });
        });
    }

    folderPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.FOLDER, SdmsClipboardType.JOB, SdmsClipboardType.MILESTONE, SdmsClipboardType.BATCH]

    pasteContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        let itemsToBePasted: any[] = this.clipboardService.getItems(this.folderPasteTypes);
        if (itemsToBePasted.length == 0) {
            return Promise.resolve({});
        }
        let promises: Promise<any>[] = [];
        if (this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true") {
            for (let item of itemsToBePasted) {
                if (item.data.TYPE == "FOLDER") {
                    promises.push(this.doCheckConventionFolder(item.data, item.data.NAME, item.data.ID, "IS_CONVENTION_FOLDER"));
                }
            }
            if (promises.length > 0) {
                return Promise.all(promises).then((res) => {
                    return this.doPaste(bicsuiteObject, tabInfo, itemsToBePasted);
                });
            }
            else {
                return this.doPaste(bicsuiteObject, tabInfo, itemsToBePasted);
            }
        }
        else {
            return this.doPaste(bicsuiteObject, tabInfo, itemsToBePasted);
        }
    }

    dropContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {

        let fields: any[] = [];
        for (let selected of checklistSelection._selected) {
            if (selected.TYPE == 'FOLDER') {
                fields.push({
                    'NAME': 'CASCADE',
                    'TRANSLATE': 'cascade',
                    'TYPE': 'CHECKBOX',
                    "SYS_OBJ_EDITABLE": true,
                    'USE_LABEL': true
                });
                break;
            }
        }
        fields.push({
            'NAME': 'FORCE',
            'TRANSLATE': 'force',
            'TYPE': 'CHECKBOX',
            "SYS_OBJ_EDITABLE": true,
            'USE_LABEL': true
        });

        return this.createConfirmDialog(fields, "drop_confirm_selected", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmts: string[] = [];
                for (let selected of checklistSelection._selected) {
                    let type = selected.TYPE != 'FOLDER' ? "job definition" : "folder";
                    let stmt = "drop " + type + " " + this.toolbox.namePathQuote(selected.NAME);
                    if (type == 'folder' && result.CASCADE && result.CASCADE == "true") {
                        stmt += ' cascade';
                    }
                    if (((result.CASCADE && result.CASCADE == "true") || type != 'folder') && result.FORCE == "true") {
                        stmt += ' force';
                    }
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                    return this.showContent(bicsuiteObject.QUOTED_FULLNAME).then(content => {
                        bicsuiteObject.NOFORM_CONTENT = content;
                        for (let selected of checklistSelection._selected) {
                            if (selected.TYPE == 'FOLDER') {
                                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "FOLDER", OP: "DROP", NAME: selected.NAME, FROM_CONTENT: true });
                            }
                            else {
                                this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "JOB_DEFINITION", OP: "DROP", NAME: selected.NAME, FROM_CONTENT: true });
                            }
                        }
                        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                        this.eventEmitterService.refreshNavigationEmittor.next();
                        return this.checkConventionFolder(bicsuiteObject);
                    });
                });
            }
            return {};
        });
    }

    copyResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.RESOURCE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = false;
        }
        return Promise.resolve({});
    }

    cutResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.RESOURCE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = true;
        }
        return Promise.resolve({});
    }

    resourcesPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.RESOURCE, SdmsClipboardType.SYSTEM, SdmsClipboardType.SYNCHRONIZING]

    conditionClipBoardOfTypeResources(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // check if there is item to be pasted and if the source is not the same
        return this.clipboardService.checkItemTypesPresent(this.resourcesPasteTypes, bicsuiteObject.ID);
    }

    dropResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {

        return this.createConfirmDialog([], "drop_confirm_selected", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmts: string[] = [];
                for (let selected of checklistSelection._selected) {
                    let stmt = "drop resource " + this.toolbox.namePathQuote(selected.NAME) + " in " + this.toolbox.namePathQuote(selected.SCOPE);
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {

                    let dropRowIndexes = [];
                    for (let selected of checklistSelection._selected) {
                        this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "DROP", NAME: selected.NAME, OWNER: selected.SCOPE })
                        dropRowIndexes.push(selected.rowIndex);
                    }
                    let newResources = [];
                    for (let definedResource of bicsuiteObject.NOFORM_RESOURCES.TABLE) {
                        if (dropRowIndexes.includes(definedResource.rowIndex)) {
                            continue;
                        }
                        newResources.push(definedResource);
                    }
                    bicsuiteObject.NOFORM_RESOURCES.TABLE = newResources;

                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                });
            }
            return {};
        });
    }

    pasteResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        let stmts: string[] = [];
        let droppedResources : any[] =[];
        let resourceCreated : boolean = false;
        for (let item of this.clipboardService.getItems(this.resourcesPasteTypes)) {
            let stmt = "";

            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.RESOURCE:
                    if (item.data.USAGE == 'STATIC') {
                        continue; // no static defined resources !!!
                    }
                    if (item.data.SCOPE == tabInfo.PATH + '.' + tabInfo.NAME && item.mode == SdmsClipboardMode.CUT) {
                            item.mode = SdmsClipboardMode.COPY;
                            item.data.fe_flagged_to_cut = false;
                    }
                    else {
                        if (item.mode == SdmsClipboardMode.CUT) {
                            stmt = "drop resource " + this.toolbox.namePathQuote(item.data.NAME) + " in " + this.toolbox.namePathQuote(item.data.SCOPE);
                            stmts.push(stmt);
                            droppedResources.push(item);
                        }
                        stmts.push(this.genCreateResource(item, tabInfo));
                    }
                    break;
                case SdmsClipboardType.SYSTEM:
                case SdmsClipboardType.SYNCHRONIZING:
                    let defaultAmount = "1";
                    if (item.itemType ==  SdmsClipboardType.SYNCHRONIZING) {
                        defaultAmount = "INFINITE";
                    }
                    let c = {
                        data : {
                            NAME : item.data.NAME,
                            AMOUNT : defaultAmount,
                            REQUESTABLE_AMOUNT : defaultAmount,
                            IS_ONLINE : "true"
                        }
                    }
                    stmts.push(this.genCreateResource(c, tabInfo));
                    resourceCreated = true;
                    break;
            }
        }
        if (stmts.length > 0) {
            return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                for (let dropped of droppedResources) {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "DROP", NAME: dropped.data.NAME, OWNER: dropped.data.SCOPE })
                    dropped.mode = SdmsClipboardMode.COPY;
                    dropped.sourceId = "";
                }
                if (resourceCreated) {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "CREATE", NAME: 'DUMMY',
                        OWNER: (bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.ORIGINAL_NAME : bicsuiteObject.ORIGINAL_NAME) })
                }
                return Promise.resolve(false);
            });
        }
        else {
            return Promise.resolve(false);
        }
    }

    private genCreateResource(item: any, tabInfo: Tab) {
        let stmt = "create resource " +
            this.toolbox.namePathQuote(item.data.NAME) + " in " +
            this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) +
            " with " +
            " " + (item.data.IS_ONLINE == "true" ? "online" : "offline");
        if (item.data.STATE) {
            stmt += ", status = '" + item.data.STATE + "'";
        }
        if (item.data.USAGE != 'STATIC') {
            stmt += ", amount = " + item.data.AMOUNT + ", requestable_amount = " + item.data.REQUESTABLE_AMOUNT;
        }
        return stmt;
    }

    getSelectedParameterComments(bicsuiteObject: any, tabInfo: Tab, selectedParameter : any) : Promise<Object> {
        let stmt : string = "show comment on parameter '" + selectedParameter.ORIGINAL_NAME + "' of folder " +
            this.toolbox.namePathQuote(bicsuiteObject.PATH + '.' + bicsuiteObject.NAME);
        return this.commandApi.execute(stmt).then((response: any) => {
            selectedParameter.NOFORM_comments = response.DATA.TABLE;
            return response;
        });
    }

    getSelectedParametersComments(bicsuiteObject: any, tabInfo: Tab, selectedParameters : any[]) : Promise<Object> {
        let promises: Promise<any>[] = [];
        for (let selectedParameter of selectedParameters) {
            if (selectedParameter.COMMENT) {
                promises.push(this.getSelectedParameterComments(bicsuiteObject, tabInfo, selectedParameter));
            }
        }
        return Promise.all(promises).then((res) => {
            return {};
        });
    }

    sortParameters(bicsuiteObject : any) {
            bicsuiteObject.PARAMETERS.TABLE.sort(function(a : any, b : any) {
                if (a.PARENTNAME == a.DEFINITION && b.PARENTNAME != b.DEFINITION) {
                    return -1;
                }
                if (a.PARENTNAME != a.DEFINITION && b.PARENTNAME == b.DEFINITION) {
                    return 1;
                }
                return a.NAME < b.NAME ? -1 : 1;
            });
    }

    copyParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.FOLDER_PARAMETER);
            return {};
        });
    }

    cutParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.FOLDER_PARAMETER);
            return this.dropParameters(bicsuiteObject, tabInfo, form, checklistSelection);
        });
    }

    dropParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newParameters = [];
        for (let parameter of bicsuiteObject.PARAMETERS.TABLE) {
            if (dropRowIndexes.includes(parameter.rowIndex)) {
                continue;
            }
            newParameters.push(parameter);
        }
        bicsuiteObject.PARAMETERS.TABLE = newParameters;
        this.sortParameters(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    parameterPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.FOLDER_PARAMETER];

    conditionPasteParameters(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.parameterPasteTypes, tabInfo.ID);
    }

    pasteParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.parameterPasteTypes)) {
            // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
            let p = JSON.parse(JSON.stringify(item.data));
            let parentName = bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.NAME : bicsuiteObject.NAME;
            if (p.PARENTNAME != parentName) {
                p.ID = undefined; // this is  a new parameter!!!
            }
            item.mode = SdmsClipboardMode.COPY;
            p.DEFINITION = parentName;
            bicsuiteObject.PARAMETERS.TABLE.push(p);
        }
        this.sortParameters(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    conditionParameterDefinedHere(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let name = (tabInfo.PATH ? tabInfo.PATH + '.' + tabInfo.NAME : tabInfo.NAME);
        if (bicsuiteObject.DEFINITION == name) {
            return true;
        }
        else {
            return false;
        }
    }

    conditionIsNewOrParentParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.ID == undefined || (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME) != bicsuiteObject.DEFINITION;
    }

    conditionIsParentParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME) != bicsuiteObject.DEFINITION;
    }

    conditionCreateBatch(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.CREATE_BATCH == "true";
    }

    conditionInConventionFolder(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let result: boolean = (bicsuiteObject.IN_CONVENTION_FOLDER == "true" &&
            this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention"));
        return result;
    }

    conditionIsConventionFolder(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let result: boolean = (bicsuiteObject.IS_CONVENTION_FOLDER == "true" &&
            this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention"));
        return result;
    }

    conditionRenameBatchInConventionFolder(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") &&
            this.conditionIsConventionFolder(editorformField, bicsuiteObject, tabInfo) &&
            this.conditionNameChanged(editorformField, bicsuiteObject, tabInfo);
    }

    conditionAddChild(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (tabInfo.MODE == TabMode.NEW && bicsuiteObject.CREATE_BATCH == "true") &&
            bicsuiteObject.IN_CONVENTION_FOLDER == "true";
    }

    conditionAddChildOnClone(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return ((tabInfo.MODE == TabMode.EDIT && bicsuiteObject.NAME != bicsuiteObject.ORIGINAL_NAME && bicsuiteObject.IS_CONVENTION_FOLDER == "true")) &&
            bicsuiteObject.IN_CONVENTION_FOLDER == "true";
    }

    conditionIsAddChildJob(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (this.conditionAddChild(editorformField, bicsuiteObject, tabInfo) || this.conditionAddChildOnClone(editorformField, bicsuiteObject, tabInfo)) &&
            bicsuiteObject.ADD_CHILD == "true";
    }

    conditionIsNewFolderWithConventionSupport(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let result: boolean = (tabInfo.MODE == TabMode.NEW &&
            this.mainInformationService.getPrivateProperty("ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "enableBatchInFolderConvention") == "true");
        return result;
    }

    conditionOwnerChanged(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.OWNER != bicsuiteObject.ORIGINAL_OWNER && tabInfo.MODE != TabMode.NEW;
    }

    conditionDropCascade(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.CASCADE == "true";
    }

    conditionClipBoardOfTypeNotEmpty(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // check if there is item to be pasted and if the source is not the same
        return this.clipboardService.checkItemTypesPresent(this.folderPasteTypes, bicsuiteObject.ID);
    }

    default(tabInfo: Tab): any {
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.PATH = tabInfo.PATH;
        record.FULLNAME = this.toolbox.namePathQuote(tabInfo.PATH + '.');
        record.NAME = '';
        record.ORIGINAL_NAME = '';
        record.INHERIT_GRANTS = "ALL";
        record.ADD_CHILD = "true";
        record.OWNER = this.privilegesService.getDefaultGroup();
        record.IN_CONVENTION_FOLDER = tabInfo.DATA.IN_CONVENTION_FOLDER;
        record.IN_CONVENTION_FOLDER_TYPE = tabInfo.DATA.IN_CONVENTION_FOLDER_TYPE;
        if (record.IN_CONVENTION_FOLDER_TYPE == "JOB") {
            record.STATIC = "false";
        }
        else {
            record.STATIC = "true";
        }
        return bicsuiteObject;
    }

    openEnvironment(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        let tab = new Tab(bicsuiteObject.ENVIRONMENT, "", "ENVIRONMENT", "env", TabMode.EDIT);
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    processContentOrPathChange(bicsuiteObject: any, tabInfo: Tab, initialBicsuiteObject: any, message: any): Promise<boolean> {
        let affected: boolean = false;
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record.hasOwnProperty("PATH")) {
            return Promise.resolve(false);
        }
        if (message.OP == "RENAME") {
            let oldName = message.OLD_NAME;
            let newName = message.NEW_NAME;
            if (tabInfo.PATH == oldName) {
                tabInfo.PATH = newName;
                tabInfo.updateTooltip();
            }
            else {
                if (tabInfo.PATH.startsWith(oldName + '.')) {
                    tabInfo.PATH = tabInfo.PATH.replace(oldName + '.', newName + '.');
                    tabInfo.updateTooltip();
                }
            }
            // Handle Content Change
            let fullName = (record.PATH && record.PATH != '') ? record.PATH + "." + record.NAME : record.NAME
            if (oldName) {
                let pathList = oldName.split('.');
                pathList.pop();
                if (pathList.join('.') == fullName) {
                    affected = true;
                }
            }
            if (newName) {
                let pathList = newName.split('.');;
                pathList.pop();
                if (pathList.join('.') == fullName) {
                    affected = true;
                }
            }
            // Handle Path Change
            if (oldName && newName) {
                if (record.PATH == oldName) {
                    record.PATH = newName;
                }
                else {
                    if (record.PATH.startsWith(oldName + '.')) {
                        record.PATH = record.PATH.replace(oldName + '.', newName + '.');
                        // record.QUOTED_FULLNAME = (tabInfo.PATH && tabInfo.PATH != '') ? this.toolbox.namePathQuote(tabInfo.PATH + "." + tabInfo.NAME) : tabInfo.NAME;
                        record.QUOTED_FULLNAME = this.getQuotedFullName(tabInfo);
                    }
                }
            }
        }
        if (message.OP == "DROP" || message.OP == "CREATE") {
            if (!message.FROM_CONTENT) {
                let path = message.NAME.split(".");
                path.pop()
                if ((tabInfo.PATH + '.' + tabInfo.NAME) == path.join('.')) {
                    affected = true;
                }
            }
        }
        // reload content if content changed
        if (affected && record.QUOTED_FULLNAME) {
            this.showContent(record.QUOTED_FULLNAME).then(content => {
                record.NOFORM_CONTENT = content;
                return this.checkConventionFolder(record).then(result => {
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                });
            });
        }
        return Promise.resolve(affected);
    }

    processResourceChange (bicsuiteObject: any, tabInfo: Tab, intialBicsuiteObject: any, message: any): Promise<boolean> {
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.NOFORM_RESOURCES) {
            return Promise.resolve(false);
        }
        let owner = tabInfo.PATH + '.' + tabInfo.NAME;
        if ((owner + '.').startsWith(message.OWNER + '.')) {
            this.reloadResources(record, tabInfo).then (() => {
                return Promise.resolve(false);
            });
        }
        return Promise.resolve(false);
    }

    getChangeConfig() {
        return {
            CATEGORY: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "NAME"
                        }
                    ]
                }
            },
            FOLDER: {
                RENAME: {
                    METHOD: "processContentOrPathChange",
                },
                CREATE: {
                    METHOD: "processContentOrPathChange"
                },
                DROP: {
                    METHOD: "processContentOrPathChange"
                }
            },
            JOB_DEFINITION: {
                RENAME: {
                    METHOD: "processContentOrPathChange"
                },
                CREATE: {
                    METHOD: "processContentOrPathChange"
                },
                DROP: {
                    METHOD: "processContentOrPathChange"
                },
                TYPECHANGE: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_CONTENT",
                            NAME: "NAME",
                            TYPENAME: "TYPE"
                        }
                    ]
                }
            },
            NR: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "NAME"
                        },
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "MANAGER_NAME"
                        }
                    ]
                }
            },
            RESOURCE: {
                CREATE: {
                    METHOD: "processResourceChange"
                },
                UPDATE: {
                    METHOD: "processResourceChange"
                },
                DROP: {
                    METHOD: "processResourceChange"
                }
            },
            RSD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "STATE"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}
