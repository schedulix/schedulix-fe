import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import scope_editorform from '../json/editorforms/scope.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { EditorformOptions } from "../data-structures/editorform-options";
import { ToolboxService } from "../services/toolbox.service";
import { NewItemDialogComponent } from "../modules/main/components/dialog-components/new-item-dialog/new-item-dialog.component";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";
import { SdmsClipboardMode, SdmsClipboardType } from "../modules/main/services/clipboard.service";
import { OutputType, SnackbarType } from "../modules/global-shared/components/snackbar/snackbar-data";

import editFolderParameterComment_editorform from '../json/editorforms/editFolderParameterComment_editorForm.json';

export class ScopeCrud extends Crud {

    new(obj: any): Promise<Object> {
        const dialogRef = this.dialog.open(NewItemDialogComponent, {
            data: {
                title: "new_item",
                translate: "create_new_item_chooser",
                itemTypes: ['SCOPE', 'SERVER'],
                path: obj.PATH ? obj.PATH + '.' + obj.NAME : obj.NAME
            },
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
        });
        dialogRef.afterClosed().subscribe((result: any) => {
            if (result !== undefined && result) {
                let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
                tab.PATH = result.path;
                // console.log(result)
                this.eventEmitterService.createTab(tab, true);
            }
        });
        return Promise.resolve({});
    }

    createWith(mode: string, obj: any, tabInfo: Tab) {
        let isAdmin: boolean = this.privilegesService.isAdmin();
        let withClause: string = "";
        let sep = " with ";
        if (obj.OWNER != obj.ORIGINAL_OWNER || tabInfo?.MODE == TabMode.NEW) {
            withClause += sep + "group = '" + obj.OWNER + "'";
            if (obj.CASCADE_SET_GROUP == "true") {
                withClause += " " + "cascade";
            }
            sep = ", "
        }
        if (this.privilegesService.isEdition('BASIC')) {
            if (obj.INHERIT_GRANTS == 'ALL') {
                withClause += sep + 'inherit grant = ' + '(DROP, EDIT, EXECUTE, RESOURCE, VIEW)';
            } else if (obj.INHERIT_GRANTS == 'VIEW') {
                withClause += sep + 'inherit grant = ' + '(VIEW)';
            } else {
                withClause += sep + 'inherit grant = ' + 'NONE';
            }
            sep = ", "
        }
        if (tabInfo.TYPE == "SERVER") {
            if ((obj.PASSWORD1 && obj.PASSWORD1 != '') || tabInfo.MODE == TabMode.NEW) {
                withClause += sep + "password = '" + obj.PASSWORD1 + "'"
                sep = ", "
            }
            if (obj.IS_ENABLED == "true") {
                withClause += sep + "enable"
            }
            else {
                withClause += sep + "disable"
            }
            withClause += ", node = '" + obj.NODE + "'";
            sep = ", "
        }
        if (tabInfo.MODE == TabMode.NEW) {
            return withClause;
        }
        withClause += sep + "parameters =";
        let pSep = "(";
        for (let param of obj.PARAMETERS.TABLE) {
            if (param.DEFINITION != (tabInfo.PATH ? tabInfo.PATH + '.' + tabInfo.NAME : tabInfo.NAME) || param.TYPE == "DYNAMICVALUE") {
                continue;
            }
            withClause += pSep + "'" + this.convertIdentifier(param.NAME, param.ORIGINAL_NAME) + "' = "
            if (param.TYPE != "DYNAMIC") {
                withClause += "'" + this.toolbox.textQuote(param.DEFAULT_VALUE) + "'"
            }
            else {
                withClause += "dynamic"
            }
            pSep = ", ";
        }
        if (pSep != "(") {
            withClause += ")";
        }
        else {
            withClause += " none";
        }
        sep = ", "
        withClause += sep + "config = (";
        let cSep = "";
        for (let config of obj.CONFIG.TABLE) {
            if (config.INHERIT == "false") {
                if (!(["KEYSTOREPASSWORD", "TRUSTSTOREPASSWORD"].includes(config.KEY)) || config.value != "") {
                    withClause += cSep + "'" + config.KEY + "' = '" + config.VALUE.replace(/\\/g, "\\\\").replace(/'/g, "\\'") + "'";
                }
            }
            else {
                withClause += cSep + "'" + config.KEY + "' = NONE";
            }
            cSep = ", ";
        }
        for (let namePattern of obj.LOGFILE_PATTERNS.TABLE) {
            if (namePattern.INHERIT == "false") {
                withClause += cSep + "'NAME_PATTERN_" + namePattern.KEY + "' = '" + namePattern.VALUE.replace(/\\/g, "\\\\").replace(/'/g, "\\'") + "'";
                obj.LOCAL_NAME_PATTERNS = obj.LOCAL_NAME_PATTERNS.filter(function(item:any) { return item !== namePattern.KEY; } );
            }
        }
        for (let key of obj.LOCAL_NAME_PATTERNS) {
            withClause += cSep + "'NAME_PATTERN_" + key + "' = NONE";
        }
        withClause += cSep + "env = "
        let eSep = "(";
        for (let env of obj.CONFIG_ENVMAPPING.TABLE) {
            if (env.INHERIT == "false") {
                withClause += eSep + "'" + env.KEY + "' = '" + env.VALUE + "'"
                eSep = ", ";
            }
        }
        if (eSep == "(") {
            withClause += "NONE";
        }
        else {
            withClause += ")";
        }
        withClause += ")";

        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let type = "scope";
        if (tabInfo.TYPE == "SERVER") {
            type = "job server";
        }
        let stmt = "create " + type + " " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + obj.NAME) + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "SCOPE", OP : "CREATE", NAME : tabInfo.PATH + "." + obj.NAME});
            return response;
        });
    }

    clone(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts: string[] = [];

        stmts.push ("copy scope "
            + obj.QUOTED_FULLNAME
            + " to "
            + "'" + obj.NAME + "'");
        stmts.push("alter scope " + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME) +  this.createWith("UPDATE", obj, tabInfo));

        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            tabInfo.NAME = obj.NAME;
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "SCOPE", OP : "CREATE", NAME : tabInfo.PATH + "." + obj.NAME});
            return response;
        });
    }

    async showSystem() {
        let stmt: string = "show system";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    showContent(quotedScopeName: string): Promise<Object> {
        let stmt: string = "list scope " + quotedScopeName;
        return this.commandApi.execute(stmt).then((response: any) => {
            response.DATA.TABLE.shift() // remove first item (itself)
            let cutIds = this.clipboardService.getCutIds();
            for (let row of response.DATA.TABLE) {
                if (cutIds.includes(row.ID)) {
                    row.fe_flagged_to_cut = true;
                }
            }
            return response.DATA;
        });
    }

    filterResources(resources : any[], path : string) : any[] {

        // filter out managed resources of pools
        let found : boolean = false;

        let filteredResources: any[] = [];
        for (let resource of resources) {
            if (resource.USAGE == 'CATEGORY') {
                resource.NOFORM_BULK_DISABLED = true;
            }
            resource.fe_flagged_to_cut = false;
            resource.DISPLAY_SCOPE = resource.SCOPE;
            if (path == resource.SCOPE) {
                resource.DISPLAY_SCOPE = '';
            }
            if (resource.POOL_CHILD == "false") {
                if (["SYNCHRONIZING","SYSTEM","POOL"].includes(resource.USAGE) &&
                  resource.AMOUNT != "INFINITE" && resource.AMOUNT && resource.AMOUNT != "") {
                    let amount = parseFloat(resource.AMOUNT);
                    if (amount != 0) {
                        resource.LOAD = (amount - parseFloat(resource.FREE_AMOUNT)) * 100 / amount;
                    }
                }
                if (resource.USAGE != 'CATEGORY') {
                    found = true;
                }
                filteredResources.push(resource);
            }
        }
        if (!found) {
            filteredResources = [];
        }
        return filteredResources;
    }

    reloadResources(bicsuiteObject: any, tabInfo: Tab) {

        let stmt = "show scope " + this.getQuotedFullName(tabInfo) + " with expand = all";
        return this.commandApi.execute(stmt).then((response: any) => {
            let record = response.DATA.RECORD;
            bicsuiteObject.NOFORM_RESOURCES.TABLE = this.filterResources(
                record.RESOURCES.TABLE, (record.PATH ? record.PATH + '.' + record.NAME : record.NAME));
            this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        });
    }

    async read(obj: any, tabInfo: Tab): Promise<Object> {

        let exportVariables: any[] = [];
        await this.showSystem().then((response: any) => {
            for (let variable of response.response.DATA.RECORD.ExportVariables.split(',')) {
                exportVariables.push( { "NAME" : variable.trim() } );
            }
        });

        let stmt: string = "show scope " + this.getQuotedFullName(tabInfo) + " with expand = all";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            let record = response.DATA.RECORD;
            tabInfo.ID = record.ID;

            record.FULLNAME = this.toolbox.namePathQuote(record.NAME);
            let pathlist = record.NAME.split(".");
            record.NAME = pathlist.pop();
            record.PATH = pathlist.join(".");
            record.QUOTED_FULLNAME = this.getQuotedFullName(tabInfo);

            record.ORIGINAL_OWNER = record.OWNER;

            let filteredConfig: any[] = [];
            let logfilePatterns: any[] = [];
            let localNamePatterns: any[] = [];
            // we need to negate the output here. If LOCAL == true then INHERITED must be false
            for (let item of record.CONFIG.TABLE) {
                if (item.LOCAL === "true" || item.LOCAL === true) {
                    item.INHERIT = "false";
                }
                else {
                    item.INHERIT = "true";
                }
                if (item.KEY.startsWith("NAME_PATTERN_")) {
                    item.KEY = item.KEY.substring("NAME_PATTERN_".length);
                    if (item.INHERIT == "false") {
                        localNamePatterns.push(item.KEY);
                    }
                    logfilePatterns.push(item);
                }else {
                    filteredConfig.push(item);
                }
            }
            record.LOCAL_NAME_PATTERNS = localNamePatterns;
            record.CONFIG.TABLE = filteredConfig;
            record.LOGFILE_PATTERNS = { 'TABLE' : logfilePatterns };
            for (let item of record.CONFIG_ENVMAPPING.TABLE) {
                if (item.LOCAL === "true" || item.LOCAL === true) {
                    item.INHERIT = "false";
                }
                else {
                    item.INHERIT = "true";
                }
                item.EXPORT_VARIABLES = exportVariables;
            }
            record.EXPORT_VARIABLES = exportVariables;

            for (let parameter of record.PARAMETERS.TABLE) {
                if (!parameter.DEFINITION_TYPE) {
                    parameter.DEFINITION_TYPE = "SCOPE";
                }
                parameter.DYNAMIC_VALUE = "<to be set by job server>";
                parameter.ORIGINAL_NAME = parameter.NAME;
                if (parameter.TYPE == "DYNAMIC") {
                    parameter.DYNAMIC_VALUE = parameter.DEFAULT_VALUE;
                }
                parameter.PARENTNAME = record.PATH + '.' + record.NAME;
                if (parameter.COMMENT) {
                    parameter.COMMENT = parameter.COMMENT.trim();
                }
                parameter.NOFORM_COMMENT = parameter.COMMENT;
                if (!this.conditionParameterDefinedHere({}, parameter, tabInfo)) {
                    parameter.NOFORM_BULK_DISABLED = true;
                }
            }
            this.sortParameters(record);

            for (let item of record.PARAMETERS.TABLE) {
                item.DYNAMIC_VALUE = "<to be set by job server>";
                if (item.TYPE == "DYNAMIC") {
                    item.DYNAMIC_VALUE = item.DEFAULT_VALUE;
                }
                item.ORIGINAL_NAME = item.NAME;
            }
            // map inherit grants for basic
            this.setInheritGrants(record);
            // if (this.privilegesService.isEdition('BASIC')) {
            //     let inherit_grants = record.INHERIT_PRIVS;
            //     if (inherit_grants == '') {
            //         record.INHERIT_GRANTS = 'NONE'
            //     } else if (inherit_grants == 'V') {
            //         record.INHERIT_GRANTS = 'VIEW'
            //     } else {
            //         record.INHERIT_GRANTS = 'ALL'
            //     }
            // }

            record.NOFORM_RESOURCES = {}
            record.NOFORM_RESOURCES.TABLE = this.filterResources(
                record.RESOURCES.TABLE, (record.PATH ? record.PATH + '.' + record.NAME : record.NAME));

            for (let parameter of record.PARAMETERS.TABLE) {
                parameter.ORIGINAL_NAME = parameter.NAME;
            }

            if (tabInfo.TYPE == 'SCOPE') {
                return this.showContent(record.QUOTED_FULLNAME).then(content => {
                    record.NOFORM_CONTENT = content; // from showContent
                    return promise;
                });
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts: string[] = [];
        let type = "scope";
        if (tabInfo.TYPE == "SERVER") {
            type = "job server";
        }
        if (obj.NAME != obj.ORIGINAL_NAME) {
            stmts.push("rename " + type + " " + obj.QUOTED_FULLNAME + " to '" + obj.NAME + "'");
        }
        let withClause = this.createWith("UPDATE", obj, tabInfo);
        if (withClause != "") {
            stmts.push("alter " + type + " " + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME) + " " + withClause);
        }
        for (let parameter of obj.PARAMETERS.TABLE) {
            if (parameter.NOFORM_comments) {
                let stmt = "create comment on parameter '" + parameter.NAME + "' of scope " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
                let withStmt = []
                for (let c of parameter.NOFORM_comments) {
                    c.TAG = c.TAG ? c.TAG : '';
                    let desc = c.COMMENT ? this.toolbox.textQuote(c.COMMENT) : '';
                    withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'");
                }
                stmt += withStmt.join(',');
                stmts.push(stmt);
            }
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            if (obj.ORIGINAL_NAME != obj.NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "SCOPE", OP : "RENAME", OLD_NAME : tabInfo.PATH + "." + tabInfo.NAME, NEW_NAME : tabInfo.PATH + "." + obj.NAME});
                tabInfo.NAME = obj.NAME;
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {

        return this.createConfirmDialog([
          {
            'NAME': 'DROP_CASCADED',
            'TRANSLATE': 'cascade',
            'TYPE': 'CHECKBOX',
            'USE_LABEL': true
          }
        ], "drop_confirm{" + obj.TYPE + "}{" + obj.ORIGINAL_NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop scope " + obj.QUOTED_FULLNAME;
                if (result.DROP_CASCADED == "true") {
                  stmt += " cascade"
                }
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "SCOPE", OP : "DROP", NAME : tabInfo.PATH + "." + tabInfo.NAME});
                    return response;
                });
            }
            return {};
        });
        // let stmt = "drop scope " + obj.QUOTED_FULLNAME;
        // // has to be removed from form later after confirm dialog is implemented to ask fo cascade
        // if (obj.DROP_CASCADED) {
        //   stmt += " cascade"
        // }
        // return this.commandApi.execute(stmt).then((response: any) => {
        //     this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "SCOPE", OP : "DROP", NAME : tabInfo.PATH + "." + tabInfo.NAME});
        //     return response;
        // });
    }

    editorform() {
        return scope_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.FULLNAME = this.toolbox.namePathQuote(tabInfo.PATH + '.');
        record.NAME = '';
        record.ORIGINAL_NAME = '';
        record.PATH = tabInfo.PATH;
        record.OWNER = this.privilegesService.getDefaultGroup();
        record.INHERIT_GRANTS = "ALL";
        return bicsuiteObject;
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)
        // console.log(bicsuiteObject)

        let grantObject = {
            TYPE: bicsuiteObject.TYPE,
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME,
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS:"",
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;
                if (grantObject.GRANTS.TABLE[0]) {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    // grantObject.GRANTS.TABLE[0].OWNER = 'Inherit grant form parent';

                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        // console.log(i)
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            // console.log("merge inheritance pirvs to the root object")
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }

                // console.log(grantObject);

            }
            let editorForm
            if (grantObject.TYPE == 'SERVER') {
                // if server
                editorForm = crud.editorformByType(grantObject.TYPE, false, true)
            } else {
                // if scope
                editorForm = crud.editorformByType(grantObject.TYPE, true, true)
            }
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for " + bicsuiteObject.TYPE + " " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }


    openResourceItemFromTree(bicsuiteObj: any, tabInfo: any, parentBicsuiteObject: any, node: any, field?: any): Promise<any> {
        let nl = node.data.NAME.split('.');
        let name = nl.pop();
        let path = nl.join('.');
        if (node.data.USAGE == "CATEGORY") {
            let categoryTab = new Tab(name, node.data.ID, 'CATEGORY', "category", TabMode.EDIT);

            categoryTab.PATH = path;
            this.eventEmitterService.createTab(categoryTab, true);
            return Promise.resolve({});
        }
        else {
            let resourceTab = new Tab(name, node.data.ID, 'RESOURCE'.toUpperCase(), node.data.USAGE.toLowerCase(), TabMode.EDIT);
            resourceTab.DATA = {};
            // console.log(bicsuiteObj)

            let owner = parentBicsuiteObject.NAME;
            if (parentBicsuiteObject.PATH) {
                owner = parentBicsuiteObject.PATH + '.' + owner;
            }

            resourceTab.OWNER = owner;
            resourceTab.PATH = path;
            // mandatory for resource instance
            resourceTab.DATA.USAGE = node.data.USAGE; // only used by pools
            resourceTab.DATA.RESOURCE_ID = node.data.ID;
            resourceTab.DATA.RESOURCE_NAME = node.data.NAME;
            resourceTab.DATA.RESOURCE_OWNER_TYPE = tabInfo.TYPE;
            resourceTab.DATA.f_isScopeOrFolderInstance = true;
            // tabInfo.DATA.NOFORM_TIME_ZONE

            this.eventEmitterService.createTab(resourceTab, true);
        }
        return Promise.resolve({});
    };

    openPoolFromTree(bicsuiteObject: any, tabInfo?: Tab) {
        // console.log(bicsuiteObject)
        let definedResourceTab = new Tab(bicsuiteObject.MANAGER_NAME.split('.').pop(), "", 'RESOURCE', "pool", TabMode.EDIT);
        definedResourceTab.DATA = {
            USAGE : "POOL",
            RESOURCE_NAME : bicsuiteObject.MANAGER_NAME,
            f_isScopeOrFolderInstance : true
        };
        definedResourceTab.OWNER = bicsuiteObject.MANAGER_SCOPENAME;
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    };

    openResourceState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition(bicsuiteObject.STATE);
    }

    openResourceScope(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openScope(bicsuiteObject.SCOPE, "scope");
    }

    openLinkScope(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openScope(bicsuiteObject.LINK_SCOPE, bicsuiteObject.LINK_SCOPE_TYPE.toLowerCase());
    }

    openManagerScope(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openScope(bicsuiteObject.MANAGER_SCOPENAME, bicsuiteObject.MANAGER_SCOPE_TYPE.toLowerCase());
    }

    openParameterScope(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openScope(bicsuiteObject.DEFINITION, bicsuiteObject.DEFINITION_TYPE.toLowerCase());
    }


    editParameterComment(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show comment on parameter '" + bicsuiteObject.NAME + "' of scope " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        return this.commandApi.execute(stmt).then((response1: any) => {
            for (let o of response1.DATA.TABLE) {
                o.DESCRIPTION = o.COMMENT
            }
            let dataObj: any = {
                COMMENT: {
                    TABLE: response1.DATA.TABLE
                },
                NAME: bicsuiteObject.NAME
            };
            if (bicsuiteObject.PRIVS) {
                dataObj.PRIVS = bicsuiteObject.PRIVS;
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editFolderParameterComment_editorform, dataObj, tabInfo, "Edit Parameter Comments", this),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                // Update Original Object
                let visibleComment = []
                for (let c of result.COMMENT.TABLE) {
                    visibleComment.push(c.TAG + "\n\n" + c.DESCRIPTION)
                }
                bicsuiteObject.NOFORM_COMMENT = visibleComment.join('\n\n\n')
                return result;
            });
        });
    }

    saveParameterComments(bicsuiteObject: any, tabInfo: Tab, deepCopyBicsuiteObject: any): Promise<Object> {
        let stmt;
        if (deepCopyBicsuiteObject.COMMENT.TABLE.length > 0) {
            stmt = "create or alter comment on parameter '" + bicsuiteObject.NAME + "' of scope " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
            let withStmt = []
            for (let c of deepCopyBicsuiteObject.COMMENT.TABLE) {
                c.TAG = c.TAG ? c.TAG : '';
                let desc = c.DESCRIPTION ? this.toolbox.textQuote(c.DESCRIPTION) : '';
                withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'")
            }
            stmt += withStmt.join(',')
        }
        else {
            stmt = "drop comment on parameter '" + bicsuiteObject.NAME + "' of scope " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            if (response.hasOwnProperty("FEEDBACK")) {
                this.eventEmitterService.pushSnackBarMessage([response.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);
            }
            return response;
        });
    }

    openContentListItem(bicsuiteObject: any, tabInfo: any, parentBicsuiteObject: any, node: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.NAME, bicsuiteObject.TYPE.toLowerCase());
    };

    doSuspendScope(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let stmt = "suspend " + dialogData.bicsuiteObject.QUOTED_FULLNAME;
        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    doResumeScope(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let stmt = "resume " + dialogData.bicsuiteObject.QUOTED_FULLNAME;
        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    doShutdownScope(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let stmt = "shutdown " + dialogData.bicsuiteObject.QUOTED_FULLNAME;
        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    doDeregisterServer(dialogData: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        let stmt = "deregister " + dialogData.bicsuiteObject.QUOTED_FULLNAME;
        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    createSuspendResumeShutdownConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string, operation: string) {
        let fields: any[] = [];
        let message = "";
        if (tabInfo.TYPE == "SCOPE") {
            message = operation + "_confirm_scope{" + (bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.NAME : bicsuiteObject.ORIGINAL_NAME) + "}";
        }
        else {
            message = operation + "_confirm_server{" + (bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.NAME : bicsuiteObject.ORIGINAL_NAME) + "}";
        }
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject);
    }

    createDeregisterServerConfirmDialog(bicsuiteObject: any, tabInfo: Tab, batch: boolean, method: string) {
        let fields: any[] = [];
        let message = "";
        message = "deregister_confirm_server{" + (bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.NAME : bicsuiteObject.ORIGINAL_NAME) + "}";
        return this.createConfirmDialog(fields, message, tabInfo, method, bicsuiteObject);
    }

    suspendScope(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createSuspendResumeShutdownConfirmDialog(bicsuiteObject, tabInfo, false, "doSuspendScope", "suspend");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
            }
            return 1;
        });
    }

    resumeScope(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createSuspendResumeShutdownConfirmDialog(bicsuiteObject, tabInfo, false, "doResumeScope", "resume");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
            }
            return 1;
        });
    }

    suspendServer(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createSuspendResumeShutdownConfirmDialog(bicsuiteObject, tabInfo, false, "doSuspendScope", "suspend");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
            }
            return 1;
        });
    }

    resumeServer(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createSuspendResumeShutdownConfirmDialog(bicsuiteObject, tabInfo, false, "doResumeScope", "resume");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
            }
            return 1;
        });
    }

    copyContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.FROM_ITEM_TYPE);
        for(let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = false;
        }
       return Promise.resolve({});
    }

    cutContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.FROM_ITEM_TYPE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = true;
        }
        return Promise.resolve({});
    }

    scopePasteTypes: SdmsClipboardType[] = [SdmsClipboardType.SCOPE, SdmsClipboardType.SERVER];

    pasteContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        let itemsToBePasted: any[] = this.clipboardService.getItems(this.scopePasteTypes);
        let sourceObject: any = {};
        let sourceTabInfo: Tab;
        let stmts: string[] = [];
        let sourcePathes : any[] = [];

        for (let item of itemsToBePasted) {
            sourceObject = item.sourceObject;
            sourceTabInfo = item.sourceTabInfo;
            if (item.mode == SdmsClipboardMode.COPY) {
                stmts.push("copy scope " + this.toolbox.namePathQuote(item.data.NAME) + " to " + this.toolbox.namePathQuote(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) + ';');
            }
            else {
                let p = item.data.NAME.split('.');
                p.pop();
                let sourcePath = p.join('.');
                if (sourcePath != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) {
                    stmts.push("move scope " + this.toolbox.namePathQuote(item.data.NAME) + " to " + this.toolbox.namePathQuote(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME) + ';');
                    if (!sourcePathes.includes(sourcePath)) {
                        sourcePathes.push(sourcePath);
                    }
                }
                else {
                    if (item.mode == SdmsClipboardMode.CUT) {
                        if (!sourcePathes.includes(sourcePath)) {
                            sourcePathes.push(sourcePath);
                        }
                        item.mode = SdmsClipboardMode.COPY;
                        item.data.fe_flagged_to_cut = false;
                    }
                    // No deep copy necessary here because we re read content later anyway, so no cross editing can occure
                }
                if (sourcePath != bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME || tabInfo.FROM_CONTEXT_MENU) {
                    // put target path in sourcepathes to get target refreshed if paste is done from tree context menu
                    if (!sourcePathes.includes(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME)) {
                        sourcePathes.push(bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME);
                    }
                }
            }
        }
        if (stmts.length > 0) {
            return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                for (let item of itemsToBePasted) {
                    item.mode = SdmsClipboardMode.COPY;
                }
                for (let sourcePath of sourcePathes) {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "SCOPE", OP : "CREATE", NAME : sourcePath + ".DUMMY"});
                }
                return this.showContent(bicsuiteObject.QUOTED_FULLNAME).then(content => {
                    bicsuiteObject.NOFORM_CONTENT = content;
                    if (itemsToBePasted.length > 1) {
                        this.eventEmitterService.pushSnackBarMessage([itemsToBePasted.length + ' items have been inserted'], SnackbarType.INFO);
                    } else {
                        this.eventEmitterService.pushSnackBarMessage(['Item has been inserted'], SnackbarType.INFO);
                    }
                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    this.eventEmitterService.refreshNavigationEmittor.next();
                    return {};
                });
            });
        }
        return Promise.resolve({});
    }

    dropContent(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let fields: any[] = [{
            'NAME': 'CASCADE',
            'TRANSLATE': 'cascade',
            'TYPE': 'CHECKBOX',
            "SYS_OBJ_EDITABLE": true,
            'USE_LABEL': true
        }];

        return this.createConfirmDialog(fields, "drop_confirm_selected", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmts: string[] = [];
                for (let selected of checklistSelection._selected) {
                    let stmt = "drop scope " + this.toolbox.namePathQuote(selected.NAME);
                    if (result.CASCADE && result.CASCADE == "true") {
                        stmt += ' cascade';
                    }
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                    return this.showContent(bicsuiteObject.QUOTED_FULLNAME).then(content => {
                        bicsuiteObject.NOFORM_CONTENT = content;
                        for (let selected of checklistSelection._selected) {
                           this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "SCOPE", OP: "DROP", NAME: selected.NAME, FROM_CONTENT: true });
                        }
                        this.eventEmitterService.refreshNavigationEmittor.next();
                        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                    });
                });
            }
            return {};
        });
    }

    copyResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.RESOURCE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = false;
        }
        return Promise.resolve({});
    }

    cutResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.RESOURCE);
        for (let selected of checklistSelection.selected) {
            selected.fe_flagged_to_cut = true;
        }
        return Promise.resolve({});
    }

    resourcesPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.RESOURCE, SdmsClipboardType.SYSTEM, SdmsClipboardType.SYNCHRONIZING, SdmsClipboardType.STATIC]

    conditionClipBoardOfTypeResources(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        // check if there is item to be pasted and if the source is not the same
        return this.clipboardService.checkItemTypesPresent(this.resourcesPasteTypes, bicsuiteObject.ID);
    }

    dropResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {

        return this.createConfirmDialog([], "drop_confirm_selected", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmts: string[] = [];
                for (let selected of checklistSelection._selected) {
                    let stmt = "drop resource " + this.toolbox.namePathQuote(selected.NAME) + " in " + this.toolbox.namePathQuote(selected.SCOPE);
                    stmts.push(stmt);
                }
                return this.commandApi.executeMulticommand(stmts).then((response: any) => {

                    let dropRowIndexes = [];
                    for (let selected of checklistSelection._selected) {
                        this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "DROP", NAME: selected.NAME, OWNER: selected.SCOPE })
                        dropRowIndexes.push(selected.rowIndex);
                    }
                    let newDefinedResources = [];
                    for (let definedResource of bicsuiteObject.NOFORM_RESOURCES.TABLE) {
                        if (dropRowIndexes.includes(definedResource.rowIndex)) {
                            continue;
                        }
                        newDefinedResources.push(definedResource);
                    }
                    bicsuiteObject.NOFORM_RESOURCES.TABLE = newDefinedResources;

                    this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
                });
            }
            return {};
        });
    }

    pasteResources(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        let stmts: string[] = [];
        let droppedResources : any[] =[];
        let resourceCreated : boolean = false;
        for (let item of this.clipboardService.getItems(this.resourcesPasteTypes)) {
            let stmt = "";

            let c : any = {};
            switch (item.itemType) {
                case SdmsClipboardType.RESOURCE:
                    if (item.data.SCOPE == tabInfo.PATH + '.' + tabInfo.NAME && item.mode == SdmsClipboardMode.CUT) {
                            item.mode = SdmsClipboardMode.COPY;
                            item.data.fe_flagged_to_cut = false;
                    }
                    else {
                        if (item.mode == SdmsClipboardMode.CUT) {
                            stmt = "drop resource " + this.toolbox.namePathQuote(item.data.NAME) + " in " + this.toolbox.namePathQuote(item.data.SCOPE);
                            stmts.push(stmt);
                            droppedResources.push(item);
                        }
                        stmts.push(this.genCreateResource(item, tabInfo));
                    }
                    break;
                case SdmsClipboardType.SYSTEM:
                case SdmsClipboardType.SYNCHRONIZING:
                case SdmsClipboardType.STATIC:
                    let defaultAmount = "1";
                    if (item.itemType ==  SdmsClipboardType.SYNCHRONIZING) {
                        defaultAmount = "INFINITE";
                    }
                    let c = {
                        data : {
                            NAME : item.data.NAME,
                            AMOUNT : defaultAmount,
                            REQUESTABLE_AMOUNT : defaultAmount,
                            IS_ONLINE : "true",
                            USAGE : item.data.USAGE
                        }
                    }
                    stmts.push(this.genCreateResource(c, tabInfo));
                    resourceCreated = true;
                    break;
            }
        }
        if (stmts.length > 0) {
            return this.commandApi.executeMulticommand(stmts).then((response: any) => {
                for (let dropped of droppedResources) {
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "DROP", NAME: dropped.data.NAME, OWNER: dropped.data.SCOPE })
                    dropped.mode = SdmsClipboardMode.COPY;
                    dropped.sourceId = "";
                }
                if (resourceCreated) {
                    // just send only one dummy message to reload only once on affected scopes and servers
                    this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: "RESOURCE", OP: "CREATE", NAME: 'DUMMY',
                        OWNER: (bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.ORIGINAL_NAME : bicsuiteObject.ORIGINAL_NAME) })
                }
                return Promise.resolve(false);
            });
        }
        else {
            return Promise.resolve(false);
        }
    }

    private genCreateResource( item: any, tabInfo: Tab) {
        let stmt = "create resource " +
            this.toolbox.namePathQuote(item.data.NAME) + " in " +
            this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) +
            " with " +
            " " + (item.data.IS_ONLINE == "true" ? "online" : "offline");
        if (item.data.STATE) {
            stmt += ", status = '" + item.data.STATE + "'";
        }
        if (item.data.USAGE != 'STATIC') {
            stmt += ", amount = " + item.data.AMOUNT + ", requestable_amount = " + item.data.REQUESTABLE_AMOUNT;
        }
        return stmt;
    }

    getSelectedParameterComments(bicsuiteObject: any, tabInfo: Tab, selectedParameter : any) : Promise<Object> {
        let stmt : string = "show comment on parameter '" + selectedParameter.ORIGINAL_NAME + "' of scope " +
            this.toolbox.namePathQuote(bicsuiteObject.PATH + '.' + bicsuiteObject.NAME);
        return this.commandApi.execute(stmt).then((response: any) => {
            selectedParameter.NOFORM_comments = response.DATA.TABLE;
            return response;
        });
    }

    getSelectedParametersComments(bicsuiteObject: any, tabInfo: Tab, selectedParameters : any[]) : Promise<Object> {
        let promises: Promise<any>[] = [];
        for (let selectedParameter of selectedParameters) {
            if (selectedParameter.COMMENT) {
                promises.push(this.getSelectedParameterComments(bicsuiteObject, tabInfo, selectedParameter));
            }
        }
        return Promise.all(promises).then((res) => {
            return {};
        });
    }

    sortParameters(bicsuiteObject : any) {
        bicsuiteObject.PARAMETERS.TABLE.sort(function(a : any, b : any) {
            if (a.PARENTNAME == a.DEFINITION && b.PARENTNAME != b.DEFINITION) {
                return -1;
            }
            if (a.PARENTNAME != a.DEFINITION && b.PARENTNAME == b.DEFINITION) {
                return 1;
            }
            return a.NAME < b.NAME ? -1 : 1;
        });
    }

    copyParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.SCOPE_PARAMETER);
            return {};
        });
    }

    cutParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.SCOPE_PARAMETER);
            return this.dropParameters(bicsuiteObject, tabInfo, form, checklistSelection);
        });
    }

    dropParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newParameters = [];
        for (let parameter of bicsuiteObject.PARAMETERS.TABLE) {
            if (dropRowIndexes.includes(parameter.rowIndex)) {
                continue;
            }
            newParameters.push(parameter);
        }
        bicsuiteObject.PARAMETERS.TABLE = newParameters;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    parameterPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.SCOPE_PARAMETER];

    conditionPasteParameters(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.parameterPasteTypes, tabInfo.ID);
    }

    pasteParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.parameterPasteTypes)) {
            // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
            let p = JSON.parse(JSON.stringify(item.data));
            let parentName = bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.NAME : bicsuiteObject.NAME;
            if (p.PARENTNAME != parentName) {
                p.ID = undefined; // this is  a new parameter!!!
            }
            item.mode = SdmsClipboardMode.COPY;
            p.DEFINITION = parentName;
            bicsuiteObject.PARAMETERS.TABLE.push(p);
        }
        this.sortParameters(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    conditionParameterDefinedHere(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let name = (tabInfo.PATH ? tabInfo.PATH + '.' + tabInfo.NAME : tabInfo.NAME);
        if (bicsuiteObject.DEFINITION == name) {
            return true;
        }
        else {
            return false;
        }
    }

    shutdownScope(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createSuspendResumeShutdownConfirmDialog(bicsuiteObject, tabInfo, false, "doShutdownScope", "shutdown");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
            }
            return 1;
        });

    }

    deregisterServer(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let confirmDialog = this.createDeregisterServerConfirmDialog(bicsuiteObject, tabInfo, false, "doDeregisterServer");
        return confirmDialog.afterClosed().toPromise().then((result: any) => {
            // reload only if sth was returned (not closed or ESC)
            if (result) {
                this.reload(bicsuiteObject, tabInfo)
            }
            return 1;
        });

    }

    createResource(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        // console.log(bicsuiteObject)
        let definedResourceTab = new Tab('*untitled', new Date().getTime().toString(), 'RESOURCE'.toUpperCase(), 'resource', TabMode.NEW);
        definedResourceTab.DATA = {};
        definedResourceTab.DATA.TYPE = bicsuiteObject.TYPE;
        definedResourceTab.DATA.ID = bicsuiteObject.ID;
        definedResourceTab.DATA.RESOURCE_NAME = bicsuiteObject.NAME;
        definedResourceTab.DATA.RESOURCE_OWNER_TYPE = bicsuiteObject.TYPE;
        definedResourceTab.DATA.f_isScopeOrFolderInstance = true;
        definedResourceTab.OWNER = bicsuiteObject.PATH + '.' + bicsuiteObject.ORIGINAL_NAME;
        this.eventEmitterService.createTab(definedResourceTab, true);
        return Promise.resolve({});
    }

    AppendEnvMap(bicsuiteObject: any, tabInfo: Tab, row: any) : Promise<Object> {
        row.EXPORT_VARIABLES = bicsuiteObject.EXPORT_VARIABLES;
        row.VALUE = row.EXPORT_VARIABLES[0].NAME;
        row.KEY = "";
        row.INHERIT = "false";
        return Promise.resolve({});
    }

    AppendLogfilePattern(bicsuiteObject: any, tabInfo: Tab, row: any) : Promise<Object> {
        row.KEY = "";
        row.VALUE = "";
        row.INHERIT = "false";
        return Promise.resolve({});
    }

    copyConfigAncestorValueToValue(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.INHERIT == "true") {
            bicsuiteObject.VALUE = bicsuiteObject.ANCESTOR_VALUE;
            // rootForm.controls.CONFIG.controls.VALUE.controls[bicsuiteObject.rowIndex].setValue(bicsuiteObject.VALUE);
            rootForm.get('CONFIG').get('VALUE').get(''+bicsuiteObject.rowIndex).setValue(bicsuiteObject.VALUE);
        }
        return Promise.resolve({});
    }

    copyDynamicValueToValue(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.TYPE == "DYNAMIC") {
            bicsuiteObject.DEFAULT_VALUE = bicsuiteObject.DYNAMIC_VALUE;
            rootForm.get('PARAMETERS').get('DEFAULT_VALUE').get(''+bicsuiteObject.rowIndex).setValue(bicsuiteObject.DEFAULT_VALUE);
        }
        else {
            rootForm.get('PARAMETERS').get('DEFAULT_VALUE').get(''+bicsuiteObject.rowIndex).setValue("");
        }
        return Promise.resolve({});
    }

    copyLogfileAncestorValueToValue(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.INHERIT == "true") {
            bicsuiteObject.VALUE = bicsuiteObject.ANCESTOR_VALUE;
            // rootForm.controls.CONFIG.controls.VALUE.controls[bicsuiteObject.rowIndex].setValue(bicsuiteObject.VALUE);
            rootForm.get('LOGFILE_PATTERNS').get('VALUE').get(''+bicsuiteObject.rowIndex).setValue(bicsuiteObject.VALUE);
        }
        return Promise.resolve({});
    }

    copyEnvAncestorValueToValue(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any, rootForm: any): Promise<Object> {
        if (bicsuiteObject.INHERIT == "true") {
            bicsuiteObject.VALUE = bicsuiteObject.ANCESTOR_VALUE;
            // rootForm.controls.CONFIG.controls.VALUE.controls[bicsuiteObject.rowIndex].setValue(bicsuiteObject.VALUE);
            rootForm.get('CONFIG_ENVMAPPING').get('VALUE').get(''+bicsuiteObject.rowIndex).setValue(bicsuiteObject.VALUE);
        }
        return Promise.resolve({});
    }


    appendParameter(bicsuiteObjec: any, tabInfo: Tab, row: any): Promise<Object> {
        row.DEFINITION = (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME);
        return Promise.resolve({});
    }

    conditionOwnerChanged(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.OWNER != bicsuiteObject.ORIGINAL_OWNER && !this.conditionIsNew(editorformField, bicsuiteObject, tabInfo);
    }

    conditionIsServer(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return tabInfo.TYPE == "SERVER";
    }

    conditionEnableSetGroupRecursive(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return !this.conditionIsServer(editorformField, bicsuiteObject, tabInfo) &&
               this.conditionOwnerChanged(editorformField, bicsuiteObject, tabInfo);
    }

    conditionIsInherit(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.INHERIT == "true") {
            return true;
        }
        return false;
    }

    conditionHasLoad(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.LOAD) {
            return true;
        }
        return false;
    }

    conditionIsStaticOrScope(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (!["SYSTEM","SYNCHRONIZING","POOL"].includes(bicsuiteObject.USAGE)) {
            return true;
        }
        return false;
    }

    conditionLogfilePatternNameEditable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.INHERIT != "true" && !(bicsuiteObject.ANCESTOR_SCOPE && bicsuiteObject.ANCESTOR_SCOPE != "")) {
            return true;
        }
        return false;
    }

    conditionEnvKeyEditable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.INHERIT != "true" && !(bicsuiteObject.ANCESTOR_SCOPE && bicsuiteObject.ANCESTOR_SCOPE != "")) {
            return true;
        }
        return false;
    }

    conditionHasAncestorScope(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.ANCESTOR_SCOPE && bicsuiteObject.ANCESTOR_SCOPE != "") {
            return true;
        }
        else {
            return false;
        }
    }

    conditionIsSuspendable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (tabInfo.TYPE == 'SCOPE' || bicsuiteObject.IS_SUSPENDED != "true") {
            return true;
        }
        else {
            return false;
        }
    }

    conditionIsResumable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (tabInfo.TYPE == 'SCOPE' || bicsuiteObject.IS_SUSPENDED == "true") {
            return true;
        }
        else {
            return false;
        }
    }

    conditionIsNewServer(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return tabInfo.TYPE == "SERVER" && this.conditionIsNew(editorformField, bicsuiteObject, tabInfo);
    }

    conditionIsOwnerOrAdmin(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (this.privilegesService.getGroups().includes(bicsuiteObject.OWNER) || this.privilegesService.isAdmin()) {
            return true;
        }
        return false;
    }

    conditionCanChangePassword(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.conditionIsServer(editorformField, bicsuiteObject, tabInfo) &&
               this.conditionIsOwnerOrAdmin(editorformField, bicsuiteObject, tabInfo) &&
               !this.conditionIsNew(editorformField, bicsuiteObject, tabInfo);
    }

    conditionCanShutdown(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return !this.conditionIsServer(editorformField, bicsuiteObject, tabInfo) ||
               (bicsuiteObject.IS_REGISTERED == "true" && bicsuiteObject.IS_TERMINATE == "false");
    }

    conditionCanDeregister(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.conditionIsServer(editorformField, bicsuiteObject, tabInfo) &&
               !this.conditionIsNew(editorformField, bicsuiteObject, tabInfo) &&
               bicsuiteObject.IS_REGISTERED == "true";
    }

    conditionParameterIsConstantAndNotFromParent(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE == "CONSTANT" && !this.conditionIsParentParameter(editorformField, bicsuiteObject, tabInfo);
    }

    conditionUsageIsCategory(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.USAGE == "CATEGORY";
    }

    conditionIsParentParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME) != bicsuiteObject.DEFINITION;
    }

    conditionIsParentParameterOrDynamicValue(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.conditionIsParentParameter(editorformField, bicsuiteObject, tabInfo) || bicsuiteObject.TYPE == "DYNAMICVALUE";
    }

    conditionClipBoardOfTypeNotEmpty(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
      // check if there is item to be pasted and if the source is not the same
      return this.clipboardService.checkItemTypesPresent(this.scopePasteTypes, bicsuiteObject.ID);
    }

    conditionIsNewOrParentParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.ID == undefined || (tabInfo.PATH == "" ? tabInfo.NAME : tabInfo.PATH + '.' + tabInfo.NAME) != bicsuiteObject.DEFINITION;
    }

    processContentOrPathChange(bicsuiteObject: any, tabInfo: Tab, initialBicsuiteObject: any, message: any): Promise<boolean> {
        let record = bicsuiteObject?.DATA?.RECORD;
        let affected: boolean = false;

        if (!record.hasOwnProperty("PATH")) {
            return Promise.resolve(false);
        }
        if (message.OP == "RENAME") {
            let oldName = message.OLD_NAME;
            let newName = message.NEW_NAME;
            if (tabInfo.PATH == oldName) {
                tabInfo.PATH = newName;
                tabInfo.updateTooltip();
            }
            else {
                if (tabInfo.PATH.startsWith(oldName + '.')) {
                    tabInfo.PATH = tabInfo.PATH.replace(oldName + '.', newName + '.');
                    tabInfo.updateTooltip();
                }
            }
            // Handle Path Change
            if (oldName && newName) {
                if (record.PATH == oldName) {
                    record.PATH = newName;
                }
                else {
                    if (record.PATH.startsWith(oldName + '.')) {
                        record.PATH = record.PATH.replace(oldName + '.', newName + '.');
                    }
                }
            }
        }
        if (message.OP == "DROP" || message.OP == "CREATE") {
            if (!message.FROM_CONTENT) {
                let path = message.NAME.split(".");
                path.pop()
                if ((tabInfo.PATH + '.' + tabInfo.NAME) == path.join('.')) {
                    affected = true;
                }
            }
        }
        // reload content if content changed
        if (affected && record.QUOTED_FULLNAME) {
            this.showContent(record.QUOTED_FULLNAME).then(content => {
                record.NOFORM_CONTENT = content;
            });
        }
        return Promise.resolve(false);
    }

    processResourceChange (bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.RESOURCES) {
            return Promise.resolve(false);
        }
        let owner = tabInfo.PATH + '.' + tabInfo.NAME;
        if ((owner + '.').startsWith(message.OWNER + '.')) {
            this.reloadResources(record, tabInfo).then (() => {
                return Promise.resolve(false);
            });
        }
        return Promise.resolve(false);
    }

    getChangeConfig() {
        return {
            CATEGORY: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "NAME"
                        },
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "MANAGER_NAME"
                        }
                    ]
                },
                TYPECHANGE: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_CONTENT",
                            NAME: "NAME",
                            TYPENAME: "TYPE"
                        }
                    ]
                }
            },
            GROUP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "OWNER"
                        }
                    ]
                }
            },
            NR: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "NAME"
                        },
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "MANAGER_NAME"
                        }
                    ]
                }
            },
            RESOURCE: {
                CREATE: {
                    METHOD: "processResourceChange"
                },
                UPDATE: {
                    METHOD: "processResourceChange"
                },
                DROP: {
                    METHOD: "processResourceChange"
                }
            },
            RSD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "STATE"
                        }
                    ]
                }
            },
            SCOPE: {
                RENAME: {
                    METHOD: "processContentOrPathChange",
                    FIELDS: [
                        {
                            TABLE: "CONFIG",
                            NAME: "ANCESTOR_SCOPE"
                        },
                        {
                            TABLE: "LOGFILE_PATTERNS",
                            NAME: "ANCESTOR_SCOPE"
                        },
                        {
                            TABLE: "CONFIG_ENVMAPPING",
                            NAME: "ANCESTOR_SCOPE"
                        },
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "SCOPE"
                        },
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "MANAGER_SCOPENAME"
                        },
                        {
                            TABLE: "NOFORM_RESOURCES",
                            NAME: "LINK_SCOPE"
                        },
                    ]
                },
                CREATE: {
                    METHOD: "processContentOrPathChange"
                },
                DROP: {
                    METHOD: "processContentOrPathChange"
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}

