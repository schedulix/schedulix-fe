import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import rsm_editorform from '../json/editorforms/rsm.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";

export class ResourceStateMappingCrud extends Crud {

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create resource state mapping " + "'" + obj.NAME + "'";
        let mappings = obj.MAPPINGS;
        // console.log(obj.RANGES)
        if (mappings != undefined && mappings != null) {
            stmt += " with map = (";
            for (let i = 0; i < mappings.TABLE.length; i++) {
                if (mappings.TABLE[i].RSD_FROM == '' || mappings.TABLE[i].RSD_FROM == 'ANY') {
                    stmt += "'" + mappings.TABLE[i].ESD_NAME + "' maps any to '" + mappings.TABLE[i].RSD_TO + "'";
                } else {
                    stmt += "'" + mappings.TABLE[i].ESD_NAME + "' maps '" + mappings.TABLE[i].RSD_FROM + "' to '" + mappings.TABLE[i].RSD_TO + "'";
                }
                stmt += i == mappings.TABLE.length - 1 ? "" : ",";
            }
            stmt += ")";
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSM", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "SHOW RESOURCE STATE MAPPING '" + tabInfo.NAME + "'";
        // execute returns a promise which can be used to fetch the response.
        return this.commandApi.execute(stmt).then((response: any) => {
            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;
            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("resource state mapping");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("resource state mapping");
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename " + "resource state mapping" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';";
        }

        // let rangeLength = obj.MAPPINGS.TABLE.length;
        stmt += " alter resource state mapping " + "'" + obj.NAME + "'";
        stmt += " with map = (";
        let mappings = obj.MAPPINGS;
        if (mappings != undefined && mappings != null) {
            for (let i = 0; i < mappings.TABLE.length; i++) {
                if (mappings.TABLE[i].RSD_FROM == '' || mappings.TABLE[i].RSD_FROM == 'ANY') {
                    stmt += "'" + mappings.TABLE[i].ESD_NAME + "' maps any to '" + mappings.TABLE[i].RSD_TO + "'";
                } else {
                    stmt += "'" + mappings.TABLE[i].ESD_NAME + "' maps '" + mappings.TABLE[i].RSD_FROM + "' to '" + mappings.TABLE[i].RSD_TO + "'";
                }
                stmt += i == mappings.TABLE.length - 1 ? "" : ",";
            }
        }
        stmt += "); end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSM", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Resource State Mapping}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop resource state mapping '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSM", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    openRsdFrom(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition(bicsuiteObject.RSD_FROM);
    }

    openRsdTo(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition(bicsuiteObject.RSD_TO);
    }

    openEsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openExitStateDefinition(bicsuiteObject.ESD_NAME);
    }

    editorform() {
        return rsm_editorform;

    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["MAPPINGS"] = {
            TABLE: [{
                ESD_NAME: "",
                RSD_FROM: "",
                RSD_TO: "",
                "NOFORM_DISABLE_AUTOCHOOSE": "true"
            }],
            DESC: [
                "ESD_NAME",
                "RSD_FROM",
                "RSD_TO",
                "NOFORM_DISABLE_AUTOCHOOSE"]
        };
        return bicsuiteObj;
    }

    getChangeConfig() {
        return {
            ESD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "MAPPINGS",
                            NAME: "ESD_NAME"
                        }
                    ]
                }
            },
            RSD: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "MAPPINGS",
                            NAME: "RSD_FROM"
                        },
                        {
                            TABLE: "MAPPINGS",
                            NAME: "RSD_TO"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
