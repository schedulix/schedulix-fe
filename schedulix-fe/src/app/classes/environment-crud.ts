import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import environment_editorform from '../json/editorforms/env.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { group } from "@angular/animations";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";

export class EnvironmentCrud extends Crud {
    createWith(mode: string, obj: any, tabInfo: Tab) {
        let withClause: string = "";
        if (obj.RESOURCES.TABLE.length > 0) {
            withClause = " with\n";
            let sep: string = "";
            withClause += " resource = (";
            for (let i = 0; i < obj.RESOURCES.TABLE.length; i++) {
                let resource = obj.RESOURCES.TABLE[i];
                if (resource.CONDITION == null || resource.CONDITION == undefined) {
                    resource.CONDITION = ''
                }
                withClause += sep + this.toolbox.namePathQuote(resource.NR_NAME) + " condition = '" + this.toolbox.textQuote(resource.CONDITION) + "'";
                sep = ", ";
            }
            withClause += ")";
        }
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create environment '" + obj.NAME + "' " + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ENV", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show environment '" + tabInfo.NAME + "' with expand = all";
        return this.commandApi.execute(stmt).then((response: any) => {
            tabInfo.ID = response.DATA.RECORD.ID;
            let job_definitions = [];
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "begin multicommand\n";
        // rename
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename environment '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
        }
        stmt += "alter environment '" + obj.NAME + "' " + this.createWith("UPDATE", obj, tabInfo) + ";\n";

        stmt += "end multicommand;";
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ENV", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Environment}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop environment " + "'" + obj.ORIGINAL_NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "ENV", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    custom(params: any, bicsuiteObject: any, tabInfo?: Tab): void {
        if (params.hasOwnProperty('CLICK_METHOD')) {
            if (params.CLICK_METHOD == 'OPEN_LISTITEM') {
                if (bicsuiteObject.hasOwnProperty('SE_PATH')) {
                    let pathlist = bicsuiteObject.SE_PATH.split('.');
                    let name = pathlist.pop();
                    let path = pathlist.join('.');

                    let folderTab = new Tab(name, bicsuiteObject.ID, bicsuiteObject.TYPE.toUpperCase(), bicsuiteObject.TYPE.toLowerCase(), TabMode.EDIT);
                    folderTab.PATH = path;

                    this.eventEmitterService.createTab(folderTab, true);
                }
            }
        }
    }

    conditionIsAdminOrEnvironmentManager(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.conditionIsAdmin({},{},tabInfo) || this.privilegesService.hasManagePriv("ENVIRONMENT");
    }

    openNrName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        // console.log(bicsuiteObject)
        return this.openNamedResource(bicsuiteObject.NR_NAME, "static");
    }

    editorform() {
        return environment_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["RESOURCES"] = {
            TABLE: [],
            DESC: [
                "RESOURCE_NAME"
            ]
        };
        return bicsuiteObj;
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)
        // console.log(bicsuiteObject)

        let grantObject = {
            TYPE: 'ENVIRONMENT',
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.ORIGINAL_NAME,
            // grants with hierarchy logic gets 'hierarchy' otherwise 'flat' for no parent structured objects
            GRANTTYPE: 'flat',
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS: "",
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;

                // read inherit parent grant
                if (grantObject.GRANTS.TABLE[0] && grantObject.GRANTTYPE != 'flat') {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    // grantObject.GRANTS.TABLE[0].OWNER = 'Inherit grant form parent';

                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        // console.log(i)
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            // console.log("merge inheritance pirvs to the root object")
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }

                // console.log(grantObject);

            }
            let editorForm = crud.editorformByType(grantObject.TYPE, false, false)
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for ENVIRONMENT " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }

    getChangeConfig() {
        return {
            NR: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "NR_NAME"
                        }
                    ]
                }
            },
            CATEGORY: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "NR_NAME"
                        }
                    ]
                }
            },
            FOLDER: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "JOB_DEFINITIONS",
                            NAME: "SE_PATH"
                        }
                    ]
                }
            },
            JOB_DEFINITION: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "JOB_DEFINITIONS",
                            NAME: "SE_PATH"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
