import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import sysinfo_editorform from '../json/editorforms/sysinfo.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import { group } from "@angular/animations";

export class SysinfoCrud extends Crud {
    create(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    update(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    drop(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }
    showSystem(obj: any): Promise<Object> {
        let stmt: string = "show system";
        return this.commandApi.execute(stmt).then((response: any) => {

            let promise = new BicsuiteResponse();
            response.DATA.RECORD.ExportVariables = response.DATA.RECORD.ExportVariables.replace(/,/g,",\n");
            promise.response = response;
            return promise;
        });
    }
    listSession(obj: any): Promise<Object> {
        let stmt: string = "list sessions";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            let sessionsLength = response.DATA.TABLE.length;
            for (let i = 0; i < sessionsLength; i++) {
                let session = response.DATA.TABLE[i];
                let statement = response.DATA.TABLE[i].STATEMENT;
                if (statement.startsWith("CONNECT")) {
                    statement = statement.replace (/^.*COMMAND *= *\(/, "").replace(/ *\)[^\)]*$/, "");
                    response.DATA.TABLE[i].STATEMENT = statement;
                    // console.log("Statement[" + i + "] = '" + statement + "'");
                }
            }
            promise.response = response;
            return promise;
        });
    }
    selectSmeQ(obj: any): Promise<Object> {
        if (!this.privilegesService.isAdmin()) return Promise.resolve({});
        let stmt: string = "SELECT * FROM SCI_SME_QUARTER ORDER BY 1 DESC, 2 DESC";
        return this.commandApi.execute(stmt, false).then((response: any) => {
            let promise = new BicsuiteResponse();
            if (response.hasOwnProperty("ERROR")) {
                response = {"DATA" : {"DESC" : [ "JAHR", "QUARTAL", "ANZAHL", "EXPECTED_ANZAHL", "AVG_ANZAHL_PRO_TAG" ], "TABLE": [] }};                
            }
            promise.response = response;
            return promise;
        });
    }
    read(obj: any, tabInfo: Tab): Promise<Object> {
        return Promise.all([
            this.showSystem(obj),
            this.listSession(obj),
            this.selectSmeQ(obj)
        ]).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response[0].response;
            promise.response.DATA.DESC.push ('SESSIONS');
            promise.response.DATA.RECORD.SESSIONS = {
                'DESC'  : response[1].response.DATA.DESC,
                'TABLE' : response[1].response.DATA.TABLE
            };
            if (this.privilegesService.isAdmin()) {
                promise.response.DATA.DESC.push ('SMEQ');
                promise.response.DATA.RECORD.SMEQ = {
                    'DESC'  : response[2].response.DATA.DESC,
                    'TABLE' : response[2].response.DATA.TABLE
                };
	        }
            // console.log(promise);
            return promise;
        });
    }

    conditionHaveSmeQ(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        if (bicsuiteObject.SMEQ && bicsuiteObject.SMEQ.TABLE.length > 0) return true; else return false;
    }

    editorform() {
        return sysinfo_editorform;
    }
}
