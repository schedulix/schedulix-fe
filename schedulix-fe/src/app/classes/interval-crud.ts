import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import int_editorform from '../json/editorforms/int.json';
import { Tab } from "../data-structures/tab";
import { TsCommonCrud } from "./ts-common-crud";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";

export class IntervalCrud extends TsCommonCrud {

    createWith(obj: any): string {
        let withClause = " with\n";
        withClause += "    group = '" + obj.OWNER + "',\n";
        withClause += "    dispatch = (\n";
        let dispatchers: string[] = [];
        for (let subSchedule of obj.SUBSCHEDULES.TABLE) {
            dispatchers.push(this.genDispatcher(subSchedule, false));
        }
        withClause += dispatchers.join(",\n");
        withClause += "\n    )";
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create interval '" + obj.NAME + "' (0)" + this.createWith(obj);
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "INTERVAL", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show interval '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let bicsuiteObject = response.DATA.RECORD;
            if (bicsuiteObject.SE_ID != "0") {
                response.ERROR = {
                    'ERRORCODE': 'ZSI-02912130907',
                    'ERRORMESSAGE': 'Cannot Edit Intervals not managed by the Web GUI Interval Editor!'
                };
            }
            bicsuiteObject.SUBSCHEDULES = { 'TABLE': [] };
            bicsuiteObject.NOFORM_INTERVALS = { 'TABLE': undefined };
            bicsuiteObject.NOFORM_SUBSCHEDULE = undefined;

            let MAXLEVEL = 1000000;
            let stopLevel = MAXLEVEL;
            let selections = '';
            let selection_modes = '';

            let subSchedule: any = {};
            let hierarchy = bicsuiteObject.HIERARCHY.TABLE;
            let synctime;
            for (let h of hierarchy) {
                let level = parseInt(h.LEVEL);
                if (level >= stopLevel) {
                    continue;
                }
                stopLevel = MAXLEVEL;
                if (h.ROLE == 'EMBEDDED' || (h.ROLE == 'FILTER' && ['', 'NONE', null].includes(h.OWNER_OBJ_TYPE))) {
                    stopLevel = level + 1
                    continue;
                }
                if (h.ROLE == 'HEAD') {
                    continue;
                }
                if (h.ROLE == 'DISPATCH') {

                    subSchedule = {
                        'NAME': h.NAME,
                        'OLD_NAME': h.NAME,
                        'ID': h.ID,
                        'ACTIVE': h.IS_ACTIVE,
                        'ENABLED': h.IS_ENABLED,
                        'INTERVALS': { 'TABLE': [] },
                        'NOFORM_SELECTED': false
                    };
                    bicsuiteObject.SUBSCHEDULES.TABLE.push(subSchedule);

                    if (!bicsuiteObject.NOFORM_SUBSCHEDULE && h.IS_ACTIVE == 'true' && h.IS_ENABLED == 'true') {
                        bicsuiteObject.NOFORM_SUBSCHEDULE = h.NAME;
                        subSchedule.NOFORM_SELECTED = true;
                    }

                    selections = '';
                    selection_modes = '';
                    synctime = '';
                }

                if (h.ROLE == 'DISPATCH_SELECT') {
                    subSchedule.START = h.STARTTIME;
                    subSchedule.END = h.ENDTIME;
                    subSchedule.INTERVAL_NAME = (h.FILTER ? h.FILTER : "");
                }

                if (['DISPATCH_FILTER', 'FILTER'].includes(h.ROLE)) {
                    let intervalType = h.NAME.split('_')[0];
                    let selections = h.SELECTION;
                    switch (intervalType) {
                        case 'REP':
                            selections = h.BASE.split(' ')[0];
                            synctime = h.SYNCTIME;
                            break;
                        case 'WOM':
                            selections = h.SELECTION.replaceAll(" ", "");
                            let dl = selections.split(',');
                            let dl1 = []
                            for (let dle of [1, 2, 3, 4, 5]) {
                                if (dl.includes((((dle - 1) * 7) + 1).toString())) {
                                    dl1.push(dle.toString());
                                }
                            }
                            for (let dle of [-5, -4, -3, -2, -1]) {
                                if (dl.includes((((dle + 1) * 7) - 1).toString())) {
                                    dl1.push(dle.toString());
                                }
                            }
                            selections = dl1.join(',');
                            break;
                        case 'ROD':
                            selections = h.SELECTION.replace(" ", "");
                            break;
                    }

                    if (['null', null, 'NONE'].includes(selections)) {
                        selections = '';
                    }

                    if (intervalType != 'WOM') {
                        let tmpSel = selections.split(",");
                        let newSel = []
                        for (let tmpS of tmpSel) {
                            newSel.push(tmpS.trim());
                        }
                        if (!['CAL', 'CLF', 'CLD'].includes(intervalType)) {
                            newSel.sort();
                        }
                        selections = newSel.join(",");
                    }

                    let selectionMode = 'NORMAL';
                    if (h.INVERSE == 'true') {
                        selectionMode = 'INVERSE'
                    }

                    // create INTERVALS table
                    let interval: any;
                    if (intervalType != 'FIX') {
                        if (['TOD', 'ROD'].includes(intervalType)) {
                            let selectionList = selections.split(',');
                            selectionList.sort();
                            for (let selection of selectionList) {
                                interval = {
                                    'TYPE': intervalType,
                                    'SETUP': selection,
                                    'SELECTION_MODE': selectionMode
                                };
                                this.setupFieldValues(interval);
                                subSchedule.INTERVALS.TABLE.push(interval);
                            }
                        }
                        else {
                            interval = {
                                'TYPE': intervalType,
                                'SETUP': selections,
                                'SELECTION_MODE': selectionMode
                            };
                            this.setupFieldValues(interval);
                            subSchedule.INTERVALS.TABLE.push(interval);
                        }
                        if (['CLD', 'CLF'].includes(intervalType)) {
                            interval.CALENDAR = h.EMBEDDED;
                            interval.SELECT_ON = h.BASE.substring(2).trim();
                        }
                    }
                }
            }

            if (!bicsuiteObject.NOFORM_SUBSCHEDULE) {
                bicsuiteObject.NOFORM_SUBSCHEDULE = bicsuiteObject.SUBSCHEDULES.TABLE[0].NAME;
                bicsuiteObject.SUBSCHEDULES.TABLE[0].NOFORM_SELECTED = true;
            }

            // set INTERVALS.TABLE of bicsuiteObject as a reference to INTERVALS.TABLE of the subschedule selected for editing
            for (subSchedule of bicsuiteObject.SUBSCHEDULES.TABLE) {
                if (subSchedule.NOFORM_SELECTED) {
                    bicsuiteObject.NOFORM_INTERVALS.TABLE = subSchedule.INTERVALS.TABLE;
                }
            }
            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;

            return promise;
        });

    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            stmt += "begin multicommand\n";
            stmt += "rename interval " + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
            obj.F_REFRESH_NAVIGATOR = true;
        }
        stmt += "alter interval " + "'" + obj.NAME + "' (0)" + this.createWith(obj);

        if (obj.F_REFRESH_NAVIGATOR) {
            stmt += ";\nend multicommand";
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "INTERVAL", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Interval}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop interval '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "INTERVAL", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return int_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.OWNER = this.privilegesService.getDefaultGroup();
        let subSchedules: any[] = [
                {
                    "ACTIVE": "true",
                    "ENABLED": "true",
                    "INTERVALS": {
                         "TABLE": [
                        ]
                    },
                    "NOFORM_SELECTED": true,
                    "INTERVAL_NAME": "",
                    "NAME": "DEFAULT",
                    "START":"",
                    "END":"",
                    "rowIndex": 0
                }
        ];
        record.SUBSCHEDULES = {
            "TABLE": subSchedules
        };
        record.NOFORM_SUBSCHEDULE = record.SUBSCHEDULES.TABLE[0].NAME;
        record.NOFORM_INTERVALS = record.SUBSCHEDULES.TABLE[0].INTERVALS;
        return bicsuiteObject;
    }

    selectDispatcher(bicsuiteObject: any, tabInfo: Tab, rowIndex: number): Promise<number> {
        for (let row of bicsuiteObject.SUBSCHEDULES.TABLE) {
            row.NOFORM_SELECTED = false;
        }
        bicsuiteObject.SUBSCHEDULES.TABLE[rowIndex].NOFORM_SELECTED = true;
        bicsuiteObject.NOFORM_SUBSCHEDULE = bicsuiteObject.SUBSCHEDULES.TABLE[rowIndex].NAME;
        bicsuiteObject.NOFORM_INTERVALS.TABLE = bicsuiteObject.SUBSCHEDULES.TABLE[rowIndex].INTERVALS.TABLE;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve(0);
    }

    syncSubScheduleName(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        for (let subschedule of bicsuiteObject.SUBSCHEDULES.TABLE) {
            if (subschedule.NOFORM_SELECTED) {
                bicsuiteObject.NOFORM_SUBSCHEDULE = subschedule.NAME;
            }
        }
        return true;
    }

    AppendDispatcher(bicsuiteObject: any, tabInfo: Tab, row: any) : Promise<Object> {
        return this.selectDispatcher(bicsuiteObject, tabInfo, row.rowIndex);
    }

    processIntervalTable(subSchedule: any, oldName: string, newName: string) : boolean {
        let affected: boolean = false;
        for (let interval of subSchedule.INTERVALS.TABLE) {
            if (interval.TYPE == "CLF" || interval.TYPE == "CLD") {
                if (interval.CALENDAR == oldName) {
                    interval.CALENDAR = newName;
                }
            }
        }
        return affected;
    }

    processIntervalRename(bicsuiteObject: any, tabInfo: Tab, initialBicsuiteObject: any, message: any): Promise<boolean> {
        let affected : boolean = false;
        if (bicsuiteObject?.DATA?.RECORD?.SUBSCHEDULES) {
            for (let subSchedule of bicsuiteObject.DATA.RECORD.SUBSCHEDULES.TABLE) {
                if (this.processIntervalTable(subSchedule, message.OLD_NAME, message.NEW_NAME)) {
                    affected = true;
                }
            }
            if (initialBicsuiteObject && initialBicsuiteObject.DATA && initialBicsuiteObject.DATA.RECORD) {
                for (let subSchedule of initialBicsuiteObject.DATA.RECORD.SUBSCHEDULES.TABLE) {
                    if (this.processIntervalTable(subSchedule, message.OLD_NAME, message.NEW_NAME)) {
                        affected = true;
                    }
                }
            }
        }
        return Promise.resolve(affected);
    }

    openIntervalName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openInterval(bicsuiteObject.INTERVAL_NAME);
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)
        // console.log(bicsuiteObject)

        let grantObject = {
            TYPE: 'INTERVAL',
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.ORIGINAL_NAME,
            // grants with hierarchy logic gets 'hierarchy' otherwise 'flat' for no parent structured objects
            GRANTTYPE: 'flat',
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS: "",
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;

                // read inherit parent grant
                if (grantObject.GRANTS.TABLE[0] && grantObject.GRANTTYPE != 'flat') {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    // grantObject.GRANTS.TABLE[0].OWNER = 'Inherit grant form parent';

                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        // console.log(i)
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            // console.log("merge inheritance pirvs to the root object")
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }

                // console.log(grantObject);

            }
            let editorForm = crud.editorformByType(grantObject.TYPE, false, false)
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for INTERVAL " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                panelClass: "fe-no-border-dialog",
                width: "1000px"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }

    dispatcherIsActive(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): any {
        let cssFlag = '';
        if(bicsuiteObject.NOFORM_SELECTED == true){
            cssFlag = 'crud-border-color-grey'
        }
        return cssFlag
    }
    
    getChangeConfig() {
        return {
            INTERVAL: {
                RENAME: {
                    METHOD: "processIntervalRename",
                    FIELDS: [
                        {
                            TABLE: "SUBSCHEDULES",
                            NAME: "INTERVAL_NAME"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}
