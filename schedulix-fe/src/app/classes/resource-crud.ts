import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import resource_editorform from '../json/editorforms/resource.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import editNamedResourceTriggerStates_editorform from '../json/editorforms/editNamedResourceTriggerStates_editorForm.json';
import editNamedResourceTriggerCondition_editorform from '../json/editorforms/editNamedResourceTriggerCondition_editorForm.json';
import { SdmsClipboardType, SdmsClipboardMode } from "../modules/main/services/clipboard.service";

export class ResourceCrud extends Crud {

    generateTriggerCommands(obj: any, tabInfo: Tab) : string[] {
        let triggerStatements : string[] = [];
        if (obj.TRIGGERS_READ == undefined) {
            obj.TRIGGERS_READ = [];
        }
        let triggersRead = [...obj.TRIGGERS_READ]; // Copy !!!
        let tmpRenames : string[] = [];
        let resourceObjUrl = this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER);
        for (let trigger of obj.TRIGGERS.TABLE) {
            if (trigger.NAME == "" || trigger.SUBMIT_NAME == "") {
                continue;
            }
            let originalName = trigger.ORIGINAL_NAME;
            let index = triggersRead.indexOf(originalName);
            trigger.NAME = this.convertIdentifier(trigger.NAME, trigger.ORIGINAL_NAME);
            trigger.ORIGINAL_NAME = trigger.NAME;
            let op = "create";
            let tmpName = trigger.NAME;
            if (index > -1) {
                op = "create or alter";
                triggersRead.splice(index, 1);
                if (trigger.NAME != originalName) {
                    tmpName = trigger.NAME + '_tmpRename' 
                    tmpRenames.push(trigger.NAME);
                    triggerStatements.push("rename trigger '" + originalName + "' on resource " + resourceObjUrl + " to '" + tmpName + "'");
                }
            }
            let triggerStatement = '';
            triggerStatement += op + " trigger '" + tmpName + "' on resource " +
                this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER) + " with ";
            let statesClause = "states = ()"
            let states = trigger.STATES;
            if (states != "") {
                statesClause = "states = (";
                let stsep = "";
                let stateList = states.split(",");
                for (let state of stateList) {
                    let elem = state.split("->");
                    let rsdNameFrom = elem[0]
                    if (rsdNameFrom == "" || rsdNameFrom == "ANY") {
                        rsdNameFrom = 'ANY';
                    }
                    else {
                        rsdNameFrom = "'" + rsdNameFrom + "'";
                    }
                    let rsdNameTo = 'ANY';
                    if (elem.length == 2) {
                        rsdNameTo = elem[1];
                    }
                    if (rsdNameTo == "" || rsdNameTo == "ANY") {
                        rsdNameTo = 'ANY';
                    }
                    else {
                        rsdNameTo = "'" + rsdNameTo + "'";
                    }
                    statesClause += stsep + rsdNameFrom + " " + rsdNameTo
                    if (rsdNameTo == "ANY" && rsdNameFrom == "ANY") {
                        statesClause = "states = (ANY ANY";
                        break;
                    }
                    stsep = ",";
                }
                statesClause += ")";
            }
            if (statesClause == "states = ()" || statesClause == "states = (ANY ANY)") {
                statesClause = "states = NONE";
            }
            triggerStatement += statesClause;
            let submitName = this.toolbox.namePathQuote(trigger.SUBMIT_NAME);
            triggerStatement += ", submit " + submitName;

            if (trigger.ACTIVE == "true") {
                triggerStatement += ", active";
            }
            else {
                triggerStatement += ", inactive";
            }
            triggerStatement += ", master, group = '" + trigger.SUBMIT_OWNER + "'";

            if (trigger.SUSPEND == "true") {
                triggerStatement += ", suspend";
            }
            else {
                triggerStatement += ", nosuspend";
            }
            let condition = trigger.CONDITION;
            if (condition != undefined && condition != null && condition != "") {
                condition.trim();
                condition = condition.replace(/\\/g, "\\\\");
                condition = condition.replace(/\'/g, "\\'");
                condition = "'" + condition + "'";
            }
            else {
                condition = "NONE";
            }
            triggerStatement += ", condition = " + condition + ";\n";
            triggerStatements.push(triggerStatement);
        }
        for (let triggerName of tmpRenames) {
            triggerStatements.push("rename trigger '" + triggerName + "_tmpRename' on named resource " + resourceObjUrl + " to '" + triggerName + "'");
        }
        let dropTriggerStatements : string[] = [];
        for (let name of triggersRead) {
            dropTriggerStatements.push("drop trigger '" + name + "' on resource " +
                this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER));
        }
        return dropTriggerStatements.concat(triggerStatements);
    }

    generateDistributionCommands(obj: any, tabInfo: Tab) : string[] {
        let distributionStatements = [];
        if (obj.DISTRIBUTIONS_READ == undefined) {
            obj.DISTRIBUTIONS_READ = [];
        }
        let distributionsRead = [...obj.DISTRIBUTIONS_READ] //Copy;
        let activeDistribution = "DEFAULT";
        // set to default here because we might drop the current active distribution
        if (obj.DISTRIBUTION_NAMES.TABLE.length > 1) {
            distributionStatements.push(
                "alter pool " + this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER) +
                " activate distribution '" + activeDistribution + "'"
            );
        }
        let tmpRenames : string[] = [];
        let poolObjUrl =  this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER);
        for (let distribution of obj.DISTRIBUTION_NAMES.TABLE) {
            // DEFAULT distribution is set within the create or alter pool statemnt !!!
            if (distribution.NAME == "DEFAULT") {
                 continue;
            }
            let originalDistributionName = distribution.ORIGINAL_NAME;
            let index = distributionsRead.indexOf(originalDistributionName)
            distribution.NAME = this.convertIdentifier(distribution.NAME, distribution.ORIGINAL_NAME);
            distribution.ORIGINAL_NAME = distribution.NAME;
            let tmpName = distribution.NAME;
            let op = "create";
            if (index > -1) {
                op = "alter";
                distributionsRead.splice(index, 1);
                if (distribution.NAME != originalDistributionName) {
                    tmpRenames.push(distribution.NAME);
                    tmpName = distribution.NAME + '_tmpRename';
                    distributionStatements.push("rename distribution '" + originalDistributionName + "' for pool " + poolObjUrl + " to '" + tmpName + "'");
                }
            }
            if (distribution.ACTIVE == "true") {
                activeDistribution = tmpName;
            }

            let distributionStatement = op + " distribution '" + tmpName + "' for pool " +  poolObjUrl + " with resource = ";
            if (distribution.DISTRIBUTION.length == 0) {
                distributionStatement += "none"
            }
            else {
                distributionStatement += "(";
                let resources : string[] = [];
                for (let resource of distribution.DISTRIBUTION) {
                    resources.push(
                        "resource " + this.toolbox.namePathQuote(resource.RESOURCENAME) + " in " + this.toolbox.namePathQuote(resource.RESOURCESCOPENAME) +
                        " " + (resource.IS_MANAGED == "true" ? "managed" : "not managed") +
                        " nominalpct = " + resource.NOMPCT +
                        " freepct = " + resource.FREEPCT +
                        " minpct = " + resource.MINPCT +
                        " maxpct = " + resource.MAXPCT
                    );
                }
                distributionStatement += resources.join(",\n");
                distributionStatement += ")";
            }
            distributionStatements.push(distributionStatement);
        }

        if (obj.DISTRIBUTION_NAMES.TABLE.length > 1) {
            distributionStatements.push(
                "alter pool " + this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER) +
                " activate distribution '" + activeDistribution + "'");
        }
        for (let distributionName of tmpRenames) {
            distributionStatements.push("rename distribution '" + distributionName + "_tmpRename' for pool " + poolObjUrl + " to '" + distributionName + "'");
        }
        // do drops before renames and alters !!!
        let dropDistributionStatements : string[] = [];
        for (let name of distributionsRead) {
            dropDistributionStatements.push("drop distribution '" + name + "' for pool " +
                this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER));
        }
        distributionStatements = dropDistributionStatements.concat( distributionStatements);
        if (distributionStatements.length > 0) {
            return distributionStatements;
        }
        else {
            return [];
        }
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        tabInfo.DATA.RESOURCE_NAME = obj.RESOURCE_NAME
        tabInfo.DATA.USAGE = obj.RESOURCE_USAGE
        obj.NAME = obj.RESOURCE_NAME.split('.').pop();
        let type = "resource";
        let stmts : string[] = [];
        if (obj.LINK_OR_RESOURCE == "RESOURCE") {
            if (obj.RESOURCE_USAGE == "POOL") {
                type = "pool";
            }
            stmts.push("create " + type + " " + this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER) +
                this.createWithStmt(obj));

            if (type == "pool") {
                stmts = stmts.concat(this.generateDistributionCommands(obj, tabInfo));
            }
            else {
                stmts = stmts.concat(this.generateTriggerCommands(obj, tabInfo));
            }
        }
        else {
            let linkSplit = obj.LINK_NAME.split(" (");
            let linkScopeName = linkSplit[0];
            let linkResourceName = linkSplit[1].split(")")[0];
            tabInfo.DATA.RESOURCE_NAME = linkResourceName;
            let targetType = "job server";
            if (tabInfo.DATA.TYPE == "SCOPE") {
                targetType = "scope";
            }
            stmts.push("link " + type + " " + this.toolbox.namePathQuote(linkResourceName) + " in " + this.toolbox.namePathQuote(linkScopeName) +
                " to " + targetType + " " + this.toolbox.namePathQuote(tabInfo.OWNER) + (obj.FORCE == "true" ? " force" : ""));
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RESOURCE", OP : "CREATE", NAME : obj.RESOURCE_NAME, OWNER : obj.RESOURCE_OWNER});
            return response;
        });
    }

    publicCreateResourceWithStmt(obj: any): string {
        let withstmt = '';
        let withstatement: string[] = [];

        if (obj.ONLINE == "true") {
            withstatement.push("online");
        } else {
            withstatement.push("offline");
        }

        if (obj.RESOURCE_USAGE == "SYNCHRONIZING" && obj.RESOURCE_STATE_PROFILE_NAME != "") {
            if (obj.SET_TIMESTAMP == "true") {
                let touch = "touch";
                if (obj.NEW_TIMESTAMP) {
                    touch += " = '" + obj.NEW_TIMESTAMP + " " + obj.NOFORM_TIME_ZONE + "'";
                }
                withstatement.push(touch);
            }

            if (obj.STATE && (obj.STATE != obj.OLD_STATE || obj.SET_TIMESTAMT == "true")) {
                withstatement.push("status = '" + obj.STATE + "'");
            }
        }
        if (obj.RESOURCE_USAGE == "SYSTEM" || obj.RESOURCE_USAGE == "SYNCHRONIZING") {
            withstatement.push("amount = " + (obj.DEFINED_AMOUNT ? obj.DEFINED_AMOUNT : 'infinite'));
            withstatement.push("requestable amount = " + (obj.REQUESTABLE_AMOUNT ? obj.REQUESTABLE_AMOUNT : 'infinite'));

            if (obj.FACTOR) {
                try {
                    withstatement.push("factor = " + (obj.FACTOR ? parseFloat(obj.FACTOR).toFixed(2) : 'none'));
                }
                catch (e) {
                    console.warn("illegal input for factor")
                }
            }
            else {
                withstatement.push("factor = 1.0");
            }

            if (obj.TAG && obj.TAG != 'NONE') {
                withstatement.push("tag = " + (obj.TAG ? "'" + obj.TAG + "'" : 'none'));
                withstatement.push("trace base = " + (obj.TRACE_BASE ? obj.TRACE_BASE : 'none'));
                withstatement.push("trace interval = " + (obj.TRACE_INTERVAL ? obj.TRACE_INTERVAL : 'none'));
                withstatement.push("base multiplier = " + (obj.TRACE_BASE_MULTIPLIER ? obj.TRACE_BASE_MULTIPLIER : 0));
            } else {
                withstatement.push("tag = none");
                withstatement.push("trace base = none");
                withstatement.push("trace interval= none");
                withstatement.push("base multiplier = 0");
            }
        }

        if (obj.OWNER != obj.OLD_OWNER) {
            withstatement.push("group = " + obj.OWNER);
        }

        let parameterstatements: string[] = [];
        if (obj.PARAMETERS.TABLE.length > 0) {
            for (let p of obj.PARAMETERS.TABLE) {
                if (p.TYPE != 'CONSTANT') {
                    parameterstatements.push("'" + p.NAME + "' = " + (p.IS_DEFAULT == "true" ? "DEFAULT" : "'" +
                        this.toolbox.textQuote(p.VALUE) + "'"));
                }
            }
        }
        if (parameterstatements.length > 0)
            withstatement.push("parameters = (" + parameterstatements.join(',') + ")");
        else {
            withstatement.push("parameters = none");
        }

        withstmt += " with " + withstatement.join(',');
        return withstmt;
    }

    publicCreatePoolWithStmt(obj: any): string {
        let withstmt = '';
        let withstatement: string[] = [];
        withstatement.push("amount = " + obj.DEFINED_AMOUNT);
        withstatement.push("cycle = " + (obj.EVALUATION_CYCLE ? obj.EVALUATION_CYCLE : 'NONE'));
        withstatement.push("group = " + (obj.OWNER ? obj.OWNER : 'NONE'));

        let resources : string[] = [];
        for (let distribution of obj.DISTRIBUTION_NAMES.TABLE) {
            if (distribution.NAME == 'DEFAULT') {
                for (let resource of distribution.DISTRIBUTION) {
                    resources.push(
                        "resource " + this.toolbox.namePathQuote(resource.RESOURCENAME) + " in " + this.toolbox.namePathQuote(resource.RESOURCESCOPENAME) +
                        " " + (resource.IS_MANAGED == "true" ? "managed" : "not managed") +
                        " nominalpct = " + resource.NOMPCT +
                        " freepct = " + resource.FREEPCT +
                        " minpct = " + resource.MINPCT +
                        " maxpct = " + resource.MAXPCT
                    );
                }
            }
        }
        if (resources.length > 0) {
            withstatement.push("resource = (" + resources.join(", ") + ")");
        }
        else {
            withstatement.push("resource = none");
        }

        if (obj.TAG && obj.TAG != 'NONE') {
            withstatement.push("tag = " + (obj.TAG ? "'" + obj.TAG + "'" : 'none'));
            withstatement.push("trace base = " + (obj.TRACE_BASE ? obj.TRACE_BASE : 'none'));
            withstatement.push("trace interval = " + (obj.TRACE_INTERVAL ? obj.TRACE_INTERVAL : 'none'));
            withstatement.push("base multiplier = " + (obj.TRACE_BASE_MULTIPLIER ? obj.TRACE_BASE_MULTIPLIER : 0));
        } else {
            withstatement.push("tag = none");
            withstatement.push("trace base = none");
            withstatement.push("trace interval= none");
            withstatement.push("base multiplier = 0");
        }

        withstmt += " with " + withstatement.join(',');
        return withstmt;
    }

    public createWithStmt(obj: any): string {
        if (obj.RESOURCE_USAGE != "POOL") {
            return this.publicCreateResourceWithStmt(obj);
        }
        else {
            return this.publicCreatePoolWithStmt(obj);
        }
    }


        // // DISTRIBUTION HERE

        // cmdlst.append('ALTER POOL ' + resourcename + ' IN ' + scopename + ' ACTIVATE DISTRIBUTION \'' + REQUEST['ACTIVE_DISTRIBUTION'] + '\'')

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmts : string [] = [];
        let type = "resource";
        if (obj.RESOURCE_USAGE == "POOL") {
            type = "pool";
        }
        let stmt = '';
        if (tabInfo.DATA.f_IsInstance) {
            stmt += "alter " + type + " " + obj.RESOURCE_ID;
        } else {
            stmt += "alter " + type + " " + this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER) +
                this.createWithStmt(obj);
        }
        stmts.push(stmt);
        if (type == "pool") {
            stmts = stmts.concat(this.generateDistributionCommands(obj, tabInfo));
        }
        else {
            stmts = stmts.concat(this.generateTriggerCommands(obj, tabInfo));
        }

        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RESOURCE", OP : "UPDATE", NAME : obj.RESOURCE_NAME, OWNER : obj.RESOURCE_OWNER});
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{RESOURCE}{" + obj.RESOURCE_NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                tabInfo.DATA.RESOURCE_NAME = obj.RESOURCE_NAME;
                obj.NAME = obj.RESOURCE_NAME.split('.').pop();
                let type = "resource";
                if (obj.RESOURCE_USAGE == "POOL") {
                    type = "pool";
                }
                let stmt = "drop " + type + " " + this.toolbox.namePathQuote(obj.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(obj.RESOURCE_OWNER) + ";";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RESOURCE", OP : "DROP", NAME : obj.RESOURCE_NAME, OWNER : obj.RESOURCE_OWNER});
                    return response;
                });
            }
            return {};
        });
    }

    readResourceStateProfile(bicsuiteObject: any, name: string) {
        let stmt: string = "show resource state profile '" + name + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            bicsuiteObject.PROFILE_STATES = response.DATA.RECORD.STATES.TABLE;
            bicsuiteObject.INITIAL_STATE = response.DATA.RECORD.INITIAL_STATE;
            return bicsuiteObject;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let ownerType = tabInfo.DATA.TYPE
        let stmt = '';

        let type = "resource";
        if (tabInfo.DATA.USAGE == "POOL") {
            type = "pool";
        }
        let readById = false;
        if (tabInfo.DATA.RESOURCE_ID) {
                stmt += "show " + type + " " + tabInfo.DATA.RESOURCE_ID + ";";
                readById = true;
        } else {
            stmt += "show " + type + " " + this.toolbox.namePathQuote(tabInfo.DATA.RESOURCE_NAME) + " in " + this.toolbox.namePathQuote(tabInfo.OWNER) + ";";
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            let record = response.DATA.RECORD;
            tabInfo.ID = record.ID;
            record.RESOURCE_OWNER_TYPE = record.SCOPE_TYPE;
            record.RESOURCE_ID = record.ID;
            record.RESOURCE_NAME = record.NAME;

            record.NAME = record.NAME.split('.').pop();
            tabInfo.NAME = record.NAME;

            record.RESOURCE_USAGE = record.USAGE;
            record.ONLINE = record.IS_ONLINE;
            record.RESOURCE_OWNER = record.SCOPENAME;
            record.CURRENT_AMOUNT = record.AMOUNT;
            record.OLD_STATE = record.STATE;
            record.OLD_OWNER = record.OWNER;
            if (!record.TAG) {
                record.TAG = "";
            }
            if (record.FACTOR == null) {
                record.FACTOR = '';
            }
            if (readById) {
                tabInfo.OWNER = record.RESOURCE_OWNER;
                let n = record.RESOURCE_NAME.split('.');
                if (n.length > 1) {
                    n.pop();
                    tabInfo.PATH = n.join('.');
                }
                tabInfo.updateTooltip();
            }
            if (type == "pool") {
                // add missing properties not given by pools
                // console.log("add missing props for pool")
                record.RESOURCE_USAGE = "POOL";
                type ResourceLookup = {
                    [key: string]: any,
                }
                let resourceLookup: ResourceLookup = {};
                for (let pooledResource of record.RESOURCES.TABLE) {
                    if (pooledResource.TYPE == "RESOURCE") {
                        pooledResource.RESOURCE_USAGE = "SYSTEM";
                    }
                    else {
                        pooledResource.RESOURCE_USAGE = "POOL"
                    }
                    resourceLookup[pooledResource.RESOURCENAME] = pooledResource;
                }
                for (let distribution of record.DISTRIBUTION_NAMES.TABLE) {
                    distribution.ACTIVE = (distribution.NAME == record.ACTIVE_DISTRIBUTION);
                }
                record.NOFORM_DISTRIBUTION = record.ACTIVE_DISTRIBUTION;
                let distributionsRead: any[] = [];
                for (let row of record.DISTRIBUTION_NAMES.TABLE) {
                    let distributionName = row.NAME;
                    if (distributionName != "DEFAULT") {
                        distributionsRead.push(distributionName);
                        row.ORIGINAL_NAME = row.NAME;
                    }
                    let distribution : any[] = [];
                    for (let distRow of record.DISTRIBUTIONS.TABLE) {
                        if (distRow.NAME == distributionName) {
                            distRow.RESOURCE_USAGE = resourceLookup[distRow.RESOURCENAME].RESOURCE_USAGE;
                            if (row.NAME == record.ACTIVE_DISTRIBUTION) {
                                resourceLookup[distRow.RESOURCENAME].IS_MANAGED = distRow.IS_MANAGED;
                            }
                            distribution.push(distRow);
                        }
                    }
                    row.DISTRIBUTION = distribution;
                    if (row.NAME == record.ACTIVE_DISTRIBUTION) {
                        record.NOFORM_DISTRIBUTIONS = { 'TABLE' : distribution}
                        row.NOFORM_SELECTED = true;
                    }
                }
                record.DISTRIBUTIONS_READ = distributionsRead;
            }
            else {
                // add missing properties not given by resources
            }

            // console.log(record.RESOURCE_STATE_PROFILE)
            if (record.RESOURCE_STATE_PROFILE != null && (tabInfo.DATA.f_isScopeOrFolderInstance)) {
                let stmt_trg: string = "list trigger for resource " + this.toolbox.namePathQuote(record.RESOURCE_NAME)
                    + " in " + this.toolbox.namePathQuote(record.RESOURCE_OWNER) + ";";
                return this.commandApi.execute(stmt_trg).then((response_trg: any) => {
                    if (!response_trg.hasOwnProperty('ERROR')) { // ignore error for resource templates (defined resources)
                        record.TRIGGERS = response_trg.DATA;
                        this.sortTriggers(record);
                        let triggersRead: any[] = [];
                        for (let row of record.TRIGGERS.TABLE) {
                            triggersRead.push(row.NAME);
                            let formatted_states = "";
                            let sep = "";
                            row.ORIGINAL_NAME = row.NAME;
                            if (row.STATES.trim() == "") {
                                row.STATES = "ANY:ANY";
                            }
                            let statesList = row.STATES.split(',');
                            if (statesList[0].trim() == "") {
                                statesList = [];
                            }
                            for (let state of statesList) {
                                state = state.trim();
                                let splitStates = state.split(':');
                                let state_from = splitStates[0].trim();
                                if (state_from == "") {
                                    state_from = "ANY";
                                }
                                let state_to = "ANY";
                                if (splitStates.length == 2) {
                                    state_to = splitStates[1].trim();
                                }
                                if (state_to == "") {
                                    state_to = "ANY";
                                }
                                formatted_states = formatted_states + sep + state_from + '->' + state_to;
                                sep = ",";
                            }
                            row.STATES = formatted_states;
                        }
                        record.TRIGGERS_READ = triggersRead;             // console.log(tabInfo.DATA)
                    }
                    else {
                        record.TRIGGERS = {};
                        record.TRIGGERS.TABLE = [];
                    }
                    return this.readResourceStateProfile(record, record.RESOURCE_STATE_PROFILE).then((response_rsp: any) => {
                        for (let trigger of record.TRIGGERS.TABLE) {
                            trigger.PROFILE_STATES = record.PROFILE_STATES;
                            trigger.INITIAL_STATE = record.INITIAL_STATE;
                        }
                        if (!tabInfo.DATA.NOFORM_TIME_ZONE) {
                            record.NOFORM_TIME_ZONE = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
                            tabInfo.DATA.NOFORM_TIME_ZONE = record.NOFORM_TIME_ZONE;
                        }
                        else {
                            record.NOFORM_TIME_ZONE = tabInfo.DATA.NOFORM_TIME_ZONE;
                        }
                        record.NOFORM_ORIGINAL_TIME_ZONE = record.NOFORM_TIME_ZONE;
                        if (record.TIMESTAMP) {
                            record.NOFORM_TIMESTAMP = this.toolbox.convertTimeStamp(
                                new Date(record.TIMESTAMP).toISOString(),
                                record.NOFORM_TIME_ZONE,
                                this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat"));
                        }
                        // console.log(promise.response);
                        return promise;
                    });
                });
            }
            else {
                record.TRIGGERS = {};
                record.TRIGGERS.TABLE = [];
                // console.log(promise.response);
                // console.log(tabInfo);
                if (record.RESOURCE_STATE_PROFILE != null) {
                    return this.readResourceStateProfile(record, record.RESOURCE_STATE_PROFILE).then((response_rsp: any) => {
                        return promise;
                    });
                }
                else {
                    return promise;
                }
            }
        });
    }

    editorform() {
        return resource_editorform;
    }

    selectDistribution(bicsuiteObject: any, tabInfo: Tab, rowIndex: number): Promise<number> {
        for (let row of bicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            row.NOFORM_SELECTED = false;
        }
        bicsuiteObject.NOFORM_DISTRIBUTION = bicsuiteObject.DISTRIBUTION_NAMES.TABLE[rowIndex].NAME;
        bicsuiteObject.NOFORM_DISTRIBUTIONS.TABLE = bicsuiteObject.DISTRIBUTION_NAMES.TABLE[rowIndex].DISTRIBUTION;
        bicsuiteObject.DISTRIBUTION_NAMES.TABLE[rowIndex].NOFORM_SELECTED = true;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve(0);
    }

    saveResourceComments(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {

        // console.log(bicsuiteObject);
        // console.log(tabInfo)

        // let monMasterTab = new Tab('Detail ' + parentBicsuiteObject.RESOURCE_OWNER.split('.').pop(), bicsuiteObject.MASTERID, 'Detail Master'.toLocaleUpperCase(), 'detail', TabMode.MONITOR_DETAIL)
        // this.eventEmitterService.createTab(monMasterTab, true);
        return Promise.resolve({})
    }
    // navigateToMasterJobList(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
    //     return Promise.resolve({});
    // }

    createOrOpenDetailTree(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        // let monMasterTab = new Tab('Detail ' + parentBicsuiteObject.RESOURCE_OWNER.split('.').pop(), bicsuiteObject.MASTERID, 'Detail Master'.toLocaleUpperCase(), 'detail', TabMode.MONITOR_DETAIL)
        let monMasterTab = new Tab(parentBicsuiteObject.RESOURCE_OWNER.split('.').pop(), bicsuiteObject.MASTERID, 'Detail Master'.toLocaleUpperCase(), 'detail', TabMode.MONITOR_DETAIL)
        this.eventEmitterService.createTab(monMasterTab, true);
        return Promise.resolve({})
    }

    chooseLinkedResource(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.RESOURCE_USAGE = chooseResult.bicsuiteObject.USAGE;
        tabInfo.shortType = bicsuiteObject.RESOURCE_USAGE.toLowerCase();
        tabInfo.NAME = chooseResult.bicsuiteObject.NAME.split(".").pop();
        return Promise.resolve(bicsuiteObject);
    }

    choosePooledResource(bicsuiteObject: any, tabInfo: Tab, chooseResult: any, parentBicsuiteObject: any): Promise<Object> {
        let splitResourceName = bicsuiteObject.RESOURCENAME.split(/ *[\(\)] */);
        if (splitResourceName.length > 1) {
            bicsuiteObject.RESOURCENAME = splitResourceName[1];
            bicsuiteObject.RESOURCESCOPENAME = splitResourceName[0];
        }
        bicsuiteObject.RESOURCE_USAGE = chooseResult.bicsuiteObject.USAGE;
        for (let distribution of parentBicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            let row = distribution.DISTRIBUTION[bicsuiteObject.rowIndex];
            row.RESOURCENAME = bicsuiteObject.RESOURCENAME;
            row.RESOURCESCOPENAME = bicsuiteObject.RESOURCESCOPENAME;
            row.RESOURCE_USAGE = bicsuiteObject.RESOURCE_USAGE;
        }
        return Promise.resolve(bicsuiteObject);
    }

    chooseResource(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        // console.log(chooseResult);
        bicsuiteObject.RESOURCE_USAGE = chooseResult.bicsuiteObject.USAGE;
        bicsuiteObject.RESOURCE_STATE_PROFILE = chooseResult.bicsuiteObject.RESOURCE_STATE_PROFILE;
        return this.loadChosenResource(bicsuiteObject, tabInfo).then((result: any) => {
            // console.log(result)
            bicsuiteObject.FACTOR = result.DATA.RECORD.FACTOR;
            bicsuiteObject.PARAMETERS;
            bicsuiteObject.PARAMETERS = result.DATA.RECORD.PARAMETERS;
            for (let parameter of result.DATA.RECORD.PARAMETERS.TABLE) {
                parameter.IS_DEFAULT = "true";
                parameter.VALUE = parameter.DEFAULT_VALUE;
            }
            bicsuiteObject.ONLINE = 'true';
            let defautMap: any = {
                SYNCHRONIZING: {
                    DEFINED_AMOUNT: "INFINITE",
                    REQUESTABLE_AMOUNT: "INFINITE",
                },
                SYSTEM: {
                    DEFINED_AMOUNT: 1,
                    REQUESTABLE_AMOUNT: 1,
                },
                STATIC: {
                    DEFINED_AMOUNT: null,
                    REQUESTABLE_AMOUNT: null,
                },
                POOL: {
                    DEFINED_AMOUNT: 0,
                    REQUESTABLE_AMOUNT: 0,
                }
            }
            bicsuiteObject.DEFINED_AMOUNT = defautMap[result.DATA.RECORD.USAGE].DEFINED_AMOUNT;
            bicsuiteObject.REQUESTABLE_AMOUNT = defautMap[result.DATA.RECORD.USAGE].REQUESTABLE_AMOUNT;

            //init tracing
            bicsuiteObject.TAG = '';
            bicsuiteObject.TRACE_INTERVAL = '';
            bicsuiteObject.TRACE_BASE = '';
            bicsuiteObject.TRACE_BASE_MULTIPLIER = 0;
            bicsuiteObject.AVG_ALLOCATION_1 = 'NONE';
            bicsuiteObject.AVG_ALLOCATION_2 = 'NONE';
            bicsuiteObject.AVG_ALLOCATION_3 = 'NONE';
            bicsuiteObject.LAST_WRITE = 'NONE';
            bicsuiteObject.AVG_ALLOCATION_WRITTEN = 'NONE';

            tabInfo.shortType = result.DATA.RECORD.USAGE.toLowerCase();
            tabInfo.NAME = result.DATA.RECORD.NAME.split('.').pop();

            if (bicsuiteObject.RESOURCE_STATE_PROFILE) {
                return this.readResourceStateProfile(bicsuiteObject, bicsuiteObject.RESOURCE_STATE_PROFILE).then((response: any) => {
                    bicsuiteObject.STATE = bicsuiteObject.INITIAL_STATE;
                    return bicsuiteObject;
                });
            }
            // rerender form when values have initialized
            this.eventEmitterService.reRenderTabEmitter.next(tabInfo);
            return bicsuiteObject;
        })
    }

    loadChosenResource(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        return this.commandApi.execute("show named resource " + this.toolbox.namePathQuote(bicsuiteObject.RESOURCE_NAME) + "");
    }

    editTriggerStates(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let states = [];
        // console.log(bicsuiteObject);
        let triggerProfileStates = [...bicsuiteObject.PROFILE_STATES];  // Copy
        triggerProfileStates.unshift({ "RSD_NAME": "ANY" });
        if (bicsuiteObject.STATES.trim() != "") {
            for (let state of bicsuiteObject.STATES.split(',')) {
                states.push({
                    "FROM_STATE": state.split('->')[0],
                    "TO_STATE": state.split('->')[1],
                    "PROFILE_STATES" : triggerProfileStates,
                    "INITIAL_STATE" : bicsuiteObject.INITIAL_STATE
                });
            }
        }
        let dataObj: any = {
            "STATES": { "TABLE": states },
            "PROFILE_STATES": triggerProfileStates,
            "INITIAL_STATE" : bicsuiteObject.INITIAL_STATE
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editNamedResourceTriggerStates_editorform, dataObj, tabInfo, "Edit Trigger States", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            let states = "";
            let sep = "";
            if (result != undefined || result != null && result) {
                for (let row of dataObj.STATES.TABLE) {
                    if (row.FROM_STATE == 'ANY' && row.TO_STATE == 'ANY') {
                        states = "ANY->ANY";
                        break
                    }
                    states = states + sep + row.FROM_STATE + '->' + row.TO_STATE;
                    sep = ','
                }
                bicsuiteObject.STATES = states;
            }
            return 1;
        });
    }

    editTriggerCondition(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let dataObj: any = {
            CONDITION: bicsuiteObject.CONDITION
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editNamedResourceTriggerCondition_editorform, dataObj, tabInfo, "Edit Trigger Condition", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                bicsuiteObject.CONDITION = dataObj.CONDITION;
            }
            return 1;
        });
    }

    AppendTrigger(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        row.PROFILE_STATES = bicsuiteObject.PROFILE_STATES;
        row.INITIAL_STATE = bicsuiteObject.INITIAL_STATE;
        return Promise.resolve({});
    }

    AppendTriggerState(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        row.PROFILE_STATES = bicsuiteObject.PROFILE_STATES;
        row.INITIAL_STATE = bicsuiteObject.INITIAL_STATE;
        return Promise.resolve({});
    }

    copyTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType,
            checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.RESOURCE_TRIGGER);
        return Promise.resolve({});
    }

    cutTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType,
            checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.RESOURCE_TRIGGER);
        return this.dropTriggers(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newTriggers = [];
        for (let child of bicsuiteObject.TRIGGERS.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newTriggers.push(child);
        }
        bicsuiteObject.TRIGGERS.TABLE = newTriggers;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    triggersPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.RESOURCE_TRIGGER];

    conditionPasteTriggers(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.triggersPasteTypes, tabInfo.ID);
    }

    pasteTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.triggersPasteTypes)) {
            // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
            let c = JSON.parse(JSON.stringify(item.data));
            // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
            // console.log(c);
            // console.log(bicsuiteObject)
            // continue;
            if (c.OBJECT_NAME != bicsuiteObject.RESOURCE_NAME + ' in ' + bicsuiteObject.RESOURCE_OWNER) {
                c.OBJECT_NAME = bicsuiteObject.RESOURCE_NAME + ' in ' + bicsuiteObject.RESOURCE_OWNER;
                c.ID = undefined; // this is  a new child!!!
            }
            item.mode = SdmsClipboardMode.COPY;
            bicsuiteObject.TRIGGERS.TABLE.push(c);
        }
        this.sortTriggers(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortTriggers(bicsuiteObject: any) {
        bicsuiteObject.TRIGGERS.TABLE.sort(function (a: any, b: any) {
            if (a.NAME < b.NAME) {
                return -1;
            }
            if (a.NAME > b.NAME) {
                return 1;
            }
            return a.SUBMIT_NAME < b.SUBMIT_NAME ? -1 : 1;
        });
    }

    chooseSubmit(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.SUBMIT_TYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }

    filterSystemResourcesOnly(editorformField: any, items: any): any {
        let result = [];
        for (let row of items) {
            let type = row.type;
            if ((type == 'SCOPE' || type == 'SERVER')) {
                if (row.children) {
                    row.children = this.filterSystemResourcesOnly(editorformField, row.children);
                }
            }
            else {
                if (row.type != "SYSTEM" && row.type != "POOL") {
                    continue;
                }
            }
            result.push(row);
        }
        if (result.length == 0) {
            result.push(this.treeFunctionService.getDummyDialogTreeNode("no_system_resource_or_pool_found_for_this_entity"));
        }
        return result;
    }

    filterNoPoolResourcesOnly(editorformField: any, items: any): any {
        let result = [];
        for (let row of items) {
            let type = row.type;
            if ((type == 'SCOPE' || type == 'SERVER')) {
                if (row.children) {
                    row.children = this.filterNoPoolResourcesOnly(editorformField, row.children);
                }
            }
            else {
                if (row.type == "POOL") {
                    continue;
                }
            }
            result.push(row);
        }
        if (result.length == 0) {
            result.push(this.treeFunctionService.getDummyDialogTreeNode("no_resource_found_for_this_entity"));
        }
        return result;
    }

    syncMasterDistributionName(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        bicsuiteObject.NOFORM_DISTRIBUTION = "";
        for (let distribution of bicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            if (distribution.NOFORM_SELECTED) {
                bicsuiteObject.NOFORM_DISTRIBUTION = distribution.NAME;
                break;
            }
        }
        return true;
    }

    AppendDistribution(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        for (let distribution of bicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            if (distribution.NAME == "DEFAULT") {
                row.DISTRIBUTION = this.toolbox.deepCopy(distribution.DISTRIBUTION);
                break;
            }
        }
        return this.selectDistribution(bicsuiteObject, tabInfo, row.rowIndex);
    }

    AppendResource(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        for (let distribution of bicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            let addRow = {
                "ID" : "0",
                "NAME" : "",
                "RESOURCENAME" : "",
                "RESOURCESCOPENAME" : "",
                "RESOURCE_USAGE" : "RESOURCE",
                "IS_MANAGED" : "false",
                "NOMPCT" : "0",
                "MINPCT" : "0",
                "MAXPCT" : "100",
                "FREEPCT" : "0",
                "TYPE" : "RESOURCE"
            }
            distribution.DISTRIBUTION.push(addRow);
        }

        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve(0);
    }

    DropResource(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        for (let distribution of bicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            distribution.DISTRIBUTION.splice(row.rowIndex, 1);
        }
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve(0);
    }

    DropDistribution(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        for (let distribution of bicsuiteObject.DISTRIBUTION_NAMES.TABLE) {
            if (distribution.ACTIVE == "true") {
                return this.selectDistribution(bicsuiteObject, tabInfo, distribution.rowIndex);
            }
        }
        return this.selectDistribution(bicsuiteObject, tabInfo, 0);
    }

    openTriggerSubmit(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.SUBMIT_NAME, bicsuiteObject.SUBMIT_TYPE.toLowerCase());
    }

    openLinkScope(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openScope(bicsuiteObject.LINK_SCOPE, bicsuiteObject.LINK_SCOPE_TYPE.toLowerCase());
    }

    openResourceOwner(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        switch (bicsuiteObject.RESOURCE_OWNER_TYPE) {
            case "SCOPE":
                return this.openScope(bicsuiteObject.RESOURCE_OWNER, "scope");
            case "SERVER":
                return this.openScope(bicsuiteObject.RESOURCE_OWNER, "server");
            case "FOLDER":
                return this.openFolder(bicsuiteObject.RESOURCE_OWNER);
            case "JOB":
                return this.openTabForPath(bicsuiteObject.RESOURCE_OWNER, "job");
            case "BATCH":
                return this.openTabForPath(bicsuiteObject.RESOURCE_OWNER, "batch");
            default:
                console.warn("Unknown RESOURCE_OWNER_TYPE: " + bicsuiteObject.RESOURCE_OWNER_TYPE);
                return Promise.resolve({});
        }
    }

    conditionIsNewScopeOrServerResource(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return tabInfo.MODE == TabMode.NEW && bicsuiteObject.LINK_OR_RESOURCE == "RESOURCE" &&
        (tabInfo.DATA.TYPE == "SCOPE" || tabInfo.DATA.TYPE == "SERVER");
    }

    conditionIsNewFolderOrDefinedResource(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return tabInfo.MODE == TabMode.NEW && bicsuiteObject.LINK_OR_RESOURCE == "RESOURCE" && ["FOLDER", "BATCH", "JOB"].includes(tabInfo.DATA.TYPE);
    }

    conditionIsNewLink(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return tabInfo.MODE == TabMode.NEW && bicsuiteObject.LINK_OR_RESOURCE == "LINK";
    }

    conditionResourceWasChosen(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESOURCE_NAME != '';
    }

    conditionPooledResourceIsManaged(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.IS_MANAGED == "true") ? true : false;
    }

    conditionResourceWasChosenAndNotPool(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESOURCE_NAME != '' && bicsuiteObject.RESOURCE_USAGE != "POOL";
    }

    conditionResourceIsTracable(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.RESOURCE_NAME != '' && tabInfo.DATA.f_isScopeOrFolderInstance &&
            (bicsuiteObject.RESOURCE_USAGE == "SYSTEM" || bicsuiteObject.RESOURCE_USAGE == "SYNCHRONIZONG")) ? true : false;
    }

    conditionResourceIsInstance(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return tabInfo.DATA.f_IsInstance;
    }

    conditionIsPooled(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.MANAGER_ID ? true : false);
    }

    conditionCanHaveAllocations(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (tabInfo.MODE != TabMode.NEW &&
            (tabInfo.DATA.f_IsInstance || tabInfo.DATA.f_isScopeOrFolderInstance) && bicsuiteObject.RESOURCE_NAME != '' && 
             (bicsuiteObject.RESOURCE_USAGE == "SYSTEM" || bicsuiteObject.RESOURCE_USAGE == "SYNCHRONIZING")) {
                return true;
        }
        return false;
    }

    conditionDefinedAmountVisible(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if ((bicsuiteObject.RESOURCE_USAGE == 'SYNCHRONIZING' ||
            bicsuiteObject.RESOURCE_USAGE == 'SYSTEM' ||
            bicsuiteObject.RESOURCE_USAGE == 'POOL') && !bicsuiteObject.MANAGER_ID) {
                return true;
        }
        return false;
    }

    conditionResourceIsSyncOrSysOrPool(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return ((tabInfo.DATA.f_IsInstance || tabInfo.DATA.f_isScopeOrFolderInstance) && (bicsuiteObject.RESOURCE_USAGE == 'SYNCHRONIZING' ||
            bicsuiteObject.RESOURCE_USAGE == 'SYSTEM' ||
            bicsuiteObject.RESOURCE_USAGE == 'POOL')) ? true : false;
    }

    conditionResourceIsSyncOrSys(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESOURCE_USAGE == 'SYNCHRONIZING' ||
            bicsuiteObject.RESOURCE_USAGE == 'SYSTEM';
    }

    conditionResourceIsPool(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.RESOURCE_USAGE == 'POOL';
    }

    conditionIsNewPooledResource(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.IS_NEW == "true";
    }

    conditionParameterisNotConstant(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE != 'CONSTANT';
    }

    conditionParameterisNotConstantAndIsNotDefault(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TYPE != 'CONSTANT' && bicsuiteObject.IS_DEFAULT == 'false';
    }

    conditionIsTraced(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.TAG != '' && bicsuiteObject.TAG != null;
    }

    resourceTypeColorCondition(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        let cssFlag = ''
        if (bicsuiteObject.TYPE == 'BLOCKED') {
            cssFlag += ' crud-color-red';
        }
        return cssFlag
    }

    conditionHasResourceStateProfile(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.RESOURCE_STATE_PROFILE) {
            return true;
        }
        else {
            return false;
        }
    }

    conditionHasResourceStateProfileAndIsInstance(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (this.conditionHasResourceStateProfile(editorformField, bicsuiteObject, tabInfo) &&
            (tabInfo.DATA.f_IsInstance || tabInfo.DATA.f_isScopeOrFolderInstance)) ? true : false;
    }

    conditionHasResourceStateProfileWithConvert (editorformField: any, bicsuiteObject: any, tabInfo: Tab):boolean {
        if (bicsuiteObject.NOFORM_ORIGINAL_TIME_ZONE != bicsuiteObject.NOFORM_TIME_ZONE) {
            bicsuiteObject.NOFORM_TIMESTAMP = this.toolbox.convertTimeStamp(
                new Date(bicsuiteObject.TIMESTAMP).toISOString(),
                bicsuiteObject.NOFORM_TIME_ZONE,
                this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat"));
                bicsuiteObject.NOFORM_ORIGINAL_TIME_ZONE = bicsuiteObject.NOFORM_TIME_ZONE;
                tabInfo.DATA.NOFORM_TIME_ZONE = bicsuiteObject.NOFORM_TIME_ZONE;
        }
        return (bicsuiteObject.RESOURCE_STATE_PROFILE &&
            (tabInfo.DATA.f_IsInstance || tabInfo.DATA.f_isScopeOrFolderInstance)) ? true : false;
    }

    conditionIsLink(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.LINK_SCOPE ? true : false);
    }

    conditionIsDefaultDistribution(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.NAME == 'DEFAULT');
    }

    conditionCanDropDistribution(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return (bicsuiteObject.NAME != 'DEFAULT' && bicsuiteObject.ACTIVE != true && bicsuiteObject.ACTIVE != "true");
    }

    conditionIsNewAndScopeOrServer(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        if (bicsuiteObject.OLD_LINK_OR_RESOURCE != bicsuiteObject.LINK_OR_RESOURCE) {

            bicsuiteObject.RESOURCE_NAME = '';          // set name to '' to indicate not yet choosen
            bicsuiteObject.RESOURCE_USAGE = "RESOURCE"; // if unchoosen reset type to RESOURCE

            bicsuiteObject.OLD_LINK_OR_RESOURCE = bicsuiteObject.LINK_OR_RESOURCE;
        }
        return this.conditionIsNew(editorformField, bicsuiteObject, tabInfo) && (
            tabInfo.DATA.TYPE == "SCOPE" || tabInfo.DATA.TYPE == "SERVER");
    }

    conditionInUserGroupsAndNotLink(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.conditionInUserGroups(editorformField, bicsuiteObject, tabInfo) &&
            !this.conditionIsLink(editorformField, bicsuiteObject, tabInfo);
    }

    conditionSetTimeStamp(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.SET_TIMESTAMP == "true";
    }

    isInstanceCSSSelector(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        if (tabInfo.DATA.f_IsInstance) {
            editorForm.CSS_CLASS = "crud-color-grey"
            editorForm.TRANSLATE = "instance"
        } else if (tabInfo.DATA.f_isScopeOrFolderInstance) {
            editorForm.CSS_CLASS = "crud-color-grey"
            editorForm.TRANSLATE = "folderOrScopeInstance"
        } else {
            editorForm.CSS_CLASS = "crud-color-grey"
            editorForm.TRANSLATE = "template"
        }
        return Promise.resolve({})
    }

    nrChooserAny(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        bicsuiteObject.unshift({
            ID: "-1",
            NAME: "ANY",
            PRIVS: "KDEVG",
            CSS_CLASS: "crud-list-any",
            ICON_DISABLED: true
        });
        return bicsuiteObject;
    }

    default(tabInfo: Tab) {
        // console.log(tabInfo)
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.RESOURCE_OWNER = tabInfo.OWNER;
        record.JOB_OWNER_ID = tabInfo.DATA.ID;
        record.OWNER = this.privilegesService.getDefaultGroup();
        record.USAGE = '*UNSET';
        record.RESOURCE_NAME = '';
        record.RESOURCE_USAGE = 'resource';
        record.LINK_OR_RESOURCE = "RESOURCE";
        record.OLD_LINK_OR_RESOURCE = "RESOURCE";
        record.REQUESTABLE_AMOUNT = "INFINITE";
        record.DEFIINED_AMOUNT = "INFINITE";
        record.RESOURCE_OWNER_TYPE = tabInfo.DATA.TYPE;
        record.NOFORM_TIME_ZONE = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
        record.NOFORM_ORIGINAL_TIME_ZONE = record.NOFORM_TIME_ZONE;
        record.NOFORM_TIMESTAMP = "";
        // bicsuiteObject.DATA.RECORD.PARAMETERS = {}
        // bicsuiteObject.DATA.RECORD.PARAMETERS.TABLE = [];
        return bicsuiteObject;
    }

    processTriggerResourceStates(triggers: any[], newName: string, oldName: string) {
        for (let trigger of triggers) {
            if (trigger.STATES) {
                let rules = trigger.STATES.split(',');
                let newRules = [];
                for (let rule of rules) {
                    rule = rule.trim();
                    let states = rule.split("->");
                    let newStates = [];
                    for (let state of states) {
                        state = state.trim();
                        if (state == oldName) {
                            state = newName;
                        }
                        newStates.push(state);
                    }
                    newRules.push(newStates.join("->"));
                }
                trigger.STATES = newRules.join(',');
            }
        }
        return false;
    }

    processRsdRename(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.TRIGGERS) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Rsd Name Change
        if (oldName && newName) {
            this.processTriggerResourceStates(record.TRIGGERS.TABLE, newName, oldName);
            if (initialRecord?.TRIGGERS) {
                this.processTriggerResourceStates(initialRecord.TRIGGERS.TABLE, newName, oldName);
            }
        }
        return Promise.resolve(false);
    }

    processCheckDropResource(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let closeTab : boolean = false;
        if ((bicsuiteObject.DATA.RECORD.RESOURCE_OWNER + '.').startsWith(message.OWNER + '.')) {
            if (message.TYPE != 'RESOURCE') {
                closeTab = true;
            }
            else {
                if (bicsuiteObject.DATA.RECORD.RESOURCE_NAME == message.NAME) {
                    closeTab = true;
                }
            }
        }
        if (closeTab) {
            this.eventEmitterService.removeTabEmitter.next(tabInfo);
        }
        return Promise.resolve(false);
    }

    processRenameResourceOwner(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        if (["JOB_DEFINITION", "FOLDER", "SCOPE", "SERVER"].includes(message.TYPE)) {
            if (tabInfo.OWNER == message.OLD_NAME) {
                tabInfo.OWNER = message.NEW_NAME;
                tabInfo.updateTooltip();
            }
            else {
                if (tabInfo.OWNER.startsWith(message.OLD_NAME + '.')) {
                    tabInfo.OWNER = tabInfo.OWNER.replace(message.OLD_NAME + '.', message.NEW_NAME + '.');
                    tabInfo.updateTooltip();
                }
            }
        }
        return Promise.resolve(false);
    }

    getChangeConfig() {
        return {
            RESOURCE: {
                DROP: {
                    METHOD: "processCheckDropResource"
                }
            },
            CATEGORY: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "RESOURCE_NAME"
                        },
                        {
                            NAME: "MANAGER_NAME"
                        },
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCENAME"
                        },
                        {
                            TABLE: "NOFORM_DISTRIBUTIONS",
                            NAME: "RESOURCENAME"
                        }
                    ]
                }
            },
            FOLDER: {
                RENAME: {
                    METHOD: "processRenameResourceOwner",
                    FIELDS: [
                        {
                            NAME: "RESOURCE_OWNER"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        },
                        {
                            TABLE: "ALLOCATIONS",
                            NAME: "JOBNAME"
                        },
                        {
                            TABLE: "ALLOCATIONS",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCESCOPENAME"
                        }
                    ]
                },
                DROP: {
                    METHOD: "processCheckDropResource"
                }
            },
            GROUP: {
                RENAME: {
                   FIELDS: [
                        {
                            NAME: "OWNER"
                        },
                        {
                            NAME: "RESOURCE_OWNER"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_OWNER"
                        }
                    ]
                }
            },
            JOB_DEFINITION: {
                RENAME: {
                    METHOD: "processRenameResourceOwner",
                    FIELDS: [
                        {
                            NAME: "RESOURCE_OWNER"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        },
                        {
                            TABLE: "ALLOCATIONS",
                            NAME: "JOBNAME"
                        },
                        {
                            TABLE: "ALLOCATIONS",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCESCOPENAME"
                        }
                    ]
                },
                DROP: {
                    METHOD: "processCheckDropResource"
                }
            },
            NR: {
                RENAME: {
                    METHOD: "processPathChange",
                    FIELDS: [
                        {
                            NAME: "RESOURCE_NAME"
                        },
                        {
                            NAME: "MANAGER_NAME"
                        },
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCENAME"
                        },
                        {
                            TABLE: "NOFORM_DISTRIBUTIONS",
                            NAME: "RESOURCENAME"
                        }
                    ]
                }
            },
            RSD: {
                RENAME: {
                    METHOD: "processRsdRename",
                    FIELDS: [
                        {
                            NAME: "STATE"
                        }
                    ]
                }
            },
            RSM: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_JOB_DEFINITIONS",
                            NAME: "RESOURCE_STATE_MAPPING"
                        }
                    ]
                }
            },
            RSP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "RESOURCE_STATE_PROFILE"
                        }
                    ]
                }
            },
            SCOPE: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "RESOURCE_OWNER"
                        },
                        {
                            NAME: "LINK_SCOPE"
                        },
                        {
                            NAME: "MANAGER_SCOPENAME"
                        },
                        {
                            TABLE: "RESOURCES",
                            NAME: "RESOURCESCOPENAME"
                        },
                        {
                            TABLE: "NOFORM_DISTRIBUTIONS",
                            NAME: "RESOURCESCOPENAME"
                        }
                    ]
                },
                DROP: {
                    METHOD: "processCheckDropResource"
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }
}
