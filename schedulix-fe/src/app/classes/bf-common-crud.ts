import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";

export abstract class BFCommonCrud extends Crud {

    doCheckConventionFolder(obj : any, path : string, id : string, property : string): Promise<Object> {
        let pathList = path.split('.');
        pathList.push(pathList[pathList.length - 1]);
        let batchName = pathList.join(".");
        let stmt = "list folder " + this.toolbox.namePathQuote(path) +
            " with expand = (" +  id + "), name like '" + batchName + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let result : string = "false";
            let table = response.DATA.TABLE;
            if (table.length > 0) {
                let lastRow = table.pop();
                if (batchName == lastRow.NAME && ["JOB","BATCH"].includes(lastRow.TYPE)) {
                    obj[property + '_TYPE'] = lastRow.TYPE;
                    result = "true";
                }
            }
            obj[property] = result;
            return {};
        });
    }

    checkConventionFolder(obj: any): Promise<Object> {
        obj.IS_CONVENTION_FOLDER = "false";
        obj.IN_CONVENTION_FOLDER = "false";
        let promises: Promise<any>[] = [];
        if (obj.PATH && obj.TYPE == "FOLDER") {
            promises.push(this.doCheckConventionFolder(obj, obj.PATH + "." + obj.NAME, obj.ID, "IS_CONVENTION_FOLDER"));
        }
        if (obj.PATH.split('.').length > 1) {
            promises.push(this.doCheckConventionFolder(obj, obj.PATH, obj.PARENT_ID, "IN_CONVENTION_FOLDER"));
        }
        return Promise.all(promises).then((res) => {
            return {};
        });
    }

    createAddChildCommand (parentName : string, childName : string, isStatic : boolean) : string {
        parentName = this.toolbox.namePathQuote(parentName);
        childName = this.toolbox.namePathQuote(childName);
        let staticOrDynamic = isStatic ? "static" : "dynamic";
        let command = 'alter job definition ' + parentName + " add or alter children = (" + childName + " " + staticOrDynamic + ")";
        return command;
    }

    createRenameBatchInFolderCommand (obj : any) : string {
        let oldBatchName = this.toolbox.namePathQuote(obj.PATH + '.' + obj.NAME + '.' + obj.ORIGINAL_NAME);
        let command = "rename job definition " + oldBatchName + " to '" + obj.NAME + "'";
        return command;
    }

    createAddFolderBatchCommand (obj : any) : string {
        let name = this.toolbox.namePathQuote(obj.PATH + '.' + obj.NAME + '.' + obj.NAME);
        let command = "create job definition " + name + " with type = batch, profile = '" + obj.ESP_NAME + "', group = '" + obj.OWNER + "'";
        return command;
    }

}
