import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import nr_editorform from '../json/editorforms/nr.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import editNamedResourceTriggerStates_editorform from '../json/editorforms/editNamedResourceTriggerStates_editorForm.json';
import editNamedResourceTriggerCondition_editorform from '../json/editorforms/editNamedResourceTriggerCondition_editorForm.json';
import { EditorformDialogComponent, EditorformDialogData } from "../modules/main/components/form-generator/editorform-dialog/editorform-dialog.component";
import { GrantCrud } from "./grant-crud";
import { SdmsClipboardType, SdmsClipboardMode } from "../modules/main/services/clipboard.service";
import { SnackbarType, OutputType } from "../modules/global-shared/components/snackbar/snackbar-data";
import editFolderParameterComment_editorform from '../json/editorforms/editFolderParameterComment_editorForm.json';

export class NamedResourceCrud extends Crud {
    updateTriggers(obj: any, tabInfo: Tab) : string[] {
        let triggerStatements : string[] = [];
        let newTriggers = [];
        if (obj.TRIGGERS_READ == undefined) {
            obj.TRIGGERS_READ = [];
        }
        let tmpRenames : string[] = [];
        let resourceObjUrl = this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME);
        let triggersRead = [...obj.TRIGGERS_READ]; // Copy !!!
        for (let trigger of obj.TRIGGERS.TABLE) {
            let triggerStatement = "";
            if (trigger.NAME == "" || trigger.SUBMIT_NAME == "") {
                continue;
            }
            let originalName = trigger.ORIGINAL_NAME;
            let index = triggersRead.indexOf(originalName);
            trigger.NAME = this.convertIdentifier(trigger.NAME, trigger.ORIGINAL_NAME);
            trigger.ORIGINAL_NAME = trigger.NAME;
            let op = "create";
            let tmpName = trigger.NAME;
            if (index > -1) {
                op = "create or alter";
                triggersRead.splice(index, 1);
                if (trigger.NAME != originalName) {
                    tmpRenames.push(trigger.NAME);
                    tmpName = trigger.NAME + '_tmpRename';
                    triggerStatements.push("rename trigger '" + originalName + "' on named resource " + resourceObjUrl + " to '" + tmpName + "'");
                }
            }
            triggerStatement += op + " trigger '" + tmpName + "' on named resource " + resourceObjUrl + " with ";
            let statesClause = "states = ()"
            let states = trigger.STATES;
            if (states != "") {
                statesClause = "states = (";
                let stsep = "";
                let stateList = states.split(",");
                for (let state of stateList) {
                    let elem = state.split("->");
                    let rsdNameFrom = elem[0]
                    if (rsdNameFrom == "" || rsdNameFrom == "ANY") {
                        rsdNameFrom = 'ANY';
                    }
                    else {
                        rsdNameFrom = "'" + rsdNameFrom + "'";
                    }
                    let rsdNameTo = 'ANY';
                    if (elem.length == 2) {
                        rsdNameTo = elem[1];
                    }
                    if (rsdNameTo == "" || rsdNameTo == "ANY") {
                        rsdNameTo = 'ANY';
                    }
                    else {
                        rsdNameTo = "'" + rsdNameTo + "'";
                    }
                    statesClause += stsep + rsdNameFrom + " " + rsdNameTo
                    if (rsdNameTo == "ANY" && rsdNameFrom == "ANY") {
                        statesClause = "states = (ANY ANY";
                        break;
                    }
                    stsep = ",";
                }
                statesClause += ")";
            }
            if (statesClause == "states = ()" || statesClause == "states = (ANY ANY)") {
                statesClause = "states = NONE";
            }
            triggerStatement += statesClause;
            let submitName = this.toolbox.namePathQuote(trigger.SUBMIT_NAME);
            triggerStatement += ", submit " + submitName;

            if (trigger.ACTIVE == "true") {
                triggerStatement += ", active";
            }
            else {
                triggerStatement += ", inactive";
            }
            triggerStatement += ", master, group = '" + trigger.SUBMIT_OWNER + "'";

            if (trigger.SUSPEND == "true") {
                triggerStatement += ", suspend";
            }
            else {
                triggerStatement += ", nosuspend";
            }
            let condition = trigger.CONDITION;
            if (condition != undefined && condition != null && condition != "") {
                condition.trim();
                condition = condition.replace(/\\/g, "\\\\");
                condition = condition.replace(/\'/g, "\\'");
                condition = "'" + condition + "'";
            }
            else {
                condition = "NONE";
            }
            triggerStatement += ", condition = " + condition + ";\n";
            triggerStatements.push(triggerStatement);
            newTriggers.push(trigger.NAME);
        }
        for (let triggerName of tmpRenames) {
            triggerStatements.push("rename trigger '" + triggerName + "_tmpRename' on named resource " + resourceObjUrl + " to '" + triggerName + "'");
        }
        // do drops before renames and alters !!!
        let dropTriggerStatements : string[] = [];
        for (let name of triggersRead) {
            dropTriggerStatements.push("drop trigger '" + name + "' on named resource " + obj.QUOTED_FULLNAME)
        }
        return dropTriggerStatements.concat(triggerStatements);
    }

    createWith(mode: string, obj: any, tabInfo: Tab) {
        let isAdmin: boolean = this.privilegesService.isAdmin();
        let withClause: string = " with\n";
        let sep = "    ";
        if (mode != "UPDATE") {
            withClause += sep + "usage = " + obj.USAGE;
            sep = ",\n    ";
        }
        if (obj.USAGE == 'SYNCHRONIZING') {
            if (obj.RESOURCE_STATE_PROFILE != "NONE" && obj.RESOURCE_STATE_PROFILE) {
                withClause += sep + "status profile = '" + obj.RESOURCE_STATE_PROFILE + "'";
                sep = ",\n    ";
            } else {
                withClause += sep + "status profile = NONE"
                sep = ",\n    ";
                // UNSET WHEN
            }
        }
        withClause += sep + "parameters = "
        let pSep = "(\n        ";
        let paramsLength = obj.PARAMETERS.TABLE.length;
        for (let i = 0; i < paramsLength; i++) {
            let item = obj.PARAMETERS.TABLE[i];
            withClause += pSep + "'" + this.convertIdentifier(item.NAME, item.ORIGINAL_NAME) + "' " + item.TYPE;
            if (item.DEFAULT_VALUE && item.DEFAULT_VALUE != "NONE") {
                let defaultValue = this.toolbox.textQuote(item.DEFAULT_VALUE);
                withClause += " = '" + defaultValue + "'"
            }
            else {
                if (item.TYPE == 'constant') {
                    withClause += " = ''"
                }
            }
            pSep = ",\n        ";
        }
        if (paramsLength > 0) {
            withClause += ")";
        }
        else {
            withClause += "NONE";
        }
        sep = ",\n    ";
        if (this.privilegesService.getGroups().includes(obj.OWNER) || this.privilegesService.isAdmin()) {
            withClause += sep + "group = '" + obj.OWNER + "'";
            if (obj.CASCADE_SET_GROUP == "true") {
                withClause += " " + "cascade";
            }
        }
        if (this.privilegesService.isEdition('BASIC')) {
            if (obj.INHERIT_GRANTS == 'ALL') {
                withClause += sep + 'inherit grant = ' + '(DROP,EDIT,RESOURCE,VIEW)'
            } else if (obj.INHERIT_GRANTS == 'VIEW') {
                withClause += sep + 'inherit grant = ' + '(VIEW)'
            } else {
                withClause += sep + 'inherit grant = ' + 'NONE'
            }
            sep = ",\n    ";
        }
        let factor = "NONE";
        if (obj.USAGE == "SYNCHRONIZING" || obj.USAGE == "SYSTEM" || obj.USAGE == "POOL") {
            factor = obj.FACTOR;
            if (factor.indexOf(".") < 0) {

            }
            if (factor == "NONE" || factor == "") {
                factor = "1.0";
            }
            else {
                if (factor.indexOf(".") < 0) {
                    factor += ".0";
                }
            }
            withClause += sep + "factor = " + factor;
        }
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME =  this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts : string[] = [];

        stmts.push("create named resource " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + obj.NAME) + this.createWith("CREATE", obj, tabInfo));
        if (obj.RESOURCE_STATE_PROFILE != undefined && obj.RESOURCE_STATE_PROFILE != null) {
            stmts = stmts.concat(this.updateTriggers(obj, tabInfo));
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "NR", OP : "CREATE", NAME : tabInfo.PATH +'.' + obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show named resource " + this.toolbox.namePathQuote(tabInfo.PATH + "." + tabInfo.NAME);
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            let record = response.DATA.RECORD;
            tabInfo.ID = record.ID;

            record.FULLNAME = this.toolbox.namePathQuote(record.NAME);
            let pathlist = record.NAME.split(".");
            record.NAME = pathlist.pop();
            record.PATH = pathlist.join(".");
            record.QUOTED_FULLNAME = this.getQuotedFullName(tabInfo);

            this.setInheritGrants(record);
            let paramsLength = record.PARAMETERS.TABLE.length;
            for (let i = 0; i < paramsLength; i++) {
                let item = record.PARAMETERS.TABLE[i];
                item.PARENTNAME = tabInfo.PATH + '.' + tabInfo.NAME;
                if (item.TAG && item.COMMENT) {
                    item.COMMENT = item.TAG + " " + item.COMMENT;
                }
                else {
                    if (item.TAG) {
                        item.COMMENT = item.TAG;
                    }
                }
                item.NOFORM_COMMENT = item.COMMENT;
                item.ORIGINAL_NAME = item.NAME;
            }
            this.sortParameters(record);
            for (let resource of record.RESOURCES.TABLE) {
                resource.SCOPE = resource.SCOPE.replace(".[", "[");
                let amount = parseFloat(resource.AMOUNT);
                if (amount) {
                    resource.LOAD = (amount - parseFloat(resource.FREE_AMOUNT)) * 100 / amount;
                }
                else {
                    resource.LOAD = 0;
                }
            }
            promise.response = response;
            if (record.RESOURCE_STATE_PROFILE != null) {
                record.RESOURCE_STATE_PROFILE_READ = record.RESOURCE_STATE_PROFILE;
                let stmt_trg: string = "list trigger for named resource " +record.QUOTED_FULLNAME;
                return this.commandApi.execute(stmt_trg).then((response_trg: any) => {
                    record.TRIGGERS = response_trg.DATA;
                    let triggersRead = [];
                    for (let row of record.TRIGGERS.TABLE) {
                        row.PARENTNAME = record.PATH + '.' + record.NAME;
                        triggersRead.push(row.NAME);
                        let formatted_states = "";
                        let sep = "";
                        row.ORIGINAL_NAME = row.NAME;
                        if (row.STATES.trim() == "") {
                            row.STATES = "ANY:ANY";
                        }
                        let statesList = row.STATES.split(',');
                        if (statesList[0].trim() == "") {
                            statesList = [];
                        }
                        for (let state of statesList) {
                            state = state.trim();
                            let splitStates = state.split(':');
                            let state_from = splitStates[0].trim();
                            if (state_from == "") {
                                state_from = "ANY";
                            }
                            let state_to = "ANY";
                            if (splitStates.length == 2) {
                                state_to = splitStates[1].trim();
                            }
                            if (state_to == "") {
                                state_to = "ANY";
                            }
                            formatted_states = formatted_states + sep + state_from + '->' + state_to;
                            sep = ",";
                        }
                        row.STATES = formatted_states;
                    }
                    record.TRIGGERS_READ = triggersRead;
                    this.sortTriggers(record);                        // console.log(tabInfo.DATA)

                    let stmt_rsp: string = "show resource state profile '" + record.RESOURCE_STATE_PROFILE + "';";
                    return this.commandApi.execute(stmt_rsp).then((response_rsp: any) => {
                        record.PROFILE_STATES = response_rsp.DATA.RECORD.STATES.TABLE;
                        record.INITIAL_STATE = response_rsp.DATA.RECORD.INITIAL_STATE;
                        for (let trigger of record.TRIGGERS.TABLE) {
                            trigger.PROFILE_STATES = record.PROFILE_STATES;
                            trigger.INITIAL_STATE = record.INITIAL_STATE;
                        }
                        record.NOFORM_JOB_DEFINITIONS = record.JOB_DEFINITIONS;
                        return promise;
                    });
                });
            }
            else {
                record.RESOURCE_STATE_PROFILE = "";
                record.RESOURCE_STATE_PROFILE_READ = "";
                record.TRIGGERS = {};
                record.TRIGGERS.TABLE = [];
                record.NOFORM_JOB_DEFINITIONS = record.JOB_DEFINITIONS;
                return promise;
            }
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmts = [];
        if (obj.NAME != obj.ORIGINAL_NAME) {
            stmts.push("rename named resource " + obj.QUOTED_FULLNAME + " to '" + obj.NAME + "'");
            obj.F_REFRESH_NAVIGATOR = true;
        }
        stmts.push("alter named Resource " + this.toolbox.namePathQuote(tabInfo.PATH + "." + obj.NAME) + " " + this.createWith("UPDATE", obj, tabInfo));
        if (obj.RESOURCE_STATE_PROFILE != undefined && obj.RESOURCE_STATE_PROFILE != null) {
            stmts = stmts.concat(this.updateTriggers(obj, tabInfo));
        }
        for (let parameter of obj.PARAMETERS.TABLE) {
            if (parameter.NOFORM_comments) {
                let stmt = "create comment on parameter '" + parameter.NAME + "' of named resource " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
                let withStmt = []
                for (let c of parameter.NOFORM_comments) {
                    c.TAG = c.TAG ? c.TAG : '';
                    let desc = c.COMMENT ? this.toolbox.textQuote(c.COMMENT) : '';
                    withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'");
                }
                stmt += withStmt.join(',');
                stmts.push(stmt);
            }
        }
        return this.commandApi.executeMulticommand(stmts).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "NR", OP : "RENAME", OLD_NAME : tabInfo.PATH + "." + tabInfo.NAME, NEW_NAME : tabInfo.PATH + "." + obj.NAME});
                tabInfo.NAME = obj.NAME;
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Named Resource}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop named resource " + obj.QUOTED_FULLNAME;
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "NR", OP : "DROP", NAME : tabInfo.PATH +'.' + obj.ORIGINAL_NAME});
                    tabInfo.NAME = obj.NAME;
                    return response;
                });
            }
            return {};
        });
    }

    editorform() {
        return nr_editorform;
    }

    chooseSubmit(bicsuiteObject: any, tabInfo: Tab, chooseResult: any): Promise<Object> {
        bicsuiteObject.SUBMIT_TYPE = chooseResult.bicsuiteObject.TYPE;
        return Promise.resolve({});
    }

    openGrant(bicsuiteObject: any, tabInfo: Tab, parentBicsuiteObject: any): Promise<Object> {
        let crud = new GrantCrud(
            this.commandApi,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertiesService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipboardService)
        // console.log(bicsuiteObject)

        let grantObject = {
            TYPE: bicsuiteObject.USAGE,
            OWNER: bicsuiteObject.OWNER,
            NAME: bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME,
            INHERIT_EFFECTIVE_PRIVS: '',
            PRIVS:"",
            GRANTS: {
                TABLE: [] as any[]
            }
        };

        // create tabinfo copy with tab-type == GRANT to have all editorform methods including conditions enabled
        let grantTabInfo: Tab = this.toolbox.deepCopy(tabInfo);
        grantTabInfo.TYPE = 'GRANT'

        return crud.read(grantObject, grantTabInfo).then((result: any) => {
            if (result.DATA) {
                grantObject.GRANTS.TABLE = result.DATA.TABLE;
                if (grantObject.GRANTS.TABLE[0]) {
                    grantObject.GRANTS.TABLE[0].GROUP = 'Inherit grant form parent';
                    // grantObject.GRANTS.TABLE[0].OWNER = 'Inherit grant form parent';

                    for (let i = 1; i < grantObject.GRANTS.TABLE.length; i++) {
                        // console.log(i)
                        if (grantObject.GRANTS.TABLE[i].GROUP == grantObject.OWNER) {
                            // console.log("merge inheritance pirvs to the root object")
                            grantObject.INHERIT_EFFECTIVE_PRIVS = grantObject.GRANTS.TABLE[i].EFFECTIVE_PRIVS;
                            grantObject.GRANTS.TABLE.splice(i, 1);
                            break;
                        }
                    }
                }
                // console.log(grantObject);
            }

            let editorForm = crud.editorformByType(grantObject.TYPE, false, true)
            if (this.privilegesService.getGroups().includes(grantObject.OWNER)) {
                grantObject.PRIVS = "E";
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editorForm, grantObject, grantTabInfo, "Select grants for " + bicsuiteObject.TYPE + " " + (bicsuiteObject != undefined ? bicsuiteObject.NAME : ""), crud),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            // * Note, dialogs unsubsrcibe autoamtically!
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                if (result) {
                    if (result.hasOwnProperty('ERROR')) {
                        return 1;
                    } else {
                        // console.log(result)
                        return 0;
                    }
                    // });
                }
                return 1;
            });
        })
    }


    editTriggerStates(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let states = [];
        let triggerProfileStates = [...bicsuiteObject.PROFILE_STATES];  // Copy
        triggerProfileStates.unshift({ "RSD_NAME": "ANY" });
        if (bicsuiteObject.STATES.trim() != "") {
            for (let state of bicsuiteObject.STATES.split(',')) {
                states.push({
                    "FROM_STATE": state.split('->')[0],
                    "TO_STATE": state.split('->')[1],
                    "PROFILE_STATES": triggerProfileStates,
                    "INITIAL_STATE": bicsuiteObject.INITIAL_STATE,
                });
            }
        }
        let dataObj: any = {
            "STATES": { "TABLE": states },
            "PROFILE_STATES": triggerProfileStates,
            "INITIAL_STATE": bicsuiteObject.INITIAL_STATE
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editNamedResourceTriggerStates_editorform, dataObj, tabInfo, "Edit Trigger States", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            let states = "";
            let sep = "";
            if (result != undefined || result != null && result) {
                for (let row of dataObj.STATES.TABLE) {
                    if (row.FROM_STATE == 'ANY' && row.TO_STATE == 'ANY') {
                        states = "ANY->ANY";
                        break
                    }
                    states = states + sep + row.FROM_STATE + '->' + row.TO_STATE;
                    sep = ','
                }
                bicsuiteObject.STATES = states;
            }
            return 1;
        });
    }

    editTriggerCondition(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let dataObj: any = {
            CONDITION: bicsuiteObject.CONDITION
        };
        if (bicsuiteObject.PRIVS) {
            dataObj.PRIVS = bicsuiteObject.PRIVS;
        }
        const dialogRef = this.dialog.open(EditorformDialogComponent, {
            data: new EditorformDialogData(editNamedResourceTriggerCondition_editorform, dataObj, tabInfo, "Edit Trigger Condition", this),
            panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
            width: "1000px"
        });
        return dialogRef.afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                bicsuiteObject.CONDITION = dataObj.CONDITION;
            }
            return 1;
        });
    }

    AppendTrigger(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        row.PROFILE_STATES = bicsuiteObject.PROFILE_STATES;
        row.INITIAL_STATE = bicsuiteObject.INITIAL_STATE;
        return Promise.resolve({});
    }

    AppendTriggerState(bicsuiteObject: any, tabInfo: Tab, row: any): Promise<Object> {
        row.PROFILE_STATES = bicsuiteObject.PROFILE_STATES;
        row.INITIAL_STATE = bicsuiteObject.INITIAL_STATE;
        return Promise.resolve({});
    }

    copyTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType,
            checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.RESOURCE_TRIGGER);
        return Promise.resolve({});
    }

    cutTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType,
            checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.RESOURCE_TRIGGER);
        return this.dropTriggers(bicsuiteObject, tabInfo, form, checklistSelection);
    }

    dropTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newTriggers = [];
        for (let child of bicsuiteObject.TRIGGERS.TABLE) {
            if (dropRowIndexes.includes(child.rowIndex)) {
                continue;
            }
            newTriggers.push(child);
        }
        bicsuiteObject.TRIGGERS.TABLE = newTriggers;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    triggersPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.RESOURCE_TRIGGER];

    conditionPasteTriggers(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.triggersPasteTypes, tabInfo.ID);
    }

    pasteTriggers(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.triggersPasteTypes)) {
            // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
            let c = JSON.parse(JSON.stringify(item.data));
            // preserve ID if the CUT was from the same parent to get save button off after cut paste in same table
            // console.log(c);
            // console.log(bicsuiteObject)
            // continue;
            if (c.PARENTNAME != bicsuiteObject.PATH + '.' + bicsuiteObject.NAME) {
                c.PARENTNAME = bicsuiteObject.PATH + '.' + bicsuiteObject.NAME;
                c.ID = undefined; // this is  a new child!!!
            }
            item.mode = SdmsClipboardMode.COPY;
            bicsuiteObject.TRIGGERS.TABLE.push(c);
        }
        this.sortTriggers(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    private sortTriggers(bicsuiteObject: any) {
        bicsuiteObject.TRIGGERS.TABLE.sort(function (a: any, b: any) {
            if (a.NAME < b.NAME) {
                return -1;
            }
            if (a.NAME > b.NAME) {
                return 1;
            }
            return a.SUBMIT_NAME < b.SUBMIT_NAME ? -1 : 1;
        });
    }

    getSelectedParameterComments(bicsuiteObject: any, tabInfo: Tab, selectedParameter : any) : Promise<Object> {
        let stmt : string = "show comment on parameter '" + selectedParameter.ORIGINAL_NAME + "' of named resource " +
            this.toolbox.namePathQuote(bicsuiteObject.PATH + '.' + bicsuiteObject.NAME);
        return this.commandApi.execute(stmt).then((response: any) => {
            selectedParameter.NOFORM_comments = response.DATA.TABLE;
            return response;
        });
    }

    getSelectedParametersComments(bicsuiteObject: any, tabInfo: Tab, selectedParameters : any[]) : Promise<Object> {
        let promises: Promise<any>[] = [];
        for (let selectedParameter of selectedParameters) {
            if (selectedParameter.COMMENT) {
                promises.push(this.getSelectedParameterComments(bicsuiteObject, tabInfo, selectedParameter));
            }
        }
        return Promise.all(promises).then((res) => {
            return {};
        });
    }

    sortParameters(bicsuiteObject : any) {
        bicsuiteObject.PARAMETERS.TABLE.sort(function(a : any, b : any) {
            return a.NAME < b.NAME ? -1 : 1;
        });
    }

    copyParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.NR_PARAMETER);
            return {};
        });
    }

    cutParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        return this.getSelectedParametersComments(bicsuiteObject, tabInfo, checklistSelection.selected).then((res) => {
            this.clipboardService.addItems(tabInfo.TYPE as unknown as SdmsClipboardType, checklistSelection.selected, tabInfo, bicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.NR_PARAMETER);
            return this.dropParameters(bicsuiteObject, tabInfo, form, checklistSelection);
        });
    }

    dropParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<Object> {
        let dropRowIndexes = [];
        for (let selected of checklistSelection._selected) {
            dropRowIndexes.push(selected.rowIndex);
        }
        let newParameters = [];
        for (let parameter of bicsuiteObject.PARAMETERS.TABLE) {
            if (dropRowIndexes.includes(parameter.rowIndex)) {
                continue;
            }
            newParameters.push(parameter);
        }
        bicsuiteObject.PARAMETERS.TABLE = newParameters;
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    parameterPasteTypes: SdmsClipboardType[] = [SdmsClipboardType.NR_PARAMETER];

    conditionPasteParameters(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.clipboardService.checkItemTypesPresent(this.parameterPasteTypes, tabInfo.ID);
    }

    pasteParameters(bicsuiteObject: any, tabInfo: Tab, form: any, checklistSelection: any): Promise<any> {
        for (let item of this.clipboardService.getItems(this.parameterPasteTypes)) {
            // IMPORTANT: deepCopy here to not get cross editing of copied objects !!!
            let p = JSON.parse(JSON.stringify(item.data));
            let parentName = bicsuiteObject.PATH ? bicsuiteObject.PATH + '.' + bicsuiteObject.NAME : bicsuiteObject.NAME;
            if (p.PARENTNAME != parentName) {
                p.PARENTNAME = parentName;
                p.ID = undefined; // this is  a new parameter!!!
            }
            item.mode = SdmsClipboardMode.COPY;
            bicsuiteObject.PARAMETERS.TABLE.push(p);
        }
        this.sortParameters(bicsuiteObject);
        this.eventEmitterService.bicsuiteRefreshEmitter.next(tabInfo);
        return Promise.resolve({});
    }

    editParameterComment(bicsuiteObject: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show comment on parameter '" + bicsuiteObject.NAME + "' of named resource " + this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        return this.commandApi.execute(stmt).then((response1: any) => {
            for (let o of response1.DATA.TABLE) {
                o.DESCRIPTION = o.COMMENT
            }
            let dataObj: any = {
                COMMENT: {
                    TABLE: response1.DATA.TABLE
                },
                NAME: bicsuiteObject.NAME
            };
            if (bicsuiteObject.PRIVS) {
                dataObj.PRIVS = bicsuiteObject.PRIVS;
            }
            const dialogRef = this.dialog.open(EditorformDialogComponent, {
                data: new EditorformDialogData(editFolderParameterComment_editorform, dataObj, tabInfo, "Edit Parameter Comments", this),
                panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
                width: "1000px"
            });
            return dialogRef.afterClosed().toPromise().then((result: any) => {
                // Update Original Object
                let visibleComment = []
                for (let c of result.COMMENT.TABLE) {
                    visibleComment.push(c.TAG + "\n\n" + c.DESCRIPTION)
                }
                bicsuiteObject.NOFORM_COMMENT = visibleComment.join('\n\n\n')
                return result;
            });
        });
    }

    saveParameterComments(bicsuiteObject: any, tabInfo: Tab, deepCopyBicsuiteObject: any): Promise<Object> {
        let stmt;
        if (deepCopyBicsuiteObject.COMMENT.TABLE.length > 0) {
            stmt = "create or alter comment on parameter '" + bicsuiteObject.NAME + "' of named resource " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME) + " with";
            let withStmt = []
            for (let c of deepCopyBicsuiteObject.COMMENT.TABLE) {
                c.TAG = c.TAG ? c.TAG : '';
                let desc = c.DESCRIPTION ? this.toolbox.textQuote(c.DESCRIPTION) : '';
                withStmt.push(" tag = '" + c.TAG + "', text = '" + desc + "'")
            }
            stmt += withStmt.join(',')
        }
        else {
            stmt = "drop comment on parameter '" + bicsuiteObject.NAME + "' of named resource " +
                this.toolbox.namePathQuote(tabInfo.PATH + '.' + tabInfo.NAME);
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            if (response.hasOwnProperty("FEEDBACK")) {
                this.eventEmitterService.pushSnackBarMessage([response.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);
            }
            return response;
        });
    }

    openResourceScope(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        // console.log(bicsuiteObject);
        switch (bicsuiteObject.TYPE) {
            case "SCOPE":
                return this.openScope(bicsuiteObject.SCOPE, "scope");
            case "SERVER":
                return this.openScope(bicsuiteObject.SCOPE, "server");
            case "FOLDER":
                return this.openFolder(bicsuiteObject.SCOPE);
            case "JOB":
                return this.openTabForPath(bicsuiteObject.SCOPE, "job");
            case "BATCH":
                return this.openTabForPath(bicsuiteObject.SCOPE, "batch");
            default:
                // console.log("Unknown TYPE: " + bicsuiteObject.TYPE);
                return Promise.resolve({});
        }
    }

    openTriggerSubmit(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.SUBMIT_NAME, bicsuiteObject.SUBMIT_TYPE.toLowerCase());
    }

    openJobDefinition(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.NAME, "job");
    }

    openResourceCategory(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openCategory(bicsuiteObject.PATH);
    }

    conditionIsNewParameter(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.ID == undefined;
    }

    conditionIsSynchronizing(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return bicsuiteObject.USAGE == "SYNCHRONIZING";
    }

    conditionIsStatic(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return bicsuiteObject.USAGE == "STATIC";
    }

    openJobDefinitionName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.NAME, "job");
    }

    openRequiredResourceStateMapping(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateMapping(bicsuiteObject.RESOURCE_STATE_MAPPING);
    }

    openStickyParent(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openTabForPath(bicsuiteObject.STICKY_PARENT, "batch");
    }

    openOwner(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openGroup (bicsuiteObject.OWNER);
    }

    openState(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition (bicsuiteObject.STATE);
    }

    // conditionIsCategory(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
    //     console.log(bicsuiteObject.USAGE)
    //     return bicsuiteObject.USAGE == "CATEGORY";
    // }

    // conditionEnableSetGroupRecursive(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
    //     console.log("conditionEnableSetGroupRecursive")
    //     return this.conditionIsCategory(editorformField, bicsuiteObject, tabInfo) &&
    //         this.conditionOwnerChanged(editorformField, bicsuiteObject, tabInfo);
    // }

    conditionHasResourceStateProfile(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        if (bicsuiteObject.RESOURCE_STATE_PROFILE)
            return true;
        else
            return false;
    }

    conditionIsPool(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return bicsuiteObject.USAGE == "POOL";
    }

    conditiontIsTemplate(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return ['JOB','BATCH'].includes(bicsuiteObject.TYPE) && !bicsuiteObject.SCOPE.endsWith(']');
    }
    
    nrChooserAny(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        bicsuiteObject.unshift({
            ID: "-1",
            NAME: "ANY",
            PRIVS: "KDEVG",
            CSS_CLASS: "crud-list-any",
            ICON_DISABLED: true
        });
        return bicsuiteObject;
    }


    setParameterDefaultValueMandatory(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {

        switch (bicsuiteObject.TYPE) {
            case 'CONSTANT': {
                editorForm.MANDATORY = true;
                break;
            }
            default: {
                editorForm.MANDATORY = false;
                break;
            }
        }
        return Promise.resolve({});
    }

    setParameterToParameterType(bicsuiteObject: any, tabInfo: Tab, editorForm: any, parentBicsuiteObject: any): Promise<Object> {
        if (parentBicsuiteObject.USAGE == 'SYNCHRONIZING' && this.privilegesService.validateEdition(['PROFESSIONAL', 'ENTERPRISE'])) {
            editorForm.VALUES = [
                "CONSTANT",
                "LOCAL_CONSTANT",
                "PARAMETER"
            ]
        } else {
            editorForm.VALUES = [
                "CONSTANT",
                "LOCAL_CONSTANT"
            ]
        }
        return Promise.resolve({});
    }

    conditionRSPEnabled(editorformField: any, bicsuiteObject: any, tabInfo: Tab) {
        return (bicsuiteObject.RESOURCE_STATE_PROFILE_READ == '' && bicsuiteObject.RESOURCES.TABLE.length < 1) || bicsuiteObject.RESOURCE_STATE_PROFILE_READ != '';
    }

    // conditionOwnerChanged(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
    //     console.log(bicsuiteObject.OWNER)
    //     console.log(bicsuiteObject.ORIGINAL_OWNER)
    //     return bicsuiteObject.OWNER != bicsuiteObject.ORIGINAL_OWNER && !this.conditionIsNew(editorformField, bicsuiteObject, tabInfo);
    // }

    default(tabInfo: Tab): any {
        let bicsuiteObject = super.default(tabInfo);
        let record = bicsuiteObject.DATA.RECORD;
        record.PATH = tabInfo.PATH;
        record.FULLNAME = this.toolbox.namePathQuote(tabInfo.PATH + '.');
        record.NAME = '';
        record.ORIGINAL_NAME = '';
        record.PATH = tabInfo.PATH;
        record.USAGE = tabInfo.TYPE;
        record.OWNER = this.privilegesService.getDefaultGroup();
        record.INHERIT_GRANTS = "ALL";
        record.TYPE = "NAMED RESOURCE";
        record.FACTOR = "1.0",
        record.RESOURCE_STATE_PROFILE = "",
        record.TRIGGERS = {
                TABLE: []
        };
        return bicsuiteObject;
    }


    processPathChange(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        if (tabInfo.PATH == oldName) {
            tabInfo.PATH = newName;
            tabInfo.updateTooltip();
        }
        else {
            if (tabInfo.PATH.startsWith(oldName + '.')) {
                tabInfo.PATH = tabInfo.PATH.replace(oldName + '.', newName + '.');
                tabInfo.updateTooltip();
            }
        }
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.PATH) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Path Change
        if (oldName && newName) {
            if (record.PATH == oldName) {
                record.PATH = newName;
                if (initialRecord) {
                    initialRecord.PATH = record.PATH;
                }
                return Promise.resolve(true);
            }
            else {
                if (record.PATH.startsWith(oldName + '.')) {
                    record.PATH = record.PATH.replace(oldName + '.', newName + '.');
                    record.FILENAME = (tabInfo.PATH && tabInfo.PATH != '') ? this.toolbox.namePathQuote(tabInfo.PATH + "." + tabInfo.NAME) : tabInfo.NAME;
                    if (initialRecord) {
                        initialRecord.PATH = record.PATH;
                        initialRecord.FILENAME = record.FILENAME;
                    }
                    return Promise.resolve(true);
                }
            }
        }
        return Promise.resolve(false);
    }

    processJobDefinitionsResourceStates(jobDefinitions: any[], newName: string, oldName: string) {
        for (let jobDefinition of jobDefinitions) {
            if (jobDefinition.STATES) {
                let states = jobDefinition.STATES.split(',');
                let newStates = [];
                for (let state of states) {
                    state = state.trim();
                    if (state == oldName) {
                        state = newName;
                    }
                    newStates.push(state);
                }
                jobDefinition.STATES = newStates.join(',');
            }
        }
        return false;
    }

    processTriggerResourceStates(triggers: any[], newName: string, oldName: string) {
        for (let trigger of triggers) {
            if (trigger.STATES) {
                let rules = trigger.STATES.split(',');
                let newRules = [];
                for (let rule of rules) {
                    rule = rule.trim();
                    let states = rule.split("->");
                    let newStates = [];
                    for (let state of states) {
                        state = state.trim();
                        if (state == oldName) {
                            state = newName;
                        }
                        newStates.push(state);
                    }
                    newRules.push(newStates.join("->"));
                }
                trigger.STATES = newRules.join(',');
            }
        }
        return false;
    }

    processRsdRename(bicsuiteObject: any, tabInfo: Tab, inttialBicsuiteObject: any, message: any): Promise<boolean> {
        let oldName = message.OLD_NAME;
        let newName = message.NEW_NAME;
        let record = bicsuiteObject?.DATA?.RECORD;
        if (!record?.TRIGGERS) {
            return Promise.resolve(false);
        }
        let initialRecord = inttialBicsuiteObject?.DATA?.RECORD;
        // Handle Rsd Name Change
        if (oldName && newName) {
            this.processTriggerResourceStates(record.TRIGGERS.TABLE, newName, oldName);
            if (initialRecord?.TRIGGERS) {
                this.processTriggerResourceStates(initialRecord.TRIGGERS.TABLE, newName, oldName);
            }
        }
        this.processJobDefinitionsResourceStates(record.NOFORM_JOB_DEFINITIONS.TABLE, newName, oldName);
        return Promise.resolve(false);
    }

    getChangeConfig() {
        return {
            CATEGORY: {
                RENAME: {
                    METHOD: "processPathChange",
                    FIELDS: [
                    ]
                }
            },
            FOLDER: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "SCOPE"
                        },
                        {
                            TABLE: "NOFORM_JOB_DEFINITIONS",
                            NAME: "NAME"
                        },
                        {
                            TABLE: "NOFORM_JOB_DEFINITIONS",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        }
                    ]
                }
            },
            GROUP: {
                RENAME: {
                   FIELDS: [
                        {
                            NAME: "OWNER"
                        },
                        {
                            TABLE: "RESOURCES",
                            NAME: "OWNER"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_OWNER"
                        }
                    ]
                }
            },
            JOB_DEFINITION: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "SCOPE"
                        },
                        {
                            TABLE: "NOFORM_JOB_DEFINITIONS",
                            NAME: "NAME"
                        },
                        {
                            TABLE: "NOFORM_JOB_DEFINITIONS",
                            NAME: "STICKY_PARENT"
                        },
                        {
                            TABLE: "TRIGGERS",
                            NAME: "SUBMIT_NAME"
                        }
                    ]
                }
            },
            RSD: {
                RENAME: {
                    METHOD: "processRsdRename",
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "STATE"
                        }
                    ]
                }
            },
            RSM: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "NOFORM_JOB_DEFINITIONS",
                            NAME: "RESOURCE_STATE_MAPPING"
                        }
                    ]
                }
            },
            RSP: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "RESOURCE_STATE_PROFILE"
                        }
                    ]
                }
            },
            SCOPE: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "RESOURCES",
                            NAME: "SCOPE"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
