import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import critical_path_editorform from '../json/editorforms/critical-path.json';
import { Crud } from "../interfaces/crud";
import { Tab, TabMode } from "../data-structures/tab";
import { group } from "@angular/animations";
import { UntypedFormGroup } from "@angular/forms";
import { promise } from "protractor";
import { TabComponent } from "../modules/main/components/tab/tab.component";

export class CriticalPathCrud extends Crud {
    displayPath: boolean = false;

    create(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        throw new Error("Method not implemented.");
    }

    showCriticalPath(tabInfo: Tab): Promise<Object> {
        let stmt: string = "show critical path for " + tabInfo.ID + " with delta = " + tabInfo.DATA.DELTA;
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            promise.response = response;
            return promise;
        });
    }

    pct2Color(pct: number): string {
        let color = "#ff0000";
        let hexAlpha = Math.round(pct * 2.55).toString(16);
        if (hexAlpha.length == 1) {
            hexAlpha = "0" + hexAlpha;
        }
        return "#ff0000" + hexAlpha;
    }


    read(obj: any, tabInfo: Tab): Promise<Object> {
        let delta = "0";
        if (tabInfo.DATA?.DELTA) {
            delta = tabInfo.DATA.DELTA;
        }
        else {
            tabInfo.DATA = { "DELTA": "0" };
        }

        return Promise.all([
            this.showCriticalPath(tabInfo)
        ]).then((response: any) => {
            let promise = new BicsuiteResponse();

            let defaultTimeZone = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
            let defaultTimeFormat = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat");
            let minStartTime = 0;
            let maxFinalTime = 0;
            let ids : string = "";
            let sep :string = "";
            let rowNum = 0;
            for (let row of response[0].response.DATA.TABLE) {
                rowNum ++;
                row.NOFORM_ROWNUM = rowNum;
                if (this.displayPath) {
                    row.SE_NAME_DISPLAY = row.SE_NAME;
                    row.MASTER_NAME_DISPLAY = row.MASTER_NAME;
                }
                else {
                    row.SE_NAME_DISPLAY = row.SE_NAME.split('.').pop();
                    row.MASTER_NAME_DISPLAY = row.MASTER_NAME.split('.').pop();
                }
                if (row.TYPE == "JOB") {
                    if (row.START_TS) {
                        let startTime = new Date(row.START_TS).valueOf();
                        if (minStartTime == 0 || startTime < minStartTime) {
                            minStartTime = startTime;
                        }
                    }
                    if (row.FINAL_TS) {
                        let finalTime = new Date(row.FINAL_TS).valueOf();
                        if (finalTime > maxFinalTime) {
                            maxFinalTime = finalTime;
                        }
                        row.NOFORM_FINAL_TS_UNIX = new Date(row.FINAL_TS).valueOf();
                    }
                    else {
                        row.NOFORM_FINAL_TS_UNIX = new Date().valueOf();
                    }
                }
                row.NOFORM_READY_TS_UNIX = new Date(row.READY_TS).valueOf();
                row.NOFORM_FINAL_TS_UNIX = new Date(row.FINAL_TS).valueOf();
                if (row.TYPE == "JOB" && row.TOTAL_TIME) {
                    minStartTime += parseInt(row.TOTAL_TIME);
                }
                ids += sep + row.SME_ID;
                sep = ",";
            }
            let sumTotalJobTime = (maxFinalTime - minStartTime) / 1000;
            for (let row of response[0].response.DATA.TABLE) {
                if (row.RUNTIME && row.RUNTIME > 0) {
                    if (row.TYPE == "JOB") row.NOFORM_RUNTIME_STYLE = "background-color: " + this.pct2Color(row.RUNTIME * 100 / sumTotalJobTime) + ";";
                    row.RUNTIME = this.treeFunctionService.msToTime(row.RUNTIME * 1000);
                } 
                else {
                    if (row.STATE != 'FINAL') {
                        row.RUNTIME = (new Date().valueOf() - minStartTime) / 1000;
                    }
                    else {
                        row.RUNTIME = 0;
                    }
                    if (row.TYPE == "JOB") row.NOFORM_RUNTIME_STYLE = "background-color: " + this.pct2Color(row.RUNTIME * 100 / sumTotalJobTime) + ";";
                }
                if (row.TYPE == "JOB") {
                    row.NOFORM_TOTAL_TIME_STYLE = "background-color: " + this.pct2Color(row.TOTAL_TIME * 100 / sumTotalJobTime) + ";";
                    row.NOFORM_EXTRA_TIME_STYLE = "background-color: " + this.pct2Color(row.EXTRA_TIME * 100 / sumTotalJobTime) + ";";
                }
                if (row.READY_TS) {
                    row.READY_TS = this.toolbox.convertTimeStamp(new Date(row.READY_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                }
                if (row.START_TS) {
                    row.START_TS = this.toolbox.convertTimeStamp(new Date(row.START_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                }
                if (row.FINISH_TS) {
                    row.FINISH_TS = this.toolbox.convertTimeStamp(new Date(row.FINISH_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                }
                if (row.FINAL_TS) {
                    row.FINAL_TS = this.toolbox.convertTimeStamp(new Date(row.FINAL_TS).toISOString(), defaultTimeZone, defaultTimeFormat);
                }

                if (row.EXTRA_TIME && row.EXTRA_TIME > 0) {
                    row.EXTRA_ = this.treeFunctionService.msToTime(row.EXTRA_TIME * 1000);
                } 
                else {
                    row.EXTRA_TIME = "";
                }
                if (row.TOTAL_TIME && row.TOTAL_TIME > 0) {
                    row.TOTAL_TIME = this.treeFunctionService.msToTime(row.TOTAL_TIME * 1000);
                }
                else {
                    row.TOTAL_TIME = "";
                }
                if (row.SUSPEND_TIME && row.SUSPEND_TIME > 0) {
                    row.SUSPEND_TIME = this.treeFunctionService.msToTime(row.SUSPEND_TIME * 1000);
                }
                else {
                    row.SUSPEND_TIME = "";
                }
            }

            promise.response = {
                "DATA": {
                    "RECORD": {
                        "CRITICAL_PATH": {
                            "TABLE": response[0].response.DATA.TABLE
                        },
                        "NOFORM_DELTA": delta,
                        "NOFORM_IDS": ids
                    }
                }
            };
            return promise;

        });
    }

    conditionDisplayPath(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
        return this.displayPath;
    }

    toggleDisplayPath(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        this.displayPath = !this.displayPath;
        for (let row of bicsuiteObject.CRITICAL_PATH.TABLE) {
            if (this.displayPath) {
                row.SE_NAME_DISPLAY = row.SE_NAME;
                row.MASTER_NAME_DISPLAY = row.MASTER_NAME;
            }
            else {
                row.SE_NAME_DISPLAY = row.SE_NAME.split('.').pop();
                row.MASTER_NAME_DISPLAY = row.MASTER_NAME.split('.').pop();
            }
        }
        return Promise.resolve({});
    }

    
    openSme(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this._openSme(bicsuiteObject, tabInfo, 'SE_NAME', 'SME_ID', arg, arg1);
    }

    openMasterSme(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this._openSme(bicsuiteObject, tabInfo, 'MASTER_NAME', 'MASTER_ID', arg, arg1);
    }

    _openSme(bicsuiteObject: any, tabInfo: any, seNameField: string, smeIdField: string, arg?: any, arg1?: any): Promise<any> {
        let name = bicsuiteObject[seNameField];
        let path = '';
        let p = name.split('.');
        if (p.length > 1) {
            name = p.pop();
            path = p.join('.');
        }
        let tab = new Tab(name, bicsuiteObject[smeIdField], 'SUBMITTED ENTITY', 'submitted_entity', TabMode.EDIT);
        tab.PATH = path;
        this.eventEmitterService.createTab(tab, true);
        return Promise.resolve({});
    }

    openSmesInHierarchy(bicsuiteObject: any, tabInfo: Tab, editorForm: any): Promise<Object> {
        return this.openSmeInHierarchyById(bicsuiteObject.NOFORM_IDS);
    }

    triggerDeltaChange(bicsuiteObject: any, tabInfo: any, form?: any, arg1?: any): Promise<any> {
        tabInfo.DATA = { "DELTA": bicsuiteObject.NOFORM_DELTA }
        return Promise.resolve({});
    }

    editorform() {
        return critical_path_editorform;
    }
}
