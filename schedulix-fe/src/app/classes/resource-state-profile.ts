import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import rsp_editorform from '../json/editorforms/rsp.json';
import { Tab } from "../data-structures/tab";
import { Crud } from "../interfaces/crud";

export class ResourceStateProfileCrud extends Crud {

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create resource state profile " + "'" + obj.NAME + "'";
        let states = obj.STATES;
        let initialState = obj.INITIAL_STATE;
        // console.log(obj.INITIAL_STATE)
        stmt += "with initial state = '" + initialState + "'";
        if (states != undefined && states != null) {
            stmt += ", states = (";
            for (let i = 0; i < states.TABLE.length; i++) {

                stmt += "'" + states.TABLE[i].RSD_NAME + "'";
                stmt += (i == states.TABLE.length - 1) ? "" : ",";
            }
            stmt += ")";
        }
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSP", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "SHOW RESOURCE STATE PROFILE '" + tabInfo.NAME + "'";
        // execute returns a promise which can be used to fetch the response.
        return this.commandApi.execute(stmt).then((response: any) => {

            tabInfo.ID = response.DATA.RECORD.ID;
            let promise = new BicsuiteResponse();
            promise.response = response;
            if (promise.response.DATA.hasOwnProperty("RECORD")) {
                promise.response.DATA.RECORD.crudIsReadOnly = !this.privilegesService.hasManagePriv("resource state profile");
                tabInfo.ISREADONLY = !this.privilegesService.hasManagePriv("resource state profile");
            }
            return promise;
        });
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "begin multicommand\n";
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        if (obj.NAME != obj.ORIGINAL_NAME) {
            // call rename
            stmt += "rename " + "resource state profile" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';";
        }
        let states = obj.STATES;
        let initialState = obj.INITIAL_STATE;
        // let rangeLength = obj.MAPPINGS.TABLE.length;
        stmt += " alter resource state profile " + "'" + obj.NAME + "'";
        stmt += " with initial state = '" + initialState + "'";
        if (states != undefined && states != null) {
            stmt += ", states = (";
            for (let i = 0; i < states.TABLE.length; i++) {

                stmt += "'" + states.TABLE[i].RSD_NAME + "'";
                stmt += (i == states.TABLE.length - 1) ? "" : ",";
            }
        }
        stmt += "); end multicommand;";
        // console.log(stmt);
        return this.commandApi.execute(stmt).then((response: any) => {
            if (obj.NAME != obj.ORIGINAL_NAME) {
                this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSP", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                obj.F_REFRESH_NAVIGATOR = true;
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{Resource State Profile}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt: string = "drop resource state profile '" + obj.ORIGINAL_NAME + "'";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "RSP", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    openRsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition(bicsuiteObject.RSD_NAME);
    }

    openInitialRsdName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openResourceStateDefinition(bicsuiteObject.INITIAL_STATE);
    }

    editorform() {
        return rsp_editorform;

    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["STATES"] = {
            TABLE: [{
                RSD_NAME: "",
                NOFORM_DISABLE_AUTOCHOOSE: "true"
            }],
            DESC: [
                "RSD_NAME",
                "NOFORM_DISABLE_AUTOCHOOSE"
            ]
        };
        return bicsuiteObj;
    }

    getChangeConfig() {
        return {
            RSD: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "INITIAL_STATE"
                        },
                        {
                            TABLE: "STATES",
                            NAME: "RSD_NAME"
                        }
                    ]
                }
            },
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            }
        }
    }

}
