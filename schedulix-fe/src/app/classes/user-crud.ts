import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import user_editorform from '../json/editorforms/user.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";
import { group } from "@angular/animations";

export class UserCrud extends Crud {

    createWith(mode: string, obj: any, tabInfo: Tab) {
        let defaultGroup: string = "PUBLIC";
        let isAdmin: boolean = this.privilegesService.isAdmin();
        let withClause: string = " with\n";
        let sep: string = "";

        if (isAdmin || (this.privilegesService.hasManagePriv("group") && this.privilegesService.hasManagePriv("user"))) {
            withClause += " groups = (";
            for (let i = 0; i < obj.GROUPS.TABLE.length; i++) {
                let g = obj.GROUPS.TABLE[i];
                // console.log(g)
                withClause += sep + "'" + g.NAME + "'";
                if (g.DEFAULT == "true" || g.DEFAULT == true) {
                    defaultGroup = g.NAME;
                }
                sep = ", ";
            }
            if (sep == "") {
                withClause += "PUBLIC";
                sep = ", ";
            }
            withClause += ")";
        } else {
            for (let i = 0; i < obj.GROUPS.TABLE.length; i++) {
                if (obj.GROUPS.TABLE[i].DEFAULT == "true" || obj.GROUPS.TABLE[i].DEFAULT == true) {
                    defaultGroup = obj.GROUPS.TABLE[i].NAME;
                }
            }
        }
        if (this.conditionIsOwnUserOrManager({}, obj, tabInfo)) {
            withClause += sep + "default group = '" + defaultGroup + "'";
            sep = ", ";
        }
        if (this.privilegesService.isAdmin() || this.privilegesService.hasManagePriv("user")) {
            if (obj.EQUIVALENT_USERS.TABLE.length == 0) {
                withClause += sep + "equivalent = none";
            }
            else {
                withClause += sep + " equivalent = (";
                let eSep = "";
                for (let e of obj.EQUIVALENT_USERS.TABLE) {
                    withClause += eSep + "'" + e.EQUIVALENT_USER + "'";
                    eSep = ", ";
                }
                withClause += ")";
            }
            sep = ", ";
        }
        if (mode == "CREATE" || obj.PASSWORD1 != "") {
            let pwd = obj.PASSWORD1.replace("\\", "\\\\").replace("'", "\\'");
            withClause += sep + "password = '" + pwd + "'";
        }
        if (isAdmin) {
            if (obj.IS_ENABLED == "true" || obj.IS_ENABLED == true) {
                withClause += sep + "enable";
            }
            else {
                withClause += sep + "disable";
            }
        }
        return withClause;
    }

    create(obj: any, tabInfo: Tab): Promise<Object> {
        obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
        let stmt = "create user " + "'" + obj.NAME + "' " + this.createWith("CREATE", obj, tabInfo) + ";";
        return this.commandApi.execute(stmt).then((response: any) => {
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "USER", OP : "CREATE", NAME : obj.NAME});
            return response;
        });
    }

    read(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt: string = "show user '" + tabInfo.NAME + "'";
        return this.commandApi.execute(stmt).then((response: any) => {
            let promise = new BicsuiteResponse();
            let defaultGroup: string = response.DATA.RECORD.DEFAULT_GROUP;
            for (let o of response.DATA.RECORD.GROUPS.TABLE) {
                o.NAME == defaultGroup ? o.DEFAULT = "true" : o.DEFAULT = "false";
                o.USERNAME = response.DATA.RECORD.NAME;
            }
            response.DATA.RECORD.PASSWORD1 = '';
            response.DATA.RECORD.PASSWORD2 = '';
            // response.DATA.RECORD.PRIVATE_PROPERTIES = {}
            promise.response = response;
            // if own user
            if (response.DATA.RECORD.NAME == this.mainInformationService.getUser().username) {
                let propertiesObject = {};
                for (let parameter of response.DATA.RECORD.PARAMETERS.TABLE) {
                    // console.log(parameter)
                    if (parameter.NAME == 'PRIVATE_PROPERTIES') {
                        try {
                            propertiesObject = JSON.parse(parameter.VALUE);
                        } catch (e) {
                            console.warn(e);
                        }
                    }
                }
                this.setDefaultProperties(propertiesObject);
                // console.log(propertiesObject)
                // ...flatten propertiesobject to bicsuiteobject
                // objectB will override existing properties, with the same name,
                // from the decomposition of objectA
                response.DATA.RECORD = { ...propertiesObject, ...response.DATA.RECORD };
                // response.DATA.RECORD = propertiesObject;
                this.mainInformationService.updateUserInformation(response.DATA.RECORD);
            }
            return promise;
        });
    }

    setDefaultProperties(properties: any) {
        // console.log(properties);
        let propertyConfig = [
            { "PROPERTY_NAME": "NAME_UPPERCASING", "CONFIG_NAME" : "nameUppercasing" },
            { "PROPERTY_NAME": "PRIVATE_EDITION" },
            { "PROPERTY_NAME": "PRIVATE_TIME_ZONE", "CONFIG_NAME" : "defaultTimeZone" },
            { "PROPERTY_NAME": "THEME", "CONFIG_NAME" : "defaultTheme" },
            { "PROPERTY_NAME": "PRIVATE_TIME_STAMP_FORMAT", "CONFIG_NAME" : "defaultTimeFormat" },
            { "PROPERTY_NAME": "ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT", "CONFIG_NAME" : "enableBatchInFolderConvention"},
            { "PROPERTY_NAME": "DEFAULT_ENVIRONMENT" },
            { "PROPERTY_NAME": "DEFAULT_EXIT_STATE_PROFILE" },
            { "PROPERTY_NAME": "DEFAULT_ERROR_LOGFILE", "CONFIG_NAME" : "defaultErrorLogfile" },
            { "PROPERTY_NAME": "DEFAULT_LOGFILE", "CONFIG_NAME" : "defaultLogfile" }
        ];
        for (let p of propertyConfig) {
            if (!properties[p.PROPERTY_NAME]) {
                if (p.CONFIG_NAME && p.CONFIG_NAME != "") {
                    properties[p.PROPERTY_NAME] = this.customPropertiesService.getCustomObjectProperty (p.CONFIG_NAME);
                }
            }
        }
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        if (true || obj.PRIVS.includes('E')) {
            let stmt = "begin multicommand\n";
            obj.NAME = this.convertIdentifier(obj.NAME, obj.ORIGINAL_NAME);
            if (obj.NAME != obj.ORIGINAL_NAME && obj.ORIGINAL_NAME != 'SYSTEM') {
                stmt += "rename " + "user" + " '" + obj.ORIGINAL_NAME + "' TO '" + obj.NAME + "';\n";
            }
            let withClause = this.createWith("UPDATE", obj, tabInfo);
            if (withClause != " with\n") {
                stmt += "alter user " + "'" + obj.NAME + "' " + withClause + ";\n";
            }
            if (stmt != "begin multicommand\n") {
                console.log(stmt)
                stmt += "end multicommand;";
                return this.commandApi.execute(stmt).then((response: any) => {

                    if (obj.NAME != obj.ORIGINAL_NAME && obj.ORIGINAL_NAME != 'SYSTEM') {
                        this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "USER", OP : "RENAME", OLD_NAME : obj.ORIGINAL_NAME, NEW_NAME : obj.NAME});
                        obj.F_REFRESH_NAVIGATOR = true;
                    }

                    if (obj.ORIGINAL_NAME == this.mainInformationService.getUser().username && response.hasOwnProperty("FEEDBACK")) {

                        if (obj.NAME != obj.ORIGINAL_NAME && obj.ORIGINAL_NAME != 'SYSTEM') {
                            this.mainInformationService.updateUsername(obj.NAME);
                        }

                        if (obj.PASSWORD1 != "") {
                            this.mainInformationService.updatePassword(obj.PASSWORD1);
                        }

                        return this.saveUserProperties(obj, response);
                    } else {
                        return response;
                    }
                });
            }
            else {
                return this.saveUserProperties(obj, {});
            }
        }
        else {
            return this.saveUserProperties(obj, {});
        }
    }

    // build object from flat bicsuitobject  which is saved in the userparameters                    
    private saveUserProperties(obj: any, response: any) {
        let propertiesObject: any = {};
        propertiesObject.NAME_UPPERCASING = obj.NAME_UPPERCASING;
        if (this.getConfig("bicsuiteDeveloper", "false") == "true") {
            propertiesObject.PRIVATE_EDITION = obj.PRIVATE_EDITION;
        }
        propertiesObject.PRIVATE_TIME_ZONE = obj.PRIVATE_TIME_ZONE;
        propertiesObject.THEME = obj.THEME;
        propertiesObject.PRIVATE_TIME_STAMP_FORMAT = obj.PRIVATE_TIME_STAMP_FORMAT;
        propertiesObject.ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT = obj.ENABLE_BATCH_IN_FOLDER_CONVENTION_SUPPORT;
        propertiesObject.DEFAULT_ENVIRONMENT = obj.DEFAULT_ENVIRONMENT;
        if (propertiesObject.DEFAULT_ENVIRONMENT == "NONE") {
            propertiesObject.DEFAULT_ENVIRONMENT = "";
        }
        propertiesObject.DEFAULT_EXIT_STATE_PROFILE = obj.DEFAULT_EXIT_STATE_PROFILE;
        if (propertiesObject.DEFAULT_EXIT_STATE_PROFILE == "NONE") {
            propertiesObject.DEFAULT_EXIT_STATE_PROFILE = "";
        }
        propertiesObject.DEFAULT_LOGFILE = obj.DEFAULT_LOGFILE;
        propertiesObject.DEFAULT_ERROR_LOGFILE = obj.DEFAULT_ERROR_LOGFILE;

        // console.log(propertiesObject)
        return this.mainInformationService.setUserParameter('PRIVATE_PROPERTIES', propertiesObject, this.mainInformationService.getUser().username).then((result) => {
            this.mainInformationService.reloadUser();
            if (!response.FREEDBACK) {
                response.FEEDBACK = "User Private User Properties updated";
            }
            return response;
        });
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        return this.createConfirmDialog([], "drop_confirm{User}{" + obj.NAME + "}", tabInfo, "", undefined).afterClosed().toPromise().then((result: any) => {
            if (result != undefined || result != null && result) {
                let stmt = "drop user " + "'" + obj.NAME + "';";
                return this.commandApi.execute(stmt).then((response: any) => {
                    this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : "USER", OP : "DROP", NAME : obj.ORIGINAL_NAME});
                    return response;
                });
            }
            return {};
        });
    }

    openGroupName(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
        return this.openGroup(bicsuiteObject.NAME);
    }

    editorform() {
        return user_editorform;
    }

    default(tabInfo: Tab): any {
        let bicsuiteObj = super.default(tabInfo);
        bicsuiteObj.DATA.RECORD["GROUPS"] = {
            TABLE: [{ "PRIVS": "", "ID": "", "NAME": "PUBLIC", "DEFAULT": "true" }]
        };
        return bicsuiteObj;
    }

    filterGroupChooser(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        let result = [];
        for (let row of bicsuiteObject) {
            if (row.NAME == 'PUBLIC') continue;
            if (row.NAME == 'ADMIN' && tabInfo.NAME == 'SYSTEM') continue;
            result.push(row);
        }
        return result;
    }

    conditionCanDropGroup(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.NAME != "PUBLIC" && (bicsuiteObject.NAME != "ADMIN" || bicsuiteObject.USERNAME != 'SYSTEM') &&
        this.conditionIsAdminOrGroupManager(editorformField, bicsuiteObject, tabInfo);
    }

    conditionIsSystem(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.NAME == "SYSTEM";
    }

    conditionIsSystemOrNew(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return bicsuiteObject.NAME == "SYSTEM" || this.conditionIsNew(null, bicsuiteObject, tabInfo);
    }

    filterEquivalentUserChooser(editorformField: any, bicsuiteObject: any, tabInfo: Tab): any {
        let result = [];
        for (let row of bicsuiteObject) {
            if (row.NAME == 'PUBLIC') continue;
            if (row.NAME == 'ADMIN' && tabInfo.NAME == 'SYSTEM') continue;
            result.push(row);
        }
        return result;
    }

    // conditionCanSetDefaultGroup(editorformField: any, bicsuiteObject: any, tabInfo: Tab, table: any): boolean {
    //     if (bicsuiteObject.USERNAME == this.mainInformationService.getUser().username ||
    //     this.conditionIsAdminOrGroupManager(editorformField, bicsuiteObject, tabInfo)) {
    //         return true;
    //     }
    //     return false;
    // }

    conditionIsOwnUser(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        let propertyName = "NAME";
        if (editorformField.NAME == "DEFAULT") {
            propertyName = "USERNAME";
        }
        return (bicsuiteObject[propertyName] == this.mainInformationService.getUser().username);
    }

    conditionIsOwnUserOrManager(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.conditionIsOwnUser(editorformField, bicsuiteObject, tabInfo) ||
            this.privilegesService.hasManagePriv("user") || this.privilegesService.isAdmin();
    }

    conditionIsAdminOrGroupManager(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.privilegesService.isAdmin() || this.privilegesService.hasManagePriv("group");
    }

    conditionIsAdminOrUserManager(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.privilegesService.isAdmin() || this.privilegesService.hasManagePriv("user");
    }

    conditionBicsuiteDeveloper(editorformField: any, bicsuiteObject: any, tabInfo: Tab): boolean {
        return this.getConfig("bicsuiteDeveloper", "false") == "true";
    }

    getChangeConfig() {
        return {
            USER: {
                RENAME: {
                    FIELDS: [
                        {
                            NAME: "CREATOR"
                        },
                        {
                            NAME: "CHANGER"
                        },
                    ]
                }
            },
            GROUP: {
                RENAME: {
                    FIELDS: [
                        {
                            TABLE: "GROUPS",
                            NAME: "NAME"
                        },
                    ]
                }
            }
        }
    }

}
