import { BicsuiteResponse } from "../data-structures/bicsuite-response";
import rsm_editorform from '../json/editorforms/rsm.json';
import { Crud } from "../interfaces/crud";
import { Tab } from "../data-structures/tab";

export class CommentCrud extends Crud {
    create(obj: any, tabInfo: Tab): Promise<Object> {
        let comments = obj.COMMENT.TABLE;
        let stmt: string = "";
        stmt += "create or alter comment on ";

        let type : string = ( obj.TYPE =="JOB" || obj.TYPE == "BATCH" || obj.TYPE == "MILESTONE" ? "job definition" : (obj.USAGE == "CATEGORY" ? "named resource" : tabInfo.TYPE.toLowerCase()));

        if (tabInfo.TYPE == "RESOURCE") {
            stmt += type + " " + tabInfo.ID;
        }
        else if (obj.PATH === undefined || obj.PATH === "") {
            if (tabInfo != undefined) {
                if (["FOLDER","CATEGORY","SCOPE"].includes(tabInfo.TYPE) && obj.PATH == "") {
                    stmt += type + " " + obj.ORIGINAL_NAME;
                }
                else {
                    stmt += type + " '" + obj.ORIGINAL_NAME + "'";
                }
            }
        } else {
            stmt += type + " " + this.toolbox.namePathQuote(obj.PATH + "." + obj.ORIGINAL_NAME);
        }

        stmt += " with "

        // Add comments
        let segments = [];
        let filteredComments = [];
        for (let i = 0; i < comments.length; i++) {
            let tag = comments[i].TAG ? comments[i].TAG.replaceAll("'", "\\'") : "";
            let commentDesc = comments[i].DESCRIPTION ? comments[i].DESCRIPTION.replaceAll("'", "\\'") : "";
            if (tag == "" && commentDesc == "") {
                continue;
            }
            filteredComments.push(comments[i]);
            let segment = "";
            segment += "tag = " + (tag != '' && tag != null ? "'" + tag + "'," : "none,");
            segment += " text = '" +  commentDesc + "'";
            segments.push(segment);
        }
        obj.COMMENT.TABLE = filteredComments;
        stmt += segments.join(", ");

        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }
    
    read(obj: any, tabInfo: Tab): Promise<Object> {
        return Promise.resolve<Object>({});
    }

    update(obj: any, tabInfo: Tab): Promise<Object> {
        return this.create(obj, tabInfo);
    }

    drop(obj: any, tabInfo: Tab): Promise<Object> {
        let stmt = "drop comment on ";
        if (tabInfo.TYPE == "RESOURCE") {
            stmt += "resource " + tabInfo.ID;
        }
        else  if (obj.PATH === undefined) {
            if (tabInfo != undefined) {
                stmt += tabInfo.TYPE + " '" + obj.ORIGINAL_NAME + "'";
            }
        } else {
            // Special Cases
            if (obj.TYPE === "JOB") {
                stmt += "job definition " + this.toolbox.namePathQuote(obj.PATH + "." + obj.ORIGINAL_NAME);
            } else {
                stmt += obj.TYPE.toLowerCase() + " " + this.toolbox.namePathQuote(obj.PATH + "." + obj.ORIGINAL_NAME);
            }
        }

        return this.commandApi.execute(stmt).then((response: any) => {
            return response;
        });
    }

    editorform() {
        return rsm_editorform;
    }
}