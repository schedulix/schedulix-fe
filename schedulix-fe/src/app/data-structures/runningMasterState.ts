

export class RunningMasterState {
    settingsObject: rmjSettingsObject = new rmjSettingsObject();
    filterObject: rmjFilterObject = new rmjFilterObject();
}

export class rmjSettingsObject {
    TYPE: string = "";
    SORT_ORDER: string = "DESCENDING";
    AUTO_REFRESH: number = 0;
    DETAIL_BOOKMARK: string = "";
    PARAMETERS: rmjParameters = {
        TABLE: [],
        DESC: [
            "PARAMETER_NAME",
            "ALIGN",
            "FORMAT",
            "LABEL",
            "COLOR"
        ]
    }
}

export class rmjParameters {
    TABLE: any[] = [];
    DESC: string[] = [
        "PARAMETER_NAME",
        "ALIGN",
        "FORMAT",
        "LABEL",
        "COLOR"
    ]
}

export class rmjFilterObject {

    HISTORY_FROM: string = '1';
    HISTORY_FROM_UNIT: string = 'DAYS';
    HISTORY_TO: string = '';
    HISTORY_TO_UNIT: string = 'DAYS';
    HISTORY_FUTURE: string = '';
    HISTORY_FUTURE_UNIT: string = 'DAYS';
    ENABLE_ONLY: string = 'false';
    JOB_ID: string = '';
    ENABLED_ONLY: string = 'NO';
    CONDITION_MODE: string = 'NONE';
    CONDITION: string = '';

    FILTER_ESD: any  = {
        TABLE: [],
        DESC: [
            "ESD"
        ]
    };

    FILTER_JOB_STATES: any  = {
        TABLE: [],
        DESC: [
            "JOB_STATE"
        ]
    };

    NAME_PATTERN: any  = {
        TABLE: [],
        DESC: [
            "NAME"
        ]
    }
}
