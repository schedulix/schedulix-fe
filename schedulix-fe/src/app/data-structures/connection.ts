export class Connection {
  name: string = '';
  host: string = '';
  user: string = '';
  password: string = '';
  port: string = '';
  serverName: string = '';
}

export class Credentials {
  username: string = '';
  password: string = '';
  host: string = '';
  port: string = '';
  serverName: string = '';
  connectionName: string = '';
}

export class SdmsServer {
  NAME: string = 'DEFAULT';
  HOST: string = 'localhost';
  PORT: string = '2506';
  VERSION: string = 'BASIC';
  HEADER_TAG: string = '';
  HEADER_TAG_COLOR: string = '';
  CACHE: string = 'Y';
  TIMEOUT: string = '60';
  SSL: string = 'N';
  SSO: boolean = false;
}
