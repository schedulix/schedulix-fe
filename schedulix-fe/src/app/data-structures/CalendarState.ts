

export class CalendarState {
    filterObject: CalendarFilterObject = new CalendarFilterObject();
}

export class CalendarFilterObject {
    NAME_PATTERN: any  = {
        TABLE: [],
        DESC: [
            "NAME"
        ]
    }
    CONDITION_MODE: string = 'NONE';
    CONDITION: string = '';
}