
// "succeed" is true when a login was successful. Otherwise false
// "response" gives further information about reponse (errors/result) 
export class AuthResponse {
    succeed: boolean = false;
    response: any;
  }