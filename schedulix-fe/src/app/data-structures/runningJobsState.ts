

export class SearchJobState {
    settingsObject: SearchSettingsObject = new SearchSettingsObject();
    filterObject: SearchFilterObject = new SearchFilterObject();
}

export class SearchSettingsObject {
    TYPE: string = "";
    SORT_ORDER: string = "DESCENDING";
    AUTO_REFRESH: number = 0;
    ENABLE_BULK: string = 'true';
    PARAMETERS: SearchParameters = {
        TABLE: [],
        DESC: [
            "PARAMETER_NAME",
            "ALIGN",
            "FORMAT",
            "LABEL",
            "COLOR"
        ]
    }
}

export class SearchParameters {
    TABLE: any[] = [];
    DESC: string[] = [
        "PARAMETER_NAME",
        "ALIGN",
        "FORMAT",
        "LABEL",
        "COLOR"
    ]
}

export class SearchFilterObject {
    
    MASTER_CHECK: string = 'false';
    MASTER_ID: string = '';
    ENABLE_ONLY: string = 'false';
    HISTORY_FROM: string = '1';
    HISTORY_FROM_UNIT: string = 'DAYS';
    HISTORY_TO: string = '';
    HISTORY_TO_UNIT: string = 'DAYS';
    HISTORY_FUTURE: string = '';
    HISTORY_FUTURE_UNIT: string = 'DAYS';
    
    JOB_ID: string = '';
    CONDITION_MODE: string = 'OR';
    CONDITION: string = 'master';

    FILTER_ESD: any  = {
        TABLE: [],
        DESC: [
            "ESD"
        ]
    };

    FILTER_JOB_STATES: any  = {
        TABLE: [
            { "JOB_STATE" : "RUNNABLE" },
            { "JOB_STATE" : "STARTING" },
            { "JOB_STATE" : "STARTED" },
            { "JOB_STATE" : "RUNNING" },
            { "JOB_STATE" : "TO_KILL" },
            { "JOB_STATE" : "KILLED" },
            { "JOB_STATE" : "BROKEN_ACTIVE" },
            { "JOB_STATE" : "BROKEN_FINISHED" },
            { "JOB_STATE" : "UNREACHABLE" },
            { "JOB_STATE" : "SYNCHRONIZE_WAIT" },
            { "JOB_STATE" : "ERROR" },
            { "JOB_STATE" : "RESTARTABLE" },
            { "JOB_STATE" : "PENDING" },
            { "JOB_STATE" : "WARNING" }
        ],
        DESC: [
            "JOB_STATE"
        ]
    };

    NAME_PATTERN: any  = {
        TABLE: [],
        DESC: [
            "NAME"
        ]
    }
}