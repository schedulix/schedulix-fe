

export class RunningDetailState {
    settingsObject: detailsettingsObject = new detailsettingsObject();
    filterObject: detailFilterObject = new detailFilterObject();
}

export class detailsettingsObject {
    TYPE: string = "";
    SORT_ORDER: string = "ASCENDING";
    AUTO_REFRESH: number = 0;
    PARAMETERS: rmjParameters = {
        TABLE: [],
        DESC: [
            "PARAMETER_NAME",
            "ALIGN",
            "FORMAT",
            "LABEL",
            "COLOR"
        ]
    }
}

export class rmjParameters {
    TABLE: any[] = [];
    DESC: string[] = [
        "PARAMETER_NAME",
        "ALIGN",
        "FORMAT",
        "LABEL",
        "COLOR"
    ]
}

export class detailFilterObject {
    

    JOB_ID: string = '';
    ENABLE_ONLY: string = 'NO';
    CONDITION_MODE: string = 'NONE';
    CONDITION: string = '';

    FILTER_ESD: any  = {
        TABLE: [],
        DESC: [
            "ESD"
        ]
    };

    FILTER_JOB_STATES: any  = {
        TABLE: [
            { "JOB_STATE" : "RUNNABLE" },
            { "JOB_STATE" : "STARTING" },
            { "JOB_STATE" : "STARTED" },
            { "JOB_STATE" : "RUNNING" },
            { "JOB_STATE" : "TO_KILL" },
            { "JOB_STATE" : "KILLED" },
            { "JOB_STATE" : "BROKEN_ACTIVE" },
            { "JOB_STATE" : "BROKEN_FINISHED" },
            { "JOB_STATE" : "UNREACHABLE" },
            { "JOB_STATE" : "SYNCHRONIZE_WAIT" },
            { "JOB_STATE" : "ERROR" },
            { "JOB_STATE" : "RESTARTABLE" },
            { "JOB_STATE" : "PENDING" },
            { "JOB_STATE" : "WARNING" }
        ],
        DESC: [
            "JOB_STATE"
        ]
    };

    NAME_PATTERN: any  = {
        TABLE: [],
        DESC: [
            "NAME"
        ]
    }
}