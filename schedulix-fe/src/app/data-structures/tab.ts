import { Bookmark } from "./bookmark";

export class Tab {
    FROM_CONTEXT_MENU: boolean = false;
    constructor(name: string, id: string, type: string, shortType: string, tabmode: TabMode) {
        this.NAME = name;
        this.ID = id;
        this.TYPE = type;
        this.shortType = shortType;
        this.MODE = tabmode;
    }
    NAME: string = '';
    ID: string = '';
    TYPE: string = '';
    shortType: string = '';
    PATH: string = ''; // Tree Path, empty if tree does not exist.
    MODE: TabMode = TabMode.NEW;
    PRIVS: string = '';
    COLOR: string = '';
    NUMERIC_ID: string = '';
    OWNER: string = '';
    BOOKMARK: Bookmark | undefined;
    AUTORELOAD: boolean = false;
    DATA: any = '';
    ISREADONLY: boolean = false;
    VERSIONED: boolean = false;
    TABSTATE: TabState = new TabState();
    TABTOOLTIP: string = '';
    SE_OWNER: string = '';
    SE_TYPE: string = '';
    IS_ACTIVE?: boolean;
    IS_INITIALIZED?: boolean;
    IS_DIRTY?: boolean;

    getCopy(): Tab {
      let copy : Tab = new Tab(this.NAME, this.ID, this.TYPE, this.shortType, this.MODE);
      Object.assign (copy, JSON.parse(JSON.stringify(this)));
      return copy;
    }

    updateTooltip() {
      this.TABTOOLTIP = this.getTooltip();
    }

    getTooltip() : string {
      let tooltipName = (this.PATH ? this.PATH + '.' + this.NAME : this.NAME);
        if (this.OWNER) {
          tooltipName += " in " + this.OWNER;
        }
        let type = this.TYPE;
        if (["STATIC", "SYSTEM", "SYNCHRONIZING", "POOL"].includes(type)) {
          type = "NAMED RESOURCE"
        }
        switch (type) {
          case "MONITOR MASTER" :
          case "MONITOR JOBS" :
            return type + " " + (this.BOOKMARK ? this.BOOKMARK.bookmark : "DEFAULT");
            break;
          case "MONITOR DETAIL" :
            return type + " " + (this.BOOKMARK ? this.BOOKMARK.bookmark : "DEFAULT") + " " + tooltipName + " [" + this.ID + "]";
            break;
          case "CRITICAL PATH" :
            return "CRITICAL PATH " + tooltipName + " [" + this.ID + "]";
            break;
          case "SUBMITTED ENTITY":
            return type + " " + tooltipName + " [" + this.ID + "]";
            break;
          case "SCHEDULES":
            return "SCHEDULES of " + this.OWNER;
            break;
          case "APPROVAL":
          case "SYSTEM INFORMATION":
          case "SHELL":
          case "BOOKMARKING":
          case "CALENDAR":
            return "";
            break;
          default:
            return type + " " + tooltipName;
        }
      }
    }

export enum TabMode {
    NEW,
    EDIT,
    MONITOR_MASTER,
    MONITOR_JOBS,
    MONITOR_DETAIL,
    CRITICAL_PATH,
    SHELL,
    HIERARCHY,
    CALENDAR
}

export class TabState {
  accordeonOpenStates: any = {};
  scrollPosition: number = 0;
  containerMinHeight: number = 0;
  fieldStates: any = {};
}
