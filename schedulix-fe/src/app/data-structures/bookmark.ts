export class Bookmark {
 
    bookmark!: string;
    type!: string;
    scope: string = '';
    value: any;
    autostart: string = 'NO';
    startmode: string = 'NAVIGATE';

    constructor(name: string, type: string){
        this.bookmark = name;
        this.type = type;

    }
}


// export enum BookmarkTypes {
//     DETAIL = 'DETAIL',
//     FOLDER = 'FOLDER',
//     MASTER = 'MASTER',
//     SEARCH = 'SEARCH',
//     SUBMIT = 'SUBMIT'
// }
