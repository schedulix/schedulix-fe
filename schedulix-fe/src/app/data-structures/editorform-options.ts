export class EditorformOptions {
    PRIVS: string = "";
    formArrayIdentifier?: string;
    rowIndex: number = -1;
    flags: EditorformFlags = new EditorformFlags();
    render: EditorformRender = new EditorformRender();
}


export class EditorformRender {
    simple: boolean = false; // simple or material styling.
    disabled: boolean = false; // whether to disable the control or not
    invisible: boolean = false; // Hide control or container but keep control in DOM.
    hidden: boolean = false; // Hide control or container (control will not be rendered or created)
}

// TODO Comment each flag.
export class EditorformFlags {
    AVAILABLE: boolean = true;
    HIDE_CONDITION: boolean = false;
    CONDITION: boolean = false;
    HAS_PRIVS: boolean = false;
    CSS_CLASS = "";
    CSS: string = "";
    SVG_ICON?: string;
    STYLE_ATTR: string = ""; 
    // TODO REFACTOR STRING TYPES TO EDITORFORM OPTIONS.
}
