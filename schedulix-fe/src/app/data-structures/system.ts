import { SdmsServer } from "./connection";

export class System {
    systemname: string = '';
    version: string = '';
    maxEdition: string = '';
    DumpLangLevel: string = '';
    maxLevel: string = '';
    server: SdmsServer = new SdmsServer();
}
