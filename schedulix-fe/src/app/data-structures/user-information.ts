export class UserInformation {
    username: string = '';
    groups: string[] = []; // for admin etc...
    managePrivs: string[] = [];
    userParameter: any = [];
    privs: string = "";
    defaultGroup: string = '';
}
