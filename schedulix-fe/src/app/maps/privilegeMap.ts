

// all privileges: KPDEMO(OCRESDILUWPMKA)SVGR
export const privilegeMap = new Map<string, string>([
    ["D", "DROP"],
    ["E", "EDIT"],
    ["M", "MONITOR"],
    ["O", "OPERATE"],
    ["P", "CREATE_PARENT_CONTENT"],
    ["S", "SUBMIT"],
    ["V", "VIEW"],
    ["K", "CREATE"],
    ["G", "GRANT"],
    ["R", "RESOURCE"],
    ["U", "USE"],
    ["X", "EXECUTE"],
    ["C", "CREATE_CONTENT"],

    // operate privs
    ["c", "CANCEL"],
    ["r", "RERUN"],
    ["e", "ENABLE"],
    ["s", "SET_STATE"],
    ["d", "IGNORE_DEPENDENCY"],
    ["i", "IGNORE_RESOURCE"],
    ["l", "CLONE"],
    ["u", "SUSPEND"],
    ["w", "CLEAR_WARNING"],
    ["p", "PRIORITY"],
    ["m", "EDIT_PARAMETER"],
    ["k", "KILL"],
    ["a", "APPROVE"]
    //,
    // ["j", "SET_JOB_STATE"]
]);

export const privilegeMapByObjectType = new Map<string, Map<string, string>>([
    ['all', privilegeMap],
    ['JOB DEFINITION', new Map<string, string>([
        ["V", "VIEW"],
        ["E", "EDIT"],
        ["D", "DROP"],
        ["S", "SUBMIT"],
        ["M", "MONITOR"],
        ["O", "OPERATE"],
         // operate privs
        ["r", "RERUN"],
        ["u", "SUSPEND"],
        ["p", "PRIORITY"],
        ["c", "CANCEL"],
        ["k", "KILL"],
        ["e", "ENABLE"],
        ["w", "CLEAR_WARNING"],
        ["s", "SET_STATE"],
        ["d", "IGNORE_DEPENDENCY"],
        ["i", "IGNORE_RESOURCE"],
        ["l", "CLONE"],
        ["m", "EDIT_PARAMETER"],
        ["a", "APPROVE"]
        // ,
        // ["j", "SET_JOB_STATE"]
    ])],
    ['FOLDER', new Map<string, string>([
        ["V", "VIEW"],
        ["E", "EDIT"],
        ["D", "DROP"],
        ["C", "CREATE_CONTENT"],
        ["R", "RESOURCE"],
        ["S", "SUBMIT"],
        ["M", "MONITOR"],
        ["O", "OPERATE"],
         // operate privs
         ["r", "RERUN"],
         ["u", "SUSPEND"],
         ["p", "PRIORITY"],
         ["c", "CANCEL"],
         ["k", "KILL"],
         ["e", "ENABLE"],
         ["w", "CLEAR_WARNING"],
         ["s", "SET_STATE"],
         ["d", "IGNORE_DEPENDENCY"],
         ["i", "IGNORE_RESOURCE"],
         ["l", "CLONE"],
         ["m", "EDIT_PARAMETER"],
         ["a", "APPROVE"],
        // ,
        // ["j", "SET_JOB_STATE"]
    ])],
    ['SCOPE', new Map<string, string>([
        ["D", "DROP"],
        ["E", "EDIT"],
        // ["M", "MONITOR"],
        // ["O", "OPERATE"],
        // ["S", "SUBMIT"],
        ["V", "VIEW"],
        // ["K", "CREATE"],
        // ["G", "GRANT"],
        ["R", "RESOURCE"],
        // ["U", "USE"],
        ["X", "EXECUTE"],
        ["C", "CREATE_CONTENT"],
         // operate privs
        // ["c", "CANCEL"],
        // ["r", "RERUN"],
        // ["e", "ENABLE"],
        // ["s", "SET_STATE"],
        // ["d", "IGNORE_DEPENDENCY"],
        // ["i", "IGNORE_RESOURCE"],
        // ["l", "CLONE"],
        // ["u", "SUSPEND"],
        // ["w", "CLEAR_WARNING"],
        // ["p", "PRIORITY"],
        // ["m", "EDIT_PARAMETER"],
        // ["k", "KILL"],
        // ["a", "APPROVE"],
        // ["j", "SET_JOB_STATE"]
    ])],
    ['NAMED RESOURCE', new Map<string, string>([
        ["D", "DROP"],
        ["E", "EDIT"],
        // ["M", "MONITOR"],
        // ["O", "OPERATE"],
        // ["S", "SUBMIT"],
        ["V", "VIEW"],
        // ["K", "CREATE"],
        // ["G", "GRANT"],
        ["R", "RESOURCE"],
        // ["U", "USE"],
        // ["X", "EXECUTE"],
        ["C", "CREATE_CONTENT"],
         // operate privs
        // ["c", "CANCEL"],
        // ["r", "RERUN"],
        // ["e", "ENABLE"],
        // ["s", "SET_STATE"],
        // ["d", "IGNORE_DEPENDENCY"],
        // ["i", "IGNORE_RESOURCE"],
        // ["l", "CLONE"],
        // ["u", "SUSPEND"],
        // ["w", "CLEAR_WARNING"],
        // ["p", "PRIORITY"],
        // ["m", "EDIT_PARAMETER"],
        // ["k", "KILL"],
        // ["a", "APPROVE"],
        // ["j", "SET_JOB_STATE"]
    ])],
    ['GROUP', new Map<string, string>([
        // ["D", "DROP"],
        // ["E", "EDIT"],
        ["M", "MONITOR"],
        ["O", "OPERATE"],
        // ["S", "SUBMIT"],
        ["V", "VIEW"],
        // ["K", "CREATE"],
        // ["G", "GRANT"],
        // ["R", "RESOURCE"],
        // ["U", "USE"],
        // ["X", "EXECUTE"],
        // ["C", "CREATE_CONTENT"],
         // operate privs
        ["c", "CANCEL"],
        ["r", "RERUN"],
        ["e", "ENABLE"],
        ["s", "SET_STATE"],
        ["d", "IGNORE_DEPENDENCY"],
        ["i", "IGNORE_RESOURCE"],
        ["l", "CLONE"],
        ["u", "SUSPEND"],
        ["w", "CLEAR_WARNING"],
        ["p", "PRIORITY"],
        ["m", "EDIT_PARAMETER"],
        ["k", "KILL"],
        // ["a", "APPROVE"],
        // ["j", "SET_JOB_STATE"]
    ])],
    ['ENVIRONMENT', new Map<string, string>([
        // ["D", "DROP"],
        // ["E", "EDIT"],
        // ["M", "MONITOR"],
        // ["O", "OPERATE"],
        // ["S", "SUBMIT"],
        ["V", "VIEW"],
        // ["K", "CREATE"],
        // ["G", "GRANT"],
        // ["R", "RESOURCE"],
        ["U", "USE"],
        // ["X", "EXECUTE"],
        // ["C", "CREATE_CONTENT"],
         // operate privs
        // ["c", "CANCEL"],
        // ["r", "RERUN"],
        // ["e", "ENABLE"],
        // ["s", "SET_STATE"],
        // ["d", "IGNORE_DEPENDENCY"],
        // ["i", "IGNORE_RESOURCE"],
        // ["l", "CLONE"],
        // ["u", "SUSPEND"],
        // ["w", "CLEAR_WARNING"],
        // ["p", "PRIORITY"],
        // ["m", "EDIT_PARAMETER"],
        // ["k", "KILL"],
        // ["a", "APPROVE"],
        // ["j", "SET_JOB_STATE"]
    ])],
    ['INTERVAL', new Map<string, string>([
        ["V", "VIEW"],
        ["E", "EDIT"],
        ["D", "DROP"]
    ])]
])

// SERVER PRIV DEF:

// public final static String S_CREATE_CONTENT     = "C";
// public final static String S_DROP               = "D";
// public final static String S_EDIT               = "E";
// public final static String S_GRANT              = "G";
// public final static String S_CREATE             = "K";
// public final static String S_MONITOR            = "M";
// public final static String S_OPERATE            = "O";
// public final static String S_CREATE_PARENT_CONTENT = "P";       // artificial privilege
// public final static String S_RESOURCE           = "R";
// public final static String S_SUBMIT             = "S";
// public final static String S_USE                = "U";
// public final static String S_VIEW               = "V";
// public final static String S_EXECUTE            = "X";

// // The Abbreviation clashes are resolved by displaying the operator privileges within
// // parenthesis
// public final static String S_CANCEL             = "C";
// public final static String S_RERUN              = "R";
// public final static String S_ENABLE             = "E";
// public final static String S_SET_STATE          = "S";
// public final static String S_IGN_DEPENDENCY     = "D";
// public final static String S_IGN_RESOURCE       = "I";
// public final static String S_CLONE              = "L";
// public final static String S_SUSPEND            = "U";
// public final static String S_CLEAR_WARNING      = "W";
// public final static String S_PRIORITY           = "P";
// public final static String S_MODIFY_PARAMETER   = "M";
// public final static String S_KILL               = "K";
// public final static String S_APPROVE            = "A";
// public final static String S_SET_JOB_STATE      = "J";

// // all privileges: KPDEMO(OCRESDILUWPMKA)SVGR
// export const OperatePrivilegeMap = new Map<string, string>([
 
// ]);