export const objectTypeMap = new Map<string, string>([
    ["BATCH", "JOB DEFINITION"],
    ["MILESTONE", "JOB DEFINITION"],
    ["JOB", "JOB DEFINITION"],
    ["SERVER", "SCOPE"],
    ["SCOPE", "SCOPE"],
    ["SYNCHRONIZING", "NAMED RESOURCE"],
    ["SYSTEM", "NAMED RESOURCE"],
    ["STATIC", "NAMED RESOURCE"],
    ["POOL", "NAMED RESOURCE"],
    ["CATEGORY", "NAMED RESOURCE"],
    ["FOLDER", "FOLDER"],
    ["GROUP", "GROUP"],
    ["ENVIRONMENT", "ENVIRONMENT"],
    ["INTERVAL", "INTERVAL"],
    // add TYPES if needed
]);

export const hierarchyTypedObjects = new Map<string, boolean>([
    ["JOB DEFINITION", true],
    ["SCOPE", true],
    ["FOLDER", true],
    ["NAMED RESOURCE", true],
    ["GROUP", false],
    ["ENVIRONMENT", false],
    // add TYPES if needed
]);