
import { ChangeDetectorRef, Component, NgZone, OnInit } from '@angular/core';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { SnackbarData, SnackbarType } from './modules/global-shared/components/snackbar/snackbar-data';
import { SnackbarComponent } from './modules/global-shared/components/snackbar/snackbar.component';
import { EventEmitterService } from './services/event-emitter.service';
import { IconRegistryService } from './services/icon-registry.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  // animations:
})
export class AppComponent implements OnInit {

  constructor(private snackbar: MatSnackBar, private eventEmitterService: EventEmitterService, private changeDetectorRef: ChangeDetectorRef, private zone: NgZone, private iconRegistryService: IconRegistryService) {
    this.iconRegistryService.registIcons();
   }

  ngOnInit() {
    // this.eventEmitterService.snackBarMessageEmittor.subscribe((event) => {
    //   this.showSnackBarMessage(event);
    // });
  }

  // showSnackBarMessage(snackbarData: SnackbarData) {

  //   let theme: string;
  //   if (snackbarData.barType == SnackbarType.ERROR) {
  //     theme = 'mat-warn';
  //   } else if (snackbarData.barType == SnackbarType.WARNING) {
  //     theme = 'mat-accent';
  //   } else {
  //     theme = 'mat-primary';
  //   }
  //   this.zone.run(() => {
  //     this.snackbar.openFromComponent(SnackbarComponent, {
  //       data: snackbarData,
  //       panelClass: ['mat-toolbar', theme, 'default-snackbar'],
  //       duration: 50000
  //     });
  //   });
  //   // this.changeDetectorRef.detectChanges();
  // }
}


