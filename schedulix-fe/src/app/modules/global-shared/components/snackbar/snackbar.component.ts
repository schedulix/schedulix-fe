import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import {MAT_LEGACY_SNACK_BAR_DATA as MAT_SNACK_BAR_DATA, MatLegacySnackBarRef as MatSnackBarRef} from '@angular/material/legacy-snack-bar';
import { environment } from 'src/environments/environment';
import { OutputType, SnackbarData, SnackbarType } from './snackbar-data';

@Component({
  selector: 'app-snackbar',
  templateUrl: './snackbar.component.html',
  styleUrls: ['./snackbar.component.scss']
})
export class SnackbarComponent implements OnInit {

  developerMode: boolean = false;
  noneType = OutputType.NONE_SEPARATED;
  spaceType = OutputType.SPACE_SEPARATED;

  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: SnackbarData, private changeDetectorRef: ChangeDetectorRef,  private _snackRef: MatSnackBarRef<SnackbarComponent>,) {
    this.developerMode = environment.developerMode;
  }



  ngOnInit(): void {
      for(let i = 0; i < this.data.message.length; i++) {
          // console.log(this.data.message[i]);
      }
      this.changeDetectorRef.detectChanges();
  }

  dismiss(){
    this._snackRef.dismiss();
  }

}
