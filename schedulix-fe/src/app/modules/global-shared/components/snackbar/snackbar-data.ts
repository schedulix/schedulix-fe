// TODO: Implement different snackbar types in snackbar component

export enum SnackbarType {
    INFO,
    ERROR,
    WARNING,
    DEBUG,
    SUCCESS
}

export enum OutputType {
    NONE_SEPARATED,
    SPACE_SEPARATED,
    DEVELOPER // = Raw output without translation
}

export class SnackbarData {
    message!: string[]
    barType: SnackbarType | undefined
    outputType: OutputType = OutputType.NONE_SEPARATED
}
