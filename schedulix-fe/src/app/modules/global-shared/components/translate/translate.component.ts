import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { EventEmitterService } from '../../../../services/event-emitter.service';
import { TranslateService } from '../../../../services/translate.service'
import { SnackbarType } from '../snackbar/snackbar-data';

@Component({
  selector: 'app-translate',
  templateUrl: './translate.component.html',
  styleUrls: ['./translate.component.scss']
})
export class TranslateComponent implements OnInit {

  public language: any = 'de';

  constructor(private translateService: TranslateService, private eventEmitterService: EventEmitterService, private localStorageHelper:LocalStorageHelper) { }

  ngOnInit(): void {
    this.language = this.translateService.language;
  }

  onLangChange(value: string): void {
    // console.log(value)
    this.translateService.language = value;

    let message: string[] = ['lang_changed', 'language'];
    //barMessage has to be a key value of lang_dict
    this.eventEmitterService.pushSnackBarMessage(message, SnackbarType.INFO);
    this.localStorageHelper.setItem('lang', value);
  }
}
