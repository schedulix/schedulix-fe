import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslatePipe } from '../../pipes/translate.pipe';
import { TranslateComponent } from './components/translate/translate.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SnackbarComponent } from './components/snackbar/snackbar.component';
import { NewItemDialogComponent } from '../main/components/dialog-components/new-item-dialog/new-item-dialog.component';
import { HighlightPipe } from 'src/app/pipes/highlight.pipe';
import { DisplayModifierPipe } from 'src/app/pipes/display-modifier.pipe';

@NgModule({
  declarations: [
    HighlightPipe,
    TranslatePipe,
    DisplayModifierPipe,
    TranslateComponent,
    SnackbarComponent,
    NewItemDialogComponent,
  ],
  exports: [
    HighlightPipe,
    TranslatePipe,
    DisplayModifierPipe,
    TranslateComponent,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  imports: [
    MaterialModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class GlobalSharedModule { }
