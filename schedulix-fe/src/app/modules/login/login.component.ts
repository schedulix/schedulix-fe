import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, NgZone } from '@angular/core';
import { AbstractControl, FormControl, UntypedFormGroup, Validators, UntypedFormBuilder, Form } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { Credentials, SdmsServer } from 'src/app/data-structures/connection';
import { AuthService } from 'src/app/services/auth.service';
import { CommandApiService } from 'src/app/services/command-api.service';
import { ErrorHandlerService, sdmsException } from 'src/app/services/error-handler.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { OutputType, SnackbarData, SnackbarType } from '../global-shared/components/snackbar/snackbar-data';
import { AddConnectionDialogComponent, ConnectionDialogData } from './components/add-connection-dialog/add-connection-dialog.component';
import { ConnectionListService } from './services/connection-list.service';
import { SnackbarComponent } from '../global-shared/components/snackbar/snackbar.component';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: UntypedFormGroup;
  formSubscription: Subscription | undefined
  credentials: Credentials = new Credentials();
  loading: boolean = false;
  serverList: SdmsServer[] = [];
  selectedServer: string = '';
  rxjsSnackbarSubscription: Subscription = new Subscription();
  logo: string = this.customPropertiesService.getCustomObjectProperty("logo");

  constructor(
    private authService: AuthService,
    private router: Router,
    private dialog: MatDialog,
    private eventEmitterService: EventEmitterService,
    private fb: UntypedFormBuilder,
    private toolbox: ToolboxService,
    public connectionListService: ConnectionListService,
    private errorHandlerService: ErrorHandlerService,
    private snackbar: MatSnackBar,
    private zone: NgZone,
    protected customPropertiesService: CustomPropertiesService,
    ) {
    // The Packet Size of a TCP/UDP protocol has a port part of 16 bit. => 2^16 = 65536 valid numbers [0; 65535].
    this.loginForm = this.fb.group({
      // form_username: ["", [Validators.required]],
      // form_password: ["", [Validators.required]],
      form_username: [""],
      form_password: [""], 
      form_connectionName: [""],
      // form_host: ["", [Validators.required]],
      form_serverName: ["", [Validators.required]],
      // form_port: ["",
      //   [
      //     Validators.required,
      //     Validators.min(0),
      //     Validators.max(65535),
      //     Validators.pattern('^[0-9]+$')
      //   ]
      // ]
    });
  }

  ngOnInit(): void {
    // Updates login credentials every time something changed in the form.
    this.formSubscription = this.loginForm.valueChanges.subscribe(data => {
      this.credentials.username = data.form_username;
      this.credentials.password = data.form_password;
      // this.credentials.host = data.form_host;
      // this.credentials.port = data.form_port;
      this.credentials.serverName = data.form_serverName;
      this.credentials.connectionName = data.form_connectionName;
      this.evaluateSelectedServerSettings(data.form_serverName);
    });

    this.authService.loadServerList().then((res)=>{
      this.serverList = this.authService.getServerList();
      if(this.serverList.length > 0) {
        this.selectedServer = this.serverList[0].NAME;
      }
      // console.log(this.serverList);
    })

    this.rxjsSnackbarSubscription = this.eventEmitterService.snackBarMessageEmittor.subscribe((event) => {
      this.showSnackBarMessage(event);
    });

  }

  ngOnDestroy(): void {
    this.formSubscription?.unsubscribe();
    this.rxjsSnackbarSubscription.unsubscribe();
  }

  getSelectedServer(): SdmsServer | undefined {
    return this.serverList.find((server: SdmsServer) => {
      if (server.NAME == this.credentials.serverName) {
        return true;
      } else {
        return false;
      }
    })
  }
  evaluateSelectedServerSettings(serverName: string) {

    let selectedServer = this.getSelectedServer();

    // if(selectedServer?.SSO == true) {
    //   // if SSO activated password and user isnot mandatory anymore
    //   this.loginForm.controls['form_username'].clearValidators();
    //   this.loginForm.controls['form_password'].clearValidators();
    //   this.loginForm.controls['form_username'].updateValueAndValidity({onlySelf: false,
    //     emitEvent: false});
    //   this.loginForm.controls['form_password'].updateValueAndValidity({onlySelf: false,
    //     emitEvent: false});
    // } else {
    //   this.loginForm.controls['form_username'].setValidators(Validators.required);
    //   this.loginForm.controls['form_password'].setValidators(Validators.required);
    //   this.loginForm.controls['form_username'].updateValueAndValidity({onlySelf: false,
    //     emitEvent: false});
    //   this.loginForm.controls['form_password'].updateValueAndValidity({onlySelf: false,
    //     emitEvent: false});
    // }
  }

  async login() {

    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    this.loading = true;
    this.eventEmitterService.pushSnackBarMessage(['logging_in'], SnackbarType.INFO)
    this.authService.setCredentials(this.credentials);
    let response = await this.authService.authenticate();

    // Login either succeeds or has a connection issue or login data was invalid
    if (response.succeed) {
      this.eventEmitterService.pushSnackBarMessage(['login_successful'], SnackbarType.INFO)
      this.router.navigate(['home']);
      this.loading = false;
    } else if (response.response instanceof HttpErrorResponse) {

    } else if (response.response.hasOwnProperty('ERROR')) {

      // wrong username or password
      if (response.response.ERROR.ERRORCODE == '02110192352') {
        this.eventEmitterService.pushSnackBarMessage(['wrong_username_or_password'], SnackbarType.INFO)
      } else {
        this.errorHandlerService.createError(new sdmsException(response.response.ERROR.ERRORMESSAGE, '', response.response.ERROR.ERRORCODE))
      }
    }
    this.loading = false;
  }

  getFormErrorMessage(formControl: AbstractControl) {
    return this.toolbox.getFormErrorMessage(formControl);
  }

  openAddConnectionsDialog() {
    if (this.loginForm.invalid) {
      this.loginForm.markAllAsTouched();
      return;
    }
    const dialogRef = this.dialog.open(AddConnectionDialogComponent, {
      panelClass: 'default-dialog'
    });
    dialogRef.afterClosed().subscribe(async result => {
      // console.log(result);
      if (result !== undefined && result !== null) {
        this.loading = true;
        this.authService.setCredentials(this.credentials);
        let authResponse = await this.authService.authenticate();

        // Display snackbars to user according to what data was provided by the user
        if (authResponse.succeed) {
          let connectionResponse = this.connectionListService.addConnection(result, this.credentials);
          if (connectionResponse.succeed) {
            this.eventEmitterService.pushSnackBarMessage(['session_added_successfully'], SnackbarType.INFO);
          }
          else if (connectionResponse.sameName) {
            this.eventEmitterService.pushSnackBarMessage(['failed_to_add_session_by_name'], SnackbarType.ERROR);
          }
          else if (connectionResponse.emptyName) {
            this.eventEmitterService.pushSnackBarMessage(['failed_to_add_session_by_emptyname'], SnackbarType.ERROR);
          }
          else if (connectionResponse.sameUserAndHost) {
            this.eventEmitterService.pushSnackBarMessage(['failed_to_add_session_by_url'], SnackbarType.ERROR);
          }
        }
        else if (authResponse.response instanceof HttpErrorResponse) {
          // http errors are intercepted at the auth-service
        } else if (authResponse.response.hasOwnProperty('ERROR')) {
          // wrong username or password
          if (authResponse.response.ERROR.ERRORCODE == '02110192352') {
            this.eventEmitterService.pushSnackBarMessage(['wrong_username_or_password'], SnackbarType.INFO)
          } else {
            this.errorHandlerService.createError(new sdmsException(authResponse.response.ERROR.ERRORMESSAGE, '', authResponse.response.ERROR.ERRORCODE))
          }
        }
        this.loading = false;
      }
    });
  }

  showSnackBarMessage(snackbarData: SnackbarData) {

    let theme: string;
    if (snackbarData.barType == SnackbarType.ERROR) {
      theme = 'mat-warn';
    } else if (snackbarData.barType == SnackbarType.WARNING) {
      theme = 'mat-accent';
    } else {
      theme = 'mat-primary';
    }
    this.zone.run(() => {
      this.snackbar.openFromComponent(SnackbarComponent, {
        data: snackbarData,
        panelClass: ['mat-toolbar', theme, 'default-snackbar'],
        duration: 5000
      });
    });
    // this.changeDetectorRef.detectChanges();
  }
}
