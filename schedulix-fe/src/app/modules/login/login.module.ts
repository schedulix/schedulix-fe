import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { LoginRoutingModule } from './login-routing.module';
import { GlobalSharedModule } from 'src/app/modules/global-shared/global-shared.module';
import { ConnectionListComponent } from './components/connection-list/connection-list.component';
import { AddConnectionDialogComponent } from './components/add-connection-dialog/add-connection-dialog.component';



@NgModule({
  declarations: [LoginComponent, ConnectionListComponent, AddConnectionDialogComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,

    GlobalSharedModule
  ]
})
export class LoginModule { }
