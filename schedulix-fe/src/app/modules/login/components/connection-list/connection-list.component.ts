import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Subject, Subscription } from 'rxjs';
import { Credentials, Connection } from 'src/app/data-structures/connection';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ConnectionListService } from '../../services/connection-list.service';

@Component({
  selector: 'app-connection-list',
  templateUrl: './connection-list.component.html',
  styleUrls: ['./connection-list.component.scss']
})
export class ConnectionListComponent implements OnInit, OnDestroy {

  constructor(private connectionListService: ConnectionListService, private eventEmitterService:EventEmitterService) { }


  @Input() loginForm!: UntypedFormGroup;

  savedConnections: Array<Connection> = [];
  displayedColumns: string[] = ['name', 'user', 'host', 'choose', 'delete'];
  dataSource = this.savedConnections;

  //Subscription object to subscribe and unsubscribe
  rxjsSubscription: Subscription = new Subscription;

  fillLoginForm(connection: Connection) {
    this.loginForm.controls.form_username.setValue(connection.user);
    // this.loginForm.controls.form_host.setValue(connection.host);
    // this.loginForm.controls.form_port.setValue(connection.port);
    this.loginForm.controls.form_password.setValue(connection.password);
    this.loginForm.controls.form_serverName.setValue(connection.serverName);
    this.loginForm.controls.form_connectionName.setValue(connection.name);

    this.eventEmitterService.pushSnackBarMessage(['session_picked'], SnackbarType.INFO);
  }

  removeConnection(connection: Connection) {
    this.connectionListService.removeConnection(connection);
    this.eventEmitterService.pushSnackBarMessage(['session_removed_successfully'], SnackbarType.INFO);
  }

  ngOnInit(): void {
    this.savedConnections = this.connectionListService.getConnections();
    this.dataSource = this.savedConnections;

    // if connectionslist has changed -> reload table
    this.rxjsSubscription = this.connectionListService.savedConnectionSubject.subscribe(e => {
      // console.log(e);
      this.savedConnections = this.connectionListService.getConnections();
      // [...]say the array as definitely changed, this neccessary for the Angular changedetection to detect that the table has to be changed
      // lower performance because it "clones" the object
      this.dataSource = [...this.savedConnections];
    });

  }

  ngOnDestroy(): void {
    //discard subscription
    this.rxjsSubscription.unsubscribe()
  }





}
