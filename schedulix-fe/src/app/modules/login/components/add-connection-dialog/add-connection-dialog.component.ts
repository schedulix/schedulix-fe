import { Component, Input, OnInit } from '@angular/core';
import { Connection } from 'src/app/data-structures/connection';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { ToolboxService } from 'src/app/services/toolbox.service';

@Component({
  selector: 'app-add-connection-dialog',
  templateUrl: './add-connection-dialog.component.html',
  styleUrls: ['./add-connection-dialog.component.scss']
})
export class AddConnectionDialogComponent implements OnInit {

  constructor(private customPropertiesService: CustomPropertiesService) { }

  allowSavePasswords: boolean = true;
  ngOnInit(): void {

    this.allowSavePasswords = this.customPropertiesService.getCustomObjectProperty("enable_saved_passwords");
  }

  dialogData:ConnectionDialogData = new ConnectionDialogData();
}

export class ConnectionDialogData {
  savePassword:boolean = false;
  connectionName:string = '';
}
