import { Injectable } from '@angular/core';
import { Local } from 'protractor/built/driverProviders';
import { Subject } from 'rxjs';
import { Connection, Credentials } from 'src/app/data-structures/connection';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { SnackbarType } from '../../global-shared/components/snackbar/snackbar-data';
import { ConnectionDialogData } from '../components/add-connection-dialog/add-connection-dialog.component';

// Response for component to handle snackbar messages
export class ConnectionListResponse {
  sameName: boolean = false;
  sameUserAndHost: boolean = false;
  emptyName: boolean = false;
  succeed: boolean = false;
}

@Injectable({
  providedIn: 'root'
})
export class ConnectionListService {

  constructor(
    private localstoragehelper: LocalStorageHelper,
    private eventEmitterService: EventEmitterService,
    private toolboxService: ToolboxService) {
    this.savedConnections = this.getStoredConnections();
  }

  savedConnections: Array<Connection> = [];
  savedConnectionSubject: Subject<String> = new Subject();

  addConnection(data: ConnectionDialogData, credentials: Credentials): ConnectionListResponse {
    let connection: Connection = new Connection();
    connection.name = data.connectionName;
    connection.host = credentials.host;
    connection.user = credentials.username;
    connection.serverName = credentials.serverName;
    connection.port = credentials.port;
    connection.password = data.savePassword ? credentials.password : "";

    let response = this.validate(connection);

    if (response.succeed) {
      this.saveConnection(connection);
    }
    return response;
  }

  removeConnection(connection: Connection) {
    this.savedConnections = this.toolboxService.findAndRemoveObjectArray(this.savedConnections, connection);
    this.setConnections(this.savedConnections);
    this.savedConnectionSubject.next("removed");
  }

  // Checks whether a Connection is already in the list.
  private validate(connection: Connection) {
    let connectionListResponse: ConnectionListResponse = new ConnectionListResponse();
    connectionListResponse.succeed = true;

    if (connection.name == "") {
      connectionListResponse.emptyName = true;
      connectionListResponse.succeed = false;
    }

    this.savedConnections.forEach(item => {
      // Session name is unique!
      if (item.name.toLowerCase() == connection.name.toLowerCase()) {
        connectionListResponse.sameName = true;
        connectionListResponse.succeed = false;
        return;
      }
      // Check for same user in the same connection
      // if ((item.host == connection.host && item.port == connection.port && item.user == connection.user)) {
      //   connectionListResponse.sameUserAndHost = true;
      //   connectionListResponse.succeed = false;
      //   return;
      // }
    });

    return connectionListResponse;
  }

  saveConnection(connection: Connection) {
    this.savedConnections.push(connection);
    this.localstoragehelper.setJSON("connections", this.savedConnections);
    this.savedConnectionSubject.next("saved")
  }

  setConnections(connections: Array<Connection>) {
    this.savedConnections = connections;
    this.localstoragehelper.setJSON("connections", this.savedConnections);
    this.savedConnectionSubject.next("set")
  }

  getConnections() {
    return this.savedConnections;
  }

  // refactor to localstoragehelper, e.g parse JSON
  private getStoredConnections() {
    try {
      let obj = JSON.parse(this.localstoragehelper.getValue("connections"));
      // ensuring that always an array structure is returned
      if (obj === null) {
        return [];
      }
      else if (!Array.isArray(obj)) {
        return [obj];
      }

      return obj;
    }
    catch (e) {
      // console.log("JSON ERROR! - ConnectionManagerService");
      return [];
    }
  }

}
