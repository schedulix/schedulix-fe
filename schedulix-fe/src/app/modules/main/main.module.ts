import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { FullCalendarModule } from '@fullcalendar/angular';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main-routing.module';
import { GlobalSharedModule } from 'src/app/modules/global-shared/global-shared.module';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { LeftNavigationComponent } from './components/left-navigation/left-navigation.component';
import { ListComponent } from './components/list/list.component';
import { TabComponent } from './components/tab/tab.component';
import { FormGeneratorComponent } from './components/form-generator/form-generator.component';
import { InputComponent } from './components/form-generator/input/input.component';
import { TextComponent } from './components/form-generator/text/text.component';
import { CommentComponent } from './components/form-generator/comment/comment.component';
import { TableComponent } from './components/form-generator/table/table.component';
import { ChooserComponent } from './components/form-generator/chooser/chooser.component';
import { FormButtonComponent } from './components/form-button/form-button.component';
import { NavigationTreeComponent } from './components/treeComponents/navigationTree/navigationTree.component';
import { NavigationListComponent } from './components/list/navigation-list.component';
import { ChooserDialogComponent } from './components/form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { CheckboxComponent } from './components/form-generator/checkbox/checkbox.component';
import { SelectComponent } from './components/form-generator/select/select.component';
import { TreeListComponent } from './components/running-master-jobs/job-list/tree-list.component';
import { AccordionComponent } from './components/form-generator/accordion/accordion.component';
import { TextareaComponent } from './components/form-generator/textarea/textarea.component';
import { MasterListComponent } from './components/running-master-jobs/master-list/master-list.component';
import { EditorformDialogComponent } from './components/form-generator/editorform-dialog/editorform-dialog.component';
import { DialogTreeComponent } from './components/treeComponents/dialog-tree/dialog-tree.component';
import { RowButtonComponent } from './components/form-generator/row-button/row-button.component';
import { FormTreeTableComponent } from './components/treeComponents/form-tree-table/form-tree-table.component';
import { LoadBarComponent } from './components/form-generator/load-bar/load-bar.component';
import { FormIconsComponent } from './components/form-generator/form-icons/form-icons.component';
import { ShellComponent } from './components/shell/shell.component';
import { HierarchyComponent } from './components/hierarchy/hierarchy.component';
import { NgxGraphModule } from '@swimlane/ngx-graph';
import { IntervalSetupComponent } from './components/form-generator/interval-setup/interval-setup.component';
import { FileInputComponent } from './components/form-generator/file-input/file-input.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import {VirtualScrollerModule} from "@iharbeck/ngx-virtual-scroller";
import { VerticalHierarchyComponent } from './components/hierarchy/vertical-hierarchy/vertical-hierarchy.component';
import { HierachyTreeComponent } from './components/hierarchy/hierachy-tree/hierachy-tree.component';
import { GraphHierarchyComponent } from './components/hierarchy/graph-hierarchy/graph-hierarchy.component';
import { HierachyTooltip } from './components/hierarchy/hierachy-tooltip/Hierachy-tooltip.module';
import { CalendarComponent } from './components/calendar/calendar.component';
import { ContextMenuComponent } from './components/context-menu/context-menu.component';
import { InteractiveTooltipModule } from './components/interactive-tooltip/interactive-tooltip.module';

@NgModule({
  declarations: [
    MainComponent,
    ToolbarComponent,
    LeftNavigationComponent,
    ListComponent,
    TreeListComponent,
    NavigationListComponent,
    TabComponent,
    FormGeneratorComponent,
    InputComponent,
    FileInputComponent,
    TextComponent,
    CommentComponent,
    TableComponent,
    ChooserComponent,
    FormButtonComponent,
    NavigationTreeComponent,
    DialogTreeComponent,
    ChooserDialogComponent,
    CheckboxComponent,
    SelectComponent,
    AccordionComponent,
    TextareaComponent,
    MasterListComponent,
    EditorformDialogComponent,
    RowButtonComponent,
    FormTreeTableComponent,
    LoadBarComponent,
    FormIconsComponent,
    ShellComponent,
    HierarchyComponent,
    IntervalSetupComponent,
    LandingPageComponent,
    VerticalHierarchyComponent,
    HierachyTreeComponent,
    GraphHierarchyComponent,
    CalendarComponent,
    ContextMenuComponent
  ],
  imports: [

    CommonModule,
    MainRoutingModule,
    NgxGraphModule,
    HierachyTooltip,
    VirtualScrollerModule,
    GlobalSharedModule,
    FullCalendarModule,
    InteractiveTooltipModule
  ]
})
export class MainModule { }
