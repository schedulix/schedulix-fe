import { Component, OnInit } from '@angular/core';
import { System } from 'src/app/data-structures/system';
import { UserInformation } from 'src/app/data-structures/user-information';
import { AuthService } from 'src/app/services/auth.service';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { MainInformationService } from '../../services/main-information.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private authService: AuthService,
    public localStorageHelper: LocalStorageHelper,
    public mainInformationService: MainInformationService,
    public customPropertiesService: CustomPropertiesService,
    public eventEmittorService: EventEmitterService) {

  }

  // init with all valid connections
  public user: UserInformation = new UserInformation();

  public system: System = new System();



  public documentation: string = "default";

  public tag: string = '';

  ngOnInit(): void {

    // load connectionn(system) and user
    this.getSystem();
    this.getUser();

    // this.tag = this.customPropertiesService.getCustomObjectProperty('header_tag');

    this.mainInformationService.userHasLoadedObserver.subscribe((message: string) => {
      if(message == 'login') {
        this.system = this.mainInformationService.getSystem();
        console.log(this.system.maxEdition);
      }
    });
  }


  getTemplate(): string {
    return this.system.server.HEADER_TAG;
  }

  getTemplateColor(): string {
    return this.system.server.HEADER_TAG_COLOR ? this.system.server.HEADER_TAG_COLOR : '#bf8b00';
  }

  logout() {
    this.authService.logout();
  }

  getUser() {
    this.user = this.mainInformationService.getUser();
  }

  getSystem() {
    this.system = this.mainInformationService.getSystem();
  }
  // TODO link for doku or inbuilt????
  openDocu(docuname: String) {
    window.open("https://independit.de/Downloads/bicsuite_web_de-2.9.pdf", "_blank");
    // console.log(docuname);
  }
  isSchedulix() {
    return (this.system.maxLevel == 'OPEN');
  }

}
