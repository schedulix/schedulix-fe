import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { CommandApiService } from 'src/app/services/command-api.service';
import { CRUDService } from 'src/app/services/crud.service';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SelectComponent implements OnInit {

  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() formArrayIdentifier?: string;
  @Input() tabInfo!: Tab;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;

  formControl: UntypedFormControl = new UntypedFormControl();
  formArrayIndex: number = -1;
  formSubscription?: Subscription;
  values: string[] = [];
  generatedFormControl: FormGeneratorServiceControl = new FormGeneratorServiceControl();

  constructor(private generatorService: FormGeneratorService, private eventEmitterService: EventEmitterService, private commandService: CommandApiService, private crudService: CRUDService, private customPropertiesService: CustomPropertiesService) { }

  ngOnInit(): void {

    // this.editorformOptions.render.disabled ? this.formControl.disable() : this.formControl.enable();

    // console.log('asd')
    // console.log(this.bicsuiteObject)
    // If VALUES IS NO ARRAY, it will use the command in the string
    if (!Array.isArray(this.field.VALUES) && this.field.VALUES) {

      this.loadValuesFromServer();

    } else if (this.field.CUSTOM_CONFIG_IDENTIFIER) {

      this.values = this.customPropertiesService.getCustomObjectProperty (this.field.CUSTOM_CONFIG_IDENTIFIER);
      // console.log(this.field.CUSTOM_CONFIG_DEFAULT)

    } else if (this.field.BICSUITE_VALUE) {
      // console.log(this.bicsuiteObject);
      for (let v of this.bicsuiteObject[this.field.BICSUITE_VALUE]) {
        this.values.push(v[this.field.BICSUITE_VALUE_IDENTIFIER])
      }
    } else {
      this.values = this.field.VALUES;
    }

    if (this.field.hasOwnProperty("ADD_CURRENT")) {
      if (this.field.ADD_CURRENT == true || this.field.ADD_CURRENT == "true") {
        let current: string = this.bicsuiteObject[this.field.NAME];
        if (current && !this.values.includes(current)) {
          // console.log("current = " +current)
          this.values.push(current);
        }
      }
    }

    // console.log(this.bicsuiteObject)
    // Special Case, bicsuiteObject does not contain the field
    if (!this.bicsuiteObject.hasOwnProperty(this.field.NAME)) {
      if (this.field.DEFAULT) {
        this.bicsuiteObject[this.field.NAME] = (this.field.DEFAULT != undefined ? this.field.DEFAULT : this.field.VALUES[0]);
      } else if (this.field.CUSTOM_CONFIG_DEFAULT) {
        this.bicsuiteObject[this.field.NAME] = (
          this.field.CUSTOM_CONFIG_DEFAULT != undefined ?
            this.customPropertiesService.getCustomObjectProperty (this.field.CUSTOM_CONFIG_DEFAULT) :
            this.field.VALUES[0]
        );
      }
    }

    this.generatedFormControl = this.generatorService.createFormControl({
      parentForm: this.form,
      formControl: this.formControl,
      editorform: this.field,
      bicsuiteObject: this.bicsuiteObject,
      formArrayIdentifier: this.editorformOptions.formArrayIdentifier
    });

    this.formControl = this.generatedFormControl.formControl;
    this.formArrayIndex = this.generatedFormControl.formArrayIndex;

    // Update bicsuite object after each value change
    this.formSubscription = this.formControl.valueChanges.subscribe((newValue: string) => {
      this.bicsuiteObject[this.field.NAME] = newValue;
    });
  }

  loadValuesFromServer() {
    this.commandService.execute(this.field.VALUES.COMMAND).then((result: any) => {
      for (let value of result.DATA.TABLE) {
        this.values.push(value[this.field.VALUES.FIELD]);
      }
    }, (rejection:any) => {
      // do nothing when error
    });
  }

  onChange($event: any): void {
    let value = $event.value;
    // this.formControl.markAsDirty();
    this.formControl.setValue(value);


    // console.log(this.bicsuiteObject);

    // tell checkboxes that are connected to a select to reset there value
    if (this.form !== undefined && this.field.UPDATE_CHECKBOXES !== undefined) {
      for (let entry of this.field.UPDATE_CHECKBOXES) {
        let key = Object.keys(entry)[0];
        let value = Object.values(entry)[0];
        let fa = this.form.get(key) as UntypedFormArray;

        console.warn(fa);
        fa.at(this.formArrayIndex).setValue(value);
      }
    }

    if (this.field.ON_CHANGE !== undefined) {
      // ON CHANGE EVENTS:
      if(this.field.ON_CHANGE_METHOD) {
        this.crudService.createOrGetCrud(this.tabInfo.TYPE).callMethod(
          this.field.ON_CHANGE_METHOD, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject, this.form.root);
      }

      if (this.field.ON_CHANGE === "REFRESH_CONDITIONS" && this.tabInfo != undefined) {
        this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
      }
    }


  }

  ngOnDestroy() {
    this.generatorService.destroyFormControl(this.generatedFormControl);
    this.formSubscription?.unsubscribe();
  }
}
