import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormIconsComponent } from './form-icons.component';

describe('FormIconsComponent', () => {
  let component: FormIconsComponent;
  let fixture: ComponentFixture<FormIconsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormIconsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormIconsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
