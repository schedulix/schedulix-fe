import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-form-icons',
  templateUrl: './form-icons.component.html',
  styleUrls: ['./form-icons.component.scss']
})
export class FormIconsComponent implements OnInit, OnDestroy {

  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() tabInfo!: Tab;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;
  @Input() form!: UntypedFormGroup;

  private crud: any;

  private editorformConditionSubscription?: Subscription;

  constructor(private crudService: CRUDService, private eventEmitterService: EventEmitterService) { }

  ngOnInit(): void {
    this.crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE);

    // deepcopy field to get different icons displayed for each row of tables
    this.field = JSON.parse(JSON.stringify(this.field))

    this.renderOptions();

    this.editorformConditionSubscription = this.eventEmitterService.updateEditorformOptionsEmitter.subscribe(({ tab, rootForm }) => {
      if (this.tabInfo.ID === tab.ID) {
        if (rootForm == undefined || rootForm == this.form.root) {
          this.renderOptions();
        }
      }
    });

  }
  ngOnDestroy() {
    this.editorformConditionSubscription?.unsubscribe();
  }

  renderOptions() {
    for (let o of this.field.ICONS) {
      o.VISIBLE_CONDITION ? o.IS_VISIBLE = this.crud.checkFieldCondition(o.VISIBLE_CONDITION, this.field, this.bicsuiteObject, this.tabInfo) : o.IS_VISIBLE = true;
      o.CSS_CLASS_CONDITION ? o.CSS_CLASS = this.crud.checkCSSCondition(o.CSS_CLASS_CONDITION, this.field, this.bicsuiteObject, this.tabInfo) : '';
    }
  }


}
