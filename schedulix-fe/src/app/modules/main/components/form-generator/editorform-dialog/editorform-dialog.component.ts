import { Component, Inject, Input, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatLegacyDialogClose as MatDialogClose, MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { ViewEncapsulation } from '@angular/core';
import { Tab } from 'src/app/data-structures/tab';
import { FormButtonsService } from '../../../services/form-buttons.service';
import { Crud } from 'src/app/interfaces/crud';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';

@Component({
  selector: 'app-editorform-dialog',
  templateUrl: './editorform-dialog.component.html',
  styleUrls: ['./editorform-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None

})
export class EditorformDialogComponent implements OnInit {

  form: UntypedFormGroup = new UntypedFormGroup({});
  errorMessage: string = '';
  errorContainerOpened: boolean = false;
  successMessage: string = '';
  successContainerOpened: boolean = false;

  isLoading: boolean = false;

  constructor(
    public dialogRef: MatDialogRef<EditorformDialogComponent>, @Inject(MAT_DIALOG_DATA) public data: EditorformDialogData,
    public eventEmitterService: EventEmitterService
  ) { }

  ngOnInit(): void {
    // console.log(this.data.parentBicsuiteObject)
    // console.log(this.data.records)

  }

  closeErrorContainer() {
    this.errorContainerOpened = false;
  }

  clickCallback(btnInfo: any) {
    // console.log(btnInfo)
    switch (btnInfo.MODE) {
      case "CLOSE":
        {
          this.dialogRef.close(null);
          break;
        }
      case "CLOSE_WITH_DATA":
        {
          this.data.records.METHOD = btnInfo.METHOD;
          this.dialogRef.close(this.data.records);
          break;
        }
      // TEST_SUBMIT_SETTINGS
      case "EXECUTE": {

        this.isLoading = true;
        this.data.crud?.callMethod(btnInfo.METHOD, this.data.records, this.data.tabInfo).then((result) => {
          // console.log(result)
          this.errorMessage = '';
          this.errorContainerOpened = false;

          this.successMessage = result.FEEDBACK;
          this.successContainerOpened = true;
          this.data.records.CHECK_ONLY = 'false';

          this.isLoading = false;
        }
          , (rejection: any) => {
            // console.log(rejection)
            // rejection.tab = this.data.tabInfo;

            this.successMessage = '';
            this.successContainerOpened = false;

            this.data.records.CHECK_ONLY = 'false';
            // if(rejection.hasOwnProperty('ERROR')){
            //   this.errorMessage = rejection.ERROR.ERRORMESSAGE;
            //   this.errorContainerOpened = true;
            // }
            this.isLoading = false;
          }
        )
        // .catch((e: any) => {
        //   console.warn(e);
        //   e.tab = this.data.tabInfo;

        //   this.successMessage = '';
        //   this.successContainerOpened = false;

        //   if(e.hasOwnProperty('error')){
        //     this.errorMessage = e.error;
        //     this.errorContainerOpened = true;
        //   }
        //   this.isLoading = false;
        // }
        // );
        break;
      }
      case "EXECUTE_AND_CLOSE":
        {
          this.isLoading = true;
          // console.log(this.data);
          this.data.crud?.callMethod(btnInfo.METHOD, this.data.records, this.data.tabInfo).then((result) => {

            this.errorContainerOpened = false;

            this.isLoading = false;
            this.dialogRef.close(result);

          }, (rejection: any) => {
            // console.log(rejection)
            // rejection.tab = this.data.tabInfo;

            this.successMessage = '';
            this.successContainerOpened = false;

            this.data.records.CHECK_ONLY = 'false';
            // if(rejection.hasOwnProperty('ERROR')){
            //   this.errorMessage = rejection.ERROR.ERRORMESSAGE;
            //   this.errorContainerOpened = true;
            // }
            this.isLoading = false;
          }
          )
        }
    }
  }

  closeSuccessContainer() {
    this.successContainerOpened = false;
  }
}

export class EditorformDialogData {
  constructor(editorform: any, editorformDataHolder: any, tabInfo: Tab, title: string, crud?: Crud) {
    this.editorform = editorform;
    this.records = editorformDataHolder;
    this.title = title;
    this.tabInfo = tabInfo;
    this.crud = crud;
  }

  public parentBicsuiteObject: any;
  public editorform: any;
  public records: any;
  public title: string;
  public tabInfo: Tab;
  public crud: Crud | undefined;
}
