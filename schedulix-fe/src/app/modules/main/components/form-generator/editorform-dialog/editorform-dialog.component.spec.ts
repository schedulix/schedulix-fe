import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditorformDialogComponent } from './editorform-dialog.component';

describe('EditorformDialogComponent', () => {
  let component: EditorformDialogComponent;
  let fixture: ComponentFixture<EditorformDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditorformDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditorformDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
