import { Component, Input, OnInit } from '@angular/core';
import { AbstractControl, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { EditorformFlags, EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { CRUDService } from 'src/app/services/crud.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';

@Component({
  selector: 'app-text',
  templateUrl: './text.component.html',
  styleUrls: ['./text.component.scss']
})
export class TextComponent implements OnInit {

  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() tabInfo?: Tab;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;
  @Input() styleAttr: string = "";

  @Input() form?: UntypedFormGroup;
  generatedControl?: FormGeneratorServiceControl;
  @Input() formControl: UntypedFormControl = new UntypedFormControl(undefined); // 

  iconReference?: string;
  isClickable: boolean = false;
  text: string = "";
  style: string = "";

  constructor(private generatorService: FormGeneratorService, private crudService: CRUDService, private toolboxService: ToolboxService) { }

  ngOnInit(): void {
    this.iconReference = this.editorformOptions.flags.SVG_ICON;
    this.isClickable = this.field.ON_CLICK !== undefined || (this.field.ROW_BUTTON_ACTOR != undefined && this.field.ROW_BUTTON_ACTOR == true);
    
    // if(this.editorformOptions.render.disabled) {
    //   this.isClickable = false;
    // }

    if (this.form != undefined && this.field.IS_FORM_TEXT == true) {
      this.generatedControl = this.generatorService.createFormControl({
        parentForm: this.form,
        formControl: new UntypedFormControl(),
        editorform: this.field,
        bicsuiteObject: this.bicsuiteObject,
        formArrayIdentifier: this.editorformOptions.formArrayIdentifier
      });
      this.formControl = this.generatedControl.formControl;
    }

    this.text = this.textConvert(this.bicsuiteObject[this.field.NAME]);
    this.style = this.bicsuiteObject[this.styleAttr];
    // this.generatedControl?.formControl.setValue(this.text);
  }

  ngDoCheck() {
    // Text changes are not visible as the form can't change. But on some occations the text might change (See paramter job-definition)
    this.text = this.textConvert(this.bicsuiteObject[this.field.NAME]);
    if (this.text != this.generatedControl?.formControl.value) {
      this.generatedControl?.formControl.setValue(this.text);
    }
    if (this.iconReference != this.editorformOptions.flags.SVG_ICON) {
      this.iconReference = this.editorformOptions.flags.SVG_ICON;
    }
  }

  ngOnDestroy() {
    if (this.generatedControl != undefined) {
      this.generatorService.destroyFormControl(this.generatedControl);
    }
  }


  onClick(method: string) {
    if (this.tabInfo !== undefined) {
      let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE);
      crud.callMethod(method, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject)
      // this.crudService.executeCustom(this.tabInfo.TYPE, this.field.ON_CLICK, this.bicsuiteObject, this.tabInfo);
    }
  }

  getFormErrorMessage(formControl: AbstractControl) {
    return this.toolboxService.getFormErrorMessage(formControl);
  }

  private textConvert(record: any) {
    let result: string = "";
    if (this.field.TABLE_CONVERT != undefined) {
      for (let entry of record.TABLE) {
        let txt = entry[this.field.TABLE_CONVERT];
        result += txt + ",";
      }
      result = result.slice(0, -1);

      return result;
    }
    else
      return record;
  }
}
