import { AfterViewChecked, ChangeDetectorRef, Component, DebugElement, EventEmitter, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BicsuiteResponse } from 'src/app/data-structures/bicsuite-response';
import { EditorformFlags, EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { FormGeneratorService } from '../../services/form-generator.service';

@Component({
  selector: 'app-form-generator',
  templateUrl: './form-generator.component.html',
  styleUrls: ['./form-generator.component.scss']
})
export class FormGeneratorComponent implements OnInit, OnDestroy, AfterViewChecked {

  @Input() editorform: any; // Editorform, used to generate a form.
  @Input() bicsuiteObject: any; // Serverobject
  @Input() tabInfo: any; // Information abbout the tab.
  @Input() field: any; // Used if only a specific field should be rendered
  @Input() formArrayIdentifier?: string; // Used if invoker is a table or form-array.
  @Input() editorformOptions?: EditorformOptions; // form generator can be initialized with it own options
  @Input() accordionOpenStates: any = {}; // Booleans dictionary.

  @Input() table: Array<any> = [];
  @Input() parentBicsuiteObject: any;

  initValueHolder: any;
  editorformOptionsArr: EditorformOptions[] = []; // holder for all field (child-components) options

  // form controls are added in the child components of the generator.
  // They should also take care of validation.
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});

  // which accordeons index are beeing rendered
  @Input() loadNumber: number = -1;

  // for table row Buttons
  @Input() callbackFunction: ((rowData: any, columnInfo: any, rowIndex: number) => void) = (()=>{});

  private editorformConditionSubscription?: Subscription;

  @Input() parentHasLoaded: boolean = true;
  @Output() setTabHasLoaded = new EventEmitter<boolean>();


  constructor(private changeDetectorRef: ChangeDetectorRef, private generatorService: FormGeneratorService, private eventEmitterService: EventEmitterService, private crudService: CRUDService, private toolboxService: ToolboxService) {

  }

  ngOnInit(): void {

    this.editorformConditionSubscription = this.eventEmitterService.updateEditorformOptionsEmitter.subscribe(({ tab, rootForm }) => {
      // console.log("formoptionsemittor")
      if (this.tabInfo.ID === tab.ID) {
        if (rootForm == undefined || rootForm == this.form.root) {
          this.reCreateEditorformOptions();
          // TODO: check for performance impact on large forms
          this.changeDetectorRef.detectChanges();       }
      }
    });

    if (this.field !== undefined) {
      this.editorform = { FIELDS: [] };
      this.editorform.FIELDS.push(this.field);
    }

    for (let a of this.editorform.FIELDS) {
      if (a.hasOwnProperty('DISPLAY_METHOD')) {
          this.editorform = this.toolboxService.deepCopy(this.editorform);
        break;
      }
    }

    // form generator is not invoking editorformConditionSubscription.next() because it would do too many unnecessary calls
    // because additional form-generator instances may be created by child components (see table.component e.s.).

    this.reCreateEditorformOptions();

    // init parent bicsuiteobject
    if(!this.parentBicsuiteObject) {
      this.parentBicsuiteObject = this.bicsuiteObject;
    }

  }



  ngOnDestroy(): void {
    if (!this.editorformConditionSubscription?.closed) {
      this.editorformConditionSubscription?.unsubscribe();
    }
  }

  ngAfterViewChecked(): void {
    // Ensures that form is not throwing a NG0100 when status was changed after change detection.
    this.changeDetectorRef.detectChanges();
  }

  private reCreateEditorformOptions() {


    this.editorformOptionsArr = [];

    for (let editorformField of this.editorform.FIELDS) {

      if (editorformField.hasOwnProperty('DISPLAY_METHOD')) {
        let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE);
        crud.callMethod(editorformField.DISPLAY_METHOD, this.bicsuiteObject, this.tabInfo, editorformField, this.parentBicsuiteObject);
        // console.log("display mnethod")
      }

      let options = this.generatorService.createEditorformOptions(editorformField, this.bicsuiteObject, this.tabInfo, this.editorformOptions);

      // Apply additional attributes:
      options.formArrayIdentifier = this.formArrayIdentifier;

      if (editorformField.TYPE == "ACCORDION") {
        if(this.accordionOpenStates) {
          if (this.accordionOpenStates[editorformField.VALUE] == undefined) {
            this.accordionOpenStates[editorformField.VALUE] = editorformField.IS_OPENED == true ? true : false;
          }
        }
      }

      if (this.tabInfo != undefined) {
        this.crudService.manipulateEditorformOptions(this.tabInfo.TYPE, options, editorformField, this.tabInfo, this.bicsuiteObject, this.table);
      }
      this.editorformOptionsArr.push(options);
    }


  }

  setHasLoaded(hasLoaded: boolean){
    // console.log(hasLoaded);
    this.setTabHasLoaded.next(hasLoaded);
  }
}
