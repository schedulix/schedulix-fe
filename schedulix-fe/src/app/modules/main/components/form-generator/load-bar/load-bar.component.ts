import { Component, Input, OnInit } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';

@Component({
  selector: 'app-load-bar',
  templateUrl: './load-bar.component.html',
  styleUrls: ['./load-bar.component.scss']
})
export class LoadBarComponent implements OnInit {
  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() tabInfo?: Tab;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();

  @Input() form?: UntypedFormGroup;

  loadnumber: number = 1;

  constructor() { }

  ngOnInit(): void {

    if(this.bicsuiteObject[this.field.NAME]) {
      let valueAsInt = parseFloat(this.bicsuiteObject[this.field.NAME]);
      if(this.isNumber(valueAsInt)){
        this.loadnumber = valueAsInt;
      }
    }
  }

  isNumber(value: any) {
    return !Number.isNaN(value);
  }
}
