import { Component, Inject, OnInit } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { ActivatedRoute } from '@angular/router';
import { Tab } from 'src/app/data-structures/tab';
import { ListComponent } from 'src/app/modules/main/components/list/list.component';
import { ToolboxService } from 'src/app/services/toolbox.service';

@Component({
  selector: 'app-chooser-dialog',
  templateUrl: './chooser-dialog.component.html',
  styleUrls: ['./chooser-dialog.component.scss']
})
export class ChooserDialogComponent implements OnInit {

  output: string = ""

  constructor( @Inject(MAT_DIALOG_DATA) public data: ChooserDialogData, public dialogRef: MatDialogRef<ChooserDialogComponent>, private toolboxService: ToolboxService) { }

  ngOnInit(): void {

  }

  callback($event:any) {
    this.dialogRef.close($event);
  }

  // callback($event:any) {
    
  //   if (this.data.displayType == undefined || this.data.displayType == "list") {
  //     this.output = $event[this.data.propertyToShow];
  //   }
  //   else if (this.data.displayType == "tree") {
  //     if ($event.bicsuiteObject[this.data.propertyToShow] == $event.namePath)
  //       this.output = $event.bicsuiteObject[this.data.propertyToShow];
  //     else
  //       this.output = $event.namePath + " (" + $event.bicsuiteObject[this.data.propertyToShow] + ")";
      
  //     console.log($event);
  //   }

  //   this.dialogRef.close(this.output);
  // }

}

export class ChooserDialogData {
  command: string = ""
  TYPE: string = ""
  shortType: string = ""
  propertyPath: string[] = [];
  displayType?: string;
  parentBicsuiteObject: any;

  chooseType?: string | Array<string>;
  childSelect?: string | Array<string>;
  chooseParentType?: string | Array<string>;
  isPropertyParent: boolean = false;

  propertyShowOption: any = {PROPERTY_NAME: "", VALUE: ""};
  propertyToShow: string = "NAME";
  nonable: boolean = false; // Whether the chooser allows "NONE" to be chosen
  noneValue: string = 'NONE';
  customTransform: string = "";
  tabInfo?: Tab;
  editorFormfield?: any;
  width?: string;
  noneDisplay: string = 'NONE';
  message?: string = "";
} 
