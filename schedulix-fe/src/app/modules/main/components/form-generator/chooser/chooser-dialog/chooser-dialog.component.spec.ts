import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChooserDialogComponent } from './chooser-dialog.component';

describe('ChooserDialogComponent', () => {
  let component: ChooserDialogComponent;
  let fixture: ComponentFixture<ChooserDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChooserDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ChooserDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
