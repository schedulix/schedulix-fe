import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { AbstractControl, FormArray, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { ChooserDialogComponent, ChooserDialogData } from 'src/app/modules/main/components/form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';
import { ClipboardService, SdmsClipboardType } from '../../../services/clipboard.service';
import { TreeFunctionsService } from '../../../services/tree-functions.service';
import { fadeAnimation } from 'src/app/app.animation';
import { MainInformationService } from '../../../services/main-information.service';

@Component({
  selector: 'app-chooser',
  templateUrl: './chooser.component.html',
  styleUrls: ['./chooser.component.scss'],
  animations: [fadeAnimation],
  encapsulation: ViewEncapsulation.None
})
export class ChooserComponent implements OnInit, OnDestroy {

  @Input() tabInfo?: Tab;
  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() formArrayIdentifier?: string;
  @Input() formControl: UntypedFormControl = new UntypedFormControl(undefined); // formcontrol representation of this input (can be set by parent component)
  @Input() editorformOptions!: EditorformOptions;
  @Input() parentBicsuiteObject: any;


  value: string = "No Value"
  formArrayIndex: number = -1;
  formSubscription: Subscription | undefined;

  isHidden: boolean = false;
  conditionNotFirst: boolean = false;
  generatedForm: FormGeneratorServiceControl = new FormGeneratorServiceControl();
  // hasGrant: boolean = false;

  constructor(private treeFunctionService: TreeFunctionsService, private clipboardService: ClipboardService, public dialog: MatDialog, private toolbox: ToolboxService, private generatorService: FormGeneratorService, private privilegesService: PrivilegesService, private eventEmitterService: EventEmitterService, private crudService: CRUDService, private mainInformationService: MainInformationService) { }

  ngOnInit(): void {

    /*** PRIVS ARE CHECKED IN FORMGENEATOR SERVICE! ***/
    // this.editorformOptions.render.disabled ? this.formControl.disable() : this.formControl.enable();


    this.generatedForm = this.generatorService.createFormControl({
      parentForm: this.form,
      formControl: this.formControl,
      editorform: this.field,
      bicsuiteObject: this.bicsuiteObject,
      formArrayIdentifier: this.formArrayIdentifier
    });

    this.formControl = this.generatedForm.formControl;
    this.formArrayIndex = this.generatedForm.formArrayIndex;

    // console.log(this.formArrayIndex)

    // this.editorformOptions.render.disabled ? this.formControl.disable() : this.formControl.enable();


    this.formSubscription = this.formControl.valueChanges.subscribe((newValue: string) => {
      this.bicsuiteObject[this.field.NAME] = newValue;
    });

    // open the choose dialog if AUTOCHOOSE is set and the filed not yet has a value
    // console.log(this.field)
    // console.log(this.bicsuiteObject)
    let didAutoChoosePropertyName = "NOFORM_DID_AUTOCHOOSE_" + this.field.NAME;
    if (this.field.AUTOCHOOSE && !this.bicsuiteObject[this.field.NAME] && !this.bicsuiteObject[didAutoChoosePropertyName] &&
        !(this.bicsuiteObject.NOFORM_DISABLE_AUTOCHOOSE == "true")) {
      // Use NOFORM_DID_AUTOCHOOSE to avoid chooser to auto choose after switching tabs if chooser was cancelled
      this.bicsuiteObject[didAutoChoosePropertyName] = true;
      this.choose(this.field.CHOOSER, this.field.CHOOSER_EDITORFORM_FIELD);
    }
  }

  ngOnDestroy(): void {
    if (this.formSubscription !== undefined && !this.formSubscription.closed) {
      this.formSubscription.unsubscribe();
    }
    this.generatorService.destroyFormControl(this.generatedForm);
  }

  onClick(method: string) {
    if (this.tabInfo !== undefined) {
      let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE);
      crud.callMethod(method, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject)
    }
  }

  choose(chooser: string, editorform_field_dependency: string = '') {
    // console.log(editorform_field_dependency);

    if (this.editorformOptions.render.disabled)
      return;

    let dependencyValue: string = '';

    // console.log(editorform_field_dependency)
    // console.log(this.bicsuiteObject);
    // console.log(this.field)
    if (editorform_field_dependency != '') {
      // if CHOOSER_EDITORFORM_FIELD_SCOPE == notParentBicsuiteObject, the dependency value for specific chooser commands will be chosen from the own bicsuitobject. NOT toplevel as default
      if (this.field.CHOOSER_EDITORFORM_FIELD_SCOPE == 'notParentBicsuiteObject') {
        dependencyValue += this.bicsuiteObject[editorform_field_dependency];
        // console.log(dependencyValue)
      } else {
        // Search value in root form
        if (this.parentBicsuiteObject[editorform_field_dependency] != null) {
          dependencyValue += this.parentBicsuiteObject[editorform_field_dependency];
          // console.log(dependencyValue)
        }
      }
    }

    let dialogData: ChooserDialogData | undefined = this.getDialogData(chooser, dependencyValue, this.parentBicsuiteObject);
    if (dialogData != undefined) {
      // Open Dialog
      // console.log(dialogData)
      if (this.field.TRANSLATE) {
        dialogData.message = this.field.TRANSLATE;
      }
      const dialogRef = this.dialog.open(ChooserDialogComponent, {
        width: (dialogData.width ? dialogData.width : '450px'),
        data: dialogData,
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
      })

      dialogRef.afterClosed().subscribe((result: any) => {

        if (result === undefined || result == false)
          return;

        this.setResult(result, dialogData);
      });
    }
  }

  setResult(result: any, dialogData: ChooserDialogData | undefined) {

    // console.log(result)
    // console.log(dialogData!.propertyToShow);
    let output = result.bicsuiteObject[dialogData!.propertyToShow];
    if (result.bicsuiteObject.NONE_VALUE != undefined) {
      output = result.bicsuiteObject.NONE_VALUE;
    }
    // console.log(result);
    // console.log(dialogData);
    // console.log(output);
    if (dialogData!.displayType == "tree" && output != result.namePath) {
      output = result.namePath + " (" + output + ")";
    }

    if (this.formArrayIdentifier != undefined) {
      this.formControl.setValue(output);
    }
    else {
      this.form.controls[this.field.NAME].setValue(output);
    }

    if (this.field.hasOwnProperty("CHOOSE_METHOD")) {
      let crud = this.crudService.createOrGetCrud(this.tabInfo!.TYPE);
      crud.callMethod(this.field.CHOOSE_METHOD, this.bicsuiteObject, this.tabInfo!, result, this.parentBicsuiteObject).then(
        (result) => {
          if (this.field.ON_CHANGE === "REFRESH_CONDITIONS" && this.tabInfo != undefined) {
            this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
          }
          return result;
        }, crud.emptyRejection
      );
    }
    else {
      if (this.field.ON_CHANGE === "REFRESH_CONDITIONS" && this.tabInfo != undefined) {
        this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
      }
    }
  }

  getDialogData(chooser: string, dependencyValue: string, parentBicsuiteObject: any): ChooserDialogData | undefined {
    let dialogData: ChooserDialogData = new ChooserDialogData();
    dialogData.parentBicsuiteObject = parentBicsuiteObject;
    dialogData.tabInfo = this.tabInfo;
    dialogData.editorFormfield = this.field;
    // Add more choosers here:
    switch (chooser) {
      case "ExitStateDefinitionChooser":
        dialogData.command = "LIST EXIT STATE DEFINITION";
        dialogData.TYPE = "EXIT STATE DEFINITION";
        dialogData.shortType = "esd";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "ExitStateMappingChooser":
        dialogData.command = "LIST EXIT STATE MAPPING";
        dialogData.TYPE = "EXIT STATE MAPPING";
        dialogData.shortType = "esm";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "noneableExitStateMappingChooser":
        dialogData.command = "LIST EXIT STATE MAPPING";
        dialogData.TYPE = "EXIT STATE MAPPING";
        dialogData.shortType = "esm";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.nonable = true;
        dialogData.noneValue = '<default>';
        dialogData.noneDisplay = '<default>';
        break;
      case "ExitStateTranslationChooser":
        dialogData.command = "LIST EXIT STATE TRANSLATION";
        dialogData.TYPE = "EXIT STATE TRANSLATION";
        dialogData.shortType = "est";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "NoneableExitStateTranslationChooser":
        dialogData.command = "LIST EXIT STATE TRANSLATION";
        dialogData.TYPE = "EXIT STATE TRANSLATION";
        dialogData.shortType = "est";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.noneValue = "";
        dialogData.nonable = true;
        break;
      case "ExitStateProfileChooser":
        dialogData.command = "LIST EXIT STATE PROFILE";
        dialogData.TYPE = "EXIT STATE PROFILE";
        dialogData.shortType = "esp";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "NoneableExitStateProfileChooser":
        dialogData.command = "LIST EXIT STATE PROFILE";
        dialogData.TYPE = "EXIT STATE PROFILE";
        dialogData.shortType = "esp";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.nonable = true;
        break;
      case "FootprintChooser":
        dialogData.command = "LIST FOOTPRINTS";
        dialogData.TYPE = "FOOTPRINT";
        dialogData.shortType = "footprint";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "noneableFootprintChooser":
        dialogData.command = "LIST FOOTPRINTS";
        dialogData.TYPE = "FOOTPRINT";
        dialogData.shortType = "footprint";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.noneDisplay = 'NONE';
        dialogData.noneValue = '';
        dialogData.nonable = true;
        break;
      case "GroupChooser":
        dialogData.command = "LIST GROUP";
        dialogData.TYPE = "GROUP";
        dialogData.shortType = "group";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "RestartExitStateDefinitionChooser":
        dialogData.command = "SHOW EXIT STATE PROFILE '" + dependencyValue + "'";
        dialogData.TYPE = "EXIT STATE DEFINITION";
        dialogData.shortType = "esd";
        dialogData.propertyPath = ["DATA", "RECORD", "STATES", "TABLE"];
        dialogData.propertyShowOption = { PROPERTY_NAME: "TYPE", VALUE: "RESTARTABLE" };
        dialogData.propertyToShow = "ESD_NAME";
        break;
      case "NonableRestartExitStateDefinitionChooser":
        dialogData.command = "SHOW EXIT STATE PROFILE '" + dependencyValue + "'";
        dialogData.TYPE = "EXIT STATE DEFINITION";
        dialogData.shortType = "esd";
        dialogData.propertyPath = ["DATA", "RECORD", "STATES", "TABLE"];
        dialogData.propertyShowOption = { PROPERTY_NAME: "TYPE", VALUE: "RESTARTABLE" };
        dialogData.propertyToShow = "ESD_NAME";
        dialogData.noneDisplay = "NONE",
        dialogData.noneValue = "";
        dialogData.nonable = true;
        break;
      case "TriggerExitStateDefinitionChooser":
        dialogData.command = "SHOW EXIT STATE PROFILE '" + dependencyValue + "'";
        dialogData.TYPE = "EXIT STATE DEFINITION";
        dialogData.shortType = "esd";
        dialogData.propertyPath = ["DATA", "RECORD", "STATES", "TABLE"];
        dialogData.propertyShowOption = { PROPERTY_NAME: "TYPE", VALUE: "" };
        dialogData.propertyToShow = "ESD_NAME";
        break;
      case "JobTimeoutExitStateDefinitionChooser":
        dialogData.command = "SHOW EXIT STATE PROFILE '" + dependencyValue + "'";
        dialogData.TYPE = "EXIT STATE DEFINITION";
        dialogData.shortType = "esd";
        dialogData.propertyPath = ["DATA", "RECORD", "STATES", "TABLE"];
        dialogData.propertyShowOption = { PROPERTY_NAME: "TYPE", VALUE: "" };
        dialogData.propertyToShow = "ESD_NAME";
        break;
      case "ResourceStateDefinitionChooser":
        dialogData.command = "LIST RESOURCE STATE DEFINITION";
        dialogData.TYPE = "RESOURCE STATE DEFINITION";
        dialogData.shortType = "rsd";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "NonableResourceStateDefinitionChooser":
        dialogData.command = "LIST RESOURCE STATE DEFINITION";
        dialogData.TYPE = "RESOURCE STATE DEFINITION";
        dialogData.shortType = "rsd";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.noneValue = "ANY";
        dialogData.noneDisplay = "ANY";
        dialogData.nonable = true;
        break;
      case "EnvironmentChooser":
        dialogData.command = "LIST ENVIRONMENTS";
        dialogData.TYPE = "ENVIRONMENT";
        dialogData.shortType = "env";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "NonableEnvironmentChooser":
        dialogData.command = "LIST ENVIRONMENTS";
        dialogData.TYPE = "ENVIRONMENT";
        dialogData.shortType = "env";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.nonable = true;
        dialogData.noneDisplay = 'NONE';
        dialogData.noneValue = '';
        break;
      case "ParameterTreeChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.displayType = "tree";
        dialogData.chooseType = "PARAMETER";
        dialogData.childSelect = "PARAMETER";
        dialogData.chooseParentType = ["JOB"]
        break;
      case "ScopeResourceTreeChooser":
        dialogData.command = "LIST SCOPE GLOBAL";
        dialogData.TYPE = "SCOPE";
        dialogData.shortType = "scope";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.displayType = "tree";
        dialogData.chooseType = ["STATIC", "SYSTEM", "SYNCHRONIZING"];
        dialogData.childSelect = "SCOPE_RESOURCE";
        dialogData.chooseParentType = ["SCOPE", "SERVER"]
        dialogData.width = "800px";
        break;
      case "ScopeSystemAndPoolResourceTreeChooser":
        dialogData.command = "LIST SCOPE GLOBAL";
        dialogData.TYPE = "SCOPE";
        dialogData.shortType = "scope";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.displayType = "tree";
        dialogData.chooseType = ["SYSTEM", "POOL"];
        dialogData.childSelect = "SCOPE_RESOURCE";
        dialogData.chooseParentType = ["SCOPE", "SERVER"]
        dialogData.width = "800px";
        break;
      case "ResourceParameterTreeChooser":
        dialogData.command = "LIST NAMED RESOURCE WITH EXPAND = (50)";
        dialogData.TYPE = "NAMED RESOURCE";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.displayType = "tree";
        dialogData.chooseType = "RESOURCE_PARAMETER";
        dialogData.childSelect = "RESOURCE_PARAMETER";
        dialogData.chooseParentType = ["SYNCHRONIZING", "STATIC", "SYSTEM", "POOL"]
        break;
      case "ParameterResourceChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = "PARAMETER";
        dialogData.childSelect = "PARAMETER";
        dialogData.displayType = "tree";
        break;
      case "BatchesAndJobDefinitionChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["BATCH", "JOB"];
        dialogData.displayType = "tree";
        break;
      case "SchedulingEntityChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["BATCH", "JOB", "MILESTONE"];
        dialogData.displayType = "tree";
        break;
      case "noneableBatchesAndJobDefinitionChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["BATCH", "JOB"];
        dialogData.displayType = "tree";
        dialogData.nonable = true;
        dialogData.noneValue = '';
        break;
      case "FolderAndSeChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["FOLDER", "BATCH", "JOB"];
        dialogData.displayType = "tree";
        break;
      case "BatchesAndJobDefinitionChooserNoCondense":
        dialogData.command = "LIST FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["BATCH", "JOB"];
        dialogData.displayType = "tree";
        break;
      case "SchedulingEntityChooser":
        dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
        dialogData.TYPE = "FOLDER";
        dialogData.shortType = "folder";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["JOB", "BATCH", "MILESTONE"];
        dialogData.displayType = "tree";
        break;
      case "UserChooser":
        dialogData.command = "LIST USER";
        dialogData.TYPE = "USER";
        dialogData.shortType = "serveruser";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "NoneableResourceStateProfileChooser":
        dialogData.command = "LIST RESOURCE STATE PROFILE";
        dialogData.TYPE = "RESOURCE STATE PROFILE";
        dialogData.shortType = "rsp";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.nonable = true;
        dialogData.noneValue = "";
        // dialogData.noneDisplay = "NONE";
        break;
      case "ResourceStateProfileChooser":
        dialogData.command = "LIST RESOURCE STATE PROFILE";
        dialogData.TYPE = "RESOURCE STATE PROFILE";
        dialogData.shortType = "rsp";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      case "StaticNamedResourceChooser":
        dialogData.command = "LIST NAMED RESOURCE WITH EXPAND = ALL";
        dialogData.TYPE = "NAMED RESOURCE";
        dialogData.shortType = "static";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["STATIC"];
        dialogData.displayType = "tree";
        dialogData.customTransform = "staticResourcesTransformer";
        break;
      case "SystemNamedResourceChooser":
        dialogData.command = "LIST NAMED RESOURCE WITH EXPAND = ALL";
        dialogData.TYPE = "NAMED RESOURCE";
        dialogData.shortType = "system";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["SYSTEM"];
        dialogData.displayType = "tree";
        dialogData.customTransform = "systemResourcesTransformer";
        break;
      case "SystemOrSynchronizingNamedResourceChooser":
        dialogData.command = "LIST NAMED RESOURCE WITH EXPAND = ALL";
        dialogData.TYPE = "NAMED RESOURCE";
        dialogData.shortType = "static";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.chooseType = ["SYSTEM", "SYNCHRONIZING"];
        dialogData.displayType = "tree";
        dialogData.customTransform = "systemOrSynchronizingResourcesTransformer";
        break;
      case "NamedResourceChooser":
        dialogData.command = "LIST NAMED RESOURCE WITH EXPAND = ALL";
        dialogData.TYPE = "NAMED RESOURCE";
        dialogData.shortType = "resource";
        dialogData.chooseType = ["STATIC", "SYSTEM", "SYNCHRONIZING", "POOL"];
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.displayType = "tree";
        break;
      case "IntervalChooser":
        dialogData.command = "LIST INTERVAL";
        dialogData.TYPE = "INTERVAL";
        dialogData.shortType = "interval";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.nonable = true;
        dialogData.noneValue = "";
        break;
      case "NoneableResourceStateMappingChooser":
        dialogData.command = "LIST RESOURCE STATE MAPPING";
        dialogData.TYPE = "RESOURCE STATE MAPPING";
        dialogData.shortType = "rsm";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.nonable = true;
        break;
      case "NoneableSpecificResourceStateMappingChooser":
        dialogData.command = "LIST RESOURCE STATE MAPPING for " + dependencyValue + "";
        dialogData.TYPE = "RESOURCE STATE MAPPING";
        dialogData.shortType = "rsm";
        dialogData.propertyPath = ["DATA", "TABLE"];
        dialogData.noneValue = '';
        dialogData.nonable = true;
        break;
      case "JobDependenyChooser":
        dialogData.command = "SHOW job definition " + dependencyValue + "";
        dialogData.TYPE = "JOB";
        dialogData.shortType = "job";
        dialogData.propertyPath = ["DATA", "RECORD", "REQUIRED_JOBS", "TABLE"];
        dialogData.nonable = true;
        dialogData.propertyToShow = "DEPENDENTNAME";
        break;
      case "WatchTypeChooser":
        dialogData.command = "LIST WATCH TYPE";
        dialogData.TYPE = "WATCH TYPE";
        dialogData.shortType = "watchtype";
        dialogData.propertyPath = ["DATA", "TABLE"];
        break;
      default:
        console.warn("Chooser Type was not found: " + chooser);
        return undefined;
    }

    return dialogData;
  }


  getPasteTypes(): SdmsClipboardType[] {
    let pasteTypes: SdmsClipboardType[] = [];
    if (this.field.PASTE_TYPES) {
        for (let type of this.field.PASTE_TYPES.split(',')) {
          // console.log(type)
          let cpType = SdmsClipboardType[type as keyof typeof SdmsClipboardType];
          pasteTypes.push(cpType);
        }
    }
    // console.log(pasteTypes);
    return pasteTypes;
  }
  conditionCanPaste(): boolean {
    if (this.field.PASTE_ALLOWED) {
      if(this.clipboardService.checkClipboardIsSingleItem()) {
        return this.clipboardService.checkItemTypesPresent(this.getPasteTypes(), this.tabInfo!.ID);
      } else {
        return false
      }
    } else {
      return false;
    }
  }

  paste() {

    let dependencyValue: string = '';
    let chooser = this.field.CHOOSER;
    let editorform_field_dependency = this.field.CHOOSER_EDITORFORM_FIELD;

    if (this.field.CHOOSER_EDITORFORM_FIELD_SCOPE == 'notParentBicsuiteObject') {
      dependencyValue += this.bicsuiteObject[editorform_field_dependency];
      // console.log(dependencyValue)
    } else {
      // Search value in root form
      if (this.parentBicsuiteObject[editorform_field_dependency] != null) {
        dependencyValue += this.parentBicsuiteObject[editorform_field_dependency];
        // console.log(dependencyValue)
      }
    }
    let dialogData: ChooserDialogData | undefined = this.getDialogData(chooser, dependencyValue, this.parentBicsuiteObject);

    let result = this.clipboardService.getItems(this.getPasteTypes())[0].data;
    this.setResult(this.treeFunctionService._dialogTransformer(result, dialogData!), dialogData);
  }
}
