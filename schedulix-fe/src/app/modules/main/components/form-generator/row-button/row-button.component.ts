
import { Component, DebugElement, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { FormGeneratorService } from '../../../services/form-generator.service';

@Component({
  selector: 'app-row-button',
  templateUrl: './row-button.component.html',
  styleUrls: ['./row-button.component.scss']
})
export class RowButtonComponent implements OnInit {
  @Input() field: any; // field property from editorform
  @Input() bicsuiteObject: any; // records from server
  @Input() tabInfo?: Tab;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  // editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() callbackFunction: ((rowData: any, columnInfo: any, rowIndex: number) => void) = (() => { });
  constructor(private formgeneratorService: FormGeneratorService) { }

  ngOnInit(): void {
    // this.editorformOptions = this.formgeneratorService.createEditorformOptions(this.field, this. bicsuiteObject, undefined, this.editorformOptions);

    // console.log(this.editorformOptions)
  }


  onClick() {
    // console.log("click")
    // this.onRowClick.emit(); // calls event, handled by table.component.ts
    this.callbackFunction(this.bicsuiteObject, this.field, this.bicsuiteObject.rowIndex);
  }


}
