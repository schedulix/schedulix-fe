import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { MatLegacyCheckboxChange as MatCheckboxChange, LegacyTransitionCheckState as TransitionCheckState } from '@angular/material/legacy-checkbox';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Subscription } from 'rxjs';
import { ValidationHelper } from 'src/app/classes/formgenerator/validation-helper';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { Crud } from 'src/app/interfaces/crud';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';
import { ChooserDialogComponent, ChooserDialogData } from '../chooser/chooser-dialog/chooser-dialog.component';
import { MainInformationService } from '../../../services/main-information.service';

@Component({
  selector: 'app-interval-setup',
  templateUrl: './interval-setup.component.html',
  styleUrls: ['./interval-setup.component.scss']
})
export class IntervalSetupComponent implements OnInit {

  @Input() tabInfo?: Tab;
  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() formArrayIdentifier?: string;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;

  formControl: UntypedFormControl = new UntypedFormControl();
  formArrayIndex: number = -1;
  formSubscription?: Subscription;
  values: string[] = [];
  generatedFormControl: FormGeneratorServiceControl = new FormGeneratorServiceControl();
  minuteSelectResolution: number = this.crudService.createOrGetCrud("INTERVAL").getConfig("minuteSelectResolution", 5);
  hourSelection: string[] = []
  minuteSelection: string[] = [];
  minuteSelectionTo: string[] = [];

  intervalIconReference = "interval";

  constructor(private calendarChooserDialog: MatDialog, private generatorService: FormGeneratorService, private cd: ChangeDetectorRef, private privilegesService: PrivilegesService, private crudService: CRUDService, private eventEmitterService: EventEmitterService, private mainInformationService: MainInformationService) {
  }

  ngOnInit(): void {
    this.generatedFormControl = this.generatorService.createFormControl({
      parentForm: this.form,
      formControl: this.formControl,
      editorform: this.field,
      bicsuiteObject: this.bicsuiteObject,
      formArrayIdentifier: this.editorformOptions.formArrayIdentifier
    });


    this.formControl = this.generatedFormControl.formControl;

    this.setIntervalValidators();


    this.formArrayIndex = this.generatedFormControl.formArrayIndex;

    let hour = 0;
    while (hour < 24) {
      let strHour = "0" + hour.toString();
      this.hourSelection.push(strHour.substring(strHour.length - 2));
      hour++;
    }
    let minute = 0;
    while (minute < 60) {
      let strMinute = "0" + minute.toString();
      this.minuteSelection.push(strMinute.substring(strMinute.length - 2));
      this.minuteSelectionTo.push(strMinute.substring(strMinute.length - 2));
      minute += this.minuteSelectResolution;
    }
    if (!this.minuteSelection.includes(this.bicsuiteObject.MINUTE)) {
      this.minuteSelection.push(this.bicsuiteObject.MINUTE);
    }
    if (!this.minuteSelection.includes(this.bicsuiteObject.MINUTE_FROM)) {
      this.minuteSelection.push(this.bicsuiteObject.MINUTE_FROM);
    }
    if (!this.minuteSelectionTo.includes(this.bicsuiteObject.MINUTE_TO)) {
      this.minuteSelectionTo.push(this.bicsuiteObject.MINUTE_TO);
    }

    this.form.valueChanges.subscribe((value) => {
      // emit event has to be false, otherwise values to change -> endless loop
      this.formControl.updateValueAndValidity({ emitEvent: false });
    });
  }

  onChange(event: any): void {
    this.bicsuiteObject.REPEAT_MINUTES = this.bicsuiteObject.REPEAT_MINUTES ? this.bicsuiteObject.REPEAT_MINUTES.toString() : "";
    // do fake change on the FIELD to trigger deepCompare for save button eneble/disable

    this.validateFormWithValue();
  }

  checked(num: number): boolean {
    let checked: boolean = false;
    let key = "CHECKBOX" + num.toString().replace("-", "_");
    if (this.bicsuiteObject[key] == "true") {
      checked = true;
    }
    return checked;
  }

  onChkChange(ob: MatCheckboxChange, num: number) {
    let key = "CHECKBOX" + num.toString().replace("-", "_");
    this.bicsuiteObject[key] = ob.checked.toString();
    this.validateFormWithValue();
  }

  listChange(ev: any) {
    try {
      this.bicsuiteObject.LIST = ev.target.value;
    } catch (e) {
      console.info('could not set textarea-value');
    }
    this.validateFormWithValue();
  }

  chooseCalendar() {
    let dependencyValue: string = '';
    if (this.bicsuiteObject.CALENDAR != null) {
      dependencyValue += this.bicsuiteObject.CALENDAR;
    }
    let dialogData: ChooserDialogData | undefined = this.getCalendarChooserDialogData(this.parentBicsuiteObject);
    if (dialogData != undefined) {
      // Open Dialog
      const dialogRef = this.calendarChooserDialog.open(ChooserDialogComponent, {
        width: '450px',
        data: dialogData,
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
      })
      dialogRef.afterClosed().subscribe((result) => {
        if (result === undefined || !result.bicsuiteObject) {
          return;
        }
        this.bicsuiteObject.CALENDAR = result.bicsuiteObject.NAME;
        this.validateFormWithValue();
      });
    }
  }

  setIntervalValidators() {
    let validators: ValidatorFn[] = [];

    validators.push(this.validateIntervalSetup());
    this.formControl.setValidators(validators);

  }
  validateFormWithValue() {

    // do change on the FIELD to trigger deepCompare for save button eneble/disable
    this.formControl.setValue(this.formControl.value);
  }

  // TODO: Validate all types of interval
  // except CLD/CLD at least 1 checkbox has to be set

  checkCheckboxSelected(checkboxes: number[]) {

    for (let checkbox of checkboxes) {
      if (this.checked(checkbox)) {
        return null;
      }
    }
    return { invalid: "At least 1 checkbox has to be selected" };
  }

  validateIntervalSetup(): ValidatorFn {
    let DOWselections = [1, 2, 3, 4, 5, 6, 7];
    let DOMselections = [
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31,
      -8, -7, -6, -5, -4, -3, -2, -1
    ];
    let WOMselections = [1, 2, 3, 4, 5, -1, -2, -3, -4, -5];
    let MOYselections = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
    let IWYselections = [
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
      11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
      21, 22, 23, 24, 25, 26, 27, 28, 29, 30,
      31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
      41, 42, 43, 44, 45, 46, 47, 48, 49, 50,
      51, 52, 53,
      -6, -5, -4, -3, -2, -1
    ];

    return (): ValidationErrors | null => {
      let result = null;

      if (this.bicsuiteObject.TYPE == 'REP') {
        if (this.bicsuiteObject.REPEAT_MINUTES) {
          result = null;
        } else {
          result = { invalid: "No Repeat Minutes specified" };
        }
        return result;
      }
      // console.log("validateIntervalSetup")
      switch (this.bicsuiteObject.TYPE) {
        case 'DOW':
          result = this.checkCheckboxSelected(DOWselections);
          break;
        case 'DOM':
          result = this.checkCheckboxSelected(DOMselections);
          break;
        case 'WOM':
          result = this.checkCheckboxSelected(WOMselections);
          break;
        case 'MOY':
          result = this.checkCheckboxSelected(MOYselections);
          break;
        case 'IWY':
          result = this.checkCheckboxSelected(IWYselections);
          break;
        case 'IWM':
          result = this.checkCheckboxSelected(WOMselections);
          break;
        case 'REP':
          if (this.bicsuiteObject.REPEAT_MINUTES) {
            result = null;
          } else {
            result = { invalid: "No Repeat Minutes specified" };
          }
          break;
        case 'CLF':
        case 'CLD':
          if (this.bicsuiteObject.CALENDAR) {
            result = null;
          } else {
            result = { invalid: "No Calendar(Interval) selected" };
          }
          break;
      }
      return result;
    }
  }

  openCalendar() {
    if (this.tabInfo !== undefined) {
      let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE);
      crud.callMethod("openCalendar", this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject)
    }
  }

  // openCalendar(bicsuiteObject: any, tabInfo: any, arg?: any, arg1?: any): Promise<any> {
  //   console.log(bicsuiteObject)
  //   alert('OK');
  //   return Promise.resolve({});
  // }


  getCalendarChooserDialogData(parentBicsuiteObject: any): ChooserDialogData | undefined {
    let dialogData: ChooserDialogData = new ChooserDialogData();
    dialogData.parentBicsuiteObject = parentBicsuiteObject;
    dialogData.tabInfo = this.tabInfo;
    dialogData.editorFormfield = this.field;
    dialogData.editorFormfield = { FILTER_METHOD : "filterSelectableIntervals" };
    dialogData.command = "LIST INTERVAL";
    dialogData.TYPE = "INTERVAL";
    dialogData.shortType = "interval";
    dialogData.propertyPath = ["DATA", "TABLE"];
    return dialogData;
  }
}
