import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IntervalSetupComponent } from './interval-setup.component';

describe('IntervalSetupComponent', () => {
  let component: IntervalSetupComponent;
  let fixture: ComponentFixture<IntervalSetupComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IntervalSetupComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IntervalSetupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
