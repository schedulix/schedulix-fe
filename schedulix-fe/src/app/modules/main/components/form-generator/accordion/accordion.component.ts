import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { FormGeneratorService } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss']
})
export class AccordionComponent implements OnInit {
  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() tabInfo?: Tab;
  @Input() editorformOptions?: EditorformOptions;
  @Input() accordionOpenStates!: any;
  @Input() parentBicsuiteObject: any;

  @Input() parentHasLoaded: boolean = true;
  @Output() setTabHasLoaded = new EventEmitter<boolean>();

  hidden: boolean = false;

  hasGrant: boolean = false;

  constructor(private formGeneratorService: FormGeneratorService,
    private privilegesService: PrivilegesService, private eventEmitterService: EventEmitterService) { }

  ngOnInit(): void {

    // if (this.tabInfo !== undefined) {
    //   // console.log(this.tabInfo)
    //   this.privilegesService.valide(this.field.PRIVS, this.tabInfo.PRIVS,this.bicsuiteObject, this.tabInfo) && this.privilegesService.hasManagePriv(this.field.MANAGEPRIVS) ? this.hasGrant = true: '';
    // } else {
    //   // no tab privs provided...
    //   this.privilegesService.valide(this.field.PRIVS, '',  this.bicsuiteObject, this.tabInfo) && this.privilegesService.hasManagePriv(this.field.MANAGEPRIVS) ? this.hasGrant = true: '';
    // }
  }

  open(field: string) {
    this.accordionOpenStates[field] = true;
    this.eventEmitterService.saveTabEmitter.next();
  }

  close(field: string) {
    this.accordionOpenStates[field] = false;
    this.eventEmitterService.saveTabEmitter.next();
  }

  setHasLoaded(hasLoaded: boolean){
    // console.log(hasLoaded);
    this.setTabHasLoaded.next(hasLoaded);
  }

}
