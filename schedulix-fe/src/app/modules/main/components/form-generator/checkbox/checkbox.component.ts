import { ChangeDetectorRef, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormArray, UntypedFormControl, UntypedFormGroup } from '@angular/forms';
import { MatLegacyCheckboxDefaultOptions as MatCheckboxDefaultOptions, MAT_LEGACY_CHECKBOX_DEFAULT_OPTIONS as MAT_CHECKBOX_DEFAULT_OPTIONS } from '@angular/material/legacy-checkbox';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    { // disables checkbox behaviour that could cause side effects as the checkbox is changed in this class.
      provide: MAT_CHECKBOX_DEFAULT_OPTIONS,
      useValue: { clickAction: 'noop' } as MatCheckboxDefaultOptions
    }
  ],
  encapsulation: ViewEncapsulation.None
})
export class CheckboxComponent implements OnInit {

  @Input() tabInfo!: Tab;
  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() formArrayIdentifier?: string;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;

  // isDisabled: boolean = false;
  formField: UntypedFormControl = new UntypedFormControl();
  formSubscription: Subscription | undefined;
  formArrayIndex = -1;
  boolFieldValue: boolean = false;
  hasGrant: boolean = false;
  generatedFormControl: FormGeneratorServiceControl = new FormGeneratorServiceControl();

  constructor(private generatorService: FormGeneratorService, private cd: ChangeDetectorRef, private privilegesService: PrivilegesService, private crudService: CRUDService, private eventEmitterService: EventEmitterService) { }

  ngOnInit(): void {
    // if (this.tabInfo !== undefined) {
    //   this.hasGrant = ;// this.privilegesService.valide(this.field.PRIVS, this.tabInfo.PRIVS);
    // }
    // else {
    //   this.hasGrant = true;
    // }

    // console.log(this.editorformOptions.render.disabled)

    if (!(this.field.IS_FORM == false)) {
      this.generatedFormControl = this.generatorService.createFormControl({
        parentForm: this.form,
        formControl: this.formField,
        editorform: this.field,
        bicsuiteObject: this.bicsuiteObject,
        formArrayIdentifier: this.editorformOptions.formArrayIdentifier // <= hier ist der Fehler.
      });
    }

    this.formField = this.generatedFormControl.formControl;
    this.formArrayIndex = this.generatedFormControl.formArrayIndex;

    // TODO RENDER OPTIONS FROM EDITORFORM
    // this.isDisabled = !this.generatorService.editableCondition(this.bicsuiteObject, this.field.EDITABLE_CONDITION);
    this.boolFieldValue = this.validateBoolean();
    this.formField.setValue(this.bicsuiteObject[this.field.NAME]);

    // console.log(this.field.NAME)
    // console.log(this.form)
    // Update bicsuite object after each value change
    this.formSubscription = this.formField.valueChanges.subscribe((newValue: string) => {
      let value = newValue;
      if (this.field.ONVALUE != undefined && newValue === "true") {
        value = this.field.ONVALUE;
      }
      if (this.field.OFFVALUE != undefined && newValue === "false") {
        value = this.field.OFFVALUE;
      }
      this.bicsuiteObject[this.field.NAME] = value;
      this.boolFieldValue = this.validateBoolean();
      // this.eventEmitterService.bicsuiteRefreshEmitter.next(this.tabInfo);
      // console.log(this.bicsuiteObject)
    });
  }

  ngOnDestroy(): void {
    if (this.formSubscription !== undefined && !this.formSubscription.closed) {
      this.formSubscription.unsubscribe();
    }

    this.generatorService.destroyFormControl(this.generatedFormControl);
  }

  validateBoolean(): boolean {
    let checkValue: string = "true";
    if (this.field.ONVALUE) {
      checkValue = this.field.ONVALUE;
    }
    return (this.bicsuiteObject[this.field.NAME] === checkValue) || (this.bicsuiteObject[this.field.NAME] === true);
  }

  ngAfterViewChecked() {
    // change occurs in the next change detection, as current change detection was already done at this timestamp.
    // this.isDisabled = !this.generatorService.editableCondition(this.bicsuiteObject, this.field.EDITABLE_CONDITION);
    this.cd.detectChanges();
  }


  check() {

    if (this.editorformOptions.render.disabled)
      return;

    // RADIO Function
    if (this.formArrayIdentifier != undefined) {
      if (this.field.RADIO !== undefined && (this.field.RADIO === "true" || this.field.RADIO === true)) {
        if (this.field.MANDATORY && this.validateBoolean()) {
          // reclicking an already actiev radio should not put it off if MANDATORY
          return;
        }
        let fa = (this.form.get(this.formArrayIdentifier) as UntypedFormArray);

        // set all entries to false
        for (const [key, control] of Object.entries(fa.controls)) {
          if (control !== this.formField)
            control.setValue("false");
        }
      }
    }

    // Toggle checkbox
    // console.log(this.bicsuiteObject[this.field.NAME])

    this.formField.setValue((this.validateBoolean() ? "false" : "true"));
    // this.form.markAsDirty();

    if(this.field.ON_CHANGE_METHOD) {
      this.crudService.createOrGetCrud(this.tabInfo.TYPE).callMethod(
        this.field.ON_CHANGE_METHOD, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject, this.form.root);
    }
    if (this.field.ON_CHANGE === "REFRESH_CONDITIONS" && this.tabInfo != undefined) {
      this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
    }

    // console.log(this.bicsuiteObject)
  }
}
