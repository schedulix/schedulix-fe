
import { AfterViewInit, ChangeDetectorRef, Component, ComponentFactoryResolver, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { BicsuiteResponse } from 'src/app/data-structures/bicsuite-response';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { OutputType, SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { CrudOption, CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { TableComponent } from '../table/table.component';
import comment_editorform from 'src/app/json/editorforms/comment.json';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { PrivilegesService } from '../../../services/privileges.service';


@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit, OnChanges, OnDestroy {

  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() tabInfo!: Tab;
  @Input() editorformOptions?: EditorformOptions;

  // Comment form is not independed by parent components.
  form: UntypedFormGroup = new UntypedFormGroup({});
  isCreated = false;
  editMode = false;
  commentsBicsuiteCopy: any;
  viewComments: any = [];
  isDisable: boolean = false;

  private dataRefreshSubscription?: Subscription;

  editorformField: any;

  constructor(
    private commandService: CommandApiService,
    private crudService: CRUDService,
    private eventEmitter: EventEmitterService,
    private privilegesService: PrivilegesService) { }


  ngOnChanges(changes: SimpleChanges): void {
    this.isCreated = this.tabInfo?.MODE !== TabMode.NEW;
    this.editorformField = comment_editorform;
  }
  ngOnDestroy(): void {
    if (this.dataRefreshSubscription !== undefined && !this.dataRefreshSubscription.closed) {
      this.dataRefreshSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    if (this.tabInfo !== undefined) {
      this.isCreated = this.tabInfo.MODE !== TabMode.NEW;
    }
    if (this.tabInfo) {
      // console.log(this.tabInfo)
      this.isDisable = !this.privilegesService.valide(this.field.PRIVS == undefined ? 'E' : this.field.PRIVS, this.tabInfo.PRIVS, this.field, this.tabInfo, this.bicsuiteObject);
      if (this.field.DEBUG) {
        // console.log(this.field)
        // console.log(this.isDisable)
      }
    }
    this.updateViewComments();
  }

  private updateViewComments() {
    if (this.isCreated && this.bicsuiteObject.COMMENT && this.bicsuiteObject.COMMENT.TABLE) {
      this.viewComments = [];
      let comments = this.bicsuiteObject.COMMENT.TABLE;
      let lastTagIndex = -1;

      for (let i = 0; i < comments.length; i++) {
        if (comments[i].TAG !== undefined && comments[i].TAG !== null && comments[i].TAG !== "") {
          this.viewComments.push({ TAG: comments[i].TAG, DESCRIPTION: [comments[i].DESCRIPTION] });
          lastTagIndex = this.viewComments.length - 1;
        }
        else if (lastTagIndex >= 0) {
          this.viewComments[lastTagIndex].DESCRIPTION.push(comments[i].DESCRIPTION);
        }
        else {
          this.viewComments.push({ TAG: comments[i].TAG, DESCRIPTION: [comments[i].DESCRIPTION] });
        }
      }
    }
  }


  comment() {
    this.editMode = true;
    // Create a new Object reference and make a deep copy of COMMENT attribute
    // This ensures that comments of the original object are not modified by any user-input change.

    if(this.field.NO_OBJECT_COPY) {
      this.commentsBicsuiteCopy = this.bicsuiteObject
      this.commentsBicsuiteCopy.COMMENT = this.bicsuiteObject.COMMENT;
    } else {
      this.commentsBicsuiteCopy = Object.create(this.bicsuiteObject);
      this.commentsBicsuiteCopy.COMMENT = JSON.parse(JSON.stringify(this.bicsuiteObject.COMMENT));
    }

  }

  save() {
    let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE)
    if (this.field.METHOD) {
      crud.lockUnlockTabMethod(crud.callMethod(this.field.METHOD, this.bicsuiteObject, this.tabInfo, this.commentsBicsuiteCopy), this.tabInfo).then((result: any) => {

        // error handling has do be done in defined CRUD method

        // Update Original Object
        this.bicsuiteObject.COMMENT = this.commentsBicsuiteCopy.COMMENT;
        this.updateViewComments();

        this.editMode = false;

      }, crud.emptyRejection);
    } else {
      if (this.tabInfo !== undefined && this.tabInfo.MODE !== TabMode.NEW) {
        let comments = this.commentsBicsuiteCopy.COMMENT.TABLE;
        let crudOption: CrudOption = comments.length > 0 ? CrudOption.CREATE : CrudOption.DELETE;

        crud.lockUnlockTabMethod(this.crudService.execute("COMMENT", crudOption, this.commentsBicsuiteCopy, this.tabInfo), this.tabInfo)
          .then((result: any) => {
           if (result.hasOwnProperty("FEEDBACK")) {
              this.eventEmitter.pushSnackBarMessage([result.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);

              // Update Original Object
              this.bicsuiteObject.COMMENT = this.commentsBicsuiteCopy.COMMENT;
              this.updateViewComments();

              this.editMode = false;
            }
          }).catch((err: any) => {
            this.eventEmitter.pushSnackBarMessage(['connection_problem'], SnackbarType.ERROR, OutputType.NONE_SEPARATED);
          });
      }
    }

    // if (this.tabInfo !== undefined && this.tabInfo.MODE !== TabMode.NEW) {
    //   let comments = this.commentsBicsuiteCopy.COMMENT.TABLE;
    //   let crudOption: CrudOption = comments.length > 0 ? CrudOption.CREATE : CrudOption.DELETE;

    //   this.crudService.execute("COMMENT", crudOption, this.commentsBicsuiteCopy, this.tabInfo)
    //     .then((result: any) => {
    //       if (result.hasOwnProperty("ERROR")) {
    //         this.eventEmitter.pushSnackBarMessage(["Error Code: ", result.ERROR.ERRORCODE], SnackbarType.ERROR, OutputType.DEVELOPER);
    //         this.eventEmitter.errorEmitter.next(result);
    //         // "TODO: OPEN ERROR CONTAINER"
    //       } else if (result.hasOwnProperty("FEEDBACK")) {
    //         this.eventEmitter.pushSnackBarMessage([result.FEEDBACK], SnackbarType.SUCCESS, OutputType.DEVELOPER);

    //         // Update Original Object
    //         this.bicsuiteObject.COMMENT = this.commentsBicsuiteCopy.COMMENT;
    //         this.updateViewComments();

    //         this.editMode = false;
    //       }
    //     }).catch((err: any) => {
    //       this.eventEmitter.pushSnackBarMessage(['connection_problem'], SnackbarType.ERROR, OutputType.NONE_SEPARATED);
    //     });
    // }
  }


  cancel() {
    this.editMode = false;
    this.commentsBicsuiteCopy = null;
  }
}
