import { Component, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { AbstractControl, UntypedFormControl, FormArray, UntypedFormGroup, Validators, ValidatorFn } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';
import { CRUDService } from 'src/app/services/crud.service';

// Styling, shared by other form components should be included in the generator scss
// and added to child components styleUrls.
@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./../form-generator.component.scss', './input.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class InputComponent implements OnInit {

  @Input() field: any; // field property from editorform
  @Input() bicsuiteObject: any; // records from server
  @Input() form: UntypedFormGroup = new UntypedFormGroup({}); // form component in which this input is
  @Input() formControl: UntypedFormControl = new UntypedFormControl(undefined); // formcontrol representation of this input (can be set by parent component)
  @Input() tabInfo?: Tab;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;


  formSubscription: Subscription | undefined;
  formArrayIndex: number = -1;

  isHidden: boolean = false;
  // conditionNotFirst: boolean = false;
  generatedForm: FormGeneratorServiceControl = new FormGeneratorServiceControl();
  useSimple = true;
  inputType: string = 'text';
  iconReference?: string;
  // hasGrant: boolean = false;

  constructor(private toolbox: ToolboxService, private generatorService: FormGeneratorService, private privilegesService: PrivilegesService, private eventEmitterService: EventEmitterService, private crudService: CRUDService) { }

  ngOnInit(): void {
    this.iconReference = this.editorformOptions.flags.SVG_ICON;

    // Apply render options
    // this.hasGrant = this.editorformOptions.flags.HAS_PRIVS;
    this.useSimple = this.editorformOptions.render.simple;

    // // Editorform flags
    // this.conditionNotFirst = (this.field.hasOwnProperty("EDITABLE_CONDITION") &&
    //   this.field["EDITABLE_CONDITION"] === "ConditionNotFirst");


    this.inputType = this.field.hasOwnProperty("INPUT_TYPE") ? this.field["INPUT_TYPE"] : 'text';

    this.generatedForm = this.generatorService.createFormControl({
      parentForm: this.form,
      formControl: this.formControl,
      editorform: this.field,
      bicsuiteObject: this.bicsuiteObject,
      formArrayIdentifier: this.editorformOptions.formArrayIdentifier,
      tabInfo: this.tabInfo
    });

    this.formControl = this.generatedForm.formControl;
    this.formArrayIndex = this.generatedForm.formArrayIndex;

    // console.log(this.formArrayIndex)

    // console.log(this.formControl.value)
    // this.editorformOptions.render.disabled ? this.formControl.disable() : this.formControl.enable();

    this.formSubscription = this.formControl.valueChanges.subscribe((newValue: string) => {
      // console.log(this.field.NAME);
      // // this.formControl.updateValueAndValidity({onlySelf: false, emitEvent: false})
      // console.log('has validator required :' +this.formControl.hasValidator(Validators.required));
      // console.log('valid?                 :' +this.formControl.valid);
      // console.log('has Error              :' + this.formControl.hasError('required'))
      // console.log(this.formControl)

      this.bicsuiteObject[this.field.NAME] = newValue;
      if (this.field.ON_CHANGE === "REFRESH_CONDITIONS" && this.tabInfo != undefined) {
        // console.log("refresh input condition")
        this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
      }
      if(this.field.ON_CHANGE_METHOD && this.tabInfo != undefined) {
        this.crudService.createOrGetCrud(this.tabInfo.TYPE).callMethod(
          this.field.ON_CHANGE_METHOD, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject, this.form.root);
      }
    });

  // this.formControl.va

  }


  getFormErrorMessage(formControl: AbstractControl) {
    return this.toolbox.getFormErrorMessage(formControl);
  }

  ngOnDestroy(): void {
    if (this.formSubscription !== undefined && !this.formSubscription.closed) {
      this.formSubscription.unsubscribe();
    }
    this.generatorService.destroyFormControl(this.generatedForm);

    // console.log("input destroyed: " + this.field.NAME)
  }
}
