import { SelectionModel } from '@angular/cdk/collections';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ChangeDetectorRef, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormArray, UntypedFormGroup } from '@angular/forms';
import { MatLegacyPaginator as MatPaginator, LegacyPageEvent as PageEvent } from '@angular/material/legacy-paginator';
import { Subscription } from 'rxjs';
import { EditorformFlags, EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { TableButtonsService } from '../../../services/table-buttons.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TableComponent implements OnInit, OnDestroy {

  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() forceDropline = false; // should a dropline always be included?
  @Input() tabInfo!: Tab;
  @Input() editorformOptions?: EditorformOptions;
  @Input() parentBicsuiteObject: any;

  @Input() parentHasLoaded: boolean = true;
  @Output() setTabHasLoaded = new EventEmitter<boolean>();

  tableForm: UntypedFormGroup = new UntypedFormGroup({});

  tableData: any[] = [];
  columnData: any[] = [];
  // parentBicsuiteObject: any;

  isEditorformTable: boolean = false;
  dragAndDrop: boolean = false;
  hasTable: boolean = false;
  dataRefreshSubscription?: Subscription;
  clipboardUpdateEmittor?: Subscription;
  loadNumber: number = -1;
  loadInterval: any;
  totalLoadedIndex: number = 0;

  tableHeight: number = 0; // in pixels
  pageEvent!: PageEvent;

  pageIndex: number = 0;
  pageSize: number = 40;
  lowValue: number = 0;
  highValue: number = 40;
  pageSizeOptions: number[] = [10, 20, 40, 80, 160];
  goTo: number = 1;
  pagesArray: number[] = [];


  // checklistSelection = new SelectionModel<any>(true /* multiple */);
  checklistSelection = new SelectionModel<any>(true /* multiple */, undefined, undefined, this.compareFunction.bind(this));

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  constructor(private cd: ChangeDetectorRef, private eventEmitter: EventEmitterService, private tableButtonService: TableButtonsService, private eventEmitterService: EventEmitterService) { }

  compareFunction(o1: any, o2: any): boolean {
    if(this.field && o1[this.field.BULK_COMPARE_VALUE]) {
      return o1[this.field.BULK_COMPARE_VALUE] == o2[this.field.BULK_COMPARE_VALUE]
    } else if(o1.ID) {
      return o1.ID == o2.ID
    } else {
      return o1.NAME == o2.NAME
    }
  }

  ngOnDestroy(): void {
    if (this.dataRefreshSubscription !== undefined) {
      this.dataRefreshSubscription.unsubscribe();
      this.form.removeControl(this.field.NAME);
    }
    if (this.clipboardUpdateEmittor !== undefined) {
      this.clipboardUpdateEmittor.unsubscribe();
    }
    clearInterval(this.loadInterval);
    // console.log('table destroyed')
  }
  goToPage() {
    this.paginator.pageIndex = this.goTo - 1;
    const event: PageEvent = {
      length: this.tableData.length,
      pageIndex: this.goTo - 1,
      pageSize: this.pageSize
    };
    // console.log(this.paginator.);
    this.getPaginatorData(event);
  }
  getPaginatorData(event: any) {
    // console.log(event.pageIndex)
    if (this.pageSize == event.pageSize) {
      // console.log("pageSize not changed")
      if (event.pageIndex === this.pageIndex + 1) {
        // console.log(event.pageIndex)
        // console.log(this.pageIndex)
        this.goTo += 1;
        this.lowValue = this.lowValue + this.pageSize;
        this.highValue = this.highValue + this.pageSize;
      }
      else if (event.pageIndex === this.pageIndex - 1) {
        // console.log(event.pageIndex)
        // console.log(this.pageIndex)
        this.goTo -= 1;
        this.lowValue = this.lowValue - this.pageSize;
        this.highValue = this.highValue - this.pageSize;
      } else if (event.pageIndex === this.pageIndex) {
        // console.log("same page index");
      }
      else {
        // console.log("jump to index: " + event.pageIndex)
        this.lowValue = event.pageIndex * this.pageSize;
        this.highValue = (event.pageIndex * this.pageSize) + this.pageSize;
        // console.log(this.pageIndex)
      }
      this.pageIndex = event.pageIndex;
    } else {
      this.pageSize = event.pageSize;
      this.goTo = 1;
      this.pageIndex = 0;
      this.lowValue = 0;
      this.highValue = this.lowValue + this.pageSize;
      this.paginator.firstPage();
      this.pagesArray = Array.from(new Array(Math.ceil(this.tableData.length / this.pageSize)), (x, i) => i + 1)
      // this.pageIndex = 0;
      // this.lowValue = 0;
      // this.highValue = this.lowValue + this.pageSize;

    }
    // console.log(event);
    // console.log("pageindex: " + this.pageIndex)
    // console.log("pageSize: " + this.pageSize)
    // console.log("lowValue: " + this.lowValue)
    // console.log("highValue: " + this.highValue)

    this.renderPage();
  }

  ngAfterViewInit() {
    // this.dataSource.paginator = this.paginator;
    // console.log(this.dataSource)
    this.pagesArray = Array.from(new Array(Math.ceil(this.tableData.length / this.pageSize)), (x, i) => i + 1)
  }


  renderPage() {
    clearInterval(this.loadInterval);

    this.loadNumber = 1; // first element is loaded instand
    const ITEMS_RENDERED_AT_ONCE = 10;
    const INTERVAL_IN_MS = 50;
    // console.log(this.loadnumber);
    if (this.loadNumber > -1) {
      this.loadInterval = setInterval(() => {
        // console.log(this.tab.MODE);
        if (this.loadNumber >= this.pageSize) {
          clearInterval(this.loadInterval);
        }

        // if(!this.isActive) {
        //   clearInterval(this.loadInterval);
        // }
        // console.log(this.loadNumber);
        // console.log(this.totalLoadedIndex);
        this.loadNumber += ITEMS_RENDERED_AT_ONCE;
      }, INTERVAL_IN_MS);
    }
  }

  setIndex(table: any[]) {
    let index = 0;
    for (let row of table) {
      row.rowIndex = index;
      index++;
    }
  }

  // TODO SET INDEX ON APPEND AND MOVE

  ngOnInit(): void {



    if (this.field.hasOwnProperty("DEFAULT_PAGE_SIZE")) {
      this.pageSize = this.field.DEFAULT_PAGE_SIZE;
      this.highValue = this.lowValue + this.pageSize;
    };
    this.hasTable = (this.bicsuiteObject[this.field.NAME] !== undefined)
    this.isEditorformTable = (this.field.hasOwnProperty("TYPE") && this.field.TYPE === "TABLE");
    this.dragAndDrop = (this.field.hasOwnProperty("DRAG_AND_DROP_SORTING") && (this.field.DRAG_AND_DROP_SORTING == true || this.field.DRAG_AND_DROP_SORTING == "true"));

    if (!this.hasTable) {
      return;
    }

    // update data after rechieving new data from the server
    this.dataRefreshSubscription = this.eventEmitter.bicsuiteRefreshEmitter.subscribe((tab: Tab) => {
      if (this.tabInfo != undefined && tab.ID === this.tabInfo.ID) {
        this.update();
      }
    });

    // update Table when clipboard was altered
    if (this.field.ENABLE_BULK == 'true') {
      this.clipboardUpdateEmittor = this.eventEmitter.clipboardEmitter.subscribe((tab: Tab) => {
        if (this.tabInfo != undefined && tab.ID === this.tabInfo.ID) {
          // console.log('update table')
          this.update(true);
        }
      });

      // check if compareNAME is set
      if(this.field.BULK_COMPARE_VALUE) {

      } else {
        if(!environment.production) {
          alert('BULK_COMPARE_VALUE is missing in field '+ this.field.NAME +' in '+ this.tabInfo.TYPE +'.json');
        }
      }
    }

    this.tableData = this.bicsuiteObject[this.field.NAME].TABLE;

    if (!this.parentBicsuiteObject) {
      this.parentBicsuiteObject = this.bicsuiteObject;
    }


    // console.log(this.bicsuiteObject)
    // console.log(this.parentBicsuiteObject)

    this.setIndex(this.tableData);
    // console.log(this.bicsuiteObject[this.field.NAME].TABLE)
    // this.dataSource = this.tableData.slice(0,20);

    // Check whether the field or bicsuite object has a table
    if (this.isEditorformTable) {
      this.columnData = this.field.TABLE.FIELDS;

      // performance killer when text forms are added
      this.form.registerControl(this.field.NAME, this.tableForm);
    }

    // this.tableForm.statusChanges.subscribe(()=> {
    //   console.log("sth changed lmao")
    // })
    this.totalLoadedIndex = this.tableData.length;
    this.loadNumber = -1;


    // this.tableHeight = this.tableData.length > 20 ? 20 * 42 : this.tableData.length * 42 + 40; // 42px each line, 40px header 600 max size of table

    this.renderPage();

    this.setCheckListSelection();
  }

  setCheckListSelection() {
    if(this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList']) {
      // if selectionlist is already there -> use this
      let newSelectedObjects = this.tableData.filter((data)=>{
        let found: boolean = false;
        for(let o of  this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList']) {
          if (this.field.BULK_DISABLED_IDENTIFIER  && data[this.field.BULK_DISABLED_IDENTIFIER] ||
              this.field.BULK_INVISIBLE_IDENTIFIER && data[this.field.BULK_INVISIBLE_IDENTIFIER]) {
            return false;
          }
          this.compareFunction(o,data) ? found = true : '';
        }
        return found;
      });
      // console.log(newSelectedObjects);
      this.checklistSelection.clear();
      this.checklistSelection.setSelection(...newSelectedObjects);
      this.cd.detectChanges();
    } else {
      // if selectionlist is not present -> create
      this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList'] = []
    }
  }

  rowClick(bicsuiteObjectRow: any, editorform: any, rowIndex: number) {
    // console.log("clicked")
    // console.log(bicsuiteObjectRow, editorform, rowIndex)
    // console.log(editorform);
    this.tableButtonService.rowExecute(editorform, bicsuiteObjectRow, this.field, this.bicsuiteObject, rowIndex, this.tabInfo).then((result: boolean) => {
      if (result) {
        // Update Table
        this.tableForm.markAsDirty();
        this.update();
      }
    });
  }

  loadLock(btnInfo: any) {
    if (btnInfo.LOCK_LOADING) {
      this.setTabHasLoaded.next(false);
    }
  }
  loadUnLock(btnInfo: any) {
    if (btnInfo.LOCK_LOADING) {
      this.setTabHasLoaded.next(true);
    }
  }
  // Table-wide functions
  clickCallback($event: any) {
    this.loadLock($event);
    this.tableButtonService.tableExecute($event, this.bicsuiteObject, this.field, this.tabInfo, this.form, this.checklistSelection).then((changedRows: number) => {
      // console.log(this.bicsuiteObject)
      // console.log(changedRows)
      if (changedRows > 0) {
        // console.log("asdasd")
        this.tableForm.markAsDirty();
        this.update();
      }
    }).then(
      (res: any) => {
        if ($event.VALUE == "APPEND") {
          if (this.paginator) {
            this.paginator.lastPage();
          }
        }
        this.loadUnLock($event);
        return res;
      },
      (rejection: any) => {
        this.loadUnLock($event);
        return rejection;
    });
  }

  itemSelectionToggle(obj: any): void {
    // console.log(obj)
    // TODO filter checklistselection so that not all information is standing in the localstorage
    this.checklistSelection.toggle(obj);
    // this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
    // console.log(this.checklistSelection.selected);
    this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList'] = this.checklistSelection.selected;
    // console.log( this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList']);
    this.eventEmitterService.saveTabEmitter.next();
  }

  update(noClearSelection?: boolean): void {
    if(!noClearSelection){
      this.checklistSelection.clear();
    };
    this.tableData = this.bicsuiteObject[this.field.NAME].TABLE;
    this.setIndex(this.tableData);
    this.eventEmitter.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
    // Some properties are not visible by the form. Thus the bicsuite object has be checked in the validationhelper if it is still valid
    this.form.updateValueAndValidity({ onlySelf: true, emitEvent: true });
    this.setCheckListSelection();
    this.cd.detectChanges();
    // console.log(this.form.valid)
  }

  confirmDragAndDrop(event: CdkDragDrop<any>) {
    moveItemInArray(this.bicsuiteObject[this.field.NAME].TABLE, event.previousIndex, event.currentIndex);
    this.update();
  }

  renderTableData() {
    // console.log(this.loadNumber);
    // console.log(this.totalLoadedIndex);
    //   clearInterval(this.loadInterval);

    //   this.loadNumber = 1; // first element is loaded instand
    //   const ITEMS_RENDERED_AT_ONCE = 5;
    //   const INTERVAL_IN_MS = 50;
    //   // console.log(this.loadnumber);
    //   if (this.loadNumber > -1) {
    //     this.loadInterval = setInterval(() => {
    //       // console.log(this.tab.MODE);
    //       if (this.loadNumber >= this.totalLoadedIndex) {
    //         clearInterval(this.loadInterval);
    //       }

    //       // if(!this.isActive) {
    //       //   clearInterval(this.loadInterval);
    //       // }
    //       console.log(this.loadNumber);
    //       console.log(this.totalLoadedIndex);
    //       this.loadNumber += ITEMS_RENDERED_AT_ONCE;
    //     }, INTERVAL_IN_MS);
    //   }
  }

  copyColumn(column: any): any {
    if (column.COPY_COLUMN == true || column.COPY_COLUMN == "true") {
      return JSON.parse(JSON.stringify(column));
    }
    else {
      return column;
    }
  }
  setHasLoaded(hasLoaded: boolean){
    this.setTabHasLoaded.next(hasLoaded);
  }
}

