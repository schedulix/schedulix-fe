import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { AbstractControl, FormArray, UntypedFormControl, UntypedFormGroup, ValidationErrors, ValidatorFn } from '@angular/forms';
import { MatLegacyCheckboxChange as MatCheckboxChange, LegacyTransitionCheckState as TransitionCheckState } from '@angular/material/legacy-checkbox';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Subscription } from 'rxjs';
import { ValidationHelper } from 'src/app/classes/formgenerator/validation-helper';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { Crud } from 'src/app/interfaces/crud';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';
import { ChooserDialogComponent, ChooserDialogData } from '../chooser/chooser-dialog/chooser-dialog.component';

@Component({
  selector: 'app-file-input',
  templateUrl: './file-input.component.html',
  styleUrls: ['./file-input.component.scss']
})
export class FileInputComponent implements OnInit {

  @Input() tabInfo?: Tab;
  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() formArrayIdentifier?: string;
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() parentBicsuiteObject: any;

  formControl: UntypedFormControl = new UntypedFormControl();
  formArrayIndex: number = -1;
  formSubscription?: Subscription;
  values: string[] = [];
  generatedFormControl: FormGeneratorServiceControl = new FormGeneratorServiceControl();
  inputType: string = 'file';
  file: any;

  constructor(private toolbox: ToolboxService, private generatorService: FormGeneratorService, private privilegesService: PrivilegesService, private eventEmitterService: EventEmitterService) { }

  ngOnInit(): void {
    this.generatedFormControl = this.generatorService.createFormControl({
      parentForm: this.form,
      formControl: this.formControl,
      editorform: this.field,
      bicsuiteObject: this.bicsuiteObject,
      formArrayIdentifier: this.editorformOptions.formArrayIdentifier
    });
    this.formControl = this.generatedFormControl.formControl;
    this.formArrayIndex = this.generatedFormControl.formArrayIndex;
  }

  fileChanged(e: any) {
    let target: any = e.target;
    if (!target || !target.files || target.files.length == 0) {
      this.bicsuiteObject[this.field.NAME] = null;
    }
    else {
      this.bicsuiteObject[this.field.NAME] = target.files[0];
    }
  }

  validateFormWithValue() {
    // do change on the FIELD to trigger deepCompare for save button eneble/disable
    this.formControl.setValue(this.formControl.value);
  }

  getFormErrorMessage(formControl: AbstractControl) {
    return this.toolbox.getFormErrorMessage(formControl);
  }

  ngOnDestroy(): void {
    if (this.formSubscription !== undefined && !this.formSubscription.closed) {
      this.formSubscription.unsubscribe();
    }
    this.generatorService.destroyFormControl(this.generatedFormControl);

    // console.log("input destroyed: " + this.field.NAME)
  }
}
