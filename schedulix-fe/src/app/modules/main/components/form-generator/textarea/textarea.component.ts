import { Component, DoCheck, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormGroup, UntypedFormControl, AbstractControl, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab } from 'src/app/data-structures/tab';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { FormGeneratorService, FormGeneratorServiceControl } from '../../../services/form-generator.service';
import { PrivilegesService } from '../../../services/privileges.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { CRUDService } from 'src/app/services/crud.service';

@Component({
  selector: 'app-textarea',
  templateUrl: './textarea.component.html',
  styleUrls: ['./textarea.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TextareaComponent implements OnInit {

  @Input() field: any; // field property from editorform
  @Input() bicsuiteObject: any; // records from server
  @Input() form: UntypedFormGroup = new UntypedFormGroup({}); // form component in which this input is
  @Input() useSimple: boolean = false; // use simple or material input
  @Input() editorformOptions: EditorformOptions = new EditorformOptions();
  @Input() formField: UntypedFormControl = new UntypedFormControl(undefined); // formcontrol representation of this input (can be set by parent component)
  @Input() tabInfo?: Tab;
  @Input() parentBicsuiteObject: any;

  formSubscription: Subscription | undefined;
  formArrayIndex: number = -1;
  generatedForm: FormGeneratorServiceControl = new FormGeneratorServiceControl();

  isHidden: boolean = false;
  hasGrant: boolean = false;
  conditionNotFirst: boolean = false;

  rows = 1;
  cols = 50;

  constructor(private toolbox: ToolboxService, private generatorService: FormGeneratorService, private privilegesService: PrivilegesService, private eventEmitterService: EventEmitterService, private crudService: CRUDService) { }

  ngOnInit(): void {
    // if (this.tabInfo !== undefined) {
    //   // this.hasGrant = this.privilegesService.valide(this.field.PRIVS, this.tabInfo.PRIVS, this.bicsuiteObject, this.tabInfo);
    //   // this.hasGrant = this.editorformOptions.render.disabled;
    //   this.editorformOptions.render.disabled ? this.formField.disable() : this.formField.enable();
    // }

    // Editorform flags
    this.conditionNotFirst = (this.field.hasOwnProperty("EDITABLE_CONDITION") &&
      this.field["EDITABLE_CONDITION"] === "ConditionNotFirst");
    this.useSimple = this.editorformOptions.render.simple;


    if (this.field.COLUMNS !== undefined)
      this.cols = this.field.COLUMNS;

    if (this.field.ROWS !== undefined)
      this.rows = this.field.ROWS;

    this.generatedForm = this.generatorService.createFormControl({
      parentForm: this.form,
      formControl: this.formField,
      editorform: this.field,
      bicsuiteObject: this.bicsuiteObject,
      formArrayIdentifier: this.editorformOptions.formArrayIdentifier
    });

    this.formField = this.generatedForm.formControl;
    this.formArrayIndex = this.generatedForm.formArrayIndex;

    this.formSubscription = this.formField.valueChanges.subscribe((newValue: string) => {
      this.bicsuiteObject[this.field.NAME] = newValue;
      if (this.field.ON_CHANGE === "REFRESH_CONDITIONS" && this.tabInfo != undefined) {
        // console.log("refresh input condition")
        this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
      }
      if(this.field.ON_CHANGE_METHOD && this.tabInfo != undefined) {
        this.crudService.createOrGetCrud(this.tabInfo.TYPE).callMethod(
          this.field.ON_CHANGE_METHOD, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject, this.form.root);
      }
    });
  }

  getFormErrorMessage(formControl: AbstractControl) {
    return this.toolbox.getFormErrorMessage(formControl);
  }

  ngOnDestroy(): void {
    if (this.formSubscription !== undefined && !this.formSubscription.closed) {
      this.formSubscription.unsubscribe();
    }

    this.generatorService.destroyFormControl(this.generatedForm);
  }
}
