import { actionHandler } from "./actionHandlerEnum";
import { createType } from "./createType";

export interface graphState {

  focusedNode: any;
  ignoreDependentChildrenCoherences: boolean;
  hasCycle: boolean;
  isVerticalTreeHierachy: boolean;
  actionHandler: actionHandler;
  hierarchyType: string;
  createEdgeType: createType;
  loading: boolean;
  graphInitialized: boolean;
  dataSourceFreeToRender: boolean;
}
