import { FlatTreeControl } from '@angular/cdk/tree';
import { AfterViewChecked, AfterViewInit, Component, ErrorHandler, HostListener, Inject, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { notDeepStrictEqual } from 'assert';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { TreeFunctionsService } from '../../services/tree-functions.service';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { Observable, Subject, Subscription } from 'rxjs';
import { Alignment, ClusterNode, DagreClusterLayout, DagreLayout, DagreNodesOnlyLayout, Edge, Node, Orientation, PanningAxis } from '@swimlane/ngx-graph';
import { treeNode } from 'src/app/interfaces/tree';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { EditorformDialogComponent, EditorformDialogData } from '../form-generator/editorform-dialog/editorform-dialog.component';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { ChooserDialogComponent, ChooserDialogData } from '../form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { HierarchyService } from './hierarchy.service';
import { FormFlatNode, simpleFormNode } from './hierachyInterfaces';
import { GraphHierarchyComponent } from './graph-hierarchy/graph-hierarchy.component';
import { CollectionViewer } from '@angular/cdk/collections';
import { actionHandler } from './actionHandlerEnum';
import { createType } from './createType';
import { graphState } from './IGraphState';
import { hierarchyDependencyOptions } from './IDependencyOptions';

@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss']
})

export class HierarchyComponent implements OnInit, OnDestroy {
  private _transformer = (node: simpleFormNode, level: number) => {
    return {
      data: node.data,
      path: node.path,
      expandable: !!node.children && node.children.length > 0,
      // name: node.name,
      level: level,
      levelArray: new Array(level)
    };
  }

  treeControl = new FlatTreeControl<FormFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  arrowContainerMindWidth: number = 0;
  arrowWidth: number = 20;

  data: any;

  initialGraphCenter: boolean = true;

  private selectedTabSubscription?: Subscription;

  private rePosGraphSubscirption?: Subscription;

  @ViewChild(GraphHierarchyComponent) hierachyComponent?: GraphHierarchyComponent;

  // Needed for lazy loading tabs (see selectedTabSubscription).
  private isInitialized = false;

  zoomToFit$: Subject<boolean> = new Subject();
  autoCenter: boolean = false;
  draggingEnabled: boolean = false;
  panningAxis: PanningAxis = PanningAxis.Both;
  maxZoomLevel = 80.0;
  // zoomLevel = 30;
  autoZoom = false;

  cluster: any[] = [];
  clusters: any[] = [];
  graphNodes: any[] = [];
  graphEdges: any[] = [];
  customDagreLayout = new DagreClusterLayout();

  update$: Subject<any> = new Subject();
  center$: Subject<any> = new Subject();
  dependencyOpions: hierarchyDependencyOptions = {
    unresolvedHandling: 'ERROR',
    unresolvedHandlingOptions: ['ERROR', 'SUSPEND', 'IGNORE', 'DEFER', 'DEFER_IGNORE'],
    dependencyMode: 'ALL_FINAL',
    dependencyModeOptions: ['ALL_FINAL', 'JOB_FINAL'],
    stateSelection: 'ALL_REACHABLE',
    stateSelectionOptions: ['FINAL', 'ALL_REACHABLE', 'UNREACHABLE', 'DEFAULT']
  }


  // this.stateSelection, this.unresolvedHandling, this.dependencyMode

  isStatic: boolean = true;

  internalExternal: string = 'INTERNAL';
  internalExternalOptions = ['INTERNAL', 'EXTERNAL']
  cyclesChecked: boolean = false;

  graphState: graphState = {
    isVerticalTreeHierachy: false,
    hasCycle: false,
    focusedNode: {},
    hierarchyType:'DEFINITION',
    actionHandler: actionHandler.NONE,
    createEdgeType: createType.NONE,
    loading:false,
    ignoreDependentChildrenCoherences: false,
    graphInitialized: false,
    dataSourceFreeToRender: true
  }

  constructor(
    public eventEmitterService: EventEmitterService,
    public commandApiService: CommandApiService,
    private treeFunctionsService: TreeFunctionsService,
    public toolboxService: ToolboxService,
    private dialog: MatDialog,
    private hierachyService: HierarchyService,
  ) { }

  @Input() tab!: Tab;
  @Input() isActive: boolean | undefined;

  hasChild = (_: number, node: FormFlatNode) => node.expandable;

  parseInt = Number.parseInt;

  // hierarchyType: string = 'DEFINITION'
  // actionHandler: actionHandler = actionHandler.NONE;
  // createEdgeType: createType = createType.NONE;
  htmlActionHandlerType = actionHandler;
  htmlCreateEdgeType = createType;
  // loading: boolean = false;

  // graphInitialized: boolean = false;

  // ignoreDependentChildrenCoherences: boolean = false;

  public refreshDataCallback: Function = this.refreshData.bind(this);
  public loadDataCallback: Function = this.loadData.bind(this);
  public focusNodeFromGraphCallback: Function = this.focusNodeFromGraph.bind(this)
  public focusNodeFromTreeCallback: Function = this.focusNodeFromTree.bind(this)
  public toggleHierarchyViewCallback: Function = this.toggleHierarchyView.bind(this);
  public clickNodeCallback: Function = this.clickNode.bind(this);
  public clearFocusCallback: Function = this.clearFocus.bind(this);
  public openActionHandlerCallback: Function = this.openActionHandler.bind(this);
  public clearMenuSelectionCallback: Function = this.clearMenuSelection.bind(this);

  prevExpansionModel = this.treeControl.expansionModel.selected;
  dataSourceObserver: Subscription = new Subscription();

  dependencyData: any[] = [];
  hierarchyData: any[] = [];



  ngOnInit(): void {
    console.log(this.graphState.actionHandler)
    this.customDagreLayout.settings.orientation = Orientation.TOP_TO_BOTTOM;
    this.customDagreLayout.settings.rankPadding = 500;
    this.customDagreLayout.settings.edgePadding = 30;
    // this.customDagreLayout.settings.multigraph = true;
    // this.customDagreLayout.settings.
    // this.customDagreLayout.settings.ranker = "network-simplex";
    // this.customDagreLayout.settings.ranker = "longest-path";
    // this.customDagreLayout.settings.compound = true;
    // this.customDagreLayout.settings.align = Alignment.CENTER;


    console.log("init")

    this.selectedTabSubscription = this.eventEmitterService.selectedTabEmitter.subscribe((result: string) => {

      if (this.tab.ID == result && !this.isInitialized) {
        this.data = this.tab.DATA
        this.graphState.loading = true;
        this.loadData().then((res) => {
          this.graphState.loading = false;
        });
        this.isInitialized = true;
      } else if (this.isActive) {

      } else {
        //  change back to true since the graph is destroyed
      }
      // if (this.tab.DATA != '') {

      // }
    });

    this.rePosGraphSubscirption = this.eventEmitterService.rePosNoFormEditors.subscribe(() => {
      if (this.isActive) {
        // console.log("outside resize")
        window.dispatchEvent(new Event('resize'));
      }
    });

    let collectionView: CollectionViewer = { viewChange: new Observable() }

    // console.log('new datasource observer')

    this.dataSourceObserver = this.dataSource.connect(collectionView).subscribe((visibleDatasource) => {
      this.hierachyService.indexVisibleDataSource(visibleDatasource);
    });

  }

  refreshData(): Promise<any> {
    this.graphState.loading = true;
    // this.ignoreDependentChildrenCoherences = ignoreDependentChildrenCoherences;
    return this.loadData().then((res) => {
      this.graphState.loading = false;
    });
  }

  refresh() {
    this.refreshData();
  }

  openActionHandler(handler: actionHandler) {
    if (this.graphState.actionHandler == handler) {
      console.log("close")
      this.clearFocus();
      this.clearMenuSelection();
      return;
    }

    // if not close set correct handler and createType
    this.graphState.actionHandler = handler;
    this.graphState.createEdgeType = createType.NONE;

    if (handler == actionHandler.CREATE_DEPENDENCY) {
      this.clearFocus();
      this.graphState.createEdgeType = createType.CREATE_DEPENDENT;
      this.eventEmitterService.pushSnackBarMessage(['Please select required node'], SnackbarType.INFO);
    }
    if (handler == actionHandler.CREATE_EXTERNAL_DEPENDENT) {
      this.clearFocus();
      this.graphState.createEdgeType = createType.CREATE_EXTERNAL_DEPENDENT;
      this.eventEmitterService.pushSnackBarMessage(['Please select dependent node'], SnackbarType.INFO);
    }
    if (handler == actionHandler.ADD_CHILDREN) {
      this.clearFocus();
      this.graphState.createEdgeType = createType.ADD_CHILDREN;
      this.eventEmitterService.pushSnackBarMessage(['Please select Parent node'], SnackbarType.INFO);
    }
  }

  clickNode(node: any, graphState: graphState) {
    // console.log(graphState.createEdgeType);
    if(node == this.graphState.focusedNode && createType.CREATE_REQUIRED == graphState.createEdgeType) {
      this.eventEmitterService.pushSnackBarMessage(['dependency to same object is not allowed'], SnackbarType.INFO);
      return;
    }

    if (graphState.createEdgeType == createType.NONE) {
      this.focusNodeFromGraph(node)
    } else if (node.data.TYPE == 'EXTERNAL' || node.data.TYPE == 'INTERNAL') {
      // this.eventEmitterService.pushSnackBarMessage(['external nodes are not allowed to select'], SnackbarType.INFO);
      return;
    } else if (createType.CREATE_DEPENDENT == graphState.createEdgeType) {

      // when user has selected the create dependency - select first node, not allowed with external depencencies
      // set createtype to create required
      graphState.createEdgeType = createType.CREATE_REQUIRED;
      this.focusNodeFromGraph(node);
      this.eventEmitterService.pushSnackBarMessage(['node selected, select required node to create dependency'], SnackbarType.INFO);
    }  else if (createType.CREATE_REQUIRED == graphState.createEdgeType) {
      this.hierachyService.createFocusedEdge(node, graphState.focusedNode, createType.CREATE_DEPENDENT, this.dependencyOpions.stateSelection, this.dependencyOpions.unresolvedHandling, this.dependencyOpions.dependencyMode).then((() => {
        this.refreshData();
        graphState.createEdgeType = createType.CREATE_DEPENDENT;
        this.clearFocus();
      }));
    } else if (createType.CREATE_EXTERNAL_DEPENDENT == graphState.createEdgeType) {
      this.focusNodeFromGraph(node);
      this.createChildrenOrDependency(actionHandler.CREATE_DEPENDENCY);
      console.log(graphState.actionHandler)
    } else if (createType.ADD_CHILDREN == graphState.createEdgeType) {
      this.focusNodeFromGraph(node);
      this.createChildrenOrDependency(graphState.actionHandler);
    }
  }
  focusNodeFromTree(node: any) {
    // console.log(node)
    if (node.data.isExternalDependency == true) {
      // console.warn("isExternal");
      return
    }
    this.unFocusNodes();
    node.data.focused = true;
    this.graphState.focusedNode = node;
  }

  focusNodeFromGraph(node: any) {
    if (node.data.isExternalDependency == true) {
      // console.warn("isExternal");
      return
    }
    this.unFocusNodes();
    node.data.focused = true;
    this.graphState.focusedNode = node;
  }

  clearFocus() {
    this.unFocusNodes();
    this.graphState.focusedNode = {};
  }


  unFocusEdges(graphEdges: any[]) {
    for (let edge of graphEdges) {
      edge.data.focused = false;
    }
  }

  unFocusNodes() {
    if (this.hierachyComponent) {
      let nodes = this.hierachyComponent.graphNodes;
      for (let node of nodes) {
        node.data.focused = false;
      }
    }
    let graphNodes = this.dataSource.data;
    for (let node of graphNodes) {
      node.data.focused = false;
    }
  }

  openSelectedItemDefinition() {

    console.log(this.graphState.focusedNode)
    let path1 = this.graphState.focusedNode.data.NAME.split('.');
    path1.pop();
    path1 = path1.join('.')
    let definedTab1 = new Tab(this.graphState.focusedNode.data.NAME.split('.').pop(), this.graphState.focusedNode.data.ID, this.graphState.focusedNode.data.TYPE, this.graphState.focusedNode.data.TYPE.toLowerCase(), TabMode.EDIT);
    definedTab1.PATH = path1;
    this.eventEmitterService.createTab(definedTab1, true);
  }

  ngOnDestroy(): void {
    // console.log('destroy');
    this.selectedTabSubscription?.unsubscribe();
    this.rePosGraphSubscirption?.unsubscribe();
    // this.dataSourceObserver.unsubscribe();
    // this.removeLines();
  }

  loadData(): Promise<any> {
    // set expandData of previos selection
    this.prevExpansionModel = this.treeControl.expansionModel.selected;
    this.cyclesChecked = false;
    return Promise.all([
      this.executeHierachyStmt(),
      this.executeDependencyStmt()
    ]).then((results: any[]) => {
      this.processResults(results);
    }, (rejection: any) => {
      console.log(rejection)
      this.graphState.loading = false;
      this.cyclesChecked = true;
    });
  }

  executeHierachyStmt(): Promise<any> {
    let hierarchyStmt = 'list job definition hierarchy ' + this.toolboxService.namePathQuote(this.data.PATH + '.' + this.data.ORIGINAL_NAME) + ' with expand = ALL';
    return this.commandApiService.execute(hierarchyStmt).then((result: any) => {
      // if (result.ERROR) {
      //   console.warn('has error')
      //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
      // }
      // console.log(result)
      // i
      // console.log(result)
      return result;
    });
  }

  executeDependencyStmt(): Promise<any> {
    // console.log(this.data)
    let dependencyStmt = ' list dependency definition ' + this.toolboxService.namePathQuote(this.data.PATH + '.' + this.data.ORIGINAL_NAME);
    return this.commandApiService.execute(dependencyStmt).then((result: any) => {
      // if (result.ERROR) {
      //   console.warn('has error')
      //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
      // }
      // console.log(result)
      return result;
    });
  }

  processResults(results: any[]) {
    // disabled autozoom before loading data in graph value holder
    console.log(new Date())
    // console.log("data provided")
    this.hierarchyData = results[0].DATA.TABLE;
    this.dependencyData = results[1].DATA.TABLE;
    // console.log("data assigned")
    // build child
    this.hierachyService.buildUndefinedHierachyTreeNodes(this.dependencyData, this.hierarchyData);
    let nestedHierarchyDataTree = this.hierachyService.buildHierarchyTreeFromList(this.hierarchyData);
    // console.log(nestedHierarchyDataTree)
    // console.log("nested tree built")
    console.log(new Date())
    // console.log(nestedHierarchyDataTree)
    // add external nodes to datasource
    // function implace

    this.hierachyService.buildExternalHierachayTreeListNodes(this.dependencyData, nestedHierarchyDataTree);
    // console.log("build external Nodes")
    this.dataSource.data = nestedHierarchyDataTree;
    // console.log("external Nodes data assigend")
    console.log(new Date())

    let topo = this.hierachyService.kahnsTopologicSort(this.dependencyData, this.hierarchyData, this.dataSource.data).then((topo) => {
      // console.log(topo)
      this.graphState.hasCycle = topo.hascycle;
      this.cyclesChecked = true;


      if (!this.graphState.hasCycle) {
        console.log(new Date())
        // console.log("start sort nodes")

        // tell graph that it is only sort and dont has to be rerendered
        this.graphState.dataSourceFreeToRender = false;
        this.dataSource.data = this.hierachyService.sortNodes(this.dataSource.data);

        // after setting the datasource(new) it has to be expanded again
        this.expandGraphNodes();
      } else {
        this.hierachyService.setTopologicResultsInTree(this.dataSource.data, topo.sortorder, topo.cycleNodes);
        this.hierachyService.setTopologicResultsInGraph(this.hierarchyData, topo.sortorder, topo.cycleNodes);
      }
    });

    // console.log("sort nodes")
    // set datasource to initiate rendering
    // this.dataSource.data = this.hierachyService.sortNodes(this.dataSource.data);

    // console.log(new Date())
    // console.log("start indexing tree")
    this.hierachyService.indexTreeControl(this.treeControl);

    let size = 200;
    // console.log("calculate size")
    if (this.dependencyData.length) {
      size = 30 * Math.sqrt(this.dependencyData.length);
      if (size > 700) {
        size = 700;
      }
    }
    this.customDagreLayout.settings.rankPadding = size;
    window.dispatchEvent(new Event('resize'));

    this.expandGraphNodes();
  }

  expandGraphNodes() {
    setTimeout(() => {
      this.graphState.dataSourceFreeToRender = true;
      if (this.prevExpansionModel.length < 1) {
        // expand all level < 2
        // console.log("expand nodes")
        for (let node of this.treeControl.dataNodes) {
          if ((node.level < 1 && node.level > -1) && node.expandable) {
            // triggers rerendering of tree
            // console.log('expand')
            // console.log(node.expandable)
            this.treeControl.expand(node)
          }
        }
      } else {
        // console.log("expand nodes from prev expansion model")
        // expand all in prevexpansionModel
        for (let node of this.treeControl.dataNodes) {
          let foundInPrevExpension = this.prevExpansionModel.find((expandedNode) => {
            if (expandedNode.data.HIERARCHY_PATH == node.data.HIERARCHY_PATH) {
              return true;
            }
            return false;
          })
          if (foundInPrevExpension && node.expandable) {
            // triggers rerendering of tree
            // console.log('expand')
            // console.log(node.expandable)
            this.treeControl.expand(node)
            // this.treeControl.expansionModel.setSelection()
          }
        }
      }
      // triggers centering of graph if loaded initial
      this.graphState.graphInitialized = true;
    }, 50);
  }

  toggleHierarchyView(mode: string) {
    // mode comes from vertical hierachy comp
    // console.log(mode)
    this.clearMenuSelection();
    if (mode == 'workflow') {
      this.graphState.graphInitialized = false;
      this.graphState.ignoreDependentChildrenCoherences = true;
      this.graphState.isVerticalTreeHierachy = false;
      // this.buildGraph(this.hierarchyData, this.dependencyData);
      this.customDagreLayout.settings.orientation = Orientation.LEFT_TO_RIGHT;
      setTimeout(() => {
        // triggers centering of graph if loaded initial
        this.graphState.graphInitialized = true;
      }, 2);
    }
    if (mode == 'graph') {
      this.graphState.graphInitialized = false;
      this.graphState.ignoreDependentChildrenCoherences = false;
      this.graphState.isVerticalTreeHierachy = false;
      // this.buildGraph(this.hierarchyData, this.dependencyData);
      this.customDagreLayout.settings.orientation = Orientation.TOP_TO_BOTTOM;
      setTimeout(() => {
        // triggers centering of graph if loaded initial
        this.graphState.graphInitialized = true;
      }, 2);
    }
    if (mode == 'tree') {
      this.graphState.isVerticalTreeHierachy = true;
    }

    // this.
  }

  createChildrenOrDependency(handler: actionHandler) {
    this.hierachyService.openBatchesAndJobChooser(handler,
      { isStatic: this.isStatic },
      this.graphEdges,
      this.graphNodes,
      this.graphState.focusedNode,
      this.tab,
      this.refreshData.bind(this),
      this.dependencyOpions.stateSelection,
      this.dependencyOpions.unresolvedHandling,
      this.dependencyOpions.dependencyMode).then((res) => {
        this.clearFocus();
      });
  }


  setDeletionMode(handler: actionHandler) {
    if (this.graphState.actionHandler == handler) {
      this.graphState.actionHandler = actionHandler.NONE
      this.graphState.createEdgeType = createType.NONE;
    } else {
      this.graphState.actionHandler = handler;
      this.graphState.createEdgeType = createType.NONE;
    }
    console.log(this.graphState.actionHandler)
  }

  createEdge(handler: actionHandler, edgeCreateType: createType) {
    this.graphState.actionHandler = actionHandler.NONE;
    if (this.graphState.createEdgeType == edgeCreateType) {
      this.graphState.createEdgeType = createType.NONE
      return;
    } else {
      this.graphState.createEdgeType = edgeCreateType;
    }
    if ((handler == actionHandler.CREATE_DEPENDENCY) && edgeCreateType == createType.CREATE_REQUIRED) {
      this.eventEmitterService.pushSnackBarMessage(["Please select required node"], SnackbarType.INFO);
    }
    if ((handler == actionHandler.CREATE_DEPENDENCY) && edgeCreateType == createType.CREATE_DEPENDENT) {
      this.eventEmitterService.pushSnackBarMessage(["Please select dependent node"], SnackbarType.INFO);
    }
  }

  getRootParent(node: any): any {
    if (node.data.parent) {
      return this.getRootParent(node.data.parent)
    } else {
      return node;
    }
  }

  clearMenuSelection() {
    this.graphState.createEdgeType = createType.NONE;
    this.graphState.actionHandler = actionHandler.NONE;

    this.graphState = [...[this.graphState]][0];
  }

}


// enum actionHandler {
//   CREATE_DEPENDENCY,
//   DELETE_DEPENDENCY,
//   CREATE_TRIGGER,
//   DELETE_TRIGGER,
//   ADD_CHILDREN,
//   REMOVE_CHILDREN,
//   NONE
// }

// enum createType {
//   CREATE_REQUIRED,
//   CREATE_DEPENDENT,
//   NONE
// }
