import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GraphHierarchyComponent } from './graph-hierarchy.component';

describe('GraphHierarchyComponent', () => {
  let component: GraphHierarchyComponent;
  let fixture: ComponentFixture<GraphHierarchyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GraphHierarchyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GraphHierarchyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
