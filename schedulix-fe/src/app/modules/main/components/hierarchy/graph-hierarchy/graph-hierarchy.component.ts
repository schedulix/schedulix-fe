import { CollectionViewer } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { AfterViewInit, Component, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatTreeFlatDataSource } from '@angular/material/tree';
import { DagreClusterLayout, Edge, Orientation, PanningAxis } from '@swimlane/ngx-graph';
import { Observable, Subject, Subscription } from 'rxjs';
import { Tab } from 'src/app/data-structures/tab';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { ChooserDialogComponent, ChooserDialogData } from '../../form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { actionHandler } from '../actionHandlerEnum';
import { createType } from '../createType';
import { FormFlatNode, simpleFormNode } from '../hierachyInterfaces';
import { HierarchyService } from '../hierarchy.service';
import { graphState } from '../IGraphState';
import { hierarchyDependencyOptions } from '../IDependencyOptions';

@Component({
  selector: 'app-graph-hierarchy',
  templateUrl: './graph-hierarchy.component.html',
  styleUrls: ['./graph-hierarchy.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GraphHierarchyComponent implements OnInit, AfterViewInit, OnDestroy, OnChanges {

  constructor(public toolboxService: ToolboxService, public eventEmitterService: EventEmitterService, public commandApiService: CommandApiService,
    private dialog: MatDialog, private hierachyService: HierarchyService) { }

  update$: Subject<any> = new Subject();
  center$: Subject<any> = new Subject();
  zoomToFit$: Subject<boolean> = new Subject();
  autoCenter: boolean = true;
  autoZoom: boolean = true;
  draggingEnabled: boolean = false;
  panningAxis: PanningAxis = PanningAxis.Both;
  maxZoomLevel = 80.0;
  zoomLevel = 0.7;


  @Input() tab!: Tab;

  cluster: any[] = []
  @Input() clusters!: any[];
  @Input() graphNodes!: any[];
  @Input() graphEdges!: any[];
  @Input() graphState!: graphState;
  @Input() dependencyOpions!: hierarchyDependencyOptions;

  @Output() graphStateChange = new EventEmitter<graphState>();

  @Input() cyclesChecked!: boolean;

  @Input() dataSource!: MatTreeFlatDataSource<any, any>;
  @Input() dependencyData!: any[];
  @Input() hierarchyData!: any[];


  htmlActionHandlerType = actionHandler;
  htmlCreateEdgeType = createType;

  initialized: boolean = false;


  @Input() public openActionHandler!: Function;
  @Input() public clickNode!: Function;
  @Input() public refreshData!: Function;
  @Input() public focusNode!: Function;
  @Input() public clearFocus!: Function;
  @Input() public clearSelection!: Function;
  @Input() public toggleHierarchyView!: Function
  @Input() treeControl!: FlatTreeControl<FormFlatNode>; // Editorform, used to generate a form.
  @Input() customDagreLayout = new DagreClusterLayout();

  dataSourceObserver: Subscription = new Subscription();

  ngOnChanges(): void {
    // console.log(this.graphState);
  }
  newDataArrivedTimeout: any;
  ngOnInit(): void {

    // console.log(this.graphState.actionHandler)
    if (this.graphState.ignoreDependentChildrenCoherences) {
      this.customDagreLayout.settings.orientation = Orientation.LEFT_TO_RIGHT;
    } else {
      this.customDagreLayout.settings.orientation = Orientation.TOP_TO_BOTTOM;
    }

    let collectionView: CollectionViewer = { viewChange: new Observable() }

    // console.log('new datasource observer')

    this.dataSourceObserver = this.dataSource.connect(collectionView).subscribe((visibleDatasource) => {


      // console.log('alter graph');
      // console.log(this.graphState.dataSourceFreeToRender);

      if (this.graphState.dataSourceFreeToRender) {
        // console.log(visibleDatasource.length);
        clearTimeout(this.newDataArrivedTimeout);
        this.newDataArrivedTimeout = setTimeout(() => {

          // console.log(this.graphState.dataSourceFreeToRender)

          // do something
          this.graphEdges;

          // show hide nodes when not in visible datasource
          this.hierachyService.compareDataSourceWithVisible(visibleDatasource, this.dataSource.data, this.treeControl)

          let stackedDataSet: any = {};
          let ignoredcount = 0
          // redraw or delete dependencies depending on visible datasource

          // setup hashmap for faster pathfinding
          let visibleDataSourceObject = visibleDatasource.reduce((a, v) => ({ ...a, [v.path]: v }), {})
          // console.log(visibleDataSourceObject)

          let visibleDependencies: any[] = [];
          for (let dependency of this.dependencyData) {

            let dependencyNodes = this.hierachyService.searchDependencyNodes(dependency, visibleDataSourceObject, visibleDatasource, this.treeControl);
            // console.log(dependencyNodes)
            if (dependencyNodes.dependent) {
              let visibleDependency = this.toolboxService.deepCopy(dependency);
              // set Path to new if stacked
              if (dependencyNodes.stackedDependent == 'stacked') {
                visibleDependency.SE_DEPENDENT_PATH = dependencyNodes.dependent.path;
                visibleDependency.stacked = true
              }
              if (dependencyNodes.stackedRequired == 'stacked') {
                visibleDependency.SE_REQUIRED_PATH = dependencyNodes.required.path;
                // visibleDependency.stacked = true
                visibleDependency.stackedRequired = true
              }
              // if(stackedDataMap.get(visibleDependency.SE_DEPENDENT_PATH + dependencyNodes.stackedDependent + 'from' + visibleDependency.SE_REQUIRED_PATH + dependencyNodes.stackedRequired)) {
              //   // ignore
              // } else {
              //   stackedDataMap.set(visibleDependency.SE_DEPENDENT_PATH + dependencyNodes.stackedDependent + 'from' + visibleDependency.SE_REQUIRED_PATH + dependencyNodes.stackedRequired, '');
              //   visibleDependencies.push(visibleDependency);
              // }
              if (stackedDataSet.hasOwnProperty(visibleDependency.SE_DEPENDENT_PATH + dependencyNodes.stackedDependent + 'from' + visibleDependency.SE_REQUIRED_PATH + dependencyNodes.stackedRequired)) {
                // ignore
                ignoredcount++;
              } else {
                stackedDataSet[visibleDependency.SE_DEPENDENT_PATH + dependencyNodes.stackedDependent + 'from' + visibleDependency.SE_REQUIRED_PATH + dependencyNodes.stackedRequired] = {}
                visibleDependencies.push(visibleDependency);
              }

            }
          }

          // let dependencyNodes = this.hierachyService.searchDependencyNodes(dependency, visibleDatasource, this.treeControl);

          this.buildGraph(visibleDatasource, visibleDependencies)

          this.graphEdges = [...this.graphEdges]
          this.graphNodes = [...this.graphNodes]

          window.dispatchEvent(new Event('resize'));
        }, 1);
      }
    })
  }

  ngOnDestroy(): void {
    console.log('destroy');
    clearTimeout(this.newDataArrivedTimeout);
    this.graphEdges = [];
    this.graphNodes = [];
    this.clusters = [];
    this.dataSourceObserver.unsubscribe();
  }

  buildGraph(visibleDatasource: any[], dependencyData: any[]) {
    // console.log('build graph')
    this.graphEdges = [];
    this.graphNodes = [];
    this.clusters = [];
    this.buildNodes(visibleDatasource, dependencyData, this.graphEdges, this.graphNodes);
    this.buildEdges(dependencyData, this.graphEdges, this.graphNodes);
    this.buildCLusters(this.dataSource.data, dependencyData, this.graphEdges, this.graphNodes, this.clusters, visibleDatasource)
    // console.log('build graph finished. rendering...')
    // console.log(new Date())
  }

  buildCLusters(hierachyTree: simpleFormNode[], dependencyData: any[], graphEdges: any[], graphNodes: any[], clusters: any[], visibleDatasource: simpleFormNode[], parentClusterEndJunctionNode?: any) {
    if (!this.graphState.ignoreDependentChildrenCoherences) {
      // hierarchy clustering
      this.createHierarchyClustering(hierachyTree, dependencyData, graphEdges, graphNodes, clusters, visibleDatasource);
    } else {
      // workflow clustering
      this.createWorkflowClustering(hierachyTree, dependencyData, graphEdges, graphNodes, clusters, visibleDatasource, parentClusterEndJunctionNode)
    }
  }

  createWorkflowClustering(hierachyTree: simpleFormNode[], dependencyData: any[], graphEdges: any[], graphNodes: any[], clusters: any[], visibleDatasource: simpleFormNode[], parentClusterEndJunctionNode?: any) {
    for (let node of hierachyTree) {
      if (node.data.isInVisibleDatasource) {
        // build startJunction and Edges to/from
        if (node.children) {
          let cluster: any = {
            id: 'CLUSTER' + node.data.HIERARCHY_PATH,
            label: node.data.NAME.split('.').pop(),
            childNodeIds: []
          }

          if (node.children.length > 0) {

            // check if atleast 1 children is  in visible datasource to create junctions
            let visibleChildren = 0;

            // cluster start node
            let clusterStartJunction: any = {
              id: this.toolboxService.replaceSpecialCharacter('CJ' + node.data.HIERARCHY_PATH),
              label: node.data.NAME.split('.').pop(),
              shortType: node.data.TYPE.toLowerCase(),
              // focused: false,
              data: {
                type: 'batch-connection',
                edge: {
                  edgeType: 'parentChild'
                }
              }
            }
            clusterStartJunction.data.focused = false;
            // add clusterStartJunctionToCluster
            cluster.childNodeIds.push(clusterStartJunction.id)



            // create edges from junction Node to parent children
            for (let children of node.children) {
              if (children.data.isInVisibleDatasource) {
                visibleChildren++;
                this.buildParentChildEdge(clusterStartJunction.id, children.data.HIERARCHY_PATH, children, 'hierachy', dependencyData, graphEdges, graphNodes)
                cluster.childNodeIds.push(children.data.HIERARCHY_PATH)
                cluster.childNodeIds.push('DJN' + children.data.HIERARCHY_PATH);
              }
            }

            if (visibleChildren > 0) {
              // from parent to startNode, clusterStartJunction
              let parentToClusterStartJunctionEdge: any = {
                id: this.toolboxService.replaceSpecialCharacter('H' + node.data.ID + 'TO' + clusterStartJunction.id),
                source: node.data.HIERARCHY_PATH,
                target: clusterStartJunction.id,
                // focused: false,
                data: {
                  focused: false,
                  type: 'batch-connection',
                  color: 'neutrals-line',
                  edge: {
                    edgeType: 'parentChild'
                  }
                }
              }

              graphNodes.push(clusterStartJunction)
              graphEdges.push(parentToClusterStartJunctionEdge);
            }
          } else {
            for (let children of node.children) {
              if (children.data.isInVisibleDatasource) {
                this.buildParentChildEdge(node.data.HIERARCHY_PATH, children.data.HIERARCHY_PATH, children, 'hierachy', dependencyData, graphEdges, graphNodes)
              }
            }
          }



          // junction End



          // search if node has outgoing dependency edges
          // check if parent has a outgoing dependency in which the workflow will merge
          const found = dependencyData.find((dependency: any) => {
            if (dependency.SE_REQUIRED_PATH && dependency.MODE == 'ALL_FINAL' && (dependency.SE_REQUIRED_PATH == node.data.HIERARCHY_PATH)) {
              return true;
            }
            return false;
          });
          if (node.children.length > 0 && found) {

            // node.data.type = 'return-connection'
            // create workflow return node

            // check if atleast 1 children is  visible
            let visibleChildren = 0;

            let clusterEndJunctionNode: any = {
              id: 'CE' + node.data.ID + 'H' + node.data.HIERARCHY_PATH,
              label: node.data.NAME.split('.').pop(),
              shortType: node.data.TYPE.toLowerCase(),
              // focused: false,
              data: {
                type: 'return-connection'
              }
            }
            cluster.childNodeIds.push(clusterEndJunctionNode.id)

            // if parent has an junctionEndNode connect current end node, to parent one
            // if (parentClusterEndJunctionNode) {
            //   let clusterEndJunctionNodeToParentClusterEndJunctionNode: any = {
            //     id: this.toolboxService.replaceSpecialCharacter('H' + parentClusterEndJunctionNode.id + 'TO' + clusterEndJunctionNode.id),
            //     source: clusterEndJunctionNode.id,
            //     target: parentClusterEndJunctionNode.id,
            //     // focused: false,
            //     data: {
            //       focused: false,
            //       type: 'return-connection',
            //       color: 'neutrals-line'
            //     }
            //   }

            //   graphEdges.push(clusterEndJunctionNodeToParentClusterEndJunctionNode)
            // } else {
            // console.log("noparentJunctionNode")
            // create edge from junctionEndNode to the parent dependent

            // }

            for (let children of node.children) {
              if (children.data.isInVisibleDatasource) {
                visibleChildren++;
                this.buildParentChildEdge(children.data.HIERARCHY_PATH, clusterEndJunctionNode.id, children, 'hierachy', dependencyData, graphEdges, graphNodes, 'return-connection')
                cluster.childNodeIds.push(children.data.HIERARCHY_PATH);
                cluster.childNodeIds.push('DJN' + children.data.HIERARCHY_PATH);
              }
              // // check if children of children
              // if (children.children) {
              //   console.log("clusterReturnChildren")
              //   this.createReturnConnection(children.children, clusterEndJunctionNode, dependencyData)
              // }
            }

            if (visibleChildren > 0) {
              let childrenToClusterEndJunctionNode: any = {
                id: this.toolboxService.replaceSpecialCharacter('H' + node.data.ID + 'TO' + clusterEndJunctionNode.id),
                source: clusterEndJunctionNode.id,
                target: found.SE_DEPENDENT_PATH,
                // focused: false,
                data: {
                  focused: false,
                  type: 'return-connection',
                  color: 'neutrals-line'
                }
              }

              graphNodes.push(clusterEndJunctionNode)
              graphEdges.push(childrenToClusterEndJunctionNode)
            }
            clusters.push(cluster)
            this.buildCLusters(node.children, dependencyData, graphEdges, graphNodes, clusters, visibleDatasource, clusterEndJunctionNode)

          } else if (node.children.length > 0 && parentClusterEndJunctionNode && !found) {


            // check if atleast 1 children is  visible
            let visibleChildren = 0;

            // if cluster has more then 1 child and there is a parentclustering running
            // create workflow return node
            let clusterEndJunctionNode: any = {
              id: 'CE' + node.data.ID + 'H' + node.data.HIERARCHY_PATH,
              label: node.data.NAME.split('.').pop(),
              shortType: node.data.TYPE.toLowerCase(),
              // focused: false,
              data: {
                type: 'return-connection'
              }
            }
            cluster.childNodeIds.push(clusterEndJunctionNode.id)




            for (let children of node.children) {
              if (children.data.isInVisibleDatasource) {
                visibleChildren++;
                this.buildParentChildEdge(children.data.HIERARCHY_PATH, clusterEndJunctionNode.id, children, 'hierachy', dependencyData, graphEdges, graphNodes, 'return-connection')
              }
            }

            if (visibleChildren) {
              let clusterEndJunctionNodeToParentClusterEndJunctionNode: any = {
                id: this.toolboxService.replaceSpecialCharacter('H' + node.data.ID + 'TO' + clusterEndJunctionNode.id),
                source: clusterEndJunctionNode.id,
                target: parentClusterEndJunctionNode.id,
                // focused: false,
                data: {
                  focused: false,
                  type: 'return-connection',
                  color: 'neutrals-line'
                }
              }

              graphNodes.push(clusterEndJunctionNode);
              graphEdges.push(clusterEndJunctionNodeToParentClusterEndJunctionNode);
            }

            clusters.push(cluster)
            this.buildCLusters(node.children, dependencyData, graphEdges, graphNodes, clusters, visibleDatasource, clusterEndJunctionNode)
          } else {
            clusters.push(cluster)
            this.buildCLusters(node.children, dependencyData, graphEdges, graphNodes, clusters, visibleDatasource)
          }
        }
      }
    }
  }

  createHierarchyClustering(hierachyTree: simpleFormNode[], dependencyData: any[], graphEdges: any[], graphNodes: any[], clusters: any[], visibleDatasource: simpleFormNode[]) {

    for (let node of hierachyTree) {
      if (node.data.isInVisibleDatasource) {
        // console.log(")
        if (node.children) {
          let cluster: any = {
            id: 'CLUSTER' + node.data.ID + node.data.HIERARCHY_PATH,
            label: node.data.NAME.split('.').pop(),
            childNodeIds: []
          }
          if (node.children.length > 1) {
            let clusterRelatedNode: any = {
              id: 'C' + node.data.ID + 'H' + node.data.HIERARCHY_PATH,
              label: node.data.NAME.split('.').pop(),
              shortType: node.data.TYPE.toLowerCase(),
              // focused: false,
              data: {
                type: 'batch-connection',
                edge: {
                  edgeType: 'parentChild'
                }
              }
            }
            node.data.focused = false;
            cluster.childNodeIds.push(clusterRelatedNode.id)
            // console.log(node)
            let foundChildren: number = 0;
            for (let children of node.children) {
              if (children.data.isInVisibleDatasource) {
                foundChildren++;
                this.buildParentChildEdge(clusterRelatedNode.id, children.data.HIERARCHY_PATH, children, 'hierachy', dependencyData, graphEdges, graphNodes)
                cluster.childNodeIds.push(children.data.HIERARCHY_PATH);
                cluster.childNodeIds.push('DJN' + children.data.HIERARCHY_PATH);
              }
            }
            // any found children ?
            if (foundChildren > 0) {
              let clusterRelatedLink: any = {
                id: this.toolboxService.replaceSpecialCharacter('H' + node.data.ID + 'TO' + clusterRelatedNode.id),
                source: node.data.HIERARCHY_PATH,
                target: clusterRelatedNode.id,
                // focused: false,
                data: {
                  focused: false,
                  type: 'batch-connection',
                  color: 'neutrals-line',
                  edge: {
                    edgeType: 'parentChild'
                  }
                }
              }
              graphEdges.push(clusterRelatedLink);
              graphNodes.push(clusterRelatedNode);
            }

          } else {
            for (let children of node.children) {
              if (children.data.isInVisibleDatasource) {
                this.buildParentChildEdge(node.data.HIERARCHY_PATH, children.data.HIERARCHY_PATH, children, 'hierachy', dependencyData, graphEdges, graphNodes)
              }
              // this.buildParentChildEdge(children.data.parent.data.HIERARCHY_PATH, children.data.HIERARCHY_PATH, 'hierachy')

              // cluster.childNodeIds.push(node.data.HIERARCHY_PATH)
              // cluster.childNodeIds.push(children.data.HIERARCHY_PATH)
            }
          }
          clusters.push(cluster)
          this.buildCLusters(node.children, dependencyData, graphEdges, graphNodes, clusters, visibleDatasource)
        }
      }
    }
  }

  buildParentChildEdge(parentID: string, childID: string, children: simpleFormNode, label: string, dependencyData: any, graphEdges: any[], graphNodes: any[], type?: string) {
    if (this.graphState.ignoreDependentChildrenCoherences) {
      let found: any = undefined
      if (type == 'return-connection') {
        // no return  connections to nodes with outgoing edge
        found = dependencyData.find((dependency: any) => {
          //child has already an incoming dependecy connection and dependency is not external
          if (dependency.SE_REQUIRED_PATH && (dependency.SE_REQUIRED_PATH == parentID)) {
            // check if incoming child dependencies parent is child of childedged to be drawn
            // find incoming dependency (required) Node
            nodeloop: for (let o of graphNodes) {
              // console.log(o)
              if (o.data.HIERARCHY_PATH == dependency.SE_REQUIRED_PATH) {
                // console.log(o)
                // check if o ist of same parent as childid
                // console.log(o.id)
                // console.log(childID)
                let depndencySourcePath = o.id.split(':').slice(0, -1);
                let childSourcePath = childID.split(':').slice(0, -1);
                // console.log()
                // console.log()
                if (depndencySourcePath.join(':') == childSourcePath.join(':')) {
                  return true;
                } else {
                  return false;
                }
                // break nodeloop;
              }
            }
            return true;
          }
          return false;
        });
      } else {
        // no heirarchy connections to nodes with incoming edge
        found = dependencyData.find((dependency: any) => {
          //child has already an incoming dependecy connection and dependency is not external
          if (dependency.SE_REQUIRED_PATH && (dependency.SE_DEPENDENT_PATH == childID)) {
            // check if incoming child dependencies parent is child of childedged to be drawn
            // find incoming dependency (required) Node
            nodeloop: for (let o of graphNodes) {
              // console.log(o)
              if (o.data.HIERARCHY_PATH == dependency.SE_REQUIRED_PATH) {
                // console.log(o)
                // // check if o ist of same parent as childid
                // console.log(o.id)
                // console.log(childID)
                let depndencySourcePath = o.id.split(':').slice(0, -1);
                let childSourcePath = childID.split(':').slice(0, -1);
                // console.log()
                // console.log()
                if (depndencySourcePath.join(':') == childSourcePath.join(':')) {
                  return true;
                } else {
                  return false;
                }
                // break nodeloop;
              }
            }
          }
          return false;
        });

      }
      if (found) {
        // child edge will not be created
      } else {
        graphEdges.push({
          id: this.toolboxService.replaceSpecialCharacter('H' + parentID + 'TO' + childID + 'T' + type),
          source: parentID,
          target: childID,
          label: label,
          data: {
            focused: false,
            type: (type ? type : 'hierarchy'),
            color: 'neutrals-line',
            edge: {
              edgeType: 'parentChild',
              childObject: children
            }
          }
        })
      }
    } else {
      graphEdges.push({
        id: this.toolboxService.replaceSpecialCharacter('H' + parentID + 'TO' + childID + 'T' + type),
        source: parentID,
        target: childID,
        label: label,
        data: {
          focused: false,
          type: (type ? type : 'hierarchy'),
          color: 'neutrals-line',
          edge: {
            edgeType: 'parentChild',
            childObject: children
          }
        }
      });
    }

  }


  buildNodes(hierarchyData: any[], dependecyData: any[], graphEdges: any[], graphNodes: any[]) {
    // console.log(hierarchyData)
    // let nodes: any[] = [];
    for (let node of hierarchyData) {
      let element = node.data;
      this.treeControl.isExpanded(node) ? element.isExpanded = true : element.isExpanded = false;
      // if node has incoming dependency edges create junction Node
      const edgesFound = dependecyData.filter((dependency: any) => {
        // stacked dependency edges are not goingt to the AND or OR Junction -> ignored here
        if (dependency.SE_DEPENDENT_PATH == element.HIERARCHY_PATH && (dependency.stacked != true)) {
          return true;
        }
        return false;
      });
      // console.log(hierarchyData)
      if (edgesFound.length > 1) {
        // console.log(edgesFound)

        // create junctionNode
        graphNodes.push({
          id: 'DJN' + element.HIERARCHY_PATH,
          label: element.NAME.split('.').pop(),
          data: element,
          shortType: 'depJunction' + element.DEPENDENCY_MODE,
          // dimension: {
          //   height: 24,
          //   width: 30
          // }
        })
        for (let edge of edgesFound) {
          edge.JUNCTION_TO_CONNECTION = 'DJN' + element.HIERARCHY_PATH;
        }
        // push edge from junction to Node
        graphEdges.push({
          id: this.toolboxService.replaceSpecialCharacter('JUNCTIONTOCONNECTION' + 'DJN' + element.HIERARCHY_PATH),
          source: 'DJN' + element.HIERARCHY_PATH,
          target: element.HIERARCHY_PATH,
          label: 'ALL_FINAL',
          focused: false,
          data: {
            type: 'dependency-return',
            color: 'blue-line',
          }
        })
      }

      let width = (element.NAME.split('.').pop().length * 10 + 24);
      element.focused = false,
        graphNodes.push({
          id: element.HIERARCHY_PATH,
          label: element.NAME.split('.').pop(),
          data: element,
          shortType: element.TYPE.toLowerCase(),
          dimension: {
            height: 65,
            width: (width > 160 ? width : 160)
          }
        })
      // console.log(graphNodes)
    }
    // graphNodes = nodes
  }

  buildEdges(edgeData: any[], graphEdges: any[], graphNodes: any[]) {
    // console.log(edgeData)
    // let edges: any[] = [];
    // let junctionNodes: any[] = [];
    // const dependencyJunctionNodeTag: string = 'DJN'
    for (let edge of edgeData) {
      if (edge.isExternal == true) {
        // console.log("edge")
        // external dependencies detected
        // 1. create External Node:
        // console.log(edge)
        edge.isExternalDependency = true;
        // if already external node with this dependency ID
        let foundNode = undefined;
        for (let node of graphNodes) {
          // console.log(edge.SE_REQUIRED_PATH)
          // console.log(node.id)
          if (node.id == edge.SE_REQUIRED_PATH) {
            foundNode = node;
          }
        }
        if (foundNode) {
          let externalNodePath = this.toolboxService.replaceSpecialCharacter('H' + edge.REQUIRED_NAME + 'TO' + edge.SE_DEPENDENT_PATH);
          graphEdges.push({
            id: 'E' + edge.ID + externalNodePath,
            source: foundNode.id,
            target: edge.JUNCTION_TO_CONNECTION ? edge.JUNCTION_TO_CONNECTION : edge.SE_DEPENDENT_PATH,
            label: edge.MODE,
            focused: false,
            data: {
              sourcePath: edge.REQUIRED_NAME,
              type: 'external-dependency',
              color: 'red-line',
              edge: edge
            }
          })
          // console.log("FOUND")
          // console.log(foundNode)
        } else {

          // if node was not found no edge can be craeted

          // let externalNodePath = this.toolboxService.replaceSpecialCharacter('H' + edge.REQUIRED_NAME + 'TO' + edge.SE_DEPENDENT_PATH);
          // let externalNode: any = {
          //   id: edge.ID,
          //   label: edge.REQUIRED_NAME.split('.').pop(),
          //   data: edge,
          //   shortType: edge.RESOLVE_MODE.toLowerCase(),
          //   dimension: {
          //     height: 65,
          //     width: 200
          //   }
          // }
          // graphNodes.push(externalNode)
          // // 2. create edge from external to dependent
          // graphEdges.push({
          //   id: 'E' + edge.ID + externalNodePath,
          //   source: edge.ID,
          //   target: edge.JUNCTION_TO_CONNECTION ? edge.JUNCTION_TO_CONNECTION : edge.SE_DEPENDENT_PATH,
          //   label: edge.MODE,
          //   focused: false,
          //   data: {
          //     sourcePath: edge.REQUIRED_NAME,
          //     type: 'external-dependency',
          //     color: 'red-line',
          //     edge: edge
          //   }
          // })
        }
      } else if (edge.SE_DEPENDENT_PATH && edge.SE_REQUIRED_PATH) {
        graphEdges.push({
          id: 'E' + edge.ID + this.toolboxService.replaceSpecialCharacter(edge.SE_DEPENDENT_PATH) + 'TO' + this.toolboxService.replaceSpecialCharacter(edge.SE_REQUIRED_PATH),
          source: (edge.JUNCTION_FROM_CONNECTION ? edge.JUNCTION_FROM_CONNECTION : edge.SE_REQUIRED_PATH),
          target: (edge.JUNCTION_TO_CONNECTION ? edge.JUNCTION_TO_CONNECTION : edge.SE_DEPENDENT_PATH),
          label: edge.MODE,
          focused: false,
          data: {
            type: 'dependency',
            color: 'blue-line',
            edge: edge
          }
        })
        // graphNodes.concat(junctionNodes)
      }
    }

    // graphEdges = edges;
  }

  ngAfterViewInit(): void {
    // this.fitGraph();
    // this.centerGraph();

  }
  fitGraph() {
    this.zoomToFit$.next(true)
  }
  centerGraph() {
    this.center$.next(true)
  }

  zoomChange(event: any) {
    // console.log('zoom changed')
    // console.log(event)
    this.autoCenter = false;
    this.autoZoom = false;
  }

  toggleExpandCollapse(node: any) {
    const found = this.treeControl.dataNodes.find((dataNode: any) => {
      if (dataNode.data.HIERARCHY_PATH == node.data.HIERARCHY_PATH) {
        return true;
      }
      return false;
    });
    if (found) {
      if (node.data.isExpanded) {
        //collapse
        this.treeControl.collapse(found)

      } else {
        // expand
        this.treeControl.expand(found)
      }
    }
  }

  getXYForCenteredLinkCircle(link: Edge): [number | undefined, number | undefined] {
    if (link.line) {
      var myPath = document.createElementNS("http://www.w3.org/2000/svg", "path");
      myPath.setAttributeNS(null, "d", link.line);
      var length = myPath.getTotalLength();
      let p = myPath.getPointAtLength(length / 2);
      return [p.x - 12, p.y - 12]; // Consider the center coordinates of the circle
    }
    return [link.midPoint?.x, link.midPoint?.y];
  }



  deleteEdge(link: Edge, actionHandlerEnum: actionHandler, type: string, graphEdges: any[], graphNodes: any[], focusedNode: any) {
    // console.log(link)
    this.hierachyService.deleteEdge(link, actionHandlerEnum, graphEdges).then(() => {
      this.refreshData();
    });
  }



  focusEdges(node: any, graphEdges: any[]) {
    for (let edge of graphEdges) {
      if (edge.source == node.id || edge.target == node.id) {
        edge.data.focused = true;
      }

      // if source is a junction, focus their parentpaths too
      if ((edge.target == node.id) && String(edge.source).startsWith('C')) {

        // focus parent edge
        this.focusSpecificEdgeByTarget(edge.source, graphEdges);
        // console.log(edge)
      }
    }
  }

  focusSpecificEdgeByTarget(edgeTarget: string, graphEdges: any[]) {
    for (let edge of graphEdges) {
      if (edge.target == edgeTarget) {
        edge.data.focused = true;
      }
    }
  }

  focusSpecificEdgeBySource(edgeSource: string, graphEdges: any[]) {
    for (let edge of graphEdges) {
      if (edge.source == edgeSource) {
        edge.data.focused = true;
      }
    }
  }

  exitDependencySelection() {
    this.clearSelection();
    this.graphState.actionHandler = actionHandler.NONE;
    this.graphState.createEdgeType = createType.NONE;
    this.graphStateChange.emit(this.graphState);
  }
}
