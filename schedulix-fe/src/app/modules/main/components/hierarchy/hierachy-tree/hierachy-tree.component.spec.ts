import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HierachyTreeComponent } from './hierachy-tree.component';

describe('HierachyTreeComponent', () => {
  let component: HierachyTreeComponent;
  let fixture: ComponentFixture<HierachyTreeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HierachyTreeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HierachyTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
