import { FlatTreeControl } from '@angular/cdk/tree';
import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output, OnDestroy, ViewEncapsulation } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { BehaviorSubject, Subject } from 'rxjs';
import { actionHandler } from '../actionHandlerEnum';
import { createType } from '../createType';
import { FormFlatNode, simpleFormNode } from '../hierachyInterfaces';
import { HierarchyService } from '../hierarchy.service';
import { graphState } from '../IGraphState';
import { hierarchyDependencyOptions } from '../IDependencyOptions';

@Component({
  selector: 'app-hierachy-tree',
  templateUrl: './hierachy-tree.component.html',
  styleUrls: ['./hierachy-tree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HierachyTreeComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor() { }

  ngOnDestroy(): void {
    // console.log('destroy')
  }
  ngAfterViewInit(): void {
    // console.log("tree view init")
  }

  @Input() treeControl!: FlatTreeControl<FormFlatNode>;
  @Input() treeFlattener!: MatTreeFlattener<any, any>;
  @Input() dataSource!: MatTreeFlatDataSource<any, any>;
  @Input() focusedNode: any;
  @Input() public toggleHierarchyView!: Function;
  @Input() isVerticalTreeHierachy!: boolean;
  @Input() minDependencyWidth: number = 500;
  @Input() createType: createType = createType.NONE

  htmlcreateType = createType;


  @Input() public toggleExpand: Function = new Function();
  @Input() public focusNode: Function = new Function();
  @Input() public deleteVTreeItem: Function = new Function();
  @Input() public createFocusedEdge: Function = new Function();


  @Input() public clickNode!: Function;
  @Input() graphState!: graphState;
  @Input() dependencyOpions!: hierarchyDependencyOptions;
  @Output() graphStateChange = new EventEmitter<graphState>();


  @Input() actionHandler!: actionHandler;

  htmlActionHandler = actionHandler;


  arrowContainerMindWidth: number = 0;
  arrowWidth: number = 20;

  hasChild = (_: number, node: FormFlatNode) => node.expandable;

  ngOnInit(): void {

  }

  // viewChanged() {
  //   console.log("VIEW CHANGED")
  // }
  expandAll() {
    // this.treeControl.expandAll();
  }

  focusNodeFromTree(node: any) {
    this.clickNode(node, this.graphState);
    // console.log(node)
  }

}
