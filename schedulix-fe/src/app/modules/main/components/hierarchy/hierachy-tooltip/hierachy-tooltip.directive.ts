import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Directive, ElementRef, EmbeddedViewRef, HostListener, Injector, Input } from '@angular/core';
import { HierarchyTooltipClass } from './hierachy-tooltip-data';
import { HierachyTooltipComponent } from './hierachy-tooltip.component';

@Directive({
  selector: '[appHierachyTooltip]'
})
export class HierachyTooltipDirective {

  @Input() appHierachyTooltip: any = '';

  private componentRef: ComponentRef<any> | null = null;

  constructor(
    private elementRef: ElementRef,
    private appRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector) {
  }

  @HostListener('mouseenter')
  onMouseEnter(): void {
    if (this.componentRef === null) {
      const componentFactory =
        this.componentFactoryResolver.resolveComponentFactory(
          HierachyTooltipComponent);
      this.componentRef = componentFactory.create(this.injector);
      this.appRef.attachView(this.componentRef.hostView);
      const domElem =
        (this.componentRef.hostView as EmbeddedViewRef<any>)
          .rootNodes[0] as HTMLElement;
      document.body.appendChild(domElem);
      this.setTooltipComponentProperties();
    }
  }

  private setTooltipComponentProperties() {
    if (this.componentRef !== null) {
      this.componentRef.instance.tooltip = this.parseObjectToHierachyTooltipData(this.appHierachyTooltip);
      const { left, right, bottom } =
        this.elementRef.nativeElement.getBoundingClientRect();
      this.componentRef.instance.left = (right - left) / 2 + left;
      this.componentRef.instance.top = bottom;
    }
  }

  parseObjectToHierachyTooltipData(bicsuiteObject: any): HierarchyTooltipClass {

    let tooltip: HierarchyTooltipClass = new HierarchyTooltipClass();


    // console.log(bicsuiteObject)
    if (bicsuiteObject.edgeType == 'parentChild') {
      // parent child edge description
      // console.log(bicsuiteObject.edgeType);
      if (bicsuiteObject.childObject) {
        let childBicsuiteObject = bicsuiteObject.childObject.data
        // console.log(childBicsuiteObject)
        tooltip.title = 'Hierachy details';
        tooltip.data = [];

        childBicsuiteObject.SH_ALIAS_NAME ? tooltip.data.push({ key: 'alias', value: childBicsuiteObject.SH_ALIAS_NAME }) : '';
        childBicsuiteObject.IS_DISABLED ? tooltip.data.push({ key: 'disabled', value: childBicsuiteObject.IS_DISABLED }) : '';
        childBicsuiteObject.IS_STATIC ? tooltip.data.push({ key: 'static', value: childBicsuiteObject.IS_STATIC }) : '';
        childBicsuiteObject.ENABLE_MODE ? tooltip.data.push({ key: 'enable mode', value: childBicsuiteObject.ENABLE_MODE }) : '';
        childBicsuiteObject.MERGE_MODE ? tooltip.data.push({ key: 'merge mode', value: childBicsuiteObject.MERGE_MODE }) : '';
        childBicsuiteObject.SH_SUSPEND ? tooltip.data.push({ key: 'suspend mode', value: childBicsuiteObject.SH_SUSPEND }) : '';
        childBicsuiteObject.SH_PRIORITY ? tooltip.data.push({ key: 'nice value', value: childBicsuiteObject.SH_PRIORITY }) : '';
        childBicsuiteObject.EST_NAME ? tooltip.data.push({ key: 'exit state translation', value: childBicsuiteObject.EST_NAME }) : '';
        childBicsuiteObject.IGNORED_DEPENDENCIES ? tooltip.data.push({ key: 'ignored dependencies', value: childBicsuiteObject.IGNORED_DEPENDENCIES }) : '';
        
        // enabled condition
        // enable mode
        // static
        // Mode
        // Alias
        // Translation
        // suspend
        // nice value
        // ignored dependencies
        // interval
      }
    } else {
      // depndency edge description

      // CARE! title and first objects in data are going to be translated
      tooltip.title = 'Dependency details';
      tooltip.data = [];

      // if custom name 
      tooltip.data.push({ key: 'name', value: bicsuiteObject.NAME ? bicsuiteObject.NAME : '*untitled' })
      // tooltip.data.push({key: '*name', value:  bicsuiteObject.REQUIRED_NAME.split('.').pop() + ' to ' + bicsuiteObject.DEPENDENT_NAME.split('.').pop()});
      tooltip.data.push({ key: 'mode', value: bicsuiteObject.MODE })
      // bicsuiteObject.MODE == 'ALL_FINAL' ? tooltip.data.push({key: 'resolve_mode', value:  'AND'}) : '';
      // bicsuiteObject.MODE == 'JOB_FINAL' ? tooltip.data.push({key: 'resolve_mode', value:  'OR'}) : '';
      tooltip.data.push({ key: 'state selection', value: bicsuiteObject.STATE_SELECTION })
      bicsuiteObject.STATES ? tooltip.data.push({ key: 'states', value: bicsuiteObject.STATES }) : '';
      tooltip.data.push({ key: 'unresolved', value: bicsuiteObject.UNRESOLVED_HANDLING })
      bicsuiteObject.CONDITION ? tooltip.data.push({ key: 'condition', value: bicsuiteObject.CONDITION }) : '';
    }
    return tooltip;
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.destroy();
  }

  ngOnDestroy(): void {
    this.destroy();
  }

  destroy(): void {
    if (this.componentRef !== null) {
      this.appRef.detachView(this.componentRef.hostView);
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }
}
