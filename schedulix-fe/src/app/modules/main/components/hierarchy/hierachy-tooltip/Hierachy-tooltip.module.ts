import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HierachyTooltipComponent } from './hierachy-tooltip.component';
import { HierachyTooltipDirective } from './hierachy-tooltip.directive';

@NgModule({
  declarations: [
    HierachyTooltipComponent,
    HierachyTooltipDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [HierachyTooltipDirective]
})
export class HierachyTooltip { }