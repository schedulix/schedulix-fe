import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HierachyTooltipComponent } from './hierachy-tooltip.component';

describe('HierachyTooltipComponent', () => {
  let component: HierachyTooltipComponent;
  let fixture: ComponentFixture<HierachyTooltipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HierachyTooltipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HierachyTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
