import { Component, OnInit } from '@angular/core';
import { HierarchyTooltipClass } from './hierachy-tooltip-data';

@Component({
  selector: 'app-hierachy-tooltip',
  templateUrl: './hierachy-tooltip.component.html',
  styleUrls: ['./hierachy-tooltip.component.scss']
})
export class HierachyTooltipComponent implements OnInit {

  tooltip: HierarchyTooltipClass = new HierarchyTooltipClass();
  left: number = 0;
  top: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

}

