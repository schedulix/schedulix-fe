import { actionHandler } from "./actionHandlerEnum";
import { createType } from "./createType";

export interface hierarchyDependencyOptions {
  unresolvedHandling: string ;
  unresolvedHandlingOptions: string[];
  dependencyMode: string;
  dependencyModeOptions: string[];
  stateSelection: string;
  stateSelectionOptions: string[];
}
