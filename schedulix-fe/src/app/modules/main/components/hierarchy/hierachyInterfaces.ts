export interface simpleFormNode {
    data: any,
    path: string;
    // name: string;
    children?: simpleFormNode[];
  }
  
  /** Flat node with expandable and level information */
export interface FormFlatNode {
    data: any,
    path: string;
    expandable: boolean;
    // name: string;
    level: number;
    levelArray: any[];
  
  }