import { CollectionViewer } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { AfterViewChecked, AfterViewInit, ChangeDetectorRef, Component, EventEmitter, HostListener, Input, OnDestroy, OnInit, Output } from '@angular/core';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Edge } from '@swimlane/ngx-graph';
import { Observable, Subscription } from 'rxjs';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { MainInformationService } from '../../../services/main-information.service';
import { hierarchyDependencyOptions } from '../IDependencyOptions';
import { graphState } from '../IGraphState';
import { actionHandler } from '../actionHandlerEnum';
import { FormFlatNode, simpleFormNode } from '../hierachyInterfaces';
import { HierarchyService } from '../hierarchy.service';

@Component({
  selector: 'app-vertical-hierarchy',
  templateUrl: './vertical-hierarchy.component.html',
  styleUrls: ['./vertical-hierarchy.component.scss']
})
export class VerticalHierarchyComponent implements OnInit, AfterViewInit, OnDestroy, AfterViewChecked {

  @Input() treeControl!: FlatTreeControl<FormFlatNode>; // Editorform, used to generate a form.
  @Input() treeFlattener!: MatTreeFlattener<any, any>; // Serverobject
  @Input() dataSource!: MatTreeFlatDataSource<any, any>; // Information abbout the tab.
  @Input() public toggleHierarchyView!: Function;
  @Input() dependencyData: any[] = [];
  @Input() public focusNode!: Function;
  @Input() public refreshData!: Function;
  @Input() unresolvedHandling!: string;
  @Input() dependencyMode!: string;
  @Input() stateSelection!: string;


  @Input() public clickNode!: Function;
  @Input() graphState!: graphState;
  @Input() dependencyOpions!: hierarchyDependencyOptions;
  @Output() graphStateChange = new EventEmitter<graphState>();

  dataSourceObserver: Subscription = new Subscription();

  lines: any[] = [];

  minDependencyWidth: number = 200;
  visibleDataSource: any[] = [];


  linesToDraw: any[] = []

  constructor(private changeDetectionRef: ChangeDetectorRef, private hierachyService: HierarchyService, private eventEmitterService: EventEmitterService, private toolBoxService: ToolboxService, private mainInformationService: MainInformationService) { }

  ngOnDestroy(): void {
    this.clearCanvas();
    this.dataSourceObserver.unsubscribe()
    // window.removeEventListener('scroll', this.scroll, true);
  }
  ngAfterViewInit(): void {

  }
  // @HostListener('scroll', ['$event']) // for scroll events of the current element
  @HostListener('window:scroll', ['$event']) // for window scroll events
  onScroll(event: any) {
    this.redrawLines(this.visibleDataSource, true, true)
  }

  ngOnInit(): void {
    let collectionView: CollectionViewer = { viewChange: new Observable() }
    this.dataSourceObserver = this.dataSource.connect(collectionView).subscribe((visibleDatasource) => {
      // console.log('alter vtree')
      this.visibleDataSource = visibleDatasource;
      this.redrawLines(visibleDatasource, true, false);
    })

    // resize the canvas to fill browser window dynamically
    window.addEventListener('resize', () => {
      this.redrawLines(this.visibleDataSource, true, true)
    }, false);


  }

  resizeCanvas() {
    const wrapper = document.getElementById('vTreeWrapper') as HTMLElement
    const wrapperRect = wrapper!.getBoundingClientRect()
    // set current wrapper offset to positionate lines
    let canvas = document.getElementById('canvas') as HTMLCanvasElement;
    canvas.width = wrapperRect.width;
    canvas.height = wrapperRect.height;
  }

  ngAfterViewChecked(): void {

  }

  clearCanvas() {
    var canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d')!;
    ctx.clearRect(0, 0, canvas.width, canvas.height);
  }
  drawLines() {

    const wrapper = document.getElementById('vTreeWrapper') as HTMLElement
    const wrapperRect = wrapper!.getBoundingClientRect()
    this.clearCanvas();
    // console.log("draw lines")
    const scrollWrapper = document.getElementById('verticalScrollWrapper') as HTMLElement
    // x offset is calculated with scroll offset and bounding coordinates of the wrapperelement
    let xOffset: number = wrapperRect.left + scrollWrapper.scrollLeft;
    // console.log(scrollWrapper.scrollLeft)
    const canvas = document.getElementById('canvas') as HTMLCanvasElement;
    const ctx = canvas.getContext('2d')!;

    // draw lines
    ctx.beginPath();
    for (let line of this.linesToDraw) {

      // canvas_arrow(ctx, 200, 200, 300, 300);
      this.drawCanvasArrow(ctx, line.fromDoc.left - xOffset, line.fromDoc.top, line.toDoc.left - xOffset, line.toDoc.top, line.endPlugColor, line.lineColor);
    }
  }

  drawCanvasArrow(context: CanvasRenderingContext2D, fromx: number, fromy: number, tox: number, toy: number, endPlugColor: string, lineColor: string) {
    context.beginPath();
    // console.log(endPlugColor)
    let ycorrection = -8;

    var headlen = 10; // length of head in pixels
    var dx = tox - fromx;
    var dy = toy - fromy;
    var angle = Math.atan2(dy, dx);


    context.rect(fromx - 2.5, fromy - 2.5 - 146 - ycorrection, 5, 5)


    context.moveTo(fromx, fromy - 146 - ycorrection);
    context.lineTo(fromx + 10, fromy - 146 - ycorrection);

    context.moveTo(fromx + 10, fromy - 146 - ycorrection);
    context.lineTo(tox + 10, toy - 146 - ycorrection);

    context.moveTo(tox + 10, toy - 146 - ycorrection);
    context.lineTo(tox, toy - 146 - ycorrection);
    context.strokeStyle = lineColor;
    context.lineWidth = 1.5;
    context.stroke();
    context.fillStyle = lineColor;
    context.fill();

    context.beginPath();
    context.moveTo(tox - 3, toy - 146 - ycorrection);
    context.lineTo(tox + 3.5, toy - 146 - ycorrection - 4);
    context.lineTo(tox + 3.5, toy - 146 - ycorrection + 4);
    context.fillStyle = endPlugColor;
    context.fill();
    // console.log('draw from:' + fromx + ', ' + fromy + ' to ' + tox + ', ' + toy);


  }

  addLine(from: string, to: string, endPlugColor: string): any {
    var line = {
      fromDoc: document.getElementById(from)?.getBoundingClientRect(),
      toDoc: document.getElementById(to)?.getBoundingClientRect(),
      endPlugColor: endPlugColor,
      lineColor: (this.mainInformationService.getTheme() == 'LIGHT' ? '#2e2e2e' : '#a1a1a1')
    }
    if (line.fromDoc && line.toDoc) {
      this.linesToDraw.push(line)
    }
    return line;
  }

  createLineFromIdToId(d1: string, d2: string, elementWrapper: HTMLElement, node: any, index: number): Promise<any> {

    let endPlugColor = '#007900';

    //
    if (node.data.DEPENDENCY_MODE == 'OR' && node.data.stackedDependentArray[index] != 'stacked') {
      endPlugColor = '#d2ae4d';
    }
    return this.addLine(d1, d2, endPlugColor);
  }

  async setLines() {
    this.linesToDraw = [];
    if (this.treeControl.dataNodes) {
      this.lines = [];
      // console.log('set lines');
      // get document wrapper for the line to be relocated, beauce they are created on toplevel body element
      let wrapperid = 'vertical-hierachy-wrapper'
      this.toolBoxService.waitForElm(wrapperid).then(() => {
        let elmWrapper = document.getElementById('vertical-hierachy-wrapper');
        let createLinePromiseArray: Promise<any>[] = [];
        // let numberOFLines = 0;
        for (let node of this.treeControl.dataNodes) {
          if (node.data.targetFromDirections) {
            for (let i = 0; i < node.data.targetFromDirections.length; i++) {
              // if (numberOFLines < 2000) {
              createLinePromiseArray.push(this.createLineFromIdToId(node.data.targetFromDirections[i].replace('target_from;', ''), node.data.targetFromDirections[i], elmWrapper!, node, i))
              // }
              // numberOFLines++;
              // console.log(numberOFLines)
            }
          }
        }

        return Promise.all(createLinePromiseArray).then(() => {

          // console.warn('stop')
          this.resizeCanvas();
          this.drawLines();

        })
      });
    }
  }

  deleteVTreeItem(dependencyOrDefinition: any, handler: actionHandler) {

    if (handler == actionHandler.DELETE_DEPENDENCY) {
      let edge: Edge = {
        target: dependencyOrDefinition.SE_DEPENDENT_PATH,
        source: dependencyOrDefinition.SE_REQUIRED_PATH,
        data: dependencyOrDefinition
      }
      this.hierachyService.deleteEdge(edge, handler, [edge]).then(() => {
        this.refreshData();
      });
    }
    if (handler == actionHandler.REMOVE_CHILDREN) {
      // console.log(dependencyOrDefinition)
      let edge: Edge = {
        target: dependencyOrDefinition.data.HIERARCHY_PATH,
        source: dependencyOrDefinition.data.parent.data.HIERARCHY_PATH,
        data: dependencyOrDefinition
      }
      this.hierachyService.deleteEdge(edge, handler, [edge]).then(() => {
        this.refreshData();
      });
    }
  }


  resetTargets(treeControl: FlatTreeControl<FormFlatNode>) {
    // console.log(treeControl.dataNodes)

    for (let i = 0; i < treeControl.dataNodes.length; i++) {
      treeControl.dataNodes[i].data.targetFromDirections = undefined;
      treeControl.dataNodes[i].data.targetFromDirectionsIndex = undefined;
      treeControl.dataNodes[i].data.targetDirections = undefined;
      treeControl.dataNodes[i].data.targetDirectionsIndex = undefined;
      treeControl.dataNodes[i].data.stackedRequiredArray = undefined;
      treeControl.dataNodes[i].data.stackedDependentArray = undefined;
    }
  }


  setGraphEmittorAndReciever(dependencyArray: any[], visibleDatasource: any[]) {

    // reset target and targetFrom Arrays
    this.resetTargets(this.treeControl)

    // setup hashmap for faster pathfinding
    let visibleDataSourceObject = visibleDatasource.reduce((a, v) => ({ ...a, [v.path]: v }), {})
    // console.log(visibleDataSourceObject)

    let arrowIndexCount = 0;


    let completeInformationListObject: any = {};

    // hashvalue holder to check if similar edge was created before
    let ToObject: any = {};
    // let arrowIndexCount = 0;
    for (let [index, dependency] of dependencyArray.entries()) {

      let dependencyNodes = this.hierachyService.searchDependencyNodes(dependency, visibleDataSourceObject, visibleDatasource, this.treeControl);
      // every node tupel with from -> to dependencies
      if (Object.keys(dependencyNodes).length !== 0) {

        let targetFrom = dependencyNodes.dependent;
        let require = dependencyNodes.required;

        let completeInformationString = dependencyNodes.dependent.path + '=' + dependencyNodes.stackedDependent + '&to=' + dependencyNodes.required.path + '=' + dependencyNodes.stackedRequired;
        if (completeInformationListObject.hasOwnProperty(completeInformationString)) {
          // do nothing exact line already exist
          // console.log('dublicate line found')
        } else {
          // set string
          completeInformationListObject[completeInformationString] = completeInformationString;

          // create line
          // SET INDEX
          if (ToObject.hasOwnProperty(dependencyNodes.dependent.path + '=' + dependencyNodes.stackedDependent)) {
            // nothing to do, arrow already exist
            dependency.arrowIndex = ToObject[dependencyNodes.dependent.path + '=' + dependencyNodes.stackedDependent];
          } else {
            dependency.arrowIndex = arrowIndexCount;
            ToObject[dependencyNodes.dependent.path + '=' + dependencyNodes.stackedDependent] = arrowIndexCount;
            arrowIndexCount++;
            this.minDependencyWidth = 25 * arrowIndexCount;
          }

          targetFrom.data.targetFromDirections ? targetFrom.data.targetFromDirections.push('target_from;' + dependency.SE_REQUIRED_PATH + ';' + dependency.SE_DEPENDENT_PATH) : targetFrom.data.targetFromDirections = ['target_from;' + dependency.SE_REQUIRED_PATH + ';' + dependency.SE_DEPENDENT_PATH]
          targetFrom.data.targetFromDirectionsIndex ? targetFrom.data.targetFromDirectionsIndex.push(dependency.arrowIndex) : targetFrom.data.targetFromDirectionsIndex = [dependency.arrowIndex];
          targetFrom.data.stackedDependentArray ? targetFrom.data.stackedDependentArray.push(dependencyNodes.stackedDependent) : targetFrom.data.stackedDependentArray = [dependencyNodes.stackedDependent]

          require.data.targetDirections ? require.data.targetDirections.push(dependency.SE_REQUIRED_PATH + ';' + dependency.SE_DEPENDENT_PATH) : require.data.targetDirections = [dependency.SE_REQUIRED_PATH + ';' + dependency.SE_DEPENDENT_PATH]
          require.data.targetDirectionsIndex ? require.data.targetDirectionsIndex.push(dependency.arrowIndex) : require.data.targetDirectionsIndex = [dependency.arrowIndex];
          require.data.targetDependency ? require.data.targetDependency.push(dependency) : require.data.targetDependency = [dependency];
          require.data.stackedRequiredArray ? require.data.stackedRequiredArray.push(dependencyNodes.stackedRequired) : require.data.stackedRequiredArray = [dependencyNodes.stackedRequired]
        }
      }
    }
  }


  setDependencyIndex(dependency: any, dependencyArray: any[], arrowIndexCount: number): number {
    let count = 0;
    for (let d of dependencyArray) {
      if ((d.SE_DEPENDENT_PATH == dependency.SE_DEPENDENT_PATH)) {
        count++;
        if (d.arrowIndex && !dependency.arrowIndex) {
          dependency.arrowIndex = d.arrowIndex;
        } else if (!d.arrowIndex && !dependency.arrowIndex) {
          dependency.arrowIndex = arrowIndexCount;
          // console.log('newIndex')
          d.arrowIndex = arrowIndexCount;
          arrowIndexCount = arrowIndexCount + 1;
        } else if (!d.arrowIndex && dependency.arrowIndex) {
          d.arrowIndex = dependency.arrowIndex
        }
      }
    }


    if (count == 0) {
      dependency.arrowIndex = arrowIndexCount;
      // console.log('newIndex')
      arrowIndexCount = arrowIndexCount + 1;
    }
    // console.log(arrowIndexCount)
    return arrowIndexCount;
  }


  findTreeNode(datasource: FormFlatNode[], node: FormFlatNode): boolean {
    let found = false;
    for (let sourceNode of datasource) {
      if (sourceNode.data.HIERARCHY_PATH == node.data.HIERARCHY_PATH) {
        found = true;
      }
    }
    return found;
  }



  redrawLines(visibleNodes: simpleFormNode[], noResize?: boolean, noDataChange?: boolean) {

    // this.removeLines();
    if(noDataChange) {
      // do nothing just redraw lines, data has not changed -> resizing or scrolling
    } else {
      this.setGraphEmittorAndReciever(this.dependencyData, visibleNodes);
    }
    // console.warn(visibleNodes)
    // console.log('set lines')
    // trigger setlines in next angular lifecycle to render loadlines and to smoothen UI
    this.setLines().then(() => {
      if (noResize) {
        // do nothing
      } else {
        window.dispatchEvent(new Event('resize'));
      }
    });

  }

}
