import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerticalHierarchyComponent } from './vertical-hierarchy.component';

describe('VerticalHierarchyComponent', () => {
  let component: VerticalHierarchyComponent;
  let fixture: ComponentFixture<VerticalHierarchyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerticalHierarchyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerticalHierarchyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
