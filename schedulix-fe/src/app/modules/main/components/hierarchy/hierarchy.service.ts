import { CdkRecycleRows } from '@angular/cdk/table';
import { FlatTreeControl } from '@angular/cdk/tree';
import { Injectable, OnDestroy } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Edge } from '@swimlane/ngx-graph';
import { promise } from 'protractor';
import { concat, Observable, Subject } from 'rxjs';
import { Tab } from 'src/app/data-structures/tab';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { WorkerService } from 'src/app/services/worker.service';
import { TreeFunctionsService } from '../../services/tree-functions.service';
import { ChooserDialogComponent, ChooserDialogData } from '../form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { actionHandler } from './actionHandlerEnum';
import { createType } from './createType';
import { FormFlatNode, simpleFormNode } from './hierachyInterfaces';
import { MainInformationService } from '../../services/main-information.service';

@Injectable({
  providedIn: 'root'
})
export class HierarchyService implements OnDestroy {


  constructor(
    private workerService: WorkerService,
    private treeFunctionService: TreeFunctionsService,
    private toolBoxService: ToolboxService,
    private eventEmitterService: EventEmitterService,
    private commandApiService: CommandApiService,
    private dialog: MatDialog,
    private mainInformationService: MainInformationService) { }

  ngOnDestroy(): void {
    this.workerService.killAllWorkers();
  }

  killWorkers() {
    this.workerService.killAllWorkers();
  }
  setTopologicResultsInTree(nodes: simpleFormNode[], toplogicSortedGraph: topoNode[], cycleNodes: topoNode[]) {
    // console.log(nodes)
    for (let node of nodes) {
      toplogicSortedGraph.forEach((value, index) => {
        // console.log(value.HIERARCHY_PATH); // 9, 2, 5
        if (value.name == node.path) {
          if (!node.data.topologicIndex && node.data.topologicIndex != 0) {
            node.data.topologicIndex = index
          }
        }
      });
      cycleNodes.forEach((value, index) => {
        // console.log(value.HIERARCHY_PATH); // 9, 2, 5
        if ((value.name as string).includes(node.path)) {
          node.data.isCyclicNode = true;
        }
      });
      if (node.children) {
        this.setTopologicResultsInTree(node.children, toplogicSortedGraph, cycleNodes)
      }
    }
  }
  setTopologicResultsInGraph(nodes: any[], toplogicSortedGraph: topoNode[], cycleNodes: topoNode[]) {
    // console.log(nodes)
    for (let node of nodes) {
      cycleNodes.forEach((value, index) => {
        // console.log(value.HIERARCHY_PATH); // 9, 2, 5
        if ((value.name as string).includes(node.HIERARCHY_PATH)) {
          node.isCyclicNode = true;
        }
      });
    }
  }

  kahnsTopologicSort(edgeList: any[], nodeList: any[], tree: any[]): Promise<any> {

    // let workerCallback = new Subject();
    let topoTopDownCallback = new Subject();
    let topoBottomUpCallback = new Subject();

    this.workerService.killAllWorkers();
    this.workerService.createTopologicWorker('topoLogicSort', [edgeList, nodeList, tree, 'topDown'], topoTopDownCallback);
    this.workerService.createTopologicWorker('topoLogicSort', [edgeList, nodeList, tree, 'bottomUp'], topoBottomUpCallback);

    return Promise.all([
      new Promise((resolve, reject) => {
        topoTopDownCallback.subscribe(((result) => {
          resolve(result);
          // console.log('topo topDown finished')
          topoTopDownCallback.unsubscribe();
        }))
      }),
      new Promise((resolve, reject) => {
        topoBottomUpCallback.subscribe(((result) => {
          resolve(result);
          // console.log('topo bottomUp finished')
          topoBottomUpCallback.unsubscribe();
        }))
      }),
    ]).then((results: any[]) => {

      // console.log(results)
      let topo1 = results[0]
      let topo2 = results[1]
      let tempNodes: any = {};
      let hasCycle = false;
      let sortedGraph: topoNode[] = [];

      if (topo1.hasCycle) {
        hasCycle = topo1.hasCycle;

        // console.log(Object.keys(topo1.tempNodes.length));
        tempNodes = topo1.tempNodes
        // console.log(topo1.tempNodes.keys().length)
      }
      // console.log(topo2)
      if (topo2.hasCycle) {
        hasCycle = topo2.hasCycle;

        tempNodes = topo2.tempNodes
        // console.log(topo2.tempNodes.keys().length)
      }
      sortedGraph = topo1.sortedGraph;

      // for the visualisation of the cycles we have to validate the tempNodes -> walk through the exact cycle to finde ALL nodes who causes the cycle and put them into the tempnodes

      // console.log(tempNodes);


      let cyclicNodes: any[] = []
      if (hasCycle) {
        // let graph3: SmdsGraph = new SmdsGraph();
        // graph3.addNodes(nodeList)
        // let hierachy3Edges: any[] = [];
        // console.log(new Date())
        // console.log('add nodes 3')
        // for (let node of tree) {
        //   this.createParentChildEdgesInverse(node, hierachy3Edges)
        // }
        // console.log(new Date())
        // console.log('add Dependency edge 3')
        // for (let edge3 of edgeList) {
        //   graph3.addDependencyEdgeNoExtendetDepdendency(edge3, nodeList);
        // }
        // console.log('add hierachy edges 3')
        // for (let hedge3 of hierachy3Edges) {
        //   graph3.addEdge(hedge3.SE_REQUIRED_PATH, hedge3.SE_DEPENDENT_PATH)
        // }

        // let cyclePathContainer: any = []
        // for (let tempNode of Object.keys(tempNodes)) {
        //   let g3tempnode = graph3.getNode(tempNode)
        //   for (let tar of Object.keys(g3tempnode!.targets)) {
        //     let newObservedNode = graph3.getNode(tar)
        //     this.markAllNodesInCycle(g3tempnode!, newObservedNode!, [tempNodes[tempNode]], graph3, cyclePathContainer);
        //   }
        // }

        // for (let cyclepath of cyclePathContainer) {
        //   cyclicNodes = cyclicNodes.concat(cyclepath);
        // }
        // cyclicNodes = [...new Set(cyclicNodes)]

        cyclicNodes = Object.values(tempNodes)

      }


      return {
        sortorder: sortedGraph,
        cycleNodes: cyclicNodes,
        hascycle: hasCycle
      };

    })
  }

  markAllNodesInCycle(cyclicNode: topoNode, observedNode: topoNode, observedNodes: topoNode[], cyclicGraph: SmdsGraph, cyclePathContainer: any[]) {
    //ist equal or starts with
    if (observedNode.name.startsWith(cyclicNode.name)) {


      let cyclePath = observedNodes.concat(observedNode);

      if (cyclePath.length < 3) {
        // console.log('cycle detected with lower length')
        // filter length < 3 because those cycles are parent child coherences
      } else {
        // cycle detected!
        cyclePathContainer.push(cyclePath)
      }
      return
      // return observedNodes;
    } else if (Object.keys(observedNode.targets).length > 0) {

      // add observed node to observedNodes since now all children will be checked
      // console.log('visit targets of ' + observedNode.name)
      for (let target of Object.keys(observedNode.targets)) {

        let newObservedNode = cyclicGraph.getNode(target)
        let newObservedNodes = observedNodes.concat([observedNode]);
        this.markAllNodesInCycle(cyclicNode, newObservedNode!, newObservedNodes, cyclicGraph, cyclePathContainer);
      }
    } else {

      return
    }

  }
  createParentChildEdges(root: any, edges: any[]): any[] {
    // console.log(root)

    // for (let node of root) {
    if (root.children) {
      for (let children of root.children) {

        // create Edge
        // check if there is a edge with same dependent path
        // edges.push({
        //   SE_REQUIRED_PATH: root.data.HIERARCHY_PATH,
        //   SE_DEPENDENT_PATH: children.data.HIERARCHY_PATH,
        // })
        edges.push({
          SE_REQUIRED_PATH: children.data.HIERARCHY_PATH,
          SE_DEPENDENT_PATH: root.data.HIERARCHY_PATH,
        })
        // console.log("edge pushed")
        if (children.children?.length > 0) {
          // console.log("new instance")
          this.createParentChildEdges(children, edges)
        }
      }
    }
    // }
    return edges;
  }
  createParentChildEdgesInverse(root: any, edges: any[]): any[] {
    // console.log(root)

    // for (let node of root) {
    if (root.children) {
      for (let children of root.children) {

        // create Edge
        // check if there is a edge with same dependent path
        edges.push({
          SE_REQUIRED_PATH: root.data.HIERARCHY_PATH,
          SE_DEPENDENT_PATH: children.data.HIERARCHY_PATH,
        })
        // edges.push({
        //   SE_REQUIRED_PATH: children.data.HIERARCHY_PATH,
        //   SE_DEPENDENT_PATH: root.data.HIERARCHY_PATH,
        // })
        // console.log("edge pushed")
        if (children.children?.length > 0) {
          // console.log("new instance")
          this.createParentChildEdgesInverse(children, edges)
        }
      }
    }
    // }
    return edges;
  }

  sortTopBottom(tree: any[], toplogicSortedGraph: any[]) {

    let sortedTree: any[] = []
    for (let topolNode of toplogicSortedGraph) {
      for (let rootNode of tree) {
        let foundNode = this.searchTreeWithPath(rootNode, topolNode.HIERARCHY_PATH)
        if (foundNode) {
          // let foundNodeRoot = this.getRootParent(foundNode);
          // if (sortedTree.indexOf(foundNodeRoot) != -1) {
          //   sortedTree.push()
          // }
          // this.sortParentNode(foundNode);
        }
      }
    }
    // console.log(sortedTree);
  }

  searchTreeWithPath(element: simpleFormNode, matchingPath: string): simpleFormNode | null {
    if (element == undefined) {
      return null;
    }
    if (element.path == matchingPath) {
      return element;
    }
    // if element path is no substring, the childs are no substring either
    else if (element.children != null && matchingPath.startsWith(element.path)) {
      var i;
      var result = null;
      for (i = 0; result == null && i < element.children.length; i++) {
        result = this.searchTreeWithPath(element.children[i], matchingPath);
      }
      return result;
    }
    return null;
  }

  evaluateStartNodes(graph: any[], nodeList: any[]): any[] {
    let startNodes: any[] = [];
    for (let node of nodeList) {
      let incomingEdges: boolean = false;
      let outgoingEdges: boolean = false;
      for (let edge of graph) {
        if (node.HIERARCHY_PATH == edge.SE_DEPENDENT_PATH) {
          incomingEdges = true;
        }
        if (node.HIERARCHY_PATH == edge.SE_REQUIRED_PATH) {
          outgoingEdges = true;
        }
      }
      if (outgoingEdges == true && incomingEdges == false) {
        startNodes.push(node);
      } else {
        // console.log(node.NAME)
      }
    }
    return startNodes
  }
  indexTreeControl(treeControl: FlatTreeControl<FormFlatNode>) {
    // console.log(treeControl.dataNodes)
    for (let i = 0; i < treeControl.dataNodes.length; i++) {
      treeControl.dataNodes[i].data.tableindex = i;
    }
  }

  indexVisibleDataSource(visibleDataSource: any[]) {
    // console.log(visibleDataSource);
    for (let i = 0; i < visibleDataSource.length; i++) {
      visibleDataSource[i].data.tableindex = i;
    }
  }

  buildUndefinedHierachyTreeNodes(dependencies: any[], flatDataSource:any[]) {
    let dataSourceMap: any = {};
    for (let node of flatDataSource) {
      dataSourceMap[node.HIERARCHY_PATH] = node.HIERARCHY_PATH;
    }
    // console.log(dataSourceMap);
    for (let dep of dependencies) {
     
      // check if requireds or dependes are in the tree, if not these nodes have no privilege to see for that user. but those nodes can cause cycles
      if (dep.SE_REQUIRED_PATH && dep.SE_DEPENDENT_PATH) {
        // required node is undefined
        if (!dataSourceMap.hasOwnProperty(dep.SE_REQUIRED_PATH)) {
          console.log('required node is restricted')

          let node = {
            TYPE: 'RESTRICTED',
            ID: 'restricted_of_'+ dep.ID,
            HIERARCHY_PATH: dep.SE_REQUIRED_PATH,
            NAME: 'Restricted Node'
          }
          flatDataSource.push(node);
          dataSourceMap[dep.SE_REQUIRED_PATH] = dep.SE_REQUIRED_PATH;
        }

        // dependent node is undefined
        if (!dataSourceMap.hasOwnProperty(dep.SE_DEPENDENT_PATH)) {
          console.log('dependent node is restricted')

          let node = {
            TYPE: 'RESTRICTED',
            ID: 'restricted_of_'+ dep.ID,
            HIERARCHY_PATH: dep.SE_REQUIRED_PATH,
            NAME: 'Restricted Node'
          }
          flatDataSource.push(node);
          dataSourceMap[dep.SE_REQUIRED_PATH] = dep.SE_REQUIRED_PATH;
        }
      }
    }
  }
  buildExternalHierachayTreeListNodes(dependencies: any[], dataSource: simpleFormNode[]) {
    // console.log(dataSource)
    // console.log(dependencies)
    for (let dep of dependencies) {

      // if dependecy is external
      if (!dep.SE_REQUIRED_PATH) {

        dep.SE_REQUIRED_PATH = 'EXTERNAL:' + dep.REQUIRED_NAME;
        // console.log(dep.REQUIRED_NAME)
        dep.isExternal = true;
        // check if external node already exists;
        let found: boolean = false;

        for (let node of dataSource) {
          if (node.data.HIERARCHY_PATH == 'EXTERNAL:' + dep.REQUIRED_NAME) {
            found = true;
          }
        }

        if (!found) {
          let node = {
            TYPE: dep.RESOLVE_MODE,
            ID: dep.ID,
            HIERARCHY_PATH: 'EXTERNAL:' + dep.REQUIRED_NAME,
            NAME: 'EXTERNAL:' + dep.REQUIRED_NAME,
            level: 1,
          }
          let transformed: simpleFormNode = this._nestedTransformer(node, -1, 'HIERARCHY_PATH');
          // console.log(transformed)
          dataSource.unshift(transformed);
        }
      }
    }
  }

  buildHierarchyTreeFromList(list: any[]): simpleFormNode[] {
    // console.log(list)
    let treeNodes: simpleFormNode[] = [];
    // console.log(list)
    for (let o of list) {
      let pathArray = o.HIERARCHY_PATH.split(':');
      let parentArray = o.HIERARCHY_PATH.split(':');
      parentArray.pop();
      let parentPath = parentArray.join(':')

      if (parentPath) {  // if there is a Parent

        let parentNode;
        // console.log(parentPath)
        for (let treeNode of treeNodes) {
          // console.log(parentPath);
          // console.log(treeNode.path);

          let possibleHit;

          possibleHit = this.searchTreeWithPath(treeNode, parentPath) // finde node parent;

          // parent = this.getParentInTree()
          // console.log(treeNode.path);
          // console.log(parentPath);


          // get the first search results
          possibleHit != null ? parentNode = possibleHit : '';
        }
        if (parentNode) {  // init children if there is no

          let level = this.getLevel(pathArray);

          let transformed: simpleFormNode = this._nestedTransformer(o, level, 'HIERARCHY_PATH');
          // transformed.parentNode = parentNode;#
          transformed.data.parent = parentNode;
          this.treeFunctionService.insertNode(parentNode, transformed);

        } else {
          // console.log("parent not found for: ")
          // console.log(parentPath)
          let treeNode: simpleFormNode = this._nestedTransformer(o, 0, 'HIERARCHY_PATH')
          treeNodes.push(treeNode);
        }
      } else {
        let treeNode: simpleFormNode = this._nestedTransformer(o, 0, 'HIERARCHY_PATH')
        treeNodes.push(treeNode);
        // console.log(treeNode)
      }
    }

    // for (let node of treeNodes) {
    //   this.sortChildren(node);
    // }
    return treeNodes;
  }

  getLevel(object: any): number {
    return object.length;
  }

  sortNodes(nodes: simpleFormNode[]): simpleFormNode[] {

    nodes = nodes.sort((a, b) => a.data.topologicIndex - b.data.topologicIndex);
    for (let node of nodes) {
      if (node.children) {
        node.children = this.sortNodes(node.children)
      }
    }
    return nodes;
    // console.log(numbers);
  }

  private _nestedTransformer = (node: any, level: number, pathSelector: string) => {


    let dynamicobj: any = new Object();
    for (const [key, value] of Object.entries(node)) {
      // if (this.displayedColumns.find(element => element.parameter_name == key)) {
      // check if there is a value matching to the key from the server
      dynamicobj[key] = value ? value : '';
      // }
    }
    let treeNode = {
      data: dynamicobj,
      hasChildren: !!node.children && node.children.length > 0,
      children: node.children,
      path: node[pathSelector] ? node[pathSelector] : -1,
      emitterArray: [],
      recieverArray: [],
      // name: node[nameSelector] ? node[nameSelector] : node[pathSelector].split('.').pop(),
      level: level,
    }
    return treeNode;
  }



  // --------------------- alter function ------------------

  deleteEdge(link: Edge, actionHandlerEnum: actionHandler, graphEdges: any[]): Promise<any> {


    // console.log("delete Edge: ")
    // console.log(actionHandler +" - " +type)
    // console.log(link)

    if (actionHandlerEnum == actionHandler.DELETE_DEPENDENCY) {

      console.log(link);

      let targetDefinition = link.target.split(':').pop();
      let sourceDefintiion = link.source.split(':').pop();

      // console.log(targetDefinition)
      if (link.data.type == 'external-dependency') {
        // console.log(link)
        // console.log(link.data)
        if (link.data.sourcePath) {
          sourceDefintiion = link.data.sourcePath;
          targetDefinition = link.data.edge.SE_DEPENDENT_PATH.split(':').pop();
        }
      }
      if (targetDefinition && sourceDefintiion) {
        // ALTER JOB DEFINITION SYSTEM.'EXAMPLES'.'E0025_BATCH_HIERARCHY'.'LOAD'.'LOAD_2' DELETE REQUIRED = (SYSTEM.'EXAMPLES'.'E0025_BATCH_HIERARCHY'.'LOAD'.'LOAD_1')
        let stmt = 'ALTER JOB DEFINITION ' + this.toolBoxService.namePathQuote(targetDefinition) + ' DELETE REQUIRED = ( ' + this.toolBoxService.namePathQuote(sourceDefintiion) + ' )'
        return this.commandApiService.execute(stmt).then((response: any) => {
          // if (response.FEEDBACK) {
          this.eventEmitterService.pushSnackBarMessage(["Dependency has been deleted"], SnackbarType.INFO);
          // this.refreshData(graphEdges, graphNodes, focusedNode, this.ignoreDependentChildrenCoherences).then(() => {
          // });
          // } else if (response.ERROR.ERRORMESSAGE) {
          //   this.eventEmitterService.pushSnackBarMessage([response.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
          //   console.warn(response)
          // }
        }, (rejection: any) => {
          // do nothing
        })
      }
    } else if (actionHandlerEnum == actionHandler.REMOVE_CHILDREN) {

      let targetDefinition = link.target.split(':').pop();
      let sourceDefinition: string | undefined = link.source
      if (targetDefinition && sourceDefinition) {
        // console.log(sourceDefinition);
        // if clusterNode inbetweeen get source of source
        if (sourceDefinition.startsWith('C')) {
          for (let edge of graphEdges) {
            if (edge.target == link.source) {
              // console.log(edge)
              sourceDefinition = edge.source.split(':').pop()
            }
          }
        } else {
          sourceDefinition = link.source.split(':').pop()
        }
        // console.log(sourceDefinition);
        // console.log(targetDefinition);
        if (targetDefinition && sourceDefinition) {
          // ALTER JOB DEFINITION SYSTEM.'EXAMPLES'.'E0095_PIPELINE_STEP_DEPENDENCY'.'INSTANCE' DELETE CHILDREN = (SYSTEM.'EXAMPLES'.'E0095_PIPELINE_STEP_DEPENDENCY'.'STEP_2')
          let stmt = 'ALTER JOB DEFINITION ' + this.toolBoxService.namePathQuote(sourceDefinition) + ' DELETE CHILDREN = ( ' + this.toolBoxService.namePathQuote(targetDefinition) + ' )'
          return this.commandApiService.execute(stmt).then((response: any) => {
            // if (response.FEEDBACK) {
            this.eventEmitterService.pushSnackBarMessage(["Children has been deleted"], SnackbarType.INFO);
            // this.refreshData(graphEdges, graphNodes, focusedNode, this.ignoreDependentChildrenCoherences);
            // } else if (response.ERROR) {
            //   this.eventEmitterService.pushSnackBarMessage([response.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
            //   console.warn(response)
            // }
          }, (rejection: any) => {
            // do nothing
          })
        }
      }
    }
    return Promise.reject();
  }


  createFocusedEdge(node: any, focusedNode: any, createEdgeType: createType, stateSelection: string, unresolvedHandling: string, dependencyMode: string): Promise<any> {

    if (createEdgeType == createType.NONE) {
      return Promise.reject();
    }

    // check if selected Node is correct
    // cases:   node is external AND createtype dependent -> no external dependents allowed
    if (['both', 'internal', 'external'].includes(node.shortType) && (createEdgeType == createType.CREATE_DEPENDENT)) {
      this.eventEmitterService.pushSnackBarMessage(["external dependents are not allowed, please choose an internal dependent"], SnackbarType.INFO);
      return Promise.reject();
    }

    // create edge

    if (createEdgeType == createType.CREATE_DEPENDENT) {

      let sourceDefinition = focusedNode.data.NAME;
      let targetDefinition = node.data.NAME;

      // ALTER JOB DEFINITION SYSTEM.'EXAMPLES'.'E0255_BATCH_SKIP'.'UNLOCK' ADD REQUIRED = (SYSTEM.'EXAMPLES'.'E0075_LOADCONTROL'.'MI_TEST')
      let stmt = 'ALTER JOB DEFINITION ' + this.toolBoxService.namePathQuote(targetDefinition) + ' ADD REQUIRED = ( ' + this.toolBoxService.namePathQuote(sourceDefinition) + this.setCreateWith(stateSelection, unresolvedHandling, dependencyMode) + ' )';

      return this.commandApiService.execute(stmt).then((response: any) => {
        // if (response.FEEDBACK) {
        // this.createEdgeType = createType.NONE;
        this.eventEmitterService.pushSnackBarMessage(["Required dependency has been created"], SnackbarType.INFO);
        // this.refreshData(graphEdges, graphNodes, focusedNode, this.ignoreDependentChildrenCoherences);
        // } else if (response.ERROR) {
        //   this.eventEmitterService.pushSnackBarMessage([response.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
        //   console.warn(response)
        // }
      }, (rejection: any) => {
        // do nothing
      })
    } else if (createEdgeType == createType.CREATE_REQUIRED) {

      // console.log(node)

      let targetDefinition = focusedNode.data.NAME;
      let sourceDefinition = node.data.NAME;

      // check if external(no requiredPATH)
      if (node.data.isExternalDependency) {
        sourceDefinition = node.data.REQUIRED_NAME;
      }
      let stmt = 'ALTER JOB DEFINITION ' + this.toolBoxService.namePathQuote(targetDefinition) + ' ADD REQUIRED = ( ' + this.toolBoxService.namePathQuote(sourceDefinition) + this.setCreateWith(stateSelection, unresolvedHandling, dependencyMode) + ' )';

      return this.commandApiService.execute(stmt).then((response: any) => {
        // if (response.FEEDBACK) {
        // this.createEdgeType = createType.NONE;
        this.eventEmitterService.pushSnackBarMessage(["Dependent has been created"], SnackbarType.INFO);
        // console.log(response)
        // this.refreshData(graphEdges, graphNodes, focusedNode, this.ignoreDependentChildrenCoherences).then(() => {
        // });
        // } else if (response.ERROR) {
        //   this.eventEmitterService.pushSnackBarMessage([response.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
        //   console.warn(response)
        // }
      }, (rejection: any) => {
        // do nothing
      })
    }
    return Promise.reject();
    // console.log(node)
  }

  setCreateWith(stateSelection: string, unresolvedHandling: string, dependencyMode: string): string {
    let stmt = '';
    // todo add state selector when final
    stmt += ' STATES = ' + (stateSelection.replace('_', ' ') == 'FINAL' ? 'NONE' : stateSelection.replace('_', ' ')) + ' UNRESOLVED = ' + unresolvedHandling + ' MODE = ' + dependencyMode;
    // console.log(stmt)
    return stmt
  }


  openBatchesAndJobChooser(actionHandlerType: actionHandler, options: any, graphEdges: any[], graphNodes: any[], focusedNode: any, tab: Tab, reloadFunction: Function, stateSelection: string, unresolvedHandling: string, dependencyMode: string) {


    // reloadfunction has to be executed here since afterclose is an subscription -> no way to return promise to component to trigger refresh

    let dialogData: ChooserDialogData = new ChooserDialogData();

    dialogData.tabInfo = tab;
    dialogData.editorFormfield = {

    };

    dialogData.command = "LIST CONDENSED FOLDER SYSTEM WITH EXPAND = (40)";
    dialogData.TYPE = "FOLDER";
    dialogData.shortType = "folder";
    dialogData.propertyPath = ["DATA", "TABLE"];
    dialogData.chooseType = ["BATCH", "JOB", "MILESTONE"];
    dialogData.displayType = "tree";

    if (dialogData != undefined) {
      // Open Dialog
      // console.log(dialogData)
      const dialogRef = this.dialog.open(ChooserDialogComponent, {
        width: '450px',
        data: dialogData,
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
      });
      return dialogRef.afterClosed().toPromise().then((result: any) => {
        if (result) {
          if (actionHandlerType == actionHandler.CREATE_DEPENDENCY) {
            this.createExternalDependency(result, graphEdges, graphNodes, focusedNode, stateSelection, unresolvedHandling, dependencyMode).then(() => {
              reloadFunction();
            });
          } else if (actionHandlerType == actionHandler.ADD_CHILDREN) {
            this.createChild(result, options, graphEdges, graphNodes, focusedNode).then(() => {
              reloadFunction();
            });
          }
          actionHandlerType = actionHandler.NONE;
        }
      });
    } return Promise.resolve({});
  }

  createChild(node: any, options: any, graphEdges: any[], graphNodes: any[], focusedNode: any): Promise<any> {
    let targetDefinition = focusedNode.data.NAME;
    let sourceDefinition = node.namePath;

    // console.log(options)

    let stmt = 'ALTER JOB DEFINITION ' + this.toolBoxService.namePathQuote(targetDefinition) + ' ADD OR ALTER CHILDREN = ( ' + this.toolBoxService.namePathQuote(sourceDefinition) + ' ' + (options.isStatic ? 'STATIC' : 'DYNAMIC') + ')'
    return this.commandApiService.execute(stmt).then((response: any) => {
      // if (response.FEEDBACK) {
      // this.createEdgeType = createType.NONE;
      this.eventEmitterService.pushSnackBarMessage(["Children was added"], SnackbarType.INFO);
      // this.refreshData(graphEdges, graphNodes, focusedNode, this.ignoreDependentChildrenCoherences);
      return response;
      // } else if (response.ERROR) {
      //   this.eventEmitterService.pushSnackBarMessage([response.ERROR.ERRORMESSAGE], SnackbarType.ERROR);
      //   console.warn(response)
      // }
    }, (rejection: any) => {
      // do nothing
    })
    // ALTER JOB DEFINITION SYSTEM.'EXAMPLES'.'E0255_BATCH_SKIP'.'UNLOCK' ADD CHILDREN = (SYSTEM.'EXAMPLES'.'E0075_LOADCONTROL'.'MI_TEST')
  }

  createExternalDependency(node: any, graphEdges: any[], graphNodes: any[], focusedNode: any, stateSelection: string, unresolvedHandling: string, dependencyMode: string): Promise<any> {
    let targetDefinition = focusedNode.data.NAME;
    let sourceDefinition = node.namePath;

    let stmt = 'ALTER JOB DEFINITION ' + this.toolBoxService.namePathQuote(targetDefinition) + ' ADD REQUIRED = ( ' + this.toolBoxService.namePathQuote(sourceDefinition) + this.setCreateWith(stateSelection, unresolvedHandling, dependencyMode) + ' )'
    return this.commandApiService.execute(stmt).then((response: any) => {
      this.eventEmitterService.pushSnackBarMessage(["External dependency was created"], SnackbarType.INFO);
      return response;
    }, (rejection: any) => {
      // do nothing
    })
    // ALTER JOB DEFINITION SYSTEM.'EXAMPLES'.'E0255_BATCH_SKIP'.'UNLOCK' ADD REQUIRED = (SYSTEM.'EXAMPLES'.'E0075_LOADCONTROL'.'MI_TEST')
  }

  searchDependencyNodes(dependency: any, visibleDatasourceObject: any, visibleDatasource: FormFlatNode[], treeControl: FlatTreeControl<FormFlatNode, FormFlatNode>): any {
    let dependent: FormFlatNode | undefined;
    let required: FormFlatNode | undefined;
    let stackedRequired: string = 'nostacked';
    let stackedDependent: string = 'nostacked';
    // find has to have 2 in the end of the function  to ensure, BOTH dependency Nodes(dependent and required) are in the visible datasource
    let find: number = 0;

    // O(n)


    // check if node is visible, just display them
    if (visibleDatasourceObject.hasOwnProperty(dependency.SE_DEPENDENT_PATH)) {
      dependent = visibleDatasourceObject[dependency.SE_DEPENDENT_PATH];
      find++;
    }

    if (visibleDatasourceObject.hasOwnProperty(dependency.SE_REQUIRED_PATH)) {
      required = visibleDatasourceObject[dependency.SE_REQUIRED_PATH];
      find++;
    }

    // both or one collapsed
    if (find != 2) {
      // check which one are missing(were collapsed)
      // O(n)
      if (!dependent) {
        dependent = this.searchAndSetCollapsedChildNodeDependencyProperties(treeControl.dataNodes, visibleDatasource, dependency, 'dependent');
        stackedDependent = 'stacked';
      }
      // O(n)
      if (!required) {
        required = this.searchAndSetCollapsedChildNodeDependencyProperties(treeControl.dataNodes, visibleDatasource, dependency, 'required');
        stackedRequired = 'stacked';
      }

      // check if Parent(or parent of parent) was found, otherwise ignore this edge
      // check if target and source is not the same node, otherwise ignore edge
      if ((dependent && required) && (dependent != required)) {
        find = 2;
      }
    }
    // both not collapsed
    if (find == 2) {
      return {
        dependent: dependent,
        required: required,
        stackedRequired: stackedRequired,
        stackedDependent: stackedDependent
      }
    }
    return {}
  }



  // return first parent with is visible
  getVisibleParent(datasource: FormFlatNode[], dependencyPath: string, visibleDatasource: FormFlatNode[]): FormFlatNode | undefined {
    let found: FormFlatNode | undefined;
    // is finding all parents but takes the last one he can find
    for (let visibleNode of visibleDatasource) {
      if (String(dependencyPath).startsWith(visibleNode.path + ':')) {
        found = visibleNode;
        // return found
      }
    }
    // hash find
    return found
  }

  searchAndSetCollapsedChildNodeDependencyProperties(datasource: FormFlatNode[], visibleDatasource: FormFlatNode[], dependency: any, mode: string): FormFlatNode | undefined {
    if (mode == 'required') {
      // O(n)
      // for (let node of datasource) {
      //   if (node.data.HIERARCHY_PATH == dependency.SE_REQUIRED_PATH) {
      // console.log("found required")
      //  O(m)
      let visibleParent = this.getVisibleParent(datasource, dependency.SE_REQUIRED_PATH, visibleDatasource)
      if (visibleParent) {
        return visibleParent;
      }
      // }
      // }
    } else if (mode == 'dependent') {
      // O(n)
      // for (let node of datasource) {
      //   if (node.data.HIERARCHY_PATH == dependency.SE_DEPENDENT_PATH) {
      // this.getVisibleParent(datasource, node, visibleDatasource)
      // console.log("found dependent")
      //  O(m)
      // let visibleParent = this.getVisibleParent(datasource, node, visibleDatasource)
      // if (visibleParent) {
      //   return visibleParent;
      // }
      let visibleParent = this.getVisibleParent(datasource, dependency.SE_DEPENDENT_PATH, visibleDatasource)
      if (visibleParent) {
        return visibleParent;
      }
      //   }
      // }
    }
    return
  }


  compareDataSourceWithVisible(visibleDatasource: simpleFormNode[], datasource: simpleFormNode[], treeControl: FlatTreeControl<FormFlatNode>): simpleFormNode[] {
    // console.log(datasource)

    for (let node of datasource) {
      const inVisibleDataSource = visibleDatasource.find((visibleNode: any) => {
        if (visibleNode.data.HIERARCHY_PATH == node.data.HIERARCHY_PATH) {
          return true;
        }
        return false;
      });
      if (inVisibleDataSource) {
        node.data.isInVisibleDatasource = true;
        if (node.children) {
          // console.log('check children')
          this.compareDataSourceWithVisible(visibleDatasource, node.children, treeControl)
        }
        // }
      } else {
        node.data.isInVisibleDatasource = false;
      }
    }

    return datasource;
  }
}



interface topoNode {
  name: string,
  targets: any,
  inDegres: number,
  inDegresParentSum: number,
  incomingEdgesSum: number
}


class SmdsGraph {

  // adjList: topoNode[] = [];
  adjListObject: any = {}

  numberOfEdges: number = 0;
  numberOfTrys: number = 0;

  constructor() {

  }

  getNode(name: string): topoNode | undefined {

    return this.adjListObject[name]
    // return found1 ? found1 : false

    // const found = this.adjList.find((node) => {
    //   if (node.name == name) {
    //     return true;
    //   }
    //   return false;
    // })
    // return found;
  }

  addNodes(nodeList: any[]) {
    for (let serverNode of nodeList) {
      this.adjListObject[serverNode.HIERARCHY_PATH] = {
        name: serverNode.HIERARCHY_PATH,
        targets: [],
        inDegres: 0,
        inDegresParentSum: 0,
        incomingEdgesSum: 0
      }
      // this.adjList.push({
      //   name: serverNode.HIERARCHY_PATH,
      //   targets: [],
      //   inDegres: 0,
      //   inDegresParentSum: 0
      // })
    }
  }

  // input type {name, target}
  addEdge(from: string, to: string) {
    // let node = this.adjList.find((node) => node.name == from);
    let node = this.getNode(from)
    if (node) {
      let found: boolean = false;
      this.numberOfTrys++;
      if (node.targets.hasOwnProperty(to)) {
        // edge already exists
      } else {
        node.targets[to] = to;
        this.numberOfEdges++;

        this.getNode(to)!.incomingEdgesSum++
      }
      // for (let tar of node.targets) {
      //   if (tar == to) {
      //     found = true;
      //     // console.log('edge from: ' + from + '\nto: ' + to + '\n already exists')
      //   }
      // }
      // found != true ? node.targets.push(to) : '';
    }
  }
  addDependencyEdge(edge: any, nodelist: any[]) {
    // add dependency
    this.addEdge(edge.SE_REQUIRED_PATH, edge.SE_DEPENDENT_PATH)
    // create virtual dependencies to all dependent children


    // SOS check ronald

    let startPath = edge.SE_DEPENDENT_PATH + ':'
    // from top to down check if node can be a child node.level >
    let hit: boolean = false;
    for (let node of nodelist) {

      // improve startswith
      if (String(node.HIERARCHY_PATH).startsWith(startPath)) {
        this.addEdge(edge.SE_REQUIRED_PATH, node.HIERARCHY_PATH)
        hit = true;
        // check if this exact edge is already there
      } else {
        if (hit == true) {
          break;
        }
      }
      // break wenn mismatch
    }
  }
  addDependencyEdgeNoExtendetDepdendency(edge: any, nodelist: any[]) {
    // add dependency
    this.addEdge(edge.SE_REQUIRED_PATH, edge.SE_DEPENDENT_PATH)
    // create virtual dependencies to all dependent children
    // for (let node of nodelist) {
    //   if (String(node.HIERARCHY_PATH).startsWith(edge.SE_DEPENDENT_PATH + ':')) {
    //     this.addEdge(edge.SE_REQUIRED_PATH, node.HIERARCHY_PATH)

    //     // check if this exact edge is already there

    //   }
    // }
  }
}
