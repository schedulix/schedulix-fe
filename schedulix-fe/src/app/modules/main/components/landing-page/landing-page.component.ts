import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { MainInformationService } from '../../services/main-information.service';
import { PrivilegesService } from '../../services/privileges.service';
import { CRUDService } from 'src/app/services/crud.service';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LandingPageComponent implements OnInit {

  constructor(private eventEmitterService: EventEmitterService, private mainInformationService: MainInformationService, private privilegeService: PrivilegesService, private crudService: CRUDService) { }

  ngOnInit(): void {
  }


  addTab(tabType: string) {
    // let tab
    switch (tabType) {
      case "Approvals":
        let approvalTab = new Tab(tabType, 'Approvals', tabType.toUpperCase(), 'approval', TabMode.EDIT);
        this.eventEmitterService.createTab(approvalTab, true);
        break;
      case "Bookmarking":
        let bookmarkTab = new Tab(tabType, 'bookmarking', tabType.toUpperCase(), 'bookmark', TabMode.EDIT);
        this.eventEmitterService.createTab(bookmarkTab, true);
        break;
      case "Monitor Master":
        let monMasterTab = new Tab(tabType, 'Monitor Master', tabType.toUpperCase(), 'runmajobs', TabMode.MONITOR_MASTER);
        this.eventEmitterService.createTab(monMasterTab, true);
        break;
      case "Monitor Jobs":
        let monJobTab = new Tab(tabType, 'Monitor Jobs', tabType.toUpperCase(), 'monitorjobs', TabMode.MONITOR_JOBS);
        this.eventEmitterService.createTab(monJobTab, true);
        break;
      case "System Information":
        let sysMonTab = new Tab(tabType, 'System Information', tabType.toUpperCase(), 'sysinfo', TabMode.EDIT);
        this.eventEmitterService.createTab(sysMonTab, true);
        break;
      case "Shell":
        let ShellTab = new Tab(tabType, 'Shell', tabType.toUpperCase(), 'shell', TabMode.SHELL);
        this.eventEmitterService.createTab(ShellTab, true);
        break;
      case "Calendar":
        let CalendarTab = new Tab(tabType, 'Calendar', tabType.toUpperCase(), 'calendar', TabMode.CALENDAR);
        this.eventEmitterService.createTab(CalendarTab, true);
        break;
    }

  }

  isSchedulix() {
    return (this.mainInformationService.getSystem().maxLevel == 'OPEN');
  }

  editonRequirement(editonRequirements: string[]): boolean {
    // ['PROFESSIONAL', 'ENTERPRISE']
    return this.privilegeService.validateEdition(editonRequirements)
  }

  openImpExpDialog () {
    let crud = this.crudService.createOrGetCrud('EXIT STATE DEFINITION'); // just need export of Curd so any crud will do
    let tab = new Tab('','','EXIT STATE DEFINITION','',TabMode.EDIT);
    crud.callMethod('export',{}, tab).then((result) => {
      // console.log(result);
    }, (reject) => {
      // console.log(reject);
    });
  }
}
