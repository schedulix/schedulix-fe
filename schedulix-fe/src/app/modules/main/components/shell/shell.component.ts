import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { filter, map, startWith } from 'rxjs/operators';
import { Tab } from 'src/app/data-structures/tab';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShellComponent implements OnInit {

  constructor(private commandApiService: CommandApiService, private eventEmitterService: EventEmitterService) { }

  @Input() tab!: Tab;
  @Input() isActive: boolean | undefined;

  lastOutput: any;
  formControl = new UntypedFormControl();
  autoclear: boolean = false;
  commands = ['SHOW', 'LIST',
    'ALTER',
    'CLEANUP',
    'CONNECT',
    'COPY',
    'CREATE',
    'DROP',
    'DUMP',
    'FINISH JOB',
    'GET',
    'GRANT',
    'KILL SESSION',
    'LINK RESOURCE',
    'MOVE',
    'REGISTER',
    'RENAME',
    'RESUME',
    'REVOKE',
    'SELECT',
    'SET PARAMETER',
    'SHUTDOWN',
    'STOP SERVER',
    'SUBMIT',
    'SUSPEND'

  ];
  Object = Object;
  subcommands: Object = {
    'SHOW':
      [
        'comment',
        'distribution',
        'environment',
        'event',
        'exit state definition',
        'exit state mapping',
        'exit state profile',
        'exit state translation',
        'folder',
        'footprint',
        'group',
        'interval',
        'job',
        'job definition',
        'named resource',
        'nice profile',
        'object monitor',
        'pool',
        'resource',
        'resource state definition',
        'resource state mapping',
        'resource state profile',
        'schedule',
        'scheduled event',
        'scope',
        'server',
        'session',
        'system',
        'trigger',
        'watch type'
      ],

    'ALTER': [
      'distribution',
      'environment',
      'event',
      'exit state mapping',
      'exit state profile',
      'exit state translation',
      'folder',
      'footprint',
      'group',
      'interval',
      'job',
      'job definition',
      'named resource',
      'nice profile',
      'object monitor',
      'pool',
      'resource state definition',
      'resource state mapping',
      'resource state profile',
      'schedule',
      'scheduled event',
      'scope',
      'server',
      'session',
      'trigger',
      'watch type',
    ],
    'CLEANUP': [
      'folder'
    ],
    'CONNECT': [],
    'COPY': [
      'distribution',
      'folder',
      'named resource',
      'scope'
    ],
    'CREATE': [
      'comment',
      'distribution',
      'environment',
      'event',
      'exit state mapping',
      'exit state profile',
      'exit state translation',
      'folder',
      'footprint',
      'group',
      'interval',
      'job',
      'job definition',
      'named resource',
      'nice profile',
      'object monitor',
      'pool',
      'resource state definition',
      'resource state mapping',
      'resource state profile',
      'schedule',
      'scheduled event',
      'scope',
      'server',
      'session',
      'trigger',
      'watch type',
    ],
    'DROP': [
      'comment',
      'distribution',
      'environment',
      'event',
      'exit state mapping',
      'exit state profile',
      'exit state translation',
      'folder',
      'footprint',
      'group',
      'interval',
      'job',
      'job definition',
      'named resource',
      'nice profile',
      'object monitor',
      'pool',
      'resource state definition',
      'resource state mapping',
      'resource state profile',
      'schedule',
      'scheduled event',
      'scope',
      'server',
      'session',
      'trigger',
      'watch type',
    ],
    'GET': [
      'submittag',
      'parameter'
    ],
    'LIST': [
      'calendar',
      'dependency definition',
      'dependency hierarchy',
      'environment',
      'event',
      'exit state definition',
      'exit state mapping',
      'exit state profile',
      'exit state translation',
      'folder',
      'footprint',
      'grant',
      'group',
      'interval',
      'job',
      'job definition hierarchy',
      'named resource',
      'nice profile',
      'object monitor',
      'pool',
      'resource state definition',
      'resource state mapping',
      'resource state profile',
      'schedule',
      'scheduled event',
      'scope',
      'session',
      'trigger',
      'user',
      'watch type',

    ],
    'MOVE': [
      'folder',
      'job definition',
      'named resource',
      'pool',
      'schedule',
      'scope'
    ],
    'RENAME': [
      'distribution',
      'environment',
      'event',
      'exit state mapping',
      'exit state profile',
      'exit state translation',
      'folder',
      'footprint',
      'group',
      'interval',
      'job',
      'job definition',
      'named resource',
      'nice profile',
      'object monitor',
      'resource state definition',
      'resource state mapping',
      'resource state profile',
      'schedule',
      'scope',
      'trigger',
      'user',
      'watch type',
    ]

  }
  filteredOptions: Observable<string[]> = new Observable();

  results: any[] = [];
  init: boolean = true;
  selectedTabSubscription: Subscription = new Subscription();
  isInitialized: boolean = false;

  ngOnInit() {
    if (this.tab.DATA != '') {
      this.formControl.setValue(this.tab.DATA);
      // console.log(this.tab.DATA)
    } else {
      this.formControl.setValue('');
    }
    this.filteredOptions = this.formControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value)),
    );
    this.formControl.valueChanges.subscribe((value) => {
      // console.log(value)
    })

    this.selectedTabSubscription = this.eventEmitterService.selectedTabEmitter.subscribe((result: string) => {
      if (this.tab.ID == result && !this.isInitialized) {
        // this.initTree(this.command);
        // so that this will be loaded only once
        this.isInitialized = true;
      } else if (this.isActive) {
        // data already loaded
        // only render

      } else {
        // when tab is inactive

      }
    });
  }

  private _filter(value: string): string[] {

    let filterValue = value.toLowerCase();
    filterValue = this.getLastCommand(filterValue);

    for (let c of this.commands) {
      if (filterValue.toLocaleLowerCase().includes(c.toLocaleLowerCase())) {
        let subcommandArray: string[] = [];
        for (let [key, value] of Object.entries(this.subcommands)) {
          if (key == c) {
            subcommandArray = value;
          }
        }
        return subcommandArray.filter(command => this.filterCommandList(command, filterValue));
      }
    }
    return this.commands.filter(command => this.filterCommandList(command, filterValue));
  }

  filterCommandList(command: string, filterValue: string) {
    return command.toLowerCase().includes(filterValue.split(' ').slice(-1).join())
  }

  getLastCommand(filterValue: string): string {
    let subFilter = filterValue;
    if (filterValue.slice(-1) == ';') {
      subFilter = filterValue.split(';').slice(-1).join();
    }
    return subFilter;
  }

  displayFn(value: string) {
    if (this.init) {
      this.init = false;
      return value;
    }
    // console.log("-----------DSIPLAY FN---------")
    // console.log(value)
    let valueArray = this.formControl.value.split(' ');
    valueArray.pop();
    valueArray = valueArray.join(' ');
    // console.log(valueArray)
    return valueArray ? valueArray + ' ' + value : value;
  }
  runCurrentStatement() {
    this.runCommands(false);
    this.tab.DATA = this.formControl.value;
    this.eventEmitterService.saveTabEmitter.next();
    return
  }

  runAllStatements() {
    this.runCommands(true);
    this.tab.DATA = this.formControl.value;
    this.eventEmitterService.saveTabEmitter.next();
  }


  splitCommands(command: any, cursorPos: any) {
    var commands: any[] = [];

    const POSITION = 0;
    const TYPE = 1;
    const STRING = 2;

    const BEGIN_MULTICOMMAND = 0;
    const END_MULTICOMMAND = 1;
    const BEGIN_COMMENT = 2;
    const END_COMMENT = 3;
    const BEGIN_LINECOMMENT = 4;

    var mclist = []

    var pos = 0;
    while (pos < command.length) {
      var s = command.substring(pos);
      var spos = s.search(/begin\s+multicommand/i);
      if (spos != -1) {
        var bmc = s.replace(/\n/g, '\uffff').replace(/.*(begin[\s\uffff]+multicommand).*/i, '$1').replace(/\uffff/g, '\n');
        mclist.push([pos + spos, BEGIN_MULTICOMMAND, bmc]);
        pos = pos + spos + bmc.length;
      }
      else
        pos = command.length;
    }

    var pos = 0;
    while (pos < command.length) {
      var s = command.substring(pos);
      // var spos = s.search(/;\s*end\s+multicommand/i);
      var spos = s.search(/end\s+multicommand/i);
      if (spos != -1) {
        // spos = s.search(/end\s+multicommand/i);
        var emc = s.replace(/\n/g, '\uffff').replace(/.*(end[\s\uffff]+multicommand).*/i, '$1').replace(/\uffff/g, '\n');
        mclist.push([pos + spos, END_MULTICOMMAND, emc]); // 1 -> end multicommand
        pos = pos + spos + emc.length;
      }
      else
        pos = command.length;
    }

    var pos = 0;
    while (pos < command.length) {
      var s = command.substring(pos);
      var spos = s.search(/\/\*/);
      if (spos != -1) {
        mclist.push([pos + spos, BEGIN_COMMENT, '/*']);
        pos = pos + spos + 2;
      }
      else
        pos = command.length;
    }

    var pos = 0;
    while (pos < command.length) {
      var s = command.substring(pos);
      var spos = s.search(/\*\//);
      if (spos != -1) {
        mclist.push([pos + spos, END_COMMENT, '*/']);
        pos = pos + spos + 2;
      }
      else
        pos = command.length;
    }

    var pos = 0;
    while (pos < command.length) {
      var s = command.substring(pos);
      var spos = s.search(/\/\//);
      if (spos != -1) {
        mclist.push([pos + spos, BEGIN_LINECOMMENT, '//']);
        pos = pos + spos + 2;
      }
      else
        pos = command.length;
    }

    mclist = mclist.sort(function (a, b) { return a[0] - b[0]; });

    var c;
    var esc = false;
    var str = false;
    var mc = false;
    var mlc = false;
    var slc = false;
    var cmd = '';
    var mclistidx = 0;
    var skipWhitespace = false;
    for (var i = 0; i < command.length; i++) {
      if (mclistidx < mclist.length) { // there are still entries in mclist
        if (i == mclist[mclistidx][POSITION]) { // we are at the beginning of a mclist entry
          if (str || slc) {
            // We ignore mclist entries if we are in a string or single line comment context
          }
          else if (mclist[mclistidx][TYPE] == BEGIN_COMMENT) {
            if (!mlc) {
              // enter multi line comment context and continue after begin comment
              mlc = true;
              cmd = cmd + mclist[mclistidx][STRING];
              i = i + mclist[mclistidx][STRING].length - 1;
              mclistidx = mclistidx + 1;
              continue;
            }
          }
          else if (mclist[mclistidx][TYPE] == END_COMMENT) {
            if (!mlc) {
              alert("Error: end comment without start comment!");
              return null;
            }
            // We continue after the multi line comment if we are in a multi line comment and
            // have a end comment mclist entry at current position
            mlc = false;
            cmd = cmd + mclist[mclistidx][STRING];
            i = i + mclist[mclistidx][STRING].length - 1;
            mclistidx = mclistidx + 1;
            continue;
          }
          else if (mclist[mclistidx][TYPE] == BEGIN_LINECOMMENT) {
            // enter single line comment context if we are not in a multine comment context and
            // continue after begin line comment
            if (!mlc)
              slc = true;
            cmd = cmd + mclist[mclistidx][STRING];
            i = i + mclist[mclistidx][STRING].length - 1;
            mclistidx = mclistidx + 1;
            continue;
          }
          else if (mclist[mclistidx][TYPE] == BEGIN_MULTICOMMAND) {
            if (mc) {
              alert("Error: Nested multi commands not allowed!");
              return null;
            }
            // enter multicomment context if not in a multi line comment context and
            // continue after begin multicommand
            if (!mlc)
              mc = true;
            cmd = cmd + mclist[mclistidx][STRING];
            i = i + mclist[mclistidx][STRING].length - 1;
            mclistidx = mclistidx + 1;
            continue;
          }
          else if (mclist[mclistidx][TYPE] == END_MULTICOMMAND) {
            if (!mc && !mlc) {
              alert("Error: 'End multicommand' without preceeding 'Begin Multicommand'!");
              return null;
            }
            // leave multicomment context if not in a multi line comment context and
            // continue after end multicommand
            if (!mlc)
              mc = false;
            cmd = cmd + mclist[mclistidx][STRING];
            i = i + mclist[mclistidx][STRING].length - 1;
            mclistidx = mclistidx + 1;
            continue;
          }
          mclistidx = mclistidx + 1;
        }
      }
      c = command.charAt(i);
      switch (c) {
        case "'":
          if (!slc && !mlc && !esc)
            str = !str;
          cmd = cmd + c;
          esc = false;
          break;
        case "\\":
          esc = !esc;
          cmd = cmd + c;
          break;
        case ";":
          if (str || slc || mlc || mc)
            cmd = cmd + c;
          else {
            cmd = cmd.trim();
            if (cmd != "" && cmd != "//") {
              if (cursorPos != null) {
                commands = [cmd];
                skipWhitespace = true;
                if (i >= cursorPos) {
                  i = command.length;
                }
                cmd = "";
              }
              else {
                commands.push(cmd);
                cmd = "";
              }
            }
          }
          esc = false;
          break;
        case "\n":
          slc = false;

          if (cursorPos != null && commands.length == 1) {
            if (i >= cursorPos && skipWhitespace == true) {
              i = command.length;
            }
          }
          cmd = cmd + "\n";
          break;
        default:
          if (c != "\t" && c != " " && c != "\n") {
            skipWhitespace = false;
          }
          cmd = cmd + c;
          esc = false;
      }
    }
    if (str) {
      alert("Error: Unclosed string literal!");
      return null;
    }
    if (mc) {
      alert("Error: 'Begin multicommand' without closing 'End Multicommand'!");
      return null;
    }
    if (mlc) {
      alert("Error: Unclosed multi line comment!");
      return null;
    }
    cmd = cmd.trim();
    if (cmd != "" && cmd != "//") {
      if (cursorPos != null) {
        commands = [];
      }
      commands.push(cmd);
    }
    let currentCommandIdx = 0;
    return commands;
  }

  async runCommands(runScript: boolean) {
    // if (document.form.AUTOCLEAR.value != "false")
    //   clearOutput();
    let textarea: HTMLInputElement = document.getElementById('shellCommandInput') as HTMLInputElement;
    var startPos: any = textarea.selectionStart;
    var endPos: any = textarea.selectionEnd;
    var cursorPos = null;

    var selectedText;

    if (runScript) {
      selectedText = textarea.value;
      cursorPos = null;
    } else {
      if (startPos != undefined && startPos != endPos) {
        selectedText = textarea.value.substring(startPos, endPos);
      }
      else {
        selectedText = textarea.value;
        cursorPos = startPos;
      }
    }


    // if (startPos != undefined && startPos != endPos) {
    //   selectedText = textarea.value.substring(startPos, endPos);
    // }
    // else {
    //   selectedText = textarea.value;
    //   if (!runScript) {
    //     cursorPos = startPos;
    //   }
    // }
    // console.log(cursorPos)
    // console.log(selectedText)
    let commands = this.splitCommands(selectedText, cursorPos);
    // console.log(commands)
    if (commands) {
      let tempResults: any[] = [];
      for (let command of commands) {
        let result = {
          data: {},
          title: ''
        }
        await this.commandApiService.execute(command, false).then((res: any) => {
          result.data = res
          if (res.hasOwnProperty("ERROR") && res.ERROR.hasOwnProperty("ERRORMESSAGE")) {
            res.ERROR.ERRORMESSAGE = res.ERROR.ERRORMESSAGE.replace(/^CONNECT .*COMMAND *= *\((.*)\s*\).*;\s*([^\)]*)$/gms, "$1\n$2");
          }
          result.title = command

          // old behaviour we'd like to have the output of multiple commands in order
          // tempResults.push(result)

          // new behaviour we'd like to have the output of multiple always with last command executed on topo
          tempResults.unshift(result)
        });
      }
      if (this.autoclear) {
        this.results = tempResults;
      } else {
        // but the output is displayed in front of existing output
        this.results = tempResults.concat(this.results);
      }

    }
    textarea.focus();
  }

  typeOf(value: any) {
    return typeof value;
  }

discardResult(index: number) {
    // console.log("discard accordion with index: " + index)
    // console.log(this.results)
    if (index > -1) {
      this.results.splice(index, 1);
    }
  }

  discardAllResults() {
    this.results = [];
  }

  clearInput() {
    this.formControl.setValue('');
  }

  downloadScript() {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:sdms/plain;charset=utf-8,' + encodeURIComponent(this.formControl.value));
    element.setAttribute('download', 'shell_script_' + Date.now() + '.sdms');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  importScript() {
    let fileInput = document.getElementById('shell-import')
    if (fileInput) {
      fileInput.click()
    }
  }

  file: any;
  fileChanged(e: any) {
    this.file = e.target.files[0];
    this.uploadDocument(this.file)
  }

  uploadDocument(file: any) {
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      // console.log(fileReader.result);
      this.formControl.setValue(fileReader.result)
    }
    fileReader.readAsText(this.file);
  }
}
