import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
// import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarView } from 'angular-calendar';
import { addSeconds, parseISO } from 'date-fns';
import { Subscription } from 'rxjs';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

// import { EventColor } from 'calendar-utils';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { ToolboxService } from 'src/app/services/toolbox.service';

import resourceTimelinePlugin from '@fullcalendar/resource-timeline';
import momentTimezonePlugin from '@fullcalendar/moment-timezone';

import { CalendarFilterObject } from 'src/app/data-structures/CalendarState';
import { Bookmark } from 'src/app/data-structures/bookmark';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { MainInformationService } from '../../services/main-information.service';

import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { CalendarState } from 'src/app/data-structures/CalendarState';
import calendarFilter_editorform from '../../../../json/editorforms/monitoring/calendarFilter.json';
import { BookmarkService } from '../../services/bookmark.service';
import { NewItemDialogComponent } from '../dialog-components/new-item-dialog/new-item-dialog.component';
import { EditorformDialogComponent, EditorformDialogData } from '../form-generator/editorform-dialog/editorform-dialog.component';
import { time } from 'console';
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})


export class CalendarComponent implements OnInit, AfterViewInit, OnDestroy {

  timeZone: string = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
  timeZones: any[] = [];

  calendarOptions: any = {
    plugins: [momentTimezonePlugin, resourceTimelinePlugin],

    // now: new Date(new Date().toLocaleString('en', {timeZone:  "America/Santiago"})),

    // now: new Date(new Date().toLocaleString('en', { timeZone: this.timeZone })),
    nowIndicator: true,
    timeZone: this.timeZone,
    contentHeight: "100%",
    // height: "100%",
    headerToolbar: {
      left: 'today prev,next',
      center: 'title',
      right: 'resourceTimeline12h,resourceTimelineDay,resourceTimelineWeek,resourceTimelineMonth,resourceTimelineYear'
    },
    titleFormat:
    {
      // hour: '2-digit',
      // minute: '2-digit',
      day: '2-digit',
      month: 'long',
      year: 'numeric',
      meridiem: true,
      hour12: false
    },
    initialView: 'resourceTimelineDay',
    resourceAreaWidth: '250px',
    eventMaxStack: 20,
    views: {
      resourceTimelineMonth: {
        slotDuration: '12:00',
        slotLabelFormat: [{
          day: 'numeric',
          weekday: 'short',
        },
        {
          hour: '2-digit',
          minute: '2-digit',
          meridiem: true,
          hour12: false
        }]
      },
      resourceTimelineYear: {
        // slotMinWidth: 70,
        slotDuration: '24:00',
        slotLabelFormat: [{
          month: 'long'
        }, {
          weekday: 'short',
          day: 'numeric',
        }]
      },
      resourceTimelineWeek: {
        slotDuration: '3:00',
        slotLabelFormat: [{
          day: 'numeric',
          weekday: 'short',
        },
        {
          hour: '2-digit',
          minute: '2-digit',
          meridiem: true,
          hour12: false
        }]
      },
      resourceTimelineDay: {
        slotDuration: '00:15',
        slotLabelFormat: [{
          hour: '2-digit',
          minute: '2-digit',
          meridiem: true,
          hour12: false
        }]
      },
      resourceTimelineZoomedDay: {
        type: 'resourceTimeline',
        duration: { days: 1 },
        buttonText: 'zoomed day',
        slotDuration: '00:05'
      },
      resourceTimeline12h: {
        type: 'resourceTimeline',
        duration: { hours: 12 },
        buttonText: '12 hour',
        slotDuration: '00:10',
        slotLabelFormat: [{
          hour: '2-digit',
          minute: '2-digit',
          hour12: false,
        }]
      },
      resourceTimeline6h: {
        type: 'resourceTimeline',
        duration: { hours: 6 },
        buttonText: '12 hour',
        slotDuration: '00:10'
      }
    },
    schedulerLicenseKey: 'GPL-My-Project-Is-Open-Source',
    resourceAreaHeaderContent: 'Scheduled Master',
    resources: [],
    eventMinWidth: 10,
    eventContent: function (arg: any, createElement: any) {
      return document.createElement('span');
    },
    viewDidMount: (arg: any) => {
      // console.log(arg)
    },
    viewClassNames: (arg: any) => {
      try {
        this.scrollToCurrentTime(this.calendarComponent.getApi());
      } catch (error) {
        return;
      }
    },
    // resourceLabelContent: (arg: any, createElement: any) => {
    //   // console.log(arg)
    //   let el = document.createElement('span')
    //   el.addEventListener("click", (res: any) => {
    //     // console.log(arg.resource.extendedProps)
    //     let props = arg.resource.extendedProps;
    //     const name = props.SE_NAME.split('.').pop();
    //     const path = props.SE_NAME
    //     // console.log(res)
    //     let bicsuiteObject = {
    //       ORIGINAL_NAME: name,
    //       PATH: path,
    //       ID: props.SE_ID,
    //       OWNER: props.SE_OWNER,
    //       PRIVS: "",
    //       TYPE: props.SE_TYPE
    //     }
    //     this.openSchedules(bicsuiteObject);
    //   });
    //   el.addEventListener("mouseenter", (res: any) => {

    //   });
    //   el.addEventListener("mouseleave", (res: any) => {

    //   });

    //   // let typeEl = document.createElement('span');

    //   let icon = document.getElementById(arg.resource.extendedProps.SE_TYPE.toLowerCase() + 'CalendarIcon');

    //   el.innerHTML = arg.fieldValue;

    //   let domElements: any[] = [el];

    //   icon ? domElements.unshift(icon.cloneNode(true)) : '';// createElement('span', {}, arg.fieldValue)
    //   // return el;
    //   return { domNodes: domElements };
    // },

    eventClick: function (arg: any) {
      // console.log(arg)
    },

    resourceRender({ el, resource }: any) {


    }
    // nowIndicator: true
  };


  // references the #calendar in the template
  @ViewChild('calendar') calendarComponent!: any;

  @Input() tab!: Tab;
  @Input() isActive: boolean | undefined;

  private selectedTabSubscription?: Subscription;
  isInitialized: boolean = false;
  loading: boolean = false;

  data: any;

  // @ViewChild('modalContent', { static: true }) modalContent: TemplateRef<any> | undefined;

  // refresh = new Subject<void>();


  activeDayIsOpen: boolean = true;
  tabTypes = TabMode;
  filterObject: CalendarFilterObject = new CalendarFilterObject();
  bookmarks: Bookmark[] = [];
  selectedBookmark: Bookmark = this.bookmarks[0];



  // timeZone = new FormControl('');

  constructor(public bookmarkService: BookmarkService, private matDialog: MatDialog, private eventEmitterService: EventEmitterService, private toolboxService: ToolboxService, private commandApiService: CommandApiService, private mainInformationService: MainInformationService, private customPropertiesService: CustomPropertiesService) { }

  ngOnDestroy(): void {
    this.selectedTabSubscription?.unsubscribe();
  }
  ngAfterViewInit(): void {
  }
  ngOnInit(): void {

    // init timeZone with default
    this.timeZone = this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone");
    this.timeZones = this.customPropertiesService.getCustomObjectProperty('timeZones');

    this.selectedTabSubscription = this.eventEmitterService.selectedTabEmitter.subscribe((result: string) => {
      // console.log(this.tab.ID)
      // console.log(result)
      // console.log(this.isInitialized)
      if (this.tab.ID == result && !this.isInitialized) {

        this.getBookmarks();
        if (this.tab.BOOKMARK) {
          this.selectedBookmark = this.tab.BOOKMARK;
          this.filterObject = this.selectedBookmark.value.filterObject;
          this.selectBookmark(this.tab.BOOKMARK, false);
        } else {
          this.selectedBookmark = this.bookmarks[0];
          this.filterObject = this.selectedBookmark.value.filterObject;
          this.selectBookmark(this.bookmarks[0], false)

        }

        // console.log(this.bookmarks)
        this.data = this.tab.DATA
        this.loading = true;
        this.loadData().then((res) => {
          this.loading = false;
        });
        this.isInitialized = true;
      } else if (this.isActive) {
        if (this.calendarComponent) {
          this.scrollToCurrentTime(this.calendarComponent.getApi());
        }
        window.dispatchEvent(new Event('resize'));
      } else {
        // not active
      }
    });
  }

  getBookmarks() {
    this.bookmarks = this.bookmarkService.getBookmarksOfType('CALENDAR');
  }


  refresh() {
    this.loading = true;
    this.loadData().then(() => {
      this.loading = false;
    })
  }

  eventAfterAllRender() {
    //render now indicator
    // var view = document.getElementById('calendar').fullCalendar('getView');
    // view.renderNowIndicator(moment());
  }
  loadData(): Promise<any> {
    // set expandData of previos selection
    // return Promise.all([
    //   this.executeListCalendar()
    // ]).then((results: any[]) => {
    //   // if(results.hasOwnProperties)
    //   console.log(results)
    //   this.processResults(results);
    // });

    return this.executeListCalendar().then((result: any) => {
      // if(result.hasOwnProperty('ERROR')) {
      //   console.warn('error')
      // } else {
      // console.log(result)
      return this.processResults(result);
      // }
    }, (rejection: any) => {
      // do nothing
    });
  }

  changeTimeZone(timeZone: string) {
    if (this.timeZone == timeZone) {
    } else {

      this.timeZone = timeZone;
      // console.log(this.timeZone);

      // let now = new Date(new Date().toLocaleString('en', { timeZone: this.timeZone }));
      // console.log(now)
      // this.calendarComponent.getApi().setOption('now', now);
      // let calApi = this.calendarComponent.getApi();
      // // calApi.setOption('nowIndicator', false);
      // calApi.setOption('now', () => { return new Date(new Date().toLocaleString("en", {timeZone: this.timeZone}))});
      // calApi.setOption('timeZone', this.timeZone);
      // calApi.gotoDate(new Date(new Date().toLocaleString("en", {timeZone: this.timeZone})))

      // this.calendarComponent = new FullCalendarComponent(this.calendarComponent);
      // console.log(this.calendarComponent)
      // this.calendarComponent.getApi().setOption('timeZone', this.timeZone);

      this.calendarOptions.timeZone = this.timeZone;
      this.calendarComponent.options = this.calendarOptions;
      // console.log(this.calendarComponent)
      this.refresh();
    }
  }

  openSchedule(arg: any) {
    let props = arg.resource.extendedProps;
    const name = props.SE_NAME.split('.').pop();
    const path = props.SE_NAME
    // console.log(res)
    let bicsuiteObject = {
      ORIGINAL_NAME: name,
      PATH: path,
      ID: props.SE_ID,
      OWNER: props.SE_OWNER,
      PRIVS: "",
      TYPE: props.SE_TYPE
    }
    this.openSchedules(bicsuiteObject);
  }

  openSchedules(bicsuiteObject: any): Promise<Object> {
    let schedulesTab = new Tab(bicsuiteObject.ORIGINAL_NAME, bicsuiteObject.PATH + "." + bicsuiteObject.ORIGINAL_NAME,
      'SCHEDULES'.toUpperCase(),
      'interval', TabMode.EDIT);
    schedulesTab.NUMERIC_ID = bicsuiteObject.ID;
    schedulesTab.SE_OWNER = bicsuiteObject.OWNER;
    schedulesTab.SE_TYPE = bicsuiteObject.TYPE;
    // console.log(bicsuiteObject);
    schedulesTab.OWNER = bicsuiteObject.PATH;
    schedulesTab.PRIVS = bicsuiteObject.PRIVS;
    this.eventEmitterService.createTab(schedulesTab, true);
    return Promise.resolve({});
  }

  executeListCalendar(): Promise<any> {
    let filterString = this.applyFilterObject(this.filterObject);
    // console.log(filterString)
    let hierarchyStmt = 'list calendar ' + (filterString ? "with " + filterString : '');
    return this.commandApiService.execute(hierarchyStmt).then((result: any) => {
      return result;
    });
  }

  applyFilterObject(filter: CalendarFilterObject): string {
    let filterString: string = '';

    let namePatterns: string[] = [];
    // FILTER = (NAME LIKE '%TEST%' OR NAME LIKE '%BATCH%')
    for (let namePattern of filter.NAME_PATTERN.TABLE) {
      namePatterns.push("NAME LIKE '(?i)%" + namePattern.NAME + "%'")
    }

    filterString = '';
    if (namePatterns.length > 0) {
      filterString += namePatterns.join(' OR ')
    }


    if (filter.CONDITION && filter.CONDITION_MODE != 'NONE') {
      if (filterString) {
        filterString += ' ' + filter.CONDITION_MODE + ' ' + filter.CONDITION;
      } else {
        filterString += filter.CONDITION;
      }
    }
    if (filterString != '') {
      filterString = 'FILTER = (' + filterString + ')'
    }
    return filterString;
  }

  removeEvents(calApi: any) {
    let previosEventSources = calApi.getEventSources();
    for (let eventSource of previosEventSources) {
      // remove alle events
      eventSource.remove()
    }
    let previosResources = calApi.getResources();
    for (let resource of previosResources) {
      resource.remove()
    }
  }

  processResults(results: any) {


    this.loading = false;
    // console.log("ffs");
    this.toolboxService.waitForElm("fullCalendarWrapper").then(() => {
      console.log("rendered");

      this.calendarComponent.options = this.calendarOptions;
      let calApi = this.calendarComponent.getApi();
      // console.log(calApi.getEventSources())
      // calApi.setOption('now', function (){ return new Date(new Date().toLocaleString("en", {timeZone: "America/Santiago"}))});
      // calApi.setOption('nowIndicator', true);

      console.log(this.calendarComponent)
      // // console.log(this.calendarComponent.calendar.view)

      this.removeEvents(calApi)

      let events = [];
      let resourceMap: any = {};
      let resouces = [];

      // if (results.length > 0) {
      for (let calendarServerEvent of results.DATA.TABLE) {

        let resourceId = calendarServerEvent.SE_ID;

        // if resource of that event was already crated, continue
        if (resourceMap.hasOwnProperty(calendarServerEvent.SE_ID)) {

        } else {
          // else create resource
          resourceMap[calendarServerEvent.SE_ID] = { id: calendarServerEvent.SE_ID, title: calendarServerEvent.SE_NAME.split('.').pop(), extendedProps: calendarServerEvent };
        }
        events.push(this.eventFilter(calendarServerEvent, resourceId))
      }
      // this.events = [...this.events]
      // }

      for (const [key, value] of Object.entries(resourceMap)) {
        // console.log(key + value);
        calApi.addResource(
          value
        )
      }
      calApi.addEventSource(events)

      // console.log(results.DATA.TABLE.length);
      // console.log(events.length);
      // console.log("event render");
      calApi.render();
      window.dispatchEvent(new Event('resize'));

      // scroll to center if not loaded initially
      if (this.isInitialized) {
        this.scrollToCurrentTime(calApi);
      }
      // console.log(this.calendarComponent)
    });
  }

  scrollToCurrentTime(calApi: any) {
    const now = new Date(new Date().toLocaleString('en', { timeZone: this.timeZone }));
    // console.log();
    // console.log(new Date(new Date().toLocaleString('en', { timeZone: this.timeZone })))
    let shiftedDate = new Date(now.setHours(now.getHours()- 1))

    let timeshiftObject = {};
    // resourceTimeline12h,resourceTimelineDay,resourceTimelineWeek,resourceTimelineMonth,resourceTimelineYear'
    if (calApi.view.type == 'resourceTimeline12h') {
      const oneHourBefore = (now.getHours() - new Date(calApi.view.currentStart).getHours()).toLocaleString().padStart(2, '0') + ':' + now.getMinutes().toLocaleString().padStart(2, '0');
      timeshiftObject = oneHourBefore;
    }
    if (calApi.view.type == 'resourceTimelineDay') {
      timeshiftObject = {
        hour: shiftedDate.getHours(),
        minute: shiftedDate.getMinutes()
      }
    }
    if (calApi.view.type == 'resourceTimelineMonth') {
      shiftedDate = new Date(now.setDate(now.getDate()- 2))
      timeshiftObject = {
        day: shiftedDate.getDate(),
        hour: shiftedDate.getHours(),
        minute: shiftedDate.getMinutes()
      }
    }
    if(calApi.view.type == 'resourceTimelineWeek') {
      timeshiftObject = {
        day: shiftedDate.getDate(),
        hour: shiftedDate.getHours(),
        minute: shiftedDate.getMinutes()
      }
    }
    if(calApi.view.type == 'resourceTimelineYear') {
      shiftedDate = new Date(now.setDate(now.getDate()- 2))
      timeshiftObject = {
        month: shiftedDate.getMonth(),
        day: shiftedDate.getDate(),
        hour: shiftedDate.getHours(),
        minute: shiftedDate.getMinutes()
      }
    }
    calApi.scrollToTime(timeshiftObject)
  }

  eventFilter(serverEvent: any, resourceId: any): any {
    // console.log(serverEvent)
    // console.log(new Date())
    let timeZone = serverEvent.STARTTIME.split(' ')[1]
    // has to be converted to UTC
    let convertedDate =  this.toolboxService.convertTimeStampUTC(serverEvent.STARTTIME, 'YYYY-MM-DDTHH:mm:ss');
    let date = convertedDate + '+00:00';
    let shift = serverEvent.EXPECTED_FINAL_TIME != '0' ? serverEvent.EXPECTED_FINAL_TIME : 1300;
    // console.log(serverEvent.STARTTIME);
    let calendarEvent: any = {
      start: date,
      end: addSeconds(parseISO(date), shift),
      resourceId: resourceId,
      title: serverEvent.ID,
      id: serverEvent.ID,
      backgroundColor: '#4d5f9c',
      borderColor: '#4d5f9c',
      extendedProps: serverEvent,
      // resizable: {
      //   beforeStart: true,
      //   afterEnd: true,
      // },
      // draggable: true,
    }
    return calendarEvent;
  }

  closeOpenMonthViewDay() {
    this.activeDayIsOpen = false;
  }

  openFilter() {
    // this.filterObject = this.selectedBookmark.value.filterObject;
    let dataObj: any = this.toolboxService.deepCopy(this.filterObject);
    dataObj.PRIVS = 'E';
    const dialogRef = this.matDialog.open(EditorformDialogComponent, {
      data: new EditorformDialogData(calendarFilter_editorform, dataObj, this.tab, "Filter options"),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
      width: "750px"
    });
    // * Note, dialogs unsubsrcibe autoamtically!
    return dialogRef.afterClosed().toPromise().then((result: any) => {
      // console.log(result);
      if (result != undefined || result != null && result) {
        // console.log(result)
        if (result.METHOD == 'apply_and_save_filter') {
          this.filterObject = result;
          this.saveBookmark()
          // this.refresh();
          // this.flatExpandList = [];
        } else if (result.METHOD == 'apply_filter') {
          // this.flatExpandList = [];
          this.filterObject = result;
          this.refresh();
          // this.reloadTree();
        }
      } else {

      }
      return 1;
    });
  }

  selectBookmark(bookmark: Bookmark, reload: boolean) {
    this.getBookmarks();
    // if bookmark.value is empty
    if (Object.keys(bookmark.value).length === 0) {
      bookmark.value = new CalendarState();
    }

    this.tab.BOOKMARK = bookmark;
    this.selectedBookmark = bookmark;
    this.filterObject = this.selectedBookmark.value.filterObject;

    // this.reloadTree();
    reload ? this.refresh() : '';
  }

  newBookmark() {
    const dialogRef = this.matDialog.open(NewItemDialogComponent, {
      data: this.bookmarkService.getNewBookmarkDialogData(this.selectedBookmark),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result.input) {
        // console.log(result.input)
        let bookmark = new Bookmark(result.input, 'CALENDAR');
        bookmark.scope = result.choosedItem;
        let calendarState = new CalendarState();
        calendarState.filterObject = this.filterObject;
        bookmark.value = calendarState;
        this.bookmarkService.setUserBookmark(bookmark, false).then(() => {
          this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_created'], SnackbarType.INFO)
          this.selectBookmark(bookmark, true);
        });
      }
    });
  }

  isSameBookmark(a: Bookmark, b: Bookmark) {
    return this.bookmarkService.isSameBookmark(a, b)
  }

  saveBookmark() {
    let calendarState = new CalendarState();
    calendarState.filterObject = this.filterObject;
    this.selectedBookmark.value = calendarState;
    this.bookmarkService.setUserBookmark(this.selectedBookmark, false).then(() => {
      this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_updated'], SnackbarType.INFO)
      this.selectBookmark(this.selectedBookmark, true);
    }, () => { });
    this.tab.BOOKMARK = this.selectedBookmark;
  }


}
