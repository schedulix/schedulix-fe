import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { Crud } from 'src/app/interfaces/crud';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { PrivilegesService } from '../../services/privileges.service';
import { CRUDService } from 'src/app/services/crud.service';
import { TranslateService } from "src/app/services/translate.service";

@Component({
  selector: 'app-left-navigation',
  templateUrl: './left-navigation.component.html',
  styleUrls: ['./left-navigation.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LeftNavigationComponent implements OnInit {

  constructor(private toolboxService: ToolboxService, private localStorageHelper: LocalStorageHelper, private eventEmitterService: EventEmitterService, private route: ActivatedRoute, private privilegeService: PrivilegesService, private crudService: CRUDService) { }

  drawerOpenState: boolean = false;

  firstlevelNavigationExpantionList: string[] = [];

  collapsed: boolean = false;

  ngOnInit(): void {
    this.getExpantionList();
    this.collapsed = this.firstLevelNavigationExpanded().includes('sidenav')
  }

  getExpantionList() {
    let storageArray = this.localStorageHelper.getConnectionValue('firstLevelExpansionList');
    (Array.isArray(storageArray)) ? this.firstlevelNavigationExpantionList = storageArray : this.firstlevelNavigationExpantionList;
  }

  setExpantionList() {
    this.localStorageHelper.setConnectionValue('firstLevelExpansionList', this.firstlevelNavigationExpantionList)
  }

  firstLevelNavigationExpanded(): string[] {
    // returns which first level navigation expanders were expanded
    return this.firstlevelNavigationExpantionList;
  }

  openImpExpDialog () {
    let crud = this.crudService.createOrGetCrud('EXIT STATE DEFINITION'); // just need export of Curd so any crud will do
    let tab = new Tab('','','EXIT STATE DEFINITION','',TabMode.EDIT);
    crud.callMethod('export',{}, tab).then((result) => {
      // console.log(result);
    }, (reject) => {
      // console.log(reject);
    });
  }

  addTab(tabType: string) {
    // let tab
    let translatorService = new TranslateService (new LocalStorageHelper(this.eventEmitterService));
    switch (tabType) {
      case "Approvals":
        let approvalTab = new Tab(tabType, 'Approvals', tabType.toUpperCase(), 'approval', TabMode.EDIT);
        this.eventEmitterService.createTab(approvalTab, true);
        break;
      case "Bookmarking":
        let bookmarkTab = new Tab(tabType, 'bookmarking', tabType.toUpperCase(), 'bookmark', TabMode.EDIT);
        this.eventEmitterService.createTab(bookmarkTab, true);
        break;
      case "Monitor Master":
        let monMasterTab = new Tab(tabType, 'Monitor Master', tabType.toUpperCase(), 'runmajobs', TabMode.MONITOR_MASTER);
        this.eventEmitterService.createTab(monMasterTab, true);
        break;
      case "Monitor Jobs":
        let monJobTab = new Tab(tabType, 'Monitor Jobs', tabType.toUpperCase(), 'monitorjobs', TabMode.MONITOR_JOBS);
        this.eventEmitterService.createTab(monJobTab, true);
        break;
      case "System Information":
        let sysMonTab = new Tab(tabType, 'System Information', tabType.toUpperCase(), 'sysinfo', TabMode.EDIT);
        this.eventEmitterService.createTab(sysMonTab, true);
        break;
      case "Shell":
        let ShellTab = new Tab(tabType, 'Shell', tabType.toUpperCase(), 'shell', TabMode.SHELL);
        this.eventEmitterService.createTab(ShellTab, true);
        break;
      case "Calendar":
        let CalendarTab = new Tab(tabType, 'Calendar', tabType.toUpperCase(), 'calendar', TabMode.CALENDAR);
        this.eventEmitterService.createTab(CalendarTab, true);
        break;
    }
    // tab.NAME = tabType;
    // tab.TYPE = tabType.toUpperCase();

  }

  firstLevelNavigationCollapse(event: string) {
    if (this.firstlevelNavigationExpantionList.includes(event)) {
      this.firstlevelNavigationExpantionList = this.toolboxService.findAndRemoveObjectArray(this.firstlevelNavigationExpantionList, event);
      this.setExpantionList();
    } else {
      // this.firstlevelNavigationExpantionList.push(event)
    }
  }
  firstLevelNavigationExpand(event: string) {
    if (this.firstlevelNavigationExpantionList.includes(event)) {
      return
    } else {
      // is not a funcion problem keep an eye on TODO
      this.firstlevelNavigationExpantionList.push(event)
      this.setExpantionList();
    }

  }

  toggleView() {
    this.collapsed ? this.collapsed = false : this.collapsed = true;

    this.collapsed ? this.firstLevelNavigationExpand('sidenav') : this.firstLevelNavigationCollapse('sidenav');
    // window.dispatchEvent(new Event('resize'));
    this.eventEmitterService.rePosNoFormEditors.next("leftNavigationCall")
    // console.log(this.collapsed)
  }

  editonRequirement(editonRequirements: string[]): boolean {
    // ['PROFESSIONAL', 'ENTERPRISE']
    return this.privilegeService.validateEdition(editonRequirements)
  }
}
