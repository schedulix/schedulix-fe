import { ChangeDetectorRef, Component, OnInit, Inject } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { EventEmitterService } from 'src/app/services/event-emitter.service';

@Component({
  selector: 'app-error-dialog',
  templateUrl: './error-dialog.component.html',
  styleUrls: ['./error-dialog.component.scss']
})
export class ErrorDialogComponent implements OnInit {

  dialogData: any = {}
  choosedItem: string = '';
  input: string = '';
  constructor(@Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<ErrorDialogComponent>,
    public eventEmitterService: EventEmitterService,
    public changeDetectorRef: ChangeDetectorRef) {
    this.dialogData = data;
    this.input = data.input;
  }

  ngOnInit(): void {
    // console.log(this.dialogData)
    this.changeDetectorRef.detectChanges()

    // remove html error Warning, ngONinit only works when the angular template is working, on temlate errors angular crashes and has to be reloaded
    // if ngOnInit works there is no html error -> therfore it can be removed, template errors can not be detected in the errorhandler...
    this.removeHtmlWarning();
  }

  removeHtmlWarning() {
    document.getElementById('htmlWarningContainer')!.remove();
  }
  close() {
    // console.log('as')
    this.dialogRef.close('manual close');

    // console.log('after close')
  }
  copyToClipboard() {

    // special error handling since it would cause a circular error handling...
    try {
      let text = '';
      for (let error of this.dialogData) {

        // var copyText = document.getElementById(error.dialogId)!;
        // text += '\n ' + copyText.textContent;
        text = "Error Message: " + error.data.error + '\n' + 
               "Error Code:    " + error.data.code + '\n' + 
               "Command:\n" + 
               error.data.command;
      }
      navigator.clipboard.writeText(text);
      this.eventEmitterService.pushSnackBarMessage(['error has been copied'], SnackbarType.INFO);

    } catch (e: any) {
      console.error(e)
      this.eventEmitterService.pushSnackBarMessage(['error has not been copied, check browser configuration'], SnackbarType.INFO);
    }
  }

  toggleShowMore(error: any) {
        // if(error.showMore) {
          error.showMore = !error.showMore
        // }
    }
}
