import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChildren } from '@angular/core';
import { MatLegacyDialogRef as MatDialogRef, MAT_LEGACY_DIALOG_DATA as MAT_DIALOG_DATA } from '@angular/material/legacy-dialog';

@Component({
  selector: 'app-new-item-dialog',
  templateUrl: './new-item-dialog.component.html',
  styleUrls: ['./new-item-dialog.component.scss']
})
export class NewItemDialogComponent implements OnInit {
  dialogData: NewItemDialogData = new NewItemDialogData();
  choosedItem: string = '';
  input: string = '';
  constructor(@Inject(MAT_DIALOG_DATA) public data: NewItemDialogData,
   public dialogRef: MatDialogRef<NewItemDialogComponent>,
   public changeDetectorRef: ChangeDetectorRef) {
    this.dialogData = data;
    this.input = data.input;


  }
  ngOnInit(): void {
    this.choosedItem = this.dialogData.itemTypes[0];

    
  }

  create() {
    // close with chooes item information

    let result: object = {
      choosedItem: this.choosedItem,
      input: this.input,
      path: this.dialogData.path
    };

    this.dialogRef.close(result);
  }


  toLowerCase(type: string) {
    return type.toLowerCase();
  }
}

export class NewItemDialogData {
  title: string = '';
  translate: string = '';
  itemTypes = [];
  inputEnabled: boolean = false;
  input: string = '';
  iconsDisabled: boolean = false;
  path: string = '';
}