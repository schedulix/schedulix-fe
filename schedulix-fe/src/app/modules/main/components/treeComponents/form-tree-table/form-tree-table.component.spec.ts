import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormTreeTableComponent } from './form-tree-table.component';

describe('FormTreeTableComponent', () => {
  let component: FormTreeTableComponent;
  let fixture: ComponentFixture<FormTreeTableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormTreeTableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormTreeTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
