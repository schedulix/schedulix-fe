import { SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl } from '@angular/cdk/tree';
import { ChangeDetectorRef, Component, Input, OnInit, EventEmitter, Output, ViewEncapsulation } from '@angular/core';
import { UntypedFormGroup } from '@angular/forms';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { Children } from 'preact/compat';
import { Subscription } from 'rxjs';
import { EditorformOptions } from 'src/app/data-structures/editorform-options';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { TableButtonsService } from '../../../services/table-buttons.service';
import { TreeFunctionsService } from '../../../services/tree-functions.service';
import { environment } from 'src/environments/environment';


interface simpleFormNode {
  data: any,
  path: string;
  // name: string;
  children?: simpleFormNode[];
}

/** Flat node with expandable and level information */
interface FormFlatNode {
  data: any,
  path: string;
  expandable: boolean;
  // name: string;
  level: number;
  levelArray: any[];

}
const TREE_DATA: simpleFormNode[] = []


@Component({
  selector: 'app-form-tree-table',
  templateUrl: './form-tree-table.component.html',
  styleUrls: ['./form-tree-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FormTreeTableComponent implements OnInit {

  private _transformer = (node: simpleFormNode, level: number) => {
    return {
      data: node.data,
      path: node.path,
      expandable: !!node.children && node.children.length > 0,
      // name: node.name,
      level: level,
      levelArray: new Array(level)
    };
  }

  private _nestedTransformer = (node: any, level: number, pathSelector: string) => {


    let dynamicobj: any = new Object();
    for (const [key, value] of Object.entries(node)) {
      // if (this.displayedColumns.find(element => element.parameter_name == key)) {
      // check if there is a value matching to the key from the server
      dynamicobj[key] = value ? value : '';
      // }
    }
    let treeNode = {
      data: dynamicobj,
      hasChildren: !!node.children && node.children.length > 0,
      children: node.children,
      path: node[pathSelector] ? node[pathSelector] : -1,
      // name: node[nameSelector] ? node[nameSelector] : node[pathSelector].split('.').pop(),
      level: level,
    }
    return treeNode;
  }

  treeControl = new FlatTreeControl<FormFlatNode>(
    node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
    this._transformer, node => node.level, node => node.expandable, node => node.children);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
  columnData: any[] = [];

  constructor(private treeFunctionsService: TreeFunctionsService,
    private eventEmitterService: EventEmitterService,
    private cd: ChangeDetectorRef,
    private tableButtonService: TableButtonsService,
    private crudService: CRUDService) {
  }

  compareFunction(o1: any, o2: any): boolean {
    if (this.field && o1[this.field.BULK_COMPARE_VALUE]) {
      return o1[this.field.BULK_COMPARE_VALUE] == o2[this.field.BULK_COMPARE_VALUE]
    } else if (o1.ID) {
      return o1.ID == o2.ID
    } else {
      return o1.NAME == o2.NAME
    }
  }

  @Input() field: any;
  @Input() bicsuiteObject: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() forceDropline = false; // should a dropline always be included?
  @Input() tabInfo!: Tab;
  @Input() editorformOptions?: EditorformOptions;
  @Input() parentBicsuiteObject: any;

  @Input() parentHasLoaded: boolean = true;
  @Output() setTabHasLoaded = new EventEmitter<boolean>();

  treeData: simpleFormNode[] = [];

  // checklistSelection = new SelectionModel<any>(true /* multiple */);
  checklistSelection = new SelectionModel<any>(true /* multiple */, undefined, undefined, this.compareFunction.bind(this));

  dataRefreshSubscription?: Subscription;

  ngOnInit(): void {

    // init tabstates
    if (this.tabInfo.TABSTATE.fieldStates[this.field.NAME]) {
      // if expandlist is already there -> use this
    } else {
      // if expandlist is not present -> create
      this.tabInfo.TABSTATE.fieldStates[this.field.NAME] = []
    }
    if (this.field.ENABLE_BULK == 'true') {
      if (this.field.BULK_COMPARE_VALUE) {
      } else {
        if (!environment.production) {
          alert('BULK_COMPARE_VALUE is missing in field ' + this.field.NAME + ' in ' + this.tabInfo.TYPE + '.json');
        }
      }
    }

    this.dataSource.data = this.buildTreeFromList(this.bicsuiteObject[this.field.NAME].TABLE, this.field.PATH_DEFINITION, this.field.NAME_DEFINITION);

    this.columnData = this.field.TABLE.FIELDS

    this.field.COLLAPSED == 'false' ? this.treeControl.expandAll() : '';
    if (this.field.EXPAND_ROOTS == "true") {
      for (let node of this.treeControl.dataNodes) {
        let n: any = node;
        if (String(n[this.field.PATH_DEFINITION]).indexOf('.') == -1) {
          this.treeControl.expand(node);
        }
      }
    } else if (this.field.EXPAND_ROOTS == "false") {
      // do nothing
    } else {
      this.treeControl.expand(this.treeControl.dataNodes[0]);
    }

    for (let node of this.treeControl.dataNodes) {
      this.expandInExpansionList(node)
    }

    // update data after rechieving new data from the server
    this.dataRefreshSubscription = this.eventEmitterService.bicsuiteRefreshEmitter.subscribe((tab: Tab) => {
      if (this.tabInfo != undefined && tab.ID === this.tabInfo.ID) {
        this.update();
      }
    });
    this.setCheckListSelection();


  }

  //pathmode == if tree hast idpath or namePath
  buildTreeFromList(list: any[], pathSelector: string, nameSelector: string): simpleFormNode[] {

    let treeNodes: simpleFormNode[] = [];
    // console.log(list)
    for (let o of list) {
      if (o.hasOwnProperty(pathSelector)) {
        let pathArray = o[pathSelector].split('.');
        let parentArray = o[pathSelector].split('.');
        parentArray.pop();
        let parentPath = parentArray.join('.')

        if (parentPath) {  // if there is a Parent

          let parentNode;
          // console.log(parentPath)
          for (let treeNode of treeNodes) {
            // console.log(parentPath);
            // console.log(treeNode.path);

            let possibleHit;
            if (this.field.TREE_CELL_TYPE == 'REQUIRED_RESOURCE') {
              possibleHit = o[nameSelector] ? this.searchTreeWithPath(treeNode, o[pathSelector]) : this.searchTreeWithPath(treeNode, parentPath) // finde node parent;
            } else {
              possibleHit = this.searchTreeWithPath(treeNode, parentPath) // finde node parent;
            }

            // get the first search results
            possibleHit != null ? parentNode = possibleHit : '';
          }
          if (parentNode) {  // init children if there is no

            let level = this.getLevel(pathArray);

            let transformed: simpleFormNode = this._nestedTransformer(o, level, pathSelector);
            // transformed.parentNode = parentNode;
            this.treeFunctionsService.insertNode(parentNode, transformed);

          } else {
            // console.log("parent not found for: ")
            // console.log(parentPath)
            let treeNode: simpleFormNode = this._nestedTransformer(o, 0, pathSelector)
            treeNodes.push(treeNode);
          }
        } else {
          let treeNode: simpleFormNode = this._nestedTransformer(o, 0, pathSelector)
          treeNodes.push(treeNode);
          // console.log(treeNode)
        }
      } else {
        console.warn("mandatory propertys missing in TreeItem")
        console.warn(o)
      }
    }

    for (let node of treeNodes) {
      this.sortChildren(node);
    }


    return treeNodes;
  }

  expandInExpansionList(node: any) {

    if (this.tabInfo.TABSTATE.fieldStates[this.field.NAME].includes(node.data[this.field.PATH_DEFINITION])) {
      this.treeControl.expand(node)
      // console.log('expand: ' + node.data[this.field.PATH_DEFINITION])
    }
    // if (node.children) {
    //   for (let child of node.children) {
    //     this.expandInExpansionList(child);
    //   }
    // }
  }

  getLevel(object: any): number {
    return 0;
  }

  sortChildren(node: any) {
    let hasChild = !!node.children && node.children.length > 0;
    if (hasChild) {
      node.children.sort(this.compare);
      for (let child of node.children) {
        let hasChild = !!child.children && child.children.length > 0;
        if (hasChild) {
          this.sortChildren(child);
        }
      }
    }
  }

  compare(a: any, b: any) {

    let achild = !!a.children && a.children.length > 0;
    let bchild = !!b.children && b.children.length > 0
    if (achild == false && bchild == true) {
      // console.log(1)
      return 1;
    }
    if (achild == true && bchild == false) {
      // console.log(-1)
      return -1;
    }
    // console.log(0)
    return 0;
  }


  searchTreeWithPath(element: simpleFormNode, matchingPath: string): simpleFormNode | null {
    if (element == undefined) {
      return null;
    }
    if (element.path == matchingPath) {
      return element;
    } else if (element.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < element.children.length; i++) {
        result = this.searchTreeWithPath(element.children[i], matchingPath);
      }
      return result;
    }
    return null;
  }
  hasChild = (_: number, node: FormFlatNode) => node.expandable;

  openItem(node: FormFlatNode) {
    // CRUD TODO
    switch (this.field.ON_CLICK_METHOD) {
      case "openRequiredResourceItem":
        let tab : any;
        if (node.data.SCOPE_NAME && !node.data.RESOURCE_NAME) {
          let tab = new Tab(node.data.SCOPE_NAME.split('.').pop(), node.data.SCOPE_ID, node.data.SCOPE_TYPE, node.data.SCOPE_TYPE.toLowerCase(), TabMode.EDIT);
          tab.PATH = node.data.SCOPE_NAME.split('.').slice(0, -1).join('.');
          this.eventEmitterService.createTab(tab, true);
        } else {
          let type = node.data.RESOURCE_USAGE.toLowerCase();
          let tab = new Tab(node.data.RESOURCE_NAME.split('.').pop(), node.data.RESOURCE_ID, 'RESOURCE', type, TabMode.EDIT);
          tab.DATA = {};
          tab.DATA.RESOURCE_ID = node.data.RESOURCE_ID;
          tab.DATA.USAGE = node.data.RESOURCE_USAGE;
          tab.DATA.f_IsInstance = false;
          if (['JOB','BATCH'].includes(node.data.SCOPE_TYPE)) {
              tab.DATA.f_isScopeOrFolderInstance = false;
              tab.DATA.f_IsInstance = true;
            }
          else {
              tab.DATA.f_isScopeOrFolderInstance = true;
          }
          this.eventEmitterService.createTab(tab, true);
        }
        break;
      // depricated refers to named_resource but not resource intance of a defined resource
      case "openDefinedResourceItem":
        // console.log(node)
        let path = node.data.NAME.split('.');
        path.pop();
        path = path.join('.')
        let definedTab = new Tab(node.data.NAME.split('.').pop(), node.data.ID, node.data.USAGE, node.data.USAGE.toLowerCase(), TabMode.EDIT);
        definedTab.PATH = path; // TODO CHANGE TO IDs
        this.eventEmitterService.createTab(definedTab, true);
        break;
      case "openJobDefinitionItem":
        // console.log(node)
        let path1 = node.data.SE_PATH.split('.');
        path1.pop();
        path1 = path1.join('.')
        let definedTab1 = new Tab(node.data.SE_PATH.split('.').pop(), node.data.ID, node.data.TYPE, node.data.TYPE.toLowerCase(), TabMode.EDIT);
        definedTab1.PATH = path1; // TODO CHANGE TO IDs
        this.eventEmitterService.createTab(definedTab1, true);
        break;

      default:
        // default crud
        let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE);
        crud.callMethod(this.field.ON_CLICK_METHOD, this.bicsuiteObject, this.tabInfo, this.parentBicsuiteObject, node);
        // this.crudService.execute(this.field.ON_CLICK_METHOD);
        break;

    }
  }

  itemSelectionToggle(obj: any): void {

    // console.log(obj);


    this.checklistSelection.toggle(obj);
    // console.log(this.checklistSelection.selected);
    this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList'] = this.checklistSelection.selected;
    // console.log( this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList']);
    this.eventEmitterService.saveTabEmitter.next();

  }
  loadLock(btnInfo: any) {
    if (btnInfo.LOCK_LOADING) {
      this.setTabHasLoaded.next(false);
    }
  }
  loadUnLock(btnInfo: any) {
    if (btnInfo.LOCK_LOADING) {
      this.setTabHasLoaded.next(true);
    }
  }

  clickCallback($event: any) {
    this.loadLock($event);
    this.tableButtonService.tableExecute($event, this.bicsuiteObject, this.field, this.tabInfo, this.form, this.checklistSelection).then((changedRows: number) => {
      // console.log(changedRows)
      if (changedRows > 0) {
        this.form.markAsDirty();
        this.update();
      }
    }).then(
      (res: any) => {
        this.loadUnLock($event);
        return res;
      },
      (rejection: any) => {
        this.loadUnLock($event);
        return rejection;
      });
  }

  toggleRename() {
    // console.log("rename toggle")
    if (this.field.TABLE.FIELDS[0]) {
      this.field.TABLE.FIELDS[0].TYPE == 'TEXT' ? this.field.TABLE.FIELDS[0].TYPE = 'INPUT' : this.field.TABLE.FIELDS[0].TYPE = 'TEXT';
    }
  }

  update(): void {
    this.dataSource.data = this.buildTreeFromList(this.bicsuiteObject[this.field.NAME].TABLE, this.field.PATH_DEFINITION, this.field.NAME_DEFINITION);
    for (let node of this.treeControl.dataNodes) {
      this.expandInExpansionList(node)
    }
    // this.setIndex(this.tableData);
    this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tabInfo, rootForm: this.form.root });
    // Some properties are not visible by the form. Thus the bicsuite object has be checked in the validationhelper if it is still valid
    this.form.updateValueAndValidity({ onlySelf: true, emitEvent: true });
    this.setCheckListSelection();
    this.cd.detectChanges();
  }

  setCheckListSelection() {
    if (this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList']) {
      // if selectionlist is already there -> use this
      let newSelectedObjects = this.treeControl.dataNodes.filter((data) => {
        // console.log(data.data);
        let found: boolean = false;
        for (let o of this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList']) {
          this.compareFunction(o, data.data) ? found = true : '';
        }
        return found;
      });

      let mappedObjects = newSelectedObjects.map((data) => {
        return data.data;
      })
      this.checklistSelection.clear();
      this.checklistSelection.setSelection(...mappedObjects);
      this.cd.detectChanges();
    } else {
      // if selectionlist is not present -> create
      this.tabInfo.TABSTATE.fieldStates[this.field.NAME + 'selectionList'] = []
    }
  }

  toggle(node: any) {
    // console.log(this.treeControl.isExpanded(node));
    // true == expanded after toggling
    if (this.treeControl.isExpanded(node)) {
      if (node.data[this.field.PATH_DEFINITION]) {
        // find if already in list dont add another one
        let index = this.tabInfo.TABSTATE.fieldStates[this.field.NAME].indexOf(node.data[this.field.PATH_DEFINITION]);
        if (index > -1) {
          // only push when not found
        } else {
          // console.log(index)
          this.tabInfo.TABSTATE.fieldStates[this.field.NAME].push(node.data[this.field.PATH_DEFINITION])
        }
        this.eventEmitterService.saveTabEmitter.next();
      } else {
        console.warn("not path definition fits to the node")
      }
    } else {
      if (node.data[this.field.PATH_DEFINITION]) {
        let index = this.tabInfo.TABSTATE.fieldStates[this.field.NAME].indexOf(node.data[this.field.PATH_DEFINITION]);
        if (index > -1) { // only splice array when item is found
          this.tabInfo.TABSTATE.fieldStates[this.field.NAME].splice(index, 1); // 2nd parameter means remove one item only
        }
        // this.tabInfo.TABSTATE.fieldStates[this.field.NAME].push(node.data[this.field.PATH_DEFINITION])
        this.eventEmitterService.saveTabEmitter.next();
      } else {
        console.warn("not path definition fits to the node")
      }
    }
  }

  setHasLoaded(hasLoaded: boolean) {
    this.setTabHasLoaded.next(hasLoaded);
  }
}
