import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, NgZone, ViewEncapsulation, ViewChild } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, merge, Observable, Subject, Subscription } from 'rxjs';
import { Bookmark } from 'src/app/data-structures/bookmark';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { TreeViewState } from 'src/app/data-structures/treeViewState';
import { NewItemDialogComponent, NewItemDialogData } from 'src/app/modules/main/components/dialog-components/new-item-dialog/new-item-dialog.component';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { BookmarkService } from '../../../services/bookmark.service';
import { MainInformationService } from '../../../services/main-information.service';
import { treeNode } from '../../../../../interfaces/tree'
import { TreeFunctionsService } from '../../../services/tree-functions.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { contextMenuObject } from '../../context-menu/context-interface';
import { NavigationTreeContextMenuDescription } from '../../context-menu/navigationTreeContextMenuDescription';
import { CRUDService } from 'src/app/services/crud.service';
import { sdmsException } from 'src/app/services/error-handler.service';
import { ClipboardService } from '../../../services/clipboard.service';
import { PrivilegesService } from '../../../services/privileges.service';
// interface treeNode {
//   id: number;
//   name: string;
//   namePath: string;
//   hasChildren: boolean;
//   isPinned: boolean;
//   isPinnedParent: boolean
//   isInvisible: boolean;


//   iconType: string;
//   type: string;

//   children?: treeNode[];
//   parentNode?: treeNode;
//   isNotSubmitable?: boolean;
// }


@Component({
  selector: 'app-navigation-tree',
  templateUrl: './navigationTree.component.html',
  styleUrls: ['./navigationTree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavigationTreeComponent implements OnInit, OnDestroy {

  constructor(private route: ActivatedRoute,
    private commandApiService: CommandApiService,
    private eventEmitterService: EventEmitterService,
    private localStorageHelper: LocalStorageHelper,
    public matDialog: MatDialog,
    public bookmarkService: BookmarkService,
    private mainInformationService: MainInformationService,
    private router: Router,
    private treeFunctionsService: TreeFunctionsService,
    private changeDetectorRef: ChangeDetectorRef,
    private toolboxService: ToolboxService,
    private crudService: CRUDService,
    private zone: NgZone,
    private clipboardService: ClipboardService,
    private privilegesService: PrivilegesService
  ) { }

  lastHoveredNode: treeNode | undefined;
  dragging: boolean = false;


  lastReload: string = '';

  treeControl = new NestedTreeControl<treeNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<treeNode>();

  isLoading = true;
  TYPE: string = '';
  shortType: string = '';
  treeNodes: treeNode[] = []

  bookmarkType: string = '';
  bookmarks: Bookmark[] = [];
  treeStateDiffersFromBookmark: boolean = false;

  expansionList: number[] = [];
  pinnedList: number[] = [];
  pinnedParrentList: number[] = [];
  pinnable: boolean = true;

  expandCommand: string = '0';

  searchControl = new UntypedFormControl('');

  searchString: string = '';

  expandCyclehook: Subject<any> = new Subject();

  expandProgress: number = 0;
  expandProgressSlice: number = 0;
  filter: boolean = false;
  // to show empty message if search delivers empty tree
  filterResultIsEmpty = false;
  loadCycle: any[] = [];

  submitOnly = false;

  rxjsRefreshListEmitter: Subscription = new Subscription();
  notifyDataChangeEmitter: Subscription = new Subscription();
  selectedBookmark: Bookmark = this.bookmarks[0];

  rxjsUserHasLoadedObserver: Subscription = new Subscription();



  hasLoadedChild = (_: number, node: treeNode) => !!node.children && node.children.length > 0;
  hasChildren = (_: number, node: treeNode) => node.hasChildren;

  ngOnInit(): void {

    this.TYPE = this.route.snapshot.data.TYPE;
    this.shortType = this.route.snapshot.data.shortType;
    this.pinnable = this.route.snapshot.data.pinnable;
    this.submitOnly = this.route.snapshot.data.SUBMITONLY;

    this.loadBookmarks();

    this.rxjsRefreshListEmitter = this.eventEmitterService.refreshNavigationEmittor.subscribe(event => {
      this.reloadTree();
    })
    this.notifyDataChangeEmitter = this.eventEmitterService.notifyDataChangeEmitter.subscribe((message: any) => {
      if (message.OP != "CLIPBOARD_CHANGE")
        return;
      this.updateFeFlaggedToCut();
    });

    // open from bookmarking tab, it changes the open bookmark in the routing and reloads the list
    this.route.queryParamMap.subscribe((data) => {
      let routeBookmarkName = data.get('selectedBookmark');
      let routteBookmarkScope = data.get('scope');
      if(routeBookmarkName && routteBookmarkScope) {
        let bookmarkToSelect = this.bookmarks.find(bookmark =>
          (bookmark.bookmark == routeBookmarkName) && (bookmark.scope == routteBookmarkScope)
        );
        if(bookmarkToSelect){
          this.selectBookmark(bookmarkToSelect,true, true)
        }
      }
    })

    // call reposition of tabs
    this.eventEmitterService.rePosNoFormEditors.next("navigationTreeCall");

    //set expand Command
    this.setExpandCommand();
    // // in bookmarking anyway
    // this.initTree(command);

    // for filter tree feature
    this.searchControl.setValue('');

    console.log(this.searchControl);
    this.searchControl.valueChanges.subscribe(value => {
      if (value == '') {
        this.reloadTree();
      }
    });

    // for loading bar
    this.expandCyclehook.subscribe(() => {
      this.loadCycle.push('')
      setTimeout(function (this: any) {
        this.loadCycle.pop();
      }.bind(this), 1000);
    });
  }

  ngOnDestroy(): void {
    this.rxjsUserHasLoadedObserver.unsubscribe();
    this.notifyDataChangeEmitter.unsubscribe();
  }

  loadBookmarks() {



    this.bookmarkType = this.route.snapshot.data.bookmarkType;

    // get selected bookmark from url
    let lastBookmark = this.localStorageHelper.getConnectionValue("selectedBoomark" + this.TYPE + (this.submitOnly ? 'SUBMIT' : ''));

    // user specific if present
    // load bookmarks even if there are no bookmarks loaded(to get defaults)
    this.getBookmarks();

    let bookmark: Bookmark | undefined = this.bookmarks.find(bookmark =>
      bookmark.bookmark == lastBookmark
    );


    // select bookmark if a specific is found, do not reload the tree because this will happen before the tree loads
    if (bookmark) {
      this.selectBookmark(bookmark, true, false);
    } else {
      this.loadStorageOptions();
    }

    // load bookmarks when userInformation(=bookmarks) has updated
    this.rxjsUserHasLoadedObserver = this.mainInformationService.userHasLoadedObserver.subscribe((subject: any) => {

      // console.log(subject);
      this.getBookmarks();

      // find selected bookmark
      let bookmark: Bookmark | undefined = this.bookmarks.find(bookmark => this.bookmarkService.isSameBookmark(bookmark, this.selectedBookmark)
        // (bookmark.bookmark == this.selectedBookmark?.bookmark) && (bookmark.scope == this.selectedBookmark?.scope) 
      );

      // select bookmark if a selection was found
      bookmark ? this.selectBookmark(bookmark, true, false) : '';
    });

  }
  getBookmarks(){
    this.bookmarks = this.bookmarkService.getBookmarksOfType(this.bookmarkType);
  }

  isSameBookmark(a: Bookmark, b: Bookmark) {
    return this.bookmarkService.isSameBookmark(a, b)
  }
  private _transformer = (node: any) => {
    // console.log(node);
    let treeNode: treeNode = {
      id: parseInt(node.ID),
      name: node.NAME.split('.').pop(), // get last element
      namePath: node.NAME,
      isPinned: false,
      isClickable: true,
      // this.pinnedList.indexOf(parseInt(node.ID, 10)) > -1

      isProperty: false,
      isPropertyParent: false,

      isPinnedParent: false,
      isInvisible: false,
      iconType: node.TYPE ? node.TYPE.toLowerCase() : node.USAGE ? node.USAGE.toLowerCase() : "other",
      // hasChildren: ((node.TYPE == 'FOLDER' || node.TYPE == 'SCOPE' || node.TYPE == undefined) && (parseFloat(node.SUBCATEGORIES) > 0 || parseFloat(node.SUBFOLDERS) > 0 || parseFloat(node.ENTITIES) > 0 || parseFloat(node.SUBSCOPES) > 0 || parseFloat(node.RESOURCES) > 0)),
      hasChildren: (
        (
          node.TYPE == 'FOLDER' &&
          (
            parseFloat(node.SUBFOLDERS) > 0 ||
            parseFloat(node.ENTITIES) > 0
          )
        ) ||
        (
          node.TYPE == 'SCOPE' &&
          parseFloat(node.SUBSCOPES) > 0
        ) ||
        (
          node.USAGE == 'CATEGORY' &&
          (
            parseFloat(node.SUBCATEGORIES) > 0 ||
            parseFloat(node.RESOURCES) > 0
          )
        )
      ),
      type: node.TYPE ? node.TYPE : node.USAGE ? node.USAGE : "other",

      isNotSubmitable: node.MASTER_SUBMITTABLE == 'false' || node.TYPE == "FOLDER",
      hasMasterSubmittableChildren: node.HAS_MSE == 'true',
      bicsuiteObject: node,
      textColor: ''
    }

    // colors for jobservers here
    if (node.hasOwnProperty('TYPE') && node.TYPE == 'SERVER') {
      if (node.IS_REGISTERED == 'true') {
        if ((node.ONLINE_SERVER == 'true' && node.IS_CONNECTED == 'true') ||
          (node.ONLINE_SERVER == 'false' && node.IDLE < node.NOPDELAY + 10 /* allow offline servers to be 10 seconds late */)
        ) {
          treeNode.textColor = 'GREEN';
        }
        else {
          treeNode.textColor = 'RED';
        }

      }
      else {
        treeNode.textColor = 'GREY';
      }
    }

    // treeNode.isPinned ? this.pinnedParrentList.concat(node.IDPATH.split('.')) : '';

    return treeNode;
  }


  initTree(command: string) {

    this.filter = false;

    command = command + this.expandCommand;
    let pinparentCollapseList: treeNode[] = [];
    let datasource: treeNode[] = this.commandApiService.executeDelegate(command, (result: any) => {
      if (result.hasOwnProperty('ERROR')) {
        throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
        return;
      } else {
        let treeNodes: treeNode[] = [];
        // console.log(result)
        let cutIds = this.clipboardService.getCutIds();

        //first element is the parent
        treeNodes.push(this._transformer(result.DATA.TABLE[0]));
        treeNodes[0].children = [];

        for (let o of result.DATA.TABLE) {
          if (cutIds.includes(o.ID)) {
            o.fe_flagged_to_cut = true;
          }

          if (o.hasOwnProperty('IDPATH')) {
            let idPathArray = o.IDPATH.split('.');
            let parentId = idPathArray[idPathArray.length - 2];
            if (parentId) {  // if there is a Parent

              let parentNode = this.searchTreeWithId(treeNodes[0], parentId) // finde node parent;

              if (parentNode) {  // init children if there is no
                let transformed = this._transformer(o);
                transformed.parentNode = parentNode;
                this.treeFunctionsService.insertNode(parentNode, transformed)

                // console.log(transformed.type)
                //if parennode not in expansion list ? -> is pinned parent
                if (!(this.expansionList.indexOf(parentNode.id) > -1)) {
                  pinparentCollapseList.indexOf(parentNode) < 0 ? pinparentCollapseList.push(parentNode) : '';
                }

              } else {
                // console.log("parent not found for: " + parent)
              }
            }
          } else {
            console.warn("mandatory propertys missing in TreeItem")
            console.warn(o)
          }
        }
        this.dataSource.data = [];
        this.dataSource.data = treeNodes;
        // console.log(this.dataSource.data)

        // expand first Node
        // this.expandNode(this.dataSource.data[0]);

        // expand all in expansionList
        for (let nodeId of this.expansionList) {
          let node = this.searchTreeWithId(treeNodes[0], nodeId);
          node ? this.treeControl.expand(node) : '';
        }

        // pin all items
        for (let nodeId of this.pinnedList) {
          let node = this.searchTreeWithId(treeNodes[0], nodeId);
          node ? this.pinNode(node) : '';
        }

        // collapse all not in expansionList
        for (let node of pinparentCollapseList) {
          this.treeControl.collapse(node);
          this.collapseNode(node);
        }

        this.isLoading = false;
        let date = new Date()
        this.lastReload = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        this.changeDetectorRef.detectChanges();

        // console.log(this.dataSource.data)
        this.resizeAndScroll();
      }
    });
  }

  // toggle is for the toggle button in the hmtl tree
  toggleNode(node: treeNode) {

    if (this.treeControl.isExpanded(node)) {
      // console.log("collapse toggle")
      // collapse
      if (node.children) {
        node.children = node.children.map(node => {
          node.isInvisible = node.isPinned || node.isPinnedParent ? false : true;
          return node;
        })
      }

      var index = this.expansionList.indexOf(node.id);
      index > -1 ? this.expansionList.splice(index, 1) : '';
      this.localStorageHelper.setConnectionValue("expansionList", this.expansionList.toString());
      // console.log(this.expansionList)
    } else if (!this.treeControl.isExpanded(node)) {
      // console.log("expand toggle")
      // expand
      if (node.children) {
        node.children = node.children.map(node => {
          node.isInvisible = false;
          return node;
        })
      }
      if (!this.expansionList.includes(node.id)) {
        this.expansionList.push(node.id);
        this.localStorageHelper.setConnectionValue("expansionList", this.expansionList.toString());
      }
    }
    if (node.children) {
      this.treeControl.toggle(node);

    } else {
      this.loadChildren(node);
    }
    this.treeStateDiffersFromBookmark = true;
    this.changeDetectorRef.detectChanges();
  }

  expandNode(node: treeNode, NOLIST?: boolean) {

    // expand
    if (node.children) {
      node.children = node.children.map(node => {
        node.isInvisible = false;
        return node;
      })
    }
    if (!this.expansionList.includes(node.id) && !NOLIST) {
      this.expansionList.push(node.id);
      this.localStorageHelper.setConnectionValue("expansionList", this.expansionList.toString());
      this.treeStateDiffersFromBookmark = true;
    }

    if (node.children) {
      // console.log("expand");
      // this.treeControl.isExpanded(node) ? console.log("already expanded") : console.log("expand"); 
      this.treeControl.expand(node);

    } else {
      this.loadChildren(node);
    }
  }

  collapseNode(node: treeNode) {
    // collapse
    if (node.children) {
      node.children = node.children.map(node => {
        node.isInvisible = node.isPinned || node.isPinnedParent ? false : true;
        return node;
      })
    }

    var index = this.expansionList.indexOf(node.id);
    index > -1 ? this.expansionList.splice(index, 1) : '';
    this.localStorageHelper.setConnectionValue("expansionList", this.expansionList.toString());
    this.treeStateDiffersFromBookmark = true;
    if (node.children) {
      this.treeControl.collapse(node);
    }

  }

  openTab(node: treeNode) {
    // console.log(node)
    if (this.submitOnly) {
      if (node.bicsuiteObject.TYPE == "FOLDER") {
        this.toggleNode(node);
      }
      else {
        let crud = this.crudService.createOrGetCrud("JOB");
        // create Tab as it would be when created/ no real tabinfo, but validate needs tabinfo
        let tab = new Tab(node.bicsuiteObject.NAME, node.bicsuiteObject.ID, "JOB", "job", TabMode.EDIT);
        this.crudService.read(crud, node.bicsuiteObject, tab).then((response: any) => {
          return crud.callMethod("submitjob", response.response.DATA.RECORD, tab);
        });
      }
    }
    else {
      let tab = new Tab(node.name, node.id.toString() /* node.id.toString() */, node.type, node.iconType, TabMode.EDIT);
      tab.PATH = node.parentNode != undefined ? node.parentNode.namePath : "";
      this.eventEmitterService.createTab(tab, true);
    }
  }

  createItem() {

    const dialogRef = this.matDialog.open(NewItemDialogComponent, {
      data: {
        title: "new_item",
        translate: "create_new_item_chooser",
        itemTypes: this.route.snapshot.data.ITEMTYPES
      },
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result) {
        let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
        // tab.MODE = TabMode.NEW;
        // tab.NAME = '*untitled';
        // tab.TYPE = result.choosedItem;
        // tab.shortType = ;
        this.eventEmitterService.createTab(tab, true);
      }
    });
  }

  reloadTree() {
    this.isLoading = true;
    this.pinnedParrentList = [];
    let command = this.route.snapshot.data.command;
    this.treeControl = new NestedTreeControl<treeNode>(node => node.children);


    this.setExpandCommand();
    this.initTree(command);
  }

  collapseAll() {
    for (let id of this.expansionList) {
      let node = this.searchTreeWithId(this.dataSource.data[0], id)
      if (node) {
        // set timeout here to add the functions to the end of the angular lifecycle to ensure angular change detection will update the UI changes
        setTimeout(function (this: any) {
          this.collapseNode(node);
          this.treeControl.collapse(node);
        }.bind(this), 0);
      }
    }
  }

  // collapseFilter() {

  // }




  async expandAll(node: treeNode) {
    // console.log("expandAll(" + node.id + ")");
    this.expandNode(node, true);
    this.expandProgress = this.expandProgress + this.expandProgressSlice;

    if (node.children) {
      setTimeout(function (this: any) {
        this.expandCyclehook.next('');
        for (let children of node.children!) {

          if (children.children) {

            this.expandAll(children);

          }
        }
      }.bind(this), 0);

    } else {
      return;
    }
    this.treeStateDiffersFromBookmark = true;
  }

  loadChildren(node: treeNode) {



    this.isLoading = true;
    console.debug("loadChildren(" + node.id + ")");
    // console.log(this.TYPE);
    let command = this.route.snapshot.data.command + " " + this.toolboxService.namePathQuote(node.namePath);
    if (this.TYPE == 'FOLDER') {
      command = this.route.snapshot.data.command.replace('SYSTEM', this.toolboxService.namePathQuote(node.namePath))
    } else if (this.TYPE == 'SCOPE') {
      command = this.route.snapshot.data.command.replace('GLOBAL', this.toolboxService.namePathQuote(node.namePath))
    }
    this.commandApiService.executeDelegate(command, (result: any) => {
      if (result.hasOwnProperty('ERROR')) {

        throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
        return;
      } else {
        let treenodeList: treeNode[] = [];

        for (let o of result.DATA.TABLE) {
          // get parentId

          let transformed = this._transformer(o);
          // set parentId
          transformed.parentNode = node;
          if (transformed.id != node.id) {
            treenodeList.push(transformed);
          }
        }
        treenodeList.sort(this.treeFunctionsService.compare);
        node.children = treenodeList;

        const data = this.dataSource.data;
        this.dataSource.data = [];
        this.dataSource.data = data;

        this.treeControl.toggle(node);
        this.isLoading = false;
        this.changeDetectorRef.detectChanges();

        return;
      }
    });
  }

  pinNode(node: treeNode) {
    node.isPinned = true;
    // console.log(this.pinnedParrentList)
    if (!(this.pinnedList.indexOf(node.id) > -1)) {
      //add pin to pinnedlist
      this.pinnedList.push(node.id);
      this.localStorageHelper.setConnectionValue("pinnedList", this.pinnedList.toString());
      this.treeStateDiffersFromBookmark = true;
    } else {
      // console.log("is already pinned")
    }


    // //add Pinparents
    this.addPinparents(node)
  }

  addPinparents(node: treeNode) {
    let parentNode = node.parentNode;
    if (parentNode) {
      this.pinnedParrentList.push(parentNode.id)
      parentNode.isPinnedParent = true;
      this.addPinparents(parentNode);
    } else {
      // console.log(node.name);
      // console.log(this.pinnedParrentList);
    }

  }

  unpinNode(node: treeNode) {
    node.isPinned = false;
    // console.log(this.pinnedParrentList)
    if ((this.pinnedList.indexOf(node.id) > -1)) {
      // console.log("pin removed from list")
      var index = this.pinnedList.indexOf(node.id);
      index > -1 ? this.pinnedList.splice(index, 1) : '';
      this.localStorageHelper.setConnectionValue("pinnedList", this.pinnedList.toString())
      this.treeStateDiffersFromBookmark = true;
      this.removePinParent(node);
    } else {
      // console.log("is already unpinned")
    }
  }

  removePinParent(node: treeNode) {

    let parentNode = node.parentNode;
    if (parentNode) {
      var index = this.pinnedParrentList.indexOf(parentNode.id);
      index > -1 ? this.pinnedParrentList.splice(index, 1) : '';
      // console.log("pinparrent " + parentNode.namePath + " removed from list")

      if ((this.pinnedParrentList.indexOf(parentNode.id) > -1)) {
        // console.log("parentnode" + parentNode.name + " is still in the pinparent list")

      } else {
        // console.log("parentnode" + parentNode.name + " is no longer in the pinparent list")
        parentNode.isPinnedParent = false;

        // if parent is collapsed and no long a pinparent -> collapse node again to hide the unpinned children again
        if (!this.treeControl.isExpanded(parentNode)) {
          this.collapseNode(parentNode)
        }
      }
      // console.log(parentNode.namePath)
      // parentNode.isPinnedParent = false;
      this.removePinParent(parentNode);
    } else {
      // console.log(node.name);
      // console.log(this.pinnedParrentList);
    }

  }

  filterTree(value: string) {
    this.filter = true;
    this.expandProgress = 0;

    if (value == '') {
      this.reloadTree();
      return;
    }

    this.isLoading = true;
    this.pinnedParrentList = [];
    let command = this.route.snapshot.data.command;

    // command = command + this.expandCommand + ', NAME LIKE \'.*' + value + '.*\'';
    // command = command + ' WITH EXPAND = ALL, NAME LIKE \'.*' + value + '(?:.(?!\\.))+$\'';
    command = command + ' WITH EXPAND = ALL, NAME LIKE \'(?i).*' + value + '[^.]*$\'';
    // [^;]+(?=;[^;]*$)

    let pinparentCollapseList: treeNode[] = [];
    let datasource: treeNode[] = this.commandApiService.executeDelegateWithRegEx(command, (result: any) => {


      if (result.hasOwnProperty('ERROR')) {

        throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
        return;
        // }
        // console.log(result);
      } else {
        if (result.DATA.TABLE.length > 1200) {
          this.expandProgress = 100;
          this.isLoading = false;
          this.eventEmitterService.pushSnackBarMessage(['too_many_search_results'], SnackbarType.WARNING)
          return
        }


        this.expandProgressSlice = 100 / (0.62 * result.DATA.TABLE.length);

        let treeNodes: treeNode[] = [];
        // console.log(result)

        //first element is the parent
        treeNodes.push(this._transformer(result.DATA.TABLE[0]));
        treeNodes[0].children = [];

        for (let o of result.DATA.TABLE) {
          if (o.hasOwnProperty('IDPATH')) {
            let idPathArray = o.IDPATH.split('.');

            let parentId = idPathArray[idPathArray.length - 2];
            if (parentId) {  // if there is a Parent

              let parentNode = this.searchTreeWithId(treeNodes[0], parentId) // finde node parent;

              if (parentNode) {  // init children if there is no
                let transformed = this._transformer(o);
                transformed.parentNode = parentNode;

                this.treeFunctionsService.insertNode(parentNode, transformed);

                // console.log(transformed.type)
                //if parennode not in expansion list ? -> is pinned parent
                if (!(this.expansionList.indexOf(parentNode.id) > -1)) {
                  pinparentCollapseList.indexOf(parentNode) < 0 ? pinparentCollapseList.push(parentNode) : '';
                }

              } else {
                // console.log("parent not found for: " + parent)
              }
            }
          } else {
            console.warn("mandatory propertys missing in TreeItem")
            console.warn(o)
          }
        }

        this.dataSource.data = treeNodes;
        // console.log(this.dataSource.data)

        // console.log(this.dataSource.data[0])
        this.expandAll(this.dataSource.data[0]);

        // expand first Node
        // this.expandNode(this.dataSource.data[0]);

        // expand all in expansionList
        // for (let nodeId of this.expansionList) {
        //   let node = this.searchTreeWithId(treeNodes[0], nodeId);
        //   node ? this.treeControl.expand(node) : '';
        // }

        // chek


        // pin all items
        for (let nodeId of this.pinnedList) {
          let node = this.searchTreeWithId(treeNodes[0], nodeId);
          node ? this.pinNode(node) : '';
        }

        // collapse all not in expansionList
        // for (let node of pinparentCollapseList) {
        //   this.treeControl.collapse(node);
        //   this.collapseNode(node);
        //   console.log("collapse" + node.id)
        // }

        this.isLoading = false;
        let date = new Date()
        this.lastReload = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
      }
    });
  }

  newBookmark() {

    // {
    //   title: "save_bookmark_as",
    //   translate: "choose_bookmark_scope",
    //   itemTypes: this.privilegesService.isAdmin() ? ["USER", "SYSTEM"] : ["USER"],
    //   inputEnabled: true,
    //   iconsDisabled: true,
    //   input: this.selectedBookmark ? this.selectedBookmark.bookmark : ''
    // }
    const dialogRef = this.matDialog.open(NewItemDialogComponent, {
      data: this.bookmarkService.getNewBookmarkDialogData(this.selectedBookmark),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result.input) {
        // console.log(result.input)
        let bookmark = new Bookmark(result.input, this.bookmarkType);
        bookmark.scope = result.choosedItem;


        // empty yet

        let treeViewState = new TreeViewState();
        treeViewState.expansionList = this.expansionList;
        treeViewState.pinnedList = this.pinnedList;

        bookmark.value = treeViewState;

        this.bookmarkService.setUserBookmark(bookmark, false).then(() => {
          this.treeStateDiffersFromBookmark = false;
          
          this.selectBookmark(bookmark, true, true);
        });

        // this.selectBookmark(bookmark, true, true);

        this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_created'], SnackbarType.INFO)
      }
    });
  }

  saveBookmark() {
    let treeViewState = new TreeViewState();
    treeViewState.expansionList = this.expansionList;
    treeViewState.pinnedList = this.pinnedList;

    this.selectedBookmark.value = treeViewState;
    this.bookmarkService.setUserBookmark(this.selectedBookmark).then(() => {
      this.treeStateDiffersFromBookmark = false;
      this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_updated'], SnackbarType.INFO)
    }, () => { });
    this.treeStateDiffersFromBookmark = false;
  }

  selectBookmark(bookmark: Bookmark, reload: boolean, changeTreeState: boolean) {

    // load bookmarks from the repository to have lates state
    this.getBookmarks();
    // if bookmark.value is empty
    if (Object.keys(bookmark.value).length === 0) {
      bookmark.value = new TreeViewState();
    }

    this.selectedBookmark = bookmark;

    // on init the treestate should no change to keep opened folder
    if (changeTreeState) {
      this.expansionList = this.selectedBookmark.value.expansionList;
      this.pinnedList = this.selectedBookmark.value.pinnedList;
      this.localStorageHelper.setConnectionValue("expansionList", this.expansionList.toString());
      this.localStorageHelper.setConnectionValue("pinnedList", this.pinnedList.toString());

      // set changed to false
      this.treeStateDiffersFromBookmark = false;
    } else {
      // set changed to true if not the same
      this.treeStateDiffersFromBookmark = true;
    }

    this.localStorageHelper.setConnectionValue("selectedBoomark" + this.TYPE + (this.submitOnly ? 'SUBMIT' : ''), this.selectedBookmark.bookmark);
    reload ? this.reloadTree() : '';
  }

  loadStorageOptions() {

    if (Object.keys(this.bookmarks[0].value).length === 0) {
      this.bookmarks[0].value = new TreeViewState();
    }
    this.selectedBookmark = this.bookmarks[0];
    let localExpList = this.localStorageHelper.getConnectionValue("expansionList");
    if (localExpList) {
      this.expansionList = localExpList.split(',').map(function (item: string) {
        return parseInt(item, 10);
      });
    }

    let localPinList = this.localStorageHelper.getConnectionValue("pinnedList");
    if (localPinList) {
      this.pinnedList = localPinList.split(',').map(function (item: string) {
        return parseInt(item, 10);
      });
    }

    this.reloadTree();
  }

  updateFeFlaggedToCut(node?: treeNode, cutIds?: string[]) {
    if (!cutIds) {
      cutIds = this.clipboardService.getCutIds();
    }
    if (!node) {
      node = this.dataSource.data[0];
    }
    node.bicsuiteObject.fe_flagged_to_cut = cutIds.includes(node.bicsuiteObject.ID);
    if (node.children) {
      for (let i = 0; i < node.children.length; i++) {
        this.updateFeFlaggedToCut(node.children[i] as treeNode, cutIds);
      }
    }
  }

  searchTreeWithId(element: treeNode, matchingId: number): treeNode | null {
    if (element.id == matchingId) {
      return element;
    } else if (element.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < element.children.length; i++) {
        result = this.searchTreeWithId(element.children[i], matchingId);
      }
      return result;
    }
    return null;
  }

  setExpandCommand() {
    let expansionListString = '0';
    let pinnedListString = '0';

    if (!this.localStorageHelper.getConnectionValue("expansionList")) {
      this.localStorageHelper.setConnectionValue("expansionList", "");
    } else {
      let expansionListFromStorageString = this.localStorageHelper.getConnectionValue("expansionList");
      expansionListString = expansionListFromStorageString;
      this.expansionList = expansionListFromStorageString.split(',').map(function (item: string) {
        return parseInt(item, 10);
      });
    }

    if (!this.localStorageHelper.getConnectionValue("pinnedList")) {
      this.localStorageHelper.setConnectionValue("pinnedList", "");
    } else {
      let pinnedListFromStorageString = this.localStorageHelper.getConnectionValue("pinnedList");
      pinnedListString = pinnedListFromStorageString;
      this.pinnedList = pinnedListFromStorageString.split(',').map(function (item: string) {
        return parseInt(item, 10);
      });
    }
    this.expandCommand = " with expand = (" + expansionListString + "," + pinnedListString + ")"
  }

  close() {
    this.eventEmitterService.rePosNoFormEditors.next("navigationTreeCall")
    this.router.navigate(['/home/']);
  }

  // drop(event: any) {
  //   // console.log(event)
  //   // if (event.previousContainer === event.container) {
  //   //   // moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);

  //   //   moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
  //   // } else {

  //   //   transferArrayItem(event.previousContainer.data,
  //   //     event.container.data,
  //   //     event.previousIndex,
  //   //     event.currentIndex);
  //   // }

  // }

  dragHover(node: treeNode) {

    this.lastHoveredNode = node;
    node.isDragTarget = true;
  }
  dragHoverEnd(node: treeNode) {
    // console.log(node)
    node.isDragTarget = false;
  }
  dragStart(node: treeNode) {
    this.dragging = true;
    // console.log(node)
  }
  dragEnd(node: treeNode, event: any) {
    this.dragging = false;
    this.zone.run(() => {
      this.lastHoveredNode ? this.moveNode(this.lastHoveredNode, node) : '';
    })

    this.changeDetectorRef.detectChanges();
  }

  moveNode(targetNode: treeNode, itemNode: treeNode) {


    // console.log("Moved " + itemNode.namePath + " to " + targetNode.namePath);
    // COPY    JOB DEFINITION SYSTEM.'TRAINING'.'WASSERBATCH_SECOND'.'TWO' TO SYSTEM.'TRAINING'.'REPORT'.'REFRESH_DATA'

    let cmd = 'MOVE';

    if (itemNode.type == 'FOLDER') {
      cmd += ' FOLDER '
    } else if (itemNode.type == 'JOB') {
      cmd += ' JOB DEFINITION '
    } else if (itemNode.type == 'MILESTONE') {
      cmd += ' MILESTONE '
    } else if (itemNode.type == 'BATCH') {
      cmd += ' JOB DEFINITION '
    } else {
      this.eventEmitterService.pushSnackBarMessage(["Itemtype ", itemNode.type, " not allowed to move"], SnackbarType.INFO)
      return;
    }
    cmd += itemNode.namePath + " TO " + targetNode.namePath;
    this.commandApiService.execute(cmd).then((result: any) => {
      // console.log(result)
      // if (result.hasOwnProperty('ERROR')) {
      //   throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
      //   return;
      // } else {
      //   console.log(result);


      this.eventEmitterService.pushSnackBarMessage(["Moved", itemNode.name, "to", targetNode.name], SnackbarType.INFO)
      this.reloadTree();

      // }

    }, (rejection: any) => {
      // do nothing
      return;
    })
  }
  // ---------------------------- CONTEXT MENU, same as in navigation list---------------------

  isDisplayContextMenu: boolean = false;
  rightClickMenuItems: contextMenuObject | undefined;
  rightClickMenuPositionX: number = 0;
  rightClickMenuPositionY: number = 0;

  displayContextMenu(event: any, node: any) {
    this.isDisplayContextMenu = true;
    let contextMenuObject: contextMenuObject = new NavigationTreeContextMenuDescription(node).contextMenu;
    this.rightClickMenuItems = contextMenuObject;

    // check if the context menu is out of bound
    // get  height of the window
    var height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;
    let heightOffSetMargin = 280;
    if (height - (event.clientY + heightOffSetMargin) > 0) {
      this.rightClickMenuPositionX = event.clientX;
      this.rightClickMenuPositionY = event.clientY;
    } else {
      this.rightClickMenuPositionX = event.clientX;
      this.rightClickMenuPositionY = event.clientY - heightOffSetMargin;
    }
  }

  getRightClickMenuStyle() {
    return {
      position: 'fixed',
      left: `${this.rightClickMenuPositionX}px`,
      top: `${this.rightClickMenuPositionY}px`
    }
  }

  handleMenuItemClick(event: any) {
    if (event.data) {
      // console.log(this.rightClickMenuItems);
    }
  }


  @HostListener('document:click')
  documentClick(): void {
    this.isDisplayContextMenu = false;
  }
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.isDisplayContextMenu = false;
  }
  @HostListener('window:mousedown', ['$event'])
  handleMouseClick(evt: MouseEvent) {
    // console.log('globalEvent');
    // console.log(evt);
    switch (evt.button) {
      case 0:
        break;
      case 1:
        break;
      case 2:
        this.isDisplayContextMenu = false;
        break;
    }
  }

  scrollTimeOut: any;
  scrollpostion: number = 0;
  containerMinHeight: number | undefined = 0;


  @HostListener('window:scroll', ['$event']) onScroll(event: any) {
    let height = document.getElementById('navigationTree-ident')?.getBoundingClientRect().height;
    this.scrollpostion = event.srcElement.scrollTop;
    this.containerMinHeight = height;
    clearTimeout(this.scrollTimeOut);
    this.scrollTimeOut = setTimeout(() => {
      // this.eventEmitterService.saveTabEmitter.next();
      this.localStorageHelper.setConnectionValue('scrollpostion' + this.TYPE, this.scrollpostion);
      this.localStorageHelper.setConnectionValue('containerMinHeight' + this.TYPE, this.containerMinHeight);
    }, 500);
  }

  resizeAndScroll() {
    //  wait for container to be rendered
    this.toolboxService.waitForElm('navigation-tree-content').then(() => {
      let renderbody = document.getElementById('navigationTree-ident');
      if (renderbody) {
        // console.log(this.tab.TABSTATE.containerMinHeight + 'px')
        renderbody.style.minHeight = this.localStorageHelper.getConnectionValue('containerMinHeight' + this.TYPE) + 'px';
        // console.log(this.tab.TABSTATE.containerMinHeight)
        this.scrollTabBody(this.localStorageHelper.getConnectionValue('scrollpostion' + this.TYPE));
      }
    });
  }

  scrollTabBody(pos: number) {
    setTimeout(() => {
      let htmlOpj = document.getElementById('navigation-tree-content')
      if (htmlOpj) {
        htmlOpj.scrollTop = pos;
      }
      this.resetMinHeight();
    }, 25);
  }

  resetMinHeight() {
    let renderbody = document.getElementById('navigationTree-ident');
    if (renderbody) {
      renderbody.style.minHeight = 0 + 'px';
    }
    // console.log("renderformdata end")
  }



}
