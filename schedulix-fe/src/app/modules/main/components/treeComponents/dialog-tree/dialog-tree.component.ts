
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';

import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, merge, Observable, Subject, Subscription } from 'rxjs';
import { Bookmark } from 'src/app/data-structures/bookmark';
import { Tab } from 'src/app/data-structures/tab';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { CRUDService } from 'src/app/services/crud.service';
import { sdmsException } from 'src/app/services/error-handler.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { treeNode } from '../../../../../interfaces/tree'
import { TreeFunctionsService } from '../../../services/tree-functions.service';
import { ChooserDialogData } from '../../form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { RowButtonComponent } from '../../form-generator/row-button/row-button.component';


@Component({
  selector: 'app-dialog-tree',
  templateUrl: './dialog-tree.component.html',
  styleUrls: ['./dialog-tree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DialogTreeComponent implements OnInit, OnDestroy {


  constructor(private route: ActivatedRoute,
    private commandApiService: CommandApiService,
    private eventEmitterService: EventEmitterService,
    private localStorageHelper: LocalStorageHelper,
    public matDialog: MatDialog,
    private treeFunctionsService: TreeFunctionsService,
    private toolboxService: ToolboxService,
    private crudService: CRUDService) { }


  treeControl = new NestedTreeControl<treeNode>(node => node.children);
  dataSource = new MatTreeNestedDataSource<treeNode>();

  isLoading = true;
  treeNodes: treeNode[] = []

  bookmarkType: string = '';
  bookmarks: Bookmark[] = [];

  expansionList: number[] = [];
  pinnedList: number[] = [];
  pinnedParrentList: number[] = [];
  pinnable: boolean = true;

  expandCommand: string = '0';

  searchControl = new UntypedFormControl();

  searchString: string = '';

  expandCyclehook: Subject<any> = new Subject();

  expandProgress: number = 0;
  expandProgressSlice: number = 0;
  filter: boolean = false;
  loadCycle: any[] = [];

  submitOnly = false;

  selectedBookmark: Bookmark = this.bookmarks[0];

  rxjsUserHasLoadedObserver: Subscription = new Subscription();

  @Input() dialogData: ChooserDialogData = new ChooserDialogData();
  @Input() tabInfo: Tab | undefined;
  @Input() editorFormfield: any;
  @Input() parentBicsuiteObject: any;

  @Output() clickCallback = new EventEmitter();

  hasLoadedChild = (_: number, node: treeNode) => !!node.children && node.children.length > 0;
  hasChildren = (_: number, node: treeNode) => node.hasChildren;
  isProperty = (_: number, node: treeNode) => node.isProperty;
  isPropertyParent = (_: number, node: treeNode) => node.isPropertyParent;
  isNoPropertyNoPropertyParentNoChildren = (_: number, node: treeNode) => (!node.isPropertyParent && !node.hasChildren && !node.isProperty);
  hasChildrenIsPropertyParent = (_: number, node: treeNode) => (node.isPropertyParent || node.hasChildren);
  isPropertyNoChildren = (_: number, node: treeNode) => (!node.hasChildren && node.isProperty);
  isPropertyWithChildren = (_: number, node: treeNode) => (node.hasChildren && node.isProperty);

  ngOnInit(): void {


    if (this.editorFormfield.TREE_CHOOSER_SUBMIT_ONLY == 'true') {
      this.submitOnly = true;
    }

    this.initTree(this.dialogData.command);
    // for filter tree feature
    this.searchControl.setValue('');
    this.searchControl.valueChanges.subscribe(value => {
      if (value == '') {
        this.reloadTree();
      }
    });

    // for loading bar
    this.expandCyclehook.subscribe(() => {
      this.loadCycle.push('')
      setTimeout(function (this: any) {
        this.loadCycle.pop();
      }.bind(this), 1000);
    });

  }

  ngOnDestroy(): void {
    this.rxjsUserHasLoadedObserver.unsubscribe();
  }



  private _chooserNodeTransformer = (node: any, parentPath: string) => {
    // console.log(node);
    let treeNode: treeNode = {

      id: parseInt(node.ID),
      name: node.NAME, // get last element
      namePath: parentPath,
      isPinned: false,
      isClickable: true,
      // this.pinnedList.indexOf(parseInt(node.ID, 10)) > -1

      isPinnedParent: false,
      isInvisible: false,

      isProperty: true,
      isPropertyParent: false,

      iconType: "none", // Set in loadProperties(...)
      hasChildren: false,
      type: "none", // Set in loadProperties(...)
      isNotSubmitable: node.MASTER_SUBMITTABLE == 'false',
      bicsuiteObject: node,
      textColor: ''
    }

    return treeNode;
  }

  setCategoryHasChildren(table: any[], idPath: string, parentsProcessed: any) {
    let idPathArray = idPath.split('.');
    idPathArray.pop(); // remove last id which is id of named resource

    for (let id of idPathArray) {
      if (parentsProcessed[id] == true) {
        continue; // we set this to has Children already
      }
      for (let row of table) {
        if (row.ID == id) {
          row.f_hasChildren = true;
        }
      }
      parentsProcessed[id] = true;
    }
  }

  transformNamedResourceTable(table: any[], usages: string[]): any[] {
    let parentsProcessed = {}
    for (let row of table) {
      if (row.USAGE != "CATEGORY") {
        if (!usages.includes(row.USAGE)) {
          row.f_isHidden = true;
        }
        else {
          row.f_isProperty = true;
          this.setCategoryHasChildren(table, row.IDPATH, parentsProcessed);
        }
      }
    }
    // remove rows not to dislay now
    let resultTable = [];
    for (let row of table) {
      if ((row.USAGE == "CATEGORY" && row.f_hasChildren == true) || (row.USAGE != "CATEGORY" && row.f_isHidden != true)) {
        resultTable.push(row);
      }
    }
    return resultTable;
  }

  customTransform(table: any[]): any[] {
    switch (this.dialogData.customTransform) {
      case "":
        break;
      case "staticResourcesTransformer":
        return this.transformNamedResourceTable(table, ["STATIC"]);
      case "systemResourcesTransformer":
        return this.transformNamedResourceTable(table, ["SYSTEM"]);
      case "systemOrSynchronizingResourcesTransformer":
        return this.transformNamedResourceTable(table, ["SYSTEM", "SYNCHRONIZING"]);
    }
    return table;
  }

  async initTree(command: string) {

    this.filter = false;

    // command = command + this.expandCommand;
    let pinparentCollapseList: treeNode[] = [];
    this.commandApiService.execute(command).then(async (result: any) => {
      this.isLoading = true;
      // if (result.hasOwnProperty('ERROR')) {
      //   console.warn(result);
      //   //tell User that something went wrong
      //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
      //   return;
      // } else {
        let treeNodes: treeNode[] = [];
        // console.log(result)

        // insert nonable node if noneablechooser
        // maybe the bicsuitedate has to me altered to work properly with other trees. TODOS: testing
        if (this.dialogData.nonable) {
          let noneDisplay = 'NONE';
          if (this.dialogData.noneDisplay) {
            noneDisplay = this.dialogData.noneDisplay
          }
          let o = this.treeFunctionsService.getDummyDialogTreeNode(noneDisplay);
          o.isClickable = true;
          o.iconType = 'fe_none';
          o.bicsuiteObject = { NAME: (this.dialogData.noneValue ? this.dialogData.noneValue : '') };
          o.namePath = (this.dialogData.noneValue ? this.dialogData.noneValue : '');
          // console.log(o)
          treeNodes.push(o);
        }

        //first element is the parent
        treeNodes.push(this.treeFunctionsService._dialogTransformer(result.DATA.TABLE[0], this.dialogData));
        treeNodes[0].children = [];

        result.DATA.TABLE = this.customTransform(result.DATA.TABLE);

        for (let o of result.DATA.TABLE) {
          if (o.hasOwnProperty('IDPATH')) {
            let idPathArray = o.IDPATH.split('.');
            let parentId = idPathArray[idPathArray.length - 2];
            if (parentId) {  // if there is a Parent

              let parentNode = this.searchTreeWithId(treeNodes[0], parentId) // finde node parent;

              if (parentNode) {  // init children if there is no
                let transformed = this.treeFunctionsService._dialogTransformer(o, this.dialogData);
                transformed.parentNode = parentNode;
                this.treeFunctionsService.insertNode(parentNode, transformed)
              } else {
                // console.log("parent not found for: " + parent)
              }
            }
          } else {
            console.warn("mandatory propertys missing in TreeItem")
            console.warn(o)
          }
        }

        for (let node of treeNodes) {
          if (node.children && node.isPropertyParent) {
            await this.loadProperties(node).then((result) => {
              node.children = node.children!.concat(result);
              // console.log(node.children);
            });
          }
        }

        this.dataSource.data = treeNodes;

        for (let node of this.dataSource.data) {
          this.treeControl.expand(node);
          // this.toggleNode(node);
        }

        this.crudFilter();
        const data = this.dataSource.data;
        // console.log(data);
        this.dataSource.data = [];
        this.dataSource.data = data;

        this.isLoading = false;
      // }
    }, (rejection: any) => {
      this.isLoading = false;
      // do nothing
      return
    });
  }

  // toggle is for the toggle button in the hmtl tree
  toggleNode(node: treeNode) {

    if (this.treeControl.isExpanded(node)) {
      // console.log("collapse toggle")
      // collapse
      if (node.children) {
        node.children = node.children.map(node => {
          node.isInvisible = node.isPinned || node.isPinnedParent ? false : true;
          return node;
        })
      }
    } else if (!this.treeControl.isExpanded(node)) {
      // console.log("expand toggle")
      // expand
      if (node.children) {
        node.children = node.children.map(node => {
          node.isInvisible = false;
          return node;
        })
      }
    }
    if (node.children) {
      this.treeControl.toggle(node);
    } else {
      let promises: Promise<any>[] = [];
      if (node.hasChildren) {
        promises.push(this.loadChildren(node));
      }
      if (node.isPropertyParent) {
        promises.push(this.loadProperties(node));
      }
      // console.log(node)
      Promise.all(promises).then((result: any[]) => {
        let children: any[] = [];
        children = children.concat(result[0] ? result[0] : []);
        children = children.concat(result[1] ? result[1] : []);
        node.children = children;

        this.crudFilter();
        // Triggers change detection for tree.
        const data = this.dataSource.data;
        this.dataSource.data = [];
        this.dataSource.data = data;

        this.treeControl.expand(node);
        this.isLoading = false;
      }, (rejection: any) => {
        this.isLoading = false;
      });
    }
  }

  expandNode(node: treeNode, NOLIST?: boolean) {

    // expand
    if (node.children) {
      node.children = node.children.map(node => {
        node.isInvisible = false;
        return node;
      })
    }
    if (node.children) {
      this.treeControl.expand(node);

    } else {
      this.loadChildren(node);
    }
  }

  collapseNode(node: treeNode) {
    // collapse
    if (node.children) {
      node.children = node.children.map(node => {
        node.isInvisible = node.isPinned || node.isPinnedParent ? false : true;
        return node;
      })
    }
    if (node.children) {
      this.treeControl.collapse(node);
    }

  }


  openTab(node: treeNode) {
    this.clickCallback.emit(node);
  }


  reloadTree() {
    this.isLoading = true;
    this.pinnedParrentList = [];
    this.initTree(this.dialogData.command);
  }

  async expandAll(node: treeNode) {
    this.expandNode(node, true);
    this.expandProgress = this.expandProgress + this.expandProgressSlice;
    if (node.children) {
      setTimeout(function (this: any) {
        this.expandCyclehook.next('');
        for (let children of node.children!) {

          if (children.children) {

            this.expandAll(children);

          }
        }
      }.bind(this), 0);

    } else {
      return;
    }
  }

  private loadChildren(node: treeNode): Promise<any> {
    this.isLoading = true;
    // console.log(node)
    //    console.debug("loadChildren(" + node.id + ")");
    // console.log(this.TYPE);
    let command = this.dialogData.command + " " + this.toolboxService.namePathQuote(node.namePath);
    if (this.dialogData.TYPE == 'FOLDER') {
      command = this.dialogData.command.replace('SYSTEM', this.toolboxService.namePathQuote(node.namePath))
    } else if (this.dialogData.TYPE == 'SCOPE') {
      command = this.dialogData.command.replace('GLOBAL', this.toolboxService.namePathQuote(node.namePath))
    } else if (this.dialogData.TYPE == 'NAMED RESOURCE') {
      command = this.dialogData.command.replace('NAMED RESOURCE', 'NAMED RESOURCE ' + this.toolboxService.namePathQuote(node.namePath))
    }
    // console.log(command)
    return this.commandApiService.execute(command).then((result: any) => {
      // if (result.hasOwnProperty('ERROR')) {
      //   console.warn(result);
      //   //tell User that something went wrong
      //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
      //   return;
      // } else {
        let treenodeList: treeNode[] = [];

        for (let o of result.DATA.TABLE) {
          // get parentId

          let transformed = this.treeFunctionsService._dialogTransformer(o, this.dialogData);
          // set parentId
          transformed.parentNode = node;
          if (transformed.id != node.id) {
            treenodeList.push(transformed);
          }
        }
        // console.log(treenodeList)
        treenodeList.sort(this.treeFunctionsService.compare);
        return treenodeList;
      // }
    }, (rejection: any) => {
      // do nothing
      return
    });
  }

  private loadProperties(node: treeNode): Promise<any> {
    this.isLoading = true;
    // console.log("load porperties....")
    // console.debug("loadProperties(" + node.id + ")");
    // console.log(this.dialogData.chooseType);

    let command = '';

    switch (this.dialogData.childSelect) { // ***Note that arrays are not supported. Might come in future if needed!!
      case "PARAMETER":

        if (node.type == 'FOLDER') {
          command = 'SHOW FOLDER ' + this.toolboxService.namePathQuote(node.namePath);
        } else {
          command = 'SHOW job definition ' + this.toolboxService.namePathQuote(node.namePath);
        }
        return this.commandApiService.execute(command).then((result: any) => {
          console.warn(result);
          // if (result.hasOwnProperty('ERROR')) {
          //   console.warn(result);
          //   //tell User that something went wrong
          //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
          //   return;
          // } else {
            // no correct naming
            if (result.DATA.RECORD.PARAMETERS) {
              result.DATA.RECORD.PARAMETER = result.DATA.RECORD.PARAMETERS;
            }
            let treenodeList: treeNode[] = [];
            if (result.DATA.RECORD.PARAMETER.TABLE.length < 1) {
              let o = this.treeFunctionsService.getDummyDialogTreeNode("no_parameter_found_for_this_entity")
              treenodeList.push(o);
            }

            for (let o of result.DATA.RECORD.PARAMETER.TABLE) {
              // get parentId
              // console.log(o)
              let transformed = this._chooserNodeTransformer(o, node.namePath);

              transformed.iconType = "parameter";
              transformed.type = "PARAMETER";

              // set parentId
              transformed.parentNode = node;
              if (transformed.id != node.id) {
                treenodeList.push(transformed);
              }
            }
            treenodeList.sort(this.treeFunctionsService.compare);
            return treenodeList;
          // }
        }, (rejection: any) => {
          // do nothing
          return
        });
        break;
      case "SCOPE_RESOURCE":
        command = 'show scope ' + this.toolboxService.namePathQuote(node.namePath) + " with expand = ALL";
        return this.commandApiService.execute(command).then((result: any) => {
          // if (result.hasOwnProperty('ERROR')) {
          //   console.warn(result);
          //   //tell User that something went wrong
          //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
          //   return;
          // } else {
            // no correct naming
            let treenodeList: treeNode[] = [];

            // This is done by filters
            // if (result.DATA.RECORD.RESOURCES.TABLE.length < 1) {
            //   let o = this.treeFunctionsService.getDummyDialogTreeNode("no_resource_found_for_this_entity");
            //   treenodeList.push(o);
            // }

            for (let o of result.DATA.RECORD.RESOURCES.TABLE) {
              if (o.USAGE == "CATEGORY" || o.SCOPE != node.namePath) {
                continue;
              }
              // get parentId
              let transformed = this._chooserNodeTransformer(o, node.namePath);

              transformed.iconType = o.USAGE.toLowerCase();
              transformed.type = o.USAGE;

              // set parentId
              transformed.parentNode = node;
              if (transformed.id != node.id) {
                treenodeList.push(transformed);
              }
            }
            treenodeList.sort(this.treeFunctionsService.compare);
            return treenodeList;
          // }
        }, (rejection: any) => {
          // do nothing
          return;
        });
        break;
      case "RESOURCE_PARAMETER":
        this.isLoading = true;
        // console.log("load children resource params")
        // console.log(node);
        command = 'SHOW NAMED RESOURCE ' + this.toolboxService.namePathQuote(node.namePath);
        return this.commandApiService.execute(command).then((result: any) => {
          console.warn(result);
          // if (result.hasOwnProperty('ERROR')) {
          //   console.warn(result);
          //   //tell User that something went wrong
          //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
          //   return;
          // } else {
            let treenodeList: treeNode[] = [];
            if (result.DATA.RECORD.PARAMETERS.TABLE.length < 1) {
              let o = this.treeFunctionsService.getDummyDialogTreeNode("no_parameter_found_for_this_entity");
              treenodeList.push(o);
            }

            for (let o of result.DATA.RECORD.PARAMETERS.TABLE) {
              // get parentId
              // console.log(o)
              let transformed = this._chooserNodeTransformer(o, node.namePath);

              transformed.iconType = "parameter";
              transformed.type = "PARAMETER";

              // set parentId
              transformed.parentNode = node;
              if (transformed.id != node.id) {
                treenodeList.push(transformed);
              }
            }
            treenodeList.sort(this.treeFunctionsService.compare);
            return treenodeList;
          // }
        }, (rejection: any) => {
          // do nothing
          return;
        });
        break;
      case "rerun":
        return Promise.resolve(undefined);
        break;

      default:
        this.isLoading = false;
        console.warn("load properties failed")
        return Promise.resolve(undefined);
        break;
    }
  }

  crudFilter() {
    if (this.editorFormfield.FILTER_METHOD && this.tabInfo) {
      let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE)
      this.dataSource.data = crud.filterMethod(this.editorFormfield.FILTER_METHOD, this.editorFormfield, this.dataSource.data, this.tabInfo, this.parentBicsuiteObject)
    }
  }

  filterTree(value: string) {
    this.filter = true;
    this.expandProgress = 0;

    if (value == '') {
      this.reloadTree();
      return;
    }

    this.isLoading = true;
    this.pinnedParrentList = [];
    let command = this.dialogData.command;

    command = command + ' WITH EXPAND = ALL, NAME LIKE \'(?i).*' + value + '[^.]*$\'';
    // [^;]+(?=;[^;]*$)

    let pinparentCollapseList: treeNode[] = [];
    let datasource: treeNode[] = this.commandApiService.executeDelegateWithRegEx(command, (result: any) => {


      if (result.hasOwnProperty('ERROR')) {
        throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
        return;
        // }
        // console.log(result);
      } else {
        if (result.DATA.TABLE.length > 1200) {
          this.expandProgress = 100;
          this.isLoading = false;
          this.eventEmitterService.pushSnackBarMessage(['too_many_search_results'], SnackbarType.WARNING)
          return
        }


        this.expandProgressSlice = 100 / (0.62 * result.DATA.TABLE.length);

        let treeNodes: treeNode[] = [];
        // console.log(result)

        //first element is the parent
        treeNodes.push(this.treeFunctionsService._dialogTransformer(result.DATA.TABLE[0], this.dialogData));
        treeNodes[0].children = [];

        for (let o of result.DATA.TABLE) {
          if (o.hasOwnProperty('IDPATH')) {
            let idPathArray = o.IDPATH.split('.');

            let parentId = idPathArray[idPathArray.length - 2];
            if (parentId) {  // if there is a Parent

              let parentNode = this.searchTreeWithId(treeNodes[0], parentId) // finde node parent;

              if (parentNode) {  // init children if there is no
                let transformed = this.treeFunctionsService._dialogTransformer(o, this.dialogData);
                transformed.parentNode = parentNode;
                this.treeFunctionsService.insertNode(parentNode, transformed);
              } else {
                // console.log("parent not found for: " + parent)
              }
            }
          } else {
            console.warn("mandatory propertys missing in TreeItem")
            console.warn(o)
          }
        }

        this.dataSource.data = treeNodes;
        // console.log(this.dataSource.data)

        this.expandAll(this.dataSource.data[0]);
        // collapse all not in expansionList
        for (let node of pinparentCollapseList) {
          this.treeControl.collapse(node);
          this.collapseNode(node);
        }

        this.isLoading = false;
      }
    });
  }


  searchTreeWithId(element: treeNode, matchingId: number): treeNode | null {
    if (element.id == matchingId) {
      return element;
    } else if (element.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < element.children.length; i++) {
        result = this.searchTreeWithId(element.children[i], matchingId);
      }
      return result;
    }
    return null;
  }
}
