import { AfterViewInit, ChangeDetectorRef, Component, HostListener, Input, OnDestroy, OnInit, AfterContentChecked, ViewChild, ElementRef } from '@angular/core';
import { FormArray, FormBuilder, UntypedFormGroup } from '@angular/forms';
import { Subscription } from 'rxjs';
import { ValidationHelper } from 'src/app/classes/formgenerator/validation-helper';
import { BicsuiteResponse } from 'src/app/data-structures/bicsuite-response';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { Crud } from 'src/app/interfaces/crud';
import { OutputType, SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CrudOption, CRUDService } from 'src/app/services/crud.service';
import { ErrorHandlerService } from 'src/app/services/error-handler.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { threadId } from 'worker_threads';
import { FormGeneratorService } from '../../services/form-generator.service';
import { MainInformationService } from '../../services/main-information.service';

@Component({
  selector: 'app-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class TabComponent implements OnInit, OnDestroy, AfterContentChecked {

  // tab is defenitely defined!
  @Input() tab!: Tab;
  @Input() isActive: boolean = false;

  @HostListener('window:scroll', ['$event']) // for window scroll events
  scrollTimeOut: any;
  onScroll(event: any) {
    if (this.hasloaded) {
      clearTimeout(this.scrollTimeOut);
      this.scrollTimeOut = setTimeout(() => {
        this.eventEmitterService.saveTabEmitter.next();
      }, 1000);
      // console.log(document.getElementById('tab-content-container')?.getBoundingClientRect().height);
      let height = document.getElementById('tab-content-container')?.getBoundingClientRect().height;
      this.tab.TABSTATE.containerMinHeight = height ? height : 0;
      this.tab.TABSTATE.scrollPosition = event.srcElement.scrollTop;
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event: any) {
    // this.evaluateButtonSpacing();
  }

  @ViewChild('scrolling_div') scrolling_div?: ElementRef ;
  @ViewChild('monitoring_list_tab_wrapper') monitoring_list_tab_wrapper?: ElementRef ;

  private screenSize: number = 0;

  // if buttonsizing was not detected, the buttonspacing will be evaluated
  private buttonSizingDetected: boolean = false;

  checkIfScreenSizeHasChanged(): [boolean, number] {
    let screenSize = undefined;
    // console.log(this.scrolling_div)
    if(this.isForm(this.tab.MODE)) {
      screenSize = this.scrolling_div?.nativeElement.offsetWidth
    } else {
      screenSize = this.monitoring_list_tab_wrapper?.nativeElement.offsetWidth
    }
    if(screenSize != undefined) {
      return [(screenSize != this.screenSize || this.buttonSizingDetected == false), screenSize];
    } else {
      return [false, 0];
    }
  }

  setScreenSizeAndEvaluateButton(size: number) {
    this.screenSize = size;
    this.evaluateButtonSpacing();
  }

  evaluateButtonSpacing() {
      if (this.isActive) {
        if (this.data.editorform && (this.tab.MODE == this.tabTypes.EDIT || this.tab.MODE == this.tabTypes.NEW) && this.data.editorform?.BUTTONS?.length) {
          // if tab is editorform
          let spaceButtonNeed = this.data.editorform?.BUTTONS?.length * 120;
          this.setButtonHaveSpace(spaceButtonNeed);
          this.buttonSizingDetected = true;
        } else if ((this.tab.MODE != this.tabTypes.EDIT && this.tab.MODE != this.tabTypes.NEW)) {
          // if tab is not editorform'
          let spaceButtonNeed = 1550;
          this.buttonSizingDetected = true;
          this.setButtonHaveSpace(spaceButtonNeed);
        } else {
          let spaceButtonNeed = 1500;
          this.buttonSizingDetected = false;
          this.setButtonHaveSpace(spaceButtonNeed);
        }
      }
  }

  setButtonHaveSpace(spaceButtonNeed: number) {
    if (this.screenSize < spaceButtonNeed) {
      this.buttonsHaveSpace = false;
    } else {
      this.buttonsHaveSpace = true;
    }
    this.changeDetector.detectChanges();
  }

  buttonsHaveSpace: boolean = true;

  errorCode: string = "";
  errorMessage: string = "";
  errorContainerOpened = false;

  // Boolean object-container. Defined here to keep data even though tab is not active.
  // is emitted empty to form-generator.component
  accordionOpenStates: any = {};

  // due to Promise<..>, data might not be available immediately
  // for child components (e.g. generator components.)
  // Though, after the Promise finished, data will be accessable eventually.
  data: BicsuiteResponse = new BicsuiteResponse();

  tabTypes = TabMode;
  // form is used for the generator and buttons
  form: UntypedFormGroup = new UntypedFormGroup({});

  private validationHelper: ValidationHelper = new ValidationHelper(this.toolboxService);

  private createTabSubscription?: Subscription;
  private updateTabSubscription?: Subscription;
  private setLockTabSubscription?: Subscription;
  private errorSubscription?: Subscription;
  private selectedTabSubscription?: Subscription;
  private reRenderTabEmitter?: Subscription;
  private notifyDataChangeEmitter?: Subscription;
  private rePosGraphSubscirption?: Subscription;
  private waitForScreenScrollElm: Promise<any> | undefined;

  // Needed for lazy loading tabs (see selectedTabSubscription).
  private isInitialized = false;

  // Need to check on value change if the form is the inital state
  private initialFormValues = {};

  dataRechieved: boolean = false;
  initValueHolder: any;

  loadnumber: number = 0;
  loadInterval: any;
  // not more then 12 allowed yet
  totalLoadedIndex: number = 200;
  hasloaded = false;

  constructor(private crudService: CRUDService,
    private generatorService: FormGeneratorService,
    private eventEmitterService: EventEmitterService,
    private changeDetector: ChangeDetectorRef,
    private mainInformationService: MainInformationService,
    private toolboxService: ToolboxService,
    private errorHanlderService: ErrorHandlerService) {

  }
  ngAfterContentChecked(): void {
    if(this.isActive) {
      let screenHasChangedTupel = this.checkIfScreenSizeHasChanged();
      // console.log(screenHasChangedTupel);
      if(screenHasChangedTupel[0]) {
        // console.log(screenHasChangedTupel);
        this.setScreenSizeAndEvaluateButton(screenHasChangedTupel[1]);
      }
    }
  }

  ngOnInit(): void {


    this.tab.IS_INITIALIZED = this.isInitialized;
    this.tab.IS_ACTIVE = this.isActive;
    // this.tab.IS_DIRTY = false;

    this.hasloaded = false;

    if (this.isForm(this.tab.MODE)) {
      let crud = this.crudService.createOrGetCrud(this.tab.TYPE);
      crud.callMethod('onTabLoad', {}, this.tab, this.form, this.getTopLevelDataObject.bind(this));
    }

    this.form.valueChanges.subscribe(result => {
      // IMPORTAND, EXCLUDE VARIABLES WHO SET IN THE CRUD AT THE DEEP EQUAL METHOD
      // console.log(this.data.response)
      // console.log(this.initialFormValues)
      if (this.toolboxService.deepEquals(this.data.response, this.initialFormValues)) {
        this.form.markAsPristine();
        this.tab.IS_DIRTY = false;
      } else {
        this.form.markAsDirty();
        this.tab.IS_DIRTY = true;
      }
      // console.log(this.tab.IS_DIRTY)
    })

    this.updateTabSubscription = this.eventEmitterService.updateTabEmitter.subscribe((result: any) => {
      if ((result.ID === this.tab.ID)) {
        this.hasloaded = false;
        this.isForm(this.tab.MODE) ? this.getBicsuiteData().then(() => {
          this.eventEmitterService.updateEditorformOptionsEmitter.next({ tab: this.tab, rootForm: this.form });
          this.renderFormData();
        }) : '';
      }
    });

    this.setLockTabSubscription = this.eventEmitterService.setTabLockEmitter.subscribe(({ Tab, lock }) => {
      if ((Tab.ID === this.tab.ID)) {
        this.hasloaded = !lock;

        this.changeDetector.detectChanges();
        this.reziseAndScroll();
      }
    });

    this.reRenderTabEmitter = this.eventEmitterService.reRenderTabEmitter.subscribe((result: any) => {
      // console.log("rerender")
      this.renderFormData();
    })

    this.notifyDataChangeEmitter = this.eventEmitterService.notifyDataChangeEmitter.subscribe((message: any) => {
      if (this.isForm(this.tab.MODE)) {
        let crud: Crud = this.crudService.createOrGetCrud(this.tab.TYPE);
        crud.processNotifyDataChange(message, this.tab, this.data.response, this.initialFormValues);

      }
    })

    this.rePosGraphSubscirption = this.eventEmitterService.rePosNoFormEditors.subscribe(() => {
      if (this.isActive) {
        // resize event at the next browser lifecycle
        setTimeout(function () {
          window.dispatchEvent(new Event('resize'));
        }, 100);
      }
    });

    this.selectedTabSubscription = this.eventEmitterService.selectedTabEmitter.subscribe((result: string) => {
      this.tab.IS_INITIALIZED = this.isInitialized;
      this.tab.IS_ACTIVE = this.isActive;
      if (this.tab.ID == result && !this.isInitialized) {
        if (this.tab.MODE == TabMode.EDIT) {
          
          this.initForms();
          this.renderFormData();

        } else if (!this.isForm(this.tab.MODE)) {
          this.tab.PRIVS = this.mainInformationService.getUser().privs;
        } else {

          // this else path is chosen when a new tab is created -> Tabmode = 0 / NEW
          // Get editorform to make it possible to create a new bicsuite object.
          // Set response to an empty record so that the generator is able to pass data into it.
          this.tab.PRIVS = this.mainInformationService.getUser().privs;
          this.data.editorform = this.crudService.getEditorform(this.tab.TYPE);
          this.data.response = this.createDefaultBicsuiteObject(this.tab, this.data.editorform);
          this.dataRechieved = true;
          this.hasloaded = true;
          this.renderFormData();
        }
        this.isInitialized = true;
        this.tab.IS_INITIALIZED = this.isInitialized;
      } else if (this.isActive && (this.tab.MODE == TabMode.EDIT || this.tab.MODE == TabMode.NEW)) {
        // console.log("render only select Tab emittor ")
        // console.warn("selectTabStacktrace")
        this.renderFormData();
        // console.log(this.tab.TABSTATE.scrollPosition)
        // this.scrollTabBody(this.tab.TABSTATE.scrollPosition)

      } else {
        clearInterval(this.loadInterval);
        this.loadnumber = 0;
      }
    })}

  scrollTabBody(pos: number) {
    // console.log("scroll:" + pos)
    setTimeout(() => {
      let htmlOpj = document.getElementById('scrolling_div')
      if (htmlOpj) {
        htmlOpj.scrollTop = pos;
      }
    }, 0);
  }

  addEmptyTablesToBicsuiteObject(bicuisteObject: any, fields: any) {
    for (let field of fields) {
      switch (field.TYPE) {
        case "TABLE":
          if (bicuisteObject.DATA.RECORD[field.NAME] == undefined) {
            bicuisteObject.DATA.RECORD[field.NAME] = { "TABLE": [] };
          }
          break;
        case "ACCORDION":
          this.addEmptyTablesToBicsuiteObject(bicuisteObject, field.FIELDS);
          break;
      }
    }
  }

  createDefaultBicsuiteObject(tabinfo: Tab, editorForm: any): any {
    let obj = this.crudService.default(this.tab);
    this.addEmptyTablesToBicsuiteObject(obj, editorForm.FIELDS);
    return obj;
  }

  initForms() {
    this.getBicsuiteData();
  }

  ngOnDestroy(): void {
    this.createTabSubscription?.unsubscribe();
    this.updateTabSubscription?.unsubscribe();
    this.errorSubscription?.unsubscribe();
    this.selectedTabSubscription?.unsubscribe();
    this.reRenderTabEmitter?.unsubscribe();
    this.notifyDataChangeEmitter?.unsubscribe();
    this.setLockTabSubscription?.unsubscribe();
    this.rePosGraphSubscirption?.unsubscribe();
    this.waitForScreenScrollElm = undefined;
    clearInterval(this.loadInterval);

    // remove tab data, so that autorefresh functionality knows when tab was destroyed
    this.tab.DATA = {};
  }

  closeErrorContainer() {
    this.errorContainerOpened = false;
  }

  isForm(mode: TabMode): boolean {
    if (this.tab.MODE == TabMode.EDIT) {
      return true;
    } else if (this.tab.MODE == TabMode.MONITOR_MASTER || this.tab.MODE == TabMode.MONITOR_JOBS || this.tab.MODE == TabMode.MONITOR_DETAIL || this.tab.MODE == TabMode.SHELL || this.tab.MODE == TabMode.HIERARCHY || this.tab.MODE == TabMode.CALENDAR) {
      return false;
    }
    return true;
  }

  public getTopLevelDataObject(mode: string): BicsuiteResponse | UntypedFormGroup {
    if (mode == 'data') {
      return this.data;
    } else if (mode == 'form') {
      return this.form;
    } else {
      return this.data;
    }

  }

  getBicsuiteData(): Promise<Object> {
    this.errorContainerOpened = false;
    return this.crudService.execute(this.tab.TYPE, CrudOption.READ, {}, this.tab)
      .then((result: BicsuiteResponse) => {
        
        this.data = result;
        // this.data.editorform =
        this.tab.PRIVS = this.data.response.DATA.RECORD.PRIVS;
        // Set correct ID
        if (this.data.response.DATA.RECORD.ID != undefined && (this.tab.ID == undefined || this.tab.ID == "")) {
          this.tab.ID = this.data.response.DATA.RECORD.ID;
        }

        // It is important that dataRechieved is set to true before changedetection (otherwise the form-generator won't exist early enough for some object initilaization)
        this.dataRechieved = true;
      
        // Tell angular to make a change detection to update the form values in order to
        // Run change detection to update the data object references (editorform and bicsuiteObject) on each child component.
        this.changeDetector.detectChanges();

        // Inform every listener that a newer Object exists
        this.eventEmitterService.bicsuiteRefreshEmitter.next(this.tab);
        //initialize new initial object for form validation
        // this.validationHelper.resetInitialValues(this.data.response.DATA.RECORD);


        // Updating form entries
        for (const [key, control] of Object.entries(this.form.controls)) {
          let value = this.data.response.DATA.RECORD[key];
          // console.log(value)
          // Non Primitve values can be arrays (tables) taht are handled by the table.component.
          if (this.toolboxService.isPrimitive(control.value)) {
            control.setValue(value, { onlySelf: true, emitEvent: true });
          }
        }
        
     
        // this.form.
        this.form.markAsPristine();
        // console.log(result)
        // console.log(this.toolboxService.deepCopy(result))
        this.toolboxService.deepNulltoEmptyString(result.response);
        this.initialFormValues = this.toolboxService.deepCopy(result.response);

        // check if form is in initial state
        this.form.updateValueAndValidity({ onlySelf: true, emitEvent: true });

        this.hasloaded = true;
        return result;
      }).catch((err: any) => {
        console.warn(err);
        // this.eventEmitterService.pushSnackBarMessage(['connection_problem'], SnackbarType.ERROR, OutputType.NONE_SEPARATED);
        this.changeDetector.detectChanges();
        // this.errorMessage = err;
        // this.errorContainerOpened = true;
        this.hasloaded = true;

        // close tab if read execution was failure because
        this.eventEmitterService.removeTabEmitter.next(this.tab)
        return err;
      });
  }



  reziseAndScroll() {
    //  wait for container to be rendered
    this.waitForScreenScrollElm = this.toolboxService.waitForElm('tab-content-container').then(() => {
      let renderbody = document.getElementById('tab-content-container');
      if (renderbody) {
        // console.log(this.tab.TABSTATE.containerMinHeight + 'px')
        renderbody.style.minHeight = this.tab.TABSTATE.containerMinHeight + 'px';
        // console.log(this.tab.TABSTATE.containerMinHeight)
        this.scrollTabBody(this.tab.TABSTATE.scrollPosition)
      }
    });
  }

  resetMinHeight() {

    let renderbody = document.getElementById('tab-content-container');
    if (renderbody) {
      renderbody.style.minHeight = 0 + 'px';
    }
    // console.log("renderformdata end")
  }

  renderFormData() {
    // console.log(this.tab.TABSTATE.accordeonOpenStates)
    this.accordionOpenStates = this.tab.TABSTATE.accordeonOpenStates;
    this.reziseAndScroll();
    clearInterval(this.loadInterval);

    this.loadnumber = 1; // first element is loaded instand
    const ITEMS_RENDERED_AT_ONCE = 2;
    const INTERVAL_IN_MS = 50;
    let init: boolean = false;
    if (this.loadnumber > -1) {
      this.loadInterval = setInterval(() => {
        // console.log(this.tab.MODE);
        if (this.loadnumber >= this.totalLoadedIndex) {
          clearInterval(this.loadInterval);
        }
        // console.log(this.data.editorform.FIELDS?.length)
        if (this.loadnumber >= this.data.editorform.FIELDS?.length && !init) {
          init = true;
          this.resetMinHeight();
        }
        // console.log("renderformdata end")
        if (!this.isActive) {
          clearInterval(this.loadInterval);
        }
        //  console.log(this.loadnumber);
        // console.log(this.totalLoadedIndex);
        this.loadnumber += ITEMS_RENDERED_AT_ONCE;
      }, INTERVAL_IN_MS);
    }
  }

  setHasLoaded(hasLoaded: boolean) {
    console.log(hasLoaded);
    this.hasloaded = hasLoaded;
    this.changeDetector.detectChanges();
  }
}
