import { contextMenuObject } from "./context-interface";

export class NavigationTreeContextMenuDescription {

  public contextMenu: contextMenuObject;

  constructor(node: any) {
    this.contextMenu = {
      TYPE: node.bicsuiteObject.TYPE ? node.bicsuiteObject.TYPE : (node.bicsuiteObject.USAGE ? node.bicsuiteObject.USAGE : ''),
      // HAS TO INVOLVE node.bicsuiteObject.PRIVS of the object!
      DATA: node.bicsuiteObject,
      NODE: node,
      FIELDS: [
        {
          TYPE: 'button',
          TRANSLATE: 'open',
          METHOD: 'open',
          ICON_LOOKUP: 'select',
          PRIVS: ''
        }, {
          TYPE: 'form_button',
          VALUE: 'NEW'
        }, {
          TYPE: 'seperator',
          PRIVS: ''
        }, {
          TYPE: 'form_button',
          VALUE: 'SUBMITJOB'
        }, {
          TYPE: 'form_button',
          VALUE: 'GRANTS'
        }, {
          TYPE: 'form_button',
          VALUE: 'HIERARCHY'
        }, {
          TYPE: 'form_button',
          VALUE: 'SCHEDULES'
        }, {
          TYPE: 'form_button',
          VALUE: 'SUSPEND_SCOPE'
        }, {
          TYPE: 'form_button',
          VALUE: 'RESUME_SCOPE'
        }, {
          TYPE: 'form_button',
          VALUE: 'SHUTDOWN_SCOPE'
        }, {
          TYPE: 'form_button',
          VALUE: 'DEREGISTER_SERVER'
        }, {
          TYPE: 'form_button',
          VALUE: 'SUSPEND'
        }, {
          TYPE: 'seperator',
          PRIVS: ''
        }, {
          TYPE: 'button',
          TRANSLATE: 'copy',
          METHOD: 'copyFromTree',
          ICON_LOOKUP: 'copy',
          CONDITION: 'conditionHasParent',
          PRIVS: ''
        }, {
          TYPE: 'button',
          TRANSLATE: 'cut',
          METHOD: 'cut',
          ICON_LOOKUP: 'cut',
          CONDITION: 'conditionHasParent',
          PRIVS: ''
        },
        {
          TYPE: 'button',
          TRANSLATE: 'paste',
          METHOD: 'paste',
          ICON_LOOKUP: 'paste',
          CONDITION: 'conditionIsFolderCategoryOrScope',
          CRUD_CONDITION: 'conditionClipBoardOfTypeNotEmpty',
          PRIVS: ''
        },
        {
          TYPE: 'seperator',
          PRIVS: ''
        }, {
          TYPE: 'form_button',
          VALUE: 'DROP'
        }
      ]
    }
  }

}
