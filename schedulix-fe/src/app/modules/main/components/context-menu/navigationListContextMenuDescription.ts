import { contextMenuObject } from "./context-interface";

export class NavigationListContextMenuDescription {

  public contextMenu: contextMenuObject;

  constructor(node: any) {
    this.contextMenu = {
      TYPE: node.bicsuiteObject.TYPE ? node.bicsuiteObject.TYPE : (node.bicsuiteObject.USAGE ? node.bicsuiteObject.USAGE : ''),
      shortType: node.bicsuiteObject.shortType,
      // HAS TO INVOLVE node.bicsuiteObject.PRIVS of the object!
      DATA: node.bicsuiteObject,
      FIELDS: [
        {
          TYPE: 'button',
          TRANSLATE: 'open',
          METHOD: 'open',
          ICON_LOOKUP: 'select',
          PRIVS: ''
        }, {
          TYPE: 'form_button',
          VALUE: 'NEW'
        }, {
          TYPE: 'form_button',
          VALUE: 'GRANTS'
        }, {
          TYPE: 'seperator',
          PRIVS: ''
        }, {
          TYPE: 'button',
          TRANSLATE: 'copy',
          METHOD: 'copyFromList',
          ICON_LOOKUP: 'copy',
          PRIVS: ''
        }, {
          TYPE: 'seperator',
          PRIVS: ''
        }, {
          TYPE: 'form_button',
          VALUE: 'DROP'
        }
      ]
    }
  }

}
