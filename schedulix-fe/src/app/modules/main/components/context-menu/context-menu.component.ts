import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import { contextMenuObject } from './context-interface';

import { PrivilegesService } from 'src/app/modules/main/services/privileges.service';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { CrudOption, CRUDService } from 'src/app/services/crud.service';
import { FormGeneratorService } from '../../services/form-generator.service';
import { Crud } from 'src/app/interfaces/crud';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { ClipboardService, SdmsClipboardMode, SdmsClipboardType } from '../../services/clipboard.service';
@Component({
  selector: 'app-context-menu',
  templateUrl: './context-menu.component.html',
  styleUrls: ['./context-menu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ContextMenuComponent implements OnInit {

  constructor(private priviledgeService: PrivilegesService, private crudService: CRUDService, private formGeneratorService: FormGeneratorService, private eventEmitterService: EventEmitterService,
    private clipboardService: ClipboardService) { }

  @Input() contextData: contextMenuObject | undefined;

  editorFormObject: any;
  loading: boolean = false;
  crud: Crud | undefined;
  tab: Tab | undefined;
  readBicsuiteObject: any;

  ngOnInit(): void {
    // console.log(this.contextData)

    this.onDataChange();
  }

  setType(bicsuiteObject: any) {
    if (bicsuiteObject.USAGE) {
      bicsuiteObject.TYPE = bicsuiteObject.USAGE;
    }
  }

  onDataChange() {
    // reset old editorformobjects Fields
    let fields: any[] = []
    this.loading = true;
    if (this.contextData) {
      this.crud = this.crudService.createOrGetCrud(this.contextData?.TYPE);
      let editorform = this.crud.editorform();
      let bicsuiteObject = this.contextData.DATA
      this.setType(bicsuiteObject);
      // create Tab as it would be when created/ no real tabinfo, but validate needs tabinfo
      let node = this.contextData.NODE;
      if (node) {
        this.tab = new Tab(node.name, node.id.toString(), node.type, node.iconType, TabMode.EDIT);
        this.tab.PATH = node.parentNode != undefined ? node.parentNode.namePath : "";
      }
      else {
        this.tab = new Tab(this.contextData.DATA.NAME, this.contextData.DATA.ID, this.contextData.DATA.TYPE, this.contextData.DATA.shortType, TabMode.EDIT);
      }
      this.crudService.read(this.crud, bicsuiteObject, this.tab).then((response: any) => {
        // TODO ERROR HANDLING

        let bicsuiteObject = response.response.DATA.RECORD
        this.readBicsuiteObject = bicsuiteObject;

        // console.log(bicsuiteObject)
        if (this.contextData) {
          for (let field of this.contextData!.FIELDS) {
            // console.log(field)
            if (field.TYPE == 'form_button') {
              // console.log(editorform.BUTTONS)
              for (let buttonfield of editorform.BUTTONS) {
                if (buttonfield.VALUE == field.VALUE) {
                  buttonfield.TYPE = 'form_button';

                  // check privs, edition and conditions if visible or disabled
                  // console.log(buttonfield)
                  let options = this.formGeneratorService.createEditorformOptions(buttonfield, bicsuiteObject, this.tab!!);
                  this.crudService.manipulateEditorformOptions(this.tab!.TYPE, options, buttonfield, this.tab!, bicsuiteObject);
                  buttonfield.method = buttonfield.METHOD ? buttonfield.METHOD : buttonfield.VALUE.toLowerCase();
                  buttonfield.render = options.render;
                  fields.push(buttonfield);

                }
              }
            }
            if (field.TYPE == 'button') {
              field.VISIBLE = true;
              if(field.CONDITION) {
                  this.callNoCrudMethods(field.CONDITION, field);
              }
              if(field.VISIBLE && field.CRUD_CONDITION) {
                this.crudCondition(field.CRUD_CONDITION, field);
            }
              fields.push(
                field
              )
            }
            if (field.TYPE == 'title') {
              fields.push(
                field
              )
            }
            if (field.TYPE == 'seperator') {
              fields.push(
                field
              )
            }
          }
        }

        this.editorFormObject = { FIELDS: fields };
        // console.log(this.editorFormObject)
        this.loading = false;
      })
    }
  }

  callMethod(method: string) {
    // console.log(this.readBicsuiteObject)
    this.crud!.callMethod(method, this.readBicsuiteObject, this.tab!)
  }

  // appent setValue whch will be written by the method inPlace
  callNoCrudMethods(method: string, setValue?: any) {
    if (method == 'open') {
      this.open()
    }
    if (method == 'copyFromTree') {
      this.copyFromTree()
    }
    if (method == 'copyFromList') {
      this.copyFromList()
    }
    if (method == 'cut') {
      this.cut()
    }
    if (method == 'paste') {
      this.paste()
    }
    if (method == 'conditionHasParent') {
      this.conditionHasParent(setValue);
    }

    if(method == 'conditionIsFolderCategoryOrScope') {
      this.conditionIsFolderCategoryOrScope(setValue);
    }
  }

  conditionIsFolderCategoryOrScope(field: any) {
    if(this.tab) {
      if(['FOLDER', 'CATEGORY', 'SCOPE'].includes(this.tab?.TYPE)) {
        field.VISIBLE = true;
      } else {
        field.VISIBLE = false;
      }
    } else {
      field.VISIBLE = false;
    }
  }

  crudCondition(conditionMethod: string, field: any) {
    if(this.tab) {
      field.ENABLED = this.crud?.checkFieldCondition(conditionMethod, field, this.readBicsuiteObject, this.tab);
      // console.log(field.ENABLED)
    }
  }

  conditionHasParent(field: any) {
    if(this.contextData?.NODE?.parentNode?.bicsuiteObject) {
      field.ENABLED = true;
    } else {
      field.ENABLED = false;
    }
    // console.log(field)
  }

  open() {
    if (this.contextData) {
      let bicsuiteObject = this.contextData.DATA
      let nameString = bicsuiteObject.NAME.split('.');
      let name = nameString.pop()
      let path = nameString.join('.')
      let shortType = this.contextData.shortType ? this.contextData.shortType : this.contextData.TYPE.toLowerCase();
      let tabToOpen = new Tab(name, bicsuiteObject.ID, this.contextData.TYPE, shortType, TabMode.EDIT);
      tabToOpen.PATH = path;
      this.eventEmitterService.createTab(tabToOpen, true);
    }
  }
  copyFromTree() {
    let notifyContents = false;
    if (this.contextData && this.tab) {
      if (this.contextData.NODE?.parentNode?.bicsuiteObject && this.crud) {
        this.setType(this.contextData.NODE?.parentNode?.bicsuiteObject);
        let node = this.contextData.NODE;
        let name = node?.parentNode?.bicsuiteObject.NAME;
        let p = node?.parentNode?.bicsuiteObject.NAME.split('.');
        let path = "";
        if (p.length > 1) {
          name = p.pop();
          path = p.join('.');
        }
        let parentTab: Tab = new Tab(name, this.contextData.NODE?.parentNode?.bicsuiteObject.ID, this.contextData.NODE?.parentNode?.bicsuiteObject.TYPE, this.contextData.NODE?.parentNode?.bicsuiteObject.TYPE.toLowerCase(), TabMode.EDIT)
        parentTab.PATH = path;
        if (this.contextData.DATA.fe_flagged_to_cut) {
          this.contextData.DATA.fe_flagged_to_cut = false;
          notifyContents = true;
        }
        this.contextData.DATA.fe_flagged_to_cut = false;
        let parentCrud: Crud = this.crudService.createOrGetCrud(parentTab.TYPE)
        this.crudService.read(parentCrud, this.contextData.NODE?.parentNode?.bicsuiteObject, parentTab).then((response: any) => {
          let readParentBicsuiteObject = response.response.DATA.RECORD;
          this.setType(readParentBicsuiteObject);
          this.clipboardService.addItems(this.tab!.TYPE as unknown as SdmsClipboardType, [this.contextData!.DATA], parentTab, readParentBicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.FROM_ITEM_TYPE);
          // make affected open tabs reload their content
          if (notifyContents) {
            let messageType = this.tab!.TYPE;
            if (["BATCH", "JOB", "MILESTONE"].includes(messageType)) {
              messageType = "FOLDER";
            }
            if (["STATIC", "SYSTEM", "SYNCHRONIZING", "POOL"].includes(messageType)) {
              messageType = "NR";
            }
            if ("SERVER" == messageType) {
              messageType = "SCOPE";
            }
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : messageType, OP : "CREATE", NAME : this.tab!.PATH + ".DUMMY"});
          }
        });
      }
    }
  }
  copyFromList() {
    if (this.contextData && this.tab) {
      if (this.crud) {
        // console.log(this.contextData)
        // this.setType(this.contextData.NODE?.parentNode?.bicsuiteObject);
        //   this.setType(readParentBicsuiteObject);
          // console.log(this.tab.TYPE);
        //   console.log(readParentBicsuiteObject)
          this.clipboardService.addItems(this.tab!.TYPE as unknown as SdmsClipboardType, [this.contextData!.DATA], this.tab, this.readBicsuiteObject, SdmsClipboardMode.COPY, SdmsClipboardType.FROM_ITEM_TYPE);
      }
    }
  }
  cut() {
    if (this.contextData && this.tab) {
      let notifyContents = false;
      if (this.contextData.NODE?.parentNode?.bicsuiteObject) {
        this.setType(this.contextData.NODE?.parentNode?.bicsuiteObject);

        let node = this.contextData.NODE;
        let name = node?.parentNode?.bicsuiteObject.NAME;
        let p = node?.parentNode?.bicsuiteObject.NAME.split('.');
        let path = "";
        if (p.length > 1) {
          name = p.pop();
          path = p.join('.');
        }
        let parentTab: Tab = new Tab(name, this.contextData.NODE?.parentNode?.bicsuiteObject.ID, this.contextData.NODE?.parentNode?.bicsuiteObject.TYPE, this.contextData.NODE?.parentNode?.bicsuiteObject.TYPE.toLowerCase(), TabMode.EDIT)
        parentTab.PATH = path;
        if (!this.contextData.DATA.fe_flagged_to_cut) {
          this.contextData.DATA.fe_flagged_to_cut = true;
          notifyContents = true;
        }
        let parentCrud: Crud = this.crudService.createOrGetCrud(parentTab.TYPE)
        this.crudService.read(parentCrud, this.contextData.NODE?.parentNode?.bicsuiteObject, parentTab).then((response: any) => {
          let readParentBicsuiteObject = response.response.DATA.RECORD;
          this.setType(readParentBicsuiteObject);
          // console.log("read parent")
          // console.log(readParentBicsuiteObject)
          this.clipboardService.addItems(this.tab!.TYPE as unknown as SdmsClipboardType, [this.contextData!.DATA], parentTab, readParentBicsuiteObject, SdmsClipboardMode.CUT, SdmsClipboardType.FROM_ITEM_TYPE);
          // make affected open tabs reload their content
          if (notifyContents) {
            let messageType = this.tab!.TYPE;
            if (["BATCH", "JOB", "MILESTONE"].includes(messageType)) {
              messageType = "FOLDER";
            }
            if (["STATIC", "SYSTEM", "SYNCHRONIZING", "POOL"].includes(messageType)) {
              messageType = "NR";
            }
            if ("SERVER" == messageType) {
              messageType = "SCOPE";
            }
            this.eventEmitterService.notifyDataChangeEmitter.next({TYPE : messageType, OP : "CREATE", NAME : this.tab!.PATH + ".DUMMY"});
          }
        });
      }
    }
  }

  paste() {
    if (this.contextData && this.tab) {
      this.crud = this.crudService.createOrGetCrud(this.contextData?.TYPE);
      // console.log(this.readBicsuiteObject)
      this.tab.FROM_CONTEXT_MENU = true;
      this.crud.callMethod('pasteContent', this.readBicsuiteObject, this.tab, {}, this.clipboardService.getItems([SdmsClipboardType.FOLDER, SdmsClipboardType.BATCH, SdmsClipboardType.JOB])).then(() => {
        this.eventEmitterService.refreshNavigationEmittor.next();
      }, this.crud.emptyRejection);
      // console.log(this.contextData);
      // console.log(this.clipboardService.checkItemTypesPresent([SdmsClipboardType.FOLDER, SdmsClipboardType.JOB, SdmsClipboardType.MILESTONE, SdmsClipboardType.BATCH], this.contextData.DATA.ID));
    }
  }
}
