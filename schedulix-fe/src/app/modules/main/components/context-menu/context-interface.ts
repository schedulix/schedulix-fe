export interface contextMenuObject {
    // which type if context menu is open, to get correct methods
    TYPE: string,

    // if icontype != type use shorttypes to select correct icon
    shortType?: string

    // methodData
    DATA?: any,

    //the node in the list or treeview to get access to parent
    NODE?: any,
    FIELDS: {
        // type = button(clickhandler) or title (just text) or seperator(just a line to seperate items groups)
        TYPE: string,

        // if type form_button the name is the NAME of the editorform buttonfield to get the editorforminfor from
        VALUE?: string,

        // below atributes for special buttons who are not in the editorform of the object

        // if type button or title the label will be displayed
        TRANSLATE?: string,
        // method which will be taken from the methodholder, TYPE + METHOD = ACTUAL_METHOD
        METHOD?: string,
        // condition which will be taken from the methodholder, TYPE + condition = condition
        CONDITION?: string,
        // condition which will be taken from the main crud of the context menu
        CRUD_CONDITION?: string,
        ICON_LOOKUP?: string,
        PRIVS?: string,
        // set by the context menu component dynamically when privs validated
        ENABLED?: boolean
        // set by the context menu component dynamically when privs validated
        VISIBLE?: boolean

        AVAILABLE?: string[]
    }[]
}
