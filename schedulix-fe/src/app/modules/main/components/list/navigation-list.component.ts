import { Component, Input, OnDestroy, OnInit, HostListener, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { map, startWith } from 'rxjs/operators';
import { ListItem } from 'src/app/data-structures/listItem';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { sdmsException } from 'src/app/services/error-handler.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { PrivilegesService } from '../../services/privileges.service';
import { contextMenuObject } from '../context-menu/context-interface';
import { NavigationListContextMenuDescription } from '../context-menu/navigationListContextMenuDescription';
import { NavigationTreeContextMenuDescription } from '../context-menu/navigationTreeContextMenuDescription';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { ToolboxService } from 'src/app/services/toolbox.service';

@Component({
  selector: 'app-navigation-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NavigationListComponent implements OnInit, OnDestroy {

  @Input() dialogData: any = {};


  searchControl = new UntypedFormControl();
  TYPE: string = '';
  shortType: string = '';
  filteredOptions: Observable<any[]> | undefined;
  listItems: any[] = [];
  lastReload: string = '';
  propertyToShow: string = 'NAME';
  rxjsSelectedTabEmitter: Subscription = new Subscription();
  rxjsRefreshListEmitter: Subscription = new Subscription();

  isNavigationList: boolean = true;

  canCreate: boolean = false;

  // get selected ID in tab view to check if sth should be highlighted or not...
  selectedTabId: string = this.eventEmitterService.selectedTabEmitter.getValue();

  constructor(private route: ActivatedRoute,
    private router: Router,
    private commandApiService: CommandApiService,
    private eventEmitterService: EventEmitterService,
    private privilegesService: PrivilegesService,
    private localStorageHelper: LocalStorageHelper,
    private toolboxService: ToolboxService) { }


  ngOnInit(): void {
    let command = this.route.snapshot.data.command;
    let filter = this.route.snapshot.data.filter;
    this.TYPE = this.route.snapshot.data.TYPE;
    this.shortType = this.route.snapshot.data.shortType;

    this.canCreate = this.privilegesService.hasManagePriv(this.TYPE) || this.privilegesService.isAdmin();
    // console.log()

    //set search value to '' at the beginning
    this.searchControl.setValue('')
    this.loadList(command, filter);
    this.rxjsSelectedTabEmitter = this.eventEmitterService.selectedTabEmitter.subscribe((id: string) => {
      // find and focus selected item in the tab context
      this.selectedTabId = id;
    });
    this.rxjsRefreshListEmitter = this.eventEmitterService.refreshNavigationEmittor.subscribe(event => {
      this.reloadList();
    })

    // call reposition of tabs
    this.eventEmitterService.rePosNoFormEditors.next("navigationTreeCall");
  }

  ngOnDestroy(): void {
    this.rxjsSelectedTabEmitter.unsubscribe();
    this.rxjsRefreshListEmitter.unsubscribe();
  }


  isOpenedItemInTabView(id: string): boolean {

    //todo is called quite often... optimize later
    if (id == this.selectedTabId) {
      return true;
    } else {
      return false;
    }
  }

  onItemClick(item: ListItem) {
    let tabItem = new Tab(item.NAME, item.ID /* item.ID */, this.TYPE, this.shortType, TabMode.EDIT);
    // tabItem.ID = item.ID;
    // tabItem.NAME = item.NAME;
    // tabItem.TYPE = this.TYPE;
    // tabItem.shortType = this.shortType;
    // tabItem.MODE = TabMode.EDIT;

    this.eventEmitterService.createTab(tabItem, true);
  }

  createItem() {


    let tab = new Tab('*untitled', new Date().getTime().toString(), this.TYPE, this.shortType, TabMode.NEW);
    // let tabItem = new Tab();
    // tabItem.MODE = TabMode.NEW;
    // tabItem.shortType = this.shortType;
    // tabItem.TYPE = this.TYPE;
    // tabItem.NAME = '*untitled'

    this.eventEmitterService.createTab(tab, true);
  }

  intervalFilter(table: any[]): any[] {
    let result = [];
    for (let row of table) {
      if (row['SE_ID'] == '0' && row['PRIVS'].includes('V')) {
        result.push(row);
      }
    }
    return result;
  }

  niceProfileFilter(table: any[]): any[] {
    for (let item of table) {
      if (item.IS_ACTIVE == "true") {
        item.NAME = item.NAME + " [" + item.ACTIVE_SEQ + "]";
        item.textColor = "TEXT_GREEN";
      }
    }
    // console.log(table);
    return table;
  }

  defaultFilter(table: any[]): any[] {
    for (let item of table) {
      item.TYPE = this.TYPE;
      item.shortType = this.shortType;
    }
    return table;
  }

  filter(table: any[], filter: string) {
    switch (filter) {
      case 'intervalFilter':
        return this.intervalFilter(table);
      case 'niceProfileFilter':
        return this.niceProfileFilter(table);
      default:
        return this.defaultFilter(table);
    }
  }

  loadList(command: string, filter: string) {
    // console.log(filter);
    this.commandApiService.executeDelegate(command, (result: any) => {
      if (result.hasOwnProperty('ERROR')) {
        throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
        return;
      } else {
        let date = new Date()
        this.lastReload = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();
        this.listItems = this.filter(result.DATA.TABLE, filter);
        this.filteredOptions = this.searchControl.valueChanges
          .pipe(
            // reuse search value after loading the list
            startWith(this.searchControl.value),
            map(value => {
              return typeof value === 'string' ? value : value.name
            }),
            map(name => name ? this._filter(name) : this.listItems.slice())
          );

        this.resizeAndScroll();
        return
      }
    });
  }

  reloadList() {
    let command = this.route.snapshot.data.command;
    let filter = this.route.snapshot.data.filter;
    this.loadList(command, filter);
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.listItems.filter((item: any) => item[this.propertyToShow].toLowerCase().includes(filterValue));
  }

  close() {
    // console.log("close")
    this.eventEmitterService.rePosNoFormEditors.next('NavigationListCall');
    this.router.navigate(['/home/'])
  }

  // ---------------------------- CONTEXT MENU, same as in navigation list---------------------

  isDisplayContextMenu: boolean = false;
  rightClickMenuItems: contextMenuObject | undefined;
  rightClickMenuPositionX: number = 0;
  rightClickMenuPositionY: number = 0;

  displayContextMenu(event: any, node: any) {
    // console.log(this.TYPE)

    this.isDisplayContextMenu = true;
    if(!node.bicsuiteObject.TYPE) {
      node.bicsuiteObject.TYPE = this.TYPE;
    }
    let contextMenuObject: contextMenuObject = new NavigationListContextMenuDescription(node).contextMenu
    this.rightClickMenuItems = contextMenuObject;


    var height = window.innerHeight
      || document.documentElement.clientHeight
      || document.body.clientHeight;
    let heightOffSetMargin = 100;
    if (height - (event.clientY + heightOffSetMargin) > 0) {
      this.rightClickMenuPositionX = event.clientX;
      this.rightClickMenuPositionY = event.clientY;
    } else {
      this.rightClickMenuPositionX = event.clientX;
      this.rightClickMenuPositionY = event.clientY - heightOffSetMargin;
    }
  }

  getRightClickMenuStyle() {
    return {
      position: 'fixed',
      left: `${this.rightClickMenuPositionX}px`,
      top: `${this.rightClickMenuPositionY}px`
    }
  }

  handleMenuItemClick(event: any) {
    if (event.data) {
      // console.log(this.rightClickMenuItems);
    }
  }

  @HostListener('document:click')
  documentClick(): void {
    this.isDisplayContextMenu = false;
  }
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler(event: KeyboardEvent) {
    this.isDisplayContextMenu = false;
  }
  @HostListener('window:mousedown', ['$event'])
    handleMouseClick(evt: MouseEvent) {
        // console.log('globalEvent');
        // console.log(evt);
        switch (evt.button) {
            case 0:
                break;
            case 1:
                break;
            case 2:
                this.isDisplayContextMenu = false;
                break;
        }
    }

  scrollTimeOut: any;
  scrollpostion: number = 0;
  containerMinHeight: number | undefined = 0;

  @HostListener('window:scroll', ['$event']) onScroll(event: any) {

    let height = document.getElementById('navigationTree-ident')?.getBoundingClientRect().height;
    this.scrollpostion = event.srcElement.scrollTop;
    this.containerMinHeight = height;

    clearTimeout(this.scrollTimeOut);
    this.scrollTimeOut = setTimeout(() => {
      // this.eventEmitterService.saveTabEmitter.next();
      this.localStorageHelper.setConnectionValue('scrollpostion' + this.TYPE , this.scrollpostion);
      // this.localStorageHelper.setConnectionValue('containerMinHeight' + this.TYPE , this.containerMinHeight);
    }, 500);
}


resizeAndScroll() {
  //  wait for container to be rendered
  this.toolboxService.waitForElm('navigationList-content').then(() => {
    // let renderbody = document.getElementById('navigationTree-ident');
    // if (renderbody) {
      // console.log(this.tab.TABSTATE.containerMinHeight + 'px')
      // renderbody.style.minHeight = this.localStorageHelper.getConnectionValue('containerMinHeight' + this.TYPE) + 'px';
      // console.log(this.tab.TABSTATE.containerMinHeight)
      this.scrollTabBody(this.localStorageHelper.getConnectionValue('scrollpostion' + this.TYPE));
    // }
  });
}

scrollTabBody(pos: number) {
  // console.log("scroll:" + pos)
  // console.log(pos);
  setTimeout(() => {
    let htmlOpj = document.getElementById('navigationList-content')
    if (htmlOpj) {
      htmlOpj.scrollTop = pos;
    }
  }, 100);
}


}
