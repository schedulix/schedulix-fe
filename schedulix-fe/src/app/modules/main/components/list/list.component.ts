import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, Subscription } from 'rxjs';
import { SubjectSubscriber } from 'rxjs/internal/Subject';
import { map, startWith } from 'rxjs/operators';
import { ListItem } from 'src/app/data-structures/listItem';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { CRUDService } from 'src/app/services/crud.service';
import { sdmsException } from 'src/app/services/error-handler.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { BookmarkService } from '../../services/bookmark.service';
import { contextMenuObject } from '../context-menu/context-interface';
import { ChooserDialogData } from '../form-generator/chooser/chooser-dialog/chooser-dialog.component';



@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent implements OnInit, OnDestroy {

  searchControl = new UntypedFormControl();
  filteredOptions: Observable<any[]> | undefined;
  listItems = [];
  lastReload: string = '';

  canCreate: boolean = false;

  isNavigationList: boolean = false;


  // for context menu in the navigation list
  isDisplayContextMenu: boolean = false;
  rightClickMenuItems: contextMenuObject | undefined;
  rightClickMenuPositionX: number = 0;
  rightClickMenuPositionY: number = 0;


  @Input() command: string = '';
  @Input() TYPE: string = '';
  @Input() shortType: string = '';
  @Input() propertyPath: string[] = [];
  @Input() propertyToShow: string = '';
  @Input() propertyShowOption: any;
  @Input() dialogData: any = {};

  // for filtering
  @Input() tabInfo: Tab | undefined;
  @Input() editorFormfield: any;
  @Input() parentBicsuiteObject: any;

  @Output() clickCallback = new EventEmitter();


  constructor(private route: ActivatedRoute,
    private commandApiService: CommandApiService,
    private eventEmitterService: EventEmitterService,
    private router: Router,
    private crudService: CRUDService) { }


  ngOnInit(): void {
    this.searchControl.setValue('')

    if (this.command != '' && this.command) {
      this.loadList(this.command)
    } else {
      this.filter();
    }
  }

  ngOnDestroy(): void {
  }


  isOpenedItemInTabView(id: string): boolean {
    return false
  }

  onItemClick(item: ListItem) {
    let resultobject: any = { bicsuiteObject: item }
    this.clickCallback.emit(resultobject);
  }

  createItem() {
  }


  filter() {
    if (this.editorFormfield.FILTER_METHOD && this.tabInfo) {
      let crud = this.crudService.createOrGetCrud(this.tabInfo.TYPE)
      this.listItems = crud.filterMethod(this.editorFormfield.FILTER_METHOD, this.editorFormfield, this.listItems, this.tabInfo, this.parentBicsuiteObject)
    }
  }

  loadList(command: string) {
    // delegate executions need own error handling...
    this.commandApiService.executeDelegate(command, (result: any) => {
      if (result.hasOwnProperty('ERROR')) {
        // console.warn(result);
        //tell User that something went wrong
        throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
        return;
      } else {
        let date = new Date()
        this.lastReload = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

        let listData: any[] = [];
        let subResult = result;

        for (let property of this.propertyPath)
          subResult = subResult[property];

        if (this.dialogData.nonable) {
          let noneItem: any = {};
          let noneValue = (this.dialogData.noneValue != undefined ? this.dialogData.noneValue : "NONE")
          noneItem.NONE_VALUE = noneValue;
          let noneDisplay = noneValue;
          if (this.dialogData.noneDisplay) {
            noneDisplay = this.dialogData.noneDisplay;
          }
          noneItem[this.propertyToShow] = noneDisplay;
          listData.push(noneItem);
        }

        for (let item of subResult) {
          // ** NOTE Add array support if needed.
          if (this.propertyShowOption.PROPERTY_NAME != '') {
            if (this.propertyShowOption.VALUE == '' || this.propertyShowOption.VALUE == item[this.propertyShowOption.PROPERTY_NAME])
              listData.push(item);
          }
          else {
            listData.push(item);
          }
        }


        this.listItems = listData as any;

        // CRUD filter if FILTER_METHOD is described
        this.filter();

        this.filteredOptions = this.searchControl.valueChanges
          .pipe(
            // reuse search value after loading the list
            startWith(this.searchControl.value),
            map(value => {
              return typeof value === 'string' ? value : value.name
            }),
            map(name => name ? this._filter(name) : this.listItems.slice())
          );
        return
      }
    });
  }

  reloadList() {
  }

  private _filter(name: string): any[] {
    const filterValue = name.toLowerCase();
    return this.listItems.filter((item: any) => item[this.propertyToShow].toLowerCase().includes(filterValue));
  }

  getItem(index: number) {

  }

  close() {
    // console.log("close")
    this.router.navigate(['/home/'])
  }

  displayContextMenu(event: any, node: any) {
  }

  getRightClickMenuStyle() {
    return {
    }
  }

  onScroll(event: any) {}
}
