import { AfterContentChecked, Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormArray, UntypedFormGroup } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Subscription } from 'rxjs';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { CrudOption, CRUDService } from 'src/app/services/crud.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { FormGeneratorService } from '../../services/form-generator.service';
import { MainInformationService } from '../../services/main-information.service';
import { PrivilegesService } from '../../services/privileges.service';
import { FormButtonsService } from '../../services/form-buttons.service';
import { Crud } from 'src/app/interfaces/crud';
import { SelectionModel } from '@angular/cdk/collections';

/* Possible Editorform Flags:
* CONFIRM
* CHANGED
* MODES
* AVAILABLE
*/

// TODO REFACTOR hasGrant, isDisabled and so on
@Component({
  selector: 'app-form-button',
  templateUrl: './form-button.component.html',
  styleUrls: ['./form-button.component.scss']
})
export class FormButtonComponent implements OnInit, AfterContentChecked, OnDestroy {

  // tabInfo! we KNOW that the tab is defined.
  @Input() tabInfo!: Tab;
  @Input() btnInfo: any;
  @Input() form: UntypedFormGroup = new UntypedFormGroup({});
  @Input() bicsuiteObject: any;
  @Input() useCustom: boolean = false;
  @Input() buttonHasSpace: boolean = true;
  @Input() btnColor: string = "secondary";
  @Input() editorform: any;
  @Input() checklistSelection?: SelectionModel<any>;
  @Input() parentHasLoaded: boolean = true;
  @Output() setTabHasLoaded = new EventEmitter<boolean>();
  @Output() clickOccured = new EventEmitter();

  private formSubscription?: Subscription;
  private clickResponse: any;

  private privateHasLoaded: boolean = true;


  isDisabled: boolean = false;
  isAvailable: boolean = true;
  hasGrant: boolean = false;
  crud: Crud | undefined;
  icon: string | undefined;
  // crud: any;
  private editorformConditionSubscription?: Subscription;

  constructor(private cRUDService: CRUDService,
    private eventEmitterService: EventEmitterService,
    private mainInformation: MainInformationService,
    private generatorService: FormGeneratorService,
    private privilegesService: PrivilegesService,
    public dialog: MatDialog,
    private formButtonsService: FormButtonsService) { }

  ngOnInit(): void {

    // this.crud = this.cRUDService.createOrGetCrud(this.tabInfo.TYPE);
    this.editorformConditionSubscription = this.eventEmitterService.updateEditorformOptionsEmitter.subscribe(({ tab, rootForm }) => {
      if (this.tabInfo.ID === tab.ID) {

        if (rootForm == undefined || rootForm == this.form.root) {
          this.hasGrant = this.privilegesService.valide(this.btnInfo.PRIVS, this.bicsuiteObject["PRIVS"], this.btnInfo, this.tabInfo, this.bicsuiteObject);
          this.ngAfterContentChecked();
          // console.log("refresh" + this.btnInfo.VALUE)
        }
      }
    });

    this.hasGrant = this.privilegesService.valide(this.btnInfo.PRIVS, this.bicsuiteObject["PRIVS"], this.btnInfo, this.tabInfo, this.bicsuiteObject);
    this.crud = this.cRUDService.createOrGetCrud(this.tabInfo.TYPE);
    this.icon = this.getIcon();
  }

  ngOnDestroy(): void {
    if (!this.editorformConditionSubscription?.closed) {
      this.editorformConditionSubscription?.unsubscribe();
    }
  }

  ngAfterContentChecked() {

    this.isAvailable = true;
    if (this.btnInfo.VALUE != 'REVERT') {
      this.isDisabled = (!this.form.dirty || this.form.invalid);
    } else {
      // revert does not require the form to be valid
      this.isDisabled = (!this.form.dirty);
    }
    // CHANGED Flag
    this.isDisabled = this.isDisabled && (this.btnInfo.CHANGED !== undefined) && (this.btnInfo.CHANGED === 'Y');
    if (!this.isDisabled && this.btnInfo.CHANGED !== undefined && this.btnInfo.CHANGED === 'N' && this.form.dirty) {
      this.isDisabled = true;
    }

    // BULK DISABLED FLAG
    if (this.btnInfo.IS_BULK_BUTTON == 'true') {
      this.checklistSelection?.selected.length == 0 ? this.isDisabled = true : '';
    }

    // special case
    if (this.btnInfo.VALUE == 'CLONE') {
      if (this.bicsuiteObject.ORIGINAL_NAME == this.bicsuiteObject.NAME) this.isDisabled = true;
    }
    // MODES Flag
    if (this.btnInfo.hasOwnProperty("MODES")) {
      if (this.tabInfo.MODE === TabMode.NEW && !this.btnInfo.MODES.hasOwnProperty("NEW")) {
        this.isDisabled = true;
      }
      else if (this.tabInfo.MODE === TabMode.EDIT && !this.btnInfo.MODES.hasOwnProperty("EDIT")) {
        this.isDisabled = true;
      }
    }

    // get icon
    this.icon = this.getIcon();

    // AVAILABLE Flag
    let editionValidated : boolean = true;
    if (this.btnInfo.hasOwnProperty("AVAILABLE")) {
      editionValidated = this.privilegesService.validateEdition(this.btnInfo.AVAILABLE);
      this.isAvailable = editionValidated;
    }

    // console.log(this.btnInfo.TRANSLATE)
    // console.log(this.privilegesService.validateEdition(this.btnInfo.AVAILABLE))
    // console.log(this.isAvailable)

    // crud conditions
    if (this.crud) {
      if (this.btnInfo.hasOwnProperty("VISIBLE_CONDITION")) {
        // console.log("available?" + this.btnInfo.VALUE)
        editionValidated ? this.isAvailable = this.crud.checkFieldCondition(this.btnInfo["VISIBLE_CONDITION"], this.btnInfo, this.bicsuiteObject, this.tabInfo) : '';
      }
      if (this.btnInfo.hasOwnProperty("ENABLE_CONDITION") && this.isDisabled == false) {
        this.isDisabled = !this.crud.checkFieldCondition(this.btnInfo["ENABLE_CONDITION"], this.btnInfo, this.bicsuiteObject, this.tabInfo);
      }
    }
    // PRIVS
    // console.log(this.hasGrant)
    if (!this.hasGrant) {
      this.isDisabled = !this.hasGrant;
    }
    // READONLY FLAG

    if (this.tabInfo.ISREADONLY == true) {
      if (this.btnInfo.NO_READ_ONLY == true) {
        // do nothing
      } else {
        this.isDisabled = true;
      }
    }

    if (this.isDisabled && this.btnInfo.HIDE_DISABLED) {
      this.isAvailable = false;
    }

    // if (this.bicsuiteObject.crudIsReadOnly == true) {
    //   // READONLY special cases:
    //   if(this.btnInfo.NO_READ_ONLY == true){
    //     // do nothing
    //   } else {
    //     this.isDisabled = true;
    //   }
    // }
  }

  submit() {
    this.callButtonMethod();
  }

  getIcon(): string | undefined {
    // ICON_LOOKUP_FLAG
    let option = this.generatorService.createEditorformOptions(this.btnInfo, this.bicsuiteObject, this.tabInfo)
    // console.log(option.flags.SVG_ICON)
    return option.flags.SVG_ICON;
  }

  loadLock() {
    this.privateHasLoaded = false;
    if (this.btnInfo.LOCK_LOADING) {
      this.setTabHasLoaded.next(false);
    }
  }
  loadUnLock() {
    this.privateHasLoaded = true;
    if (this.btnInfo.LOCK_LOADING) {
      this.setTabHasLoaded.next(true);
    }
  }
  isLoading(): boolean {
    if ((this.btnInfo.LOCK_LOADING && !this.parentHasLoaded) || !this.privateHasLoaded) {
      return true;
    } else {
      return false;
    }
  }
  private callButtonMethod() {
    if (!this.useCustom) {
      let crud: Crud = this.cRUDService.createOrGetCrud(this.tabInfo.TYPE);
      let method: string = this.btnInfo.VALUE.toLowerCase();
      if (this.btnInfo.METHOD != undefined) {
        method = this.btnInfo.METHOD;
      }
      this.loadLock();

      let result: Promise<any> = crud.callMethod(method, this.bicsuiteObject, this.tabInfo).then(
        (res: any) => {
          this.loadUnLock();
          return res;
        },
        (rejection: any) => {
          this.loadUnLock();
          return rejection;
        });

    } else {
      this.clickOccured.emit(this.btnInfo);
    }
  }
}
