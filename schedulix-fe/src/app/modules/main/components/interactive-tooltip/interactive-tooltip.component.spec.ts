import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InteractiveTooltipComponent } from './interactive-tooltip.component';

describe('InteractiveTooltipComponent', () => {
  let component: InteractiveTooltipComponent;
  let fixture: ComponentFixture<InteractiveTooltipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InteractiveTooltipComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(InteractiveTooltipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
