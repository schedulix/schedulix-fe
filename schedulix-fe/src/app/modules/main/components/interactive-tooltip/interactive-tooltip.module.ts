import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InteractiveTooltipComponent } from './interactive-tooltip.component';
import { InteractiveTooltipDirective } from './interactive-tooltip.directive';




@NgModule({
  declarations: [
    InteractiveTooltipComponent,
    InteractiveTooltipDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [InteractiveTooltipDirective]
})
export class InteractiveTooltipModule { }
