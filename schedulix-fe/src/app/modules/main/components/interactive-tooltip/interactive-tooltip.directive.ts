
import { Subscription } from "rxjs";
import { InteractiveTooltipComponent } from "./interactive-tooltip.component";

import { ApplicationRef, ComponentFactoryResolver, ComponentRef, Directive, ElementRef, EmbeddedViewRef, HostListener, Injector, Input } from '@angular/core';

@Directive({ selector: '[interactiveTooltip]' })
export class InteractiveTooltipDirective {

  @Input() interactiveTooltip = '';

  private componentRef: ComponentRef<any> | null = null;

  constructor(
    private elementRef: ElementRef,
    private appRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private injector: Injector) {
  }
  hover: boolean = false;

  destroyMessageSubscription?: Subscription;

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.hover = true;
    if (this.componentRef === null) {
      const componentFactory =
        this.componentFactoryResolver.resolveComponentFactory(
          InteractiveTooltipComponent);
      this.componentRef = componentFactory.create(this.injector);
      this.appRef.attachView(this.componentRef.hostView);
      const domElem =
        (this.componentRef.hostView as EmbeddedViewRef<any>)
          .rootNodes[0] as HTMLElement;
      document.body.appendChild(domElem);
      this.setTooltipComponentProperties();
    }
  }

  private setTooltipComponentProperties() {
    if (this.componentRef !== null) {
      this.componentRef.instance.tooltip = this.interactiveTooltip;
      const { left, right, bottom, top } =
        this.elementRef.nativeElement.getBoundingClientRect();
      this.componentRef.instance.left = (right - left) / 2 + left;
      this.componentRef.instance.top = top - 49; // height of tooltip is 49
    }
  }

  timeout: any;
  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.hover = false;
    this.timeout = setTimeout(() => {
      if(!this.hover){
        this.destroy();
      }
    }, 100);
  }

  ngOnDestroy(): void {
    this.destroyMessageSubscription?.unsubscribe();
    this.hover = false;
    this.destroy();
  }

  destroy(): void {
    if (this.componentRef !== null) {
      if (this.componentRef!.instance.hover) {
        // dont destroy if the user is hovering the interactive tooltip, wait for destroy message
        this.destroyMessageSubscription = this.componentRef.instance.destroyMessage.subscribe((message: any) => {
          if (this.componentRef !== null) {
            this.appRef.detachView(this.componentRef!.hostView);
            this.componentRef!.destroy();
            this.componentRef = null;
          }
        })
      } else {
        this.appRef.detachView(this.componentRef!.hostView);
        this.componentRef!.destroy();
        this.componentRef = null;
      }
    }
  }
}
