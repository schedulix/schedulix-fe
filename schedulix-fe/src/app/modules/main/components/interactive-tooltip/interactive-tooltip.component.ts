import { Component, HostListener, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-interactive-tooltip',
  templateUrl: './interactive-tooltip.component.html',
  styleUrls: ['./interactive-tooltip.component.scss']
})
export class InteractiveTooltipComponent implements OnInit {

  tooltip: string = '';
  left: number = 0;
  top: number = 0;
  hover: boolean = false;
  destroyMessage: Subject<any> = new Subject();

  constructor() {}

  ngOnInit(): void {}

  @HostListener('mouseenter')
  onMouseEnter(): void {
    this.hover = true;
  }

  @HostListener('mouseleave')
  onMouseLeave(): void {
    this.hover = false;
    // tell parent to destroy the tooltip
    this.destroyMessage.next(this.hover);
  }
}
