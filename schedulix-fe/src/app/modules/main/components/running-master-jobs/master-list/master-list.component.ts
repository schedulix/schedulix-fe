import { SelectionModel } from '@angular/cdk/collections';
import { NestedTreeControl } from '@angular/cdk/tree';
import { Component, HostListener, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { Subject, Subscription, TimeInterval } from 'rxjs';
import { Bookmark } from 'src/app/data-structures/bookmark';
import { rmjFilterObject, rmjSettingsObject, RunningMasterState } from 'src/app/data-structures/runningMasterState';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { NewItemDialogComponent } from 'src/app/modules/main/components/dialog-components/new-item-dialog/new-item-dialog.component';
import { OutputType, SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { BookmarkService } from '../../../services/bookmark.service';
import { MainInformationService } from '../../../services/main-information.service';

import rmjsettings_editorform from '../../../../../json/editorforms/monitoring/rmjsettings.json';
import rmjfilter_editorform from '../../../../../json/editorforms/monitoring/rmjfilter.json';

import { masterTreeNode, column, treeNode } from '../../../../../interfaces/tree'
import { TreeFunctionsService } from '../../../services/tree-functions.service';
import { SubmitEntityService } from '../../../services/submit-entity.service';
import { PrivilegesService } from '../../../services/privileges.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { EditorformDialogComponent, EditorformDialogData } from '../../form-generator/editorform-dialog/editorform-dialog.component';
import { CRUDService } from 'src/app/services/crud.service';
import { SubmittedEntityCrud } from 'src/app/classes/submitted-entity-crud';
import { sdmsException } from 'src/app/services/error-handler.service';

declare var hooks: any;

@Component({
  selector: 'app-master-list',
  templateUrl: './master-list.component.html',
  styleUrls: ['./master-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MasterListComponent implements OnInit, OnDestroy {

  constructor(
    // private route: ActivatedRoute,
    private commandApi: CommandApiService,
    private eventEmitterService: EventEmitterService,
    // private localStorageHelper: LocalStorageHelper,
    public matDialog: MatDialog,
    public bookmarkService: BookmarkService,
    public mainInformationService: MainInformationService,
    public treeFunctionsService: TreeFunctionsService,
    public submitEntityService: SubmitEntityService,
    public dialog: MatDialog,
    public privilegesService: PrivilegesService,
    private toolboxService: ToolboxService,
    private customPropertiesService: CustomPropertiesService,
    private crudService: CRUDService) {
  }

  @HostListener('window:resize', ['$event'])
  onresize(event: any) {
    // this.renderData();
  }

  public viewPortItems: any;
  totalLoadedNodeIndex: number = 0;
  loadInterval: any;
  loadnumber: number = 0;
  lastReload: string = '';

  treeControl = new NestedTreeControl<masterTreeNode>(node => node.children);
  getLevel = (node: masterTreeNode) => node.level ? node.level : 0;

  dataSource: any[] = []
  dataSourceAfterInit: any[] = []

  isLoading = true;
  TYPE: string = '';
  shortType: string = '';
  treeNodes: masterTreeNode[] = []

  expansionList: number[] = [];
  pinnedList: number[] = [];
  pinnedParrentList: number[] = [];

  displayPath: boolean = false;
  // pinnable: boolean = true;

  expandCommand: string = '0';

  // searchControl = new FormControl();
  searchString: string = '';

  // init empty
  settingsObject: rmjSettingsObject = new rmjSettingsObject();
  filterObject: rmjFilterObject = new rmjFilterObject();

  //init interval for autoloading
  interval: any;
  // autoReload: boolean = true;

  // ensures that the last loaded request is gonna be loaded
  loadToken: string = '0';

  @Input() command: string = '';
  @Input() shorttype: string = '';
  @Input() treeType: string = '';
  @Input() tab!: Tab;
  @Input() isActive: boolean | undefined;

  @Input() buttonHasSpace: boolean = true;
  // Needed for lazy loading tabs (see selectedTabSubscription).
  private isInitialized = false;

  desc: string[] = [];
  selectedTabSubscription: Subscription = new Subscription();
  bookmarks: Bookmark[] = [];
  detailBookmarks: Bookmark[] = [];
  selectedBookmark: Bookmark = this.bookmarks[0];
  rxjsUserHasLoadedObserver: Subscription = new Subscription();


  // set selected bookmark to a value at the very first load
  isFirstLoadedBookmark: boolean = true;

  //liste der momentan existierenden states
  globalStateList: string[] = [];

  // set defaults also here TODO maybe solve in json
  displayedColumns: column[] = this.toolboxService.deepCopy(this.customPropertiesService.getCustomObjectProperty('monitorDefaultColumns'));

  checklistSelection = new SelectionModel<masterTreeNode>(true /* multiple */);

  haveCriticalPath: boolean = false;

  hasLoadedChild = (_: number, node: masterTreeNode) => !!node.children && node.children.length > 0;
  hasChildren = (_: number, node: masterTreeNode) => node.hasChildren;

  ngOnInit(): void {
    // console.log('create');
    // dont load colors when active, it will be verified in the load later which wil be displayed
    !this.isActive ? this.checkColorCodingBeforeLoading(this.tab, this.command ,this._MasterJobtransformer.bind(this)): '';
    this.selectedTabSubscription = this.eventEmitterService.selectedTabEmitter.subscribe((result: string) => {
      if (this.tab.ID == result && !this.isInitialized) {
        this.getBookmarks();
        // check if the tab brings an default bookmark
        this.setBookmark();
        // load bookmarks when userInformation has updated
        this.rxjsUserHasLoadedObserver = this.mainInformationService.userHasLoadedObserver.subscribe((subject: any) => {
          // this.getBookmarks()
          if (this.isFirstLoadedBookmark) {
            this.setBookmark();
            this.isFirstLoadedBookmark = false;
          }
        });

        // so that this will be loaded only once
        this.isInitialized = true;
      } else if (this.isActive) {
        // data already loaded
      } else {
        // when tab is inactive clear interval and DOM
        // clearInterval(this.loadInterval);
        this.loadnumber = 0;
      }
    });
    this.haveCriticalPath = this.mainInformationService.checkVersion('2.12');
  }

  getBookmarks() {
    this.bookmarks = this.bookmarkService.getBookmarksOfType('MASTER');
    this.detailBookmarks = this.bookmarkService.getBookmarksOfType('DETAIL');
  }

  setBookmark(load: boolean = true) {
    if (this.tab.BOOKMARK) {
      let tabBookmark = this.bookmarkService.getBookmarkOfNameAndType(this.tab.BOOKMARK.bookmark, 'MASTER');
      tabBookmark ? this.tab.BOOKMARK = tabBookmark : '';
      this.selectBookmark(this.tab.BOOKMARK, load);
    } else {
      this.selectBookmark(this.bookmarks[0], load)
    }
  }

  ngOnDestroy(): void {
    // console.log('DESTROY COMPONENT')
    this.endTimer();
    this.rxjsUserHasLoadedObserver.unsubscribe();
    this.selectedTabSubscription.unsubscribe();
    // clearInterval(this.loadInterval);
    // clearInterval(this.interval);

  }

  startTimer() {
    this.endTimer();
    // console.log(typeof this.settingsObject.AUTO_REFRESH)
    // console.log(this.settingsObject.AUTO_REFRESH)
    if (this.settingsObject.AUTO_REFRESH > 0) {
      this.interval = setInterval(() => {
        if (this.settingsObject.AUTO_REFRESH > 0) {
          this.reloadTree();
        }
      }, this.settingsObject.AUTO_REFRESH > 0 ? this.settingsObject.AUTO_REFRESH * 1000 : 10000);
    } else {
      this.endTimer();
    }
  }

  endTimer() {
    clearInterval(this.interval);
  }

  private _MasterJobtransformer = (node: any, index: number) => {


    let dynamicobj: any = new Object();
    for (const [key, value] of Object.entries(node)) {
      // if (this.displayedColumns.find(element => element.parameter_name == key)) {
      // check if there is a value matching to the key from the server
      dynamicobj[key] = value ? value : '';
      // }

    }

    let a = '';
    a.replace('', '')
    let childrenStates = this.submitEntityService.getChildrenStates(node);
    // console.log(dynamicobj)
    let treeNode: masterTreeNode = {
      id: parseInt(node.ID),
      index: this.totalLoadedNodeIndex,

      name: node.CHILDTAG != "" ? node.HIERARCHY_PATH.replace('[' + node.CHILDTAG + ']', '').split('.').pop() : node.HIERARCHY_PATH.split('.').pop(),// get last element
      namePath: node.HIERARCHY_PATH,
      isPinned: false,
      // this.pinnedList.indexOf(parseInt(node.ID, 10)) > -1

      isPinnedParent: false,
      isInvisible: false,
      iconType: node.SE_TYPE ? node.SE_TYPE.toLowerCase() : "other",
      hasChildren: parseFloat(node.CHILDREN) > 0,
      type: node.SE_TYPE ? node.SE_TYPE : "other",

      hit: '',
      state: node.STATE,
      jobState: node.STATE,
      end: node.FINISH_TS,


      start: (this.treeFunctionsService.startTime(node) ? (this.toolboxService.convertTimeStamp(new Date(this.treeFunctionsService.startTime(node)).toISOString(), this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone"),
        this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat"))) : ''),


      exitstate: node.FINAL_ESD,
      runtime: (node.STATE == 'SCHEDULED' ? -1 : (node.FINAL_TS == null ? new Date().getTime() - Date.parse(node.SUBMIT_TS) : Date.parse(node.FINAL_TS) - Date.parse(node.SUBMIT_TS))),

      // state logic
      isSuspended: node.PARENT_SUSPENDED,
      parentSuspended: node.PARENT_SUSPENDED,

      parentIconColor: this.submitEntityService.getParentIconColor(node, node.STATE, childrenStates),  // icon des batch aspects
      jobIconColor: this.submitEntityService.getIconColor(node, node.STATE, node.SE_TYPE, node.JOB_IS_FINAL, node.IS_RESTARTABLE, node.IS_DISABLED, childrenStates),  // icon des job aspects
      textColor: this.submitEntityService.getTextColor(node, node.STATE, node.SE_TYPE, node.JOB_IS_FINAL, node.IS_RESTARTABLE, node.IS_DISABLED, childrenStates),
      displayState: this.submitEntityService.getDisplayState(node, childrenStates),   // angezeigter technischer state

      // final_ts: node.FINAL_TS != null ? formatDate(new Date(node.FINAL_TS), 'M/d/yy, h:mm a', 'en-US') : '',
      // submit_ts: formatDate(new Date(node.SUBMIT_TS), 'M/d/yy, h:mm a', 'en-US'),

      final_ts: node.FINAL_TS != null ?
        this.toolboxService.convertTimeStamp(new Date(node.FINAL_TS).toISOString(), this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone"),
          this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat")) : '',
      submit_ts: '',
      // this.toolboxService.convertTimeStamp(new Date(node.SUBMIT_TS).toISOString(), this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone"),
      // this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat")),

      parity: index % 2 == 0 ? 'even' : 'odd',
      childrenStates: childrenStates,

      is_restartable: node.IS_RESTARTABLE,
      is_disabled: node.IS_DISABLED,
      // is_replaced: node.IS_REPLACED;
      is_suspended: node.IS_SUSPENDED,
      is_cancelled: node.IS_CANCELLED,
      cnt_restartable: node.CNT_RESTARTABLE,

      workdir: node.WORKDIR,
      logUrl: this.toolboxService.makeLogPath(node.HTTPHOST, node.HTTPPORT),
      normalizedPrivs : this.privilegesService.normalize(node.PRIVS)
    }
    this.totalLoadedNodeIndex++;
    let assigned = Object.assign(dynamicobj, treeNode);
    return assigned;
  }

  getCommand(command: string): string {
    command = this.applyServerSettings(command);
    return command;
  }

  initTree(command: string) {

    this.endTimer()
    this.totalLoadedNodeIndex = 0;

    let loadTokenHash = this.toolboxService.uuidv4();
    this.loadToken = loadTokenHash;

    
    // this.commandApi.executeDelegate(this.getCommand(command), (result: any) => {
    this.commandApi.execute(this.getCommand(command)).then((result: any) => {
      if (this.loadToken == loadTokenHash) {
        // console.log("-----------------------LOAD TREE-----------------------------------")
        if (result.hasOwnProperty('ERROR')) {

          throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
          return;
        } else {
          this.desc = result.DATA.DESC
          let treeNodes: masterTreeNode[] = [];
          // console.log(result)

          //first element is the parent
          // treeNodes.push(this._MasterJobtransformer(result.DATA.TABLE[0]));
          // treeNodes[0].children = [];
          let index = 0;

          // reset state list
          this.globalStateList = [];
          for (let o of result.DATA.TABLE) {
            // if (o.hasOwnProperty('IDPATH')) {
              
            // add normalized PRIVS to nodes
            //  is done with tree node creation now
            // o.normalizedPrivs = this.privilegesService.normalize(o.PRIVS);

            let parentPath = o.HIERARCHY_PATH.split('.')

            parentPath.pop()
            parentPath = parentPath.join('.');
            // console.log(parentPath)

            // console.log("parent not found for: " + parentPath + " create new independent Node")
            // o.children = [];
            let treeNode: masterTreeNode = this._MasterJobtransformer(o, index)
            treeNode.state = treeNode.childrenStates ? this.submitEntityService.evaluateState(treeNode) : treeNode.state;

            this.globalStateList.push(treeNode.textColor);

            // for (let o of treeNode.childrenStates? treeNode.childrenStates : []) {
            //   this.globalStateList.push(o);
            // }

            treeNodes.push(treeNode);


            // } else {
            //   console.warn("mandatory propertys missing in TreeItem")
            //   console.warn(o)
            // }
            index++;
          }
          // sort etc
          treeNodes = this.applySettingsAndFilters(treeNodes);

          this.indexTree(treeNodes);

          this.dataSource = treeNodes;
          this.dataSourceAfterInit = treeNodes;

          // console.log(this.dataSource.data)

          let date = new Date()
          this.lastReload = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();

          // console.log(this.globalStateList);

          // console.log(this.globalStateList);

          // TODO evaluation ot correct
          this.tab ? this.tab.COLOR = this.treeFunctionsService.getTabColorEvaluation(this.globalStateList) : '';

          this.settingsObject.AUTO_REFRESH != 0 ? this.tab.AUTORELOAD = true : this.tab.AUTORELOAD = false;
          this.eventEmitterService.updateTabEmitter.next(this.tab);

          this.resetSelection(this.dataSource);
          this.endTimer()
          this.startTimer();
          this.isLoading = false;
          // this.renderData();

          // console.log(this.dataSource)
        }
      } else {
        // console.log('...Tree not loaded, Token invalid')
      }
    }, (rejection: any) => {
      // console.log(rejection)
      this.isLoading = false;  
    });
  }

  checkColorCodingBeforeLoading(tab: Tab, command: string, transformer: Function) {

    this.getBookmarks();
    this.setBookmark(false);

    this.commandApi.execute(this.getCommand(command)).then((result: any) => {
      let globalStateList = [];
      for (let o of result.DATA.TABLE) {

        let parentPath = o.HIERARCHY_PATH.split('.')
        parentPath.pop()
        parentPath = parentPath.join('.');

        let treeNode: masterTreeNode = transformer(o,1);
        treeNode.state = treeNode.childrenStates ? this.submitEntityService.evaluateState(treeNode) : treeNode.state;
        globalStateList.push(treeNode.textColor);
      }
      // console.log(globalStateList);
      let color = this.treeFunctionsService.getTabColorEvaluation(globalStateList)
      tab ? tab.COLOR = color : '';
      // console.log(color);
    });
  }

  indexTree(treeNodes: masterTreeNode[]) {
    let runindex = 0;
    for (let node of treeNodes) {
      node.index = runindex;
      runindex++;
    }
  }

  openCriticalPath(node: any) {
    let criticalPathTab = new Tab(node.name, String(node.ID), 'CRITICAL PATH', 'critical_path', TabMode.CRITICAL_PATH);
    criticalPathTab.MODE = TabMode.EDIT;
    this.eventEmitterService.createTab(criticalPathTab, true);
  }
  
  openTab(node: any) {
    // let tab = new Tab(node.name, node.id.toString(), node.type, node.iconType, TabMode.EDIT);
    // // tab.ID = node.id.toString();
    // // tab.MODE = TabMode.EDIT;
    // // tab.NAME = node.name;
    // // tab.shortType = node.iconType;
    // // tab.TYPE = node.type;
    this.getBookmarks();
    let detailBookmark = undefined;
    if (node.P_DETAIL_BOOKMARK) {
      detailBookmark = this.detailBookmarks.find(element => element.bookmark == node.P_DETAIL_BOOKMARK);
    }
    if (!detailBookmark && this.settingsObject.DETAIL_BOOKMARK) {
      detailBookmark = this.detailBookmarks.find(element => element.bookmark == this.settingsObject.DETAIL_BOOKMARK);
    }
    if (!detailBookmark) {
      detailBookmark = this.detailBookmarks.find(element => element.bookmark == 'DEFAULT')
    }
    if (node.STATE == 'SCHEDULED') {
      let monitorJobTab = new Tab(node.name, String(node.id), node.type, node.type.toLowerCase(), TabMode.EDIT)
      this.eventEmitterService.createTab(monitorJobTab, true);
    } else {
      let p = node.namePath.split('.');
      let path = '';
      if (p.length > 1) {
        p.pop();
        path = p.join('.');
      }
      if (node.CHILDREN > 0) {
        // let monitorJobTab = new Tab('Detail ' + node.name, String(node.id), 'MONITOR_DETAIL', 'detail', TabMode.MONITOR_DETAIL)
        let monitorJobTab = new Tab(node.name, String(node.id), 'MONITOR DETAIL', 'detail', TabMode.MONITOR_DETAIL)
        monitorJobTab.PATH = path;
        monitorJobTab.BOOKMARK = detailBookmark;
        this.eventEmitterService.createTab(monitorJobTab, true);
      } else {
        let monitorJobTab = new Tab(node.name, String(node.id), 'SUBMITTED ENTITY', 'submitted_entity', TabMode.EDIT)
        monitorJobTab.PATH = path;
        this.eventEmitterService.createTab(monitorJobTab, true);
      }
    }
    // let monSearchTab = new Tab('Detail ' + node.name, String(node.id), 'Detail Master'.toLocaleUpperCase(), 'detail', TabMode.MONITOR_DETAIL)
    // monSearchTab.BOOKMARK =  this.settingsObject.DETAIL_BOOKMARK ? this.detailBookmarks.find(element => element.bookmark ==  this.settingsObject.DETAIL_BOOKMARK): undefined;
    // this.eventEmitterService.createTab(monSearchTab, true);
  }

  reloadTree() {
    this.isLoading = true;
    // this.checklistSelection.
    // this.pinnedParrentList = [];
    let command = this.command;

    // this.setExpandCommand();
    this.initTree(command);
  }

  getDisplayedDesc() {

    // console.log(this.displayedColumns)
    return this.desc.filter((item) => this.displayedColumns.find(element => element.parameter_name == item));
  }

  resetDisplay() {
    this.displayedColumns = this.toolboxService.deepCopy(this.customPropertiesService.getCustomObjectProperty('monitorDefaultColumns'));
  }


  applySettingsAndFilters(treeNodes: masterTreeNode[]) {
    // ORDER
    treeNodes = this.treeFunctionsService.order(treeNodes, this.settingsObject.SORT_ORDER);

    return treeNodes;
  }

  applyServerSettings(command: string) {
    // console.log(command)

    this.resetDisplay();

    // APPLY FILTER
    //begin filter
    //ENABLED ONLY CHECK
    let enableString = '';
    if (this.filterObject.ENABLE_ONLY == 'true') {
      enableString = 'ENABLED ONLY,';
    }
    let filterCommand: string = ',' + enableString + ' MASTER AND ('

    // HISTORY
    // HISTORY FROM TO
    let seperator = '';
    if (this.filterObject.HISTORY_TO != '') {
      let historyFilterComment: string = 'HISTORY BETWEEN ' + this.filterObject.HISTORY_FROM + ' ' + this.filterObject.HISTORY_FROM_UNIT + ' AND ' + this.filterObject.HISTORY_TO + ' ' + this.filterObject.HISTORY_TO_UNIT;
      filterCommand += historyFilterComment;
      seperator = ' AND ';
    } else if (this.filterObject.HISTORY_FROM) {
      let historyFilterComment: string = 'HISTORY = ' + this.filterObject.HISTORY_FROM + ' ' + this.filterObject.HISTORY_FROM_UNIT;
      filterCommand += historyFilterComment;
      seperator = ' AND ';
    }
    // HISTORY FUTURE
    if (this.filterObject.HISTORY_FUTURE != '') {
      let historyFilterComment: string = seperator + ' FUTURE = ' + this.filterObject.HISTORY_FUTURE + ' ' + this.filterObject.HISTORY_FUTURE_UNIT;
      filterCommand += historyFilterComment;
      seperator = ' AND ';
    }



    // NAME PATTERNS
    let s = '';
    s = this.treeFunctionsService.applyNameLike(this.filterObject);
    if (s != '') {
      filterCommand += seperator + s;
      seperator = ' AND ';
    }
    //MERGED EXIT STATES
    s = this.treeFunctionsService.applyMergedExitStates(this.filterObject);
    if (s != '') {
      filterCommand += seperator + s;
      seperator = ' AND ';
    }

    // JOB STATUS
    s = this.treeFunctionsService.applyJobStates(this.filterObject);
    if (s != '') {
      filterCommand += seperator + s;
      seperator = ' AND ';
    }

    // CONDITION
    if (this.filterObject.CONDITION_MODE != 'NONE' && this.filterObject.CONDITION != '') {
      if (seperator != '') {
        filterCommand += ' ' + this.filterObject.CONDITION_MODE + ' (' + this.filterObject.CONDITION + ')';
      }
      else {
        filterCommand += ' (' + this.filterObject.CONDITION + ')';
      }
    }
    //end filter
    filterCommand = filterCommand + ')';
    // console.log(filterCommand);
    command += filterCommand;


    // APPLY SETTINGS
    // PARAMETERS
    command += this.treeFunctionsService.applyParameter(this.settingsObject, this.displayedColumns);

    return command;
  }

  openSettings() {

    let dataObj: any = this.toolboxService.deepCopy(this.settingsObject);
    dataObj.PRIVS = 'E';
    const dialogRef = this.dialog.open(EditorformDialogComponent, {
      data: new EditorformDialogData(rmjsettings_editorform, dataObj, this.tab, "List settings"),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
      width: "1000px"
    });
    // * Note, dialogs unsubsrcibe autoamtically!
    return dialogRef.afterClosed().toPromise().then((result: any) => {
      // console.log(result);
      if (result != undefined || result != null && result) {
        // console.log(result)
        if (result.METHOD == 'apply_and_save_filter') {
          this.settingsObject = result;

          this.settingsObject.AUTO_REFRESH = parseInt(result.AUTO_REFRESH);
          this.saveBookmark()
          // this.reloadTree();
        } else if (result.METHOD == 'apply_filter') {
          this.settingsObject = result;

          this.settingsObject.AUTO_REFRESH = parseInt(result.AUTO_REFRESH);
          this.reloadTree();
        }
      }
      return 1;
    });
  }

  openFilter() {
    let dataObj: any = this.toolboxService.deepCopy(this.filterObject);
    dataObj.PRIVS = 'E';
    const dialogRef = this.dialog.open(EditorformDialogComponent, {
      data: new EditorformDialogData(rmjfilter_editorform, dataObj, this.tab, "Filter options"),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
      width: "1000px"
    });
    // * Note, dialogs unsubsrcibe autoamtically!
    return dialogRef.afterClosed().toPromise().then((result: any) => {
      // console.log(result);
      if (result != undefined || result != null && result) {
        // console.log(result)
        if (result.METHOD == 'apply_and_save_filter') {
          this.filterObject = result;
          this.saveBookmark()
          // this.reloadTree();
        } else if (result.METHOD == 'apply_filter') {
          this.filterObject = result;
          this.reloadTree();
        }
      } else {
        if (this.selectedBookmark.startmode == 'QUERY' && this.isLoading) {
          // this.openFilter()
          this.eventEmitterService.pushSnackBarMessage(["used_Bookmark_require_a_filter"], SnackbarType.INFO)
        };
      }
      return 1;
    });
  }


  newBookmark() {
    const dialogRef = this.matDialog.open(NewItemDialogComponent, {
      data: this.bookmarkService.getNewBookmarkDialogData(this.selectedBookmark),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });



    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result.input) {
        // console.log(result.input)
        let bookmark = new Bookmark(result.input, 'MASTER');
        bookmark.scope = result.choosedItem;

        let treeViewState = new RunningMasterState();
        treeViewState.settingsObject = this.settingsObject;
        treeViewState.filterObject = this.filterObject;

        bookmark.value = treeViewState;


        this.bookmarkService.setUserBookmark(bookmark, false).then(() => {
          this.tab.BOOKMARK = bookmark;
          this.selectBookmark(bookmark)
          this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_created'], SnackbarType.INFO)
        });

      }
    });
  }


  selectBookmark(bookmark: Bookmark, load: boolean = true) {
    // if bookmark.value is empty
    this.getBookmarks()
    if (Object.keys(bookmark.value).length === 0) {
      bookmark.value = new RunningMasterState();
    }

    this.selectedBookmark = bookmark;
    this.tab.BOOKMARK = bookmark;

    this.settingsObject = this.selectedBookmark.value.settingsObject;
    this.filterObject = this.selectedBookmark.value.filterObject;
    if(load) {
      if (this.selectedBookmark.startmode == 'QUERY') {
        this.openFilter();
      } else {
        this.reloadTree();
      }
    }
  }

  saveBookmark() {
    // console.log("SAVE")
    let treeViewState = new RunningMasterState();
    treeViewState.settingsObject = this.settingsObject;
    treeViewState.filterObject = this.filterObject;

    this.selectedBookmark.value = treeViewState;
    this.bookmarkService.setUserBookmark(this.selectedBookmark, false).then(() => {
      this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_updated'], SnackbarType.INFO);
      this.selectBookmark(this.selectedBookmark);
    }, () => {
      // this.eventEmitterService.pushSnackBarMessage(['insufficiant_privilege'], SnackbarType.INFO)
    });
    this.tab.BOOKMARK = this.selectedBookmark;
  }

  isSameBookmark(a: Bookmark, b: Bookmark) {
    return this.bookmarkService.isSameBookmark(a, b)
  }

  toggleSelectAll(event: any) {
    // console.log(event)
    for (let o of this.dataSource) {
      if (this.checkBulkSelectable(o)) {
        event.checked ? this.checklistSelection.select(o) : this.checklistSelection.deselect(o);
      }
    }
  }

  /** Toggle a leaf to-do item selection. Check all the parents to see if they changed */
  leafItemSelectionToggle(node: masterTreeNode): void {

    this.checklistSelection.toggle(node);
    // console.log(this.checklistSelection.selected)
    // this.checkAllParentsSelection(node);
  }

  checkBulkSelectable(node: any): boolean {
    return this.treeFunctionsService.checkBulkSelectable(node);
  }

  resetSelection(nodes: any[]) {
    // console.log(nodes);
    // console.log(this.checklistSelection.selected);
    let newSelected = [];
    for (let n of nodes) {
      for (let o of this.checklistSelection.selected) {
        if (n.id == o.id && this.checkBulkSelectable(n)) {
          newSelected.push(n);
          break;
        }
      }
    }
    this.checklistSelection.clear();
    for (let s of newSelected) {
      this.checklistSelection.select(s);
    }
  }

  sortData(sort: any) {
    // console.log(sort)
    const data = this.dataSource.slice();
    // when 3th state(nor asc or desc is selected)
    if (!sort.active || sort.direction === '') {
      this.dataSource = this.dataSourceAfterInit;
      return;
    }

    this.dataSource = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      for (let entry of this.displayedColumns) {

        // console.log(entry.parameter_name + ' ' + sort.active)
        if (sort.active == 'name') {
          return this.compare(a.name, b.name, isAsc);
        }
        if (entry.parameter_name == sort.active) {
          // console.log(entry.parameter_name)
          return this.compare((a as any)[entry.parameter_name], (b as any)[entry.parameter_name], isAsc);
        }
      }
      return 0;
    });
  }
  compare(a: masterTreeNode | any, b: masterTreeNode | any, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  msToTime(s: number): string {


    return this.treeFunctionsService.msToTime(s);
  }

  // ---------------------------------- Operator Actions ----------------------------------

  checkBulkOperationsAllowed(operation: string): boolean {
    return this.treeFunctionsService.checkBulkOperationsAllowed(operation, this.checklistSelection);
  }

  cancelNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.createCancelConfirmDialog(bicsuiteObject, tabInfo, false, "doCancelEntity");
      return confirmDialog.afterClosed().toPromise().then((result: any) => {
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  clearWarning(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.clearWarningEntity(bicsuiteObject, tabInfo).then((result: any) => {
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  cancelSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createCancelConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doCancelEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }

  rerunNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      return submittedEntityCrud.createRerunConfirmDialog(bicsuiteObject, tabInfo, false, "doRerunEntity").then((result: any) => {
        return result.afterClosed().toPromise().then((result: any) => {

          // reload only if sth was returned (not closed or ESC)
          if (result) {
            this.reloadTree();
          }
          return 1;
        });
      });
    });
  }

  rerunSelectedRecursive() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    return submittedEntityCrud.createRerunConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doRerunEntitiesRecursive").then((result: any) => {
      return result.afterClosed().toPromise().then((result: any) => {
        // reload only if sth was returned (not closed or ESC)
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  resumeNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.createResumeConfirmDialog(bicsuiteObject, tabInfo, false, "doResumeEntity");
      return confirmDialog.afterClosed().toPromise().then((result: any) => {
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  resumeSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createResumeConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doResumeEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }

  suspendNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.createSuspendConfirmDialog(bicsuiteObject, tabInfo, false, "doSuspendEntity");
      return confirmDialog.afterClosed().toPromise().then((result: any) => {
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  suspendSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createSuspendConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doSuspendEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }
  openLog(logfile: string, node: masterTreeNode) {
    this.toolboxService.OpenTextWindow(logfile, node);
  }
}
