import { CollectionViewer, DataSource, SelectionChange, SelectionModel } from '@angular/cdk/collections';
import { FlatTreeControl, NestedTreeControl } from '@angular/cdk/tree';
import { ChangeDetectorRef, Component, HostListener, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { UntypedFormControl } from '@angular/forms';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { MatTreeNestedDataSource } from '@angular/material/tree';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject, merge, Observable, Subscription } from 'rxjs';
import { expand, filter, map } from 'rxjs/operators';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { NewItemDialogComponent, NewItemDialogData } from 'src/app/modules/main/components/dialog-components/new-item-dialog/new-item-dialog.component';
import { SnackbarType } from 'src/app/modules/global-shared/components/snackbar/snackbar-data';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { BicsuiteResponse } from 'src/app/data-structures/bicsuite-response';
import { SearchFilterObject, SearchSettingsObject, SearchJobState } from 'src/app/data-structures/runningJobsState';
import { Bookmark } from 'src/app/data-structures/bookmark';
import { BookmarkService } from '../../../services/bookmark.service';
import { MainInformationService } from '../../../services/main-information.service';
import searchSettings_editorform from '../../../../../json/editorforms/monitoring/searchsettings.json';
import searchFilter_editorform from '../../../../../json/editorforms/monitoring/searchFilter.json';
import detailSettings_editorform from '../../../../../json/editorforms/monitoring/detailSettings.json';
import detailFilter_editorform from '../../../../../json/editorforms/monitoring/detailFilter.json';
import { formatDate, registerLocaleData } from '@angular/common'

import localeDe from '@angular/common/locales/de'
import localeNl from '@angular/common/locales/nl'
import localeEn from '@angular/common/locales/en'


import { masterTreeNode, column, treeNode } from '../../../../../interfaces/tree'
import { TreeFunctionsService } from '../../../services/tree-functions.service';
import { SubmitEntityService } from '../../../services/submit-entity.service';
import { detailFilterObject, detailsettingsObject, RunningDetailState } from 'src/app/data-structures/runningDetailState';
import { PrivilegesService } from '../../../services/privileges.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { EditorformDialogComponent, EditorformDialogData } from '../../form-generator/editorform-dialog/editorform-dialog.component';
import { SubmittedEntityCrud } from 'src/app/classes/submitted-entity-crud';
import { CRUDService } from 'src/app/services/crud.service';
// import { VirtualScrollerComponent } from 'ngx-virtual-scroller';


@Component({
  selector: 'app-list-tree',
  templateUrl: './tree-list.component.html',
  styleUrls: ['./tree-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class TreeListComponent implements OnInit, OnDestroy {



  constructor(private route: ActivatedRoute,
    private commandApiService: CommandApiService,
    private eventEmitterService: EventEmitterService,
    private localStorageHelper: LocalStorageHelper,
    public matDialog: MatDialog,
    public bookmarkService: BookmarkService,
    public mainInformationService: MainInformationService,
    private treeFunctionsService: TreeFunctionsService,
    private submitEntityService: SubmitEntityService,
    private privilegesService: PrivilegesService,
    private toolboxService: ToolboxService,
    private customPropertiesService: CustomPropertiesService,
    private crudService: CRUDService) {


  }

  @HostListener('window:resize', ['$event'])
  onresize(event: any) {
    // this.renderData();
  }

  selectedTabSubscription: Subscription = new Subscription();
  isInitialized: boolean = false;

  lastReload: string = '';

  treeControl = new NestedTreeControl<masterTreeNode>(node => node.children);
  getLevel = (node: masterTreeNode) => node.level ? node.level : 0;

  dataSource = new MatTreeNestedDataSource<masterTreeNode>();
  dataSourceAfterInit = new MatTreeNestedDataSource<masterTreeNode>();

  isLoading = true;
  TYPE: string = '';
  shortType: string = '';
  treeNodes: masterTreeNode[] = []

  expansionList: number[] = [];
  pinnedList: number[] = [];
  pinnedParrentList: number[] = [];
  // pinnable: boolean = true;

  expandCommand: string = '0';

  totalLoadedNodeIndex = 0;

  searchControl = new UntypedFormControl();

  // is changed if there is a node which extends the space
  maxCssTreeNodeSize: number = 240;

  searchString: string = '';

  // init empty
  settingsObject: SearchSettingsObject = new SearchSettingsObject();
  filterObject: SearchFilterObject = new SearchFilterObject();

  @Input() command: string = '';
  @Input() shorttype: string = '';
  @Input() treeType: string = '';
  @Input() tab!: Tab;
  @Input() isActive: boolean | undefined;

  @Input() buttonHasSpace: boolean = true;

  loadnumber: number = 0;

  loadInterval: any;

  tabTypes = TabMode;

  desc: string[] = [];

  bookmarks: Bookmark[] = [];
  selectedBookmark: Bookmark = this.bookmarks[0];
  rxjsUserHasLoadedObserver: Subscription = new Subscription();

  // ensures that the last loaded request is gonna be loaded
  loadToken: string = '0';

  // all loaded states
  globalStateList: string[] = [];

  // set selected bookmark to a value at the very first load
  isFirstLoadedBookmark: boolean = true;

  //init interval for autoloading
  interval: any;

  // set defaults also here TODO maybe solve in json
  displayedColumns: column[] = this.toolboxService.deepCopy(this.customPropertiesService.getCustomObjectProperty('monitorDefaultColumns'));
  checklistSelection = new SelectionModel<masterTreeNode>(true /* multiple */);

  haveCriticalPath: boolean = false;


  hasLoadedChild = (_: number, node: masterTreeNode) => !!node.children && node.children.length > 0;
  hasChildren = (_: number, node: masterTreeNode) => node.hasChildren;

  ngOnDestroy(): void {
    // console.log('DESTROY COMPONENT')

    this.selectedTabSubscription.unsubscribe();
    this.rxjsUserHasLoadedObserver.unsubscribe();
    // clearInterval(this.loadInterval);
    clearInterval(this.interval);
    this.isActive = false;
  }

  ngOnInit(): void {
    // dont load colors when active, it will be verified in the load later which wil be displayed
    !this.isActive ? this.checkColorCodingBeforeLoading(this.tab, this.command, this.flatPathToLeverTransformer.bind(this)): '';
    // console.log('INIT COMPONENT')
    this.selectedTabSubscription = this.eventEmitterService.selectedTabEmitter.subscribe((result: string) => {
      if (this.tab.ID == result && !this.isInitialized) {
        // this.initTree(this.command);


        // get parameters from bookmarking
        this.getBookmarks();

        // check if the tab brings an default bookmark
        this.setTabBookmark(true);
        // load bookmarks when userInformation has updated
        this.rxjsUserHasLoadedObserver = this.mainInformationService.userHasLoadedObserver.subscribe((subject: any) => {
          this.getBookmarks();

          if (this.isFirstLoadedBookmark) {
            this.setTabBookmark(true);
            this.isFirstLoadedBookmark = false;
          }
          // if nothing is selected select first -> default
        });
        // so that this will be loaded only once
        this.isInitialized = true;
      } else if (this.isActive) {
        // data already loaded
        // only render
        // this.renderData();
      } else {
        // when tab is inactive
        // clearInterval(this.loadInterval);
        this.loadnumber = 0;
      }
    });
    registerLocaleData(localeDe, 'de-DE', localeDe);
    this.haveCriticalPath = this.mainInformationService.checkVersion('2.12');
  }
  getBookmarks() {
    // get parameters from bookmarking
    if (this.tab.MODE == TabMode.MONITOR_JOBS) {
      this.bookmarks = this.bookmarkService.getBookmarksOfType('SEARCH');
    } else if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      this.bookmarks = this.bookmarkService.getBookmarksOfType('DETAIL');
    }
  }

  isSameBookmark(a: Bookmark, b: Bookmark) {
    return this.bookmarkService.isSameBookmark(a, b)
  }

  getBookmarksOfNameAndType(bookmark: Bookmark) {
    if (this.tab.MODE == TabMode.MONITOR_JOBS) {
      return this.bookmarkService.getBookmarkOfNameAndType(bookmark.bookmark, 'SEARCH');
    } else if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      return this.bookmarkService.getBookmarkOfNameAndType(bookmark.bookmark, 'DETAIL');
    } else {
      return this.bookmarkService.getBookmarkOfNameAndType(bookmark.bookmark, 'SEARCH');
    }
  }

  setTabBookmark(select: boolean) {
    if (this.tab.BOOKMARK) {
      let tabBookmark = this.getBookmarksOfNameAndType(this.tab.BOOKMARK);
      tabBookmark ? this.tab.BOOKMARK = tabBookmark : '';
      this.selectedBookmark = this.tab.BOOKMARK;
      this.settingsObject = this.selectedBookmark.value.settingsObject;
      this.filterObject = this.selectedBookmark.value.filterObject;
      select ? this.selectBookmark(this.tab.BOOKMARK) : '';
    } else {
      this.selectedBookmark = this.bookmarks[0];
      this.settingsObject = this.selectedBookmark.value.settingsObject;
      this.filterObject = this.selectedBookmark.value.filterObject;
      select ? this.selectBookmark(this.bookmarks[0]) : '';
    }
  }

  compare(a: masterTreeNode | any, b: masterTreeNode | any, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  openTab(node: any) {

    if (node.STATE == 'SCHEDULED') {
      let monitorJobTab = new Tab(node.name, String(node.id), node.type, node.type.toLowerCase(), TabMode.EDIT)
      this.eventEmitterService.createTab(monitorJobTab, true);
    } else {
      let tab = new Tab(node.name, node.id.toString(), 'SUBMITTED ENTITY', 'submitted_entity', TabMode.EDIT);
      let p = node.namePath.split('.');
      let path = '';
      if (p.length > 1) {
        p.pop();
        path = p.join('.');
      }
      tab.PATH = path;
      this.eventEmitterService.createTab(tab, true);
    }
  }

  openCriticalPath(node: any) {
    let criticalPathTab = new Tab(node.name, String(node.ID), 'CRITICAL PATH', 'critical_path', TabMode.CRITICAL_PATH);
    criticalPathTab.MODE = TabMode.EDIT;
    this.eventEmitterService.createTab(criticalPathTab, true);
  }

  reloadTree() {

    //TODO CHECK DOPPELKLICK
    // this.isLoading = true;
    // this.pinnedParrentList = [];

    // this.setExpandCommand();
    // this.initTree(command);
    this.loadFlatTree();
  }

  getLevelArray(node: masterTreeNode) {

    let level = this.countLevel(node, 0);
    // console.log("PARENTNODE FOR LEVEL ARRAY")
    // console.log(node.parentNode)
    return new Array(level);
  }

  countLevel(node: masterTreeNode, count: number): number {
    if (node.parentNode) {
      // console.log("+++")
      count++;
      return this.countLevel(node.parentNode, count);
    } else {
      return count;
    }
  }



  getDisplayedDesc() {
    return this.desc.filter((item) => this.displayedColumns.find(element => element.parameter_name == item));
  }

  // TODO REFACTOR
  resetDisplay() {
    this.displayedColumns = this.toolboxService.deepCopy(this.customPropertiesService.getCustomObjectProperty('monitorDefaultColumns'));
  }

  applyFrontEndSettings(treeNodes: any[]) {
    treeNodes = this.treeFunctionsService.flatTreeorder(treeNodes, this.settingsObject.SORT_ORDER);
    return treeNodes;
  }

  applyServerSettings() {
    // console.log(command

    //begin filter
    let filterCommand: string = ''


    let masterString = '';
    //MASTER ID
    let masterid = this.filterObject.MASTER_ID;
    if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      masterid = this.tab.ID
      if (this.tab.DATA?.ID) {
        masterid = this.tab.DATA.MASTER_ID;
      }
    }
    if (masterid != '') {
      masterString += '( MASTER_ID IN (' + masterid + '))';
    } else {
    }

    // MASTER CHECKBOX OR DETAIL
    if (masterString != '') {
      masterString += " AND ";
    }
    if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      masterString += "( MASTER";
      if (this.tab.DATA?.ID) {
        masterString += " OR JOB IN (" + this.tab.DATA.ID + ")";
      }
    }
    else if (this.filterObject.MASTER_CHECK == 'true') {
      masterString += "MASTER";
    }
    masterString ? filterCommand += ', ' + masterString : '';


    let filterCommandAppendix: string[] = []

    let historyString = '';
    // HISTORY
    // HISTORY FROM TO IS ALWAYS but in detail
    if (this.tab.MODE != TabMode.MONITOR_DETAIL) {
      if (this.filterObject.HISTORY_TO != '') {
        let historyFilterComment: string = ' HISTORY BETWEEN ' + this.filterObject.HISTORY_FROM + ' ' + this.filterObject.HISTORY_FROM_UNIT + ' AND ' + this.filterObject.HISTORY_TO + ' ' + this.filterObject.HISTORY_TO_UNIT;
        historyString += historyFilterComment;
      } else if (this.filterObject.HISTORY_FROM) {
        let historyFilterComment: string = ' HISTORY = ' + this.filterObject.HISTORY_FROM + ' ' + this.filterObject.HISTORY_FROM_UNIT;
        historyString += historyFilterComment;
      }
      // HISTORY FUTURE
      if (this.filterObject.HISTORY_FUTURE != '') {
        let historyFilterComment: string = ' AND FUTURE = ' + this.filterObject.HISTORY_FUTURE + ' ' + this.filterObject.HISTORY_FUTURE_UNIT;
        historyString += historyFilterComment;
      }
    }

    historyString != '' ? filterCommandAppendix.push(historyString) : '';

    // NAME PATTERNS
    let namelike = this.treeFunctionsService.applyNameLike(this.filterObject);
    namelike != '' ? filterCommandAppendix.push(namelike) : '';

    //MERGED EXIT STATES
    let exitstates = this.treeFunctionsService.applyMergedExitStates(this.filterObject);
    exitstates != '' ? filterCommandAppendix.push(exitstates) : '';

    // JOB STATUS
    let jobStates = this.treeFunctionsService.applyJobStates(this.filterObject)
    jobStates != '' ? filterCommandAppendix.push(jobStates) : '';

    let filterCommandAppendixString = filterCommandAppendix.join(' AND ')


    // CONDITION
    if (this.filterObject.CONDITION_MODE != 'NONE' && this.filterObject.CONDITION != '') {
      filterCommandAppendixString += ' ' + this.filterObject.CONDITION_MODE + ' (' + this.filterObject.CONDITION + ')';
    }
    if (filterCommandAppendixString != '') {
      if (masterString) {
        if (this.tab.MODE == TabMode.MONITOR_DETAIL) {

          filterCommand += ' OR '
        } else {
          filterCommand += ' AND '
        }
      } else {
        filterCommand += ', ';
      }
    }

    filterCommand += filterCommandAppendixString;

    if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      filterCommand += ')';
    }

    //MASTER

    //ENABLED ONLY CHECK
    if (this.filterObject.ENABLE_ONLY == 'true') {
      filterCommand += ', ENABLED ONLY'
    }

    //end filter
    // console.log(filterCommand)
    // filterCommand = filterCommand;
    // console.log(filterCommand);



    filterCommand += this.treeFunctionsService.applyParameter(this.settingsObject, this.displayedColumns);
    // console.log(filterCommand)
    return filterCommand;
  }

  // FILTER OPTIONS



  //   return command;
  // }

  openSettings() {
    // open settings container
    if (this.tab.MODE == TabMode.MONITOR_JOBS) {
      let dataObj: any = this.toolboxService.deepCopy(this.settingsObject);
      dataObj.PRIVS = 'E';
      const dialogRef = this.matDialog.open(EditorformDialogComponent, {
        data: new EditorformDialogData(searchSettings_editorform, dataObj, this.tab, "List settings"),
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
        width: "1000px"
      });
      // * Note, dialogs unsubsrcibe autoamtically!
      return dialogRef.afterClosed().toPromise().then((result: any) => {
        // console.log(result);
        if (result != undefined || result != null && result) {
          // console.log(result)
          if (result.METHOD == 'apply_and_save_filter') {
            this.settingsObject = result;
            this.settingsObject.AUTO_REFRESH = parseInt(result.AUTO_REFRESH);
            this.saveBookmark()
            // this.reloadTree();
          } else if (result.METHOD == 'apply_filter') {
            this.settingsObject = result;
            this.settingsObject.AUTO_REFRESH = parseInt(result.AUTO_REFRESH);
            this.reloadTree();
          }
        }
        return 1;
      });
    } else if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      let dataObj: any = this.toolboxService.deepCopy(this.settingsObject);
      dataObj.PRIVS = 'E';
      const dialogRef = this.matDialog.open(EditorformDialogComponent, {
        data: new EditorformDialogData(detailSettings_editorform, dataObj, this.tab, "List settings"),
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
        width: "1000px"
      });
      // * Note, dialogs unsubsrcibe autoamtically!
      return dialogRef.afterClosed().toPromise().then((result: any) => {
        // console.log(result);
        if (result != undefined || result != null && result) {
          // console.log(result)
          if (result.METHOD == 'apply_and_save_filter') {
            this.settingsObject = result;
            this.saveBookmark();
            // this.reloadTree();
          } else if (result.METHOD == 'apply_filter') {
            this.settingsObject = result;
            this.reloadTree();
          }
        }
        return 1;
      });
    } else {
      return
    }

  }

  openFilter() {
    // this.filterObject = this.selectedBookmark.value.filterObject;

    if (this.tab.MODE == TabMode.MONITOR_JOBS) {
      let dataObj: any = this.toolboxService.deepCopy(this.filterObject);
      dataObj.PRIVS = 'E';
      const dialogRef = this.matDialog.open(EditorformDialogComponent, {
        data: new EditorformDialogData(searchFilter_editorform, dataObj, this.tab, "Filter options"),
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
        width: "1000px"
      });
      // * Note, dialogs unsubsrcibe autoamtically!
      return dialogRef.afterClosed().toPromise().then((result: any) => {
        // console.log(result);
        if (result != undefined || result != null && result) {
          // console.log(result)
          if (result.METHOD == 'apply_and_save_filter') {
            this.filterObject = result;
            this.saveBookmark()
            // this.reloadTree();
            this.flatExpandList = [];
          } else if (result.METHOD == 'apply_filter') {
            this.flatExpandList = [];
            this.filterObject = result;
            this.reloadTree();
          }
        } else {
          if (this.selectedBookmark.startmode == 'QUERY' && this.isLoading) {
            this.eventEmitterService.pushSnackBarMessage(["used_Bookmark_requires_a_filter"], SnackbarType.INFO)
          }
        }

        return 1;
      });
    } else if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      let dataObj: any = this.toolboxService.deepCopy(this.filterObject);
      dataObj.PRIVS = 'E';
      const dialogRef = this.matDialog.open(EditorformDialogComponent, {
        data: new EditorformDialogData(detailFilter_editorform, dataObj, this.tab, "Filter options"),
        panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
        width: "1000px"
      });
      // * Note, dialogs unsubsrcibe autoamtically!
      return dialogRef.afterClosed().toPromise().then((result: any) => {
        // console.log(result);
        if (result != undefined || result != null && result) {
          // console.log(result)
          if (result.METHOD == 'apply_and_save_filter') {
            this.filterObject = result;
            this.saveBookmark()
            // this.reloadTree();
            this.flatExpandList = [];
          } else if (result.METHOD == 'apply_filter') {
            this.filterObject = result;
            this.reloadTree();
            this.flatExpandList = [];
          }
        } else {
          if (this.selectedBookmark.startmode == 'QUERY' && this.isLoading) {
            this.eventEmitterService.pushSnackBarMessage(["used_Bookmark_requires_a_filter"], SnackbarType.INFO)
          }
        }
        return 1;
      });
    } else {
      return
    }


  }

  newBookmark() {
    const dialogRef = this.matDialog.open(NewItemDialogComponent, {
      data: this.bookmarkService.getNewBookmarkDialogData(this.selectedBookmark),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result.input) {
        // console.log(result.input)
        let bookmark: Bookmark;
        if (this.tab.MODE == TabMode.MONITOR_JOBS) {
          bookmark = new Bookmark(result.input, 'SEARCH');
        } else if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
          bookmark = new Bookmark(result.input, 'DETAIL');
        } else {
          return
        }


        bookmark.scope = result.choosedItem;

        if (this.tab.MODE == TabMode.MONITOR_JOBS) {
          let treeViewState = new SearchJobState();
          treeViewState.settingsObject = this.settingsObject;
          treeViewState.filterObject = this.filterObject;
          bookmark.value = treeViewState;

        } else if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
          let treeViewState = new RunningDetailState();

          // cast searchsettings to detailsettings is working because searchsettings also fits to detail settings (detail has less options)
          treeViewState.settingsObject = this.settingsObject;
          treeViewState.filterObject = this.filterObject;
          bookmark.value = treeViewState;

        } else {
          return
        }


        this.bookmarkService.setUserBookmark(bookmark, false).then(() => {
          this.tab.BOOKMARK = bookmark;
          this.selectBookmark(bookmark);
        });

        this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_created'], SnackbarType.INFO)
      }
    });
  }

  selectBookmark(bookmark: Bookmark) {
    this.getBookmarks();
    // if bookmark.value is empty
    if (Object.keys(bookmark.value).length === 0) {
      bookmark.value = new SearchJobState();
    }

    this.selectedBookmark = bookmark;
    this.tab.BOOKMARK = bookmark;

    this.settingsObject = this.selectedBookmark.value.settingsObject;
    this.filterObject = this.selectedBookmark.value.filterObject;

    // this.reloadTree();
    if (this.selectedBookmark.startmode == 'QUERY') {
      this.openFilter();
    } else {
      this.reloadTree();
    }
  }

  saveBookmark() {
    let treeViewState = new SearchJobState();
    treeViewState.settingsObject = this.settingsObject;
    treeViewState.filterObject = this.filterObject;

    this.selectedBookmark.value = treeViewState;
    this.bookmarkService.setUserBookmark(this.selectedBookmark, false).then(() => {
      this.eventEmitterService.pushSnackBarMessage(['bookmark_has_been_updated'], SnackbarType.INFO)
      this.selectBookmark(this.selectedBookmark)
    }, () => { });
    this.tab.BOOKMARK = this.selectedBookmark;
  }

  // descendantsAllSelected(node: masterTreeNode): boolean {
  //   const descendants = this.treeControl.getDescendants(node);
  //   const descAllSelected = descendants.length > 0 && descendants.every(child => {
  //     return this.checklistSelection.isSelected(child);
  //   });
  //   return descAllSelected;
  // }

  // /** Whether part of the descendants are selected */
  // descendantsPartiallySelected(node: masterTreeNode): boolean {
  //   const descendants = this.treeControl.getDescendants(node);
  //   const result = descendants.some(child => this.checklistSelection.isSelected(child));
  //   return result && !this.descendantsAllSelected(node);
  // }

  itemSelectionToggle(node: any): void {
    // console.log("select")
    this.checklistSelection.toggle(node);
    // this.checkAllParentsSelection(node);
  }

  itemSelectChildren(node: masterTreeNode) {
    if (node.children) {
      for (let o of node.children) {
        this.checklistSelection.select(o)
        this.itemSelectChildren(o);
      }
    }
  }

  itemDeselectChildren(node: masterTreeNode) {
    if (node.children) {
      for (let o of node.children) {
        this.checklistSelection.deselect(o);
        this.itemDeselectChildren(o)
      }
    }
  }


  msToTime(s: number): string {
    return this.treeFunctionsService.msToTime(s);
  }

  resetSelection(nodes: any[]) {
    // console.log(nodes);
    // console.log(this.checklistSelection.selected);
    let newSelected = [];
    for (let n of nodes) {
      for (let o of this.checklistSelection.selected) {
        if (n.id == o.id && this.checkBulkSelectable(n)) {
          newSelected.push(n);
          break;
        }
      }
    }
    this.checklistSelection.clear();
    for (let s of newSelected) {
      this.checklistSelection.select(s);
    }
  }

  startTimer(iconColorEncodingOnly: boolean = false) {
    this.endTimer();

    // console.log(typeof this.settingsObject.AUTO_REFRESH)
    // console.log(this.settingsObject.AUTO_REFRESH)
    if (this.settingsObject.AUTO_REFRESH > 0) {
      this.interval = setInterval(() => {
        if (this.settingsObject.AUTO_REFRESH > 0) {
          // this.reloadTree();
          iconColorEncodingOnly ? this.checkColorCodingBeforeLoading(this.tab, this.command, this.flatPathToLeverTransformer.bind(this)) : this.reloadTree();
        }
      }, this.settingsObject.AUTO_REFRESH > 0 ? this.settingsObject.AUTO_REFRESH * 1000 : 10000);
    } else {
      this.endTimer();
    }

  }

  endTimer() {
    clearInterval(this.interval);
  }



  // ------------------------------------------- submit entitys -------------------------------------

  checkBulkSelectable(node: any): boolean {
    return this.treeFunctionsService.checkBulkSelectable(node);
  }

  checkBulkOperationsAllowed(operation: string): boolean {
    // console.log(this.checklistSelection)
    return this.treeFunctionsService.checkBulkOperationsAllowed(operation, this.checklistSelection);
  }

  resumeNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.createResumeConfirmDialog(bicsuiteObject, tabInfo, false, "doResumeEntity");
      return confirmDialog.afterClosed().toPromise().then((result: any) => {
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  resumeSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createResumeConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doResumeEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }

  // suspendNode(node: masterTreeNode) {
  //   let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
  //   let bicsuiteObject = null;
  //   let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
  //   submittedEntityCrud.read(null, tabInfo).then((result: any) => {
  //     let bicsuiteObject = result.response.DATA.RECORD;
  //     let confirmDialog = submittedEntityCrud.createSuspendConfirmDialog(bicsuiteObject, tabInfo, false, "doSuspendEntity");
  //     return confirmDialog.afterClosed().toPromise().then((result: any) => {
  //       if (result) {
  //         this.reloadTree();
  //       }
  //       return 1;
  //     });
  //   });
  // }

  suspendSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createSuspendConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doSuspendEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }

  clearWarning(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.clearWarningEntity(bicsuiteObject, tabInfo).then((result: any) => {
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  cancelNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      let confirmDialog = submittedEntityCrud.createCancelConfirmDialog(bicsuiteObject, tabInfo, false, "doCancelEntity");
      return confirmDialog.afterClosed().toPromise().then((result: any) => {
        // reload only if sth was returned (not closed or ESC)
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  rerunNode(node: masterTreeNode) {
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let bicsuiteObject = null;
    let tabInfo: Tab = new Tab(node.name, node.id.toString(), "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    submittedEntityCrud.read(null, tabInfo).then((result: any) => {
      let bicsuiteObject = result.response.DATA.RECORD;
      return submittedEntityCrud.createRerunConfirmDialog(bicsuiteObject, tabInfo, false, "doRerunEntity").then((result: any) => {
        return result.afterClosed().toPromise().then((result: any) => {
          // reload only if sth was returned (not closed or ESC)
          if (result) {
            this.reloadTree();
          }
          return 1;
        });
      });
    });
  }

  rerunSelectedRecursive() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    return submittedEntityCrud.createRerunConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doRerunEntitiesRecursive").then((result: any) => {
      return result.afterClosed().toPromise().then((result: any) => {
        // reload only if sth was returned (not closed or ESC)
        if (result) {
          this.reloadTree();
        }
        return 1;
      });
    });
  }

  enableSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createEnableConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doEnableEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }

  disableSelected() {
    // console.log(this.checklistSelection.selected);
    let submittedEntityCrud: SubmittedEntityCrud = this.crudService.createOrGetCrud("SUBMITTED ENTITY") as SubmittedEntityCrud;
    let tabInfo: Tab = new Tab("", "", "SUBMITTED ENTITY", 'submitted_entity', TabMode.EDIT);
    let confirmDialog = submittedEntityCrud.createDisableConfirmDialog(this.checklistSelection.selected, tabInfo, true, "doDisableEntities");
    return confirmDialog.afterClosed().toPromise().then((result: any) => {
      // reload only if sth was returned (not closed or ESC)
      if (result) {
        this.reloadTree();
      }
      return 1;
    });
  }

  openLog(logfile: string, node: any) {
    this.toolboxService.OpenTextWindow(logfile, node);
  }




  // new tree with list

  flatdesc: any[] = [];
  flatStateList: any[] = [];
  flatLoadToken: string = '0';
  flatDataSource: any[] = [];
  flatExpandList: string[] = [];
  displayPath: boolean = false;
  // flatDataSource: any[] = [];

  // @ViewChild(VirtualScrollerComponent)
  // private virtualScroll!: VirtualScrollerComponent;


  flatPathToLeverTransformer(node: any, pathIdentifier: string, pathSplitter: string): any {

    let childrenStates = this.submitEntityService.getChildrenStates(node);
    let transformedNode: any = node;

    // remove dynamic tag before

    transformedNode.level = this.removeDynamicTag(node[pathIdentifier]).split(pathSplitter).length;
    transformedNode.name = this.flatNameTransformer(node[pathIdentifier]);
    transformedNode.path = node[pathIdentifier];
    transformedNode.namePath = this.removeDynamicTag(node[pathIdentifier]).split(pathSplitter).pop();
    transformedNode.type = node.SE_TYPE;
    transformedNode.id = node.ID;
    transformedNode.isSuspended = node.IS_SUSPENDED;
    transformedNode.parentSuspended = node.PARENT_SUSPENDED;
    transformedNode.state = node.STATE;
    transformedNode.jobState = node.STATE;
    transformedNode.parentIconColor = this.submitEntityService.getParentIconColor(node, node.STATE, childrenStates);  // icon des batch aspects
    transformedNode.jobIconColor = this.submitEntityService.getIconColor(node, node.STATE, node.SE_TYPE, node.JOB_IS_FINAL, node.IS_RESTARTABLE, node.IS_DISABLED, childrenStates);  // icon des job aspects
    transformedNode.textColor = this.submitEntityService.getTextColor(node, node.STATE, node.SE_TYPE, node.JOB_IS_FINAL, node.IS_RESTARTABLE, node.IS_DISABLED, childrenStates);
    transformedNode.displayState = this.submitEntityService.getDisplayState(node, childrenStates);   // angezeigter technischer state


    transformedNode.end = node.FINISH_TS;

    transformedNode.start = (this.treeFunctionsService.startTime(node) ? (this.toolboxService.convertTimeStamp(new Date(this.treeFunctionsService.startTime(node)).toISOString(), this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone"),
      this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat"))) : ''),

    // formatDate(new Date(), 'M/d/yy, h:mm a', 'en-US');
    transformedNode.exitstate = node.FINAL_ESD;
    if (node.STATE == 'SCHEDULED') {
      transformedNode.runtime = -1;
    }
    else {
      let start_ts = (node.type == "JOB" ? node.START_TS  : (node.FINISH_TS == null ? new Date().getTime() : node.FINISH_TS));
      let end_ts   = (node.type == "JOB" ? node.FINISH_TS : (node.FINAL_TS == null ? new Date().getTime() : node.FINAL_TS));
      transformedNode.runtime = Date.parse(end_ts) - Date.parse(start_ts)
    }

    transformedNode.final_ts = node.FINAL_TS != null ?
      this.toolboxService.convertTimeStamp(new Date(node.FINAL_TS).toISOString(), this.mainInformationService.getPrivateProperty("PRIVATE_TIME_ZONE", "defaultTimeZone"),
        this.mainInformationService.getPrivateProperty("PRIVATE_TIME_STAMP_FORMAT", "defaultTimeFormat")) : '';
    transformedNode.submit_ts = '';

    transformedNode.is_restartable = node.IS_RESTARTABLE;
    transformedNode.is_disabled = node.IS_DISABLED;
    // is_replaced =node.IS_REPLACED;
    transformedNode.is_suspended = node.IS_SUSPENDED;
    transformedNode.is_disabled = node.IS_DISABLED;
    transformedNode.is_cancelled = node.IS_CANCELLED;
    transformedNode.cnt_restartable = node.CNT_RESTARTABLE;

    // deactivate hit with detail
    transformedNode.hit = node.HIT;

    node.isVisible = true;


    node.workdir = node.WORKDIR,
      node.logUrl = this.toolboxService.makeLogPath(node.HTTPHOST, node.HTTPPORT)
    transformedNode.iconType = node.SE_TYPE.toLowerCase();
    this.totalLoadedNodeIndex++;
    return transformedNode;
  }


  flatNameTransformer(path: string): string {
    const reg = new RegExp('\\[.*?\\]');
    const found = path.match(reg);
    let replacedPath = path.replace(reg, '')
    let name = replacedPath.split('.').pop();
    name = name + (found ? found['0'] : '')
    return name;
  }

  removeDynamicTag(path: string): string {
    const reg = new RegExp('\\[.*?\\]');
    const found = path.match(reg);
    let replacedPath = path.replace(reg, '')
    // console.log(replacedPath)
    return replacedPath;
  }


  getCommand(): string {
    let command: string = '';
    if (this.tab.MODE == TabMode.MONITOR_DETAIL) {
      let jobid = (this.filterObject.JOB_ID ? this.filterObject.JOB_ID : '');
      if (!this.flatExpandList.includes(this.tab.ID)) {
        this.flatExpandList.push(this.tab.ID);
      }
      command = "LIST CONDENSED JOB " + jobid + " WITH MODE = TREE, EXPAND = " + (this.flatExpandList.length < 1 ? 'NONE' : "(" + this.flatExpandList.join(",") + ")");
    }
    if (this.tab.MODE == TabMode.MONITOR_JOBS) {
      let jobid = (this.filterObject.JOB_ID ? this.filterObject.JOB_ID : '');
      command = "LIST CONDENSED JOB " + jobid + " WITH MODE = TREE, EXPAND = " + (this.flatExpandList.length < 1 ? 'NONE' : "(" + this.flatExpandList.join(",") + ")");
    }

    // console.log(command)
    command += this.applyServerSettings();
    return command;
  }

  loadFlatTree() {
    this.isLoading = true;
    this.endTimer()
    // clearInterval(this.loadInterval);


    this.totalLoadedNodeIndex = 0;
    // if(this.loadTreePromise != undefined) {
    //   this.loadTreePromise.
    // }

    // ensured that the last init of the the Tree is gonna be executed


    this.resetDisplay();


    // console.log(command)


    let loadTokenHash = this.toolboxService.uuidv4();
    this.flatLoadToken = loadTokenHash;
    // console.log(this.loadToken)

    this.commandApiService.execute(this.getCommand()).then((result: any) => {
      // checks if the loadToken is the last loaded execution -> build Tree
      if (this.flatLoadToken == loadTokenHash) {
        // console.log("-----------------------LOAD TREE-----------------------------------")

        // console.log(result)
        this.flatStateList = [];
        this.flatdesc = result.DATA.DESC
        // let expandParents: any[] = [];
        let treeNodes: any[] = [];

        let index: number = 0;
        // reset state list
        this.globalStateList = [];
        for (let o of result.DATA.TABLE) {

          // add normalized PRIVS to nodes
          o.normalizedPrivs = this.privilegesService.normalize(o.PRIVS);
          let transformedNode = this.flatPathToLeverTransformer(o, 'HIERARCHY_PATH', ':');
          // console.log(transformedNode.level);
          treeNodes.push(transformedNode);
          transformedNode.parity = index % 2 == 0 ? 'even' : 'odd',
            transformedNode.index = index;

          // // .replace('[' + o.CHILDTAG + ']', '')
          // // console.log(o.HIERARCHY_PATH)

          transformedNode.state = transformedNode.childrenStates ? this.submitEntityService.evaluateState(transformedNode) : transformedNode.state;
          this.globalStateList.push(transformedNode.textColor);
          //   treeNodes.push(treeNode);
          // }
          index++;
        }
        // // expand every parent with children Hits
        // for (let o of expandParents) {
        //   this.expandNode(o);
        // }
        treeNodes = this.applyFrontEndSettings(treeNodes);


        // reindex after sorting
        this.reIndexFlatData(treeNodes);


        // this.dataSource.data = treeNodes;
        // this.dataSourceAfterInit.data = treeNodes;

        // // console.log(this.dataSource.data)

        // let date = new Date()
        // this.lastReload = date.getHours() + ':' + date.getMinutes() + ':' + date.getSeconds();


        // // TODO evaluation ot correct
        // console.log(this.globalStateList);
        this.tab ? this.tab.COLOR = this.treeFunctionsService.getTabColorEvaluation(this.globalStateList) : '';

        this.settingsObject.AUTO_REFRESH != 0 ? this.tab.AUTORELOAD = true : this.tab.AUTORELOAD = false;
        // this.eventEmitterService.updateTabEmitter.next(this.tab);

        this.resetSelection(treeNodes);
        this.setExpand(treeNodes);

        this.flatDataSource = treeNodes;

        this.endTimer();
        this.startTimer();
        this.isLoading = false;
        // this.renderData();
        // console.log(this.flatDataSource)

      } else {
        // console.log('...Tree not loaded, Token invalid')
      }

    }, (rejection: any) => {
      // do nothing
      this.isLoading = false;
    });

    // let datasource: masterTreeNode[] = this.commandApiService.executeDelegate(command, (result: any) => {

    // });
  }

  checkColorCodingBeforeLoading(tab: Tab, command: string, transformer: Function) {
    // console.log(this.command)

    // get bookmarks to correctly show the colors;
    this.getBookmarks();
    // check if the tab brings an default bookmark
    this.setTabBookmark(false);
    // console.log(this.tab.MODE);
    // console.log(this.tab.NAME);
    // console.log(this.settingsObject.AUTO_REFRESH)
    this.commandApiService.execute(this.getCommand()).then((result: any) => {
      let globalStateList = [];
      for (let o of result.DATA.TABLE) {

        let parentPath = o.HIERARCHY_PATH.split('.')
        parentPath.pop()
        parentPath = parentPath.join('.');
        let treeNode = transformer(o, 'HIERARCHY_PATH', ':')
        treeNode.state = treeNode.childrenStates ? this.submitEntityService.evaluateState(treeNode) : treeNode.state;
        globalStateList.push(treeNode.textColor);
      }
      // console.log(globalStateList);
      let color = this.treeFunctionsService.getTabColorEvaluation(globalStateList)
      tab ? tab.COLOR = color : '';

      this.endTimer();
      this.startTimer(true);
    });
  }

  removeChildren(treenodes: any[], path: string) {

  }

  setExpand(treeNodes: any[]) {

    for (let i = 0; i < treeNodes.length; i++) {


      if (treeNodes[i].CHILDREN > 0) {
        treeNodes[i].partCollapsed = false;
        // check children are already in the table
        let foundChildren = 0;
        let foundHits = 0;
        innerloop: for (let j = i + 1; j < treeNodes.length; j++) {
          if (String(treeNodes[j].path).startsWith(treeNodes[i].path + ':')) {
            if ((treeNodes[j].level - treeNodes[i].level) == 1) {
              foundChildren++;
            }
            if (treeNodes[j].HIT == 'H') {
              foundHits++;
            }
          } else {
            break innerloop;
          }
        }
        // console.log(foundChildren)
        if (foundChildren == treeNodes[i].CHILDREN) {
          // console.log()
          treeNodes[i].expanded = true;
        }
        // if (foundChildren < treeNodes[i].CHILDREN && foundChildren != 0) {
        //   treeNodes[i].partCollapsed = true;
        // }
        if (foundHits > 0) {
          treeNodes[i].childHits = true;
        }
      }
    }
  }


  // loadFlatChildren(node: any, index: number) {


  //   this.isLoading = true;


  //   let command = 'LIST CONDENSED JOB ' + node.id + ' WITH EXPAND = ALL';
  //   command += this.applyServerSettings()

  //   this.commandApiService.executeDelegate(command, (result: any) => {
  //     if (result.hasOwnProperty('ERROR')) {
  //       console.warn(result);
  //       //tell User that something went wrong
  //       this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
  //       return;
  //     } else {
  //       let treenodeList: any[] = [];
  //       // console.log
  //       for (let i = 1; i < result.DATA.TABLE.length; i++) {
  //         let transformedNode = this.flatPathToLeverTransformer(result.DATA.TABLE[i], 'HIERARCHY_PATH', ':')
  //         console.log(transformedNode);

  //         treenodeList.push(transformedNode);
  //       }
  //       // this.treeFunctionsService.order(treenodeList, this.so);
  //       this.setExpand(treenodeList);
  //       // treenodeList = this.applyFrontEndSettings(treenodeList);

  //       this.flatDataSource.splice(index + 1, 0, ...treenodeList);

  //       this.isLoading = false;
  //       this.virtualScroll.refresh();
  //       this.reIndexFlatData(this.flatDataSource);
  //       // this.renderChildData();
  //       return;
  //     }
  //   });
  // }

  reIndexFlatData(data: any[]) {
    let index = 0;
    let dataLength = data.length;
    let maxLevel = 0;
    for (let i = 0; i < data.length; i++) {
      let o = data[i];
      o.index = index;
      o.parity = index % 2 == 0 ? 'even' : 'odd';
      if (o.level > maxLevel) { 
        maxLevel = o.level;
      }
      index++;
    }
    let isLastArray : boolean[] = [];
    // since we are running backward we initialize it with true
    for (let i = 0; i < maxLevel; i ++) {
      isLastArray[i] = true;
      // console.log("Init:isLastArray[" + i + "] = true")
    }
    let lastLevel = 0;
    for (let i = data.length - 1; i >= 0; i--) {
      let o = data[i];
      let level = o.level - 1;
      // console.log("data [" + i + "]:level = " + level + "id = " + o.ID)
      if (level == lastLevel) {
        isLastArray[level] = false;
        // console.log("isLastArray[" + level + "] = false")
      }
      else {
        if (level < lastLevel) { // only can be level = lastLevel - 1 because we always hava a parent 
          // reset isLastArray for the higher level to true
          isLastArray[lastLevel] = true;
          // console.log("isLastArray[" + level + "] = true")
        }
        else {
            isLastArray[lastLevel] = false;
        }
      }
      lastLevel = level;
      o.isLastArray = [...isLastArray];
      // console.log(o.isLastArray)
    }
  }

  toggleFlatNode(node: any) {

    // console.log(node.index)

    if (node.expanded) {
      // console.log("collapse toggle")
      // collapse

      // search all children and make them invisible

      let removedItems = 0;
      let keepHits: any[] = [];
      for (let i = parseInt(node.index) + 1; i < this.flatDataSource.length; i++) {

        if (String(this.flatDataSource[i].path).startsWith(node.path + ':')) {
          if (this.flatDataSource[i].HIT == 'H' || this.flatDataSource[i].childHits) {
            keepHits.push(this.flatDataSource[i]);
          }
          if (this.flatDataSource[i].childHits) {
            this.flatDataSource[i].expanded = false;
            const index = this.flatExpandList.indexOf(this.flatDataSource[i].id);
            // console.log(node)
            if (index > -1) { // only splice array when item is found
              this.flatExpandList.splice(index, 1); // 2nd parameter means remove one item only
            }
          }
          removedItems++;
        } else {
          break;
        }
      }

      // remove items from the array
      // console.log(keepHits)
      this.flatDataSource.splice(node.index + 1, removedItems)
      this.flatDataSource.splice(node.index + 1, 0, ...keepHits)
      if (keepHits.length > 0) {
        node.partCollapsed = true;
      }
      this.reIndexFlatData(this.flatDataSource);
      node.expanded = false;

      // remove id from expandlist
      const index = this.flatExpandList.indexOf(node.id);
      // console.log(node)
      if (index > -1) { // only splice array when item is found
        this.flatExpandList.splice(index, 1); // 2nd parameter means remove one item only
      }
    } else if (!node.expanded) {
      // console.log("expand toggle")
      // expand
      // search all children and make then visible

      node.expanded = true;
      this.flatExpandList.push(node.id);

      this.loadFlatTree();
    }
  }

  toggleFlatSelectAll(event: any) {
    // console.log(event)
    for (let o of this.flatDataSource) {
      if (this.checkBulkSelectable(o)) {
        event.checked ? this.checklistSelection.select(o) : this.checklistSelection.deselect(o);
      }
    }
  }


}
