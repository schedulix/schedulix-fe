import { Injectable } from '@angular/core';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { SnackbarType } from '../../global-shared/components/snackbar/snackbar-data';
import { ToolboxService } from "../../../services/toolbox.service";

@Injectable({
  providedIn: 'root'
})
export class ClipboardService {

  private clipBoardItems: SdmsClipboardItem[] = [];
  notifyDataChangeEmitter: any;
  toolbox: ToolboxService;

  constructor(private eventEmitterService: EventEmitterService) {
    this.toolbox = new ToolboxService();
    this.notifyDataChangeEmitter = this.eventEmitterService.notifyDataChangeEmitter.subscribe((message: any) => {
      this.processNotifyDataChange(message);
    });
  }

  handleDropForHierachyObject(clipboardItem: SdmsClipboardItem, message: any) {
    let newClipboardItems: SdmsClipboardItem[] = [];
    for (let item of this.clipBoardItems) {
      let remove: boolean = false;
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem) {
        // clipboard items comming from content or tree have the full path in NAME
        // we add a '.' to have startWith also be true for equals
        if ((item.data.NAME + '.').startsWith(message.NAME + '.')) {
          remove = true;
        }
      }
      if (!remove) {
        newClipboardItems.push(item);
      }
    }
    this.clipBoardItems = newClipboardItems;
  }

  handleRenameForHierachyObject(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem) {
        // clipboard items comming from content or tree have the full path in NAME
        // we add a '.' to have startWith also be true for equals
        // console.log(message)
        // console.log(item)
        if ((item.data.NAME + '.').startsWith(message.OLD_NAME + '.')) {
          // since replace only replaces once for a string pattern and we checkt the path with startWith, this is ok
          // in clipboardItems NAME is full pathname
          item.data.NAME = item.data.NAME.replace(message.OLD_NAME, message.NEW_NAME);
          // item.sourceObject.QUOTED_FULLNAME = item.sourceObject.QUOTED_FULLNAME.replace(message.OLD_NAME, message.NEW_NAME);
        }
      }
    }
  }

  handleDropForHierachyObjectForParameter(clipboardItem: SdmsClipboardItem, message: any) {
    let newClipboardItems: SdmsClipboardItem[] = [];
    for (let item of this.clipBoardItems) {
      let remove: boolean = false;
      if (item === clipboardItem && item.data.REFERENCE && ["REFERENCE", "CHILDREFERENCE", "RESOURCEREFERENCE"].includes(item.data.TYPE)) {
        let l = item.data.REFERENCE.split(' ');
        if ((l[0] + '.').startsWith(message.OLD_NAME + '.')) {
          remove = true;
        }
      }
      if (!remove) {
        newClipboardItems.push(item);
      }
    }
    this.clipBoardItems = newClipboardItems;
  }

  handleRenameForHierachyObjectForParameter(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && item.data.REFERENCE && ["REFERENCE", "CHILDREFERENCE", "RESOURCEREFERENCE"].includes(item.data.TYPE)) {
        let l = item.data.REFERENCE.split(' ');
        if ((l[0] + '.').startsWith(message.OLD_NAME + '.')) {
          item.data.REFERENCE = l[0].replace(message.OLD_NAME, message.NEW_NAME) + ' ' + l[1];
        }
      }
      // TODO: handle PARENTNAME fields, do we have to handle cut/paste with intermediate rename/create/drop/folder ?
      // if ((item.data.PARENTNAME + '.').startsWith(message.OLD_NAME + '.')) {
      //   item.data.PARENTNAME = item.data.PARENTNAME.replace(message.OLD_NAME, message.NEW_NAME);
      // }
    }
  }

  handleDropSeForJobChild(clipboardItem: SdmsClipboardItem, message: any) {
    let newClipboardItems: SdmsClipboardItem[] = [];
    for (let item of this.clipBoardItems) {
      let remove: boolean = false;
      if (item === clipboardItem && (item.data.CHILDNAME + '.').startsWith(message.OLD_NAME + '.')) {
        remove = true;
      }
      if (!remove) {
        newClipboardItems.push(item);
      }
    }
    this.clipBoardItems = newClipboardItems;
  }

  handleRenameSeForJobChild(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.CHILDNAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.CHILDNAME = item.data.CHILDNAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleDropIntervalForJobChild(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      if (item === clipboardItem && item.data.INT_NAME == message.OLD_NAME) {
        item.data.INT_NAME = "";
      }
    }
  }

  handleRenameIntervalForJobChild(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && item.data.INT_NAME == message.OLD_NAME) {
        item.data.INT_NAME = message.NEW_NAME;
      }
    }
  }

  handleDropEstForJobChild(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      if (item === clipboardItem && item.data.EST_NAME == message.OLD_NAME) {
        item.data.EST_NAME = "";
      }
    }
  }

  handleRenameEstForJobChild(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && item.data.EST_NAME == message.OLD_NAME) {
        item.data.EST_NAME = message.NEW_NAME;
      }
    }
  }

  handleDropSeForJobDependency(clipboardItem: SdmsClipboardItem, message: any) {
    let newClipboardItems: SdmsClipboardItem[] = [];
    for (let item of this.clipBoardItems) {
      let remove: boolean = false;
      if (item === clipboardItem && (item.data.REQUIREDNAME + '.').startsWith(message.OLD_NAME + '.')) {
        remove = true;
      }
      if (!remove) {
        newClipboardItems.push(item);
      }
    }
    this.clipBoardItems = newClipboardItems;
  }

  handleRenameSeForJobDependency(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.REQUIREDNAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.REQUIREDNAME = item.data.REQUIREDNAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleDropEsdForJobDependency(clipboardItem: SdmsClipboardItem, message: any) {
    // handler is called for each clipboard item, so only handle current item
    for (let item of this.clipBoardItems) {
      if (item === clipboardItem) {
        let newStates = [];
        let states = item.data.STATES.split(',');
        for (let state of states) {
          if (state != message.OLD_NAME) {
            newStates.push(state);
          }
        }
        item.data.STATES = newStates.join(',');
      }
    }
  }

  handleRenameEsdForJobDependency(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem) {
        let newStates = [];
        let states = item.data.STATES.split(',');
        for (let state of states) {
          if (state == message.OLD_NAME) {
            state = message.NEW_NAME;
          }
          newStates.push(state);
        }
        item.data.STATES = newStates.join(',');
      }
    }
  }

  handleRenameNrForRequiredResource(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.RRESOURCE_NAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.RRESOURCE_NAME = item.data.RRESOURCE_NAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleRenameRsmForRequiredResource(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && item.data.RESOURCE_STATE_MAPPING == message.OLD_NAME) {
        item.data.RESOURCE_STATE_MAPPING = message.NEW_NAME;
      }
    }
  }

  handleRenameRsdForRequiredResource(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem) {
        let newStates = [];
        let states = item.data.STATES.split(',');
        for (let state of states) {
          if (state == message.OLD_NAME) {
            state = message.NEW_NAME;
          }
          newStates.push(state);
        }
        item.data.STATES = newStates.join(',');
      }
    }
  }

  handleRenameSeForRequiredResource(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.STICKY_PARENT + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.STICKY_PARENT = item.data.STICKY_PARENT.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleRenameEsdForTrigger(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem) {
        let newStates = [];
        let states = item.data.STATES.split(',');
        for (let state of states) {
          if (state == message.OLD_NAME) {
            state = message.NEW_NAME;
          }
          newStates.push(state);
        }
        item.data.STATES = newStates.join(',');
      }
    }
  }

  handleRenameSeForTrigger(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.SUBMIT_NAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.SUBMIT_NAME = item.data.SUBMIT_NAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
      if (item === clipboardItem && (item.data.OBJECT_NAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.OBJECT_NAME = item.data.OBJECT_NAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleRenameNamedResourceForResource(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.NAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.NAME = item.data.NAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleRenameRsdForResource(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && item.data.STATE == message.OLD_NAME) {
        item.data.STATE = message.NEW_NAME;
      }
    }
  }

  handleRenameFolderForFolderParameter(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.DEFINITION + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.DEFINITION = item.data.DEFINITION.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleRenameScopeForScopeParameter(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.DEFINITION + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.DEFINITION = item.data.DEFINITION.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  handleRenameRsdForTrigger(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
        if (item.data.STATES) {
            let rules = item.data.STATES.split(',');
            let newRules = [];
            for (let rule of rules) {
                rule = rule.trim();
                let states = rule.split("->");
                let newStates = [];
                for (let state of states) {
                    state = state.trim();
                    if (state == message.OLD_NAME) {
                        state = message.NEW_NAME;
                    }
                    newStates.push(state);
                }
                newRules.push(newStates.join("->"));
            }
            item.data.STATES = newRules.join(',');
        }
    }
    return false;
  }

  handleGenericDrop(clipboardItem: SdmsClipboardItem, message: any) {
    let newClipboardItems: SdmsClipboardItem[] = [];
    console.log(message)
    for (let item of this.clipBoardItems) {
      console.log(item)
      let remove: boolean = false;
      if (item === clipboardItem && (item.data.NAME + '.').startsWith(message.NAME + '.')) {
        remove = true;
      }
      if (!remove) {
        newClipboardItems.push(item);
      }
    }
    this.clipBoardItems = newClipboardItems;
    console.log(this.clipBoardItems)
  }

  handleGenericRename(clipboardItem: SdmsClipboardItem, message: any) {
    for (let item of this.clipBoardItems) {
      // handler is called for each clipboard item, so only handle current item
      if (item === clipboardItem && (item.data.NAME + '.').startsWith(message.OLD_NAME + '.')) {
        item.data.NAME = item.data.NAME.replace(message.OLD_NAME, message.NEW_NAME);
      }
    }
  }

  getChangeConfigsForClipboardItemType(clipBoardItemType: SdmsClipboardType) {
    switch (clipBoardItemType) {
      case SdmsClipboardType['EXIT STATE DEFINITION']:
      case SdmsClipboardType['EXIT STATE MAPPING']:
      case SdmsClipboardType['EXIT STATE PROFILE']:
      case SdmsClipboardType['RESOURCE STATE DEFINITION']:
      case SdmsClipboardType['RESOURCE STATE MAPPING']:
      case SdmsClipboardType['RESORUCE STATE PROFLIE']:
      case SdmsClipboardType.ENVIRONMENT:
      case SdmsClipboardType.FOOTPRINT:
      case SdmsClipboardType.USER:
      case SdmsClipboardType.GROUP:
      case SdmsClipboardType['NICE PROFILE']:
      case SdmsClipboardType.INTERVALS:
        return [{
          // Generic renames and drops for objects with NAME
          CHANGE_MESSAGE_TYPES: ["ESD", "ESM", "ESP", "RSD", "RSP", "RSM", "ENV", "FP", "USER", "GROUP", "NP", "INTERVAL"],
          OP_METHODS: {
            DROP: this.handleGenericDrop,
            RENAME: this.handleGenericRename
          }
        }
        ]        
      case SdmsClipboardType.FOLDER:
      case SdmsClipboardType.JOB:
      case SdmsClipboardType.BATCH:
      case SdmsClipboardType.MILESTONE:
      case SdmsClipboardType.CATEGORY:
      case SdmsClipboardType.STATIC:
      case SdmsClipboardType.SYSTEM:
      case SdmsClipboardType.SYNCHRONIZING:
      case SdmsClipboardType.POOL:
      case SdmsClipboardType.SCOPE:
      case SdmsClipboardType.SERVER:
        return [{
          // JOB_DEFINTION is the message type for JOB, BATCH and MILESTONE
          // NR is message type for STATIC, SYSTEM, SYNCHRONIZING and POOL
          // SCOPE is message type for SCOPE and SERVER
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION", "CATEGORY", "NR", "SCOPE"],
          OP_METHODS: {
            DROP: this.handleDropForHierachyObject,
            RENAME: this.handleRenameForHierachyObject
          }
        }
        ]
      case SdmsClipboardType.JOB_PARAMETER:
        return [{
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION", "CATEGORY", "NR"],
          OP_METHODS: {
            DROP: this.handleDropForHierachyObjectForParameter,
            RENAME: this.handleRenameForHierachyObjectForParameter
          }
        }
        ]
      case SdmsClipboardType.JOB_CHILD:
        return [{
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION"],
          OP_METHODS: {
            DROP: this.handleDropSeForJobChild,
            RENAME: this.handleRenameSeForJobChild
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["INTERVAL"],
          OP_METHODS: {
            DROP: this.handleDropIntervalForJobChild,
            RENAME: this.handleRenameIntervalForJobChild
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["EST"],
          OP_METHODS: {
            DROP: this.handleDropEstForJobChild,
            RENAME: this.handleRenameEstForJobChild
          }
        }
        ]
      case SdmsClipboardType.JOB_DEPENDENCY:
        return [{
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION"],
          OP_METHODS: {
            DROP: this.handleDropSeForJobDependency,
            RENAME: this.handleRenameSeForJobDependency
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["ESD"],
          OP_METHODS: {
            DROP: this.handleDropEsdForJobDependency,
            RENAME: this.handleRenameEsdForJobDependency
          }
        }
        ]
      case SdmsClipboardType.REQUIRED_RESOURCE:
        return [{
          CHANGE_MESSAGE_TYPES: ["CATEGORY", "NAMED_RESOURCE"],
          OP_METHODS: {
            // No DROP because drop named resource not allowed if used as required resource
            RENAME: this.handleRenameNrForRequiredResource
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["RSM"],
          OP_METHODS: {
            // No DROP because drop resource state mapping not allowed if used in resource requirement
            RENAME: this.handleRenameRsmForRequiredResource
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["RSD"],
          OP_METHODS: {
            // No DROP because drop resource state definition not allowed if used in resource requirement
            RENAME: this.handleRenameRsdForRequiredResource
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION"],
          OP_METHODS: {
            // No DROP because drop se not allowed if used as sticky parent in resource requirement
            RENAME: this.handleRenameSeForRequiredResource
          }
        }
        ]
      case SdmsClipboardType.JOB_TRIGGER:
        return [{
          CHANGE_MESSAGE_TYPES: ["ESD"],
          OP_METHODS: {
            // No DROP because drop exit state definition not allowed if used in trigger states
            RENAME: this.handleRenameEsdForTrigger
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION"],
          OP_METHODS: {
            // No DROP because drop se not allowed if used as sticky parent in resource requirement
            RENAME: this.handleRenameSeForTrigger
          }
        }
        ]
      case SdmsClipboardType.RESOURCE:
        return [{
          CHANGE_MESSAGE_TYPES: ["CATEGORY", "NR"],
          OP_METHODS: {
            // No DROP because drop exit state definition not allowed if used in trigger states
            RENAME: this.handleRenameNamedResourceForResource
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["RSD"],
          OP_METHODS: {
            // No DROP because drop resource state definition not allowed if used in resource requirement
            RENAME: this.handleRenameRsdForResource
          }
        }
        ]
      case SdmsClipboardType.FOLDER_PARAMETER:
        return [{
          CHANGE_MESSAGE_TYPES: ["FOLDER"],
          OP_METHODS: {
            // No DROP because drop exit state definition not allowed if used in trigger states
            RENAME: this.handleRenameFolderForFolderParameter
          }
        }
        ]
      case SdmsClipboardType.SCOPE_PARAMETER:
        return [{
          CHANGE_MESSAGE_TYPES: ["SCOPE"],
          OP_METHODS: {
            // No DROP because drop exit state definition not allowed if used in trigger states
            RENAME: this.handleRenameScopeForScopeParameter
          }
        }
        ]
      case SdmsClipboardType.RESOURCE_TRIGGER:
        return [{
          CHANGE_MESSAGE_TYPES: ["RSD"],
          OP_METHODS: {
            // No DROP because drop exit state definition not allowed if used in trigger states
            RENAME: this.handleRenameRsdForTrigger
          }
        },
        {
          CHANGE_MESSAGE_TYPES: ["FOLDER", "JOB_DEFINITION", "SCOPE"],
          OP_METHODS: {
            // No DROP because drop se not allowed if used as sticky parent in resource requirement
            RENAME: this.handleRenameSeForTrigger
          }
        }
        ]
      default:
        return null;
    }
  }

  getCutIds(): string[] {
    let ids: string[] = [];
    for (let item of this.clipBoardItems) {
      if (item.mode == SdmsClipboardMode.CUT) {
        ids.push(item.data.ID);
      }
    };
    return ids;
  }

  printClipboardItems() {
    for (let item of this.clipBoardItems) console.log(
      "itemType = " + item.itemType + ", item.data.TYPE = " + item.data.TYPE + ", item.data.NAME = " + item.data.NAME
    );
  }

  processNotifyDataChange(message: any) {
    interface IIndexable {
      [key: string]: any;
    };
    // console.log("processNotifyDataChange:");
    // console.log("--- message:")
    // console.log(message);
    // console.log("--- clipBoardItems before:");
    // this.printClipboardItems();
    for (let clipboardItem of this.clipBoardItems) {
      // console.log(clipboardItem);
      let changeConfigs = this.getChangeConfigsForClipboardItemType(clipboardItem.itemType);
      // console.log(changeConfigs);
      if (changeConfigs) {
        for (let changeConfig of changeConfigs) {
          if (changeConfig.CHANGE_MESSAGE_TYPES.includes(message.TYPE)) {
            let handler: any = (changeConfig.OP_METHODS as IIndexable)[message.OP];
            if (handler) {
              handler = handler.bind(this);
              handler(clipboardItem, message);
            }
          }
        }
      }
    }
    // console.log("--- clipBoardItems after:");
    // this.printClipboardItems();
  }

  getNotifyMessageTypeForItemType(itemType: SdmsClipboardType): string {
    let messageType = "";
    switch (itemType) {
      case SdmsClipboardType.FOLDER:
      case SdmsClipboardType.JOB:
      case SdmsClipboardType.BATCH:
      case SdmsClipboardType.MILESTONE:
        messageType = "FOLDER";
        break;
      case SdmsClipboardType.CATEGORY:
      case SdmsClipboardType.STATIC:
      case SdmsClipboardType.SYSTEM:
      case SdmsClipboardType.SYNCHRONIZING:
      case SdmsClipboardType.POOL:
        messageType = "CATEGORY";
        break;
      case SdmsClipboardType.SCOPE:
      case SdmsClipboardType.SERVER:
        messageType = "SCOPE";
        break;
    }
    return messageType;
  }

  public addItems(sourceType: SdmsClipboardType, itemArray: any[], sourcetab: Tab, sourceObject: any, mode: SdmsClipboardMode, itemType: SdmsClipboardType) {

    // IMPORTANT: all items to add must map to the same messageType
    //            since we at the moment we clear the clipboard indpendently from the clearClipoards paramater also
    //            all items currentl in the clipboard have to map to the same messageType
    if (itemArray.length == 0) {
      return;
    }
    // console.log(itemArray);
    let messageType = "";
    let notifyNavigation: boolean = false;
    if (this.clipBoardItems.length > 0) {
      // clear clipboard
      messageType = this.getNotifyMessageTypeForItemType(this.clipBoardItems[0].itemType);
      let sourcePathes: any[] = [];
      for (let item of this.clipBoardItems) {
        if (item.mode == SdmsClipboardMode.CUT) {
          if (item.data.fe_flagged_to_cut) {
            item.data.fe_flagged_to_cut = false;
            notifyNavigation = true;
          }
        }
      }
      // notifies on cleared clipboard items for open tabs with content
      if (messageType != "") {
        for (let sourcePath of sourcePathes) {
          this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: messageType, OP: "CREATE", NAME: sourcePath + ".DUMMY" });
        }
      }
      this.clipBoardItems = [];
    }
    // insert new items or set mode of existing items
    for (let item of itemArray) {
      let newClipboardItem: SdmsClipboardItem = new SdmsClipboardItem();
      if (mode == SdmsClipboardMode.COPY) {
        // do deepcopy item on COPY because we would paste changes made to the source made AFTER copy !!!
        newClipboardItem.data = JSON.parse(JSON.stringify(item));
      }
      else {
        // do not deepcopy item on CUT because we have to manage fe_flagged_to_cut !!!!
        newClipboardItem.data = item;
        notifyNavigation = true;
      }
      // todo evaluate correct Types
      if (itemType == SdmsClipboardType.FROM_ITEM_TYPE) {
        newClipboardItem.itemType = SdmsClipboardType[item.TYPE as keyof typeof SdmsClipboardType];
      }
      else if (itemType == SdmsClipboardType.FROM_ITEM_USAGE) {
        newClipboardItem.itemType = SdmsClipboardType[item.USAGE as keyof typeof SdmsClipboardType];
      }
      else {
        newClipboardItem.itemType = itemType;
      }
      newClipboardItem.mode = mode;
      newClipboardItem.sourceTabInfo = sourcetab;
      newClipboardItem.sourceObject = sourceObject;
      newClipboardItem.sourceId = sourceObject.ID;
      newClipboardItem.sourceType = SdmsClipboardType[sourcetab.TYPE as keyof typeof SdmsClipboardType];
      this.clipBoardItems.push(newClipboardItem)
    }
    this.eventEmitterService.clipboardEmitter.next(sourcetab);
    if (itemArray.length > 1) {
      this.eventEmitterService.pushSnackBarMessage([itemArray.length + ' items have been added to the clipboard'], SnackbarType.INFO);
    } else {
      this.eventEmitterService.pushSnackBarMessage(['Item has been added to the clipboard'], SnackbarType.INFO);
    }
    if (notifyNavigation) {
      this.eventEmitterService.notifyDataChangeEmitter.next({ TYPE: messageType, OP: "CLIPBOARD_CHANGE" });
    }
  }

  public getItems(clipboardTypes: SdmsClipboardType[]): any[] {
    let result: any[] = [];
    // search items of type
    for (let clipboardType of clipboardTypes) {
      for (let clipBoardItem of this.clipBoardItems) {
        if (clipBoardItem.itemType == clipboardType) {
          result.push(clipBoardItem);
        }
      }
    }
    return result;
  }

  public checkClipboardIsSingleItem(): boolean {
    // console.log(this.clipBoardItems.length)
    return (this.clipBoardItems.length > 0 && this.clipBoardItems.length < 2)
  }

  public checkItemTypesPresent(clipboardTypes: SdmsClipboardType[], sourceId: string, mode?: SdmsClipboardMode) {
    let present: boolean = false;
    for (let clipboardType of clipboardTypes) {
      for (let item of this.clipBoardItems) {
        if ((item.itemType == clipboardType) && (item.sourceId != sourceId || item.mode == SdmsClipboardMode.CUT)) {
          present = true;
          break;
        }
      }
    }
    return present;
  }
}

class SdmsClipboardItem {
  itemType: SdmsClipboardType;
  data: any;
  mode: SdmsClipboardMode;
  sourceTabInfo: Tab;
  sourceObject: any;
  sourceId: string;
  sourceType: SdmsClipboardType;

  constructor() {
    this.itemType = SdmsClipboardType.UNTYPED;
    this.data = {}
    this.mode = SdmsClipboardMode.COPY
    this.sourceTabInfo = new Tab("", "", "", "", TabMode.EDIT);
    this.sourceObject = {};
    this.sourceId = '';
    this.sourceType = SdmsClipboardType.UNTYPED;
  }
}

export enum SdmsClipboardType {
    UNTYPED,

    // Tree types
    FOLDER,
    BATCH,
    JOB,
    MILESTONE,


    SCOPE,
    SERVER,

    CATEGORY,
    SYNCHRONIZING,
    POOL,
    STATIC,
    SYSTEM,

    // list types
    'EXIT STATE DEFINITION',
    'EXIT STATE MAPPING',
    'EXIT STATE TRANSLATION',
    'EXIT STATE PROFILE',
    'RESOURCE STATE DEFINITION',
    'RESOURCE STATE MAPPING',
    'RESORUCE STATE PROFLIE',
    ENVIRONMENT,
    FOOTPRINT,
    USER,
    GROUP,
    'NICE PROFILE',
    INTERVALS,

    JOB_PARAMETER,
    JOB_CHILD,
    FROM_ITEM_TYPE,
    FROM_ITEM_USAGE,
    JOB_DEPENDENCY,
    REQUIRED_RESOURCE,
    JOB_TRIGGER,
    RESOURCE,
    FOLDER_PARAMETER,
    SCOPE_PARAMETER,
    RESOURCE_TRIGGER,
    NR_PARAMETER
}

export enum SdmsClipboardMode {
  CUT,
  COPY
}
