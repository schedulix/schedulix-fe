import { Injectable } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { EditorformDialogComponent, EditorformDialogData } from '../components/form-generator/editorform-dialog/editorform-dialog.component';

import submitDialog_editorform from '.././../../json/editorforms/submitDialog.json';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { SnackbarType } from '../../global-shared/components/snackbar/snackbar-data';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';

@Injectable({
  providedIn: 'root'
})
export class FormButtonsService {

  constructor(private dialog: MatDialog,
    private commandApiService: CommandApiService,
    private eventEmitterService: EventEmitterService,
    private customPropertiesService: CustomPropertiesService) { }


  render(isDisabled: boolean, isAvailable: boolean, bicsuiteObject: any, btnInfo: any, tabInfo: Tab) {
    if (btnInfo.VALUE == 'ENABLE_ENTITY') {
      if (bicsuiteObject.STATE != 'DEPENDENCY_WAIT' ||
        bicsuiteObject.IS_DISABLED == 'false' ||
        bicsuiteObject.RERUN_SEQ != '0' ||
        bicsuiteObject.CNT_RUNNABLE != '0' ||
        bicsuiteObject.CNT_STARTING != '0' ||
        bicsuiteObject.CNT_STARTED != '0' ||
        bicsuiteObject.CNT_RUNNING != '0' ||
        bicsuiteObject.CNT_TO_KILL != '0' ||
        bicsuiteObject.CNT_KILLED != '0' ||
        bicsuiteObject.CNT_FINISHED != '0' ||
        bicsuiteObject.CNT_FINAL != '0' ||
        bicsuiteObject.CNT_BROKEN_ACTIVE != '0' ||
        bicsuiteObject.CNT_BROKEN_FINISHED != '0' ||
        bicsuiteObject.CNT_ERROR != '0' ||
        bicsuiteObject.CNT_UNREACHABLE != '0' ||
        bicsuiteObject.CNT_RESTARTABLE != '0' ||
        bicsuiteObject.CNT_PENDING != '0') {
        isAvailable = false
      }
    }
    if (btnInfo.VALUE == 'DISABLE_ENTITY') {
      if (bicsuiteObject.STATE != 'DEPENDENCY_WAIT' ||
        bicsuiteObject.IS_DISABLED == 'true' ||
        bicsuiteObject.IS_PARENT_DISABLED == 'true' ||
        bicsuiteObject.RERUN_SEQ != '0' ||
        bicsuiteObject.CNT_RUNNABLE != '0' ||
        bicsuiteObject.CNT_STARTING != '0' ||
        bicsuiteObject.CNT_STARTED != '0' ||
        bicsuiteObject.CNT_RUNNING != '0' ||
        bicsuiteObject.CNT_TO_KILL != '0' ||
        bicsuiteObject.CNT_KILLED != '0' ||
        bicsuiteObject.CNT_FINISHED != '0' ||
        bicsuiteObject.CNT_FINAL != '0' ||
        bicsuiteObject.CNT_BROKEN_ACTIVE != '0' ||
        bicsuiteObject.CNT_BROKEN_FINISHED != '0' ||
        bicsuiteObject.CNT_ERROR != '0' ||
        bicsuiteObject.CNT_UNREACHABLE != '0' ||
        bicsuiteObject.CNT_RESTARTABLE != '0' ||
        bicsuiteObject.CNT_PENDING != '0') {
        isAvailable = false
      }
    }
    if (btnInfo.VALUE == 'CHANGE_ENITIY_STATE') {
      if (bicsuiteObject.JOB_IS_RESTARTABLE != 'true' ||
        (bicsuiteObject.SE_TYPE == 'JOB' && bicsuiteObject.STATE == 'FINISHED' && bicsuiteObject.JOB_IS_FINAL == 'false') ||
        (bicsuiteObject.SE_TYPE == 'JOB' && ['SUSPEND', 'ADMINSUSPEND'].includes(bicsuiteObject.IS_SUSPENDED) && !['STARTING', 'STARTED', 'RUNNING', 'TO_KILL', 'KILLED', 'BROKEN_ACTIVE'].includes(bicsuiteObject.STATE))
      ) {
        isAvailable = false
      }
    }

    if (btnInfo.VALUE == 'KILL_ENTITY') {
      if (bicsuiteObject.STATE == 'RUNNING' && bicsuiteObject.SE_KILL_PROGRAM != 'NONE' && bicsuiteObject.SE_KILL_PROGRAM != '') {
        isAvailable = false
      }
    }
    if (btnInfo.VALUE == 'CANCEL_ENTITY') {
      if (!['CANCELLED', 'FINAL'].includes(bicsuiteObject.STATE) && bicsuiteObject.IS_CANCELLED != 'true') {
        isAvailable = false
      }
    }
    if (btnInfo.VALUE == 'RERUN_ENTITY_CHILDREN') {
      if (bicsuiteObject.CNT_RESTARTABLE != '0' || bicsuiteObject.CNT_ERROR != '0' || bicsuiteObject.CNT_BROKEN_FINISHED != '0') {
      
      } else {
        isAvailable = false
      }
    }
    if (btnInfo.VALUE == 'RERUN_ENTITY') {
      if (bicsuiteObject.JOB_IS_RESTARTABLE == 'true') {
      } else {
        isAvailable = false
      }
    }
    if (btnInfo.VALUE == 'RESUME_ENTITY') {
      if (bicsuiteObject.IS_SUSPENDED == 'NOSUSPEND' && bicsuiteObject.IS_SUSPENDED == 'ADMINSUSPEND' && bicsuiteObject.STATE != 'FINAL' && bicsuiteObject.STATE != 'CANCELLED') {
      } else {
        isAvailable = false
      }
    }


  }

 
}
