import { Injectable } from '@angular/core';
import { Tab } from 'src/app/data-structures/tab';
import { MainInformationService } from './main-information.service';

@Injectable({
  providedIn: 'root'
})
export class PrivilegesService {

  constructor(private mainInformationService: MainInformationService) { }


  valide(requiredPrivs: string, ownedPrivs: string, editorform: any, tabInfo: Tab, bicsuiteObject?: any): boolean {

    // for system variables/objects
    if (tabInfo?.ID && !editorform.SYS_OBJ_EDITABLE == true) {
      if (parseInt(tabInfo.ID) < 1000) {
        return false;
      }
    }
    // if(editorform.SYS_OBJ_EDITABLE == true && this.isAdmin()) {
    //   return true;
    // }

    if (requiredPrivs === undefined || requiredPrivs === "") {
      return true;
    } else if (ownedPrivs === undefined) {
      return false;
    }


    let normalizedOwnedPrivs = this.normalize(ownedPrivs);
    let isGranted = false;


    // // if you have no E you cant have any change privileges
    // if (!normalizedOwnedPrivs.includes("E")) {
    //   return false;
    // }


    let normalizedRequiredPrivs = this.normalize(requiredPrivs);


    let requiredPrivsChars = normalizedRequiredPrivs.split("");
    for (let c of requiredPrivsChars) {
      if (normalizedOwnedPrivs.includes(c)) {
        isGranted = true;
        break;
      }
    }
    // console.log(requiredPrivs+ 'owned'+ ownedPrivs)
    return isGranted;
  }

  public normalize(privsString: string): string {
    let privs: string = "";
    let superPriv: string = "";
    let pushedPriv: string = "";

    let privsChars = privsString.split("");

    for (let c of privsChars) {
      if (c == "O" && superPriv == "") {
        pushedPriv = c;
      } else {
        if (c == '(') {
          superPriv = pushedPriv;
          pushedPriv = '';
        } else if (c == ')') {
          superPriv = '';
        } else {
          privs = privs + pushedPriv;
          if (c == superPriv) {
            privs = privs + c;
          }
          else {
            if (superPriv == "O") {
              privs += c.toLowerCase();
            }
            else {
              privs += c;
            }
          }
        }
      }
    }

    return privs + pushedPriv;
  }

  getDefaultGroup(): string {
    return this.mainInformationService.getUser().defaultGroup;
  }

  getMaxEdition(): string {
    let privateEdition = this.mainInformationService.getPrivateProperty("PRIVATE_EDITION", "");
    let edition = privateEdition;
    if (privateEdition == "" || privateEdition == "DEFAULT") {
      edition =  this.mainInformationService.getSystem().maxEdition;
    }
    return edition;
  }

  isEdition(edition: string) {
    if (this.getMaxEdition() == edition) {
      return true;
    }
    return false;
  }
  // keine Editionen müssen explizit angegeben werden
  validateEdition(requiredEditions: string[]): boolean {
    if (requiredEditions !== undefined) {
      for (let edition of requiredEditions) {
        if (edition.includes(this.getMaxEdition())) {
          return true;
        }
      }
      return false;
    }
    return true;
  }

  isAdmin() {
    return this.mainInformationService.getUser().groups.includes('ADMIN');
  }

  getGroups() {
    return this.mainInformationService.getUser().groups;
  }

  hasManagePriv(priv: string) {
    if (this.isAdmin()) return true;
    if (priv && priv != '') {
      // console.log(priv)
      // console.log(this.mainInformationService.getUser());
      return this.mainInformationService.getUser().managePrivs.includes(priv.toLowerCase());
    } else {
      return true;
    }

  }

  // TODO (für später)
  // custom user level
}
