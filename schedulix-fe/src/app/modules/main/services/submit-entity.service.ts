import { Injectable } from '@angular/core';
import { Subject, Subscription } from 'rxjs';
import { masterTreeNode } from 'src/app/interfaces/tree';
import { CommandApiService } from 'src/app/services/command-api.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { SnackbarType } from '../../global-shared/components/snackbar/snackbar-data';

@Injectable({
  providedIn: 'root'
})
export class SubmitEntityService {

  constructor(public commandApiService: CommandApiService,
    public eventEmitterService: EventEmitterService) { }

  evaluateState(node: any) {
    let parentstate = node.state;

    let childstates: any[] = node.childrenStates ? node.childrenStates : [];

    let displayState = parentstate;
    // console.log(childstates)

    //??
    let idle: number = 0;


    // check if idle first
    idle = this.isIdle(childstates) ? 1 : 0;


    if (parentstate == 'FINISHED') {
      if (node.isSuspended != 'NOSUSPEND' || node.parentSuspended != '0') {
        displayState == 'SUSPENDED';
      } else if (node.type == 'BATCH' && node.JOB_IS_FINAL == 'true') {
        if (idle == 0) {
          displayState = 'ACTIVE';
        } else {
          if (!childstates.includes('PENDING')) {
            displayState = 'IDLE';
          } else {
            displayState = 'PENDING';
          }
        }

      } else if (node.type == 'JOB' && node.IS_RESTARTABLE == 'false' && node.JOB_IS_FINAL == 'false') {
        displayState = 'PENDING';
      }
    }
    if (parentstate == 'SYNCHRONIZE_WAIT') {
      if (node.isSuspended != 'NOSUSPEND' || node.parentSuspended != '0') {
        displayState = 'SUSPENDED';
      }
    }
    if (parentstate != 'CANCELLED' && node.IS_CANCELLED == 'true') {
      displayState = 'CANCELLING';
    }
    if (parentstate != 'CANCELLED' && node.IS_DISABLED == 'true') {
      displayState = 'DISABLED';
    }
    return displayState;
  }

  getDisplayState(node: any, childrenStates: string[]): string {
    let displayState = node.STATE;
    let idle = this.isIdle(childrenStates);
    if (node.STATE == 'FINISHED') {
      if (node.IS_SUSPENDED != 'NOSUSPEND' || parseInt(node.PARENT_SUSPENDED) > 0) {
        displayState = 'SUSPENDED';
      }
      else if (node.SE_TYPE == 'BATCH' || (node.SE_TYPE == 'JOB' && node.JOB_IS_FINAL == 'true')) {
        if (!idle) {
          displayState = 'ACTIVE';
        }
        else {
          if (childrenStates.includes('PENDING')) {
            displayState = 'PENDING';
          }
          else {
            displayState = 'IDLE';
          }
        }
      }
      else if (node.SE_TYPE == 'JOB' && node.STATE == 'FINISHED' && node.IS_RESTARTABLE == 'false' && node.JOB_IS_FINAL == 'false') {
        displayState = 'PENDING'
      }
    }
    if (node.STATE == 'SYNCHRONIZE_WAIT') {
      if (node.IS_SUSPENDED != 'NOSUSPEND' || parseInt(node.PARENT_SUSPENDED) > 0) {
        displayState = 'SUSPENDED';
      }
    }
    if (node.STATE != 'CANCELLED' && node.IS_CANCELLED == 'true') {
      displayState = 'CANCELLING';
    }
    if (node.IS_DISABLED == 'true' && node.STATE != 'CANCELLED') {
      displayState = 'DISABLED';
    }
    return displayState;
  }


  // get color of batch aspect only used for jobs with children
  getParentIconColor(node: any, state: string, childrenStates: string[]): string {
    return 'ICON_' + this.getParentColor(node, state, childrenStates);
  }

  getParentTextColor(node: any, state: string, childrenStates: string[]): string {
    return 'TEXT_' + this.getParentColor(node, state, childrenStates);
  }

  getParentColor(node: any, state:string, childrenStates: string[]): string {
    // let idle: boolean = this.isIdle(childrenStates);
    // let active: boolean = this.isActive(childrenStates);
    let stalled: boolean = this.isStalled(childrenStates);
    // console.log(stalled)
    let color: string = '';
    if (childrenStates.length > 0) {
      if (state == 'FINAL') {
        color = 'GREEN';
      }
      else if (state != 'CANCELLED' && (node.CNT_RESTARTABLE > 0 || node.CNT_UNREACHABLE > 0)) {
        color = 'RED';
      }
      else if (node.IS_DISABLED == 'true' && state != 'CANCELLED') {
        color = 'GREY';
      }
      else if (stalled || state == 'DEPENDENCY_WAIT') {
        color = 'PURPLE';
      }
      else if (state == 'FINISHED' || this.isActive(childrenStates)) {
        color = 'BLUE';
      }
      else if (state == 'CANCELLED') {
        color = 'BROWN';
      }
      else if (node.IS_DISABLED == 'true') {
        color = 'GREY';
      }
    }
    return color;
  }

  isIdle(childstates: string[]): boolean {
    if (!(childstates.includes('STARTING')
      || childstates.includes('STARTED')
      || childstates.includes('RUNNING')
      || childstates.includes('PENDING')
      || childstates.includes('RUNNABLE'))) {
      // console.log("isidle")
      return true;
    }
    return false;
  }

  isActive(childstates: string[]): boolean {
    if (childstates.includes('RUNNABLE')
      || childstates.includes('RUNNING')
      || childstates.includes('ACTIVE')
      || childstates.includes('PENDING')
      || childstates.includes('STARTING')
      || childstates.includes('STARTED')
      || childstates.includes('KILLING')
      || childstates.includes('TO_KILL')
      || childstates.includes('KILLED')
      || childstates.includes('CANCELLING')) {
      // console.log("isactive")
      return true;
    }
    return false;
  }

  isStalled(childstates: string[]): boolean {
    if (!this.isActive(childstates) && !this.isIdle(childstates) && childstates.includes('RESOURCE_WAIT')) {
      return true;
    }
    return false;
  }

  getTextColor(node: any, state: string, seType: string, isFinal: string, is_restartable: string, isDisabled: string, childstates: string[]): string {
    // console.log(node)
    let color: string = '';
    if (seType == 'JOB' || childstates.length == 0) {
      if (node.JOB_IS_FINAL == 'true') {
        color = 'TEXT_GREEN';
      }
      else if (['RUNNABLE', 'STARTING', 'STARTED', 'RUNNING'].includes(state)
      // Warum brauchtt es die folgende Bedingung ?
         || (isFinal == 'false' && node.JOB_ESD != null && is_restartable == 'false' && !(['CANCELLED','ERROR'].includes(state)))
        ) {
        color = 'TEXT_BLUE';
        // console.log(node.ID + ' --------------------------------------------------------------------------');
        // console.log(node.JOB_ESD);
      }
      else if (is_restartable == 'true' || ['UNREACHABLE', 'BROKEN_FINISHED', 'BROKEN_ACTIVE', 'ERROR'].includes(state)) {
        color = 'TEXT_RED';
      }
      else if (isDisabled == 'true' && state != 'CANCELLED') {
        color = 'TEXT_GREY';
      }
      else if (state == 'DEPENDENCY_WAIT' || state == 'SYNCHRONIZE_WAIT' || state == 'RESOURCE_WAIT' || state == 'SCHEDULED') {
        color = 'TEXT_PURPLE';
      }
      else if (state == 'KILLED' || state == 'TO_KILL') {
        color = 'TEXT_ORANGE';
      }
      else if (state == 'CANCELLED') {
        color = 'TEXT_BROWN';
      }
    }
    else {
      color = this.getParentTextColor(node, state, childstates);
    }
    // console.log(color);
    return color;
  }

  getIconColor(node: any, state: string, seType: string, isFinal: string, is_restartable: string, isDisabled: string, childrenStates: string[]): string {
    return this.getTextColor(node, state, seType, isFinal, is_restartable, isDisabled, childrenStates).replace('TEXT_', 'ICON_');
  }


  getChildrenStates(node: any) {

    // console.log(node)
    let childrenStateList: string[] = [];
    for (const [key, value] of Object.entries(node)) {
      if (key.includes('CNT_')) {
        if (value != 0) {
          childrenStateList.push(key.replace('CNT_', ''))
        }

      }
    }
    return childrenStateList;
  }





  // ----------------------------------- FUNCTIONS FOR MASTER AND JOBLIST MONITORING -----------------------
  async rerunEntitys(entitys: any[], type: any, recursive: boolean, audit: string) {

    // console.log("RERUN")
    for (let entity of entitys) {
      if (type == 'MASTER' && recursive) {
        let command = 'alter job ' + entity.id + ' with rerun recursive';
        if (audit != '') {
        command += ", comment = '" + audit + "'"
        }
        await this.rerun(command, entity);
      } else if (type == 'SEARCH') {
        if (entity.hasChildren) {
          let command = 'alter job ' + entity.id + ' with rerun recursive';
          if (audit != '') {
          command += ", comment = '" + audit + "'"
          }
          await this.rerun(command, entity);
        } else {
          let command = 'alter job ' + entity.id + ' with rerun';
          if (audit != '') {
          command += ", comment = '" + audit + "'"
          }
          await this.rerun(command, entity);
        }
      }
    }
  }


  async rerun(command: string, entity: masterTreeNode) {
    await this.commandApiService.execute(command).then((result: any) => {

      // if (result.hasOwnProperty('ERROR')) {
      //   //tell User that something went wrong
      //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
      //   return;
      // } else {
        this.eventEmitterService.pushSnackBarMessage([entity.name + ' has been rerunned'], SnackbarType.INFO)
        return;
      // }
    }, (rejection: any) => {
      // do nothing
      return;
    });

  }


  // async cancelEntitys(entitys: any[], type: any, recursive: boolean, audit: string) {
  //   for (let entity of entitys) {

  //     if (type == 'MASTER' || type == 'SEARCH') {
  //       let command = 'alter job ' + entity.id + ' with cancel';
  //       if (audit != '') {
  //       command += ", comment = '" + audit + "'"
  //       }
  //       await this.commandApiService.execute(command).then((result: any) => {
  //         if (result.hasOwnProperty('ERROR')) {
  //           //tell User that something went wrong
  //           this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
  //           return;
  //         } else {
  //           this.eventEmitterService.pushSnackBarMessage([entity.name + ' has been cancelled'], SnackbarType.INFO)
  //           return;
  //         }
  //         // callback.next('done')
  //       }
  //       );
  //     }
  //   }
  // }

  async supendEntitys(entitys: any[], type: any, recursive: boolean, audit: string) {
    for (let entity of entitys) {

      if (type == 'MASTER' || type == 'SEARCH') {
        let command = 'alter job ' + entity.id + ' with suspend';
        if (audit != '') {
          command += ", comment = '" + audit + "'"
        }
        await this.commandApiService.execute(command).then((result: any) => {
          // if (result.hasOwnProperty('ERROR')) {
          //   //tell User that something went wrong
          //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
          //   return;
          // } else {
            this.eventEmitterService.pushSnackBarMessage([entity.name + ' has been suspended'], SnackbarType.INFO)
            return;
          // }
          // callback.next('done')
        }, (rejection: any) => {
          // do nothing
          return;
        });
      }
    }

  }

  async resumeEntitys(entitys: any[], type: any, recursive: boolean, audit: string) {
    for (let entity of entitys) {

      if (type == 'MASTER' || type == 'SEARCH') {
        let command = 'alter job ' + entity.id + ' with resume';
        if (audit != '') {
        command += ", comment = '" + audit + "'"
        }
        await this.commandApiService.execute(command).then((result: any) => {
          // if (result.hasOwnProperty('ERROR')) {
          //   //tell User that something went wrong
          //   this.eventEmitterService.pushSnackBarMessage([result.ERROR.ERRORMESSAGE], SnackbarType.ERROR)
          //   return;
          // } else {
            this.eventEmitterService.pushSnackBarMessage([entity.name + ' has been resumed'], SnackbarType.INFO)
            return;
          }, (rejection: any) => {
            // do nothing
            return;
          }
        // }
        );
      }
    }
  }

  killEntity(entitys: any[], type: any) {

  }

  setEntityState(entitys: any[], type: any) {

  }

  enableEntitys(entitys: any[], type: any) {

  }

  disableEntitys(entitys: any[], type: any) {

  }
}
