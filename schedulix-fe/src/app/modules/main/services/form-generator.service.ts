import { Injectable } from '@angular/core';
import { Form, UntypedFormArray, UntypedFormControl, UntypedFormGroup, ValidatorFn, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { getValidatorsByEdtitorform } from 'src/app/classes/formgenerator/getValidatorsByEditorform';
import { ValidationHelper } from 'src/app/classes/formgenerator/validation-helper';
import { EditorformFlags, EditorformOptions, EditorformRender } from 'src/app/data-structures/editorform-options';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { CRUDService } from 'src/app/services/crud.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { MainInformationService } from './main-information.service';
import { PrivilegesService } from './privileges.service';

@Injectable({
  providedIn: 'root'
})
export class FormGeneratorService {

  constructor(private privilegesService: PrivilegesService, private crudService: CRUDService, private mainInformationService: MainInformationService,
    private toolboxService: ToolboxService) {

  }

  private evaluateRender(efo: EditorformOptions, bicsuiteObject: any, currentEditorformField: any, tabInfo: Tab): EditorformRender {
    // console.log('evaluateRender')
    let simple: boolean = (currentEditorformField.SIMPLE != undefined && currentEditorformField.SIMPLE == true);
    let invisible: boolean = (currentEditorformField.INVISIBLE != undefined && currentEditorformField.INVISIBLE == true);
    let hidden: boolean = (currentEditorformField.HIDDEN != undefined && currentEditorformField.HIDDEN == true);
    let disabled: boolean = (currentEditorformField.DISABLED != undefined && currentEditorformField.DISABLED == true);

    if (currentEditorformField.CONDITION != undefined) {
      hidden = (efo.flags.CONDITION === false);
    }

    disabled = (efo.flags.HAS_PRIVS === false);

    /************** RENDER_OPTION FLAG <== Flags that CAN be overwritten by Render-Options should occur above THIS LINE !! **************/

    if (currentEditorformField.RENDER_OPTIONS !== undefined) {
      // console.log(currentEditorformField.RENDER_OPTIONS)
      let renderOptions = currentEditorformField.RENDER_OPTIONS;
      let renderOptionsValueArr = [];
      let value = (bicsuiteObject[renderOptions.FIELD_NAME] == undefined ? null : bicsuiteObject[renderOptions.FIELD_NAME]);

      // Handle editorform IS_VALUE property
      if (typeof renderOptions.IS_VALUE === "string" || renderOptions.IS_VALUE instanceof String) {
        renderOptionsValueArr.push(renderOptions.IS_VALUE);
      } else if (Array.isArray(renderOptions.IS_VALUE)) {
        renderOptionsValueArr = renderOptions.IS_VALUE;
      }


      for (let renderOptValue of renderOptionsValueArr) {
        // console.log(renderOptValue)
        // console.log(value)
        if (value === renderOptValue) {
          simple = (renderOptions.SIMPLE === "true" || renderOptions.SIMPLE === true);
          invisible = (renderOptions.INVISIBLE === "true" || renderOptions.INVISIBLE === true);
          hidden = (renderOptions.HIDDEN === "true" || renderOptions.HIDDEN === true);
          disabled = (renderOptions.DISABLED === "true" || renderOptions.DISABLED === true);
          break;
        }
      }
    }

    // AVAILABLE Flag, Only overwrite when version is not available!!!
    if (efo.flags.AVAILABLE == false) {
      hidden = true;
    }

    /************** RENDER_OPTION FLAG <== Flags that CAN NOT be overwritten by Render-Options should occur above THIS LINE !! **************/

    if (tabInfo.ISREADONLY == true) {
      disabled = true;
    }

    let render: EditorformRender = {
      simple: true, // TODO, maybe deprecated and not used at all.
      invisible: invisible,
      hidden: hidden,
      disabled: disabled
    };
    return render;
  }

  createEditorformOptions(editorformField: any, bicsuiteObject: any, tabInfo: Tab, activeEFO?: EditorformOptions | null): EditorformOptions {

    // console.log("createEditorformOptions")
    let efo = new EditorformOptions();
    // let rootEditorform = this.crudService.getEditorform(tabInfo.TYPE);

    // Set correct PRIVS
    // REWORK TASK HERE @DIETER @ENNO
    // consojle.log(activeEFO?.PRIVS)
    // active(parentPrivs) have highest level of truth
    if (activeEFO !== undefined && activeEFO !== null) {
      // console.log(activeEFO)
      efo.PRIVS = activeEFO.PRIVS;
    } else if (bicsuiteObject.PRIVS !== undefined) {
      // console.log(bicsuiteObject.PRIVS)
      efo.PRIVS = bicsuiteObject.PRIVS;
    } else {
      efo.PRIVS = "";
    }
    // console.log(bicsuiteObject)
    // console.log(efo.PRIVS + ',userprivs: ' + this.mainInformationService.getUser().privs + ', activeEFO:' + activeEFO?.PRIVS + ', BObject: ' + bicsuiteObject.PRIVS);

    if (efo.PRIVS == undefined || efo.PRIVS == "") {
      // basic kann nur leer sein
      efo.PRIVS = this.mainInformationService.getUser().privs;
      // console.log(this.mainInformationService.getUser().privs);
    }

    // Set icon if exists
    let svgIcon: string | undefined;
    // console.log(bicsuiteObject)

    if (editorformField.ICON_LOOKUP != undefined) {
      if (typeof editorformField.ICON_LOOKUP == "object") {
        if (editorformField.ICON_LOOKUP.PREFIX) {
          svgIcon = editorformField.ICON_LOOKUP.PREFIX + bicsuiteObject[editorformField.ICON_LOOKUP.RECORD_PROPERTY];
        }
        else {
          svgIcon = bicsuiteObject[editorformField.ICON_LOOKUP.RECORD_PROPERTY];
        }
      } else if (editorformField.ICON_LOOKUP != undefined && typeof editorformField.ICON_LOOKUP == "string") {
        svgIcon = editorformField.ICON_LOOKUP;
      }
    }


    // if (editorformField.TYPE == 'CHECKBOX') {
    //   console.log("owned prifs bzw EFO\n");
    //   console.log(efo)

    //   console.log("required privs bzw editorformfield\n");
    //   console.log(editorformField)
    // }

    efo.flags = {
      HIDE_CONDITION: false,
      AVAILABLE: this.isAvailable(editorformField),
      CONDITION: true,
      HAS_PRIVS: this.privilegesService.valide((editorformField.PRIVS == undefined ? 'E' : editorformField.PRIVS), efo.PRIVS, editorformField, tabInfo, bicsuiteObject),
      CSS: editorformField.CSS != undefined ? editorformField.CSS : "",
      CSS_CLASS: editorformField.CSS_CLASS != undefined ? editorformField.CSS_CLASS : "",
      SVG_ICON: svgIcon?.toLowerCase(),  // Icon-Ids have to be LOWER CASE! Always.
      STYLE_ATTR: editorformField.STYLE_ATTR
    };

    efo.render = this.evaluateRender(efo, bicsuiteObject, editorformField, tabInfo);


    // Add CRUD Manipulation
    // this.crudService.UI();


    return efo;
  }


  private isAvailable(editorformField: any): boolean {
    if (editorformField.hasOwnProperty("AVAILABLE")) {
      // Not valid => return true and pretend that option is available
      if (!Array.isArray(editorformField.AVAILABLE))
        return true;

      let systemInfo = this.mainInformationService.getSystem();
      for (let edition of editorformField.AVAILABLE) {
        if (edition == systemInfo.maxEdition)
          return true;
      }

      // Version not in Available mentioned
      return false;
    }

    // Return true when AVAILABLE Property is missing
    return true;
  }

  // returns true if editableCondition is matched
  editableCondition(obj: any, condition: string | undefined) {
    if (obj.hasOwnProperty("TYPE") && condition !== undefined) {
      let type: string = obj.TYPE;
      return condition.toLowerCase() === ("conditiontypeis" + type.toLowerCase());
    }

    // No condition is always true.
    return true;
  }



  // General form validation creation.
  createFormControl(data: { parentForm: UntypedFormGroup, formControl: UntypedFormControl, editorform: any, bicsuiteObject: any, formArrayIdentifier?: string, tabInfo?: Tab }): FormGeneratorServiceControl {

    // if (data.editorform.CREATE_MANDATORY !== undefined && data.editorform.CREATE_MANDATORY === true && data.tabInfo?.MODE == TabMode.NEW) {
    //   validators.push(Validators.required);
    // }

    let formArrayIndex: number = -1;
    // Set default value and validators
    data.formControl.setValidators(new getValidatorsByEdtitorform(this.toolboxService).getValidators(data.editorform));
    data.formControl.setValue(data.bicsuiteObject[data.editorform.NAME]);


    if (data.formArrayIdentifier !== undefined) {
      let formArray: UntypedFormArray;
      // console.log(data.editorform.NAME)
      // console.log(data.parentForm)
      if (!data.parentForm.contains(data.formArrayIdentifier)) {
        formArray = new UntypedFormArray([]);
        data.parentForm.registerControl(data.formArrayIdentifier, formArray);
        // console.log(data.editorform.NAME)
        // console.log(" registered")
      }
      else {
        formArray = (data.parentForm.get(data.formArrayIdentifier) as UntypedFormArray);
        // console.log(data.editorform.NAME)
        // console.log(" already there registered")
      }

      formArray.push(data.formControl);
      // console.log(formArray)
      // console.log(data.editorform.NAME)
      // console.log(form)
      formArrayIndex = formArray.length - 1;
    }
    else {
      data.parentForm.registerControl(data.editorform.NAME, data.formControl);
    }


    // Preparing result
    let result = new FormGeneratorServiceControl();
    result.formControl = data.formControl;
    result.parentForm = data.parentForm;
    result.formArrayIdentifier = data.formArrayIdentifier;
    result.formArrayIndex = formArrayIndex;
    result.editorFormField = data.editorform;

    return result;
  }

  destroyFormControl(control: FormGeneratorServiceControl) {
    if (control.formArrayIdentifier !== undefined) {
      let formArray: UntypedFormArray = (control.parentForm.get(control.formArrayIdentifier) as UntypedFormArray);
      let removeIdx: number = -1;

      for (let item of formArray.controls) {
        removeIdx++;
        if (item === control.formControl) {
          break;
        }
      }

      if (removeIdx > -1) {
        formArray.removeAt(removeIdx);
      }
    }
    else {
      control.parentForm.removeControl(control.editorFormField.NAME);
    }
  }
}

export class FormGeneratorServiceControl {
  formControl: UntypedFormControl = new UntypedFormControl({});
  parentForm: UntypedFormGroup = new UntypedFormGroup({});
  formArrayIdentifier?: string;
  formArrayIndex: number = -1;
  editorFormField: any = {};
}
