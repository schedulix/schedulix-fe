import { Injectable } from '@angular/core';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { EditorformDialogComponent, EditorformDialogData } from '../components/form-generator/editorform-dialog/editorform-dialog.component';
import trigger_editorform from '.././../../json/editorforms/batchesandjobs/trigger-detail.json';
import { NewItemDialogComponent } from '../components/dialog-components/new-item-dialog/new-item-dialog.component';
import { CRUDService } from 'src/app/services/crud.service';
import { SelectionModel } from '@angular/cdk/collections';
import { IntervalCrud } from 'src/app/classes/interval-crud';
import { SchedulesCrud } from 'src/app/classes/schedules-crud';
import { ResourceCrud } from 'src/app/classes/resource-crud';
import { JobCrud } from 'src/app/classes/job-crud';
import { MainInformationService } from './main-information.service';

@Injectable({
  providedIn: 'root'
})
export class TableButtonsService {

  constructor(private eventEmitterService: EventEmitterService,
    private crudService: CRUDService,
    private dialog: MatDialog,
    private mainInformationService: MainInformationService) {
  }

  // Returns true if table change occured
  public rowExecute(columnInfo: any, rowRecord: any, editorform: any, bicsuiteObject: any, rowIndex: number, tabInfo: Tab): Promise<boolean> {
    let func = "";
    if (typeof columnInfo.VALUE == 'string') {
      func = columnInfo.VALUE;
    }
    else if (typeof columnInfo.VALUE == 'object') {
      func = columnInfo.VALUE.FUNCTION;
    }

    switch (func) {
      case "EDIT_MASTERSCHEDULE": {
        let schedulesCrud : SchedulesCrud = this.crudService.createOrGetCrud("SCHEDULES") as SchedulesCrud;
        return schedulesCrud.selectMasterschedule(bicsuiteObject, tabInfo, rowIndex).then((result) => { return result > 0; });
      }
      case "EDIT_SUBSCHEDULE": {
        let schedulesCrud : SchedulesCrud = this.crudService.createOrGetCrud("SCHEDULES") as SchedulesCrud;
        return schedulesCrud.selectSubschedule(bicsuiteObject, tabInfo, rowIndex).then((result) => { return result > 0; });
      }
      case "EDIT_DISPATCHER": {
        let intervalCrud : IntervalCrud = this.crudService.createOrGetCrud("INTERVAL") as IntervalCrud;
        return intervalCrud.selectDispatcher(bicsuiteObject, tabInfo, rowIndex).then((result) => { return result > 0; });
      }
      case "EDIT_DISTRIBUTION": {
        let resourceCrud : ResourceCrud = this.crudService.createOrGetCrud("RESOURCE") as ResourceCrud;
        return resourceCrud.selectDistribution(bicsuiteObject, tabInfo, rowIndex).then((result) => { return result > 0; });
      }
      case "DROP": {
        return this.drop(bicsuiteObject, editorform, tabInfo, rowIndex).then((result) => { return result > 0; });
      }
      case "POPUP_EDIT_TRIGGER": {
        return this.openTriggerDetails(editorform, bicsuiteObject, tabInfo, rowIndex).then((result) => { return result > 0; });
      }
      default: {
        return Promise.resolve(false);
      }
    }

    return Promise.resolve(false);
  }

  // Returns number of rows changend
  public tableExecute(btnInfo: any, bicsuiteObject: any, editorform: any, tabInfo: Tab, form: any, checklistSelection: SelectionModel<any>): Promise<number> {

    let changedRows: number = 0;


    let crud = this.crudService.createOrGetCrud(tabInfo.TYPE);
    switch (btnInfo.VALUE) {
      case "APPEND": return this.append(bicsuiteObject, editorform, tabInfo, form); break;
      case "CREATE_FOLDER_ITEM": return this.create_folder_item(bicsuiteObject, editorform); break;
      case "CREATE_CATEGORY_ITEM": return this.create_category_item(bicsuiteObject, editorform); break;
      case "CREATE_SCOPE_ITEM": return this.create_scope_item(bicsuiteObject, editorform); break;
      case "ESM_SORT": return this.esm_sort(bicsuiteObject, editorform, tabInfo); break;
      case "POPUP_APPEND_TRIGGER": return this.openTriggerDetails(editorform, bicsuiteObject, tabInfo); break;
      // NOW WORKS WITH CRUD CALL METHOD in form-table and form-tree-table
      default: '';
    }

    // should return number aka changed rows
    return crud.callMethod(btnInfo.METHOD, bicsuiteObject,  tabInfo, form, checklistSelection);
  }
  // Table functions
  private openTriggerDetails(editorform: any, bicsuiteObject: any, tabInfo?: Tab, rowIndex?: number): Promise<number> {
    if (tabInfo == undefined)
      return Promise.resolve(0);

    // console.log(bicsuiteObject);

    let dataObj: any = {
      NAME: "",
      ESP_NAME: bicsuiteObject.ESP_NAME,
      ACTIVE: true,
      //COMMENT: "",
      STATES: {
        TABLE: [],
        DESC: ["EXIT_STATE"]
      },
      //SUBMIT_TYPE: "",
      //SUBMIT_NAME: "",
      PARAMETERS: {
        TABLE: [],
        DESC: ["NAME", "EXPRESSION"]
      },
      //SUBMIT_SE_OWNER: "",
      //TRIGGER_TYPE: "",
      //MASTER: false,
      //WARN: false,
      //SUSPEND: false
    };
    let bsParametersArray: any[], bsStatesArray: any[] = [];


    if (rowIndex != undefined && rowIndex >= 0) {
      bsStatesArray = bicsuiteObject[editorform.NAME].TABLE[rowIndex].STATES.TABLE;
      bsParametersArray = bicsuiteObject[editorform.NAME].TABLE[rowIndex].PARAMETERS.TABLE;
      // Copy whole bicsuiteEntry by value.
      dataObj = JSON.parse(JSON.stringify(bicsuiteObject[editorform.NAME].TABLE[rowIndex]));
      dataObj.ESP_NAME = bicsuiteObject.ESP_NAME;
    }

    const dialogRef = this.dialog.open(EditorformDialogComponent, {
      data: new EditorformDialogData(trigger_editorform, dataObj, tabInfo, "Trigger for " + (tabInfo != undefined ? tabInfo.NAME : "")),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
      width: "800px"
    });

    // * Note, dialogs unsubsrcibe autoamtically!
    return dialogRef.afterClosed().toPromise().then((result: any) => {
      // console.log(result);
      if (result != null) {
        if (rowIndex != undefined && rowIndex >= 0) {
          Object.assign(bicsuiteObject[editorform.NAME].TABLE[rowIndex], result);
        } else {
          bicsuiteObject[editorform.NAME].TABLE.push(result);
        }
      }
      return 1;
    });
  }

  private append(bicsuiteObject: any, editorform: any, tabInfo: Tab, form: any): Promise<number> {
    let appendObj : any = {};
    if (editorform.hasOwnProperty("APPEND_DEFAULT")) {
      appendObj = JSON.parse(JSON.stringify(editorform.APPEND_DEFAULT));
    }
    appendObj.rowIndex = bicsuiteObject[editorform.NAME].TABLE ? bicsuiteObject[editorform.NAME].TABLE.length : 0
    bicsuiteObject[editorform.NAME].TABLE.push(appendObj);
    if (editorform.hasOwnProperty("APPEND_METHOD")) {
      let crud = this.crudService.createOrGetCrud(tabInfo.TYPE);
      crud.callMethod(editorform.APPEND_METHOD, bicsuiteObject, tabInfo, appendObj);
    }
    return Promise.resolve(1);
  }

  private async esm_sort(bicsuiteObject: any, editorform: any, tabInfo: Tab): Promise<number> {
    let changedRows = 1;

    let tableRef = bicsuiteObject[editorform.NAME].TABLE;
    if (tableRef.length <= 0)
      return Promise.resolve(0);

    // remove empties
    for (let i = 0; i < tableRef.length; i++) {
      if (tableRef[i].ECR_START === "") {
        changedRows += await this.drop(bicsuiteObject, editorform, tabInfo, i);
        i--;
      }
    }

    // sort valid values
    tableRef.sort(function (a: any, b: any) {
      return a.ECR_START - b.ECR_START;
    });

    for (let i = 0; i < tableRef.length - 1; i++) {
      let curr = tableRef[i];
      let next = tableRef[i + 1];

      curr.ECR_END = next.ECR_START - 1;
    }


    tableRef[0].ECR_START = -2147483648; // = -2^31. <==> Min int for 32 BIT
    tableRef[tableRef.length - 1].ECR_END = 2147483647 // = 2^31 - 1 <==> Max int for 32 BIT;

    return Promise.resolve(changedRows);
  }


  private drop(bicsuiteObject: any, editorform: any, tabInfo: Tab, index: number): Promise<number> {
    if (index != undefined && index > -1) {
      let row = bicsuiteObject[editorform.NAME].TABLE[index];
      // console.log(editorform)
      if (editorform.hasOwnProperty("DROP_METHOD")) {
        let crud = this.crudService.createOrGetCrud(tabInfo.TYPE);
        let res = crud.callMethod(editorform.DROP_METHOD, bicsuiteObject, tabInfo, row);
        res.then((ress)=> {
          // console.log(ress);
          // 'NO_DROP' should return the confirm dialog if aborted -> no splicing allowed
          if(ress != 'NO_DROP') {
            bicsuiteObject[editorform.NAME].TABLE.splice(index, 1);
          }
        })
      } else {
        bicsuiteObject[editorform.NAME].TABLE.splice(index, 1);
      }
    }
    return Promise.resolve(1);
  }


  private create_folder_item(bicsuiteObject: any, editorform: any): Promise<number> {


    // createItem() {

    console.log(bicsuiteObject);

    const dialogRef = this.dialog.open(NewItemDialogComponent, {
      data: {
        title: "new_item",
        translate: "create_new_item_chooser",
        itemTypes: ['FOLDER', 'BATCH', 'JOB', 'MILESTONE'],
        path: bicsuiteObject.PATH + '.' + bicsuiteObject.NAME
      },
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });

    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result) {
        let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
        tab.PATH = result.path;
        if (!tab.DATA) {
            tab.DATA = {}
        }
        tab.DATA.IN_CONVENTION_FOLDER = bicsuiteObject.IS_CONVENTION_FOLDER;
        tab.DATA.IN_CONVENTION_FOLDER_TYPE = bicsuiteObject.IS_CONVENTION_FOLDER_TYPE;
        this.eventEmitterService.createTab(tab, true);
      }
    });


    return Promise.resolve(1);
  }

  private create_category_item(bicsuiteObject: any, editorform: any): Promise<number> {
    const dialogRef = this.dialog.open(NewItemDialogComponent, {
      data: {
        title: "new_item",
        translate: "create_new_item_chooser",
        itemTypes: ['CATEGORY', 'STATIC', 'SYSTEM', 'SYNCHRONIZING', 'POOL'],
        path: bicsuiteObject.PATH + '.' + bicsuiteObject.NAME
      },
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result) {
        let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
        tab.PATH = result.path;
        // console.log(result)
        this.eventEmitterService.createTab(tab, true);
      }
    });
    return Promise.resolve(1);
  }

  private create_scope_item(bicsuiteObject: any, editorform: any): Promise<number> {
    const dialogRef = this.dialog.open(NewItemDialogComponent, {
      data: {
        title: "new_item",
        translate: "create_new_item_chooser",
        itemTypes: ['SCOPE', 'SERVER'],
        path: bicsuiteObject.PATH + '.' + bicsuiteObject.NAME
      },
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary']
    });
    dialogRef.afterClosed().subscribe((result: any) => {
      if (result !== undefined && result) {
        let tab = new Tab('*untitled', new Date().getTime().toString(), result.choosedItem, result.choosedItem.toLowerCase(), TabMode.NEW);
        tab.PATH = result.path;
        // console.log(result)
        this.eventEmitterService.createTab(tab, true);
      }
    });
    return Promise.resolve(1);
  }

}
