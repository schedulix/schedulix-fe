import { Injectable, Output } from '@angular/core';
import { resolve } from 'dns';
import { Subject } from 'rxjs';
import { promise } from 'selenium-webdriver';
import { System } from 'src/app/data-structures/system';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { UserInformation } from 'src/app/data-structures/user-information';
import { AuthService } from 'src/app/services/auth.service';
import { CommandApiService } from 'src/app/services/command-api.service';
import { CustomPropertiesService } from 'src/app/services/custom-properties.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { OutputType, SnackbarType } from '../../global-shared/components/snackbar/snackbar-data';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { SdmsServer } from 'src/app/data-structures/connection';

@Injectable({
  providedIn: 'root'
})
export class MainInformationService {

  private user: UserInformation = new UserInformation();
  private fronendUser: UserInformation = new UserInformation();
  private system: System = new System();

  public userHasLoadedObserver: Subject<any> = new Subject();

  // public frontendParametersHasLoadedObserver: Subject<any> = new Subject();
  // private userParamters: any;


  public isInitialized: boolean = false;

  constructor(private toolBoxService: ToolboxService, private commandApiService: CommandApiService, private eventEmitterService: EventEmitterService, private customPropertiesService: CustomPropertiesService, private authService: AuthService) {
    // this.getConnectionInformation();
    // this.getUserInformation();
  }
  getConnectionInformation(): Promise<any> {
    return this.commandApiService.execute("SHOW SYSTEM")
  }

  setConnectionInformation(result: any) {
      this.system.maxEdition = this.setMaxEdition(result);
      this.system.version = result.DATA.RECORD.VERSION;
      this.system.systemname = result.DATA.RECORD.SSObicsuitePrefix;
      this.system.DumpLangLevel = result.DATA.RECORD.DumpLangLevel;
      this.system.maxLevel = this.setMaxLevel(result);
  }

  setMaxLevel(result: any): string{
    // choose frontend maxlevel if configured
    if(this.system.server?.VERSION) {
      return this.system.server.VERSION;
    } else {
      return result.DATA.RECORD.MAX_LEVEL;
    }
  }

  setMaxEdition(result: any): string{
    // choose frontend maxlevel if configured
    if(this.system.server?.VERSION) {
      return this.system.server.VERSION;
    } else {
      return result.DATA.RECORD.CompatibilityLevel;
    }
  }


  getServers(): Promise<any> {
    return this.authService.loadServerList();
  }

  setServer() {
    this.authService.setSelectedServer();
    this.system.server = this.authService.getSelectedServer() ? this.authService.getSelectedServer()! : new SdmsServer();
  }
  clean() {
    this.user = new UserInformation();
    this.fronendUser = new UserInformation();
  }

  login(): Promise<void> {
    return Promise.all([
      // console.log('alter systemparameters and userParameters resolved'),
      this.getUserInformation(),
      this.getFrontendUserInformation(),
      this.getConnectionInformation(),
      this.getServers()
    ]).then((result: any) => {

      this.setUserInformation(result[0]);
      this.setFrontendUserInformation(result[1]);
      this.setServer();
      this.setConnectionInformation(result[2]);

      this.userHasLoadedObserver.next('login');
    // update bookmarkTab
      // has to be the same values as in the left navigation, so that the bookmarking tab will be updated -> check changes here and in left-navigation
      let tab: Tab = new Tab('Bookmarking', 'bookmarking', 'Bookmarking'.toUpperCase(), 'bookmark', TabMode.EDIT);
      // if bookmarking tab is opened it will be updated
      this.eventEmitterService.updateTabEmitter.next(tab);

    });
    // this.getFrontendUserInformation();
  }



  getPrivateProperty(propertyName: string, configName: string) {
    // config.json template must define configName
    let propertiesObject: any = {};
    try {
      propertiesObject = this.getUserParamter('PRIVATE_PROPERTIES');
    }
    catch (e) {
      console.warn(e);
    }
    if (propertiesObject && propertiesObject[propertyName]) {
      return propertiesObject[propertyName];
    }
    if (configName && configName != "") {
      return this.customPropertiesService.getCustomObjectProperty(configName);
    }
    return "";
  }


  getFrontendUserInformation(): Promise<any> {
    return this.commandApiService.execute("SHOW USER FE_PROPERTIES")
  }
  setFrontendUserInformation(result: any){
    this.fronendUser.username = result.DATA.RECORD.NAME;
    this.fronendUser.userParameter = result.DATA.RECORD.PARAMETERS.TABLE;
  }
  getTheme(): string {
    let theme = this.getPrivateProperty('THEME', '')
    if(theme != "") {
      return theme;
    } else {
      return "LIGHT"
    }
    // return this.getPrivateProperty('THEME', '');
  }
  isSchedulix() {
    return (this.getSystem().maxLevel == 'OPEN');
  }

  getThemeString() {
    return 'fe-theme-' + this.getTheme() + (this.isSchedulix() ? '-SCHEDULIX': '');
  }

  checkVersion(minVersion : string) : boolean {
    let sysl = this.getSystem().version.split('.');
    let minl = minVersion.split('.');
    
    let index = 0;
    while (index < minl.length && index < sysl.length) {
      if (parseInt(minl[index]) > parseInt(sysl[index])) {
        return false;
      }
      if (parseInt(minl[index]) < parseInt(sysl[index])) {
        return true;
      }
      index ++;
    }
    return true;  
  }
  
  updateUserInformation(userObj: any) {
    // console.log(result)
    this.user.username = userObj.NAME;
    this.user.userParameter = userObj.PARAMETERS.TABLE;
    this.user.defaultGroup = userObj.DEFAULT_GROUP;
    this.user.groups = [];
    let privChars = [];
    for (let group of userObj.GROUPS.TABLE) {
      this.user.groups.push(group.NAME);
      privChars.push(group.PRIVS);
    }
    this.user.privs = privChars.filter((value, index, self) => {
      return self.indexOf(value) === index; // returns unqiue values (as long as they are primitive data)
    }).join("");

    this.user.managePrivs = [];
    for (let managePrivs of userObj.MANAGE_PRIVS.TABLE) {
      this.user.managePrivs.push(managePrivs.PRIVS)
    }
  }

  getUserInformation(): Promise<any> {
    return this.commandApiService.execute("SHOW USER")
  }

  setUserInformation(result: any){
    this.updateUserInformation(result.DATA.RECORD);
  }

  getUserParamter(parameterName: string) {
    // console.log(this.user)
    if (this.user.userParameter) {
      for (let o of this.user.userParameter) {
        if (o.NAME == parameterName) {
          return JSON.parse(o.VALUE);
        }
      }
    }
  }

  getFrontendUserParamter(parameterName: string) {
    // console.log(this.fronendUser)
    if (this.fronendUser.userParameter) {
      for (let o of this.fronendUser.userParameter) {
        if (o.NAME == parameterName) {
          try {
            return JSON.parse(o.VALUE)
          }
          catch {
            this.eventEmitterService.pushSnackBarMessage(['JSON PARSE error in the parameter object with name ' + parameterName], SnackbarType.ERROR);
          }
        }
      }
    }
  }

  private createUserParamter(parameterName: string, object: any, user: string) {
    console.warn('create user parameter');
    let stringObject = this.toolBoxService.textQuote(JSON.stringify(object));
    return this.commandApiService.execute("ALTER USER '" + user + "' ADD PARAMETERS = (" + parameterName + ' = \'' + stringObject + '\')').then((result: any) => {
      return result;
    }, (rejection: any) => {
      // do nothing
      return;
    });
  }

  setUserParameter(parameterName: string, object: any, user: string) {
    let stringObject = this.toolBoxService.textQuote(JSON.stringify(object));
    let command = "ALTER USER '" + user + "' ALTER PARAMETERS = (" + parameterName + " = '" + stringObject + "')";
    return this.commandApiService.execute(command, false).then(
      (result: any) => {
        if(result.hasOwnProperty('ERROR')) {
          if(result.ERROR.ERRORCODE == '03110181532') {
            return this.createUserParamter(parameterName, object, user)
          }
          if(result.ERROR.ERRORCODE == '01312181241') {
            // do nothing user has no privs to update FE_PROPERTIES
            this.commandApiService.throwSdmsException(result.ERROR.ERRORCODE, result.ERROR.ERRORMESSAGE, command);
            return Promise.reject(result);
          }

          this.commandApiService.throwSdmsException(result.ERROR.ERRORCODE, result.ERROR.ERRORMESSAGE, command);
          return Promise.reject(result);
        }
        return result;
      }, (result: any)=> {
        return result
      }
    );
  }

  public getUser() {
    return this.user;
  }

  public getSystem() {
    return this.system;
  }

  public reloadUser(): Promise<any> {
    return this.getUserInformation();
  }

  public updatePassword(pw: string) {
    let credentials = this.authService.getCredentials();
    credentials.password = pw;
    this.authService.setCredentials(credentials);
  }

  public updateUsername(n: string) {
    let credentials = this.authService.getCredentials();
    credentials.username = n;
    this.user.username = n;
    this.authService.setCredentials(credentials);
  }
}
