import { Injectable } from '@angular/core';
import { Bookmark } from 'src/app/data-structures/bookmark';
import { CalendarState } from 'src/app/data-structures/CalendarState';
import { RunningDetailState } from 'src/app/data-structures/runningDetailState';
import { SearchJobState } from 'src/app/data-structures/runningJobsState';
import { RunningMasterState } from 'src/app/data-structures/runningMasterState';
import { Tab, TabMode } from 'src/app/data-structures/tab';
import { TreeViewState } from 'src/app/data-structures/treeViewState';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { MainInformationService } from './main-information.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { PrivilegesService } from './privileges.service';

@Injectable({
  providedIn: 'root'
})
export class BookmarkService {

  private bookmarks: Bookmark[] = [];
  // new Bookmark('', BookmarkModes.DETAIL);
  constructor(private mainInformationService: MainInformationService,
    private eventEmitterService: EventEmitterService, private toolBoxService: ToolboxService, private privilegesService: PrivilegesService) {
    this.createDefaults();
  }

  getAllBookmarks() {
    return this.toolBoxService.deepCopy(this.bookmarks);
    // return this.bookmarks;
  }

  getBookmarksOfType(type: string) {
    let bookmarksOfType: Bookmark[] = []
    // console.log(this.bookmarks)

    for (let bookmark of this.bookmarks) {
      if (bookmark.type == type) {

        bookmarksOfType.push(bookmark)
      }
    }
    return this.toolBoxService.deepCopy(bookmarksOfType);
    // return bookmarksOfType;
  }
  getBookmarkOfNameAndType(name: string, type: string) {

    for (let bookmark of this.bookmarks) {
      if (bookmark.bookmark == name && bookmark.type == type) {
        return bookmark;

      }
    }
    return
  }

  setUserBookmark(bookmark: Bookmark, sendNotification: boolean = true): Promise<any> {
    const found = this.bookmarks.find((element: Bookmark) => this.isSameBookmark(element, bookmark));
    // element.bookmark == bookmark.bookmark && element.type == bookmark.type && element.scope == bookmark.scope);
    if (found) {
      this.bookmarks[this.bookmarks.indexOf(found)] = bookmark;
    } else {
      // add new
      this.bookmarks.push(bookmark);
    }
    return this.saveBookmarks(bookmark.type, sendNotification);
  }

  setAllBookmarks(bookmarks: Bookmark[]) {
    this.bookmarks = bookmarks;
    // sync server State
    this.saveBookmarks('');
  }

  deleteUserBookmark(bookmark: Bookmark) {
    const found = this.bookmarks.find((element: Bookmark) => element.bookmark == bookmark.bookmark && element.type == bookmark.type);
    if (found) {
      this.bookmarks.splice(this.bookmarks.indexOf(found), 1);
    } else {
      //nothing to delete
    }
    // sync server State
    this.saveBookmarks(bookmark.type);
  }

  loadBookmarks() {
    let userBookmarks: Bookmark[] = this.mainInformationService.getUserParamter('BOOKMARKS');
    let frontEndBookmarks: Bookmark[] = this.mainInformationService.getFrontendUserParamter('BOOKMARKS');
    // this.bookmarks contains the default Bookmarks,
    // initilize bookmarks with this.bookmarks, so there is a chance to replace them with system or user bookmarks!
    let bookmarksFromServer: Bookmark[] = this.toolBoxService.deepCopy(this.bookmarks);
    Array.isArray(userBookmarks) ? bookmarksFromServer = bookmarksFromServer.concat(userBookmarks) : console.warn('wrong USER Bookmark format from server');
    Array.isArray(frontEndBookmarks) ? bookmarksFromServer = bookmarksFromServer.concat(frontEndBookmarks) : console.warn('wrong SYSTEM Bookmark format from server');

    if (Array.isArray(bookmarksFromServer)) {
      for (let bookmarkFromServerObject of bookmarksFromServer) {
        let matchingBookmark: Bookmark | undefined = this.bookmarks.find(bookmark =>
          this.isSameBookmark(bookmark, bookmarkFromServerObject)
          // (bookmark.bookmark == bookmarkFromServerObject.bookmark) && (bookmark.type == bookmarkFromServerObject.type) && (bookmark.scope == bookmarkFromServerObject.scope)
        );
        if (matchingBookmark) {
          this.bookmarks[this.bookmarks.indexOf(matchingBookmark)] = bookmarkFromServerObject;
        } else {
          // add if new
          this.bookmarks.push(bookmarkFromServerObject);
        }
      }
    }
  }

  isSameBookmark(a: Bookmark, b: Bookmark): boolean {
    return ((a.bookmark == b.bookmark) && (a.type == b.type) && (a.scope == b.scope))
  }

  clean() {
    this.bookmarks = [];
    this.createDefaults();
  }

  saveBookmarks(calltype: string, sendNotification: boolean = true): Promise<any> {

    // create 2 lists for user and schedulix_fe user
    let systemBookmarks: Bookmark[] = [];
    let userBookmarks: Bookmark[] = [];
    for (let bookmark of this.bookmarks) {
      bookmark.scope == 'SYSTEM' ? systemBookmarks.push(bookmark) : userBookmarks.push(bookmark);
    }

    // is going to resolve if both functions have resolved
    return Promise.all([
      this.mainInformationService.setUserParameter('BOOKMARKS', userBookmarks, this.mainInformationService.getUser().username),
      this.privilegesService.isAdmin() ? this.mainInformationService.setUserParameter('BOOKMARKS', systemBookmarks, 'FE_PROPERTIES'): ''
    ]).then((result: any) => {
      // is going to resolve if new information is present from the server
      return Promise.all([
        this.mainInformationService.getFrontendUserInformation(),
        this.mainInformationService.getUserInformation()
      ]).then((result: any) => {
        // set the new data in the main information service
        this.privilegesService.isAdmin() ? this.mainInformationService.setFrontendUserInformation(result[0]) : '';
        this.mainInformationService.setUserInformation(result[1]);

        // update bookmarkTab
        // has to be the same values as in the left navigation, so that the bookmarking tab will be updated -> check changes here and in left-navigation
        let tab: Tab = new Tab('Bookmarking', 'bookmarking', 'Bookmarking'.toUpperCase(), 'bookmark', TabMode.EDIT);

        // if bookmarking tab is opened it will be updated
        this.eventEmitterService.updateTabEmitter.next(tab);
        // trigger subscribers that user has been loaded
        sendNotification ? this.mainInformationService.userHasLoadedObserver.next(calltype): '';
        return;
      });

    });
  }

  createDefaults() {

    let existingTypes: string[] = ['DETAIL', 'FOLDER', 'MASTER', 'SEARCH', 'SUBMIT', 'CALENDAR']

    for (const value of existingTypes) {

      //go through all types and create Default if not created before

      if (this.getBookmarkOfNameAndType('DEFAULT', value)) {

        // already default bookmark exist
        //console.log("default bookmark already exist")
      } else {

        // console.log("default created")
        // console.log(value)
        // no default bookmark exist
        let bookmark = new Bookmark('DEFAULT', value);
        bookmark.scope = 'SYSTEM';

        // empty yet
        bookmark.value = {};

        // TODO, ==> Enno rework!
        if (value == 'FOLDER') {
          bookmark.value = new TreeViewState();
        }
        if (value == 'MASTER') {
          bookmark.value = new RunningMasterState();
        }
        if (value == 'SEARCH') {
          // TODO RUNNING SEARCH STATE
          bookmark.value = new SearchJobState();
        }
        if (value == 'SUBMIT') {
          bookmark.value = new TreeViewState();
        }

        if(value == 'DETAIL') {
          bookmark.value = new RunningDetailState();
        }

        if(value == 'CALENDAR') {
          bookmark.value = new CalendarState();
        }

        this.bookmarks.push(bookmark);
        // this.setUserBookmark(bookmark)
      }


    }
    //console.log(this.bookmarks)

  }

  openAutostartTabs() {
    // console.log("AUTOSTART BOOKMARKS ARE GONNA BE STARTED HERE")
    // let qq = new RunningMasterState();
    for (let bookmark of this.bookmarks) {
      if (bookmark.type == 'MASTER') {
        // console.log(bookmark.value.settingsObject.AUTOSTART)
        if (bookmark.autostart == 'YES') {
          // console.log("START NEW TAB WITH MASTER BOOKMARK: " + bookmark.bookmark)
          this.addTab(bookmark);
        }
      }
      if (bookmark.type == 'SEARCH') {
        // console.log("OPEN SEARCH BOOKMARK")
        // console.log(bookmark.value.settingsObject.AUTOSTART)
        if (bookmark.autostart == 'YES') {
          // console.log("START NEW TAB WITH SEARCH BOOKMARK: " + bookmark.bookmark)
          this.addTab(bookmark);
        }
      }
    }
  }

  getNewBookmarkDialogData(selectedBookmark: Bookmark){
    return {
      title: "save_bookmark_as",
      translate: "choose_bookmark_scope",
      itemTypes: this.privilegesService.isAdmin() ? ["USER", "SYSTEM"] : ["USER"],
      inputEnabled: true,
      iconsDisabled: true,
      input: selectedBookmark.bookmark
    }
  }

  addTab(bookmark: Bookmark) {
    // let tab = new Tab();
    switch (bookmark.type) {
      case "MASTER":
        let monMasterTab = new Tab('Monitor Master ' + bookmark.bookmark, 'Monitor Master ' + bookmark.bookmark, 'Monitor Master'.toLocaleUpperCase(), 'runmajobs', TabMode.MONITOR_MASTER)
        this.eventEmitterService.createTab(monMasterTab, false);
        break;

      case "SEARCH":
        let monSearchTab = new Tab('Monitor Jobs ' + bookmark.bookmark, 'Monitor Jobs ' + bookmark.bookmark, 'Monitor Jobs'.toLocaleUpperCase(), 'monitorjobs', TabMode.MONITOR_JOBS)
        this.eventEmitterService.createTab(monSearchTab, false);
        break;

    }

  }

  isSelectedBookmarkEditable(selectedBookmark: Bookmark): boolean {
    if(selectedBookmark.scope == 'USER'){
      return true;
    } else if(selectedBookmark.scope == 'SYSTEM' && this.privilegesService.isAdmin()) {
      return true;
    } else {
      return false
    }
  }

}
