import { SelectionModel } from '@angular/cdk/collections';
import { Injectable } from '@angular/core';
import { detailFilterObject, detailsettingsObject } from 'src/app/data-structures/runningDetailState';
import { SearchFilterObject, SearchSettingsObject } from 'src/app/data-structures/runningJobsState';
import { rmjFilterObject, rmjSettingsObject } from 'src/app/data-structures/runningMasterState';
import { treeNode, masterTreeNode } from '../../../interfaces/tree'
import { PrivilegesService } from './privileges.service';
import constants from '../../../json/constants.json';
import { ChooserDialogData } from '../components/form-generator/chooser/chooser-dialog/chooser-dialog.component';
import { ToolboxService } from 'src/app/services/toolbox.service';

@Injectable({
  providedIn: 'root'
})
export class TreeFunctionsService {

  constructor(private privilegesService: PrivilegesService, private toolboxService: ToolboxService) { }

  compare(a: treeNode | masterTreeNode, b: treeNode | masterTreeNode) { // for sorting

    // Use toUpperCase() to ignore character casing
    const bandA = a.type.toUpperCase().length;
    const bandB = b.type.toUpperCase().length;
    let comparison = 0;
    if (bandA > bandB) {
      comparison = -1;
    } else if (bandA < bandB) {
      comparison = 1;
    }
    return comparison;
  }

  insertMasterNode(parent: masterTreeNode, children: masterTreeNode) {
    if (parent.children) {
      if (parent.children.length > 0) {

        // WC O(n), BC(1), AC(1)
        for (var i = parent.children.length - 1; i >= 0; i--) {
          if (children.hasChildren) {
            if (parent.children[i].hasChildren) {
              //insert in array
              parent.children.splice(i + 1, 0, children);
              return;
            }
          } else {
            parent.children.push(children);
            return;
          }
        }
        //if nothing in the for loup that has children, just insert at first
        parent.children.splice(0, 0, children);
        return;
      } else {
        parent.children.push(children);
        return;
      }
    } else {
      parent.children = [];
      parent.children.push(children);
      return;
    }
  }

  insertNode(parent: treeNode | any, children: treeNode | any) {
    if (parent.children) {
      if (parent.children.length > 0) {

        // WC O(n), BC(1), AC(1)
        for (var i = parent.children.length - 1; i >= 0; i--) {
          if (children.hasChildren) {
            if (parent.children[i].hasChildren) {
              //insert in array
              parent.children.splice(i + 1, 0, children);
              return;
            }
          } else {
            parent.children.push(children);
            return;
          }
        }
        //if nothing in the for loup that has children, just insert at first
        parent.children.splice(0, 0, children);
        return;
      } else {
        parent.children.push(children);
        return;
      }
    } else {
      parent.children = [];
      parent.children.push(children);
      return;
    }
  }

  checkCssSize(level: number, name: string): number {
    let size = level * 20; // level size
    size += name.length * 8; // letter size
    size += 38; // hr size
    return size;
  }

  calcWidth(o: string) {
    if (o == 'submit_ts' || o == 'final_ts') {
      return '1 0 150px'
    } else if (o == 'treeNode') {
      return '1 0 240px'
    } else if (o == 'ID') {
      return '1 0 60px'
    } else if (o == 'runtime') {
      return '1 0 85px'
    } else if (o == 'displayState') {
      return '1 0 125px'
    } else if (o == 'options') {
      return '1 0 104px'
    } else {
      return '1 0 125px'
    }
  }
  order(treeNodes: any[], sortOrder: string) {
    // console.log("sort by: "+sortOrder)
    if (sortOrder == 'DESCENDING') {
      treeNodes.sort(function (a, b) {

        if (b.level == a.level) {
          if(a.STATE == 'SCHEDULED') {
            return -1;
          }
          return b.id - a.id;
        }
        return 0;
      });
      // console.log('DESCENDING')
      // console.log(treeNodes)

      // console.log("sort done")
      return treeNodes;
    }
    if (sortOrder == 'ASCENDING') {
      treeNodes.sort(function (a, b) {
        if (b.level == a.level) {
          if(a.STATE == 'SCHEDULED') {
            return 1;
          }
          return a.id - b.id;
        }
        return 0;
      });
      // console.log('ASCENDING')
      // console.log(treeNodes)


      // console.log("sort done")
      return treeNodes;
    }


    return treeNodes;
  }

  unflattenTree(list: any[]) {
    for (let i = 0; i < list.length; i++) {
      if (list[i].unflat_children == undefined) {
        // console.log("push")
        list[i].unflat_children = [];
      }
      for (let j = parseInt(list[i].index) + 1; j < list.length; j++) {
        if (String(list[j].path).startsWith(list[i].path + ':')) {
          if ((list[j].level - list[i].level) == 1) {
            list[i].unflat_children.push(list[j])
          }
        } else {
          break;
        }
      }
    }
    for (let i = 0; i < list.length; i++) {
      if (list[i].level > 1) {
        list.splice(i, 1);
        i--;
      }
    }
  }

  sortUnflattenTree(nestedList: any[], sortOrder: string) {
    let order = 1;
    if (sortOrder == 'DESCENDING') {
      order = -1;
    }
    nestedList.sort((a, b) => {
      // if (b.level == a.level) {
      let comp_a = a.end ? a.end : a.start;
      let comp_b = b.end ? b.end : b.start;
      if (comp_a == comp_b) {
        comp_a = a.name;
        comp_b = b.name;
      }
      if (comp_a == comp_b) {
        comp_a = a.namePath;
        comp_b = b.namePath;
      }
      if (comp_a == comp_b) {
        comp_a = a.id;
        comp_b = b.id;
      }
      if (comp_a > comp_b) {
        return order;
      }
      return -order;
    });
    for (let node of nestedList) {
      this.sortUnflattenTree(node.unflat_children, sortOrder);
    }
  }

  flattenTree(nestedList: any[]) {
    for (let i = 0; i < nestedList.length; i++) {

      if (nestedList[i].unflat_children.length > 0) {
        // console.log( nestedList[i].unflat_children.length)
        nestedList.splice(i + 1, 0, ...nestedList[i].unflat_children);
        nestedList[i].unflat_children = [];
      }
    }
  }

  flatTreeorder(treeNodes: any[], sortOrder: string) {


    // build nested tree, sort, flatten tree again
    this.unflattenTree(treeNodes);
    this.sortUnflattenTree(treeNodes, sortOrder);
    this.flattenTree(treeNodes);


    return treeNodes;
  }

  getTabColorEvaluation(stateList: string[]) {
    // console.log(stateList)
    // from high to low priority
    // restartable = red
    if (stateList.includes('TEXT_RED')) {
      return 'RED';
    }

    // waiting for recources = purple
    if (stateList.includes('TEXT_ORANGE')) {

      return 'ORANGE';
    }
    if (stateList.includes('TEXT_PURPLE')) {

      return 'PURPLE';
    }
    // Jobs are running = blue
    if (stateList.includes('TEXT_BLUE')) {
      return 'BLUE';
    }

    // sth has ben cancelled = brown
    if (stateList.includes('TEXT_BROWN')) {
      return 'BROWN';
    }

    // everthing is FINAL = GREEN
    if (stateList.includes('FINAL')) {
      return 'GREEN';
    }

    // no states left = grey
    return 'GREY'

  }
  msToTime(s: number): string {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;    
    var mins = s % 60;
    s = (s - mins) / 60;    
    var hrs = s % 24;
    var days = (s - hrs) / 24;

    var result = '';
    if (days) result += days + 'd';
    if (hrs || result) result += hrs + 'h';
    if (mins || result) result += mins + 'm';
    result += secs + 's';
    if (result == '0s') {
      result = '<1s';
    }
    return result;
  }

  getDummyDialogTreeNode(name: string): treeNode {
    let node: treeNode = {
      id: -1,
      name: name, // get last element
      namePath: '',
      isPinned: false,
      isClickable: false,
      // this.pinnedList.indexOf(parseInt(node.ID, 10)) > -1

      isPinnedParent: false,
      isInvisible: false,

      isProperty: true,
      isPropertyParent: false,

      iconType: "other",
      hasChildren: false,
      type: "other",

      isNotSubmitable: true,
      bicsuiteObject: undefined,
      textColor: ''
    }
    return node;
  }

  startTime(node: any): string {

    // TODO DATE CONVERSION
    // console.log(node)
    let d = '';
    if (node.STATE == 'SCHEDULED') {
      // console.log('node.SUBMIT_TS')
      d = node.SUBMIT_TS
    } else {
      if (node.RESUME_TS == null) {
        if (node.SE_TYPE == 'BATCH' || node.SE_TYPE == 'MILESTONE') {
          // console.log('node.FINISH_TS')
          d = node.FINISH_TS
        } else {
          // console.log('node.START_TS')
          d = node.START_TS
        }
      } else {
        // console.log('node.RESUME_TS')
        d = node.RESUME_TS
      }
    }
    return d;
  }
  
  checkBulkSelectable(node: any): boolean {
    return node.state != 'FINAL' && node.state != 'CANCELLED' && node.is_cancelled == 'false' && node.STATE != 'SCHEDULED';
  }

  checkBulkOperationsAllowed(operation: string, checklistSelection: SelectionModel<masterTreeNode>): boolean {
    if (checklistSelection.selected.length == 0) {
      return false;
    }

    for (let selectedNode of checklistSelection.selected) {
      // if (states.includes(selectedNode.state)) {
      //   return true;
      // }
      if (['FINAL', 'CANCELLED'].includes(selectedNode.state) || selectedNode.is_cancelled == "true") {
        return false;
      }
      switch (operation) {
        case "cancel":
          if (!selectedNode.normalizedPrivs.includes("c")) { return false; }
          // already checked because FINAL and CANCELLED/CANCELLING submitted entities do not alloow any operator action
          // if (['FINAL', 'CANCELLED'].includes(selectedNode.state) || selectedNode.is_cancelled == "true") {
          //   return false;
          // }
          break;
        case "rerun":
          if (!selectedNode.normalizedPrivs.includes("r")) { return false; }
          if (selectedNode.is_restartable == "false" && selectedNode.cnt_restartable == '0') {
            return false;
          }
          break;
        case "resume":
          if (!selectedNode.normalizedPrivs.includes("u")) { return false; }
          if (selectedNode.is_suspended == 'NOSUSPEND' || (selectedNode.is_suspended == 'ADMINSUSPEND' && !this.privilegesService.isAdmin())) {
            return false;
          }
          break;
        case "suspend":
          if (!selectedNode.normalizedPrivs.includes("u")) { return false; }
          // check if suspedended or admin suspended -> only admin can supsend further
          if (!this.privilegesService.isAdmin() && selectedNode.is_suspended != 'NOSUSPEND') {
            return false;
          }
          break;
        case "enable":
          if (!selectedNode.normalizedPrivs.includes("e")) { return false; }
          // check if suspedended or admin suspended -> only admin can supsend further
          if (selectedNode.is_disabled == 'false' || selectedNode.state != 'DEPENDENCY_WAIT') {
            return false;
          }
          break;
        case "disable":
          if (!selectedNode.normalizedPrivs.includes("e")) { return false; }
          // check if suspedended or admin suspended -> only admin can supsend further
          if (selectedNode.is_disabled == 'true' || selectedNode.state != 'DEPENDENCY_WAIT') {
            return false;
          }
          break;
      }
    }

    return true;
  }

  applyParameter(settingsObject: SearchSettingsObject | detailsettingsObject | rmjSettingsObject, displayedColumns: any[]): string {
    let command = '';
    command = command + ", PARAMETERS = ('DETAIL_BOOKMARK'";
    if (settingsObject.hasOwnProperty('PARAMETERS')) {
      for (let row of settingsObject.PARAMETERS.TABLE) {
        command = command + ", '" + row.PARAMETER_NAME + "'";
        let displayedIndex = displayedColumns.findIndex(element => element.parameter_name == 'P_' + row.PARAMETER_NAME);
        // TODO not working perfectly yet for both trees
        // update displayed columns before loading the new table/tree
        // console.log(row.LABEL == '')
        displayedIndex >= 0 ? displayedColumns[displayedIndex] = {
          parameter_name: 'P_' + row.PARAMETER_NAME,
          color: row.COLOR,
          label: row.LABEL ? row.LABEL : row.PARAMETER_NAME,
          align: row.ALIGN
        } : displayedColumns.push({
          parameter_name: 'P_' + row.PARAMETER_NAME,
          color: row.COLOR,
          label: row.LABEL == '' ? row.PARAMETER_NAME : row.LABEL,
          align: row.ALIGN
        });
        // console.log(this.displayedColumns)
      }
    }
    command = command + ')';
    return command;
  }

  applyNameLike(filtersObject: SearchFilterObject | detailFilterObject | rmjFilterObject): string {
    let command = '';
    filtersObject.NAME_PATTERN.TABLE.length != 0 ? command += '(' : '';
    for (let namePattern of filtersObject.NAME_PATTERN.TABLE) {
      if (!namePattern.NAME) continue;
      if (filtersObject.NAME_PATTERN.TABLE.indexOf(namePattern) > 0) {
        let pattern = namePattern.NAME.replace('_', '\\_').replace('%', '\%');
        command += ' OR NAME LIKE \'(?i)%' + pattern + '%\''
        // console.log(namePattern.NAME)
      } else {

        let pattern = namePattern.NAME.replace('_', '\\_').replace('%', '\%');
        command += 'NAME LIKE \'(?i)%' + pattern + '%\''
        // console.log(namePattern.NAME)
      }

    }
    filtersObject.NAME_PATTERN.TABLE.length != 0 ? command += ')' : '';
    if (command == "()") command = "";
    return command;
  }

  applyMergedExitStates(filtersObject: SearchFilterObject | detailFilterObject | rmjFilterObject): string {
    let command = '';
    let validStates: number = 0;
    //MERGED EXIT STATES
    filtersObject.FILTER_ESD.TABLE.length != 0 ? command += 'MERGED EXIT STATUS IN (' : '';
    // console.log(filtersObject.FILTER_ESD.TABLE)
    for (let esd of filtersObject.FILTER_ESD.TABLE) {
      let esdName = esd.ESD == '' ? 'EMPTY' : esd.ESD;
      if (filtersObject.FILTER_ESD.TABLE.indexOf(esd) > 0) {
        command += ',\'' + esdName + '\''
        validStates++;
      } else {
        command += '\'' + esdName + '\''
        validStates++;
      }
    }
    filtersObject.FILTER_ESD.TABLE.length != 0 ? command += ')' : '';

    validStates == 0 ? command = '' : '';
    return command;
  }

  applyJobStates(filtersObject: SearchFilterObject | detailFilterObject | rmjFilterObject): string {
    let command = '';
    let validStates: number = 0;

    filtersObject.FILTER_JOB_STATES.TABLE.length != 0 ? command += '(' : '';
    let jobStates: string[] = [];
    for (let jobstate of filtersObject.FILTER_JOB_STATES.TABLE) {


      let jobStateName = jobstate.JOB_STATE == '' ? 'EMPTY' : jobstate.JOB_STATE;
      // console.log(filtersObject.FILTER_JOB_STATES.TABLE)
      if (constants.jobStates.includes(jobStateName)) {
        jobStates.push(jobStateName)
        validStates++;
      }

    }
    let jobStateString = jobStates.join(',')
    jobStateString == '' ? '' : jobStateString = 'JOB_STATUS IN (' + jobStateString + ')'
    command += jobStateString;
    let virtualJobStates: string[] = [];
    for (let jobstate of filtersObject.FILTER_JOB_STATES.TABLE) {

      let jobStateName = jobstate.JOB_STATE == '' ? 'EMPTY' : jobstate.JOB_STATE;
      if (constants.virtualJobStates.includes(jobStateName)) {
        virtualJobStates.push(jobStateName)
        validStates++;
      }
    }
    if (jobStates.length != 0 && virtualJobStates.length != 0) {
      command += ' OR '
    }
    let virtualJobStatesString = virtualJobStates.join(' OR ')
    command += virtualJobStatesString;
    filtersObject.FILTER_JOB_STATES.TABLE.length != 0 ? command += ')' : '';
    validStates == 0 ? command = '' : '';
    return command;
  }

  // selectionIsHasState(checklistSelection: SelectionModel<masterTreeNode>, states: string[], operationString: string): boolean {

  //   for (let selectedNode of checklistSelection.selected) {
  //     if (states.includes(selectedNode.state)) {
  //       return true;
  //     }
  //   }
  //   return false;
  // }

  private dialogNodeIsPropertyParentNode(node: any, dialogData: ChooserDialogData) {
    // console.log(node.type)
    // console.log(this.dialogData.chooseParentType)
    if (dialogData.chooseParentType == undefined)
      return false;

    if (Array.isArray(dialogData.chooseParentType)) {
      return dialogData.chooseParentType.find(e => e == node.type) != undefined;
    }
    else if (this.toolboxService.isPrimitive(dialogData.chooseParentType)) {
      return dialogData.chooseParentType == node.type;
    }

    return false;
  }


  private dialogNodeIsChoosable(node: any, dialogData: ChooserDialogData) {
    if (dialogData.chooseType == undefined)
      return false;

    if (Array.isArray(dialogData.chooseType)) {
      return dialogData.chooseType.find(e => e == node.type) != undefined;
    }
    else if (this.toolboxService.isPrimitive(dialogData.chooseType)) {
      return dialogData.chooseType == node.type;
    }

    return false;
  }

  public _dialogTransformer = (node: any, dialogData: ChooserDialogData) => {

    // console.log(node)
    // console.log(parseFloat(node.SUBCATEGORIES) > 0)
    // console.log(parseFloat(node.SUBFOLDERS) > 0)
    // console.log(parseFloat(node.ENTITIES) > 0)
    // console.log(parseFloat(node.SUBSCOPES) > 0)
    // console.log(parseFloat(node.RESOURCES) > 0)
    // console.log(this.dialogData.chooseType);
    node.type = node.TYPE ? node.TYPE : node.USAGE ? node.USAGE : "other";
    let treeNode: treeNode = {

      // TODO: takeover from node for all properties

      id: parseInt(node.ID),
      name: node.NAME.split('.').pop(), // get last element
      namePath: node.NAME,
      isPinned: false,
      isClickable: true,

      isPinnedParent: false,
      isInvisible: false,
      isPropertyParent: this.dialogNodeIsPropertyParentNode(node, dialogData),
      isProperty: node.f_isProperty ? node.f_isProperty : this.dialogNodeIsChoosable(node, dialogData),
      iconType: node.TYPE ? node.TYPE.toLowerCase() : node.USAGE ? node.USAGE.toLowerCase() : "other",
      hasChildren: ((node.TYPE == 'FOLDER' || node.TYPE == 'SCOPE' || node.TYPE == undefined) && (parseFloat(node.SUBCATEGORIES) > 0 || parseFloat(node.SUBFOLDERS) > 0 || parseFloat(node.ENTITIES) > 0 || parseFloat(node.SUBSCOPES) > 0 || parseFloat(node.RESOURCES) > 0)),
      type: node.type,

      isNotSubmitable: node.MASTER_SUBMITTABLE == 'false',
      bicsuiteObject: node,
      textColor: ''
    }
    // console.log(treeNode)

    return treeNode;
  }
}

