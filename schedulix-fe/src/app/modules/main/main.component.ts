
import { ɵNoopAnimationStyleNormalizer } from '@angular/animations/browser';
import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { Tab, TabMode, TabState } from 'src/app/data-structures/tab';
import { AuthService } from 'src/app/services/auth.service';
import { EventEmitterService } from 'src/app/services/event-emitter.service';
import { IconRegistryService } from 'src/app/services/icon-registry.service';
import { LocalStorageHelper } from 'src/app/services/local-storage-helper.service';
import { ToolboxService } from 'src/app/services/toolbox.service';
import { BookmarkService } from './services/bookmark.service';
import { MainInformationService } from './services/main-information.service';
import { PrivilegesService } from './services/privileges.service';
import { SnackbarData, SnackbarType } from '../global-shared/components/snackbar/snackbar-data';
import { SnackbarComponent } from '../global-shared/components/snackbar/snackbar.component';
import { MatLegacySnackBar as MatSnackBar } from '@angular/material/legacy-snack-bar';
import { MatLegacyTooltipDefaultOptions as MatTooltipDefaultOptions } from '@angular/material/legacy-tooltip';
import { CRUDService } from 'src/app/services/crud.service';
import { TranslateService } from 'src/app/services/translate.service';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { EditorformDialogComponent, EditorformDialogData } from './components/form-generator/editorform-dialog/editorform-dialog.component';

export const myCustomTooltipDefaults: MatTooltipDefaultOptions = {
  showDelay: 1000,
  hideDelay: 1000,
  touchendHideDelay: 1000,
};
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, OnDestroy {

  constructor(private authService: AuthService,
    public localStorageHelper: LocalStorageHelper,
    private mainInformationService: MainInformationService,
    private iconRegistryService: IconRegistryService,
    private eventEmitterService: EventEmitterService,
    private toolboxService: ToolboxService,
    private bookmarkService: BookmarkService,
    private changedectionRef: ChangeDetectorRef,
    private privilegesService: PrivilegesService,
    protected dialog: MatDialog,
    private translatorService: TranslateService,
    private crudService: CRUDService,
    private snackbar: MatSnackBar) {
  }



  public tabs: Tab[] = []

  selectedIndex: number = 0;

  rxjsCreateTabSubscription: Subscription = new Subscription();
  rxjsUpdateTabSubscription: Subscription = new Subscription();
  rxjsRemoveTabSubscription: Subscription = new Subscription();
  rxjsUserHasLoadedObserver: Subscription = new Subscription();
  rxjsSaveTabSubscription: Subscription = new Subscription();
  rxjsSnackbarSubscription: Subscription = new Subscription();

  loadedTabs: number[] = [];

  isInitialized: boolean = false;

  @ViewChild('tabGroup') tabGroup: any;

  ngOnInit(): void {

    this.mainInformationService.login().then(() => {
      this.isInitialized = true;
    });

    // TODO nach oben
    this.rxjsUserHasLoadedObserver = this.mainInformationService.userHasLoadedObserver.subscribe((subject: any) => {
      this.bookmarkService.loadBookmarks();




      // open all tabs when bookmarks / user has been saved ? TODO
      this.openTabs();

      // if bookmarks first loaded, open all bookmark autostart
      if (subject == 'login') {
        this.bookmarkService.openAutostartTabs();
      }
    });

    this.rxjsCreateTabSubscription = this.eventEmitterService.createTabEmitter.subscribe(({ tab, target }) => {
      this.addTab(tab, target);
    });
    this.rxjsUpdateTabSubscription = this.eventEmitterService.updateTabEmitter.subscribe((tab: Tab) => {
      this.updateTab(tab);
    });

    this.rxjsRemoveTabSubscription = this.eventEmitterService.removeTabEmitter.subscribe((tab: Tab) => {
      this.doRemoveTab(tab);
    });

    this.rxjsSaveTabSubscription = this.eventEmitterService.saveTabEmitter.subscribe((tab: Tab) => {
      this.saveTabs();
    });

    this.rxjsSnackbarSubscription = this.eventEmitterService.snackBarMessageEmittor.subscribe((event) => {
      this.showSnackBarMessage(event);
    });
  }

  ngOnDestroy(): void {
    this.rxjsCreateTabSubscription.unsubscribe();
    this.rxjsUpdateTabSubscription.unsubscribe();
    this.rxjsSaveTabSubscription.unsubscribe();
    this.rxjsRemoveTabSubscription.unsubscribe();
    this.rxjsUserHasLoadedObserver.unsubscribe();
    this.rxjsSnackbarSubscription.unsubscribe();

    this.mainInformationService.clean();
    this.bookmarkService.clean();
  }

  onTabChanged(event: any) {
    // console.log(this.loadedTabs.includes(event.index))
    //    if(!this.loadedTabs.includes(event.index)) {
    //      this.loadedTabs.push(event.index)
    //    }
    //  console.log(this.loadedTabs)
    // index -1 == no element left
    let index = event.index;
    while (!(this.tabs[index]) && index > -1) {
      index--;
    }
    if (index > -1) {
      let tab: Tab = this.tabs[index]
      this.eventEmitterService.selectedTabEmitter.next(tab.ID);
      this.localStorageHelper.setConnectionValue("activeTabID", this.selectedIndex);
    } else {
      this.eventEmitterService.selectedTabEmitter.next('');
    }
  }

  scrollTabs(event: any) {
    const children = this.tabGroup._tabHeader._elementRef.nativeElement.children;

    // get the tabGroup pagination buttons
    const back = children[0];
    const forward = children[2];

    // depending on scroll direction click forward or back
    if (event.deltaY > 0) {
      forward.click();
    } else {
      back.click();
    }
  }



  addTab(tab: Tab, target: boolean) {
    // console.log("_-----------------------------")
    if (!this.searchTab(tab, target)) {
      tab.updateTooltip();
      this.tabs.push(tab);
      setTimeout(() => {
        if (target == true) {
          this.selectedIndex = this.tabs.length - 1;  // no real timeout, good solution to que the command in the end of angular lifecycle hook, so that the tab is created before you try to select it...
        }
      });
      this.saveTabs();
    }
  }

  findTabCondition(tabInList: Tab, tabToFind: Tab): boolean {
    if (tabInList.TYPE != tabToFind.TYPE) {
      return false;
    }
    // console.log(tabToFind.ID);
    // console.log(tabInList.ID);
    if (tabToFind.ID) {
      return tabInList.ID == tabToFind.ID;
    }
    if (tabToFind.NAME != tabInList.NAME) {
      return false;
    }
    if (((tabToFind.PATH) && (tabInList.PATH)) && (tabToFind.PATH != tabInList.PATH)) {
      return false;
    }
    if (tabToFind.OWNER && tabInList.OWNER) {
      return tabInList.OWNER == tabToFind.OWNER;
    }
    return true;
  }

  searchTab(tab: Tab, target: boolean): boolean {
    var obj = this.tabs.find(o => this.findTabCondition(o, tab));
    if (obj) {
      if (target == true) {
        this.selectedIndex = this.tabs.indexOf(obj);
      }
      return true;
    } else {
      return false;
    }
  }

  saveTabs() {
    let stringArray = [];
    for (let o of this.tabs) {
      // let oString = JSON.stringify(o);

      // // dont save data
      // let f = this.toolboxService.deepCopy(o)
      // f.DATA = '';
      stringArray.push(o);
    }
    this.localStorageHelper.setConnectionValue("tabList", stringArray);
  }

  openTabs() {
    if (!this.localStorageHelper.getConnectionValue("tabList")) {
      this.localStorageHelper.setConnectionValue("tabList", "");
    } else {
      let tabList = this.localStorageHelper.getConnectionValue("tabList");
      let activeTab = this.localStorageHelper.getConnectionValue("activeTabID");

      let assignedTabs: Tab[] = [];
      for (let tab of tabList) {
        let t = new Tab('', '', '', '', TabMode.EDIT)
        let ts = new TabState();
        // correct assigning of TAB object
        tab = Object.assign(t, tab);
        tab.TABSTATE = Object.assign(ts, tab.TABSTATE);
        tab.updateTooltip();
        assignedTabs.push(tab);
      }
      this.tabs = assignedTabs;
      if (this.tabs.length > 0) {
        if (activeTab != '') {
          this.selectedIndex = activeTab;
          this.eventEmitterService.selectedTabEmitter.next(this.tabs[this.selectedIndex].ID);
        } else {
          this.eventEmitterService.selectedTabEmitter.next(this.tabs[0].ID);
        }
      }
    }
    this.changedectionRef.detectChanges();
  }

  updateTab(tab: Tab) {
    // console.log(tab)
    let updateItem = this.tabs.find(o => o.ID == tab.ID && o.TYPE == tab.TYPE);

    if (updateItem != undefined) {
      tab.updateTooltip();
      let index = this.tabs.indexOf(updateItem);
      this.tabs[index] = tab;

      this.saveTabs(); // TODO: ave tab has race conditions
    } else {
      // console.log(tab)
      // this.eventEmitterService.pushSnackBarMessage(['ID not found in Tablist'], SnackbarType.ERROR)
      console.warn('ID not found in Tablist');
    }
  }

  createCloseConfirm(tab: Tab): Promise<any> {
    let dialogForm = {
      "FIELDS": [
        {
          "NAME": "confirmTextFile",
          "USE_LABEL": false,
          "TYPE": "TEXT",
          "TRANSLATE": "Do you really want to lose your changes ?",
          "CSS_CLASS": "confirm-text"
        },
        {
          "NAME": "confirmText",
          "USE_LABEL": false,
          "TYPE": "TEXT",
          "TRANSLATE": "Do you really want to lose your changes ?",
          "CSS_CLASS": "confirm-text"
        }
      ],
      "DIALOG_BUTTONS": [
        {
          "MODE": "CLOSE",
          "TRANSLATE": "no",
          "SYS_OBJ_EDITABLE": true,
          "STROKED": true,
        },
        {
          "MODE": "CLOSE_WITH_DATA",
          "SYS_OBJ_EDITABLE": true,
          "METHOD": "confirm",
          "TRANSLATE": "yes"
        }
      ]
    };

    let dataObject = {
      "confirmTextFile": this.translatorService.translate("changes_in_file"),
      "confirmText": this.translatorService.translate("unsaved_file_lose_changes")
    }

    const dialogRef = this.dialog.open(EditorformDialogComponent, {
      data: new EditorformDialogData(dialogForm, dataObject, tab, this.translatorService.translate("confirm"), this.crudService.createOrGetCrud(tab.TYPE)),
      panelClass: [this.mainInformationService.getThemeString(), 'background-theme-primary', 'fe-no-border-dialog'],
      width: "800px"
    });

    return dialogRef.afterClosed().toPromise().then((result: any) => {
      //console.log(result);
      if (result != undefined || result != null && result) {

        if (result.hasOwnProperty('ERROR')) {
          return 1;
        } else {
          // console.log(result)
          return result;
        }
        // });
      } else {
        return 1;
      }

    });
  }

  removeTab(tab: Tab) {

    // check if the tab is dirty -> create confirm dialog
    if (tab.IS_DIRTY) {
      this.createCloseConfirm(tab).then((result: any) => {
        console.log(result)
        if (result && result != 1) {
          // confirmed and can be dropped
          this.doRemoveTab(tab);
        } else {
          // dirty and user want to keep changes do nothing

        }
      });
    } else {
      this.doRemoveTab(tab);
    }

  }

  doRemoveTab(tab: Tab) {
    //find the tab with the id, since the tab element from the emittor has maybe changed in name
    let updateItem = this.tabs.find(o => o.ID == tab.ID && o.TYPE == tab.TYPE);
    if (updateItem != undefined) {
      let index = this.tabs.indexOf(updateItem);
      this.tabs.splice(index, 1);

      // selectedIndex is currupt after deletion, this fixes it
      if (index < this.selectedIndex) {
        this.selectedIndex--;
      }
      // change isactive values before fire rxjs messages to child components
      this.changedectionRef.detectChanges();

      // check if selectedIndex is in the tabList, if not - the tab is changed anyway see onTabChange(), selectedTabEmittor.next is triggered there
      if (this.tabs[this.selectedIndex]) {
        this.eventEmitterService.selectedTabEmitter.next(this.tabs[this.selectedIndex].ID);
        this.localStorageHelper.setConnectionValue("activeTabID", this.selectedIndex);
        // this.selectedIndex = this.selectedIndex.valueOf();
      }
    } else {
      // this.eventEmitterService.pushSnackBarMessage(['ID not found in Tablist'], SnackbarType.ERROR)
    }
    this.saveTabs();
  }


  getThemeString() {
    return this.mainInformationService.getThemeString();
  }

  showSnackBarMessage(snackbarData: SnackbarData) {

    let theme: string;
    if (snackbarData.barType == SnackbarType.ERROR) {
      theme = 'mat-warn';
    } else if (snackbarData.barType == SnackbarType.WARNING) {
      theme = 'mat-accent';
    } else {
      theme = 'mat-primary';
    }
    // this.zone.run(() => {
    this.snackbar.openFromComponent(SnackbarComponent, {
      data: snackbarData,
      panelClass: ['mat-toolbar', theme, 'default-snackbar', this.getThemeString()],
      duration: 5000
    });
    // });
    // this.changeDetectorRef.detectChanges();
  }
}
