import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NavigationListComponent } from './components/list/navigation-list.component';
import { TreeListComponent } from './components/running-master-jobs/job-list/tree-list.component';
import { NavigationTreeComponent } from './components/treeComponents/navigationTree/navigationTree.component';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [
      {
        path: 'BatchesAndJobs', component: NavigationTreeComponent, data: { command: "LIST FOLDER SYSTEM", TYPE: 'FOLDER', pinnable: true, shortType: 'folder', ITEMTYPES: ["FOLDER", "BATCH", "JOB"], SUBMITONLY: false, selectedBookmark: 'DEFAULT', bookmarkType: 'FOLDER' }
      },
      {
        path: 'SubmitBatchesAndJobs', component: NavigationTreeComponent, data: { command: "LIST FOLDER SYSTEM", TYPE: 'FOLDER', pinnable: false, shortType: 'folder', ITEMTYPES: ["FOLDER", "BATCH", "JOB"], SUBMITONLY: true, selectedBookmark: 'DEFAULT', bookmarkType: 'SUBMIT' }
      },




      // state definitions
      {
        path: 'ExitStateDefinitions', component: NavigationListComponent, data: { command: "LIST EXIT STATE DEFINITION", TYPE: 'EXIT STATE DEFINITION', shortType: 'esd' },
      },
      {
        path: 'ExitStateMappings', component: NavigationListComponent, data: { command: "LIST EXIT STATE MAPPING", TYPE: 'EXIT STATE MAPPING', shortType: 'esm' },
      },
      {
        path: 'ExitStateTranslations', component: NavigationListComponent, data: { command: "LIST EXIT STATE TRANSLATION", TYPE: 'EXIT STATE TRANSLATION', shortType: 'est' },
      },
      {
        path: 'ExitStateProfiles', component: NavigationListComponent, data: { command: "LIST EXIT STATE PROFILE", TYPE: 'EXIT STATE PROFILE', shortType: 'esp' },
      },
      {
        path: 'ResourceStateDefinitions', component: NavigationListComponent, data: { command: "LIST RESOURCE STATE DEFINITION", TYPE: 'RESOURCE STATE DEFINITION', shortType: 'rsd' }
      },
      {
        path: 'ResourceStateMappings', component: NavigationListComponent, data: { command: "LIST RESOURCE STATE MAPPING", TYPE: 'RESOURCE STATE MAPPING', shortType: 'rsm' }
      },
      {
        path: 'ResourceStateProfiles', component: NavigationListComponent, data: { command: "LIST RESOURCE STATE PROFILE", TYPE: 'RESOURCE STATE PROFILE', shortType: 'rsp' }
      },
      //Resources
      {
        path: 'NamedResources', component: NavigationTreeComponent, data: { command: "LIST NAMED RESOURCE", TYPE: 'NAMED RESOURCE', pinnable: false, shortType: 'nr', ITEMTYPES: ["CATEGORY", "POOL", "STATIC", "SYSTEM", "SYNCHRONIZING"], selectedBookmark: 'DEFAULT', bookmarkType: 'FOLDER' }
      },
      {
        path: 'JobserverAndResources', component: NavigationTreeComponent, data: { command: "LIST SCOPE GLOBAL", TYPE: 'SCOPE', pinnable: false, shortType: 'jsar', ITEMTYPES: ["SCOPE", "SERVER"], selectedBookmark: 'DEFAULT', bookmarkType: 'FOLDER' }
      },
      {
        path: 'Environments', component: NavigationListComponent, data: { command: "LIST ENVIRONMENTS", TYPE: 'ENVIRONMENT', shortType: 'env' }
      },
      {
        path: 'Footprints', component: NavigationListComponent, data: { command: "LIST FOOTPRINTS", TYPE: 'FOOTPRINT', shortType: 'footprint' }
      },
      {
        path: 'NiceProfiles', component: NavigationListComponent, data: { command: "LIST NICE PROFILES", TYPE: 'NICE PROFILE', shortType: 'niceprofile', filter: "niceProfileFilter" }
      },

      // object monitoring
      {
        path: 'ObjectMonitoring', component: NavigationListComponent, data: { command: "LIST OBJECT MONITOR", TYPE: 'OBJECT MONITOR', shortType: 'objectmonitoring' }
      },
      {
        path: 'WatchTypes', component: NavigationListComponent, data: { command: "LIST WATCH TYPES", TYPE: 'WATCH TYPE', shortType: 'watchtype' }
      },

      //time scheduling
      {
        path: 'Intervals', component: NavigationListComponent, data: { command: "LIST INTERVALS", TYPE: 'INTERVAL', shortType: 'interval', filter: "intervalFilter" }
      },

      //System
      {
        path: 'User', component: NavigationListComponent, data: { command: "LIST USER", TYPE: 'USER', shortType: 'serveruser' }
      },
      {
        path: 'Groups', component: NavigationListComponent, data: { command: "LIST GROUP", TYPE: 'GROUP', shortType: 'group' }
      }
    ]
    // {
    //   path: 'RunningMasterJobs', data: { TYPE: 'NO LIST' }
    // },


  }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
