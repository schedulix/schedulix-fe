import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, CanDeactivate, CanLoad, Route, Router, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { LoginComponent } from '../modules/login/login.component';


@Injectable({
  providedIn: 'root'
})
export class NegateAuthGuard implements CanActivate,  CanDeactivate<unknown> {

      constructor(private authService: AuthService, private router: Router)
      {

      }

      canActivate(next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree | any {
            return true;
      }

      // = Negation of canActivate. Can be used to check whether a module is NOT allowed to enter.
      canDeactivate(
        component: LoginComponent,
        currentRoute: ActivatedRouteSnapshot,
        currentState: RouterStateSnapshot,
        nextState?: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
          // console.log("can deactivate");
        return false;
      }

}
