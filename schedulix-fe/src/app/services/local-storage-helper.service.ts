import { Injectable } from '@angular/core';
import { Credentials } from '../data-structures/connection';
import { SnackbarType } from '../modules/global-shared/components/snackbar/snackbar-data';
import { AuthService } from './auth.service';
import { EventEmitterService } from './event-emitter.service';

interface StorageObject {
  [key: string]: any
}

@Injectable({
  providedIn: 'root'
})

// Central service to manage a session.
export class LocalStorageHelper {

  constructor(private eventEmitterService: EventEmitterService) { }

  currentConnection = 'default';
  storageObject: StorageObject = {
  }


  // init the currentConnection might be better in other service TODO/talk with bumi
  initConnection(credentials: Credentials) {
    this.currentConnection =   credentials.connectionName + '.' + credentials.username + '.' + credentials.host + '' + credentials.port + '' + credentials.serverName;
    this.storageObject = this.getConnectionStorage();
  }

  setConnectionValue(key: string, value: any) {
    // console.log(this.storageObject)
    this.storageObject[key] = value
    localStorage.setItem(this.currentConnection, JSON.stringify(this.storageObject));
  }

  // can be undefined!
  getConnectionValue(key: string): any {
    let item = localStorage.getItem(this.currentConnection)

    if (item === null) {
      // console.log('localStorage value ' + key + ' is null')
      return '';
    } else {
       return JSON.parse(item)[key]
    }
  }
  // can be undefined!
  getConnectionStorage(): any{
    let item = localStorage.getItem(this.currentConnection)

    if (item === null) {
      return {}
    } else {
       return JSON.parse(item)
    }
  }

  getValue(key: string): string {
    // console.log(this.getConnectionValue(key));
    let value = localStorage.getItem(key)
    if (value === null) {
      // this.eventEmitterService.pushSnackBarMessage(['localStorage value ' + key + ' is null'], SnackbarType.ERROR)
      // console.log('localStorage value ' + key + ' is null')
      return ''
    } else {
      return value
    }
    // return localStorage.getItem(key);
  }

  setItem(key: string, value: string) {
    // this.setConnectionValue(key, value)
    localStorage.setItem(key, value);
  }

  setJSON(key: string, value: any) {
    this.setItem(key, JSON.stringify(value));
  }

  getJSONValue(key: string) {
    let value = localStorage.getItem(key)
    if (value === null) {
      // console.log('localStorage value ' + key + ' is null')
      return ''
    } else {
      return JSON.parse(value)
    }
  }

  clearSession() {
    localStorage.clear();
  }

  clearProperty(key: string) {
    localStorage.removeItem(key);
  }
}
