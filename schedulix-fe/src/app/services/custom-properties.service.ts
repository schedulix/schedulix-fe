import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class CustomPropertiesService {

  constructor(private http: HttpClient, private router: Router) { }


  // add more custom variables
  webServerUrl: string = ''
  private customObject: any = {};
  private customObjectTemplate: any = {};

  /**
   * Loads the configuration data for the application.
   * If the application is hosted on a zope server, it fetches the custom zope config data.
   * If not hosted on a zope server, it fetches the local config file.
   * 
   * @returns A promise that resolves with the loaded configuration data.
   */
  loadConfigData(): Promise<any> {
    var ts : String = Date.now().valueOf().toString();
    // Check if the application is hosted on a zope server
    let fullUrl = location.href;
    if (fullUrl.includes('/schedulix-fe')) {
      console.log("Hosted on a zope server. Loading custom zope config data ...");
      let url = fullUrl.split('/schedulix-fe')[0];
      let configUrl = url + "/custom/config.json?ts=" + ts;
      console.log("Loading custom zope config data from: ", configUrl);
      // Fetch the custom zope config data
      return this.http.get(configUrl).toPromise().then((data: any) => {
        console.log("Custom config data loaded: ", data);
        this.customObject = data;
        if (data.hasOwnProperty('webserver_url')) {
          this.webServerUrl = data.webserver_url + "/SDMS/webservice";
          return data;
        }
        return data;
      });
    } else {
      console.log("Not hosted on a zope server. Load local config file.");
      // Fetch the local config file
      return this.http.get("custom/config.json?ts=" + ts).toPromise().then((data: any) => {
        console.log("Custom config data loaded: ", data);
        this.customObject = data;
        if (data.hasOwnProperty('webserver_url') && this.webServerUrl == '') {
          this.webServerUrl = data.webserver_url + "/SDMS/webservice";
          return data;
        }
        return data;
      });
    }
  }

  loadConfigTemplateData(): Promise<any> {
    var ts : String = Date.now().valueOf().toString();
    return this.http.get("custom/config.template.json?ts=" + ts).toPromise().then((data: any) => {
      console.log("Custom config template loaded: ", data);
      this.customObjectTemplate = data;
      if (data.hasOwnProperty('webserver_url') && this.webServerUrl == '') {
        this.webServerUrl = data.webserver_url + "/SDMS/webservice";
        // console.log(this.webServerUrl);
        return data;
      }
      return data
    })
  }


  loadCustomJsonObjects() {
    this.loadCustomJs();
    // console.log("load config data ...");


    return Promise.all([
      this.loadConfigData(),
      this.loadConfigTemplateData()
    ]).then((data: any) => {
      console.log("Custom config datas finished", data);
    }, (error) => {
      console.log("Error loading custom config data: ", error);
      return {}
    });
  }

  // getCustomObject(): any {
  //   return this.customObject;
  // }

  // getCustomObjectTemplate(): any {
  //   return this.customObjectTemplate;
  // }

  getCustomObjectProperty(property: string) {
    if (this.customObject.hasOwnProperty(property)) {
      return this.customObject[property];
    }
    // fallback to config value of property in config.template.json
    return this.customObjectTemplate[property];
  }

  loadCustomJs() {
    // console.log("load custom javascript ...");
    var ts : String = Date.now().valueOf().toString();
    try {
      // This array contains all the files/CDNs
      const dynamicScripts = [
        'custom/hooks.js'
      ];

      for (let i = 0; i < dynamicScripts.length; i++) {
        const node = document.createElement('script');
        node.src = dynamicScripts[i] +"?ts=" + ts;
        node.id = 'hooks';
        node.type = 'text/javascript';
        node.async = false;
        document.getElementsByTagName('head')[0].appendChild(node);
      }
    } catch (e) {
      console.warn(e);
    }

  }
}
