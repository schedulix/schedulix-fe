import { Injectable } from '@angular/core';
import { AbstractControl, FormGroup } from '@angular/forms';
import { BehaviorSubject, Subject } from 'rxjs';
import { Tab } from '../data-structures/tab';
import { OutputType, SnackbarData, SnackbarType } from '../modules/global-shared/components/snackbar/snackbar-data';

@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {

  constructor() { }

  snackBarMessageEmittor: Subject<SnackbarData> = new Subject();

  // is Emitted when the List has to be refreshed in the list component or tree in the tree component
  refreshNavigationEmittor: Subject<any> = new Subject();

  // is Emitted by the ID of the selected Tab in the main component
  selectedTabEmitter: BehaviorSubject<string> = new BehaviorSubject('');

  // is Emitted when a Tab has to be created in the main component
  createTabEmitter: Subject<{ tab: Tab, target: boolean }> = new Subject();

  // is Emitted when a Tab has to be updated in the main component
  updateTabEmitter: Subject<Tab> = new Subject();

  // is Emitted when a Tab is going to be deleted in the main component
  removeTabEmitter: Subject<Tab> = new Subject();

  // is Emitted when all Tabs are goint te be saved in browserstorage
  saveTabEmitter: Subject<Tab> = new Subject();

  // is Emitted when a tab rechieves new data from the server in the tab component.
  bicsuiteRefreshEmitter: Subject<Tab> = new Subject();

  // is Emitted when a field asks to update the conditions of the whole form
  updateEditorformOptionsEmitter: Subject<{ tab: Tab, rootForm?: AbstractControl }> = new Subject();

  // is Emitted when a Error occured
  errorEmitter: Subject<Tab> = new Subject();

  //is emitted in crud methods to manually rerender the tab
  reRenderTabEmitter: Subject<Tab> = new Subject();

  //is emitted when a tab has to be locked, and unlocked
  setTabLockEmitter: Subject<{Tab: Tab, lock: boolean}> = new Subject();

  //is emitted in in left navigation to trigger outside repositioning of a graph
  rePosNoFormEditors: Subject<string> = new Subject();

  //is emitted in clipboardservice when new item was added to the clipboard
  clipboardEmitter: Subject<Tab> = new Subject();

  // is emitted froom crud (update, create, clone, drop methods)when objects bare renamed, created any maybe dropped
  notifyDataChangeEmitter: Subject<any> = new Subject();

  // //is emitted when validators have to be
  // reSetValidatorByName: Subject<string> = new Subject();



  // message[] has to be a key value in the localization.json if SnackbarType is not SnackbarType.ERROR
  // Keys in message[] are only separated by a space when the outputType is set to SPACE_SEPARATED.
  pushSnackBarMessage(message: string[], snackBarType: SnackbarType,
    outputType: OutputType = OutputType.NONE_SEPARATED): void {

    let snackbarData = new SnackbarData();
    snackbarData.message = message;
    snackbarData.barType = snackBarType;
    snackbarData.outputType = outputType;
    this.snackBarMessageEmittor.next(snackbarData);
  }

  // target if the tabs should be selected or not
  createTab(tab: Tab, target: boolean) {
    this.createTabEmitter.next({ tab: tab, target: target });
  }
}
