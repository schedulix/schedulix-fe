import { Injectable } from '@angular/core';
import { Console } from 'console';
import { Observable, Subject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkerService {

  constructor() { }

  workers: Worker[] = [];

  killAllWorkers(){
    for(let worker of this.workers){
      try{
        worker.terminate();
      } catch(e){
        console.warn(e)
      }
    }
    // console.log('worker destroyed')
  }

  createTopologicWorker(methodName: string, parameters: any[], callback: Subject<any>): Worker | undefined{

    // const worker = new Worker(, import.meta.url));) {
    if (typeof Worker !== 'undefined') {
      // Create a new
      const worker = new Worker(new URL('./topologicSort.worker', import.meta.url));
      this.workers.push(worker);
      worker.onmessage = ({ data }) => {
        // let res = workerFunction(parameters)
        // console.log(`page got message: ${data}`);
        callback.next(data)
        worker.terminate();
      };
      worker.postMessage({
        methodName: methodName,
        parameters: parameters
      });
      return worker;
    } else {
      // Web Workers are not supported in this environment.
      // fallback so that your program still executes correctly.
      return undefined;
    }
  }
}
