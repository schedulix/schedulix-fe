import { HttpClient, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { LocalStorageHelper } from './local-storage-helper.service';
import { EventEmitterService } from './event-emitter.service';
import { SnackbarType } from '../modules/global-shared/components/snackbar/snackbar-data';
import { Credentials, SdmsServer } from '../data-structures/connection';
import { AuthResponse } from '../data-structures/auth-response';
import { CommandApiService } from './command-api.service';
import { CustomPropertiesService } from './custom-properties.service';
import { BicsuiteHttpException, ErrorHandlerService, sdmsException } from './error-handler.service';
import { catchError, retry } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  credentials: Credentials = new Credentials();
  serverList: SdmsServer[] = [];
  connectedServer: SdmsServer | undefined;

  constructor(private http: HttpClient, private router: Router, private localStorageHelper: LocalStorageHelper,
    private eventEmitterService: EventEmitterService,
    private customPropertiesService: CustomPropertiesService, private errorHandlerService: ErrorHandlerService) {
  }

  url: string = this.customPropertiesService.webServerUrl;

  // Credentials should be set here. If in future the credentials are changed
  // (encrpted, hashed, cookies), this is the spot to do so.
  setCredentials(credentials: Credentials) {
    this.credentials = credentials;
    this.localStorageHelper.initConnection(credentials);
    // For debugging
    this.localStorageHelper.setConnectionValue("username", this.credentials.username);
    this.localStorageHelper.setConnectionValue("password", this.credentials.password);
    this.localStorageHelper.setConnectionValue("host", this.credentials.host);
    this.localStorageHelper.setConnectionValue("port", this.credentials.port);
    this.localStorageHelper.setConnectionValue("serverName", this.credentials.serverName);
    this.localStorageHelper.setConnectionValue("connectionName", this.credentials.connectionName);
  }

  checkIfUrlIsEmpty() {
    return this.customPropertiesService.webServerUrl == ''
  }

  setSelectedServer() {
    this.connectedServer = this.serverList.find((server: SdmsServer) => {
      if (server.NAME == this.credentials.serverName) {
        return true;
      } else {
        return false;
      }
    })
  }

  getSelectedServer(): SdmsServer | undefined {
    return this.connectedServer;
  }

  getCredentials() {
    // For debugging
    // this.credentials.username = this.localStorageHelper.getValue("username");
    // this.credentials.password = this.localStorageHelper.getValue("password");
    // this.credentials.host = this.localStorageHelper.getValue("host");
    // this.credentials.port = this.localStorageHelper.getValue("port");
    // this.credentials.serverName = this.localStorageHelper.getValue("serverName");
    // this.credentials.connectionName = this.localStorageHelper.getValue("connectionName");


    this.localStorageHelper.initConnection(this.credentials);

    this.credentials.username = this.localStorageHelper.getConnectionValue("username");
    this.credentials.password = this.localStorageHelper.getConnectionValue("password");
    this.credentials.host = this.localStorageHelper.getConnectionValue("host");
    this.credentials.port = this.localStorageHelper.getConnectionValue("port");
    this.credentials.serverName = this.localStorageHelper.getConnectionValue("serverName");
    this.credentials.connectionName = this.localStorageHelper.getConnectionValue("connectionName");

    return this.credentials;
  }

  loadServerList(): Promise<any> {
    console.log('load serverlist')
    if(this.checkIfUrlIsEmpty()) {
      let ex = new BicsuiteHttpException('webServerUrl is empty in custom/config.json', 'loadServerList', 'webServerUrl is empty in custom/config.json', 'custom/config.json')
      this.errorHandlerService.createError(ex)
      return Promise.reject('webServerUrl is empty in custom/config.json')
    }
    
    let params: HttpParams = new HttpParams().set("OPERATION", "GET_SERVERS");
    return this.http.post(this.url, params).toPromise().then((response: any) => {
      if (response) {
        this.serverList = [];
        for (let key of Object.keys(response)) {
          let o: SdmsServer = new SdmsServer();
          o = response[key];
          o.NAME = key;
          this.serverList.push(o);
        }
        console.log(this.serverList)
      }
    });
  }

  getServerList(): SdmsServer[] {
    return this.serverList;
  }

  logout() {
    this.credentials = new Credentials();
    this.router.navigate(['login']);
  }

  // Used to check whether the given credentials are allowed to communicate with the backend.
  // A command is not needed to check authentification
  async authenticate(command: string = ''): Promise<AuthResponse> {
    this.credentials = this.getCredentials();
    let authResponse: AuthResponse = new AuthResponse();

    let params: HttpParams = new HttpParams()
      .set("COMMAND", command)
      // .set("HOST", this.credentials.host)
      // .set("PORT", this.credentials.port)
      .set("USER", this.credentials.username)
      .set("PASSWORD", this.credentials.password)
      .set("SERVER", this.credentials.serverName);

    // Connection Block
    try {
      const response = await this.http.post(this.url, params).toPromise();
      authResponse.response = response;

      // Response has an error-attribute => Login was invalid.
      if (response.hasOwnProperty('ERROR')) {
        authResponse.succeed = false;
      }
      else {
        authResponse.succeed = true;
      }
    }
    catch (err) {
      // Connection errors are handled here (HttpErrorResponse).
      authResponse.succeed = false;
      authResponse.response = err;
      // throw(err)
    }
    return authResponse;
  }
}
