/// <reference lib="webworker" />


addEventListener('message', ({ data }) => {
  const response = `worker response to ${data.methodName}`;
  const topo = performTopoLogicSort(data.parameters[0], data.parameters[1], data.parameters[2], data.parameters[3])
  postMessage(topo);
});

function performTopoLogicSort(edgeList: any[], nodeList: any[], tree: any[], mode: string): any {
  if (mode == 'topDown') {
    // console.log(new Date())
    // console.log('start top down topologic sort')
    let allTargets: any = {};
    // graph #1  created parent child edges from child -> parent
    let graph: SmdsGraph = new SmdsGraph();
    graph.addNodes(nodeList, tree)
    let hierachyEdges: any[] = [];
    for (let node of tree) {
      createParentChildEdges(node, hierachyEdges, graph)
    }
    for (let edge of edgeList) {
      allTargets[edge.SE_DEPENDENT_PATH];
      // graph.addEdge(edge.SE_REQUIRED_PATH, edge.SE_DEPENDENT_PATH);
      //
      graph.addDependencyEdge(edge, nodeList, mode);
      // addVirtualEdges(edge, nodeList);
    }

    console.log(graph);
    // for (let hedge of hierachyEdges) {
    //   graph.addEdge(hedge.SE_REQUIRED_PATH, hedge.SE_DEPENDENT_PATH)
    // }
    // console.log(graph.numberOfEdges)
    // console.log(graph.numberOfTrys)

    let topo1 = doTopologicSort(graph, allTargets);
    // console.log(new Date())
    // console.log('end top down topologic sort')
    return topo1;
  } else if (mode == 'bottomUp') {
    // console.log(new Date())
    // console.log('start bottomUp topologic sort')
    let allTargets: any = {};
    let graph2: SmdsGraph = new SmdsGraph();


    graph2.addNodes(nodeList, tree)
    let hierachy2Edges: any[] = [];
    for (let node of tree) {
      createParentChildEdgesInverse(node, hierachy2Edges, graph2)
    }
    // console.log('add Dependency edge 2')
    for (let edge2 of edgeList) {
      allTargets[edge2.SE_DEPENDENT_PATH];
      graph2.addDependencyEdge(edge2, nodeList, mode);
    }

    // console.log(graph2);
    // for (let hedge2 of hierachy2Edges) {
    //   graph2.addEdge(hedge2.SE_REQUIRED_PATH, hedge2.SE_DEPENDENT_PATH)
    // }
    let topo2 = doTopologicSort(graph2, allTargets);
    // console.log(new Date())
    // console.log('end bottom up topologic sort')
    return topo2
  }
}


function createParentChildEdges(root: any, edges: any[], graph: SmdsGraph): any[] {
  // console.log(root)

  // for (let node of root) {
  // console.log(root.data);
  if (root.children) {

    for (let children of root.children) {
      let parentPath = root.data.HIERARCHY_PATH;
      let childPath = children.data.HIERARCHY_PATH;
      let childNode = graph.getNode(childPath);
      // if children is a job with detached node the parent child dependency is to be created at both nodes
      if(childNode) {
        if(childNode?.detachedJobNode != '') {
          // console.log('child is a detached job');
          // create detached parent child edge
          graph.addEdge(childNode.detachedJobNode, root.data.HIERARCHY_PATH);
        }
      }
      graph.addEdge(childPath, root.data.HIERARCHY_PATH);
      if(childNode){
        childNode!.children[parentPath] = parentPath;
      }
      // console.log("edge pushed")
      if (children.children?.length > 0) {
        // console.log("new instance")
        createParentChildEdges(children, edges, graph)
      }
    }
  }
  // }
  return edges;
}
function createParentChildEdgesInverse(root: any, edges: any[], graph: SmdsGraph): any[] {
  // console.log(root)

  // for (let node of root) {
  if (root.children) {

    let rootNode = graph.getNode(root.data.HIERARCHY_PATH);
    for (let children of root.children) {
      let childPath = children.data.HIERARCHY_PATH;
      let parentPath = root.data.HIERARCHY_PATH;
      let childNode = graph.getNode(childPath);

      if(childNode) {
        if(childNode?.detachedJobNode != '') {
          // console.log('child is a detached job');
          // create detached parent child edge
          graph.addEdge(root.data.HIERARCHY_PATH, childNode.detachedJobNode, );
        }
      }
      // create Edge
      // check if there is a edge with same dependent path
      // edges.push({
      //   SE_REQUIRED_PATH: root.data.HIERARCHY_PATH,
      //   SE_DEPENDENT_PATH: children.data.HIERARCHY_PATH,
      // })

      graph.addEdge(parentPath, childPath);
      if(rootNode){
        rootNode!.children[childPath] = childPath;
      }

      // edges.push({
      //   SE_REQUIRED_PATH: children.data.HIERARCHY_PATH,
      //   SE_DEPENDENT_PATH: root.data.HIERARCHY_PATH,
      // })
      // console.log("edge pushed")
      if (children.children?.length > 0) {
        // console.log("new instance")
        createParentChildEdgesInverse(children, edges, graph)
      }
    }
    // console.log(rootNode);
  }
  // }
  return edges;
}

function doTopologicSort(graph: SmdsGraph, allTargetsHashList: any): any {
  function chooseNodesWithNoIncomingEdge(graph: SmdsGraph, allTargetsHashList: any): {} | undefined {
    let nodes: any = {};
    for (let node of Object.keys(graph.adjListObject)) {
      // console.log(node)
      // let found: boolean = false;
      if (!allTargetsHashList.hasOwnProperty(node)) {
        nodes[node] = graph.adjListObject[node]
      }
      // if (!found) {
      //   nodes[node] = graph.adjListObject[node]
      // }
    }
    return nodes;
  }
  function chooseNodeWithNoIncomingEdge(temp: any, graph: SmdsGraph): any | undefined {
    for (let node of Object.keys(temp)) {
      let found: boolean = false;
      inner: for (let graphNode of Object.keys(graph.adjListObject)) {
        if (graph.adjListObject[graphNode].targets[node]) {
          found = true;
          break inner;
        };
      }
      if (!found) {
        return temp[node]
      }
    }
    return
  }

  function getNodesWithNoIncomingEdge(temp: any, graph: SmdsGraph): any | undefined {
    let nodes: any[] = [];
    for (let node of Object.keys(temp)) {
      if (temp[node].incomingEdgesSum == 0) {
        nodes.push(temp[node]);
      }
    }
    return nodes
    // for (let node of Object.keys(temp)) {
    //   let found: boolean = false;
    //   inner: for (let graphNode of Object.keys(graph.adjListObject)) {
    //     if (graph.adjListObject[graphNode].targets[node]) {
    //       found = true;
    //       break inner;
    //     };
    //   }
    //   if (!found) {
    //     nodes.push(temp[node]);
    //   }
    // }
    // return nodes;
  }


  let sortedGraph: topoNode[] = [];
  // console.log('start calculating incoming nodes')
  let tempNodes: any = chooseNodesWithNoIncomingEdge(graph, allTargetsHashList);
  // console.log('incoming nodes calculated')
  // console.log(tempNodes)
  let hasCycle: boolean = false;

  if (tempNodes == undefined || Object.keys(tempNodes).length < 1) {
    tempNodes = {};
    // if no node has no incoming edge we always have a cycle and all nodes can be involved
    tempNodes = graph.adjListObject;
    console.warn(tempNodes)
    hasCycle = true;
  } else {
    while (Object.keys(tempNodes).length > 0) {
      // console.log('tempnodes:' + Object.keys(tempNodes).length)
      //     remove node from tempNodes
      // shift always work since lenght always greater 0
      // let node = chooseNodeWithNoIncomingEdge(tempNodes, graph)
      let nodeList = getNodesWithNoIncomingEdge(tempNodes, graph);
      // console.log('nodeList length with no edges:' + nodeList.length)
      if (nodeList.length === 0) {
        console.warn(tempNodes)
        hasCycle = true;
        break;
      }
      for (let node of nodeList) {
        delete tempNodes[node.name];
        // new Worker
        // return
        sortedGraph.push(node);
        // for each node m with an edge e from node to m do
        // console.log('targets:' + Object.keys(node.targets).length)
        for (let nodetarget of Object.keys(node.targets)) {

          let m = graph.adjListObject[nodetarget]
          if (m) {
            // console.log()
            // node.targets.splice(node.targets.indexOf(nodetarget), 1);
            delete node.targets[nodetarget]
            graph.adjListObject[m.name].incomingEdgesSum--;

            graph.adjListObject[m.name].inDegres++
            // if m has no other incoming edges then x
            if (graph.adjListObject[m.name].inDegres == 1) {
              // insert m into tempNodes x
              // tempNodes.push(m);
              tempNodes[m.name] = graph.adjListObject[m.name]
            }
          }
        }
      }


      // if (node) {
      //   delete tempNodes[node.name];
      //   // new Worker
      //   // return
      //   sortedGraph.push(node);
      //   // for each node m with an edge e from node to m do
      //   console.log('targets:' + Object.keys(node.targets).length)
      //   for (let nodetarget of Object.keys(node.targets)) {

      //     let m = graph.adjListObject[nodetarget]
      //     if (m) {
      //       // console.log()
      //       // node.targets.splice(node.targets.indexOf(nodetarget), 1);
      //       delete node.targets[nodetarget]
      //       // console.log("FOUND")
      //       graph.adjListObject[m.name].inDegres++
      //       // if m has no other incoming edges then x
      //       if (graph.adjListObject[m.name].inDegres == 1) {
      //         // insert m into tempNodes x
      //         // tempNodes.push(m);
      //         tempNodes[m.name] = graph.adjListObject[m.name]
      //       }
      //     }
      //   }
      // } else {
      //   // console.log(node)
      //   console.warn(tempNodes)
      //   hasCycle = true;
      //   break;
      // }
      //  return
    }
  }
  return { sortedGraph: sortedGraph, hasCycle: hasCycle, tempNodes: tempNodes };
}

function markAllNodesInCycle(cyclicNode: topoNode, observedNode: topoNode, observedNodes: topoNode[], cyclicGraph: SmdsGraph, cyclePathContainer: any[]) {
  //ist equal or starts with
  if (observedNode.name.startsWith(cyclicNode.name)) {


    let cyclePath = observedNodes.concat(observedNode);

    if (cyclePath.length < 3) {
      // console.log('cycle detected with lower length')
      // filter length < 3 because those cycles are parent child coherences
    } else {
      // cycle detected!
      // console.log('cycle detected')
      // console.log(observedNode.name)
      // let cyclePath = observedNodes.concat(observedNode);
      // console.log(cyclePath)
      cyclePathContainer.push(cyclePath)
    }
    return
    // return observedNodes;
  } else if (Object.keys(observedNode.targets).length > 0) {

    // add observed node to observedNodes since now all children will be checked
    // console.log('visit targets of ' + observedNode.name)
    for (let target of Object.keys(observedNode.targets)) {

      let newObservedNode = cyclicGraph.getNode(target)
      let newObservedNodes = observedNodes.concat([observedNode]);
      markAllNodesInCycle(cyclicNode, newObservedNode!, newObservedNodes, cyclicGraph, cyclePathContainer);
    }
  } else {

    return
  }

}



interface topoNode {
  name: string,
  targets: any,
  inDegres: number,
  inDegresParentSum: number,
  incomingEdgesSum: number,
  children: any,
  type: string,
  detachedJobNode: string
}


class SmdsGraph {

  // adjList: topoNode[] = [];
  adjListObject: any = {}

  numberOfEdges: number = 0;
  numberOfTrys: number = 0;

  constructor() {

  }

  getNode(name: string): topoNode | undefined {

    return this.adjListObject[name]
    // return found1 ? found1 : false

    // const found = this.adjList.find((node) => {
    //   if (node.name == name) {
    //     return true;
    //   }
    //   return false;
    // })
    // return found;
  }

  addNodes(nodeList: any[], tree: any[]) {
    for (let serverNode of nodeList) {

      // job with childs have a special behavior, because with 'job final' dependencies they can trigger own children which leads to false cycles.
      // to fix this they have to be splitted in a job and a 'batch', both hold the same initial dependencies, but the job is now able to trigger children of the batch when final.
      let isJobWithChilds: boolean = false;
      if(serverNode.TYPE == "JOB") {
        if(this.hasChildren(serverNode, nodeList)) {
          // console.log('JOB '+ serverNode.HIERARCHY_PATH +' HAS CHILDS');
          isJobWithChilds = true;
        }
      }
      if(isJobWithChilds) {
        // detached Job which will get the same initial dependencies as the original + he gets the job final dependencies to all of the original childs
        this.adjListObject[serverNode.HIERARCHY_PATH + 'detachedJobNode'] = {
          name: serverNode.HIERARCHY_PATH + 'detachedJobNode',
          targets: [],
          inDegres: 0,
          inDegresParentSum: 0,
          incomingEdgesSum: 0,
          children: {},
          type: serverNode.TYPE,
          detachedJobNode: ''
        }
        // initial object gets detached path for lookup later
        this.adjListObject[serverNode.HIERARCHY_PATH] = {
          name: serverNode.HIERARCHY_PATH,
          targets: [],
          inDegres: 0,
          inDegresParentSum: 0,
          incomingEdgesSum: 0,
          children: {},
          type: serverNode.TYPE,
          detachedJobNode: serverNode.HIERARCHY_PATH + 'detachedJobNode'
        }
      } else {
        // single node was created, job or batch
        this.adjListObject[serverNode.HIERARCHY_PATH] = {
          name: serverNode.HIERARCHY_PATH,
          targets: [],
          inDegres: 0,
          inDegresParentSum: 0,
          incomingEdgesSum: 0,
          children: {},
          type: serverNode.TYPE,
          detachedJobNode: ''
        }
      }
      // if node is job  and has Children

        // create dummy node

        // mark original node with has dummy to get dummynode out of the original node

        // job final dependencies are beeing created from that dummy node instead of original node
    }
  }

  hasChildren(node: any, nodeList: any[]): boolean {
    let hit = false;
    for( let serverNode of nodeList) {
      if (String(serverNode.HIERARCHY_PATH).startsWith(node.HIERARCHY_PATH + ':')) {
        hit = true;
        break;
      }
    }
    return hit;
  }

  // input type {name, target}
  addEdge(from: string, to: string) {
    // let node = this.adjList.find((node) => node.name == from);
    let node = this.getNode(from)
    if (node) {
      let found: boolean = false;
      this.numberOfTrys++;
      if (node.targets.hasOwnProperty(to)) {
        // edge already exists
      } else {
        node.targets[to] = to;
        this.numberOfEdges++;

        this.getNode(to)!.incomingEdgesSum++
      }
      // for (let tar of node.targets) {
      //   if (tar == to) {
      //     found = true;
      //     // console.log('edge from: ' + from + '\nto: ' + to + '\n already exists')
      //   }
      // }
      // found != true ? node.targets.push(to) : '';
    }
  }

  removeEdge(from: string, to: string) {
    // let node = this.adjList.find((node) => node.name == from);
    let node = this.getNode(from)
    if (node) {
      let found: boolean = false;
      if (node.targets.hasOwnProperty(to)) {
        delete node.targets[to]; // edge already exists
        this.getNode(to)!.incomingEdgesSum--;
        // console.log('removed: ' +  node.name + '"s target:' + to )
        found = true;
      }
    }
  }



  isChildOf(node1: topoNode, node2L: topoNode): boolean {

    return true
  }

  addDependencyEdge(edge: any, nodelist: any[], mode: string) {
    // add dependency

    // create virtual dependencies to all dependent children
    // if source or target is a job with children, the dependency will be created at the detached node with all nested depdencies
    let requiredNode = this.getNode(edge.SE_REQUIRED_PATH);
    let dependendNode = this.getNode(edge.SE_DEPENDENT_PATH);
    if( requiredNode && dependendNode) {
        // console.log(requiredNode);
        // console.log(dependendNode);

        if(requiredNode.detachedJobNode != '') {
          // node is job and has children
          // console.log('requiredNode is job with detached job');
          if(edge.MODE == 'JOB_FINAL') {
              // all outgoing 'job final' dependencies are going from the detached job to the target and its children
              let detachedEdge = {
                SE_REQUIRED_PATH: requiredNode.detachedJobNode,
                SE_DEPENDENT_PATH: edge.SE_DEPENDENT_PATH
              }
              this.addEdgeAndToItsChildren(detachedEdge, nodelist);

          } else {
              // all outgoing 'all final' dependencies are remain at the node
              this.addEdgeAndToItsChildren(edge, nodelist);
          }
        } else {
          // not detached
          this.addEdgeAndToItsChildren(edge, nodelist);
        }

        if(dependendNode.detachedJobNode != '') {
          // node is job and has children
          // console.log('dependendNode is job with detached job');

          // all ingoing dependencies are craeted at the node and dublicated to the detached job
          this.addEdgeAndToItsChildren(edge, nodelist);

          // add the dependency to the detached edge, the 'children' of its original job already got all the dependencies
          let detachedEdge = {
            SE_REQUIRED_PATH: edge.SE_REQUIRED_PATH,
            SE_DEPENDENT_PATH: dependendNode.detachedJobNode
          }
          this.addEdge(detachedEdge.SE_REQUIRED_PATH, detachedEdge.SE_DEPENDENT_PATH);
        }
    }


    // if(edge.MODE == 'JOB_FINAL') {
    //   let requiredNode = this.getNode(edge.SE_REQUIRED_PATH);
    //   let dependendNode = this.getNode(edge.SE_DEPENDENT_PATH);
    //   // console.log(requiredNode);
    //   // console.log(dependendNode);
    //   // if(mode == 'bottomUp') {

    //     if( requiredNode && dependendNode) {

    //       console.log(requiredNode);
    //       console.log(dependendNode);




        //   // if dependent is child of required
        //   if(requiredNode?.children[dependendNode.name]) {
        //     if(requiredNode.type == 'JOB'){
        //       // remove parent child edge
        //       console.log('source is job ' + mode);
        //       // this.removeEdge(requiredNode.name, dependendNode.name);
        //     }
        //   }
        //   // if required is child of dependent
        //   if(dependendNode?.children[requiredNode.name]) {
        //     if(dependendNode.type == 'JOB'){
        //       // remove parent child edge
        //       console.log('target is job ' + mode);
        //       // this.removeEdge(dependendNode.name,requiredNode.name);
        //     }
        //   }
    //     }
    // }


    // this.addEdgeAndToItsChildren(edge, nodelist);

  }
  addEdgeAndToItsChildren(edge: any, nodelist: any[]){
    this.addEdge(edge.SE_REQUIRED_PATH, edge.SE_DEPENDENT_PATH)

    // create a dependency edge from the start to ALL childs of the target
    let startPath = edge.SE_DEPENDENT_PATH + ':'
    // from top to down check if node can be a child node.level >
    let hit: boolean = false;
    for (let node of nodelist) {

      // improve startswith
      if (String(node.HIERARCHY_PATH).startsWith(startPath)) {
        this.addEdge(edge.SE_REQUIRED_PATH, node.HIERARCHY_PATH)
        hit = true;
        // check if this exact edge is already there
      } else {
        // if hit is true, there cant be more children in the pre sorted table
        if (hit == true) {
          break;
        }
      }
    }
  }
  addDependencyEdgeNoExtendetDepdendency(edge: any, nodelist: any[]) {
    // add dependency
    this.addEdge(edge.SE_REQUIRED_PATH, edge.SE_DEPENDENT_PATH)
    // create virtual dependencies to all dependent children
    // for (let node of nodelist) {
    //   if (String(node.HIERARCHY_PATH).startsWith(edge.SE_DEPENDENT_PATH + ':')) {
    //     this.addEdge(edge.SE_REQUIRED_PATH, node.HIERARCHY_PATH)

    //     // check if this exact edge is already there

    //   }
    // }
  }
}
