import { Injectable, Type } from '@angular/core';
import { ExitStateDefinitionCrud } from '../classes/exit-state-definition-crud';
import { ExitStateMappingCrud } from '../classes/exit-state-mapping-crud';
import { ExitStateProfileCrud } from '../classes/exit-state-profile-crud';
import { ExitStateTranslationCrud } from '../classes/exit-state-translation-crud';
import { JobCrud } from '../classes/job-crud';
import { BookmarkingCrud } from '../classes/bookmarking-crud';
import { ResourceStateDefinitionCrud } from '../classes/resource-state-definition-crud';
import { ResourceStateMappingCrud } from '../classes/resource-state-mapping-crud';
import { ResourceStateProfileCrud } from '../classes/resource-state-profile';
import { BicsuiteResponse } from '../data-structures/bicsuite-response';
import { Tab } from '../data-structures/tab';
import { Crud } from '../interfaces/crud';
import { CommandApiService } from './command-api.service';
import { BookmarkService } from '../modules/main/services/bookmark.service';
import { Router } from '@angular/router';
import { ToolboxService } from './toolbox.service';
import { CommentCrud } from '../classes/comment-crud';
import { EventEmitterService } from './event-emitter.service';
import { TriggerCrud } from '../classes/trigger-crud';
import { UserCrud } from '../classes/user-crud';
import { PrivilegesService } from '../modules/main/services/privileges.service';
import { GroupCrud } from '../classes/group-crud';
import { SysinfoCrud } from '../classes/sysinfo-crud';
import { MatLegacyDialog as MatDialog } from '@angular/material/legacy-dialog';
import { SubmittedEntityCrud } from '../classes/submitted-entity-crud';
import { WatchTypeCrud } from '../classes/watch-type-crud';
import { CategoryCrud } from '../classes/category-crud';
import { ScopeCrud } from '../classes/scope-crud';
import { NamedResourceCrud } from '../classes/named-resource-crud';
import { EditorformOptions } from '../data-structures/editorform-options';
import { FolderCrud } from '../classes/folder-crud';
import { TreeFunctionsService } from '../modules/main/services/tree-functions.service';
import { CustomPropertiesService } from './custom-properties.service';
import { EnvironmentCrud } from '../classes/environment-crud';
import { FootprintCrud } from '../classes/footprint-crud';
import { IntervalCrud } from '../classes/interval-crud';
import { MonitorMasterCrud } from '../classes/monitor-master-crud';
import { MonitorJobsCrud } from '../classes/monitor-jobs-crud';
import { SubmitEntityService } from '../modules/main/services/submit-entity.service';
import { MainInformationService } from '../modules/main/services/main-information.service';
import { SchedulesCrud } from '../classes/schedules-crud';
import { ResourceCrud } from '../classes/resource-crud';
import { GrantCrud } from '../classes/grant-crud';
import { NiceProfileCrud } from '../classes/nice-profile-crud';
import { ClipboardService } from '../modules/main/services/clipboard.service';
import { CalendarCrud } from '../classes/calendar-crud';
import { ObjectMonitoringCrud } from '../classes/object-monitoring-crud';
import { ApprovalCrud } from '../classes/approval-crud';
import { CriticalPathCrud } from '../classes/critical-path-crud';

export enum CrudOption {
  CREATE,
  CLONE,
  READ,
  UPDATE,
  DELETE,
  NONE
}

@Injectable({
  providedIn: 'root'
})
export class CRUDService {

  private readonly crudClasses: { [key: string]: any } = {
    "ENVIRONMENT": EnvironmentCrud,
    "EXIT STATE DEFINITION": ExitStateDefinitionCrud,
    "EXIT STATE MAPPING": ExitStateMappingCrud,
    "EXIT STATE PROFILE": ExitStateProfileCrud,
    "EXIT STATE TRANSLATION": ExitStateTranslationCrud,
    "FOOTPRINT": FootprintCrud,
    "RESOURCE STATE DEFINITION": ResourceStateDefinitionCrud,
    "RESOURCE STATE MAPPING": ResourceStateMappingCrud,
    "RESOURCE STATE PROFILE": ResourceStateProfileCrud,
    "JOB": JobCrud,
    "VERSIONED JOB": JobCrud,
    "BATCH": JobCrud,
    "VERSIONED BATCH": JobCrud,
    "MILESTONE": JobCrud,
    "VERSIONED MILESTONE": JobCrud,
    "TRIGGER": TriggerCrud,
    "BOOKMARKING": BookmarkingCrud,
    "COMMENT": CommentCrud,
    "USER": UserCrud,
    "GROUP": GroupCrud,
    "SYSTEM INFORMATION": SysinfoCrud,
    "SUBMITTED ENTITY": SubmittedEntityCrud,
    "WATCH TYPE": WatchTypeCrud,
    "OBJECT MONITOR": ObjectMonitoringCrud,
    "NAMED RESOURCE": NamedResourceCrud,
    "CATEGORY": CategoryCrud,
    "STATIC": NamedResourceCrud,
    "SYSTEM": NamedResourceCrud,
    "SYNCHRONIZING": NamedResourceCrud,
    "POOL": NamedResourceCrud,
    "FOLDER": FolderCrud,
    "INTERVAL": IntervalCrud,
    "SCHEDULES": SchedulesCrud,
    "SCOPE": ScopeCrud,
    "SERVER": ScopeCrud,
    "MONITOR MASTER": MonitorMasterCrud,
    "MONITOR JOBS": MonitorJobsCrud,
    "MONITOR DETAIL": MonitorJobsCrud,
    "CALENDAR": CalendarCrud,
    "RESOURCE": ResourceCrud,
    "GRANT": GrantCrud,
    "NICE PROFILE": NiceProfileCrud,
    "APPROVALS": ApprovalCrud,
    "CRITICAL PATH": CriticalPathCrud
    /** Add more types above in the fashion "<TAB-TYPE>":<CRUD.classname> **/
  };


  private initializedCRUDs: any = {};


  /* CRUDS should not be created or used directly outside this service, due to post maniuplations of some crud functionality (see create, update of crud.service). */
  constructor(private commandService: CommandApiService,
    private bookmarkService: BookmarkService,
    private router: Router,
    private toolbox: ToolboxService,
    private eventEmitterService: EventEmitterService,
    private privilegesService: PrivilegesService,
    private dialog: MatDialog,
    private treeFunctionService: TreeFunctionsService,
    private customPropertyService: CustomPropertiesService,
    private submitEntityService: SubmitEntityService,
    private mainInformationService: MainInformationService,
    private clipBoardService: ClipboardService) {

  }

  // obj can be a bicsuite object or a tab object.
  // tab objects are used for read operations
  // throws a HttpErrorResponse on failure
  execute(crudType: string, option: CrudOption, obj: any, tabInfo: Tab): Promise<any> {

    let crud = this.createOrGetCrud(crudType);

    if (crud !== undefined) {
      if (option === CrudOption.CREATE) {
        return this.create(crud, obj, tabInfo);
      }
      if (option === CrudOption.CLONE) {
        return this.clone(crud, obj, tabInfo);
      }
      if (option === CrudOption.READ) {
        return this.read(crud, obj, tabInfo);
      }
      if (option === CrudOption.UPDATE) {
        return this.update(crud, obj, tabInfo);
      }
      if (option === CrudOption.DELETE) {
        return crud.drop(obj, tabInfo);
      }
      if (CrudOption.NONE) {
        return new Promise(function () { }); // NOOP.
      }
    }
    // Tell developer that CRUD Type does not exist.
    return Promise.reject(
      console.warn("[CRUDService] Execution of CRUDService skipped: CrudType \"" + crudType + "\" does not exist."));
  }

  executeCustom(crudType: string, params: any, bicsuiteObject: any, tabInfo?: Tab) {
    let crud = this.createOrGetCrud(crudType);
    return crud.custom(params, bicsuiteObject, tabInfo);
  }

  getEditorform(crudType: string): any {
    let crud = this.createOrGetCrud(crudType);
    if (crud === undefined) {
      console.warn("[CRUDService] Problems on getting the Editorform occured: CrudType \"" + crudType + "\" does not exist.");
      return null;
    }

    return crud.editorform();
  }

  // Default bicsuite object for a specific Crud to create new items
  default(tabInfo: Tab): any {
    let crudType = tabInfo.TYPE;
    let crud = this.createOrGetCrud(crudType);
    if (crud === undefined) {
      console.warn("[CRUDService] Problems on getting the Editorform occured: CrudType \"" + crudType + "\" does not exist.");
      return null;
    }
    return crud.default(tabInfo);
  }

  // passed in editorformOptions are called by reference and thus are manipulated directly!!
  // ** Note this function should not manipulate other fields (that's why they are passed as readonly) **/
  manipulateEditorformOptions(crudType: string, editorformOptions: EditorformOptions, editorformField: Readonly<any>, tabInfo: Readonly<Tab>, bicsuiteObject: Readonly<any>, table?: any) {
    this.createOrGetCrud(crudType).manipulateEditorformOptions(editorformOptions, editorformField, bicsuiteObject, tabInfo, table);
  }

  private create(crud: Crud, obj: any, tabInfo: Tab): Promise<any> {
    return crud.create(obj, tabInfo).then(async (result: any) => {
      // On success, get the new Object as well.
      if (result.hasOwnProperty("FEEDBACK")) {
        result.CREATED = await this.read(crud, obj, tabInfo);
      }
      return result;
    });
  }

  private clone(crud: Crud, obj: any, tabInfo: Tab): Promise<any> {
    return crud.clone(obj, tabInfo).then(async (result: any) => {
      // On success, get the new Object as well.
      if (result.hasOwnProperty("FEEDBACK")) {
        result.CREATED = await this.read(crud, obj, tabInfo);
      }
      return result;
    });
  }

  // is used by the context menu aswell
  read(crud: Crud, obj: any, tabInfo: Tab): Promise<any> {
    return crud.callMethod("read", obj, tabInfo).then((result: any) => {
      // Special Case. If read is not supported by a crud (Crud should return a null object on that case)
      if (Object.keys(result).length === 0) {
        return null;
      }
      // Original Name is needed for updating a object
      else if (result.response.DATA !== undefined) {
        result.response.DATA.RECORD.ORIGINAL_NAME = result.response.DATA.RECORD.NAME;
      }
      result.editorform = crud.editorform();
      return result;
    });
  }

  private update(crud: Crud, obj: any, tabInfo: Tab): Promise<any> {
    return crud.update(obj, tabInfo).then(async (result: any) => {
      // On success, get the new Object as well.
      if (result.hasOwnProperty("FEEDBACK")) {
        result.UPDATED = await this.read(crud, obj, tabInfo);
      }
      return result;
    });
  }

  public createOrGetCrud(crudType: string): Crud {
    if (crudType in this.crudClasses) {
      let crudClass = this.crudClasses[crudType];
      let key = crudClass.name;

      if (this.initializedCRUDs[key] == undefined) {
        this.initializedCRUDs[key] = new crudClass
          (this.commandService,
            this.bookmarkService,
            this.router,
            this.toolbox,
            this.eventEmitterService,
            this.privilegesService,
            this.dialog,
            this.treeFunctionService,
            this.customPropertyService,
            this.submitEntityService,
            this.mainInformationService,
            this.clipBoardService
          );
      }

      return this.initializedCRUDs[key];
    }

    throw console.warn("Crud Type [" + crudType + "] does not exist.");
  }

}
