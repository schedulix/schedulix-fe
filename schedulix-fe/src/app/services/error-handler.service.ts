import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MatLegacyDialog as MatDialog, MatLegacyDialogRef as MatDialogRef, MatLegacyDialogState as MatDialogState } from '@angular/material/legacy-dialog';
import { throwError } from 'rxjs/internal/observable/throwError';
import { ErrorDialogComponent } from '../modules/main/components/dialog-components/error-dialog/error-dialog.component';
import { ToolboxService } from './toolbox.service';

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(private dialog: MatDialog, private toolboxService: ToolboxService) { }

  unclosedErrors: any[] = []

  dialogRef: MatDialogRef<ErrorDialogComponent, any>| undefined;



  createError(error: BicsuiteJavascriptException | sdmsException | BicsuiteHttpException) {
    // console.log('asdS')
    error.dialogId = this.toolboxService.uuidv4()
    this.unclosedErrors.unshift(error)
    // post error in console
    // console.error(error)

    // this.closeAll()
    // create Dialog
    if(this.dialogRef == undefined) {
      // The dialog is opened.
      this.dialogRef = this.dialog.open(ErrorDialogComponent, {
        data: this.unclosedErrors,
        panelClass: "fe-no-border-dialog",
        // width: "700px"
      });

    } else{
      if(this.dialogRef.getState() === MatDialogState.CLOSED || this.dialogRef.getState() === MatDialogState.CLOSING){
        this.dialogRef = this.dialog.open(ErrorDialogComponent, {
          data: this.unclosedErrors,
          panelClass: "fe-no-border-dialog"
          // width: "700px"
        });
      } else {
        // add errors to existing dialog\
        let componentInstance = this.dialogRef.componentInstance;
        if(componentInstance){
            componentInstance.data = this.unclosedErrors;
        }
      }
    }



    this.dialogRef.afterClosed().toPromise().then((result: any) => {
      // console.log(result);
      if(result){
        // has been closed manually
        // clear errors
        this.unclosedErrors = [];
      }
    });
  }

  openDialog(){

  }

  closeAll(){
    this.dialog.closeAll()
    // this.dialog.getDialogById()
  }
}



export abstract class genericExeption {

  exceptionType: frontendExceptionTypes = 0;
  exceptionExcuse: string = '';
  time: Date = new Date();
  // gets an id from the dialog
  dialogId = '';
  abstract data: any;

  constructor(exceptionType: frontendExceptionTypes, exceptionExcuse: string){
    this.exceptionType = exceptionType;
    this.exceptionExcuse = exceptionExcuse;
  }
  getData(){
    return this.data;
  }

  getExceptionType(){
    return this.exceptionType
  }

  getTime(){
    return this.time;
  }

  // js error are our fault, SDMS errors can be users fault -> different headers
  getExceptionExcuse(){
    return this.exceptionExcuse;
  }

  // low prio
  // getForm(){
  // }

}

export enum frontendExceptionTypes {
  // 0, 1, 2, ... , n
  javascriptException, sdmsException, httpException
}

export class BicsuiteJavascriptException extends genericExeption{

  data: BicsuiteJavascriptExceptionData = {
    stacktrace: ''
  }

  constructor( data: any){
    // must call super since extend abstract class
    super(frontendExceptionTypes.javascriptException, 'sorry_something_went_Wrong');
    this.data.stacktrace = data;
  }
}
interface sdmsExceptionData {
  error: any,
  command: string
}


export class sdmsException extends genericExeption{

  data: sdmsExceptionData = {
    error: '',
    code: '',
    command: '',
  }
  constructor(data: any, command: string, code: string){
    // must call super since extend abstract class
    super(frontendExceptionTypes.sdmsException, 'statement_error');
    this.data.error = data;
    this.data.command = command;
    this.data.code = code;
  }
}



export class BicsuiteHttpException extends genericExeption{

  data: HttpExceptionData = {
    error: '',
    code: '',
    command: '',
    url: ''
  }

  constructor(data: any, command: string, code: string, url: string){
    // must call super since extend abstract class
    super(frontendExceptionTypes.httpException, 'connection_error');
    this.data.error = data;
    this.data.command = command;
    this.data.code = code;
    this.data.url = url;
  }
}


interface HttpExceptionData {
  error: any,
  command: string,
  code: string,
  url: string
}
interface sdmsExceptionData {
  error: any,
  command: string,
  code: string
}
interface BicsuiteJavascriptExceptionData {
  stacktrace: any
}
