import { Injectable } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import moment  from 'moment';
import * as momentTimezone from 'moment-timezone';
import { masterTreeNode } from '../interfaces/tree';


@Injectable({
  providedIn: 'root'
})
export class ToolboxService {

  constructor() { }

  count: number = 0;

  findAndRemoveObjectArray(oArray: any[], o: any) {
    // be carefull with the any type, find and remove will cast the object type to any
    oArray = oArray.filter(function (obj) {
      return obj != o;
    });
    return oArray
  }

  isEmpty(ObjectArray: any[]): boolean {
    // be carefull with the any type, find and remove will cast the object type to any
    if (ObjectArray === undefined || ObjectArray.length == 0) {
      return true;
    } else {
      return false
    }
  }
  // removes dublicates
  arrayRemoveDublicates(array: string[]) {
    var a = array.concat();
    for (var i = 0; i < a.length; ++i) {
      for (var j = i + 1; j < a.length; ++j) {
        if (a[i] === a[j])
          a.splice(j--, 1);
      }
    }

    return a;
  }

  textQuote(text: string): string {
    let t = '';
    if (text) {
      t = text;
    }
    let result = t.replace(/\\/g, '\\\\').replace(/'/g, "\\'");
    return result;
  }

  namePathQuote(string: string): string {
    let splittedStr = string.split(".");
    if (splittedStr[0] == "") {
      splittedStr.shift();
    }
    if (splittedStr.length <= 0) {
      return string;
    }
    for (let i = 1; i < splittedStr.length; i++) {
      splittedStr[i] = "'" + splittedStr[i] + "'";
    }
    let res = splittedStr.join('.');
    return res;
  }

  // 'A'='B', 'C': Matches content between quotes  => 'A' 'B' 'C'
  quoteMatch(str: string): string[] | null {
    let match: string[] | null = str.match(/(["'])(?:(?=(\\?))\2.)*?\1/g);

    if (match != null) {
      for (let i = 0; i < match.length; i++)
        match[i] = match[i].slice(1, match[i].length - 1) // Remove first and last '
    }

    return match;
  }

  // Used to check validation errors on forms
  getFormErrorMessage(formControl: AbstractControl) {
    if (formControl.hasError('required')) {
      return 'mandator_please_enter_value';
    }
    if (formControl.hasError('pattern')) {
      return 'invalid_characters';
    }
    if (formControl.hasError('minlength')) {
      return 'value_has_too_few_characters';
    }
    if (formControl.hasError('maxlength')) {
      return 'value_has_too_many_characters';
    }
    return '';
  }

  isPrimitive(target: any) {
    return (target !== Object(target));
  }

  uuidv4(): string {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }

  deQuote(str: string): string {
    if (str[0] == "'") {
      str = str.substring(1);
    }

    if (str[str.length - 1] == "'") {
      str = str.substring(0, str.length - 1);
    }
    return str;
  }

  deepEquals(x: any, y: any) {
    let debug = false;
    if (x === y) {
      return true; // if both x and y are null or undefined and exactly the same
    } else if (!(x instanceof Object) || !(y instanceof Object)) {
      if (debug) console.log("!(x instanceof Object) || !(y instanceof Object)");
      return false; // if they are not strictly equal, they both need to be Objects
    } else if (x.constructor !== y.constructor) {
      // they must have the exact same prototype chain, the closest we can do is
      // test their constructor.
      if (debug) console.log("x.constructor !== y.constructor");
      return false;
    } else {
      for (const p in x) {
        // TODO: f_table is deprecated
        if (p == 'rowIndex' || p == 'f_table' || p == 'F_REFRESH_NAVIGATOR') {
          continue
        }
        if (typeof p === 'string') {
          if (p.startsWith('NOFORM_')) {
            continue
          }
        }
        if (!x.hasOwnProperty(p)) {
          continue; // other properties were tested using x.constructor === y.constructor
        }
        if (!y.hasOwnProperty(p)) {
          if (debug) console.log("!y.hasOwnProperty(p)");
          if (debug) console.log(p);
          return false; // allows to compare x[ p ] and y[ p ] when set to undefined
        }
        if (x[p] === y[p]) {
          continue; // if they have the same strict value or identity then they are equal
        }
        if (typeof (x[p]) !== 'object') {
          if (debug)  console.log(x);
          if (debug)  console.log(y);
          if (debug)  console.log(typeof (x[p]));
          if (debug)  console.log(p);
          if (debug)  console.log('>' + x[p] + '<')
          if (debug)  console.log('>' + y[p] + '<')
          return false; // Numbers, Strings, Functions, Booleans must be strictly equal
        }
        if (!this.deepEquals(x[p], y[p])) {
          if (debug)  console.log(x[p], y[p])
          return false;
        }
      }
      for (const p in y) {
        if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) {
          return false;
        }
      }
      return true;
    }
  }

  deepNulltoEmptyString(o: any) {
    // In some cases we need to keep the null for example job paramater DEFAULT_VALUES
    let keepNullPropeties = [ "DEFAULT_VALUE"];
    if (!(o instanceof Object)) return;
    for (let p in o) {
      if (o[p] == null && !keepNullPropeties.includes(p)) {
        o[p] = '';
      }
      else {
        if (typeof (o[p]) == 'object') {
          this.deepNulltoEmptyString(o[p]);
        }
      }
    }
  }

  quoteSeperatedString(str: string, seperator: string, trim: boolean = true): string {
    if (str) {
      let strArray = str.split(seperator);
      let resultArray = [];
      for (let str of strArray) {

        resultArray.push("'" + (trim ? str.trim() : str) + "'")
      }
      return resultArray.join(seperator);
    } else {
      return str;
    }
  }

  deepCopy = <T>(target: T): T => {
    // console.log(this.count++);
    if (target === null) {
      return target;
    }
    if (target instanceof Date) {
      return new Date(target.getTime()) as any;
    }
    if (target instanceof Array) {
      const cp = [] as any[];
      (target as any[]).forEach((v) => { cp.push(v); });
      return cp.map((n: any) => this.deepCopy<any>(n)) as any;
    }


    // test deepcopy
    if (typeof target === 'object' && Object.keys(target).length !== 0) {

      const cp = { ...(target as { [key: string]: any }) } as { [key: string]: any };
      // console.log(cp)
      Object.keys(cp).forEach(k => {
        if (k != 'f_table') {
          cp[k] = this.deepCopy<any>(cp[k]);
        }
      });
      return cp as T;
    }
    return target;
  };

  escapeString(str: string, charToBeEscaped: string, escapingChar: string): string {
    if (str) {
      let reg: RegExp = new RegExp(charToBeEscaped, "g")
      str = str.replace(reg, escapingChar + charToBeEscaped);
      return str;
    }
    return str;
  }

  replaceSpecialCharacter(str: string): string {
    if (str) {
      str = str.replace(/[^a-zA-Z0-9]/g, '')
      return str;
    }
    return str;
  }


  // input timestamp in iso format 'YYYY-MM-DD hh:mm timezone'
  // output format defauls to 'YYYY-MM-DD hh:mm timezone'
  convertTimeStamp(timestamp: string, toTimeZone: string, format: string, addTz?: boolean): string {
    // console.log(timestamp)
    let l = timestamp.split(' ');
    let cts = momentTimezone.tz(l[0], l[1]).tz(toTimeZone).format(format) + (addTz ? " " + toTimeZone : "");

    // console.log(cts)
    return cts ;
  }

  convertTimeStampUTC(timestamp: string, format: string) {
    let l = timestamp.split(' ');
    return momentTimezone.tz(l[0], l[1]).tz('UTC').format(format);
  }
  // rawUtcServerDate == YYYY-MM-DD hh:mm timezone
  // convertRawTimeStamp(specifiedTimezone: string, rawUtcServerDate: string): string{

  //   let serverDateSplit = rawUtcServerDate.split(" ")
  //   let cts = momentTimezone.tz(serverDateSplit[0], serverDateSplit[1]).tz(specifiedTimezone).format()
  //   // momentTimezone(serverTime)
  //   console.log(serverDateSplit[0])
  //   console.log(cts)
  //     return '';
  // }

  mktime(timestamp: string) {
    return momentTimezone.tz(timestamp, "GMT").unix();
  }

  unixTimetoISO (unixTimestamp: string) {
    // return moment(unixTimestamp, "X").format();
    return momentTimezone.tz(moment(unixTimestamp, "X"), "GMT").format().substring(0,16);
  }

  wrapPathStringVertical(pathString: string): string {
    let vpath = ''

    vpath = pathString.replace(/\./g, '.\n')

    return vpath;
  }

  waitForElm(selector: any) {
    // console.log("wait for:" + selector)
    return new Promise(resolve => {
      if (document.getElementById(selector)) {
        return resolve(document.getElementById(selector));
      }

      const observer = new MutationObserver(mutations => {
        if (document.getElementById(selector)) {
          resolve(document.getElementById(selector));
          observer.disconnect();
        }
      });

      observer.observe(document.body, {
        childList: true,
        subtree: true,
      });
    });
  }

  getLogUrl(logfile: string, node: any): string {
    let url: string = ''
    if (logfile[0] == '/' || logfile[1] == ':') {
      url = 'http://' + node.logUrl + '?FNAME=' + logfile;
    } else {
      url = 'http://' + node.logUrl + '?FNAME=' + node.WORKDIR + '/' + logfile;
    }

    return url;
  }

  makeLogPath(httphost: string, httpport: string): string {
    // if(if FILENAME[0] == '/' or FILENAME[1] == ':':)
    return httphost + ':' + httpport
  }

  OpenTextWindow(logfile: string, node: masterTreeNode) {
    let url = this.getLogUrl(logfile, node);
    let w = window.open(url, "_blank", "scrollbars=yes, resizable=yes, width=800, height=600");
    try {
      w ? w.focus() : '';
    } catch (e) {
      alert("Failed to open popup. Check your browser settings to allow javascript to open popup windows");
    }
  }
}
