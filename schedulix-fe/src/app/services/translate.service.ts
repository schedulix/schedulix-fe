import { Injectable } from '@angular/core';
import lang_dictionary from '../json/localization.json';
import { LocalStorageHelper } from './local-storage-helper.service';

@Injectable({
  providedIn: 'root'
})


export class TranslateService {

  // lang set to display in the appnn
  public languages = lang_dictionary.languages;

  // default lang, change this key value for lang change
  public language: string = 'de';
  private f_warned: boolean = false;

  // dictionary is now saved at localization.json
  private dictionary: { [key: string]: TranslationSet } = lang_dictionary.translate; 

  constructor( private localStorageHelper:LocalStorageHelper) { 
    if(this.localStorageHelper.getValue('lang') == '') {
      // console.log(this.localStorageHelper.getValue('lang'))
      this.localStorageHelper.setItem('lang', this.language)
    } else {
      this.language = this.localStorageHelper.getValue('lang');
    }
  }

  //return the translated value
  translate(key: string) {
    if (typeof key === 'undefined')
        return "undefined";
    let paramList = [];
    let pos = 0;
    while (true) {
      pos = key.indexOf("{", pos) + 1;
      if (pos <= 0) {
        break;
      }
      paramList.push(key.substring(pos, key.indexOf("}", pos)));
    }
    if (paramList.length > 0) {
      // console.log(paramList);
      key = key.replace(/\{[^\}]*\}/g, "{}");
      // console.log(key);
    }
    if (this.dictionary[key] != null) {
      let translation = (this.dictionary as any)[key][this.language];
      pos = -1;
      while (true) {
        pos = translation.indexOf("{", pos) + 1;
        if (pos <= 0) {
          break;
        }
        let endPos =  translation.indexOf("}", pos) + 1;
        let paramString = translation.substring(pos - 1, endPos);
        // console.log(paramString);
        // console.log(paramString.substring(1, paramString.length - 1));
        let paramIdx = parseInt(paramString.substring(1, endPos - 1)) - 1;
        // console.log(paramIdx);
        if (paramList.length > paramIdx) {
          // console.log(translation);
          translation = translation.replace (paramString, paramList[paramIdx])
          // console.log(translation);
        }
      }
      // if (paramList.length > 0) console.log(translation);
      return translation;
    } else {
      if (this.f_warned == false) {
        console.warn("there is no Key in the localization.json for key: " + key);
        this.f_warned = true;
      }
      return "KEY NOT FOUND["+ key + "]";
    }
  }

}

// To display new languages, they have to be defined in here, so that the dictionary knows what language keys are available/valid.
export class TranslationSet {
  //+undefined because of no initialization of the parameters necessary
  public en: string | undefined;
  public de: string | undefined;
  public nl: string | undefined;
}
