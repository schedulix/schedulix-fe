import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { ChangeDetectorRef, Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { ToolboxService } from './toolbox.service'
import { CustomPropertiesService } from './custom-properties.service';
import { ErrorHandlerService, sdmsException } from './error-handler.service';

@Injectable({
  providedIn: 'root'
})
export class CommandApiService {

  constructor(private authService: AuthService,
    private httpClient: HttpClient,
    private toolbox: ToolboxService,
    private http: HttpClient,
    private customPropertiesService: CustomPropertiesService, private errorHandlerService: ErrorHandlerService
  ) {
    // this.loadCustomJson();
  }

  url: string = this.customPropertiesService.webServerUrl;

  // command: The command to be issued  .
  // delegate: A function passed that has as first param the result of the issued command.
  // returns: Buitesuite-Server object
  executeDelegate(command: string, delegate: any, errorDelegate: any = null): any {
    let body = this.createBody(command);
    let headers = new HttpHeaders();
    headers = headers.set("Content-type", "application/x-www-form-urlencoded");

    this.httpClient.post(this.url, body, { headers: headers })
      .pipe(
        catchError((err: HttpErrorResponse) => {
          // empty observable. The error is delegated to errorDelegate(...).
          // The error is always an HttpErrorResponse.
          // alternative would be to call "throwError which invokes throw and writes a message to console"
          // throwError("Connection Error: " + err.message);
          if (errorDelegate != null) {
            errorDelegate(err);
          }
          return new Observable();
        })
      ).subscribe((result: any) => {
        return delegate(result);
      });
  }

  executeDelegateWithRegEx(command: string, delegate: any, errorDelegate: any = null): any {
    let body = this.createBody(command, 'regEx');
    let headers = new HttpHeaders();
    headers = headers.set("Content-type", "application/x-www-form-urlencoded");

    this.httpClient.post(this.url, body, { headers: headers })
      .pipe(
        catchError((err: HttpErrorResponse) => {
          // empty observable. The error is delegated to errorDelegate(...).
          // The error is always an HttpErrorResponse.
          // alternative would be to call "throwError which invokes throw and writes a message to console"
          // throwError("Connection Error: " + err.message);
          if (errorDelegate != null) {
            errorDelegate(err);
          }
          return new Observable();
        })
      ).subscribe((result: any) => {
        return delegate(result);
      });
  }

  // command: The command to be issued.
  // returns: Bicsuite-Server object.
  execute(command: string, doThrow: boolean = true): Promise<Object> {
    // console.log(command);
    let body = this.createBody(command);
    let headers = new HttpHeaders();
    headers = headers.set("Content-type", "application/x-www-form-urlencoded");

    // do throw error missing TODO

    return this.httpClient.post(this.url, body, { headers: headers }).toPromise().then(
      (result: any) => {
        // console.log(result)
        if (doThrow && result.hasOwnProperty("ERROR")) {
          this.throwSdmsException(result.ERROR.ERRORCODE, result.ERROR.ERRORMESSAGE, command);
          return Promise.reject(result);
        }
        return result;
      }
    );
  }

  throwSdmsException(errorCode: string, errorMessage: string, command: string, doThrow? : boolean) {
    let exception: sdmsException = new sdmsException(errorMessage, command, errorCode);
    this.errorHandlerService.createError(exception);
    if (doThrow) {
      throw exception;
    }
  }

  // rejectNoError(result:any){
  //   if (result.hasOwnProperty("ERROR")) {
  //     return Promise.reject(result);
  //   }
  //   return result;
  // }

  // interceptError(command: string, result: any) {
  //   console.log(command)
  //   // console.log(result)
  //   if (result.hasOwnProperty("ERROR")) {
  //     // console.log(result.ERROR)
  //     // throw new sdmsException(result.ERROR.ERRORMESSAGE, '', result.ERROR.ERRORCODE)
  //     this.throwSdmsException(result.ERROR.ERRORCODE, result.ERROR.ERRORMESSAGE, command);
  //     // console.log(this.errorHandlerService)
  //     // throw result;
  //     // causes uncaught in promise error when no reject was done with .then(acceptfunction, rejectfunction(mostly empty))
  //     return Promise.reject(result);
  //   }
  //   return result;
  // }

  executeMulticommand(stmts: string[], doThrow: boolean = true): Promise<Object> {
    let stmt = "";
    if (stmts.length == 1) {
      stmt = stmts[0];
    }
    else {
      stmt = "begin multicommand\n"
      stmt += stmts.join(";\n");
      stmt += ";\nend multicommand;";
    }
    return this.execute(stmt, doThrow);
  }

  // TODO: Not yet used still to be tested
  async executeBatch(stmts: string[], parallel: boolean, stopOnError: boolean): Promise<any[]> {
    if (parallel) {
      let promises: Promise<any>[] = [];
      for (let stmt of stmts) {
        promises.push(this.execute(stmt));
      }
      return Promise.all(promises).then((results) => {
        return results;
      });
    }
    else {
      let results: any[] = [];
      for (let stmt of stmts) {
        results.push(await this.execute(stmt));
      }
      return results;
    }
  }

  private createBody(command: string, regEx?: string) {
    let credentials = this.authService.getCredentials();

    // Web Service needs an url encoded request. Otherwise errors may occur when
    // a request is done.
    let rawBody =

      "SERVER=" + encodeURIComponent(credentials.serverName) + "&" +
      // "HOST=" + encodeURIComponent(credentials.host) + "&" +
      // "PORT=" + encodeURIComponent(credentials.port) + "&" +
      "USER=" + encodeURIComponent(credentials.username) + "&" +
      "PASSWORD=" + encodeURIComponent(credentials.password) + "&" +
      "COMMAND=" + encodeURIComponent(regEx ? command : command);
    return rawBody;
    // let rawBody2 =
    // "SERVER=" + encodeURIComponent(credentials.serverName) + "&" +
    // "USER=" + encodeURIComponent(credentials.username) + "&" +
    // "PASSWORD=" + encodeURIComponent(credentials.password) + "&" +
    // "COMMAND=" + encodeURIComponent(regEx ? command : command);
  }

  // not used!
  // executeNoProxy(command: string): Promise<Object> {
  //   let body = this.createBody(command);
  //   let headers = new HttpHeaders();
  //   headers = headers.set("Content-type", "application/x-www-form-urlencoded");

  //   return this.httpClient.post(this.url, body, { headers: headers }).toPromise().then((result) => { return this.interceptError(command, result) });
  // }
}
