import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { NegateAuthGuard } from './guards/negate-auth.guard';
import { LoginComponent } from './modules/login/login.component';

const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login',
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule) ,
    canActivate: [ NegateAuthGuard ]
  },
  {
    path: 'home',
    loadChildren: () => import('./modules/main/main.module').then(m => m.MainModule) ,
    canActivate: [ AuthGuard ]
  },
  { path: '**', redirectTo: 'login'}

];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
