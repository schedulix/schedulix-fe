import sys
import json

reload(sys)
sys.setdefaultencoding('utf-8')

def checkArguments():
    printUsage = False
    if len(sys.argv) < 2:
        printUsage = True
    elif sys.argv[1] == "sort":
        if len(sys.argv) != 3:
            print("illegal number of arguments for operation sort")
            printUsage = True
    elif sys.argv[1] == "keys":
        if len(sys.argv) != 3:
            print("illegal number of arguments for operation keys")
            printUsage = True
    elif sys.argv[1] == "merge":
        if len(sys.argv) != 4:
            print("illegal number of arguments for operation merge")
            printUsage = True
    elif sys.argv[1] == "untranslated":
        if len(sys.argv) != 4:
            print("illegal number of arguments for operation untranslated")
            printUsage = True
    else:
        print("invalid operation '" + sys.argv[1] + "'")
        printUsage = True
    if printUsage:
        print("usages:")
        print("    " + sys.argv[0] + " sort <file>")
        print("    " + sys.argv[0] + " merge <file1(leading)> <file2>")
        sys.exit(1)

def getLanguagesAndSortedWordListFromFile(filename):
    f = open(filename, "r")
    linesRead = f.readlines()
    f.close()

    joinedLinesRead = ''.join(linesRead)

    # create python data structure from joinedLinesRead 
    input = json.loads(joinedLinesRead);

    # get language
    languages = input["languages"];

    translations = input["translate"];
    wordList = list(translations.items())
    wordList.sort()
    return (languages, wordList)

def getKeysFromFile(filename):
    f = open(filename, "r")
    linesRead = f.readlines()
    f.close()

    joinedLinesRead = ''.join(linesRead)

    # create python data structure from joinedLinesRead 
    input = json.loads(joinedLinesRead);

    # get language
    translate = input["translate"];

    keyItems = list(translate.items())
    keyItems.sort()
    keys = []
    for keyItem in keyItems:
        keys.append(keyItem[0])
    return (keys)

def mergeWordLists(wordList1, wordList2):
    l1 = len(wordList1)
    l2 = len(wordList2)
    p1 = 0
    p2 = 0
    KEY = 0
    wordList = []
    while p1 < l1 or p2 < l2:
        if p1 < l1 and p2 == l2:
            wordList.append(wordList1[p1])
            p1 = p1 + 1
        elif p1 == l1 and p2 < l2:
            wordList.append(wordList2[p2])
            p2 = p2 + 1
        elif wordList1[p1][KEY] == wordList2[p2][KEY]:
            wordList.append(wordList1[p1])
            p1 = p1 + 1
            p2 = p2 + 1
        elif wordList1[p1][KEY] < wordList2[p2][KEY]:
            wordList.append(wordList1[p1])
            p1 = p1 + 1
        else:
            wordList.append(wordList2[p2])
            p2 = p2 + 1
    return wordList

def mergeLanguages(languages1, languages2):
    languages = languages1.copy()
    for lang in languages2:
        if  not (lang in languages):
            languages.append(lang)
    return languages

def printOutput(languages, wordList):
    # TODO preserve properties not in languages

    output = []
    output.append('{\n')
    output.append('  "languages": [\n')
    l = []
    for language in languages:
        l.append('    "' + language + '"')
    output.append(',\n'.join(l))
    output.append('\n  ],\n')
    output.append('  "translate": {\n')
    l = []
    for word in wordList:
        s = '    "' + word[0] + '": {\n'
        l1 = []
        for language in languages:
            l1.append('      "' + language + '": "' + word[1][language] + '"')
        for wordItem in word[1].items():
            if not (wordItem[0] in languages):
                l1.append('      "' + wordItem[0] + '": "' + str(wordItem[1]) + '"')
        s = s + ',\n'.join(l1) + '\n    }'
        l.append(s)
    output.append(",\n".join(l))
    output.append('\n  }\n')
    output.append('}\n')

    output = ''.join(output)
    #test = json.loads(output)
    #print (test)
    print(output)

checkArguments()

if sys.argv[1] == "sort":
    t = getLanguagesAndSortedWordListFromFile(sys.argv[2])
    languages = t[0]
    wordList = t[1]
    printOutput(languages, wordList)

elif sys.argv[1] == "merge":
    t1 = getLanguagesAndSortedWordListFromFile(sys.argv[2])
    t2 = getLanguagesAndSortedWordListFromFile(sys.argv[3])
    languages = mergeLanguages(t1[0], t2[0])
    wordList = mergeWordLists(t1[1], t2[1])
    printOutput(languages, wordList)

elif sys.argv[1] == "keys":
    keys = getKeysFromFile(sys.argv[2])
    for key in keys:
        print (key)

if sys.argv[1] == "untranslated":
    lang = sys.argv[2]
    t = getLanguagesAndSortedWordListFromFile(sys.argv[3])
    languages = t[0]
    wordList = t[1]
    filtersWordList = []
    for word in wordList:
        if word[1]["en"] != word[1][lang] or "ok" in word[1] or "ok_" + lang in word[1]:
            continue
        filtersWordList.append(word)
    wordList = filtersWordList
    printOutput(languages, wordList)

